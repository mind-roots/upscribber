package com.upscribber.checkout

import android.app.Application
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.upscribber.commonClasses.Constant
import com.upscribber.commonClasses.ModelStatusMsg
import com.upscribber.commonClasses.WebServicesCustomers
import com.upscribber.payment.paymentCards.PaymentCardModel
import com.upscribber.signUp.ModelVerifyEmail
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import java.io.File

class CheckoutRepository(var application: Application) {

    private val mDataBuy = MutableLiveData<ModelBuySubscription>()
    private val mDataRenew = MutableLiveData<ModelBuySubscription>()
    private val mDataTax = MutableLiveData<ModelBuySubscription>()
    val mDataFailure = MutableLiveData<ModelStatusMsg>()
    val mDataFailureTax = MutableLiveData<ModelStatusMsg>()
    val mDataStripeData = MutableLiveData<ArrayList<PaymentCardModel>>()
    val mVerifyInvite = MutableLiveData<ModelVerifyEmail>()


    fun getBuySubscription(
        auth: String,
        price: String,
        totalPri: String,
        subscriptionId: String,
        taxPrice: String,
        firstName: String,
        lastName: String,
        streetName: String,
        stateName: String,
        city: String,
        zipCode: String,
        cardId: String,
        billingType: String,
        billingStreet: String,
        billingState: String,
        billingCity: String,
        billingZip: String,
        wallet: String,
        freeTrial: String,
        totalAmount: String,
        typeDiscount: String,
        intro: String,
        phoneNumber: String,
        actualPriceParam: String,
        buyType: String,
        discount_percentage: String,
        paymentType: String,
        paymentMethodName: String,
        last4: String
    )

    {

        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.buySubscription(
            auth,
            price,
            totalPri,
            subscriptionId,
            taxPrice,
            firstName,
            lastName,
            streetName,
            stateName,
            city,
            zipCode,
            cardId,
            billingType,
            billingStreet,
            billingState,
            billingCity,
            billingZip,
            wallet,
            freeTrial,
            totalAmount,
            typeDiscount,
            intro,
            phoneNumber,
            actualPriceParam,
            buyType,
            "",
            discount_percentage,
            paymentType,
            paymentMethodName,
            last4
        )
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelBuySubscription = ModelBuySubscription()
                        val modelStatus = ModelStatusMsg()
                        if (status == "true") {
                            val data = json.getJSONObject("data")
                            modelBuySubscription.frequency_type = data.optString("frequency_type")
                            modelBuySubscription.redeemtion_cycle_qty =
                                data.optString("redeemtion_cycle_qty")
                            modelBuySubscription.amount = data.optString("amount")
                            modelBuySubscription.created_at = data.optString("created_at")
                            modelBuySubscription.campaign_name = data.optString("campaign_name")
                            modelBuySubscription.campaign_logo = data.optString("campaign_logo")
                            modelBuySubscription.business_logo = data.optString("business_logo")
                            modelBuySubscription.campaign_logo = data.optString("campaign_logo")
                            modelBuySubscription.next_billing_date =
                                data.optString("next_billing_date")
                            modelBuySubscription.business_name = data.optString("business_name")
                            modelBuySubscription.status = status
                            modelBuySubscription.msg = msg
                            modelStatus.status = status
                            modelStatus.msg = msg

                            mDataBuy.value = modelBuySubscription
                        } else {
                            modelStatus.status = status
                            modelStatus.msg = msg
                            mDataFailure.value = modelStatus
                        }

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus
            }
        })
    }

    fun getmDataFailure(): LiveData<ModelStatusMsg> {
        return mDataFailure
    }

    fun getmDataFailureTax(): LiveData<ModelStatusMsg> {
        return mDataFailureTax
    }


    fun getmDataBuySubscription(): LiveData<ModelBuySubscription> {
        return mDataBuy
    }

    fun getmDataRenewSubscription(): LiveData<ModelBuySubscription> {
        return mDataRenew
    }


    fun getmDataTax(): LiveData<ModelBuySubscription> {
        return mDataTax
    }


    fun getRetrieveCards(stripeCustomerId: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.stripeBaseUrl)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.stripeRetrieve(stripeCustomerId, "100")
        call.enqueue(object : Callback<ResponseBody> {


            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val arrayData = ArrayList<PaymentCardModel>()

                        val data = json.optJSONArray("data")
                        for (i in 0 until data.length()) {
                            val stripeData = data.getJSONObject(i)
                            val cardModel = PaymentCardModel()
                            cardModel.id = stripeData.optString("id")
                            cardModel.brand = stripeData.optString("brand")
                            cardModel.country = stripeData.optString("country")
                            cardModel.last4 = stripeData.optString("last4")
                            arrayData.add(cardModel)
                        }
                        mDataStripeData.value = arrayData

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Toast.makeText(application, "Error!", Toast.LENGTH_LONG).show()
            }
        })
    }


    fun getmDataStripeData(): LiveData<ArrayList<PaymentCardModel>> {
        return mDataStripeData
    }

    fun getTax(
        auth: String,
        price: String,
        campaignId: String,
        zip: String,
        state: String
    ) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.get_tax(auth, price, campaignId, zip, state)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelBuySubscription = ModelBuySubscription()
                        val modelStatus = ModelStatusMsg()
                        val data = json.getJSONObject("data")
                        if (status == "true") {
                            modelBuySubscription.tax = data.optString("tax")
                            modelBuySubscription.status = status
                            modelBuySubscription.msg = msg
                            mDataTax.value = modelBuySubscription
                        } else {
                            modelStatus.status = "false"
                            modelStatus.msg = "Network Error!"
                            mDataFailureTax.value = modelStatus
                        }
                    } catch (e: Exception) {
                        val modelStatus = ModelStatusMsg()
                        modelStatus.status = "false"
                        mDataFailureTax.value = modelStatus
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailureTax.value = modelStatus
            }
        })
    }

    fun getRenewCycle(
        auth: String,
        orderId: String,
        id: String,
        credit: String,
        totalAmountCredit: String,
        typeDiscount: String
    ) {

        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> =
            service.renewCycle(auth, orderId, id, credit, totalAmountCredit, typeDiscount)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelBuySubscription = ModelBuySubscription()
                        val modelStatus = ModelStatusMsg()
                        if (status == "true") {

                            modelBuySubscription.status = status
                            modelBuySubscription.msg = msg
                            modelStatus.status = status
                            modelStatus.msg = msg

                            mDataRenew.value = modelBuySubscription
                        } else {
                            modelStatus.status = status
                            modelStatus.msg = msg
                            mDataFailure.value = modelStatus
                        }

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus
            }
        })
    }

    fun getBuySubscriptionMerchant(
        userId: String,
        subTotal: String,
        total: String,
        subscriptionId: String,
        tax: String,
        firstName: String,
        email: String,
        street: String,
        state: String,
        city: String,
        zip: String,
        cardId: String,
        billingType: String,
        billingStreet: String,
        billingState: String,
        billingCity: String,
        billingZip: String,
        freeTrial: String,
        totalAmount: String,
        intro: String,
        contactNo: String,
        actualPrice: String,
        buyType: String,
        file: File,
        newUser: String,
        last4: String,
        paymentType: String,
        paymentMethodName: String,
        credit: String
    ) {

        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val userId1 = RequestBody.create(MediaType.parse("multipart/form-data"), userId)
        val subTotal1 = RequestBody.create(MediaType.parse("multipart/form-data"), subTotal)
        val total1 = RequestBody.create(MediaType.parse("multipart/form-data"), total)
        val subscriptionId1 =
            RequestBody.create(MediaType.parse("multipart/form-data"), subscriptionId)
        val tax1 = RequestBody.create(MediaType.parse("multipart/form-data"), tax)
        val firstName1 = RequestBody.create(MediaType.parse("multipart/form-data"), firstName)
        val email1 = RequestBody.create(MediaType.parse("multipart/form-data"), email)
        val street1 = RequestBody.create(MediaType.parse("multipart/form-data"), street)
        val state1 = RequestBody.create(MediaType.parse("multipart/form-data"), state)
        val city1 = RequestBody.create(MediaType.parse("multipart/form-data"), city)
        val zip1 = RequestBody.create(MediaType.parse("multipart/form-data"), zip)
        val cardId1 = RequestBody.create(MediaType.parse("multipart/form-data"), cardId)
        val billingType1 = RequestBody.create(MediaType.parse("multipart/form-data"), billingType)
        val billingStreet1 =
            RequestBody.create(MediaType.parse("multipart/form-data"), billingStreet)
        val billingState1 = RequestBody.create(MediaType.parse("multipart/form-data"), billingState)
        val billingCity1 = RequestBody.create(MediaType.parse("multipart/form-data"), billingCity)
        val billingZip1 = RequestBody.create(MediaType.parse("multipart/form-data"), billingZip)
        val freeTrial1 = RequestBody.create(MediaType.parse("multipart/form-data"), freeTrial)
        val totalAmount1 = RequestBody.create(MediaType.parse("multipart/form-data"), totalAmount)
        val intro1 = RequestBody.create(MediaType.parse("multipart/form-data"), intro)
        val contactNo1 = RequestBody.create(MediaType.parse("multipart/form-data"), contactNo)
        val newUser1 = RequestBody.create(MediaType.parse("multipart/form-data"), newUser)
        val actualPrice1 = RequestBody.create(MediaType.parse("multipart/form-data"), actualPrice)
        val buyType1 = RequestBody.create(MediaType.parse("multipart/form-data"), buyType)
        val lastName = RequestBody.create(MediaType.parse("multipart/form-data"), "")
        val last4Field = RequestBody.create(MediaType.parse("multipart/form-data"), last4)
        val paymeny_method = RequestBody.create(MediaType.parse("multipart/form-data"), paymentType)
        val paymeny_method_name = RequestBody.create(MediaType.parse("multipart/form-data"), paymentMethodName)
        val credit1 = RequestBody.create(MediaType.parse("multipart/form-data"), credit)
        val couponType = RequestBody.create(MediaType.parse("multipart/form-data"), "")
        // MultipartBody.Part is used to send also the actual file name
        val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file)

        val body = MultipartBody.Part.createFormData("signature", file.name, requestFile)

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.buySubscriptionMerchant(
            userId1,
            subTotal1,
            total1,
            subscriptionId1,
            tax1,
            firstName1,
            email1,
            street1,
            state1,
            city1,
            zip1,
            cardId1,
            billingType1,
            billingStreet1,
            billingState1,
            billingCity1,
            billingZip1,
            freeTrial1,
            totalAmount1,
            intro1,
            contactNo1,
            actualPrice1,
            buyType1,
            lastName,
            credit1,
            couponType,
            body,
            newUser1,
            last4Field,
            paymeny_method,
            paymeny_method_name
        )
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelBuySubscription = ModelBuySubscription()
                        val modelStatus = ModelStatusMsg()
                        if (status == "true") {
                            val data = json.getJSONObject("data")
                            modelBuySubscription.frequency_type = data.optString("frequency_type")
                            modelBuySubscription.redeemtion_cycle_qty =
                                data.optString("redeemtion_cycle_qty")
                            modelBuySubscription.amount = data.optString("amount")
                            modelBuySubscription.created_at = data.optString("created_at")
                            modelBuySubscription.campaign_name = data.optString("campaign_name")
                            modelBuySubscription.campaign_logo = data.optString("campaign_logo")
                            modelBuySubscription.business_logo = data.optString("business_logo")
                            modelBuySubscription.status = status
                            modelBuySubscription.msg = msg
                            modelStatus.status = status
                            modelStatus.msg = msg

                            mDataBuy.value = modelBuySubscription
                        } else {
                            modelStatus.status = status
                            modelStatus.msg = msg
                            mDataFailure.value = modelStatus
                        }

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus
            }
        })

    }



    fun userRefer(auth: String, inviteCode: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.userRefer(auth, inviteCode)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {

                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelVerifyEmail = ModelVerifyEmail()
                        val modelStatus = ModelStatusMsg()

                        if (status == "true") {
                            modelVerifyEmail.status = status
                            modelStatus.status = status
                            modelStatus.msg = msg
                            mVerifyInvite.value = modelVerifyEmail
                        } else {
                            modelVerifyEmail.status = status
                            modelVerifyEmail.msg = msg
                            modelStatus.status = status
                            modelStatus.msg = msg
                            mVerifyInvite.value = modelVerifyEmail

                        }
                    } catch (e: java.lang.Exception) {
                        e.printStackTrace()
                    }
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus
            }
        })
    }

    fun getVerifyInviteCode(): LiveData<ModelVerifyEmail> {
        return mVerifyInvite
    }
}