package com.upscribber.checkout

import android.os.Parcel
import android.os.Parcelable

class ModelCheckoutOne() : Parcelable {
    var billingStreet: String = ""
    var billingState: String = ""
    var billingCity: String = ""
    var billingZip: String = ""
    var billingType: String = ""
    var billingSuite: String = ""
    var firstName: String = ""
    var emailAddress: String = ""
    var phoneNumber: String = ""
    var streetName: String = ""
    var stateName: String = ""
    var city: String = ""
    var zipCode: String = ""
    var suite: String = ""
    var credits : String = "0.00"

    constructor(parcel: Parcel) : this() {
        billingStreet = parcel.readString()
        billingState = parcel.readString()
        billingCity = parcel.readString()
        billingZip = parcel.readString()
        billingType = parcel.readString()
        billingSuite = parcel.readString()
        firstName = parcel.readString()
        emailAddress = parcel.readString()
        phoneNumber = parcel.readString()
        streetName = parcel.readString()
        stateName = parcel.readString()
        city = parcel.readString()
        zipCode = parcel.readString()
        suite = parcel.readString()
        credits = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(billingStreet)
        parcel.writeString(billingState)
        parcel.writeString(billingCity)
        parcel.writeString(billingZip)
        parcel.writeString(billingType)
        parcel.writeString(billingSuite)
        parcel.writeString(firstName)
        parcel.writeString(emailAddress)
        parcel.writeString(phoneNumber)
        parcel.writeString(streetName)
        parcel.writeString(stateName)
        parcel.writeString(city)
        parcel.writeString(zipCode)
        parcel.writeString(suite)
        parcel.writeString(credits)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ModelCheckoutOne> {
        override fun createFromParcel(parcel: Parcel): ModelCheckoutOne {
            return ModelCheckoutOne(parcel)
        }

        override fun newArray(size: Int): Array<ModelCheckoutOne?> {
            return arrayOfNulls(size)
        }
    }


}