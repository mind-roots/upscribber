package com.upscribber.checkout

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.upscribber.commonClasses.ModelStatusMsg
import com.upscribber.payment.paymentCards.PaymentCardModel
import com.upscribber.signUp.ModelVerifyEmail
import java.io.File

class CheckOutViewModel(application: Application) : AndroidViewModel(application) {
    var checkoutRepository: CheckoutRepository = CheckoutRepository(application)

    fun getBuySubscription(
        auth: String,
        price: String,
        totalPri: String,
        subscriptionId: String,
        taxPrice: String,
        firstName: String,
        lastName: String,
        streetName: String,
        stateName: String,
        city: String,
        zipCode: String,
        cardId: String,
        billingType: String,
        billingStreet: String,
        billingState: String,
        billingCity: String,
        billingZip: String,
        wallet: String,
        freeTrial: String,
        totalAmount: String,
        typeDiscount: String,
        intro: String,
        phoneNumber: String,
        actualPriceParam: String,
        buy_type: String,
        discount_percentage: String,
        payment_type: String,
        payment_method_name: String,
        last4: String
    ) {
        checkoutRepository.getBuySubscription(
            auth,
            price,
            totalPri,
            subscriptionId,
            taxPrice,
            firstName,
            lastName,
            streetName,
            stateName,
            city,
            zipCode
            ,
            cardId,
            billingType,
            billingStreet,
            billingState,
            billingCity,
            billingZip,
            wallet,
            freeTrial,
            totalAmount,
            typeDiscount,
            intro,
            phoneNumber,
            actualPriceParam,
            buy_type,
            discount_percentage,
            payment_type,
            payment_method_name,
            last4
        )
    }

    fun getStatus(): LiveData<ModelStatusMsg> {
        return checkoutRepository.getmDataFailure()
    }

    fun getmDataFailureTax(): LiveData<ModelStatusMsg> {
        return checkoutRepository.getmDataFailureTax()
    }

    fun getmDataBuySubscription(): LiveData<ModelBuySubscription> {
        return checkoutRepository.getmDataBuySubscription()
    }

    fun getmDataRenewSubscription(): LiveData<ModelBuySubscription> {
        return checkoutRepository.getmDataRenewSubscription()
    }


    fun getmDataTax(): LiveData<ModelBuySubscription> {
        return checkoutRepository.getmDataTax()
    }


    fun getmDataStripeData(): LiveData<ArrayList<PaymentCardModel>> {
        return checkoutRepository.getmDataStripeData()
    }

    fun retrieveCards(stripeCustomerId: String) {
        checkoutRepository.getRetrieveCards(stripeCustomerId)

    }

    fun getTax(
        auth: String,
        price: String,
        campaignId: String,
        zip: String,
        state: String
    ) {
        checkoutRepository.getTax(auth, price, campaignId, zip, state)
    }

    fun getRenewCycle(
        auth: String,
        orderId: String,
        id: String,
        credit: String,
        totalAmountCredit: String,
        typeDiscount: String
    ) {
        checkoutRepository.getRenewCycle(auth, orderId, id, credit, totalAmountCredit, typeDiscount)

    }

    fun getBuySubscriptionMercahnt(
        userId: String,
        subTotal: String,
        total: String,
        subscriptionId: String,
        tax: String,
        firstName: String,
        email: String,
        street: String,
        state: String,
        city: String,
        zip: String,
        cardId: String,
        billingType: String,
        billingStreet: String,
        billingState: String,
        billingCity: String,
        billingZip: String,
        freeTrial: String,
        totalAmount: String,
        isIntro: String,
        contactNo: String,
        actualPrice: String,
        buyType: String,
        file: File,
        newUser: String,
        last4: String,
        paymentType: String,
        paymentMethodName: String,
        credit: String
    ) {

        checkoutRepository.getBuySubscriptionMerchant(
            userId,
            subTotal,
            total,
            subscriptionId,
            tax,
            firstName,
            email,
            street,
            state,
            city,
            zip,
            cardId,
            billingType,
            billingStreet,
            billingState,
            billingCity,
            billingZip,
            freeTrial,
            totalAmount,
            isIntro,
            contactNo,
            actualPrice,
            buyType,
            file,
            newUser,
            last4,
            paymentType,
            paymentMethodName,
            credit
        )

    }

    fun getInvite(): LiveData<ModelVerifyEmail> {
        return checkoutRepository.getVerifyInviteCode()
    }


    fun setCode(auth: String, invite: String) {
        checkoutRepository.userRefer(auth, invite)

    }


}