package com.upscribber.checkout

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.databinding.ActivityConfirmCheckoutBinding

class ConfirmCheckoutActivity : AppCompatActivity() {

    lateinit var mBinding :ActivityConfirmCheckoutBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding= DataBindingUtil.setContentView(this,R.layout.activity_confirm_checkout)
        clickListeners()
        setData()
    }

    private fun setData() {
        val data = intent.getParcelableExtra<ModelBuySubscription>("model")
        val imagePath = Constant.getPrefs(this).getString(Constant.dataImagePath, "")
        mBinding.txtBillingCycle.text =  data.frequency_type
        mBinding.textView335.text = "Your subscription is available to be redeemed at " + data.business_name

        if (data.redeemtion_cycle_qty == "500000"){
            mBinding.txtQuantity.text = "Unlimited"
        }else{
            mBinding.txtQuantity.text = data.redeemtion_cycle_qty
        }


        Glide.with(this).load(imagePath + data.campaign_logo).placeholder(R.mipmap.circular_placeholder).into(mBinding.imageView53)
        mBinding.amount.text = "$" + data.amount
        mBinding.textView336.text = data.campaign_name
        mBinding.txtBillingDate.text = Constant.getDateMonthh(data.created_at)
    }

    private fun clickListeners() {
        mBinding.goToSubscription.setOnClickListener {
            val sharedPreferences = getSharedPreferences("dashboard", Context.MODE_PRIVATE)
            val editor = sharedPreferences.edit()
            editor.putString("dashLayout","subscription")
            editor.apply()
            finish()
        }
    }


}
