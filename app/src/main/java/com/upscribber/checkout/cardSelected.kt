package com.upscribber.checkout

import com.upscribber.payment.paymentCards.PaymentCardModel

interface cardSelected {
    fun selectedCard(
        position: Int,
        modell: PaymentCardModel
    )
    fun editCard(position: Int, model: PaymentCardModel)
}