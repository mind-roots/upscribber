package com.upscribber.checkout

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.Outline
import android.graphics.Paint
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.upscribber.commonClasses.Constant
import com.upscribber.dashboard.DashboardActivity
import com.upscribber.payment.paymentCards.AddCardActivity
import com.upscribber.subsciptions.extrasSubsription.ExtrasSubscriptionActivity
import com.upscribber.R
import com.upscribber.commonClasses.RoundedBottomSheetDialogFragment
import com.upscribber.payment.paymentCards.PaymentCardModel
import com.upscribber.subsciptions.merchantSubscriptionPages.ModelCampaignInfo
import com.upscribber.subsciptions.merchantSubscriptionPages.ModelSubscriptionData
import com.upscribber.subsciptions.merchantSubscriptionPages.plan.PlanModel
import kotlinx.android.synthetic.main.activity_check_out_two.*
import kotlinx.android.synthetic.main.activity_check_out_two.frequencyType
import kotlinx.android.synthetic.main.activity_check_out_two.productOffer
import kotlinx.android.synthetic.main.activity_check_out_two.productPrice
import kotlinx.android.synthetic.main.activity_check_out_two.roundedImageView
import kotlinx.android.synthetic.main.activity_check_out_two.tvCompanyName
import kotlinx.android.synthetic.main.activity_check_out_two.tvName
import kotlinx.android.synthetic.main.activity_check_out_two.tvVisits
import kotlinx.android.synthetic.main.checkout_two_total.*
import kotlinx.android.synthetic.main.checkout_two_total.creditApplied
import kotlinx.android.synthetic.main.checkout_two_total.subTotal
import kotlinx.android.synthetic.main.checkout_two_total.subTotalPrice
import kotlinx.android.synthetic.main.checkout_two_total.tax
import kotlinx.android.synthetic.main.checkout_two_total.textView140
import kotlinx.android.synthetic.main.code_sheet.*
import kotlinx.android.synthetic.main.cost_too_much.*
import java.math.BigDecimal
import java.math.RoundingMode


@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class CheckOutTwo : AppCompatActivity(), cardSelected {

    private lateinit var recyclerViewCards: RecyclerView
    lateinit var toolbar: Toolbar
    lateinit var title: TextView
    lateinit var code: TextView
    lateinit var toolbarTitle: TextView
    lateinit var nextCheckout: TextView
    lateinit var tVTrial: TextView
    lateinit var tVAddCard: TextView
    lateinit var tViewText: TextView
    private var status: Boolean = true
    private lateinit var categoriesList: ArrayList<PaymentCardModel>
    private lateinit var adapterCards: AdapterCards
    var taxx: String = "0"
    var typeDiscount = ""

    companion object {
        lateinit var mViewModel: CheckOutViewModel

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_check_out_two)
        mViewModel = ViewModelProviders.of(this)[CheckOutViewModel::class.java]
        initz()
        setToolbar()

        apiInitialization()
        observerInit()
        adapters()
    }

    private fun apiInitialization() {
        val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
        val planData = intent.getParcelableExtra<PlanModel>("allData")
        val modelCheckOutOne = intent.getParcelableExtra<ModelCheckoutOne>("modelCheckOut")
        var zip = ""
        var state = ""
        var priceCalculate = ""
        if (modelCheckOutOne.billingType == "0") {
            zip = modelCheckOutOne.zipCode
            state = modelCheckOutOne.stateName
        } else {
            zip = modelCheckOutOne.billingZip
            state = modelCheckOutOne.billingState
        }

        if (intent.hasExtra("totalPrice")) {
            priceCalculate = intent.getStringExtra("totalPrice").replace("$", "")
        }

        mViewModel.getTax(auth, priceCalculate, planData.campaign_id, zip, state)
        progressBar.visibility = View.VISIBLE
    }

    private fun adapters() {
        recyclerViewCards.layoutManager =
            LinearLayoutManager(this, RecyclerView.HORIZONTAL, false) as RecyclerView.LayoutManager?
        adapterCards = AdapterCards(this, 1)
        recyclerViewCards.adapter = adapterCards

    }


    @SuppressLint("SetTextI18n")
    private fun observerInit() {
        mViewModel.getStatus().observe(this, Observer {
            progressBar.visibility = View.GONE
            if (it.msg == "Invalid auth code") {
                Constant.commonAlert(this)
            } else {
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
            }
        })


        mViewModel.getmDataFailureTax().observe(this, Observer {
            progressBar.visibility = View.GONE
            if (it.msg == "Invalid auth code") {
                Constant.commonAlert(this)
            } else {
                finish()
                Toast.makeText(this, "Please give valid details of location!", Toast.LENGTH_SHORT)
                    .show()
            }
        })


        mViewModel.getmDataTax().observe(this, Observer {
            progressBar.visibility = View.GONE
            taxx = it.tax
            if (taxx == "0") {
                tax.text = "$0.00"
            } else {
                tax.text = "$$taxx"
            }

            try {
                val modelSubscriptionNewData =
                    intent.getParcelableExtra<ModelSubscriptionData>("subscriptionData")
                clickEvents(modelSubscriptionNewData)
            } catch (e: Exception) {
                val modelSubscriptionData = ModelSubscriptionData()
                clickEvents(modelSubscriptionData)
                e.printStackTrace()
            }


        })

        mViewModel.getInvite().observe(this, Observer {
            progressBar.visibility = View.GONE
            if (it.status == "true") {
                try {
                    val modelSubscriptionNewData =
                        intent.getParcelableExtra<ModelSubscriptionData>("subscriptionData")
                    modelSubscriptionNewData.couponDiscount = "1"
                    modelSubscriptionNewData.discountedPrice =
                        Constant.getDiscountPrice("20", modelSubscriptionNewData.subTotal)
                    modelSubscriptionNewData.mainPrice = (modelSubscriptionNewData.mainPrice.toDouble() - modelSubscriptionNewData.discountedPrice.toDouble()).toString()
                    clickEvents(modelSubscriptionNewData)
                } catch (e: Exception) {
                    val modelSubscriptionData = ModelSubscriptionData()
                    clickEvents(modelSubscriptionData)
                    e.printStackTrace()
                }
                val mDialogView =
                    LayoutInflater.from(this).inflate(R.layout.invite_code_alert, null)
                val mBuilder = AlertDialog.Builder(this)
                    .setView(mDialogView)
                val mAlertDialog = mBuilder.show()
                mAlertDialog.setCancelable(false)
                mAlertDialog.window.setBackgroundDrawableResource(R.drawable.bg_alert)
                mAlertDialog.button.setOnClickListener {
                    mAlertDialog.dismiss()


                }
            } else {
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
            }
        })



        mViewModel.getmDataBuySubscription().observe(this, Observer {
            progressBar.visibility = View.GONE
            if (it.status == "true") {
                progressBar11.visibility = View.GONE
                val sharedPreferences = getSharedPreferences("dashboard", Context.MODE_PRIVATE)
                val editor = sharedPreferences.edit()
                editor.putString("dashLayout", "1")
                editor.apply()
                val intent = Intent(this, DashboardActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                intent.putExtra("showDialog", "0")
                intent.putExtra("model", it)
                startActivity(intent)
                finish()
            }
        })

    }

    private fun setToolbar() {
        setSupportActionBar(toolbar)
        title.text = ""
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)
        if (intent.hasExtra("checkOut")) {
//            toolbarTitle.text = "3-3"
        } else {
//            toolbarTitle.text = "2-2"
        }

    }

    private fun initz() {
        recyclerViewCards = findViewById(R.id.recyclerViewCards)
        toolbar = findViewById(R.id.toolbar)
        toolbarTitle = findViewById(R.id.title)
        title = findViewById(R.id.title)
        nextCheckout = findViewById(R.id.nextCheckout)
        tVTrial = findViewById(R.id.tVTrial)
        tViewText = findViewById(R.id.tViewText)
        tVAddCard = findViewById(R.id.tVAdCard)
        code = findViewById(R.id.codeTitle)
    }

    @SuppressLint("SetTextI18n")
    private fun clickEvents(modelSubscriptionNewData: ModelSubscriptionData) {
        val planData = intent.getParcelableExtra<PlanModel>("allData")
        val modelSubscription = intent.getParcelableExtra<ModelCampaignInfo>("modelSubscription")
        val imagePath = Constant.getPrefs(this).getString(Constant.dataImagePath, "")
        val profileData = Constant.getArrayListProfile(this, Constant.dataProfile)
        val modelCheckOutONe = intent.getParcelableExtra<ModelCheckoutOne>("modelCheckOut")
        val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
        val finalTax = (taxx.toDouble())
//        val finalTax = 1.0
        var discountedPrice = ""
        var totalPrice = 0.0
        var subPrice = 0.0
        var actualPrice = 0.0
        var isIntro = ""
        var discount_percentage = "0"
        var actualPriceParam = ""
//        var modelSubscriptionNewData =
//            intent.getParcelableExtra<ModelSubscriptionData>("subscriptionData")


        code.setOnClickListener {

            if (modelSubscriptionNewData.boomStatus == "0" || modelSubscriptionNewData.boomDiscount != "0.00") {
                Constant.commonErrorAlert(
                    this,
                    "A discount has already been applied for this purchase. Only one discount code can be applied at a time."
                )
            } else {
                val fragment = MenuFragmentCode(progressBar)
                fragment.show(supportFragmentManager, fragment.tag)
            }

        }
//
        val taxPrice = "$$finalTax"
        val taxPricee = taxPrice.replace("$", "")
//        tax.text = taxPrice

        tv_last_text.text = "$" + planData.price.toDouble().toInt()
        tv_last_text.paintFlags = tv_last_text.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
        var freeTrial = ""
        freeTrial = if (planData.free_trial == "No free trial" || planData.free_trial == "0") {
            "0"
        } else {
            "1"
        }
        try {
            var pricing = "$" + String.format(
                "%.2f",
                (planData.price.toDouble() - (planData.discount_price.toDouble() / 100 * planData.price.toDouble()))
            )
//            val splitProductPrice = pricing.split(".")
//            pricing = if (splitProductPrice[1] == "00" || splitProductPrice[1] == "0") {
//                splitProductPrice[0]
//            } else {
//                pricing
//            }


            if (modelSubscription.logo.isEmpty()) {
                Glide.with(this).load(R.mipmap.placeholder_subscription_square)
                    .into(roundedImageView)
            } else {
                Glide.with(this).load(imagePath + modelSubscription.logo).into(roundedImageView)
            }

            if (planData.free_trial == "No free trial" && planData.introductory_days == "0") {
                tVTrial.visibility = View.GONE
            } else if (planData.free_trial != "No free trial") {
                tVTrial.visibility = View.VISIBLE
                tVTrial.text = planData.free_trial + " free trial"
            } else {
                tVTrial.visibility = View.VISIBLE
                tVTrial.text = planData.introductory_days + " Days Introductory Price"
            }


            val typeFrequency = planData.frequency_type
            val separateFrequecy =
                typeFrequency.split("ly".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()

            if (planData.free_trial != "No free trial") {
                val separated =
                    planData.free_trial.split(" ".toRegex()).dropLastWhile { it.isEmpty() }
                        .toTypedArray()

                if (separated[1] == "Year" && planData.frequency_value == "1" || separated[1] == "Years" && planData.frequency_value == "1") {
                    tViewText.text =
                        " You will have " + (separated[0].toDouble() * 365.toDouble()).toInt() + " days to try the product for free.After this period you will be charged " + pricing + " every " + separateFrequecy[0]
                } else if (separated[1] == "Year" && planData.frequency_value != "1" || separated[1] == "Years" && planData.frequency_value != "1") {
                    tViewText.text =
                        " You will have " + (separated[0].toDouble() * 365.toDouble()).toInt() + " days to try the product for free.After this period you will be charged " + pricing + " every " + planData.frequency_value + " " + separateFrequecy[0]
                } else if (separated[1] == "Week" && planData.frequency_value == "1" || separated[1] == "Weeks" && planData.frequency_value == "1") {
                    tViewText.text =
                        " You will have " + (separated[0].toDouble() * 7.toDouble()).toInt() + " days to try the product for free.After this period you will be charged " + pricing + " every " + separateFrequecy[0]
                } else if (separated[1] == "Week" && planData.frequency_value != "1" || separated[1] == "Weeks" && planData.frequency_value != "1") {
                    tViewText.text =
                        " You will have " + (separated[0].toDouble() * 7.toDouble()).toInt() + " days to try the product for free.After this period you will be charged " + pricing + " every " + planData.frequency_value + " " + separateFrequecy[0]
                } else if (separated[1] == "Month" && planData.frequency_value == "1" || separated[1] == "Months" && planData.frequency_value == "1") {
                    tViewText.text =
                        " You will have " + (separated[0].toDouble() * 30.toDouble()).toInt() + " days to try the product for free.After this period you will be charged " + pricing + " every " + separateFrequecy[0]
                } else if (separated[1] == "Month" && planData.frequency_value != "1" || separated[1] == "Months" && planData.frequency_value != "1") {
                    tViewText.text =
                        " You will have " + (separated[0].toDouble() * 30.toDouble()).toInt() + " days to try the product for free.After this period you will be charged " + pricing + " every " + planData.frequency_value + " " + separateFrequecy[0]
                }

            } else {
                val textIntroductoryPrice = planData.introductory_days

                if (textIntroductoryPrice == "1") {
                    tViewText.text =
                        " You will have " + textIntroductoryPrice + " day to try the product for $" + planData.introductory_price + ".After this period you will be charged " + pricing + " every " + separateFrequecy[0]
                } else {
                    tViewText.text =
                        " You will have " + textIntroductoryPrice + " days to try the product for $" + planData.introductory_price + ".After this period you will be charged " + pricing + " every " + separateFrequecy[0]
                }

            }

            tvVisits.setOnClickListener {
                var redemQuantity = ""
                redemQuantity = if (planData.redeemtion_cycle_qty == "500000") {
                    "Unlimited"
                } else {
                    planData.redeemtion_cycle_qty
                }
                Constant.CommonIAlert(
                    this,
                    "Your subscription includes:",
                    redemQuantity + " " + planData.unit
                )
            }

//            if (planData.redeemtion_cycle_qty == "500000") {
//                tvVisits.text = "Unlimited " + planData.unit
//            } else {
//                tvVisits.text = planData.redeemtion_cycle_qty + " " + planData.unit
//            }
            frequencyType.text = planData.frequency_type

            frequencyType.setOnClickListener {
                Constant.CommonIAlert(
                    this,
                    "Subscription is Billed " + planData.frequency_type + " from the purchase date",
                    ""
                )
            }

            val calculatedDiscount =
                (planData.discount_price.toDouble() / 100 * planData.price.toDouble())
            val disPrice = planData.price.toDouble() - calculatedDiscount

            tvName.text = modelSubscription.name.trim()
            tvCompanyName.text = modelSubscription.company_name.trim()
            productOffer.text = planData.discount_price.toDouble().toInt().toString() + "% off"
//            subTotal.text = "$" + planData.price.toDouble().toString()
//            actualPriceParam = planData.price.toDouble().toString()
            val creditsAvailable = profileData.wallet.toDouble()


            txtRetailPrice.text = "$" + modelSubscriptionNewData.retailPrice

            if (modelSubscriptionNewData.introPrice != "0.00") {
                isIntro = "1"
                txtIntroPrice.text = "$" + modelSubscriptionNewData.introPrice
                discountPriceee.visibility = View.GONE
                textView14.visibility = View.GONE
                productOffer.visibility = View.GONE
                tv_last_text.visibility = View.GONE
                subTotal.visibility = View.GONE
                textView140.visibility = View.GONE
                if (modelSubscriptionNewData.introPrice.contains(".")) {
                    val splitProPrice = modelSubscriptionNewData.introPrice.split(".")
                    if (splitProPrice[1] == "00" || splitProPrice[1] == "0") {
                        productPrice.text = splitProPrice[0]
                    } else {
                        productPrice.text =
                            BigDecimal(modelSubscriptionNewData.introPrice).setScale(
                                2,
                                RoundingMode.HALF_UP
                            ).toString()
                    }
                } else {
                    productPrice.text = BigDecimal(modelSubscriptionNewData.introPrice).setScale(
                        2,
                        RoundingMode.HALF_UP
                    ).toString()
                }
            } else {
                isIntro = "0"
                txtIntroPrice.visibility = View.GONE
                introPrice.visibility = View.GONE
                productOffer.visibility = View.VISIBLE
                tv_last_text.visibility = View.VISIBLE
                val discount = Constant.getDiscountPrice(
                    modelSubscriptionNewData.discountPercentage,
                    modelSubscriptionNewData.retailPrice
                ).toString()
                discountPriceee.text = "-$" + BigDecimal(
                    discount
                ).setScale(2, RoundingMode.HALF_UP)
                Log.e("discountPrice", discountPriceee.text.toString())
//                discountPriceee.setTextColor(resources.getColor(R.color.accent))
                textView14.text = "Discount - ${modelSubscriptionNewData.discountPercentage.replace(
                    ".00",
                    ""
                ).replace(".0", "")}% off"
                subTotal.text = "$" + modelSubscriptionNewData.subscriptionPrice
                if (modelSubscriptionNewData.subscriptionPrice.contains(".")) {
                    val splitProPrice = modelSubscriptionNewData.subscriptionPrice.split(".")
                    if (splitProPrice[1] == "00" || splitProPrice[1] == "0") {
                        productPrice.text = splitProPrice[0]
                    } else {
                        productPrice.text =
                            BigDecimal(modelSubscriptionNewData.subscriptionPrice).setScale(
                                2,
                                RoundingMode.HALF_UP
                            ).toString()
                    }
                } else {
                    productPrice.text =
                        BigDecimal(modelSubscriptionNewData.subscriptionPrice).setScale(
                            2,
                            RoundingMode.HALF_UP
                        ).toString()
                }
            }

            if (modelSubscriptionNewData.credits != "0.00") {
                creditApplied.text =
                    "-$" + modelSubscriptionNewData.credits.replace("$", "").replace("-", "")
//                creditApplied.setTextColor(resources.getColor(R.color.accent))
            } else {
                creditApplied.visibility = View.GONE
                credits.visibility = View.GONE
            }

            if (modelSubscriptionNewData.priceAfterDiscount != "0.00") {
                subTotalPrice.text =
                    "$" + BigDecimal(modelSubscriptionNewData.priceBeforeDiscount).setScale(
                        2,
                        RoundingMode.HALF_UP
                    )
            } else {
                subTotalPrice.text =
                    "$" + BigDecimal(modelSubscriptionNewData.subTotal).setScale(
                        2,
                        RoundingMode.HALF_UP
                    )
            }


            totalPricee.text =
                "$" + (BigDecimal(modelSubscriptionNewData.mainPrice.toDouble() + finalTax).setScale(
                    2,
                    RoundingMode.HALF_UP
                )).toString()

            if (modelSubscriptionNewData.boomStatus == "0" || modelSubscriptionNewData.boomDiscount != "0.00") {
                boomDiscount.visibility = View.VISIBLE
                textBoomDiscount.visibility = View.VISIBLE
                boomDiscount.text =
                    "Discount code - Share ${modelSubscriptionNewData.boomDiscount}% off"
                typeDiscount = "2"
                textBoomDiscount.text =
                    "-$" + BigDecimal(modelSubscriptionNewData.discountedPrice).setScale(
                        2,
                        RoundingMode.HALF_UP
                    )
//                textBoomDiscount.setTextColor(resources.getColor(R.color.accent))
                discount_percentage = modelSubscriptionNewData.boomDiscount
            } else if (modelSubscriptionNewData.couponDiscount != "0") {
                boomDiscount.visibility = View.VISIBLE
                textBoomDiscount.visibility = View.VISIBLE
                boomDiscount.text = "Referral Discount"
                textBoomDiscount.text =
                    "-$" + BigDecimal(modelSubscriptionNewData.discountedPrice).setScale(
                        2,
                        RoundingMode.HALF_UP
                    )
//                textBoomDiscount.setTextColor(resources.getColor(R.color.accent))
                typeDiscount = "3"
                discount_percentage = "20"
            } else {
                typeDiscount = ""
                discount_percentage = "0"
                boomDiscount.visibility = View.GONE
                textBoomDiscount.visibility = View.GONE
            }

            if (modelSubscriptionNewData.credits.replace("$", "") != "0.00") {

                if (modelSubscriptionNewData.boomStatus == "0" || modelSubscriptionNewData.boomDiscount != "0.00") {
                    subPrice = modelSubscriptionNewData.mainPrice.toDouble()
                    actualPrice =
                        modelSubscriptionNewData.subscriptionPrice.toDouble() - modelSubscriptionNewData.discountedPrice.toDouble()

                } else if (modelSubscriptionNewData.couponDiscount != "0") {
                    subPrice = modelSubscriptionNewData.mainPrice.toDouble()
                    actualPrice =
                        modelSubscriptionNewData.subscriptionPrice.toDouble() - modelSubscriptionNewData.discountedPrice.toDouble()

                } else {
                    subPrice =
                        BigDecimal(modelSubscriptionNewData.subscriptionPrice.toDouble() - modelSubscriptionNewData.discountedPrice.toDouble()).setScale(
                            2,
                            RoundingMode.HALF_UP
                        ).toDouble()
                    actualPrice =
                        modelSubscriptionNewData.subscriptionPrice.toDouble()
                }
            } else {
                if (modelSubscriptionNewData.boomStatus == "0" || modelSubscriptionNewData.boomDiscount != "0.00") {
                    subPrice = modelSubscriptionNewData.mainPrice.toDouble()
                    actualPrice =
                        modelSubscriptionNewData.mainPrice.toDouble()

                } else if (modelSubscriptionNewData.couponDiscount != "0") {
                    subPrice = modelSubscriptionNewData.mainPrice.toDouble()
                    actualPrice =
                        modelSubscriptionNewData.mainPrice.toDouble()
                } else {
                    subPrice =
                        BigDecimal(modelSubscriptionNewData.subscriptionPrice.toDouble() - modelSubscriptionNewData.discountedPrice.toDouble()).setScale(
                            2,
                            RoundingMode.HALF_UP
                        ).toDouble()
                    actualPrice =
                        modelSubscriptionNewData.subscriptionPrice.toDouble()
                }
            }


            /*       if (planData.introductory_days != "0") {
                       txtIntroPrice.visibility = View.VISIBLE
                       textView140.visibility = View.GONE
                       productOffer.visibility = View.GONE
                       tv_last_text.visibility = View.GONE
                       subTotal.visibility = View.GONE
                       introPrice.visibility = View.VISIBLE
                       productPrice.text = planData.introductory_price.toDouble().toInt().toString()
                       isIntro = "1"
                       txtIntroPrice.text = "$" + planData.introductory_price
       //                actualPriceParam = planData.introductory_price
                       discountPriceee.text = "$0"
                       if (profileData.wallet == "0.00" || profileData.wallet == "0") {
                           val amounttttt = planData.introductory_price.toDouble() + finalTax
                           val amountFormated = BigDecimal(amounttttt).setScale(2, RoundingMode.HALF_UP)
                           subTotalPrice.text = "$ $amountFormated"
                           discountedPrice = amountFormated.toString()
                           totalPricee.text = "$ $amountFormated"
                           val creditTobeApplied = BigDecimal(creditsAvailable).setScale(2, RoundingMode.HALF_UP)
                           creditApplied.text = "$$creditTobeApplied"
       //                    val totalPri = "$" + String.format("%.2f", finalTax)
                           totalPrice = amountFormated.toString()
                       } else {
                           if (creditsAvailable >= planData.introductory_price.toDouble()) {
                               subTotalPrice.text = "$0"
                               discountedPrice = "0"
                               totalPricee.text = BigDecimal(finalTax).setScale(2, RoundingMode.HALF_UP).toString()
                               val creditTobeFormat = planData.introductory_price.toDouble()
                               val creditTobeApplied = BigDecimal(creditTobeFormat).setScale(2, RoundingMode.HALF_UP)
                               creditApplied.text = "$$creditTobeApplied"
                               val totalPri = "$" + String.format("%.2f", finalTax)
                               totalPrice = totalPri.replace("$", "")
                           } else {
                               val ttt = planData.introductory_price.toDouble()
                               val lastAmount = ttt - creditsAvailable
                               discountedPrice = "0"
                               val amountFormated = BigDecimal(lastAmount).setScale(2, RoundingMode.HALF_UP)
                               val amountFormated1 = BigDecimal(lastAmount + finalTax).setScale(2, RoundingMode.HALF_UP)
                               subTotalPrice.text = "$ $amountFormated"
                               totalPricee.text = "$ $amountFormated1"
                               creditApplied.text = "$$creditsAvailable"
                               val totalPri = "$" + String.format("%.2f", amountFormated1)
                               totalPrice = totalPri.replace("$", "")
                           }
                       }

                   } else {
                       txtIntroPrice.visibility = View.GONE
                       introPrice.visibility = View.GONE
                       textView140.visibility = View.VISIBLE
                       subTotal.visibility = View.VISIBLE
                       isIntro = "0"
                       val proPrice =  BigDecimal(disPrice).setScale(2, RoundingMode.HALF_UP).toString()
                       val splitProductPrice = proPrice.split(".")
                       if (splitProductPrice[1] == "00" || splitProductPrice[1] == "0") {
                           productPrice.text = splitProductPrice[0]
                       } else {
                           productPrice.text = proPrice
                       }

                       if (modelSubscription.cancel_discount != "0" && modelSubscription.cancel_discount != "") {
                           val twentyDiscount =
                               calculatedDiscount + (modelSubscription.cancel_discount.toDouble() / 100 * disPrice)
                           val disPriceFormated =
                               BigDecimal(twentyDiscount).setScale(2, RoundingMode.HALF_UP)
                           discountPriceee.text = "$$disPriceFormated"
                           discountedPrice = twentyDiscount.toString()
                           typeDiscount = "3"
                           if (creditsAvailable >= disPrice) {
                               subTotalPrice.text = "$0"
                               totalPricee.text = BigDecimal(finalTax).setScale(2, RoundingMode.HALF_UP).toString()
                               val creditTobeFormat = disPrice
                               val creditTobeApplied = BigDecimal(creditTobeFormat).setScale(2, RoundingMode.HALF_UP)
                               creditApplied.text = "$$creditTobeApplied"
                               try {
                                   val text =
                                       BigDecimal(finalTax).setScale(2, RoundingMode.HALF_UP).toString()
                                   val tttaxxx = (discountedPrice.toDouble() + text.toDouble())
                                   val totalPri = "$" + String.format("%.2f", finalTax)
                                   totalPrice = totalPri.replace("$", "")
                               } catch (e: java.lang.Exception) {
                                   e.printStackTrace()
                               }
                           } else {
                               val ttt = planData.price.toDouble() - twentyDiscount
                               val lastAmount = ttt - creditsAvailable
                               val ttPrice = lastAmount + finalTax
                               val amountFormated = BigDecimal(ttPrice).setScale(2, RoundingMode.HALF_UP)
                               val amountFormated1 =
                                   BigDecimal(lastAmount).setScale(2, RoundingMode.HALF_UP)
                               discountPriceee.text = "$$disPriceFormated"
                               discountedPrice = twentyDiscount.toString()
                               subTotalPrice.text = "$ $amountFormated1"
                               totalPricee.text = "$$amountFormated"
                               creditApplied.text = "$$creditsAvailable"
                               try {
                                   val text =
                                       BigDecimal(finalTax).setScale(2, RoundingMode.HALF_UP).toString()
                                   val tttaxxx = (discountedPrice.toDouble() + text.toDouble())
                                   val totalPri = "$" + String.format("%.2f", amountFormated)
                                   totalPrice = totalPri.replace("$", "")
                               } catch (e: java.lang.Exception) {
                                   e.printStackTrace()
                               }

                           }
                       } else if (modelSubscription.boom_discount != "0" || modelSubscription.boom_status == "0") {
                           val twentyDiscount = calculatedDiscount + (modelSubscription.boom_discount.toDouble() / 100 * disPrice)
                           val disPriceFormated = BigDecimal(twentyDiscount).setScale(2, RoundingMode.HALF_UP)
                           discountPriceee.text = "$$disPriceFormated"
                           discountedPrice = twentyDiscount.toString()
                           typeDiscount = "1"
                           if (creditsAvailable >= disPrice) {
                               subTotalPrice.text = "$0"
                               totalPricee.text =
                                   BigDecimal(finalTax).setScale(2, RoundingMode.HALF_UP).toString()
                               val creditTobeFormat = disPrice
                               val creditTobeApplied =
                                   BigDecimal(creditTobeFormat).setScale(2, RoundingMode.HALF_UP)
                               creditApplied.text = "$$creditTobeApplied"
                               try {
                                   val text =
                                       BigDecimal(finalTax).setScale(2, RoundingMode.HALF_UP).toString()
                                   val tttaxxx = (discountedPrice.toDouble() + text.toDouble())
                                   val totalPri = "$" + String.format("%.2f", finalTax)
                                   totalPrice = totalPri.replace("$", "")
                               } catch (e: java.lang.Exception) {
                                   e.printStackTrace()
                               }
                           } else {
                               val ttt = planData.price.toDouble() - twentyDiscount
                               val lastAmount = ttt - creditsAvailable
                               val ttPrice = lastAmount + finalTax
                               val amountFormated = BigDecimal(ttPrice).setScale(2, RoundingMode.HALF_UP)
                               val amountFormated1 =
                                   BigDecimal(lastAmount).setScale(2, RoundingMode.HALF_UP)
                               discountPriceee.text = "$$disPriceFormated"
                               discountedPrice = twentyDiscount.toString()
                               subTotalPrice.text = "$ $amountFormated1"
                               totalPricee.text = "$$amountFormated"
                               creditApplied.text = "$$creditsAvailable"
                               try {
                                   val text =
                                       BigDecimal(finalTax).setScale(2, RoundingMode.HALF_UP).toString()
                                   val tttaxxx = (discountedPrice.toDouble() + text.toDouble())
                                   val totalPri = "$" + String.format("%.2f", amountFormated)
                                   totalPrice = totalPri.replace("$", "")
                               } catch (e: java.lang.Exception) {
                                   e.printStackTrace()
                               }

                           }

                       } else if (modelSubscription.coupon != "0" && modelSubscription.coupon != "") {
                           val twentyDiscount = calculatedDiscount + (modelSubscription.coupon.toDouble() / 100 * disPrice)
                           val disPriceFormated =
                               BigDecimal(twentyDiscount).setScale(2, RoundingMode.HALF_UP)
                           discountPriceee.text = "$$disPriceFormated"
                           discountedPrice = twentyDiscount.toString()
                           typeDiscount = "2"
                           if (creditsAvailable >= disPrice) {
                               subTotalPrice.text = "$0"
                               totalPricee.text =
                                   BigDecimal(finalTax).setScale(2, RoundingMode.HALF_UP).toString()
                               val creditTobeFormat = disPrice
                               val creditTobeApplied =
                                   BigDecimal(creditTobeFormat).setScale(2, RoundingMode.HALF_UP)
                               creditApplied.text = "$$creditTobeApplied"
                               try {
                                   val text =
                                       BigDecimal(finalTax).setScale(2, RoundingMode.HALF_UP).toString()
                                   val tttaxxx = (discountedPrice.toDouble() + text.toDouble())
                                   val totalPri = "$" + String.format("%.2f", finalTax)
                                   totalPrice = totalPri.replace("$", "")
                               } catch (e: java.lang.Exception) {
                                   e.printStackTrace()
                               }
                           } else {
                               val ttt = planData.price.toDouble() - twentyDiscount
                               val lastAmount = ttt - creditsAvailable
                               val ttPrice = lastAmount + finalTax
                               val amountFormated = BigDecimal(ttPrice).setScale(2, RoundingMode.HALF_UP)
                               val amountFormated1 =
                                   BigDecimal(lastAmount).setScale(2, RoundingMode.HALF_UP)
                               discountPriceee.text = "$$disPriceFormated"
                               discountedPrice = twentyDiscount.toString()
                               subTotalPrice.text = "$ $amountFormated1"
                               totalPricee.text = "$$amountFormated"
                               creditApplied.text = "$$creditsAvailable"
                               try {
                                   val text =
                                       BigDecimal(finalTax).setScale(2, RoundingMode.HALF_UP).toString()
                                   val tttaxxx = (discountedPrice.toDouble() + text.toDouble())
                                   val totalPri = "$" + String.format("%.2f", amountFormated)
                                   totalPrice = totalPri.replace("$", "")
                               } catch (e: java.lang.Exception) {
                                   e.printStackTrace()
                               }
                           }
                       } else {
                           typeDiscount = ""
                           if (creditsAvailable >= disPrice) {
                               try {
                                   subTotalPrice.text = "$0"
                                   totalPricee.text = BigDecimal(finalTax).setScale(2, RoundingMode.HALF_UP).toString()
                                   val creditTobeFormat = disPrice
                                   discountedPrice = "0"
                                   val creditTobeApplied =
                                       BigDecimal(creditTobeFormat).setScale(2, RoundingMode.HALF_UP)
                                   creditApplied.text = "$$creditTobeApplied"
                                   discountPriceee.text = "$$creditTobeApplied"
                                   val text =
                                       BigDecimal(finalTax).setScale(2, RoundingMode.HALF_UP).toString()
                                   val totalPri = "$" + String.format("%.2f", finalTax)
                                   totalPrice = totalPri.replace("$", "")

                               } catch (e: Exception) {
                                   e.printStackTrace()
                               }
                           } else {
                               val ttt = planData.price.toDouble() - calculatedDiscount
                               val lastAmount = ttt - creditsAvailable
                               val lastAmount1 = lastAmount + finalTax
                               val disPriceFormated = BigDecimal(calculatedDiscount).setScale(2, RoundingMode.HALF_UP)
                               val amountFormated = BigDecimal(lastAmount1).setScale(2, RoundingMode.HALF_UP)
                               discountPriceee.text = "$$disPriceFormated"
                              var lastAmount4 =  BigDecimal(lastAmount).setScale(2, RoundingMode.HALF_UP)
                               subTotalPrice.text = "$ $lastAmount4"
                               discountedPrice = lastAmount4.toString()
                               totalPricee.text = "$ $amountFormated"
                               creditApplied.text = "$$creditsAvailable"
                               try {
                                   val lastAmount2 = (disPrice - creditsAvailable)
                                   val totalPri = "$" + String.format("%.2f", (lastAmount2 + finalTax))
                                   totalPrice = totalPri.replace("$", "")
                               } catch (e: java.lang.Exception) {
                                   e.printStackTrace()
                               }
                           }
                       }
                   }

       */


            tVTrial.setOnClickListener {
                if (status) {
                    status = false
                    tViewText.visibility = View.VISIBLE
                } else {
                    status = true
                    tViewText.visibility = View.GONE
                }
            }


            tVAddCard.setOnClickListener {
                startActivity(Intent(this, AddCardActivity::class.java))
            }


            var totalAmountCredit = ""
            var credit = ""
//            if (totalPrice.toDouble() >= profileData.wallet.toDouble()) {
//                credit = profileData.wallet
//                totalAmountCredit = (totalPrice.toDouble() - credit.toDouble()).toString()
//            } else {
//                credit = totalPrice
//                totalAmountCredit = "0"
//            }

            nextCheckout.setOnClickListener {
                var exist = 0
                var id = ""
                var brand = ""
                var last4 = ""
                for (i in 0 until categoriesList.size) {
                    if (categoriesList[i].card) {
                        id = categoriesList[i].id
                        brand = categoriesList[i].brand
                        last4 = categoriesList[i].last4
                        exist = 1
                        break
                    }
                }


                if (exist == 0) {
                    Toast.makeText(this, "Please select card.", Toast.LENGTH_SHORT).show()
                    return@setOnClickListener

                } else {
                    progressBar11.visibility = View.VISIBLE
                    mViewModel.getBuySubscription(
                        auth,
                        subTotalPrice.text.toString().replace("$", "").replace("-", ""),//wrong
                        totalPricee.text.toString().replace("$", "").replace("-", ""),//wrong
                        planData.subscription_id,
                        taxPricee,
                        modelCheckOutONe.firstName,
                        modelCheckOutONe.emailAddress,
                        modelCheckOutONe.streetName,
                        modelCheckOutONe.stateName,
                        modelCheckOutONe.city,
                        modelCheckOutONe.zipCode,
                        id,
                        modelCheckOutONe.billingType,
                        modelCheckOutONe.billingStreet,
                        modelCheckOutONe.billingState,
                        modelCheckOutONe.billingCity,
                        modelCheckOutONe.billingZip,
                        modelSubscriptionNewData.credits.replace("$", "").replace("-", ""),//wrong
                        freeTrial,
                        totalPricee.text.toString().replace("$", "").replace("-", ""),//wrong
                        typeDiscount.replace("$", "").replace("-", ""),
                        isIntro,
                        "+1" + modelCheckOutONe.phoneNumber.replace(" ", ""),
                        actualPrice.toString(),//wrong
                        "0",
                        discount_percentage.replace("$", "").replace("-", ""),
                        "2", brand, last4

                    )
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }


    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

//    private fun setAdapter() {
//        val homeProfileData = Constant.getArrayListProfile(application, Constant.dataProfile)
//        mViewModel.retrieveCards(homeProfileData.stripe_customer_id)
//        progressBar.visibility = View.VISIBLE
//
//    }


    override fun selectedCard(
        position: Int,
        modell: PaymentCardModel
    ) {
        val model = categoriesList[position]
        if (model.card) {
            model.card = false
        } else {
            for (child in categoriesList) {
                child.card = false
            }
            model.card = true
        }
        categoriesList[position] = model
        adapterCards.notifyDataSetChanged()

    }


    override fun editCard(position: Int, model: PaymentCardModel) {
        startActivity(
            Intent(this, AddCardActivity::class.java).putExtra(
                "updateCard",
                "update"
            ).putExtra("checkoutOne", Constant.checkoutoneData)

        )

    }

    override fun onResume() {
        super.onResume()
        val mPrefs = getSharedPreferences("ProductArray", MODE_PRIVATE)

        val totalAmounts = mPrefs.getString("addTotal", "")
        if (!totalAmounts.isEmpty()) {
            val editor = mPrefs.edit()
            editor.remove("addTotal")
            editor.apply()
        }

        categoriesList = Constant.cardArrayListData
        adapterCards.update(Constant.cardArrayListData)

    }

    fun getNewActivity(v: View) {
        val intent = Intent(this, ExtrasSubscriptionActivity::class.java)
        startActivity(intent)


    }

    class MenuFragmentCode(var progressBar: View) :
        RoundedBottomSheetDialogFragment() {

        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val view = inflater.inflate(R.layout.code_sheet, container, false)

            val invite = view.findViewById<TextView>(R.id.invite)
            val etQuantity = view.findViewById<TextView>(R.id.etCode)
            val imageViewTop = view.findViewById<ImageView>(R.id.imageViewTop)

            val isSeller = Constant.getSharedPrefs(activity!!).getBoolean(Constant.IS_SELLER, false)
            if (isSeller) {
                imageViewTop.setImageResource(R.mipmap.bg_popup)
            } else {
                imageViewTop.setImageResource(R.mipmap.bg_popup_customer)
            }
            roundImage(imageViewTop)
            invite.setOnClickListener {
                if (etQuantity.text.isEmpty()) {
                    Toast.makeText(activity, "Please enter code first!", Toast.LENGTH_LONG)
                        .show()
                } else {
                    progressBar.visibility = View.VISIBLE
                    val auth = Constant.getPrefs(activity!!).getString(Constant.auth_code, "")
                    mViewModel.setCode(auth, etQuantity.text.toString())
                    dismiss()
                }
            }

            etQuantity.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(
                    s: CharSequence,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                    if (getTextFieldsData()) {
                        invite.setBackgroundResource(R.drawable.favorite_delete_background)
                    } else {
                        invite.setBackgroundResource(R.drawable.favorite_delete_background_opacity)
                    }
                }

                override fun afterTextChanged(s: Editable) {
                }
            })

            return view
        }

        private fun getTextFieldsData(): Boolean {
            val quantity = etCode.text.toString().trim().length

            if (quantity < 8) {
                return false
            }

            return true

        }

        private fun roundImage(imageViewTop: ImageView) {
            val curveRadius = 50F

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                imageViewTop.outlineProvider = object : ViewOutlineProvider() {

                    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                    override fun getOutline(view: View?, outline: Outline?) {
                        outline?.setRoundRect(
                            0,
                            0,
                            view!!.width,
                            view.height + curveRadius.toInt(),
                            curveRadius

                        )
                    }
                }

                imageViewTop.clipToOutline = true

            }

        }

    }
}
