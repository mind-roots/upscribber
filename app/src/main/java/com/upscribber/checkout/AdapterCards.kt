package com.upscribber.checkout

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.payment.paymentCards.PaymentCardModel
import com.upscribber.upscribberSeller.subsriptionSeller.subscriptionMain.customerCheckout.ModelAddSignatureData
import kotlinx.android.synthetic.main.cards_layout_checkout.view.*



class AdapterCards(private val mContext: Context, var i: Int) :
    RecyclerView.Adapter<AdapterCards.MyViewHolder>() {

    var listener = mContext as cardSelected
    private var list: ArrayList<PaymentCardModel>  = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(mContext).inflate(R.layout.cards_layout_checkout, parent, false)
        return MyViewHolder(v)

    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = list[position]
        val imagePath = Constant.getPrefs(mContext).getString(Constant.dataImagePath, "")
//        Glide.with(mContext).load(imageCard).into(holder.itemView.imageCard)
        holder.itemView.CardNumber.text = "xxxx-xxxx-xxxx-" + model.last4
        when (model.brand) {
            "visa" -> {
                holder.itemView.imageCard.setImageResource(R.drawable.new_visa_icon)
            }
            "mastercard" -> {
                holder.itemView.imageCard.setImageResource(R.drawable.ic_mastercard_new)
            }
            else -> {
                holder.itemView.imageCard.setImageResource(R.drawable.ic_card_dummy)
            }
        }


        holder.itemView.setOnClickListener {
            if (list.size > 1) {
                listener.selectedCard(position,model)
            }else{
                listener.editCard(position,model)
            }
        }

        if (list.size <= 1) {
            holder.itemView.cl_cards.layoutParams.width = ConstraintLayout.LayoutParams.MATCH_PARENT
            holder.itemView.cl_cards.requestLayout()
            model.card = true
        }



        if (model.card) {
            holder.cl_cards.setBackgroundResource(R.drawable.bg_selected_card)
            val modelAddSignature = ModelAddSignatureData()
            modelAddSignature.lastFour = model.last4
        } else {
            holder.cl_cards.setBackgroundResource(R.drawable.bg_card)
        }

    }

    fun update(it: java.util.ArrayList<PaymentCardModel>?) {
//        if (i == 1){
            list = it!!
//        }else{
//            it!!.reverse()
//            val model:PaymentCardModel =it[0]
//            list.clear()
//            list.add(model)
//
//        }

        notifyDataSetChanged()

    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var cl_cards: ConstraintLayout = itemView.findViewById(R.id.cl_cards)
    }
}