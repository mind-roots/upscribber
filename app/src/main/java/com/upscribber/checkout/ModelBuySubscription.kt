package com.upscribber.checkout

import android.os.Parcel
import android.os.Parcelable

class ModelBuySubscription() : Parcelable {

    var business_name: String = ""
    var next_billing_date: String = ""
    var campaign_name: String = ""
    var campaign_logo: String = ""
    var business_logo: String = ""
    var tax: String = ""
    var frequency_type: String = ""
    var redeemtion_cycle_qty: String = ""
    var status: String = ""
    var msg: String = ""
    var amount: String = ""
    var created_at: String = ""

    constructor(parcel: Parcel) : this() {
        business_name = parcel.readString()
        next_billing_date = parcel.readString()
        campaign_name = parcel.readString()
        campaign_logo = parcel.readString()
        business_logo = parcel.readString()
        tax = parcel.readString()
        frequency_type = parcel.readString()
        redeemtion_cycle_qty = parcel.readString()
        status = parcel.readString()
        msg = parcel.readString()
        amount = parcel.readString()
        created_at = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(business_name)
        parcel.writeString(next_billing_date)
        parcel.writeString(campaign_name)
        parcel.writeString(campaign_logo)
        parcel.writeString(business_logo)
        parcel.writeString(tax)
        parcel.writeString(frequency_type)
        parcel.writeString(redeemtion_cycle_qty)
        parcel.writeString(status)
        parcel.writeString(msg)
        parcel.writeString(amount)
        parcel.writeString(created_at)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ModelBuySubscription> {
        override fun createFromParcel(parcel: Parcel): ModelBuySubscription {
            return ModelBuySubscription(parcel)
        }

        override fun newArray(size: Int): Array<ModelBuySubscription?> {
            return arrayOfNulls(size)
        }
    }
}
