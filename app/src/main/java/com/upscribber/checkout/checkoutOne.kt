package com.upscribber.checkout

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.location.Geocoder
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.MenuItem
import android.view.View
import android.widget.LinearLayout
import android.widget.Switch
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.upscribber.R
import com.upscribber.becomeseller.seller.ModelAddress
import com.upscribber.businessAddress.ManualBusinessAddress
import com.upscribber.commonClasses.Constant
import com.upscribber.payment.paymentCards.AddCardActivity
import com.upscribber.subsciptions.merchantSubscriptionPages.ModelCampaignInfo
import com.upscribber.subsciptions.merchantSubscriptionPages.ModelSubscriptionData
import com.upscribber.subsciptions.merchantSubscriptionPages.plan.PlanModel
import kotlinx.android.synthetic.main.activity_checkout_one.*

class checkoutOne : AppCompatActivity() {

    lateinit var nextCheckout: TextView
    lateinit var switch3: Switch
    lateinit var toolbar: Toolbar
    lateinit var toolbarTitle: TextView
    lateinit var title: TextView
    lateinit var linearLayout16: LinearLayout
    var billingType = "0"
    var latitude = 0.0
    var longitude = 0.0
    val model = ModelCheckoutOne()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_checkout_one)
        initz()
        setToolbar()
        clickEvents()
        editTextClicks()
    }

    private fun editTextClicks() {
        etphoneNumber.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(
                s: CharSequence,
                start: Int,
                count: Int,
                after: Int
            ) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (getTextFieldsData()) {
                    nextCheckout.setBackgroundResource(R.drawable.invite_button_background)
                } else {
                    nextCheckout.setBackgroundResource(R.drawable.invite_button_background_opacity)
                }
            }

            override fun afterTextChanged(s: Editable) {
            }
        })
        etFirstName.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(
                s: CharSequence,
                start: Int,
                count: Int,
                after: Int
            ) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (getTextFieldsData()) {
                    nextCheckout.setBackgroundResource(R.drawable.invite_button_background)
                } else {
                    nextCheckout.setBackgroundResource(R.drawable.invite_button_background_opacity)
                }
            }

            override fun afterTextChanged(s: Editable) {
            }
        })

        eTEmailAddress.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(
                s: CharSequence,
                start: Int,
                count: Int,
                after: Int
            ) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (getTextFieldsData()) {
                    nextCheckout.setBackgroundResource(R.drawable.invite_button_background)
                } else {
                    nextCheckout.setBackgroundResource(R.drawable.invite_button_background_opacity)
                }
            }

            override fun afterTextChanged(s: Editable) {
            }
        })

        eTPrimaryAddress.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(
                s: CharSequence,
                start: Int,
                count: Int,
                after: Int
            ) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (getTextFieldsData()) {
                    nextCheckout.setBackgroundResource(R.drawable.invite_button_background)
                } else {
                    nextCheckout.setBackgroundResource(R.drawable.invite_button_background_opacity)
                }
            }

            override fun afterTextChanged(s: Editable) {
            }
        })
        etBillingAddress.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(
                s: CharSequence,
                start: Int,
                count: Int,
                after: Int
            ) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (getTextFieldsData()) {
                    nextCheckout.setBackgroundResource(R.drawable.invite_button_background)
                } else {
                    nextCheckout.setBackgroundResource(R.drawable.invite_button_background_opacity)
                }
            }

            override fun afterTextChanged(s: Editable) {
            }
        })
    }

    private fun setToolbar() {
        setSupportActionBar(toolbar)
        title.text = ""
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)
        Places.initialize(applicationContext, resources.getString(R.string.google_maps_key))
        val homeProfileData = Constant.getArrayListProfile(application, Constant.dataProfile)
//        getRetrieveCards(homeProfileData.stripe_customer_id, application, 0)
    }

    private fun getTextFieldsData(): Boolean {
        val phoneNumber = etphoneNumber.text.toString().trim().length
        val name = etFirstName.text.toString().trim().length
        val email = eTEmailAddress.text.toString().trim().length
        val address = eTPrimaryAddress.text.toString().trim().length
        val billingAddress = etBillingAddress.text.toString().trim().length

        if (phoneNumber == 0) {
            return false
        }
        if (email == 0 || !Constant.isValidEmailId(eTEmailAddress.text.toString())) {
            return false
        }
        if (name == 0) {
            return false
        }
        if (address == 0) {
            return false
        }
        if (!switch3.isChecked) {
            if (billingAddress == 0) {
                return false
            }
        }

        return true

    }



    @SuppressLint("SetTextI18n")
    private fun initz() {
        toolbar = findViewById(R.id.toolbar)
        title = findViewById(R.id.title)
        nextCheckout = findViewById(R.id.goToSubscription)
        toolbarTitle = findViewById(R.id.title)
        linearLayout16 = findViewById(R.id.linearLayout16)
        switch3 = findViewById(R.id.switch3)
        etphoneNumber.isClickable = false
        etphoneNumber.isFocusableInTouchMode = false
        etphoneNumber.alpha = 0.7f

        etphoneNumber.setTextColor(resources.getColor(R.color.upscribber_tv))
        val modelData = Constant.getArrayListProfile(this, Constant.dataProfile)
        etFirstName.setText(modelData.name)
        if(modelData.name.isNotEmpty() &&modelData.contact_no.isNotEmpty() && modelData.primary_address.street.isNotEmpty() && modelData.email.isNotEmpty()){
            nextCheckout.setBackgroundResource(R.drawable.invite_button_background)
        }
        if (modelData.contact_no.isNotEmpty()) {
            etphoneNumber.setText(Constant.formatPhoneNumber(modelData.contact_no))
        }
        eTEmailAddress.setText(modelData.email)
        if (modelData.primary_address.street.isNotEmpty()) {
            eTPrimaryAddress.text =
                modelData.primary_address.street + " " + modelData.primary_address.state + " " + modelData.primary_address.city + " " + modelData.primary_address.zip_code
        }

        if (modelData.billing_address.billing_street.isNotEmpty()) {
            etBillingAddress.text =
                modelData.billing_address.billing_street + " " + modelData.billing_address.billing_state + " " + modelData.billing_address.billing_city + " " + modelData.billing_address.billing_zipcode
        }

        model.streetName = modelData.primary_address.street
        model.stateName = modelData.primary_address.state
        model.city = modelData.primary_address.city
        model.zipCode = modelData.primary_address.zip_code
        model.billingStreet = modelData.billing_address.billing_street
        model.billingState = modelData.billing_address.billing_state
        model.billingCity = modelData.billing_address.billing_city
        model.billingZip = modelData.billing_address.billing_zipcode

    }


    private fun clickEvents() {
        nextCheckout.setOnClickListener {
            if (switch3.isChecked) {
                billingType = "0"
                if (etFirstName.text.toString().isEmpty() || eTEmailAddress.text.toString().isEmpty() || etphoneNumber.text.toString().isEmpty() || eTPrimaryAddress.text.toString().isEmpty()) {
                    Toast.makeText(this, "All fields are mandatory", Toast.LENGTH_LONG).show()
                }else if (!Constant.isValidEmailId(eTEmailAddress.text.toString().trim())) {
                    Toast.makeText(this, "Please enter your valid email id", Toast.LENGTH_LONG).show()
                }else if (Constant.cardArrayListData.size > 0) {
                    model.firstName = etFirstName.text.toString()
                    model.emailAddress = eTEmailAddress.text.toString()
                    model.phoneNumber = etphoneNumber.text.toString().replace("(","").replace(")","").replace("-","").replace(" ","")
                    model.billingType = billingType
                    Constant.checkoutoneData = model

                    startActivity(
                        Intent(this, CheckOutTwo::class.java).putExtra(
                            "allData",
                            intent.getParcelableExtra<PlanModel>("allData")
                        )
                            .putExtra(
                                "modelSubscription",
                                intent.getParcelableExtra<ModelCampaignInfo>("modelSubscription")
                            )
                            .putExtra("modelCheckOut", Constant.checkoutoneData)
                            .putExtra("totalPrice", intent.getStringExtra("totalPrice")).putExtra("subscriptionData",intent.getParcelableExtra<ModelSubscriptionData>("subscriptionData"))
                    )

                } else {
                    model.firstName = etFirstName.text.toString()
                    model.emailAddress = eTEmailAddress.text.toString()
                    model.phoneNumber = etphoneNumber.text.toString().replace("(","").replace(")","").replace("-","").replace(" ","")
                    model.billingType = billingType
                    Constant.checkoutoneData = model

                    startActivity(
                        Intent(this, AddCardActivity::class.java).putExtra(
                            "checkout",
                            "others"
                        ).putExtra("allData", intent.getParcelableExtra<PlanModel>("allData"))
                            .putExtra(
                                "modelSubscription",
                                intent.getParcelableExtra<ModelCampaignInfo>("modelSubscription")
                            )
                            .putExtra("modelCheckOut", Constant.checkoutoneData)
                            .putExtra("totalPrice", intent.getStringExtra("totalPrice")).putExtra("subscriptionData",intent.getParcelableExtra<ModelSubscriptionData>("subscriptionData"))
                    )
                }
            } else {
                billingType = "1"
                if (etFirstName.text.toString().isEmpty() || eTEmailAddress.text.toString().isEmpty() || etphoneNumber.text.toString().isEmpty()
                    || eTPrimaryAddress.text.toString().isEmpty() || etBillingAddress.text.toString().isEmpty()
                ) {
                    Toast.makeText(this, "All fields are mandatory", Toast.LENGTH_LONG).show()
                } else if (!Constant.isValidEmailId(eTEmailAddress.text.toString().trim())) {
                    Toast.makeText(this, "Please enter your valid email id", Toast.LENGTH_LONG).show()
                }else if (Constant.cardArrayListData.size > 0) {
                    model.firstName = etFirstName.text.toString()
                    model.emailAddress = eTEmailAddress.text.toString()
                    model.phoneNumber = etphoneNumber.text.toString().replace("(","").replace(")","").replace("-","").replace(" ","")
                    model.billingType = billingType
                    Constant.checkoutoneData = model

                    startActivity(
                        Intent(this, CheckOutTwo::class.java).putExtra(
                            "allData",
                            intent.getParcelableExtra<PlanModel>("allData")
                        )
                            .putExtra(
                                "modelSubscription",
                                intent.getParcelableExtra<ModelCampaignInfo>("modelSubscription")
                            )
                            .putExtra("modelCheckOut", Constant.checkoutoneData)
                            .putExtra("totalPrice", intent.getStringExtra("totalPrice")).putExtra("subscriptionData",intent.getParcelableExtra<ModelSubscriptionData>("subscriptionData"))
                    )
                } else {
                    model.firstName = etFirstName.text.toString()
                    model.phoneNumber = etphoneNumber.text.toString().replace("(","").replace(")","").replace("-","").replace(" ","")
                    model.emailAddress = eTEmailAddress.text.toString()
                    model.billingType = billingType
                    Constant.checkoutoneData = model

                    startActivity(
                        Intent(this, AddCardActivity::class.java).putExtra(
                            "checkout",
                            "others"
                        ).putExtra("allData", intent.getParcelableExtra<PlanModel>("allData"))
                            .putExtra(
                                "modelSubscription",
                                intent.getParcelableExtra<ModelCampaignInfo>("modelSubscription")
                            )
                            .putExtra("modelCheckOut", Constant.checkoutoneData)
                            .putExtra("totalPrice", intent.getStringExtra("totalPrice")).putExtra("subscriptionData",intent.getParcelableExtra<ModelSubscriptionData>("subscriptionData"))
                    )
                }
            }


        }

        switch3.setOnCheckedChangeListener { p0, p1 ->
            if (p1) {
//                linearLayout16.visibility = View.GONE
                textView.visibility = View.GONE
                updateBilling.visibility = View.GONE
                etBillingAddress.visibility = View.GONE

            } else {
                textView.visibility = View.VISIBLE
                updateBilling.visibility = View.GONE
                etBillingAddress.visibility = View.VISIBLE
//                linearLayout16.visibility = View.VISIBLE
            }

        }

        eTPrimaryAddress.setOnClickListener {
            billingType = "0"
            Constant.hideKeyboard(this, etBillingAddress)
            Places.createClient(this)
            val fields =
                listOf(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG)
            val intent = Autocomplete.IntentBuilder(
                AutocompleteActivityMode.FULLSCREEN, fields
            )
                .build(this)
            startActivityForResult(intent, 10)
        }

        etBillingAddress.setOnClickListener {
            billingType = "1"
            Constant.hideKeyboard(this, etBillingAddress)
            Places.createClient(this)
            val fields =
                listOf(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG)
            val intent = Autocomplete.IntentBuilder(
                AutocompleteActivityMode.FULLSCREEN, fields
            )
                .build(this)
            startActivityForResult(intent, 11)
        }


    }

    @SuppressLint("SetTextI18n")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 10) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    try {
                        val place = Autocomplete.getPlaceFromIntent(data!!)
                        val latLng = place.latLng
                        latitude = latLng!!.latitude
                        longitude = latLng.longitude
                        val geocoder = Geocoder(this)
                        val addresses =
                            latLng.latitude.let {
                                geocoder.getFromLocation(
                                    it,
                                    latLng.longitude,
                                    1
                                )
                            }
                        val locationModel = ModelAddress()
                        locationModel.streetAddress = addresses[0].thoroughfare
                        locationModel.city = addresses[0].locality
                        locationModel.state = addresses[0].adminArea
                        locationModel.latitude = latitude.toString()
                        locationModel.longitude = longitude.toString()
                        locationModel.zipCode = addresses[0].postalCode
                        startActivityForResult(
                            Intent(
                                this,
                                ManualBusinessAddress::class.java
                            ).putExtra("checkout", locationModel), 10
                        )

                    } catch (e: Exception) {
                        Toast.makeText(this, "Could not find the location!", Toast.LENGTH_SHORT)
                            .show()
                        e.printStackTrace()
                    }
                }
            }
        }


        if (requestCode == 11) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    try {
                        val place = Autocomplete.getPlaceFromIntent(data!!)
                        val latLng = place.latLng
                        latitude = latLng!!.latitude
                        longitude = latLng.longitude
                        val geocoder = Geocoder(this)
                        val addresses =
                            latLng.latitude.let {
                                geocoder.getFromLocation(
                                    it,
                                    latLng.longitude,
                                    1
                                )
                            }
                        val locationModel = ModelAddress()
                        locationModel.billingZip = addresses[0].postalCode
                        locationModel.billingCity = addresses[0].locality
                        locationModel.billingState = addresses[0].adminArea
                        locationModel.billingStreet = addresses[0].thoroughfare

                        startActivityForResult(
                            Intent(
                                this,
                                ManualBusinessAddress::class.java
                            ).putExtra("checkoutBilling", locationModel), 10
                        )

                    } catch (e: Exception) {
                        Toast.makeText(this, "Could not find the location!", Toast.LENGTH_SHORT)
                            .show()
                        e.printStackTrace()
                    }
                }
            }
        }


        if (resultCode == 1 && data != null) {
            val modelGetProfile = data.getParcelableExtra<ModelAddress>("address")
            if (billingType == "0") {
                eTPrimaryAddress.text =
                    modelGetProfile.streetAddress + "," + modelGetProfile.city + "," +
                            modelGetProfile.state + "," + modelGetProfile.zipCode

                model.streetName = modelGetProfile.streetAddress
                model.stateName = modelGetProfile.state
                model.city = modelGetProfile.city
                model.zipCode = modelGetProfile.zipCode
                model.suite = modelGetProfile.suiteNumber

            } else {
                etBillingAddress.text =
                    modelGetProfile.billingStreet + "," + modelGetProfile.billingCity + "," +
                            modelGetProfile.billingState + "," + modelGetProfile.billingZip

                model.billingStreet = modelGetProfile.billingStreet
                model.billingState = modelGetProfile.billingState
                model.billingCity = modelGetProfile.billingCity
                model.billingZip = modelGetProfile.billingZip
                model.billingSuite = modelGetProfile.billingSuite

            }
        }

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}
