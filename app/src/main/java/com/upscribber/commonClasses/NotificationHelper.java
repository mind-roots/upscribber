package com.upscribber.commonClasses;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.provider.Settings;

import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.RemoteMessage;
import com.upscribber.R;
import com.upscribber.dashboard.DashboardActivity;

import java.util.Map;

public class NotificationHelper {

    private Context mContext;
    private NotificationManager mNotificationManager;
    private NotificationCompat.Builder mBuilder;
    private static final String NOTIFICATION_CHANNEL_ID = "10001";

    NotificationHelper(Context context) {
        mContext = context;
    }


    void createNotification(RemoteMessage messageBody) {
/**Creates an explicit intent for an Activity in your app**/
        try {
            String title = messageBody.getNotification().getBody();
//            String type = messageBody.get("type");
            Intent resultIntent = null;


//            String type1 = type;

//                JSONObject body = new JSONObject(messageBody.get("data"));
//            String body = messageBody.get("body");
//                title = body.optString("msg");
//                title = messageBody.get("message");
//                resultIntent = new Intent(mContext, NotificationActivity.class);
//                resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                resultIntent.putExtra("push", body);
//                resultIntent.putExtra("postType", body.optString("post_type"));


            String body = messageBody.getNotification().getBody();
            title = messageBody.getNotification().getBody();
            resultIntent = new Intent(mContext, DashboardActivity.class);
            resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            resultIntent.putExtra("user", body);


//            else {
//                String body = messageBody.get("body");
//                title = messageBody.get("message");
//                resultIntent = new Intent(mContext, NotificationCustomerOnlyActivity.class);
//                resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                resultIntent.putExtra("user", body);
//            }

//            } else {
//                String body = messageBody.get("body");
//                title = messageBody.get("message");
//                resultIntent = new Intent(mContext, NotificationCustomerOnlyActivity.class);
//                resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                resultIntent.putExtra("user", body);
//            }


            resultIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            PendingIntent resultPendingIntent = PendingIntent.getActivity(mContext,
                    0, resultIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT);


            mBuilder = new NotificationCompat.Builder(mContext);
            mBuilder.setSmallIcon(R.mipmap.app_icon);
            mBuilder.setContentText(title)
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(title))
                    .setAutoCancel(true)
                    .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                    .setContentIntent(resultPendingIntent);

            mNotificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                int importance = NotificationManager.IMPORTANCE_HIGH;
                NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "Upscribbr", importance);
                notificationChannel.enableLights(true);
                notificationChannel.setLightColor(Color.RED);
                notificationChannel.enableVibration(true);
                notificationChannel.getDescription();
                notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
                assert mNotificationManager != null;
                mBuilder.setChannelId(NOTIFICATION_CHANNEL_ID);
                mNotificationManager.createNotificationChannel(notificationChannel);
            }
            assert mNotificationManager != null;
            mNotificationManager.notify(0, mBuilder.build());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}