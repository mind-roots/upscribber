package com.upscribber.commonClasses

import android.content.ContentValues.TAG
import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage


class MyService : FirebaseMessagingService() {


    override fun onMessageReceived(msg: RemoteMessage) {
        super.onMessageReceived(msg)
        Log.d(TAG, "From: ${msg.from}")
        Log.e("Message: ", msg.data.toString())
        val notificationHelper = NotificationHelper(this)
        notificationHelper.createNotification(msg)
//        if (MainActivity.isAppRunning) {

//            val intent = Intent(this, MyApplication::class.java)
//            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
//            intent.putExtra("reLoad", "reLoad")
//            startActivity(intent)
//
//        } else {
//            val notificationHelper = NotificationHelper(this)
//            notificationHelper.createNotification(msg)
//        }


    }

    private fun handleNow() {


    }

    private fun scheduleJob() {

    }

    override fun onNewToken(p0: String) {
        super.onNewToken(p0)
        Log.d(TAG, "Refreshed token: $p0")
        sendRegistrationToServer(p0)

    }


    fun sendRegistrationToServer(token: String?) {
        val editor = Constant.getPrefs(this).edit()
        editor.putString("deviceId", token)
        Log.d(TAG, "Refreshed token: " + token)
        editor.apply()
    }
}
