package com.upscribber.commonClasses

import android.widget.EditText
import android.widget.TextView
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import org.json.JSONArray
import retrofit2.Call
import retrofit2.http.*

interface WebServicesMerchants {

    @FormUrlEncoded
    @POST("saveCampaign")
    fun saveSubscription(
        @Field("campaign_id") campaign_id: String,
        @Field("auth_code") auth_code: String,
        @Field("image") image: String,
        @Field("name") name: String,
        @Field("products") product: String,
        @Field("campaign_start") campaign_start: String,
        @Field("campaign_end") campaign_end: String,
        @Field("campaign_type") campaign_type: String,
        @Field("subscription_data") subscription_data: JSONArray,
        @Field("description") subscription_info: String,
        @Field("team_member") team_member: String,
        @Field("tags") tags: String,
        @Field("share_setting") sharable: String,
        @Field("is_public") privacy: String,
        @Field("location_id") location: String,
        @Field("max_subscriber") max_subscriber: String,
        @Field("status") status: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("getPhotos")
    fun getPhotos(
        @Field("auth_code") auth_code: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("deleteImage")
    fun deleteImage(
        @Field("auth_code") auth_code: String,
        @Field("image") image: String
    ): Call<ResponseBody>


    @Multipart
    @POST("addPhoto")
    fun addPhoto(
        @Part file: MultipartBody.Part,
        @Part("auth_code") auth_code: RequestBody,
        @Part("type") type: RequestBody
    ): Call<ResponseBody>


    @Multipart
    @POST("saveCampaign")
    fun saveBasic(
        @Part file: MultipartBody.Part,
        @Part("auth_code") auth_code: RequestBody,
        @Part("campaign_id") campaign_id: RequestBody,
        @Part("name") name: RequestBody,
        @Part("product") product: RequestBody,
        @Part("campaign_start") campaign_start: RequestBody,
        @Part("campaign_end") campaign_end: RequestBody,
        @Part("campaign_type") campaign_type: RequestBody
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("saveCampaign")
    fun details(
        @Field("auth_code") auth_code: String,
        @Field("campaign_id") campaign_id: String,
        @Field("description") subscription_info: String,
        @Field("max_subscriber") max_subscriber: String,
        @Field("team_member") team_member: String,
        @Field("tags") tags: String,
        @Field("share_setting") sharable: String,
        @Field("is_public") privacy: String,
        @Field("location") location: String,
        @Field("status") status: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("getCampaignInfo")
    fun getCampaignInfo(
        @Field("auth_code") auth_code: String,
        @Field("campaign_id") campaign_id: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("cloneCampaign")
    fun cloneCampaign(
        @Field("auth_code") auth_code: String,
        @Field("campaign_id") campaign_id: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("saveCampaign")
    fun createSub(
        @Field("auth_code") auth_code: String,
        @Field("campaign_id") campaign_id: String,
        @Field("status") status: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("changeCampaignStatus")
    fun changeCampaignStatus(
        @Field("auth_code") auth_code: String,
        @Field("campaign_id") campaign_id: String,
        @Field("type") type: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("getCampaignSubscriptions")
    fun getCampaignSubscriptions(
        @Field("auth_code") auth_code: String,
        @Field("campaign_id") campaign_id: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("deleteSubscription")
    fun deleteSubscription(
        @Field("auth_code") auth_code: String,
        @Field("type") type: String,                //1- campaign 2 - subscription
        @Field("campaign_id") campaign_id: String,
        @Field("subscription_id") subscription_id: String
    ): Call<ResponseBody>


    @Multipart
    @POST("merchant_signup")
    fun merchantSignUpTeamMember(
        @Part("auth_code") auth_code: RequestBody,
        @Part file: MultipartBody.Part,
        @Part("business_id") business_id: RequestBody,
        @Part("name") name: RequestBody,
        @Part("email") email: RequestBody,
        @Part("speciality") speciality: RequestBody,
        @Part("type") type: RequestBody,
        @Part("step") step: RequestBody,
        @Part("notification_id") notification_id: RequestBody
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST(" merchant_signup")
    fun merchantSignupThird(
        @Field("auth_code") auth_code: String,
        @Field("email") email: String,
        @Field("contact_no") contact_no: String,
        @Field("home_address") home_address: String,
        @Field("legal_name") legal_name: String,
        @Field("step") step: String,
        @Field("home_city") city: String,
        @Field("home_state") state: String,
        @Field("home_zipcode") zip_code: String,
        @Field("home_street") street: String,
        @Field("billing_street") billing_street: String,
        @Field("billing_state") billing_state: String,
        @Field("billing_city") billing_city: String,
        @Field("billing_zipcode") billing_zipcode: String,
        @Field("billing_type") billing_type: String,
        @Field("billing_address") billing_address: String,
        @Field("type") type: String,
        @Field("business_id") business_id: String,
        @Field("notification_id") notification_id: String,
        @Field("first_name") first_name: String,
        @Field("last_name") last_name: String,
        @Field("lat") lat: String,
        @Field("long") long: String,
        @Field("suite") suite: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST(" merchant_signup")
    fun merchantSignupTax(
        @Field("auth_code") auth_code: String,
        @Field("ssn") ssn: String,
        @Field("birthdate") birthdate: String,
        @Field("type") type: String,
        @Field("step") step: String,
        @Field("business_id") business_id: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST(" merchant_signup")
    fun merchantSignupStripe(
        @Field("auth_code") auth_code: String,
        @Field("stripe_token") stripe_token: String,
        @Field("pay_type") pay_type: String,
        @Field("step") step: String,
        @Field("type") type: String,
        @Field("business_id") business_id: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST(" merchant_signup")
    fun merchantSignuBank(
        @Field("auth_code") auth_code: String,
        @Field("account_number") account_number: String,
        @Field("routing_number") routing_number: String,
        @Field("bank_owner_name") bank_owner_name: String,
        @Field("step") step: String,
        @Field("pay_type") pay_type: String,
        @Field("type") type: String,
        @Field("business_id") business_id: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("merchant_signup")
    fun merchantSignuTerms(
        @Field("auth_code") auth_code: String,
        @Field("step") step: String,
        @Field("type") type: String,
        @Field("business_id") business_id: String,
        @Field("notification_id") notification_id: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("productDetails")
    fun productDetails(
        @Field("auth_code") auth_code: String,
        @Field("id") id: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("rejectAssociationRequest")
    fun rejectAssociationRequest(
        @Field("auth_code") auth_code: String,
        @Field("business_id") business_id: String,
        @Field("notification_id") notification_id: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("relinkStaff")
    fun relinkStaff(
        @Field("auth_code") auth_code: String,
        @Field("staff_id") staff_id: String,
        @Field("business_id") business_id: String,
        @Field("notification_id") notification_id: String
    ): Call<ResponseBody>

    @Multipart
    @POST("uploadDocumentStripe")
    fun uploadDocumentStripe(
        @Part("auth_code") auth_code: RequestBody,
        @Part front: MultipartBody.Part,
        @Part back: MultipartBody.Part
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("getTransactions")
    fun getTransactions(
        @Field("auth_code") auth_code: String,
        @Field("type") type: String,
        @Field("customer_id") customer_id: String,
        @Field("tab") tab: String,
        @Field("trans_page") trans_page: String,
        @Field("pay_page") pay_page: String

    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("refundRequest")
    fun refundRequest(
        @Field("auth_code") auth_code: String,
        @Field("order_id") order_id: String,
        @Field("merchant_id") merchant_id: String,
        @Field("transaction_id") transaction_id: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("genrateRefund")
    fun genrateRefund(
        @Field("auth_code") auth_code: String,
        @Field("transaction_id") transaction_id: String,
        @Field("merchant_amount") merchant_amount: String,
        @Field("partial_amount") partial_amount: String,
        @Field("admin_amount") admin_amount: String,
        @Field("type") type : String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("getCampaignInvitedCustomer")
    fun getCampaignInvitedCustomer(
        @Field("auth_code") auth_code: String,
        @Field("campaign_id") campaign_id: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("updateCampDisStatus")
    fun updateCampDisStatus(
        @Field("auth_code") auth_code: String,
        @Field("campaign_id") campaign_id: String,
        @Field("status") status: String,
        @Field("type") type: String,
        @Field("limit") limit: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("getSubscription")
    fun getSubscription(
        @Field("auth_code") auth_code: String,
        @Field("view_type") view_type: String,
        @Field("page") page: String,
        @Field("view_id") view_id: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("unredeem")
    fun getUnredeem(
        @Field("auth_code") auth_code: String,
        @Field("redeem_id") redeem_id: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("deniedRefund")
    fun getDeniedRefund(
        @Field("auth_code") auth_code: String,
        @Field("order_id") order_id: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("updateMerchantPaymentMethod")
    fun updateMerchantPaymentMethod(
        @Field("auth_code") auth_code: String,
        @Field("payment_id") payment_id: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("changePayoutMethod")
    fun changePayoutMethod(
        @Field("auth_code") auth_code: String,
        @Field("payout") payout : String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("updateExternalAccount")
    fun updateExternalAccount(
        @Field("auth_code") auth_code: String,
        @Field("default") default : String,
        @Field("type") type : String,
        @Field("bank_owner_name") bank_owner_name : String,
        @Field("routing_number") routing_number : String,
        @Field("account_number") account_number : String
    ): Call<ResponseBody>

 @FormUrlEncoded
    @POST("updateExternalAccount")
    fun updateExternalCardAccount(
        @Field("auth_code") auth_code: String,
        @Field("default") default : String,
        @Field("type") type : String,
        @Field("stripe_token") stripe_token : String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("getCampaignSubscriber")
    fun getCampaignSubscriber(
        @Field("auth_code") auth_code: String,
        @Field("page") page : String,
        @Field("campaign_id") campaign_id : String
    ): Call<ResponseBody>



}