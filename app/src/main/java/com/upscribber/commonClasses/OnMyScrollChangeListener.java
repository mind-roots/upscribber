package com.upscribber.commonClasses;

public interface OnMyScrollChangeListener {
        void onScrollUp();
        void onScrollDown();
    }