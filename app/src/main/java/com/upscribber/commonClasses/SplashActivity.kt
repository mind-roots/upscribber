package com.upscribber.commonClasses

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.upscribber.R
import com.upscribber.loginSignUpScreen.LoginorSignUpActivity
import com.upscribber.dashboard.DashboardActivity


class SplashActivity : AppCompatActivity() {

    private var mDelayHandler: Handler? = null
    private val mSplashDelay: Long = 4000

    private val mRunnable: Runnable = Runnable {
        if (!isFinishing) {

            if (Constant.getLoggedStatus(this)) {
                val intent = Intent(this, DashboardActivity::class.java)
                startActivity(intent)
                finish()
            } else {
                val intent = Intent(applicationContext, LoginorSignUpActivity::class.java)
                startActivity(intent)
            }

            finish()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        Constant.getPrefs(this).edit().remove(Constant.dataFeatues).remove(Constant.dataLatestSubs)
            .remove(Constant.categories).remove(Constant.user_id).apply()
        mDelayHandler = Handler()
        mDelayHandler!!.postDelayed(mRunnable, mSplashDelay)

    }

    public override fun onDestroy() {

        if (mDelayHandler != null) {
            mDelayHandler!!.removeCallbacks(mRunnable)
        }

        super.onDestroy()
    }
}