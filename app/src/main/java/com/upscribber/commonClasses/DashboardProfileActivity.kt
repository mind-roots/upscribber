package com.upscribber.commonClasses

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import com.upscribber.R
import kotlinx.android.synthetic.*
import android.content.Intent
import com.upscribber.profile.EditProfileActivity


class DashboardProfileActivity : AppCompatActivity() {

    lateinit var dashProfile : Fragment
    lateinit var toolbar7: Toolbar
    lateinit var simialId: TextView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard_profile)
        toolbar7 = findViewById(R.id.toolbar7)
        simialId = findViewById(R.id.simialId)
        setSupportActionBar(toolbar7)
        simialId.text = "Profile"

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)

        init()
    }

    private fun init(){
        dashProfile.clearFindViewByIdCache()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.nav_edit_profile, menu)
        return super.onCreateOptionsMenu(menu)

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        if (item.itemId == R.id.edit){
            startActivity(Intent(this,EditProfileActivity::class.java))
        }
        return super.onOptionsItemSelected(item)
    }

}
