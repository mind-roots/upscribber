package com.upscribber.commonClasses;

import android.content.Context;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.view.GestureDetectorCompat;
import androidx.core.widget.NestedScrollView;

/**
 * Created by Manisha Thakur on 7/3/19.
 */
public class ExtendedScrollView extends NestedScrollView implements GestureDetector.OnGestureListener {
    private final Context con;
    private float mTouchPosition;
    private float mReleasePosition;

    OnMyScrollChangeListener listener;
    private GestureDetectorCompat myGestureDetector;

    public ExtendedScrollView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        con = context;
        listener = (OnMyScrollChangeListener) context;
        myGestureDetector = new GestureDetectorCompat(context, this);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            mTouchPosition = event.getRawY();
        }

        if (event.getAction() == MotionEvent.ACTION_UP) {
            mReleasePosition = event.getRawY();

            if (mTouchPosition - mReleasePosition > 0) {
                // user scroll down
                listener.onScrollDown();

            } else {
                //user scroll up
                listener.onScrollUp();
            }
        }
        return super.onTouchEvent(event);
    }

    @Override
    public boolean onDown(MotionEvent e) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent e) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        Toast.makeText(con, "Dis: "+distanceY, Toast.LENGTH_SHORT).show();
        return false;
    }

    @Override
    public void onLongPress(MotionEvent e) {

    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        return false;
    }

}
