package com.upscribber.commonClasses

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.media.ExifInterface
import android.net.Uri
import android.os.Environment
import android.provider.MediaStore
import android.telephony.PhoneNumberUtils
import android.util.DisplayMetrics
import android.util.Log
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat.startActivity
import androidx.core.graphics.BitmapCompat
import com.appsflyer.CreateOneLinkHttpTask
import com.appsflyer.share.ShareInviteHelper
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.TransformationUtils
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.upscribber.R
import com.upscribber.checkout.ModelCheckoutOne
import com.upscribber.home.HomeModelMain
import com.upscribber.home.WhatsNewModelOne
import com.upscribber.loginSignUpScreen.LoginorSignUpActivity
import com.upscribber.payment.paymentCards.PaymentCardModel
import com.upscribber.profile.ModelGetProfile
import com.upscribber.surveyFeedback.AnswerArray
import com.upscribber.upscribberSeller.navigationCustomer.InviteSubscription
import kotlinx.android.synthetic.main.address_alert.*
import kotlinx.android.synthetic.main.alert_resend_code.*
import kotlinx.android.synthetic.main.common_alert_staff.*
import kotlinx.android.synthetic.main.social_alert.*
import kotlinx.android.synthetic.main.social_alert.okBtn
import org.json.JSONArray
import java.io.File
import java.io.FileOutputStream
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern
import kotlin.collections.ArrayList


@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class Constant {

    companion object {
        val new_notification_status: String = "new_notification_status"
        val push_setting: String = "push_setting"
        val new_user: String = "new_user"
        val default_loc: String = "default_loc"
        val stripe_new_customer_from_merchant: String = "stripe_new_customer_from_merchant"
        val regtrationType: String = "regtrationType"
        val gifTutorial2: String = "gifTutorial2"
        val gifTutorial1: String = "gifTutorial1"
        val countryCode: String = "countryCode"
        val dataSubCategory: String = "dataSubCategory"
        var feedback: String = ""
        var status: String = ""
        var msg: String = ""
        val last_associated_id: String = "last_associated_id"
        val tags: String = "tagsKey"
        val categories: String = "categoriesKey"
        val feedArray: String = "feedArray"
        val cardData: String = "cardData"
        var list: String = ""
        val wallet: String = "walletKey"
        var mDataHome = HomeModelMain()
        val isMerchant: String = ""
        val cardToken: String = ""
        val locationData: String = "locationData"
        val dataImagePath: String = "dataImagePath"
        val dataStatus: String = "dataStatus"
        val dataSimilarSubscription: String = "dataSimilarSubscription"
        val dataFeatues: String = "dataFeatues"
        val dataLatestSubs: String = "dataLatestSubs"
        val dataProfile: String = "dataProfile"
        var prefs: SharedPreferences? = null
        val IS_SELLER: String = "isSeller"

        //                const val BASE_URL = "http://cloudart.com.au/projects/upscribbr/webservices/data_v1/"
        const val BASE_URL = "https://app.upscribbr.com/webservices/data_v1/"
        var stripeTokenUrl = "https://api.stripe.com/v1/"
        var stripeBaseUrl = "https://api.stripe.com/v1/customers/"
        var paymentCards = ArrayList<PaymentCardModel>()
        var checkoutoneData = ModelCheckoutOne()
        var checkoutMerchantData = ModelCheckoutOne()
        var cardArrayListData: ArrayList<PaymentCardModel> = ArrayList()
        var cardArrayListDataMerchant: ArrayList<PaymentCardModel> = ArrayList()


        fun getSharedPrefs(context: Context): SharedPreferences {
            return if (prefs == null) {
                context.getSharedPreferences("data", 0)
            } else {
                prefs!!
            }
        }

        val auth_code: String = "token"
        val user_id: String = "user_id"
        val stripe_customer_name: String = "stripe_customer_name"
        val type: String = "type"

        fun getPrefs(context: Context): SharedPreferences {
            return context.getSharedPreferences("Register", Context.MODE_PRIVATE)
        }

        fun getArrayListProfile(context: Context, key: String): ModelGetProfile {
            val prefs = getPrefs(context)
            val gson = Gson()
            val json = prefs.getString(key, "{}")
            return gson.fromJson(
                json,
                object : TypeToken<ModelGetProfile>() {

                }.type
            )
        }

        fun getPrefs1(context: Context): SharedPreferences {
            return context.getSharedPreferences("getFeedbackArray", Context.MODE_PRIVATE)
        }


        fun getFeedbackArrayList(context: Context, key: String): ArrayList<AnswerArray> {
            val prefs = getPrefs1(context)

            val gson = Gson()
            val json = prefs.getString(key, "[]")
            val jsonType = JSONArray(json)
            if (jsonType.length() == 0) {
                return ArrayList()
            } else {
                return gson.fromJson(
                    json,
                    object : TypeToken<ArrayList<AnswerArray>>() {

                    }.type
                )
            }

        }


        fun getCategories(context: Context, key: String): List<WhatsNewModelOne>? {
            val prefs = getPrefs(context)
            val gson = Gson()
            val json = prefs.getString(key, null)
            var jsonType = JSONArray(json)
            val type = gson.fromJson<List<WhatsNewModelOne>>(
                json,
                object : TypeToken<List<WhatsNewModelOne>>() {

                }.type
            )

            return type
        }

        fun getCardData(context: Context, key: String): List<PaymentCardModel>? {
            val prefs = getPrefs(context)
            val gson = Gson()
            val json = prefs.getString(key, null)
            var jsonType = JSONArray(json)
            val type = gson.fromJson<List<PaymentCardModel>>(
                json,
                object : TypeToken<List<PaymentCardModel>>() {

                }.type
            )

            return type
        }

        fun getCampaignId(context: Context): String? {
            val prefs = context.getSharedPreferences("Register", Context.MODE_PRIVATE)
            return prefs.getString("CampaignID", "")
        }


//        fun getArrayListSubscription(context: Context, key: String): List<ModelHomeDataSubscriptions>? {
//            val prefs = getPrefs(context)
//            val gson = Gson()
//            val json = prefs.getString(key, null)
//            var jsonType = JSONArray(json)
//            val type = gson.fromJson<List<ModelHomeDataSubscriptions>>(
//                json,
//                object : TypeToken<List<ModelHomeDataSubscriptions>>() {
//
//                }.type
//            )
//
//            return type
//        }


//        fun getArrayListPause(context: Context, key: String): ModelPause {
//            val prefs = getPrefs(context)
//            val gson = Gson()
//            val json = prefs.getString(key, null)
//            val type = gson.fromJson<ModelPause>(
//                json,
//                object : TypeToken<ModelPause>() {
//
//                }.type
//            )
//            return type
//        }

//        fun getDataInArrayList(context: Context): ArrayList<PaymentCardModel> {
//            val prefs = getPrefs(context)
//            val gson = Gson()
//            val json = prefs.getString("paymentCards", "")
//            if (json.isEmpty()) {
//                return ArrayList()
//            }
//            val type = object : TypeToken<ArrayList<PaymentCardModel>>() {
//
//            }.type
//            return gson.fromJson<ArrayList<PaymentCardModel>>(json, type)
//        }


        fun hideKeyboard(context: Context, view: View) {
            val imm = context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }


        fun showKeyBoard(context: Context) {
            val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.toggleSoftInput(
                InputMethodManager.SHOW_FORCED,
                InputMethodManager.HIDE_IMPLICIT_ONLY
            )
        }


        fun CommonIAlert(context: Context, title: String, msg: String) {
            val mDialogView = LayoutInflater.from(context).inflate(R.layout.common_alert, null)
            val mBuilder = AlertDialog.Builder(context)
                .setView(mDialogView)
            val mAlertDialog = mBuilder.show()
            val heading: TextView = mDialogView.findViewById(R.id.textHeader)
            val image: ImageView = mDialogView.findViewById(R.id.image)
            val description: TextView = mDialogView.findViewById(R.id.textMessage)
            val doneButton: TextView = mDialogView.findViewById(R.id.textDone)
            heading.text = title
            if (msg.isEmpty()) {
                description.visibility = View.GONE
            } else {
                description.text = msg
            }


            val isSeller = getSharedPrefs(context).getBoolean(IS_SELLER, false)
            if (isSeller) {
                doneButton.setBackgroundResource(R.drawable.bg_seller_button_blue)
                image.setImageResource(R.drawable.ic_info_merchant)
            } else {
                doneButton.setBackgroundResource(R.drawable.invite_button_background)
                image.setImageResource(R.drawable.ic_info_customer)
            }


            doneButton.setOnClickListener {
                mAlertDialog.dismiss()
            }
            mAlertDialog.setCancelable(false)
            mAlertDialog.window.setBackgroundDrawableResource(R.drawable.bg_alert_new)
            mAlertDialog.textDone.setOnClickListener {
                mAlertDialog.dismiss()
            }
        }


        fun commonAlert(context: Context) {
            val mDialogView = LayoutInflater.from(context).inflate(R.layout.address_alert, null)
            val mBuilder = AlertDialog.Builder(context).setView(mDialogView)
            val mAlertDialog = mBuilder.show()
            mAlertDialog.setCancelable(false)
            mAlertDialog.window.setBackgroundDrawableResource(R.drawable.bg_alert)
            mAlertDialog.address.visibility = View.GONE
            mAlertDialog.addresss.text = "Invalid auth code!"
            mAlertDialog.okBtn.setOnClickListener {
                val editor = getPrefs(context).edit()
                val editor1 = getSharedPrefs(context).edit()
                editor.clear()
                editor1.clear()
                editor.apply()
                editor1.apply()
                val intent = Intent(context, LoginorSignUpActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                context.startActivity(intent)
                mAlertDialog.dismiss()
            }

        }

        fun commonErrorAlert(context: Context, header: String) {
            val mDialogView = LayoutInflater.from(context).inflate(R.layout.address_alert, null)
            val mBuilder = AlertDialog.Builder(context).setView(mDialogView)
            val mAlertDialog = mBuilder.show()
            mAlertDialog.setCancelable(false)
            mAlertDialog.window.setBackgroundDrawableResource(R.drawable.bg_alert)
            mAlertDialog.address.visibility = View.GONE
            mAlertDialog.addresss.text = header
            mAlertDialog.okBtn.setOnClickListener {
                mAlertDialog.dismiss()
            }
        }


        fun isValidEmailId(email: String): Boolean {
            val pattern: Pattern
            val matcher: Matcher
            val EMAIL_PATTERN = Patterns.EMAIL_ADDRESS
            pattern = Pattern.compile(EMAIL_PATTERN.toString())
            matcher = pattern.matcher(email)
            return matcher.matches()
        }
//        "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"


        fun isValidWebsite(url: String): Boolean {
            var p = Patterns.WEB_URL
            var m = p.matcher(url.toLowerCase())
            return m.matches()

//            return PatternsCompat.WEB_URL.matcher(url.toLowerCase(Locale.US)).matches()

//            val pattern : Pattern
//            val matcher : Matcher
//            val WebUrl = "^((ftp|http|https):\\/\\/)?(www.)?(?!.*(ftp|http|https|www.))[a-zA-Z0-9_-]+(\\.[a-zA-Z]+)+((\\/)[\\w#]+)*(\\/\\w+\\?[a-zA-Z0-9_]+=\\w+(&[a-zA-Z0-9_]+=\\w+)*)?$"
//            pattern = Pattern.compile(WebUrl)
//            matcher = pattern.matcher(url)
//            return matcher.matches()
        }


        fun getCalculatedPrice(discountPrice: String, price: String): String {
            return (price.toDouble() - (discountPrice.toDouble() / 100 * price.toDouble())).toString()
//            var bd = BigDecimal((price.toDouble() - (discountPrice.toDouble() / 100 * price.toDouble())))
//            bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP)
//            return bd.toString()
        }


        fun getDiscountPrice(discountPrice: String, price: String): String {
            if (discountPrice.isNotEmpty() && price.isNotEmpty()) {
                return ((discountPrice.toDouble() / 100 * price.toDouble())).toString()
            } else {
                return "0.0"
            }
//            var bd = BigDecimal((price.toDouble() - (discountPrice.toDouble() / 100 * price.toDouble())))
//            bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP)
//            return bd.toString()
        }


        //        fun loader(context: Context){
//            val progress = ProgressDialog(context)
//            progress.setTitle("Loading")
//            progress.setMessage("Wait while loading...")
//            progress.setCancelable(false) // disable dismiss by tapping outside of the dialog
//            progress.show()
//        }
        val LOGGED_IN_PREF = "logged_in_status"

        fun getLoggedStatus(context: Context): Boolean {
            return getPrefs(context)
                .getBoolean(LOGGED_IN_PREF, false)
        }

        fun setLoggedStatus(context: Context) {
            return getPrefs(context)
                .edit().putBoolean(LOGGED_IN_PREF, true).apply()
        }


        fun getDateMonth(ourDate: String): String {
            var ourDate = ourDate
            val formatter = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
            val dateFormatter =
                SimpleDateFormat("dd/MM/YY", Locale.getDefault()) //this format changeable

            try {
                formatter.timeZone = TimeZone.getTimeZone("UTC")
                val value = formatter.parse(ourDate)

                dateFormatter.timeZone = TimeZone.getDefault()
                ourDate = dateFormatter.format(value)
            } catch (e: Exception) {
                ourDate = "00-00-0000 00:00"
            }
            return ourDate
        }


        fun getDateMonthh(ourDate: String): String {
            var ourDate = ourDate
            val formatter = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
            val dateFormatter =
                SimpleDateFormat("dd MMM", Locale.getDefault()) //this format changeable

            try {
                formatter.timeZone = TimeZone.getTimeZone("UTC")
                val value = formatter.parse(ourDate)

                dateFormatter.timeZone = TimeZone.getDefault()
                ourDate = dateFormatter.format(value)
            } catch (e: Exception) {
                ourDate = "00-00-0000 00:00"
            }
            return ourDate
        }


        fun getDateMonthYear(ourDate1: String): String {
            var ourDate = ourDate1
            val formatter = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
            val dateFormatter =
                SimpleDateFormat("MMM dd yyyy", Locale.getDefault()) //this format changeable

            try {
                formatter.timeZone = TimeZone.getTimeZone("UTC")
                val value = formatter.parse(ourDate)
                dateFormatter.timeZone = TimeZone.getDefault()
                ourDate = dateFormatter.format(value)
            } catch (e: Exception) {
                ourDate = "00/00/0000"
            }
            return ourDate
        }


        fun getDateMonthYearComplete(ourDate1: String): String {
            var ourDate = ourDate1
            val formatter = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
            val dateFormatter =
                SimpleDateFormat("dd MMMM yyyy", Locale.getDefault()) //this format changeable

            try {
                formatter.timeZone = TimeZone.getTimeZone("UTC")
                val value = formatter.parse(ourDate)

                dateFormatter.timeZone = TimeZone.getDefault()
                ourDate = dateFormatter.format(value)
            } catch (e: Exception) {
                ourDate = "00/00/0000"
            }
            return ourDate
        }


        fun getDateMonthYearCompletePayments(ourDate1: String): String {
            var ourDate = ourDate1
            val formatter = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
            val dateFormatter =
                SimpleDateFormat("dd MMMM yyyy", Locale.getDefault()) //this format changeable

            try {
                formatter.timeZone = TimeZone.getTimeZone("UTC")
                val value = formatter.parse(ourDate)

                dateFormatter.timeZone = TimeZone.getDefault()
                ourDate = dateFormatter.format(value)
            } catch (e: Exception) {
                ourDate = "00/00/0000"
            }
            return ourDate
        }


        fun getDateMonthYearCompletePayout(ourDate1: String): String {
            var ourDate = ourDate1
            val formatter = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
            val dateFormatter =
                SimpleDateFormat(
                    "dd MMMM yyyy hh:mm a",
                    Locale.getDefault()
                ) //this format changeable

            try {
                formatter.timeZone = TimeZone.getTimeZone("UTC")
                val value = formatter.parse(ourDate)

                dateFormatter.timeZone = TimeZone.getDefault()
                ourDate = dateFormatter.format(value)
            } catch (e: Exception) {
                ourDate = "00/00/0000"
            }
            ourDate = ourDate.replace("pm", "PM").replace("am", "AM")
            return ourDate
        }


        fun getDatee(ourDate1: String): String {
            var ourDate = ourDate1
            val formatter = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
            val dateFormatter =
                SimpleDateFormat("MM/dd/yyyy", Locale.getDefault()) //this format changeable

            try {
                formatter.timeZone = TimeZone.getTimeZone("UTC")
                val value = formatter.parse(ourDate)
                dateFormatter.timeZone = TimeZone.getDefault()
                ourDate = dateFormatter.format(value)
            } catch (e: Exception) {
                ourDate = "00/00/0000"
            }
            return ourDate
        }

        //---------------------------------------for notification------------------------------------------------//(18 Dec 2019)
        fun getDatt(ourDate1: String): String {
            var ourDate = ourDate1
            val formatter = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
            val dateFormatter =
                SimpleDateFormat("dd MMM yyyy", Locale.getDefault()) //this format changeable

            try {
                formatter.timeZone = TimeZone.getTimeZone("UTC")
                val value = formatter.parse(ourDate)

                dateFormatter.timeZone = TimeZone.getDefault()
                ourDate = dateFormatter.format(value)
            } catch (e: Exception) {
                ourDate = "00-00-0000 00:00"
            }
            return ourDate
        }


        fun getTime(ourDate1: String): String {
            var ourDate = ourDate1
            val formatter = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
            val dateFormatter =
                SimpleDateFormat("hh:mm a", Locale.getDefault()) //this format changeable

            try {
                formatter.timeZone = TimeZone.getTimeZone("UTC")
                val value = formatter.parse(ourDate)
                dateFormatter.timeZone = TimeZone.getDefault()
                ourDate = dateFormatter.format(value)


            } catch (e: Exception) {
                ourDate = "00:00"
            }
            val datee = ourDate.replace("pm", "PM").replace("am", "AM")
            return datee
        }


        fun getDateTime(ourDate1: String): String {
            var ourDate = ourDate1
            val formatter = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
            val dateFormatter =
                SimpleDateFormat("dd MMM , hh:mm a", Locale.getDefault()) //this format changeable

            try {
                formatter.timeZone = TimeZone.getTimeZone("UTC")
                val value = formatter.parse(ourDate)
                dateFormatter.timeZone = TimeZone.getDefault()
                ourDate = dateFormatter.format(value)


            } catch (e: Exception) {
                ourDate = "00:00"
            }
            val datee = ourDate.replace("pm", "PM").replace("am", "AM")
            return datee
        }


        fun getDate(ourDate: String): Date {
            val formatter = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
            //  val dateFormatter = SimpleDateFormat("dd", Locale.getDefault()) //this format changeable
            //  formatter.timeZone = TimeZone.getTimeZone("UTC")
            return formatter.parse(ourDate)
        }

        fun uploadImage(
            bitmap2: Uri,
            context: Context
        ): File {
            val bitmap = MediaStore.Images.Media.getBitmap(context.contentResolver, bitmap2)
            var size = 0
            // val resultBitmap = getImageUri(bitmap)
            var file = getImageUri3(bitmap, 0)

            // Get length of file in bytes
            var fileSizeInBytes = file.length()
            // Convert the bytes to Kilobytes (1 KB = 1024 Bytes)
            var fileSizeInKB = fileSizeInBytes / 1024
            // Convert the KB to MegaBytes (1 MB = 1024 KBytes)
            var fileSizeInMB = fileSizeInKB / 1024

            var bitmapByteCount = BitmapCompat.getAllocationByteCount(bitmap)

            if (fileSizeInMB >= 1.0) {
                var bitmdap = BitmapFactory.decodeFile(file.absolutePath)
                file = getImageUri3(
                    TransformationUtils.rotateImage(
                        BitmapFactory.decodeFile(file.absolutePath),
                        0
                    ), 1
                )
            } else {
                file = getImageUri3(
                    TransformationUtils.rotateImage(
                        BitmapFactory.decodeFile(file.absolutePath),
                        0
                    ), 1
                )
            }

            return file
        }


        private fun getImageUri3(inImage: Bitmap, i: Int): File {

            val root: String = Environment.getExternalStorageDirectory().toString()
            val myDir = File("$root/req_images")

            val fname = "Image_profile_" + System.currentTimeMillis().toString() + ".jpeg"
            val file = File(myDir, fname)
            if (file.exists())
                file.delete()
            myDir.mkdirs()
            try {

                val out = FileOutputStream(file)
                if (i == 1) {
                    inImage.compress(Bitmap.CompressFormat.JPEG, 80, out)
                } else {
                    inImage.compress(Bitmap.CompressFormat.JPEG, 100, out)
                }
                out.flush()
                out.close()
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return file
        }

        fun getRotateImage(photoFile: File?): Bitmap {

            val ei: ExifInterface = ExifInterface(photoFile!!.path)
            val orientation = ei.getAttributeInt(
                ExifInterface.TAG_ORIENTATION,
                1
            )
            val bitmap = BitmapFactory.decodeFile(photoFile.path)

            var rotatedBitmap: Bitmap? = bitmap
            when (orientation) {
                ExifInterface.ORIENTATION_ROTATE_90 ->
                    rotatedBitmap = TransformationUtils.rotateImage(bitmap, 90)

                ExifInterface.ORIENTATION_ROTATE_180 ->
                    rotatedBitmap = TransformationUtils.rotateImage(bitmap, 180)

                ExifInterface.ORIENTATION_ROTATE_270 ->
                    rotatedBitmap = TransformationUtils.rotateImage(bitmap, 270)


                ExifInterface.ORIENTATION_NORMAL ->
                    rotatedBitmap = bitmap
                ExifInterface.ORIENTATION_FLIP_HORIZONTAL -> flipImage(bitmap, true, false)
                ExifInterface.ORIENTATION_FLIP_VERTICAL -> flipImage(bitmap, false, true)
            }

            return rotatedBitmap!!
        }

        private fun flipImage(bitmap: Bitmap, horizontal: Boolean, vertical: Boolean): Bitmap {
            val matrix = Matrix()
            matrix.preScale(
                (if (horizontal) -1 else 1).toFloat(),
                (if (vertical) -1 else 1).toFloat()
            )
            return Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
        }

        @SuppressLint("SimpleDateFormat")
        private fun createImageFile(): File {
// Create an image file name
            val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
            val imageFileName = "JPEG_" + timeStamp + "_"
            val storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES
            )
            val image = File.createTempFile(
                imageFileName, // prefix
                ".jpg", // suffix
                storageDir // directory
            )
            return image
        }

        fun commonStaffAlert(
            mContext: Context,
            image1: String,
            name: String,
            speciality: String,
            facebook: String,
            instagram: String,
            twitter: String
        ) {
            val mDialogView =
                LayoutInflater.from(mContext).inflate(R.layout.common_alert_staff, null)
            val mBuilder = androidx.appcompat.app.AlertDialog.Builder(mContext)
                .setView(mDialogView)
            val mAlertDialog = mBuilder.show()
            val heading: TextView = mDialogView.findViewById(R.id.textHeader)
            val image: ImageView = mDialogView.findViewById(R.id.image)
            val description: TextView = mDialogView.findViewById(R.id.textMessage)
            val doneButton: ImageView = mDialogView.findViewById(R.id.textDoneImage)
            val doneButton1: ImageView = mDialogView.findViewById(R.id.textDoneImage1)
            val imgFacebook: ImageView = mDialogView.findViewById(R.id.imgFacebook)
            val imgInstagram: ImageView = mDialogView.findViewById(R.id.imgInstagram)
            val imgTwitter: ImageView = mDialogView.findViewById(R.id.imgTwitter)
            val imagePath = getPrefs(mContext).getString(
                dataImagePath,
                "http://cloudart.com.au/projects/upscribbr/uploads/"
            )
            val isSeller = getSharedPrefs(mContext).getBoolean(IS_SELLER, false)
            if (isSeller) {
                doneButton1.visibility = View.VISIBLE
                doneButton.visibility = View.GONE
            } else {
                doneButton1.visibility = View.GONE
                doneButton.visibility = View.VISIBLE
            }
            Glide.with(mContext).load(imagePath + image1).placeholder(R.drawable.ic_male_dummy)
                .into(image)
            heading.text = name

            imgFacebook.setOnClickListener {
                val mDialogView = LayoutInflater.from(mContext).inflate(R.layout.social_alert, null)
                val mBuilder = AlertDialog.Builder(mContext)
                    .setView(mDialogView)
                val mAlertDialog = mBuilder.show()
                mAlertDialog.setCancelable(false)
                if (facebook == "") {
                    mAlertDialog.socialInfo.text = "No info added"
                } else {
                    mAlertDialog.socialInfo.text = facebook
                }
                mAlertDialog.okBtn.setOnClickListener {
                    mAlertDialog.dismiss()
                }

            }


            imgInstagram.setOnClickListener {
                val mDialogView = LayoutInflater.from(mContext).inflate(R.layout.social_alert, null)
                val mBuilder = AlertDialog.Builder(mContext)
                    .setView(mDialogView)
                val mAlertDialog = mBuilder.show()
                mAlertDialog.setCancelable(false)
                if (instagram == "") {
                    mAlertDialog.socialInfo.text = "No info added"
                } else {
                    mAlertDialog.socialInfo.text = instagram
                }
                mAlertDialog.okBtn.setOnClickListener {
                    mAlertDialog.dismiss()
                }

            }

            imgTwitter.setOnClickListener {
                val mDialogView = LayoutInflater.from(mContext).inflate(R.layout.social_alert, null)
                val mBuilder = AlertDialog.Builder(mContext)
                    .setView(mDialogView)
                val mAlertDialog = mBuilder.show()
                mAlertDialog.setCancelable(false)
                if (twitter == "") {
                    mAlertDialog.socialInfo.text = "No info added"
                } else {
                    mAlertDialog.socialInfo.text = twitter
                }
                mAlertDialog.okBtn.setOnClickListener {
                    mAlertDialog.dismiss()
                }

            }


            if (speciality.trim() == "") {
//                description.text = mContext.resources.getString(R.string.lorum)
                description.text = "No speciality added"
            } else {
                description.text = speciality
            }

            doneButton.setOnClickListener {
                mAlertDialog.dismiss()
            }

            doneButton1.setOnClickListener {
                mAlertDialog.dismiss()
            }

            mAlertDialog.setCancelable(false)
            mAlertDialog.window.setBackgroundDrawableResource(R.drawable.bg_alert_transparent)

            mAlertDialog.textDoneImage.setOnClickListener {
                mAlertDialog.dismiss()
            }

        }


        fun getSplitPhoneNumber(contactNo: String, mContext: Context): String {

            var new3 = ""
            try {

                if (contactNo.startsWith("+91")) {
                    var withoutPlus = contactNo.replace("+91", "")
                    val newNum = withoutPlus
                    withoutPlus = PhoneNumberUtils.formatNumber(newNum, Locale.US.country)

                    if (withoutPlus.contains(")")) {
                        val new1 = withoutPlus.replace(")", "")
                        if (new1.contains("-")) {
                            val new2 = new1.replace("-", " ")
                            if (new2.startsWith("(")) {
                                new3 = new2.replace("(", "")
                            }
                        }
                    } else {
                        new3 = withoutPlus

                    }


                } else if (contactNo.startsWith("+1")) {
                    var withoutPlus = contactNo.replace("+1", "")
                    val newNum = withoutPlus
                    withoutPlus = PhoneNumberUtils.formatNumber(newNum, Locale.US.country)

                    if (withoutPlus.contains(")")) {
                        val new1 = withoutPlus.replace(")", "")
                        if (new1.contains("-")) {
                            val new2 = new1.replace("-", " ")
                            if (new2.startsWith("(")) {
                                new3 = new2.replace("(", "")
                            }
                        }
                    } else {
                        new3 = withoutPlus
                    }


                } else {
                    val newNumber = PhoneNumberUtils.formatNumber(contactNo, Locale.US.country)
                    if (newNumber.contains(")")) {
                        val new1 = newNumber.replace(")", "")
                        if (new1.contains("-")) {
                            val new2 = new1.replace("-", " ")
                            if (new2.startsWith("(")) {
                                new3 = new2.replace("(", "")
                            }
                        }
                    } else {
                        new3 = newNumber
                    }


                }
            } catch (e: Exception) {
                e.printStackTrace()
            }


            return if (new3 == "") {
                new3
            } else {
                getCountryDialCode(mContext) + " " + new3
            }
        }


        fun getSplitPhoneNumberWithoutCode(contactNo: String, mContext: Context): String {
            var new3 = ""
            if (contactNo.startsWith("+91")) {
                var withoutPlus = contactNo.replace("+91", "")
                val newNum = withoutPlus
                withoutPlus = PhoneNumberUtils.formatNumber(newNum, Locale.US.country)

                if (withoutPlus.contains(")")) {
                    val new1 = withoutPlus.replace(")", "")
                    if (new1.contains("-")) {
                        val new2 = new1.replace("-", " ")
                        if (new2.startsWith("(")) {
                            new3 = new2.replace("(", "")
                        }
                    }
                }


            } else if (contactNo.startsWith("+1")) {
                var withoutPlus = contactNo.replace("+1", "")
                val newNum = withoutPlus
                withoutPlus = PhoneNumberUtils.formatNumber(newNum, Locale.US.country)

                if (withoutPlus.contains(")")) {
                    val new1 = withoutPlus.replace(")", "")
                    if (new1.contains("-")) {
                        val new2 = new1.replace("-", " ")
                        if (new2.startsWith("(")) {
                            new3 = new2.replace("(", "")
                        }
                    }
                }


            } else {
                val newNumber = PhoneNumberUtils.formatNumber(contactNo, Locale.US.country)
                if (newNumber.contains(")")) {
                    val new1 = newNumber.replace(")", "")
                    if (new1.contains("-")) {
                        val new2 = new1.replace("-", " ")
                        if (new2.startsWith("(")) {
                            new3 = new2.replace("(", "")
                        }
                    }
                }


            }
            return new3
        }


        fun getCountryDialCode(context: Context): String {
            var CountryID: String = ""
            var contryDialCode = ""
            var countryCode = ""

            CountryID = getPrefs(context).getString(Constant.countryCode, "")
            val arrContryCode = context.resources.getStringArray(R.array.DialingCountryCode)
            for (element in arrContryCode) {
                val arrDial = element.split(",")
                if (arrDial[1].trim() == CountryID.trim()) {
                    contryDialCode = arrDial[0]
                    countryCode = "+$contryDialCode"
                    break
                }
            }
            return countryCode
        }

        fun getDisplayMetrics(windowManager: WindowManager): DisplayMetrics {
            val display = windowManager.defaultDisplay
            val metrics = DisplayMetrics()
            display.getMetrics(metrics)
            return metrics
        }

        fun getImageUri(inImage: Bitmap): File {
            val root: String = Environment.getExternalStorageDirectory().toString()
            val myDir = File("$root/req_images")
            myDir.mkdirs()
            val fname = "Image_profile.jpg"
            val file = File(myDir, fname)
            if (file.exists()) {
                file.delete()
            }

            try {
                val out = FileOutputStream(file)
                inImage.compress(Bitmap.CompressFormat.JPEG, 70, out)
                out.flush()
                out.close()
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
            return file
        }

        fun formatPhoneNumber(contact: String): String {
            var number =
                contact.replace("+1", "").replace("+91", "").replace(" ", "").replace("-", "")
            number = number.substring(0, number.length - 4) + " - " + number.substring(
                number.length - 4,
                number.length
            )
            number = number.substring(0, number.length - 10) + ") " + number.substring(
                number.length - 10,
                number.length
            )
            number = number.substring(0, number.length - 15) + "(" + number.substring(
                number.length - 15,
                number.length
            )
            return number

        }


        fun checkFav(campaignId: String, context: Context): Boolean {
            val mPrefs = context.getSharedPreferences(
                "data",
                AppCompatActivity.MODE_PRIVATE
            )
            val gson = Gson()
            val getFav = mPrefs.getString("favData", "")
            if (getFav != "") {
                val type = gson.fromJson<ArrayList<String>>(
                    getFav,
                    object : TypeToken<ArrayList<String>>() {

                    }.type
                )
                var exist = 0
                for (item in type) {
                    if (campaignId == item) {
                        exist = 1
                    }
                }
                return exist == 1
            } else {
                return false
            }
        }

        //        using sharekit
        fun shareAppUsingApsFlyer(context: InviteSubscription) {
            val linkGenerator = ShareInviteHelper.generateInviteUrl(context)
            linkGenerator.channel = "Skype"
            linkGenerator.addParameter("af_cost_value", "2.5")
            linkGenerator.addParameter("af_cost_currency", "USD")

            val listener = object : CreateOneLinkHttpTask.ResponseListener {
                override fun onResponse(s: String?) {
                    val share = Intent(Intent.ACTION_SEND)
                    share.type = "text/plain"
                    share.putExtra(
                        Intent.EXTRA_TEXT,
                        "Come abroad Upscribbr and experience this amazing subscription at a very attractive price.\n$s"
                    )
                    context.startActivity(Intent.createChooser(share, "Share!"))
                    Log.d("Invite Link", s)
                }

                override fun onResponseError(s: String?) {

                    Log.d("Invite Link", s)

                }
            }
            linkGenerator.generateLink(context, listener)
        }

    }

}