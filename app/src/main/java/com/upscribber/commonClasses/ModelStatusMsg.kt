package com.upscribber.commonClasses

import android.os.Parcel
import android.os.Parcelable

class ModelStatusMsg() : Parcelable{

    var email: String = ""
    var businessName: String = ""
    var position: Int = 0
    var campaign_name: String = ""
    var countryCode: String = ""
    var reg_type: String = ""
    var name: String = ""
    var count: String = ""
    var invited_customers: String = ""
    var auth_code: String = ""
    var status: String = ""
    var msg: String = ""
    var sentStatus = ""
    var type = ""
    var image = ""
    val like = -1

    constructor(parcel: Parcel) : this() {
        email = parcel.readString()
        businessName = parcel.readString()
        position = parcel.readInt()
        campaign_name = parcel.readString()
        countryCode = parcel.readString()
        reg_type = parcel.readString()
        name = parcel.readString()
        count = parcel.readString()
        invited_customers = parcel.readString()
        auth_code = parcel.readString()
        status = parcel.readString()
        msg = parcel.readString()
        sentStatus = parcel.readString()
        type = parcel.readString()
        image = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(email)
        parcel.writeString(businessName)
        parcel.writeInt(position)
        parcel.writeString(campaign_name)
        parcel.writeString(countryCode)
        parcel.writeString(reg_type)
        parcel.writeString(name)
        parcel.writeString(count)
        parcel.writeString(invited_customers)
        parcel.writeString(auth_code)
        parcel.writeString(status)
        parcel.writeString(msg)
        parcel.writeString(sentStatus)
        parcel.writeString(type)
        parcel.writeString(image)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ModelStatusMsg> {
        override fun createFromParcel(parcel: Parcel): ModelStatusMsg {
            return ModelStatusMsg(parcel)
        }

        override fun newArray(size: Int): Array<ModelStatusMsg?> {
            return arrayOfNulls(size)
        }
    }


}
