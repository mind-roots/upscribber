package com.upscribber.commonClasses

import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.http.*


interface WebServicesCustomers {

    @FormUrlEncoded
    @POST("buySubscription")
    fun buySubscription(
        @Field("auth_code") auth_code: String,
        @Field("sub_total") sub_total: String,
        @Field("total") total: String,
        @Field("subscription_id") subscription_id: String,
        @Field("tax") tax: String,
        @Field("first_name") first_name: String,
        @Field("email") email: String,
        @Field("street") street: String,
        @Field("state") state: String,
        @Field("city") city: String,
        @Field("zip_code") zip_code: String,
        @Field("card_id") card_id: String,
        @Field("billing_type") billing_type: String,
        @Field("billing_street") billing_street: String,
        @Field("billing_state") billing_state: String,
        @Field("billing_city") billing_city: String,
        @Field("billing_zipcode") billing_zipcode: String,
        @Field("credit") credit: String,
        @Field("free_trial") free_trial: String,
        @Field("total_amount") total_amount: String,
        @Field("coupon_type") coupon_type: String,
        @Field("is_intro") is_intro: String,
        @Field("contact_no") contact_no: String,
        @Field("actual_price") actual_price: String,
        @Field("buy_type") buy_type: String,
        @Field("last_name") last_name: String,
        @Field("discount_percentage") discount_percentage:String,
        @Field("payment_type") payment_type:String,
        @Field("payment_method_name")payment_method_name:String,
        @Field("last_4")last_4:String
    ): Call<ResponseBody>


     @Multipart
    @POST("buySubscription")
    fun buySubscriptionMerchant(
        @Part("user_id") user_id: RequestBody,
        @Part("sub_total") sub_total: RequestBody,
        @Part("total") total: RequestBody,
        @Part("subscription_id") subscription_id: RequestBody,
        @Part("tax") tax: RequestBody,
        @Part("first_name") first_name: RequestBody,
        @Part("email") email: RequestBody,
        @Part("street") street: RequestBody,
        @Part("state") state: RequestBody,
        @Part("city") city: RequestBody,
        @Part("zip_code") zip_code: RequestBody,
        @Part("card_id") card_id: RequestBody,
        @Part("billing_type") billing_type: RequestBody,
        @Part("billing_street") billing_street: RequestBody,
        @Part("billing_state") billing_state: RequestBody,
        @Part("billing_city") billing_city: RequestBody,
        @Part("billing_zipcode") billing_zipcode: RequestBody,
        @Part("free_trial") free_trial: RequestBody,
        @Part("total_amount") total_amount: RequestBody,
        @Part("is_intro") is_intro: RequestBody,
        @Part("contact_no") contact_no: RequestBody,
        @Part("actual_price") actual_price: RequestBody,
        @Part("buy_type") buy_type: RequestBody,
        @Part("last_name") last_name: RequestBody,
        @Part("credit") credit: RequestBody,
        @Part("coupon_type") coupon_type: RequestBody,
        @Part file: MultipartBody.Part,
        @Part("new_user") new_user: RequestBody,
        @Part("last_4") last_4: RequestBody,
        @Part("payment_type") payment_type: RequestBody,
        @Part("payment_method_name") payment_method_name: RequestBody

    ): Call<ResponseBody>



    @FormUrlEncoded
    @POST("renewCycle")
    fun renewCycle(
        @Field("auth_code") auth_code: String,
        @Field("order_id") order_id: String,
        @Field("card_id") card_id: String,
        @Field("credit") credit: String,
        @Field("total_amount") total_amount: String,
        @Field("coupon_type") coupon_type: String
    ): Call<ResponseBody>


    @Headers(
        "Accept: application/json",
        "Content-Type: application/x-www-form-urlencoded",
        "Authorization: Bearer sk_live_MY8EIcR6g0TW0jmuNBtZqWkZ006jNZ5gWt"
    )
    @POST("tokens")
    fun tokensMerchant(
        @Query("card[number]") cardNumber: String,
        @Query("card[exp_month]") expMonth: String,
        @Query("card[exp_year]") expYear: String,
        @Query("card[cvc]") cvc: String,
        @Query("card[currency]") currency: String
    ): Call<ResponseBody>

    @Headers(
        "Accept: application/json",
        "Content-Type: application/x-www-form-urlencoded",
        "Authorization: Bearer sk_live_MY8EIcR6g0TW0jmuNBtZqWkZ006jNZ5gWt"
    )
    @POST("tokens")
    fun tokensBank(
        @Query("bank_account[account_number]") account_number: String,
        @Query("bank_account[routing_number]") routing_number: String,
        @Query("bank_account[account_holder_name]") account_holder_name: String,
        @Query("bank_account[country]") country: String,
        @Query("bank_account[currency]") currency: String
    ): Call<ResponseBody>


    @Headers(
        "Accept: application/json",
        "Content-Type: application/x-www-form-urlencoded",
        "Authorization: Bearer sk_live_MY8EIcR6g0TW0jmuNBtZqWkZ006jNZ5gWt"
    )
    @POST("tokens")
    fun tokens(
        @Query("card[number]") cardNumber: String,
        @Query("card[exp_month]") expMonth: String,
        @Query("card[exp_year]") expYear: String,
        @Query("card[cvc]") cvc: String,
        @Query("card[name]") name: String,
        @Query("card[currency]") currency: String
    ): Call<ResponseBody>


    @Headers(
        "Accept: application/json",
        "Content-Type: application/x-www-form-urlencoded",
        "Authorization: Bearer sk_live_MY8EIcR6g0TW0jmuNBtZqWkZ006jNZ5gWt"
    )
    @GET("{customerId}/sources?")
    fun stripeRetrieve(
        @Path("customerId") customerId: String,
        @Query("limit") limit: String
    ): Call<ResponseBody>


//    @Headers(
//        "Accept: application/json",
//        "Content-Type: application/x-www-form-urlencoded",
//        "Authorization: Bearer sk_live_MY8EIcR6g0TW0jmuNBtZqWkZ006jNZ5gWt"
//    )
//    @GET("{customerId}/sources?")
//    fun stripeRetrieveCard(
//        @Path("paymentId") customerId: String,
//        @Query("limit") limit: String
//    ): Call<ResponseBody>


    @Headers(
        "Accept: application/json",
        "Content-Type: application/x-www-form-urlencoded",
        "Authorization: Bearer sk_live_MY8EIcR6g0TW0jmuNBtZqWkZ006jNZ5gWt"
    )
    @POST("{customerId}/sources?")
    fun stripe_customers(
        @Path("customerId") customerId: String,
        @Query("source") token: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("getBarCode")
    fun getBarCode(
        @Field("auth_code") auth_code: String,
        @Field("order_id") order_id: String,
        @Field("quantity") quantity: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("deleteBarCode")
    fun deleteBarCode(
        @Field("auth_code") auth_code: String,
        @Field("order_id") order_id: String,
        @Field("code") code: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("redeemResponse")
    fun redeemResponse(
        @Field("auth_code") auth_code: String,
        @Field("order_id") order_id: String,
        @Field("code") code: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("redeem")
    fun redeem(
        @Field("auth_code") auth_code: String,
        @Field("code") code: String,
        @Field("quantity") quantity: String,
        @Field("order_id") order_id: String,
        @Field("type") type: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("redeem")
    fun redeemCustomer(
        @Field("auth_code") auth_code: String,
        @Field("quantity") quantity: String,
        @Field("order_id") order_id: String,
        @Field("type") type: String,
        @Field("customer_id") customer_id: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("declinedInvitedSubs")
    fun declinedInvitedSubs(
        @Field("auth_code") auth_code: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("getBarcodeQty")
    fun getBarcodeQty(
        @Field("auth_code") auth_code: String,
        @Field("code") code: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("viewAll_brands")
    fun viewAllBrands(
        @Field("auth_code") auth_code: String,
        @Field("business_id") business_id: String,
        @Field("page") page: String,
        @Field("type") type: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("viewAll_brands")
    fun viewAllSubscriprtion(
        @Field("auth_code") auth_code: String,
        @Field("campaign_id") business_id: String,
        @Field("page") page: String,
        @Field("type") type: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("saveProduct")
    fun saveProductMerchant(
        @Field("name") name: String,
        @Field("auth_code") auth_code: String,
        @Field("price") price: String,
        @Field("description") description: String,
        @Field("tags") tags: String,
        @Field("discount_price") discount_price: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("saveProduct")
    fun saveProductEditMerchant(
        @Field("name") name: String,
        @Field("auth_code") auth_code: String,
        @Field("price") price: String,
        @Field("description") description: String,
        @Field("tags") tags: String,
        @Field("id") id: String,
        @Field("discount_price") discount_price: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("deleteProduct")
    fun deleteProduct(
        @Field("auth_code") auth_code: String,
        @Field("id") id: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("readNewNotification")
    fun readNewNotification(
        @Field("auth_code") auth_code: String
    ): Call<ResponseBody>




    @FormUrlEncoded
    @POST("logout")
    fun logout(
        @Field("auth_code") auth_code: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("getCampaigns")
    fun getCampaigns(
        @Field("auth_code") auth_code: String,
        @Field("page") page: String,
        @Field("search") search: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("getCampaigns")
    fun getCampaigns(
        @Field("auth_code") auth_code: String,
        @Field("page") page: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("getUserIdByCode")
    fun getUserIdByCode(
        @Field("code") code: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("get_tax")
    fun get_tax(
        @Field("auth_code") auth_code: String,
        @Field("amount") amount: String,
        @Field("campaign_id") campaign_id: String,
        @Field("zipcode") zipcode: String,
        @Field("state") state: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("getTeamMembers")
    fun teamMember(
        @Field("auth_code") auth_code: String,
        @Field("self") self: String,
        @Field("is_invited") is_invited: String,
        @Field("search") search: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("staffLink")
    fun staffLink(
        @Field("auth_code") auth_code: String,
        @Field("staff_id") staff_id: String,
        @Field("type") type: String,
        @Field("business_id") business_id: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("staffRequest")
    fun staffRequest(
        @Field("auth_code") auth_code: String,
        @Field("type") type: String,
        @Field("contact_no") contact_no: String,
        @Field("name") name: String,
        @Field("email") email: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("assignTeamMember")
    fun assignTeamMember(
        @Field("auth_code") auth_code: String,
        @Field("order_id") order_id: String,
        @Field("staff_id") staff_id: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("get_categories")
    fun getCategories(
        @Field("auth_code") auth_code: String,
        @Field("type") type: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("merchant_signup")
    fun merchantSignup(
        @Field("auth_code") auth_code: String,
        @Field("category_id") category_id: String,
        @Field("step") step: String,
        @Field("type") type: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("merchant_signup")
    fun merchantSignupSecond(
        @Field("auth_code") auth_code: String,
        @Field("business_name") business_name: String,
        @Field("ein") ein: String,
        @Field("step") step: String,
        @Field("type") type: String,
        @Field("business_id") business_id: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("merchant_signup")
    fun merchantSignupThird(
        @Field("auth_code") auth_code: String,
        @Field("business_email") business_email: String,
        @Field("business_phone") business_phone: String,
        @Field("business_address") business_address: String,
        @Field("website") website: String,
        @Field("step") step: String,
        @Field("city") city: String,
        @Field("state") state: String,
        @Field("zip_code") zip_code: String,
        @Field("street") street: String,
        @Field("billing_street") billing_street: String,
        @Field("billing_state") billing_state: String,
        @Field("billing_city") billing_city: String,
        @Field("billing_zipcode") billing_zipcode: String,
        @Field("billing_type") billing_type: String,
        @Field("billing_address") billing_address: String,
        @Field("type") type: String,
        @Field("lat") lat: String,
        @Field("long") long: String,
        @Field("suite") suite: String,
        @Field("business_id") business_id: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("merchant_signup")
    fun merchantSignupFourth(
        @Field("auth_code") auth_code: String,
        @Field("step") step: String,
        @Field("first_name") first_name: String,
        @Field("last_name") last_name: String,
        @Field("home_address") home_address: String,
        @Field("birthdate") birthdate: String,
        @Field("ssn") ssn: String,
        @Field("contact_no") contact_no: String,
        @Field("city") city: String,
        @Field("state") state: String,
        @Field("zip_code") zip_code: String,
        @Field("street") street: String,
        @Field("home_street") home_street: String,
        @Field("home_state") home_state: String,
        @Field("home_city") home_city: String,
        @Field("home_zipcode") home_zipcode: String,
        @Field("type") type: String,
        @Field("lat") lat: String,
        @Field("long") long: String,
        @Field("home_suite") home_suite: String,
        @Field("legal_name") legal_name: String,
        @Field("business_id") business_id: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("merchant_signup")
    fun merchantSignupFifth(
        @Field("auth_code") auth_code: String,
        @Field("stripe_token") stripe_token: String,
        @Field("pay_type") pay_type: String,
        @Field("step") step: String,
        @Field("type") type: String,
        @Field("business_id") business_id: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("merchant_signup")
    fun merchantSignuBank(
        @Field("auth_code") auth_code: String,
        @Field("account_number") account_number: String,
        @Field("routing_number") routing_number: String,
        @Field("bank_owner_name") bank_owner_name: String,
        @Field("step") step: String,
        @Field("pay_type") pay_type: String,
        @Field("type") type: String,
        @Field("business_id") business_id: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("merchant_signup")
    fun merchantSignuTerms(
        @Field("auth_code") auth_code: String,
        @Field("step") step: String,
        @Field("type") type: String,
        @Field("business_id") business_id: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("pauseSubscription")
    fun pauseSubscription(
        @Field("auth_code") auth_code: String,
        @Field("status") status: String,
        @Field("order_id") order_id: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("updateAddress")
    fun updateBillingAddress(
        @Field("auth_code") auth_code: String,
        @Field("street") street: String,
        @Field("state") state: String,
        @Field("city") city: String,
        @Field("zip_code") zip_code: String,
        @Field("suite") suite: String,
        @Field("type") type: String,
        @Field("u_type") u_type: String
    ): Call<ResponseBody>


     @FormUrlEncoded
    @POST("updateAddress")
    fun updateAddressByMerchant(
        @Field("auth_code") auth_code: String,
        @Field("user_id") user_id: String,
        @Field("street") street: String,
        @Field("state") state: String,
        @Field("city") city: String,
        @Field("zip_code") zip_code: String,
        @Field("suite") suite: String,
        @Field("billing_street") billing_street: String,
        @Field("billing_state") billing_state: String,
        @Field("billing_city") billing_city: String,
        @Field("billing_zip_code") billing_zip_code: String,
        @Field("billing_suite") billing_suite: String,
        @Field("type") type: String,
        @Field("u_type") u_type: String
    ): Call<ResponseBody>






    @FormUrlEncoded
    @POST("updateAddress")
    fun updateBillingAddress1(
        @Field("auth_code") auth_code: String,
        @Field("billing_street") billing_street: String,
        @Field("billing_state") billing_state: String,
        @Field("billing_city") billing_city: String,
        @Field("billing_zip_code") billing_zip_code: String,
        @Field("billing_suite") billing_suite: String,
        @Field("type") type: String,
        @Field("u_type") u_type: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("validBusinessName")
    fun validBusinessName(
        @Field("business_name") business_name: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("getActivity")
    fun getActivity(
        @Field("auth_code") auth_code: String,
        @Field("type") type: String,
        @Field("tab") tab: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("my_subscriptions")
    fun mySubscriptions(
        @Field("auth_code") auth_code: String,
        @Field("type") type: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("refundSubscription")
    fun refundSubscription(
        @Field("auth_code") auth_code: String,
        @Field("subscription_id") subscription_id: String,
        @Field("amount") amount: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("discountCancel")
    fun discountCancel(
        @Field("auth_code") auth_code: String,
        @Field("subscription_id") subscription_id: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("cancelSubscription")
    fun cancelSubscription(
        @Field("auth_code") auth_code: String,
        @Field("order_id") order_id: String,
        @Field("reason") reason: String,
        @Field("type") type: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("category")
    fun category(
        @Field("auth_code") auth_code: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("search")
    fun search(
        @Field("auth_code") auth_code: String,
        @Field("search") search: String,
        @Field("lat") lat: String,
        @Field("filter_by") filter_by: JSONObject,
        @Field("sort_by") sort_by: JSONObject,
        @Field("long") long: String,
        @Field("type") type: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("getAllBusiness")
    fun getAllBusiness(
        @Field("auth_code") auth_code: String,
        @Field("search") search: String,
        @Field("page") page: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("getRatingTags")
    fun getRatingTags(
        @Field("auth_code") auth_code: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("getNotifications")
    fun getUserNotifications(
        @Field("auth_code") auth_code: String,
        @Field("type") type: String,
        @Field("page") page: Int
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("getBusinessLinkInfo")
    fun getBusinessLinkInfo(
        @Field("auth_code") auth_code: String,
        @Field("business_id") business_id: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("get_state")
    fun getStates(
        @Field("auth_code") auth_code: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("viewAll_categories")
    fun viewAllCategories(
        @Field("auth_code") auth_code: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("share_subscription")
    fun shareSubscription(
        @Field("auth_code") auth_code: String,
        @Field("full_name") full_name: String,
        @Field("contact_no") contact_no: String,
        @Field("email") email: String,
        @Field("order_id") order_id: String,
        @Field("type") type: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("share_subscription")
    fun unShareSubscription(
        @Field("auth_code") auth_code: String,
        @Field("order_id") order_id: String,
        @Field("type") type: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("getCredit")
    fun getCredit(
        @Field("auth_code") auth_code: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("viewAll")
    fun viewAll(
        @Field("auth_code") auth_code: String,
        @Field("campaign_id") campaign_id: String,
        @Field("page") page: String,
        @Field("type") type: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("viewAll")
    fun viewAllStore(
        @Field("auth_code") auth_code: String,
        @Field("business_id") business_id: String,
        @Field("page") page: String,
        @Field("type") type: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("subscription_details")
    fun subscriptionDetails(
        @Field("auth_code") auth_code: String,
        @Field("campaign_id") campaign_id: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("buyCampaignRequest")
    fun buyCampaignRequest(
        @Field("auth_code") auth_code: String,
        @Field("campaign_id") campaign_id: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("merchant_details")
    fun merchantDetails(
        @Field("auth_code") auth_code: String,
        @Field("business_id") business_id: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("get_favorite")
    fun getFavourite(
        @Field("auth_code") auth_code: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("set_favorite")
    fun setFavorite(
        @Field("auth_code") auth_code: String,
        @Field("campaign_id") campaign_id: String,
        @Field("type") type: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("getStepsHistory")
    fun getStepsHistory(
        @Field("auth_code") auth_code: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("change_password")
    fun updatePassword(
        @Field("auth_code") auth_code: String,
        @Field("old_password") old_password: String,
        @Field("new_password") new_password: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("HomeV1")
    fun Home(
        @Field("auth_code") auth_code: String,
        @Field("tag_name") tag_name: String,
        @Field("city") city: String,
        @Field("sort_by") sort_by: JSONObject,
        @Field("filter_by") filter_by: JSONObject,
        @Field("type") type: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("getProfile")
    fun getProfile(
        @Field("auth_code") auth_code: String,
        @Field("type") type: String,
        @Field("view_type") view_type: String,
        @Field("view_id") view_id: String
    ): Call<ResponseBody>

    @Multipart
    @POST("updateProfile")
    fun updateProfile(
        @Part("auth_code") auth_code: RequestBody,
        @Part("type") type: RequestBody,
        @Part("name") name: RequestBody,
        @Part("email") email: RequestBody,
        @Part file: MultipartBody.Part,
        @Part("phone") phone: RequestBody
    ): Call<ResponseBody>


     @FormUrlEncoded
    @POST("updateProfile")
    fun updateProfileMerchant(
        @Field("user_id") user_id: String,
        @Field("name") name: String,
        @Field("phone") phone: String,
        @Field("email") email: String,
        @Field("type") type: String
    ): Call<ResponseBody>


    @Multipart
    @POST("updateProfile")
    fun updateImage(
        @Part("auth_code") auth_code: RequestBody,
        @Part("type") type: RequestBody,
        @Part file: MultipartBody.Part
    ): Call<ResponseBody>

    @Multipart
    @POST("updateProfile")
    fun updateProfile2(
        @Part("auth_code") auth_code: RequestBody,
        @Part("type") type: RequestBody,
        @Part("name") name: RequestBody,
        @Part("email") email: RequestBody,
        @Part("phone") phone: RequestBody
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("updateProfile")
    fun updateSkills(
        @Field("auth_code") auth_code: String,
        @Field("type") type: String,
        @Field("speciality") speciality: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("updateProfile")
    fun updateTwitter(
        @Field("auth_code") auth_code: String,
        @Field("type") type: String,
        @Field("twitter") twitter: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("updateProfile")
    fun updateSocial(
        @Field("auth_code") auth_code: String,
        @Field("type") type: String,
        @Field("facebook") facebook: String,
        @Field("instagram") instagram: String,
        @Field("twitter") twitter: String,
        @Field("schedule") schedule: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("sendEmailVerification")
    fun sendEmailVerification(
        @Field("auth_code") auth_code: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("updateProfile")
    fun updateNotificationToggle(
        @Field("auth_code") auth_code: String,
        @Field("push_setting") push_setting: String,
        @Field("type") type: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("deleteCustomer")
    fun deleteCustomer(
        @Field("auth_code") auth_code: String,
        @Field("contact_no") contact_no: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("updateProfile")
    fun updateFacebook(
        @Field("auth_code") auth_code: String,
        @Field("type") type: String,
        @Field("facebook") facebook: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("updateProfile")
    fun updateBusinessInfo(
        @Field("auth_code") auth_code: String,
        @Field("type") type: String,
        @Field("about") about: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("updateProfile")
    fun updateInstagram(
        @Field("auth_code") auth_code: String,
        @Field("type") type: String,
        @Field("instagram") instagram: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("verify_mobile")
    fun verifyMobileNumber(
        @Field("phone_no") phone_no: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("getOtp")
    fun GetOtp(
        @Field("contact_no") contact_no: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("sendOtpWithMobileVerification")
    fun sendOtpWithMobileVerification(
        @Field("contact_no") contact_no: String,
        @Field("type") type: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("verifyOtp")
    fun VerifyOtp(
        @Field("contact_no") contact_no: String,
        @Field("otp") otp: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("login")
    fun Login(
        @Field("username") username: String,
        @Field("password") password: String,
        @Field("device_id") device_id: String,
        @Field("device_name") device_name: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("saveLocation")
    fun saveLocation(
        @Field("auth_code") auth_code: String,
        @Field("id") id: String,
        @Field("street") street: String,
        @Field("city") city: String,
        @Field("state") state: String,
        @Field("zip_code") zip_code: String,
        @Field("store_timing") store_timing: JSONArray,
        @Field("lat") lat: String,
        @Field("long") long: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("getProducts")
    fun getProducts(
        @Field("auth_code") auth_code: String,
        @Field("search") search: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("getRedeemHistory")
    fun getRedeemHistory(
        @Field("auth_code") auth_code: String,
        @Field("type") type: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("getLocation")
    fun getLocation(
        @Field("auth_code") auth_code: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("deleteLocation")
    fun deleteLocation(
        @Field("auth_code") auth_code: String,
        @Field("location_id") location_id: String,
        @Field("is_default") is_default: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("setDefultLocation")
    fun setDefultLocation(
        @Field("auth_code") auth_code: String,
        @Field("location_id") location_id: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("productCategory")
    fun productCategory(
        @Field("auth_code") auth_code: String
    ): Call<ResponseBody>


//    @FormUrlEncoded
//    @POST("signup")
//    fun SignUp(
//        @Field("contact_no") contact_no: String,
//        @Field("email") email: String,
//        @Field("password") password: String,
//        @Field("reg_type") reg_type: String,
//        @Field("device_id") device_id: String,
//        @Field("onesignal_id") onesignal_id: String,
//        @Field("gender") gender: String,
//        @Field("dob") dob: String,
//        @Field("invite_code") invite_code: String,
//        @Field("name") name: String,
//        @Field("social_id") social_id: String,
//        @Field("device_name") device_name: String
//    ): Call<ResponseBody>


    @Multipart
    @POST("signup")
    fun SignUp(
        @Part("contact_no") contact_no: RequestBody,
        @Part("email") email: RequestBody,
        @Part("password") password: RequestBody,
        @Part("reg_type") reg_type: RequestBody,
        @Part("device_id") device_id: RequestBody,
        @Part("onesignal_id") onesignal_id: RequestBody,
        @Part("gender") gender: RequestBody,
        @Part("dob") dob: RequestBody,
        @Part("invite_code") invite_code: RequestBody,
        @Part("name") name: RequestBody,
        @Part("social_id") social_id: RequestBody,
        @Part("device_name") device_name: RequestBody,
        @Part file: MultipartBody.Part
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("signup")
    fun SignUpMerchant(
        @Field("contact_no") contact_no: String,
        @Field("email") email: String,
        @Field("signup_type") signup_type: String,
        @Field("auth_code") auth_code: String,
        @Field("name") name: String,
        @Field("billing_street") billing_street: String,
        @Field("billing_state") billing_state: String,
        @Field("billing_city") billing_city: String,
        @Field("billing_zipcode") billing_zipcode: String,
        @Field("billing_suite") billing_suite: String,
        @Field("home_street") home_street: String,
        @Field("home_state") home_state: String,
        @Field("home_city") home_city: String,
        @Field("home_zipcode") home_zipcode: String,
        @Field("home_suite") home_suite: String,
        @Field("reg_type") reg_type: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("getAuthCode")
    fun getAuthCode(
        @Field("contact_no") contact_no: String,
        @Field("device_id") device_id: String,
        @Field("device_name") device_name: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("verifyEmail")
    fun verifyEmail(
        @Field("email") email: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("verify_account")
    fun verify_account(
        @Field("sid") sid: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("saveReview")
    fun saveReview(
        @Field("auth_code") auth_code: String,
        @Field("order_id") order_id: String,
        @Field("type") type: String,
        @Field("message_id") message_id: String,
        @Field("message") message: String,
        @Field("notification_id") notification_id: String

    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("getCustomers")
    fun getCustomers(
        @Field("auth_code") auth_code: String,
        @Field("search") search: String,
        @Field("view_type") view_type: String,
        @Field("view_id") view_id: String,
        @Field("type") type: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("inviteCustomer")
    fun inviteCustomer(
        @Field("auth_code") auth_code: String,
        @Field("customers") customers: String,
        @Field("subscription_id") subscription_id: String,
        @Field("phone_no") phone_no: String,
        @Field("url") url: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("boomSubscription")
    fun boomSubscription(
        @Field("auth_code") auth_code: String,
        @Field("campaign_id") campaign_id: String,
        @Field("buy") buy: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("userRefer")
    fun userRefer(
        @Field("auth_code") auth_code: String,
        @Field("invite_code") invite_code: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("forgotPassword")
    fun forgotPassword(
        @Field("contact_no") contact_no: String,
        @Field("password") password: String,
        @Field("device_id") device_id: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("saveFeedback")
    fun saveFeedback(
        @Field("auth_code") auth_code: String,
        @Field("response") response: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("searchBusiness")
    fun searchBusiness(
        @Field("auth_code") auth_code: String,
        @Field("name") name: String
    ): Call<ResponseBody>

    @FormUrlEncoded
    @POST("updateUserCity")
    fun updateUserCity(
        @Field("auth_code") auth_code: String,
        @Field("city") city: String
    ): Call<ResponseBody>


    @FormUrlEncoded
    @POST("dashboard")
    fun dashboard(
        @Field("auth_code") auth_code: String
    ): Call<ResponseBody>

    @Headers(
        "Accept: application/json",
        "Content-Type: application/x-www-form-urlencoded",
        "Authorization: Bearer sk_live_MY8EIcR6g0TW0jmuNBtZqWkZ006jNZ5gWt"
    )
    @GET("accounts/{stripeId}/external_accounts?")
    fun paymentCardBank(@Path("stripeId") stripeId: String, @Query("object") type: String): Call<ResponseBody>

    @GET("json")
    fun getCountryCode(): Call<ResponseBody>


}