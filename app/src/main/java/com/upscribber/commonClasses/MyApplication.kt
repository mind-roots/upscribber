package com.upscribber.commonClasses

import android.app.Application
import android.util.Log
import com.appsflyer.AppsFlyerConversionListener
import com.appsflyer.AppsFlyerLib
import com.google.firebase.FirebaseApp
import com.google.firebase.messaging.FirebaseMessagingService
import com.instabug.library.Instabug
import com.instabug.library.invocation.InstabugInvocationEvent
import com.google.firebase.analytics.FirebaseAnalytics
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import androidx.multidex.MultiDex
import org.antlr.runtime.misc.IntArray


class MyApplication : Application() {

    private val AF_DEV_KEY = "9ZoQoVJhU3FRyk8oC5X9kA"
    lateinit var mFirebaseAnalytics: FirebaseAnalytics

    override fun onCreate() {
        super.onCreate()

        MultiDex.install(this);
        try {FirebaseApp.initializeApp(this)
            mFirebaseAnalytics = FirebaseAnalytics.getInstance(this)
        } catch (e: Exception) {
            e.printStackTrace()
        }

//        Instabug.Builder(this, "f07a71c164528790d4e1962ff39be80a")
//            .setInvocationEvents(InstabugInvocationEvent.SHAKE, InstabugInvocationEvent.SCREENSHOT)
//            .build()
//        getAppFlyers()
    }


    private fun getAppFlyers() {

        val conversionListener = object : AppsFlyerConversionListener {

            override fun onInstallConversionDataLoaded(conversionData: MutableMap<String, String>?) {
                for (model in conversionData!!.keys) {
                    Log.d("LOG_TAG", "attribute: " + model + " = " + conversionData[model])
                }
            }

            override fun onAppOpenAttribution(conversionData: MutableMap<String, String>?) {

                for (model in conversionData!!.keys) {
                    Log.d("LOG_TAG", "attribute: " + model + " = " + conversionData[model])
                }
            }

            override fun onAttributionFailure(errorMessage: String?) {

                Log.d("LOG_TAG", "error onAttributionFailure : $errorMessage")
            }

            override fun onInstallConversionFailure(errorMessage: String?) {

                Log.d("LOG_TAG", "error getting conversion data: $errorMessage")
            }
        }
        AppsFlyerLib.getInstance().init(AF_DEV_KEY, conversionListener, applicationContext)
        AppsFlyerLib.getInstance().startTracking(this)
    }

}