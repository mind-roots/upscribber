package com.upscribber.search

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.upscribber.categories.subCategories.SubCategoryModel
import com.upscribber.commonClasses.ModelStatusMsg
import org.json.JSONObject

class SearchViewModel(application: Application) : AndroidViewModel(application) {

    var searchRepository: SearchRepository = SearchRepository(application)

    fun getTopFiveCategories(auth: String) {
        searchRepository.getTopFiveCategories(auth)
    }

    fun getStatus(): LiveData<ModelStatusMsg> {
        return searchRepository.getmDataFailure()
    }

    fun getmDataCategories(): LiveData<ArrayList<TopCategoriesSearchModel>> {
        return searchRepository.getmDataCategories()
    }

    fun getmDataSearch(): LiveData<ArrayList<SubCategoryModel>> {
        return searchRepository.getmDataSearch()
    }


    fun getSearchData(
        auth: String,
        query: String,
        latitude: Double,
        filterBy: JSONObject,
        sortBy: JSONObject,
        longitude: Double,
        type: Int
    ) {
        searchRepository.getSerachData(auth, query, latitude, filterBy, sortBy, longitude, type)

    }

    fun setFavourite(auth: String, subCategoryModel: SubCategoryModel) {
        searchRepository.getFavourite(auth,subCategoryModel)

    }

    fun getmDataFav(): LiveData<SubCategoryModel> {
        return searchRepository.getmDataFav()
    }


}