package com.upscribber.search

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.upscribber.categories.subCategories.SubCategoriesActivity
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import kotlinx.android.synthetic.main.search_top_category.view.*

class TopCategoryAdapter
    (
    private val mContext: Context

) :
    RecyclerView.Adapter<TopCategoryAdapter.MyViewHolder>() {

    private var list: ArrayList<TopCategoriesSearchModel> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(mContext)
            .inflate(R.layout.search_top_category, parent, false)
        return MyViewHolder(v)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = list[position]
        Glide.with(mContext).load(model.selected_image).placeholder(R.mipmap.circular_placeholder).into(holder.itemView.ivCategoryLogo)

        holder.itemView.tvCategoryTitle.text = model.name
        holder.itemView.numberSubscriptions.text = model.subscription_count + " subscriptions"

        holder.itemView.setOnClickListener {
            mContext.startActivity(Intent(mContext, SubCategoriesActivity::class.java)
                .putExtra("model",model.name).putExtra("topCategory",model))
        }
    }

    fun update(it: java.util.ArrayList<TopCategoriesSearchModel>?) {
        list = it!!
        notifyDataSetChanged()

    }


    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}
