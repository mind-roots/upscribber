package com.upscribber.search

import android.app.Application
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.common.reflect.TypeToken
import com.google.gson.Gson
import com.upscribber.categories.subCategories.SubCategoryModel
import com.upscribber.commonClasses.Constant
import com.upscribber.commonClasses.ModelStatusMsg
import com.upscribber.commonClasses.WebServicesCustomers
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class SearchRepository(var application: Application) {

    private val mDataCategories = MutableLiveData<ArrayList<TopCategoriesSearchModel>>()
    private val mDataSearchData = MutableLiveData<ArrayList<SubCategoryModel>>()
    val mDataFailure = MutableLiveData<ModelStatusMsg>()
    private val mDataFavourite=  MutableLiveData<SubCategoryModel>()

    fun getTopFiveCategories(auth: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.category(auth)
        call.enqueue(object : Callback<ResponseBody> {


            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        var subCount = ""
                        val arrayCategories = ArrayList<TopCategoriesSearchModel>()
                        if (status == "true") {
                            val data = json.getJSONArray("data")
                            for (i in 0 until data.length()) {
                                val dataCategries = data.getJSONObject(i)
                                val modelCategories = TopCategoriesSearchModel()
                                modelCategories.name = dataCategries.optString("name")
                                modelCategories.id = dataCategries.optString("id")
                                modelCategories.subscription_count = dataCategries.optString("subscription_count")
                                modelCategories.unselected_image = dataCategries.optString("unselected_image")
                                modelCategories.selected_image = dataCategries.optString("selected_image")
                                subCount = dataCategries.optString("subscription_count")
                                modelCategories.status = status
                                modelCategories.msg = msg
                                if (subCount != "0"){
                                    arrayCategories.add(modelCategories)
                                }
                            }

                            mDataCategories.value = arrayCategories
                        } else {
                            modelStatus.status = "false"
                            mDataFailure.value = modelStatus
                        }


                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus

            }
        })
    }


    fun getmDataFailure(): LiveData<ModelStatusMsg> {
        return mDataFailure
    }

    fun getmDataCategories(): LiveData<ArrayList<TopCategoriesSearchModel>> {
        return mDataCategories
    }

    fun getSerachData(
        auth: String,
        query: String,
        latitude: Double,
        filterBy: JSONObject,
        sortBy: JSONObject,
        longitude: Double,
        type: Int
    ) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.search(
            auth, query, latitude.toString(), filterBy, sortBy, longitude.toString(),
            type.toString()
        )
        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        val arraySubCategory = ArrayList<SubCategoryModel>()
                        if (status == "true") {
                            val data = json.getJSONObject("data")
                            val search = data.getJSONArray("search")
                            for (i in 0 until search.length()) {
                                val dataSearch = search.getJSONObject(i)
                                val modelSubCategory = SubCategoryModel()
                                modelSubCategory.subscription_id = dataSearch.optString("subscription_id")
                                modelSubCategory.campaign_id = dataSearch.optString("campaign_id")
                                modelSubCategory.price = dataSearch.optString("price")
                                modelSubCategory.discount_price = dataSearch.optString("discount_price")
                                modelSubCategory.unit = dataSearch.optString("unit")
                                modelSubCategory.created_at = dataSearch.optString("created_at")
                                modelSubCategory.updated_at = dataSearch.optString("updated_at")
                                modelSubCategory.frequency_type = dataSearch.optString("frequency_type")
                                modelSubCategory.introductory_price = dataSearch.optString("introductory_price")
                                modelSubCategory.frequency_value = dataSearch.optString("frequency_value")
                                modelSubCategory.campaign_image = dataSearch.optString("campaign_image")
                                modelSubCategory.campaign_name = dataSearch.optString("campaign_name")
                                modelSubCategory.subscriber = dataSearch.optString("subscriber")
                                modelSubCategory.location_id = dataSearch.optString("location_id")
                                modelSubCategory.description = dataSearch.optString("description")
                                modelSubCategory.business_logo = dataSearch.optString("business_logo")
                                modelSubCategory.like = dataSearch.optInt("like")
                                modelSubCategory.like_percentage = dataSearch.optString("like_percentage")
                                modelSubCategory.views = dataSearch.optString("views")
                                modelSubCategory.business_logo = dataSearch.optString("business_logo")
                                modelSubCategory.free_trial = dataSearch.optString("free_trial")
                                modelSubCategory.tags = dataSearch.optString("tags")
                                modelSubCategory.type = dataSearch.optInt("type")
                                modelSubCategory.bought = dataSearch.optString("bought")
                                modelSubCategory.location = dataSearch.optJSONArray("location").toString()
                                modelSubCategory.status = status
                                modelSubCategory.msg = msg
                                arraySubCategory.add(modelSubCategory)
                            }
                            mDataSearchData.value = arraySubCategory
                        } else {
                            modelStatus.status = "false"
                            mDataFailure.value = modelStatus
                        }


                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus

            }
        })

    }

    fun getmDataSearch(): LiveData<ArrayList<SubCategoryModel>> {
        return mDataSearchData
    }

    fun getFavourite(auth: String, subCategoryModel: SubCategoryModel) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()
        var likeOrUnlike = 1
        if (subCategoryModel.like.equals(1)) {
            likeOrUnlike = 2
        } else {
            likeOrUnlike = 1
        }
        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.setFavorite(auth, subCategoryModel.campaign_id, likeOrUnlike.toString())
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        if (status == "true") {
                            subCategoryModel.status = status
                            subCategoryModel.msg = msg
                            var arrayList : ArrayList<String> = ArrayList()
                            val mPrefs = application.getSharedPreferences(
                                "data",
                                AppCompatActivity.MODE_PRIVATE
                            )
                            val gson = Gson()
                            val getFav = mPrefs.getString("favData", "")
                            if (getFav != "") {
                                arrayList = gson.fromJson<ArrayList<String>>(
                                    getFav,
                                    object : TypeToken<ArrayList<String>>() {
                                    }.type
                                )
                                var exist = 0
                                for (item in arrayList) {
                                    if (subCategoryModel.campaign_id == item) {
                                        exist = 1
                                        break
                                    }
                                }
                                if (exist == 0) {
                                    arrayList.add(subCategoryModel.campaign_id)
                                }else{
                                    if(msg.contains("Favorite removed successfully.")){
                                        arrayList.remove(subCategoryModel.campaign_id)
                                    }
                                }
                            } else {
                                arrayList.add(subCategoryModel.campaign_id)
                            }

                            val prefsEditor = mPrefs.edit()
                            val json = gson.toJson(arrayList)
                            prefsEditor.putString("favData", json)
                            prefsEditor.apply()
                            Log.e("favData", json)
                            if (subCategoryModel.like == 1) {
                                subCategoryModel.like = 2
                            } else {
                                subCategoryModel.like = 1
                            }
                            mDataFavourite.value = subCategoryModel
                        } else {
                            subCategoryModel.status = status
                            modelStatus.msg = msg
                            mDataFailure.value = modelStatus
                        }

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus

            }

        })

    }

    fun getmDataFav(): LiveData<SubCategoryModel> {
        return mDataFavourite
    }
}