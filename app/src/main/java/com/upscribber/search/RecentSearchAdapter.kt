package com.upscribber.search

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.R
import kotlinx.android.synthetic.main.search_fragment_recycler.view.*

class RecentSearchAdapter(
    private val mContext: Context,
    private val list: ArrayList<RecentSearchModel>,
    searchFragment: Fragment
) :
    RecyclerView.Adapter<RecentSearchAdapter.MyViewHolder>() {

    val listener = searchFragment as Selection

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(mContext)
            .inflate(R.layout.search_fragment_recycler, parent, false)
        return MyViewHolder(v)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = list[position]

        holder.itemView.tvSearchText.setOnClickListener {
            listener.getPositionSelected(model.searchText,position)

        }
        holder.itemView.ivSearchLogo.setImageResource(model.searchLogo)
        holder.itemView.tvSearchText.text = model.searchText


    }


    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    interface Selection {
        fun getPositionSelected(name : String,position: Int)
    }

}
