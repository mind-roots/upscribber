package com.upscribber.search

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by Loveleen on 5/3/19.
 */
class TopCategoriesSearchModel() : Parcelable{

    var banner: String = ""
    var id: String = ""
    var unselected_image: String = ""
    var msg: String = ""
    var status: String = ""
    var selected_image: String = ""
    var subscription_count: String = ""
    var name: String = ""

    constructor(parcel: Parcel) : this() {
        banner = parcel.readString()
        id = parcel.readString()
        unselected_image = parcel.readString()
        msg = parcel.readString()
        status = parcel.readString()
        selected_image = parcel.readString()
        subscription_count = parcel.readString()
        name = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(banner)
        parcel.writeString(id)
        parcel.writeString(unselected_image)
        parcel.writeString(msg)
        parcel.writeString(status)
        parcel.writeString(selected_image)
        parcel.writeString(subscription_count)
        parcel.writeString(name)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<TopCategoriesSearchModel> {
        override fun createFromParcel(parcel: Parcel): TopCategoriesSearchModel {
            return TopCategoriesSearchModel(parcel)
        }

        override fun newArray(size: Int): Array<TopCategoriesSearchModel?> {
            return arrayOfNulls(size)
        }
    }


}