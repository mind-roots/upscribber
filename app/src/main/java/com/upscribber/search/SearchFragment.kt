package com.upscribber.search

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.commonClasses.Constant
import com.upscribber.categories.mainCategories.CategoryActivity
import com.upscribber.categories.subCategories.SubCategoriesActivity
import com.upscribber.dashboard.FilterHomeInterface
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.upscribber.commonClasses.OnMyScrollChangeListener
import com.upscribber.R
import com.upscribber.categories.subCategories.SubCategoryModel
import com.upscribber.filters.ModelData
import com.upscribber.home.WhatsNewModelOne
import com.upscribber.subsciptions.merchantStore.merchantDetails.ModeTags
import kotlin.collections.ArrayList

@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS", "NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class SearchFragment : Fragment(), OnMyScrollChangeListener, RecentSearchAdapter.Selection {

    private var arraySearchList: java.util.ArrayList<SubCategoryModel> = ArrayList()
    lateinit var mRvRecentSearch: RecyclerView
    lateinit var tvRecentSearch: TextView
    lateinit var rvTag: ChipGroup
    lateinit var textView19: TextView
    lateinit var tvTags: TextView
    lateinit var btnFilter: TextView
    lateinit var tvTopCategories: TextView
    lateinit var mChip: Chip
    private lateinit var searchView: SearchView
    lateinit var mRvTopCategories: RecyclerView
    lateinit var update: FilterHomeInterface
    var array_name = ArrayList<String>()
    lateinit var mViewModel: SearchViewModel
    lateinit var progressBar17: LinearLayout
    lateinit var chipsLinear: LinearLayout
    lateinit var mAdapterCategories: TopCategoryAdapter
    var isTyping = false
    var searchValue = ""
    val arrayList: ArrayList<RecentSearchModel> = ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_search, container, false)
        mViewModel = ViewModelProviders.of(this)[SearchViewModel::class.java]
        initz(view)
        setAdapter()
        setAdapterSearch()
        chipData()
        click()
        apiImplimentation()
        observerInit()
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val prefs = activity!!.getSharedPreferences("data", AppCompatActivity.MODE_PRIVATE)
        val searchInfo = prefs.getString("searchQuery", "")
        if (searchInfo.isEmpty() || searchInfo == "[]") {
            tvRecentSearch.visibility = View.GONE
        } else {
            tvRecentSearch.visibility = View.VISIBLE
        }


        val categoryName = Constant.getPrefs(activity!!).getString(Constant.tags, "")
        if (categoryName.isEmpty() || categoryName == "[]") {
            tvTags.visibility = View.GONE
            chipsLinear.visibility = View.GONE
        } else {
            tvTags.visibility = View.VISIBLE
            chipsLinear.visibility = View.VISIBLE
        }
    }


    private fun observerInit() {
        mViewModel.getStatus().observe(this, Observer {
            if (it.msg == "Invalid auth code") {
                Constant.commonAlert(activity!!)
            } else {
                Toast.makeText(activity, it.msg, Toast.LENGTH_SHORT).show()
            }
        })

        mViewModel.getmDataCategories().observe(this, Observer {
            if (it.size > 0) {
                progressBar17.visibility = View.GONE
                mAdapterCategories.update(it)
            }
        })

    }

    private fun initz(view: View) {
        mRvRecentSearch = view.findViewById(R.id.rvRecentSearch)
        tvRecentSearch = view.findViewById(R.id.tvRecentSearch)
        mRvTopCategories = view.findViewById(R.id.rvTopCategories)
        textView19 = view.findViewById(R.id.textView19)
        mChip = view.findViewById(R.id.chip)
        rvTag = view.findViewById(R.id.rvTags)
        tvTags = view.findViewById(R.id.tvTags)
        chipsLinear = view.findViewById(R.id.chipsLinear)
        tvTopCategories = view.findViewById(R.id.tvTopCategories)
        searchView = view.findViewById(R.id.searchView)
        btnFilter = view.findViewById(R.id.btnFilter)
        progressBar17 = view.findViewById(R.id.progressBar17)


    }

    private fun apiImplimentation() {
        val auth = Constant.getPrefs(activity!!).getString(Constant.auth_code, "")
        mViewModel.getTopFiveCategories(auth)
        progressBar17.visibility = View.VISIBLE

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        update = context as FilterHomeInterface
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        update = activity as FilterHomeInterface
    }

    private fun click() {

        textView19.setOnClickListener {
            val intent = Intent(context, CategoryActivity::class.java)
            startActivity(intent)
        }

        btnFilter.setOnClickListener {
            Constant.hideKeyboard(activity!!, searchView)
            if (searchView != null) {
                searchView.setQuery("", false)
                searchView.clearFocus()
            }
            update.openFilter(1)
        }

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {


            override fun onQueryTextSubmit(query: String): Boolean {


                val prefs = activity!!.getSharedPreferences("data", AppCompatActivity.MODE_PRIVATE)
                val searchInfo = prefs.getString("searchQuery", "")
                var type = ArrayList<RecentSearchModel>()
                if (searchInfo.isNotEmpty()) {
                    val gson = Gson()

                    val mPrefsLocation = activity!!.getSharedPreferences("data", AppCompatActivity.MODE_PRIVATE)
                    val prefsEditor = mPrefsLocation.edit()
                    prefsEditor.putString("filterData", "")
                    prefsEditor.apply()

                    type = gson.fromJson(
                        searchInfo,
                        object : TypeToken<List<RecentSearchModel>>() {

                        }.type
                    )
                    val recentSearchModel = RecentSearchModel()
                    recentSearchModel.searchLogo = R.drawable.ic_search_fragment
                    recentSearchModel.searchText = query
                    if (type.size > 0) {

                        type.add(0, recentSearchModel)

                    }
                    if (type.size > 3) {
                        type.removeAt(type.size - 1)
                    }

                    val mPrefs = activity!!.getSharedPreferences("data", AppCompatActivity.MODE_PRIVATE)
                    val json = gson.toJson(type)
                    prefsEditor.putString("searchQuery", json)
                    prefsEditor.apply()

                } else {
                    val recentSearchModel = RecentSearchModel()
                    recentSearchModel.searchLogo = R.drawable.ic_search_fragment
                    recentSearchModel.searchText = query
                    arrayList.add(recentSearchModel)
                    val mPrefs = activity!!.getSharedPreferences("data", AppCompatActivity.MODE_PRIVATE)
                    val prefsEditor = mPrefs.edit()
                    val gson = Gson()
                    val json = gson.toJson(arrayList)
                    prefsEditor.putString("searchQuery", json)
                    prefsEditor.apply()
                }



                startActivity(
                    Intent(activity, SubCategoriesActivity::class.java)
                        .putExtra("model", query).putExtra("typingQuery", query)
                )
                return false
            }

            override fun onQueryTextChange(changedText: String): Boolean {


//                val lastEditTime = System.currentTimeMillis()
//
//                if (changedText.isNotEmpty()) {
//                    searchValue = changedText
////                    if (!isTyping) {
////                        Log.d("typing", "started typing");
////                        // Send notification for start typing event
////                        isTyping = true
////                    }
//
//                        Handler().postDelayed({
//                            if ((System.currentTimeMillis() - lastEditTime) > 1500) {
//
//                                //isTyping = false
//                                Log.d("typing", "stopped typing")
//                                callActivity()
//                            }
//                        }, 1500)
//
//
//                }
                return false
            }


        })


    }


    fun callActivity() {

        startActivity(
            Intent(activity, SubCategoriesActivity::class.java)
                .putExtra("model", searchValue).putExtra(
                    "typingQuery",
                    searchValue
                ).setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
        )


    }

    override fun onResume() {
        super.onResume()
        Constant.hideKeyboard(activity!!, searchView)
//        apiImplimentation()
        val searchFrag = Constant.getSharedPrefs(activity!!).getBoolean("dataSearch", false)
        if (searchFrag) {
            mRvRecentSearch.visibility = View.VISIBLE
            mRvTopCategories.visibility = View.GONE
            rvTag.visibility = View.GONE
            textView19.visibility = View.GONE
            tvTags.visibility = View.GONE
            chipsLinear.visibility = View.GONE
            tvTopCategories.visibility = View.GONE

        } else {
            mRvRecentSearch.visibility = View.VISIBLE
            mRvTopCategories.visibility = View.VISIBLE
            rvTag.visibility = View.VISIBLE
            textView19.visibility = View.VISIBLE
            val categoryName = Constant.getPrefs(activity!!).getString(Constant.tags, "")
            if (categoryName.isEmpty() || categoryName == "[]") {
                tvTags.visibility = View.GONE
                chipsLinear.visibility = View.GONE
            } else {
                tvTags.visibility = View.VISIBLE
                chipsLinear.visibility = View.VISIBLE
            }
            tvTopCategories.visibility = View.VISIBLE
        }

        Constant.getSharedPrefs(activity!!).edit().putBoolean("dataSearch", false).apply()
    }


    override fun onScrollUp() {
        searchView.visibility = View.VISIBLE

    }

    override fun onScrollDown() {
        searchView.visibility = View.GONE
    }


    override fun onStop() {
        super.onStop()
        Constant.hideKeyboard(activity!!, searchView)
    }

    @SuppressLint("SetTextI18n", "ResourceType")
    private fun chipData() {
        val categoryName = Constant.getPrefs(activity!!).getString(Constant.tags, "")
        var type = ArrayList<ModeTags>()
        if (categoryName.isNotEmpty()) {
            val gson = Gson()
            val json = Constant.getPrefs(activity!!).getString(Constant.tags, null)
            type = gson.fromJson<ArrayList<ModeTags>>(
                json,
                object : TypeToken<List<ModeTags>>() {

                }.type
            )
        }

        //type.reverse()

        for (index in type.indices) {
            if (index < 5) {
                val chip = Chip(rvTag.context)
                chip.text = "${type[index].parent_name}"

                // necessary to get single selection working
                chip.isClickable = true
                chip.isCheckable = false
                chip.setTextAppearanceResource(R.style.ChipTextStyle_Selected)
                chip.setChipBackgroundColorResource(R.color.colorSkip)
//            chip.chipStartPadding = 10.0f
                chip.chipEndPadding = 20.0f
                chip.gravity = Gravity.CENTER
                chip.setOnClickListener {
                    startActivity(
                        Intent(this.activity, SubCategoriesActivity::class.java)
                            .putExtra("model", type[index].parent_name).putExtra("chips", type[index].parent_id)
                    )
                }
                rvTag.addView(chip)
            }
        }

        rvTag.isSingleSelection = true
    }


    fun setAdapter() {

        mRvTopCategories.layoutManager = LinearLayoutManager(activity, RecyclerView.HORIZONTAL, false)
        mAdapterCategories = TopCategoryAdapter(this.activity!!)
        mRvTopCategories.adapter = mAdapterCategories

    }

    fun setAdapterSearch() {
        mRvRecentSearch.layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
        mRvRecentSearch.adapter = RecentSearchAdapter(this.activity!!, getList(), this)

    }

    private fun getList(): ArrayList<RecentSearchModel> {


        /* val arrayList: ArrayList<RecentSearchModel> = ArrayList()

         var recentSearchModel = RecentSearchModel()
         recentSearchModel.searchLogo = R.drawable.ic_search_fragment
         recentSearchModel.searchText = "Mars Spa"
         arrayList.add(recentSearchModel)

         recentSearchModel = RecentSearchModel()
         recentSearchModel.searchLogo = R.drawable.ic_search_fragment
         recentSearchModel.searchText = "Mars Spa"
         arrayList.add(recentSearchModel)

         recentSearchModel = RecentSearchModel()
         recentSearchModel.searchLogo = R.drawable.ic_search_fragment
         recentSearchModel.searchText = "Mars Spa"
         arrayList.add(recentSearchModel)
 */


        val prefs = activity!!.getSharedPreferences("data", AppCompatActivity.MODE_PRIVATE)
        val searchInfo = prefs.getString("searchQuery", "")
        var type = ArrayList<RecentSearchModel>()
        if (searchInfo.isNotEmpty()) {
            val gson = Gson()
            type = gson.fromJson(
                searchInfo,
                object : TypeToken<List<RecentSearchModel>>() {

                }.type
            )

            if (type.size > 3) {
                type.removeAt(type.size - 1)
            }
//            if (type.size >= 1) {
//                for (i in 0 until type.size) {
//                    if (i >= 3) {
//                        type.removeAt(i)
//                    } else {
//                        continue
//                    }
//                }
//                return type
//            }

        }
        return type


    }

    override fun getPositionSelected(name: String, position: Int) {
        //searchView.setQuery(name, false)

        startActivity(
            Intent(activity, SubCategoriesActivity::class.java)
                .putExtra("model", name).putExtra(
                    "typingQuery",
                    name
                ).setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
        )
    }
}