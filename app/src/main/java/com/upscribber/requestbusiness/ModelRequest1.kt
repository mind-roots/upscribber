package com.upscribber.requestbusiness

import android.os.Parcel
import android.os.Parcelable
import android.widget.ImageView
import android.widget.TextView

/**
 * Created by amrit on 14/3/19.
 */
class ModelRequest1() : Parcelable {

    var imgRequest: Int = 0
    lateinit var nameRequest: String
    lateinit var addressRequest: String
    var isSelected = false
    var id = 0

    constructor(parcel: Parcel) : this() {
        imgRequest = parcel.readInt()
        nameRequest = parcel.readString()
        addressRequest = parcel.readString()
        isSelected = parcel.readByte() != 0.toByte()
        id = parcel.readInt()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(imgRequest)
        parcel.writeString(nameRequest)
        parcel.writeString(addressRequest)
        parcel.writeByte(if (isSelected) 1 else 0)
        parcel.writeInt(id)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ModelRequest1> {
        override fun createFromParcel(parcel: Parcel): ModelRequest1 {
            return ModelRequest1(parcel)
        }

        override fun newArray(size: Int): Array<ModelRequest1?> {
            return arrayOfNulls(size)
        }
    }

}