package com.upscribber.requestbusiness

import android.content.Context
import android.view.View
import androidx.viewpager.widget.PagerAdapter
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import com.upscribber.R
import java.util.ArrayList

class ConfirmationPagerAdapter(
    var context: Context,
    var contextt: RequestSuccessActivity,
    var mData: ArrayList<ModelRequest1>
) :
    PagerAdapter() {

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun getCount(): Int {
        return mData.size
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val itemView = LayoutInflater.from(container.context)
            .inflate(R.layout.confirmation_add_business, container, false)

        val model = mData[position]
        val storeName = itemView.findViewById(R.id.storeName) as TextView
        val address = itemView.findViewById(R.id.address) as TextView

        storeName.text = model.nameRequest
        address.text = model.addressRequest



        container.addView(itemView)
        return itemView
    }


    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View?)
    }


}