package com.upscribber.requestbusiness

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import kotlinx.android.synthetic.main.request_layout.view.*

/**
 * Created by amrit on 14/3/19.
 */
class AdapterRequest1(
    private val mContext: Context,
    listen: BusinessSelectListener
) : RecyclerView.Adapter<AdapterRequest1.MyViewHolder>() {


    var items = ArrayList<ModelRequest1>()

    var selectListener: BusinessSelectListener = listen

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(mContext)
            .inflate(R.layout.request_layout, parent, false)

        return MyViewHolder(v)

    }

    override fun getItemCount(): Int {
        return items.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = items[position]
        // holder.itemView.imgRequest.setImageResource(model.imgRequest)
        holder.itemView.addressRequestt.text = model.addressRequest
        holder.itemView.nameRequestt.text = model.nameRequest
        val isSeller = Constant.getSharedPrefs(mContext).getBoolean(Constant.IS_SELLER, false)


        if (model.isSelected) {
            holder.itemView.check.visibility = View.VISIBLE
            if(isSeller){
                holder.itemView.adapLayout.background = mContext.getDrawable(R.drawable.bg_seller_blue_outline)
                holder.itemView.check.setImageResource(R.drawable.ic_check_blue)
            }else{
                holder.itemView.adapLayout.background = mContext.getDrawable(R.drawable.signup_invite_code)
                holder.itemView.check.setImageResource(R.drawable.ic_check_plan)
            }
        } else {
            holder.itemView.check.visibility = View.INVISIBLE
            holder.itemView.adapLayout.setBackgroundResource(0)
        }

        holder.itemView.setOnClickListener {
            selectListener.onBusinessSelect(position, items[position].isSelected,items)
        }
    }

    fun updateArray(position: Int) {
        val selectedModel = items[position]
        items[position].isSelected = !selectedModel.isSelected

        notifyDataSetChanged()
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    fun update(list: ArrayList<ModelRequest1>) {
        items = list
        notifyDataSetChanged()
    }
}