package com.upscribber.requestbusiness

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.commonClasses.RequestRoundedBottomSheetDialogFragment
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import org.json.JSONArray
import org.json.JSONObject


class RequestBusinessFragment1 : Fragment(), SelectionAdapter.removeFromArray {

    lateinit var update: RequestInterface
    lateinit var mbackDash: ImageView
    lateinit var selectedModel: ArrayList<ModelRequest1>
    lateinit var constraintLayout7: ConstraintLayout
    lateinit var request: TextView
    lateinit var textAdding: TextView
    lateinit var textSelected: TextView
    lateinit var textDescription: TextView
    lateinit var add: TextView
    private lateinit var recyclerRequest: RecyclerView
    private lateinit var mAdapter: SelectionAdapter
    private lateinit var requestBusinessViewModel: RequestBusinessViewModel


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_request_business1, container, false)
        requestBusinessViewModel = ViewModelProviders.of(this)[RequestBusinessViewModel::class.java]

        mbackDash = view.findViewById(R.id.backDash)
        recyclerRequest = view.findViewById(R.id.recyclerRequest)
        constraintLayout7 = view.findViewById(R.id.constraintLayout7)
        request = view.findViewById(R.id.request)
        textAdding = view.findViewById(R.id.textAdding)
        textSelected = view.findViewById(R.id.textSelected)
        textDescription = view.findViewById(R.id.textDescription)
        add = view.findViewById(R.id.add)

        val isSeller = Constant.getSharedPrefs(activity!!).getBoolean(Constant.IS_SELLER, false)
        if (isSeller){
            add.setBackgroundResource(R.drawable.button_blue_padding)
            request.setBackgroundResource(R.drawable.button_blue_padding)
        }else{
            add.setBackgroundResource(R.drawable.button_pink_padding1)
            request.setBackgroundResource(R.drawable.button_pink_padding1)
        }


        clickEvents()
        setAdapter()
        return view
    }

    private fun setAdapter() {
        recyclerRequest.layoutManager = LinearLayoutManager(activity)
        mAdapter = SelectionAdapter(this.activity!!, this)
        recyclerRequest.adapter = mAdapter
    }

    private fun clickEvents() {
        mbackDash.setOnClickListener {
            update.goBack(1)
            textAdding.visibility = View.VISIBLE
            textDescription.visibility = View.VISIBLE
            constraintLayout7.visibility = View.GONE
            textSelected.visibility = View.GONE
            add.visibility = View.VISIBLE
            request.visibility = View.GONE

        }

        add.setOnClickListener {
            val intent = Intent(activity, SearchBusinessActivity::class.java)
            startActivityForResult(intent, 1)
        }


        request.setOnClickListener {
            if (selectedModel.size > 0) {
                val jsonArray = JSONArray()
                for (items in selectedModel) {
                    val jsonObject = JSONObject()
                    jsonObject.put("name", items.nameRequest)
                    jsonObject.put("address", items.addressRequest)
                    jsonArray.put(jsonObject)
                }

                requestBusinessViewModel.setBusinessRequest(jsonArray.toString())


            } else {
                Toast.makeText(activity, "Sorry, No business is selected", Toast.LENGTH_SHORT).show()
            }
        }

        requestBusinessViewModel.getBusinessRequest().observe(this, Observer {
            startActivity(
                Intent(activity, RequestSuccessActivity::class.java).putParcelableArrayListExtra(
                    "modelData",
                    selectedModel
                )
            )
        })


    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        update = context as RequestInterface
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        update = activity as RequestInterface
    }


    class MenuFragment : RequestRoundedBottomSheetDialogFragment() {

        private lateinit var recyclerRequest: RecyclerView
        lateinit var update: RequestInterface


        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
            val view = inflater.inflate(R.layout.request_business_sheet, container, false)
            recyclerRequest = view.findViewById(R.id.recyclerRequest)
            setLayout()
            setAdapter()
            return view
        }


        private fun setLayout() {
            val manager3 = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
            recyclerRequest.layoutManager = manager3
            recyclerRequest.itemAnimator = DefaultItemAnimator()
            recyclerRequest.isNestedScrollingEnabled = true
        }

        private fun setAdapter() {
            recyclerRequest.layoutManager = LinearLayoutManager(activity)

        }

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
            selectedModel = data!!.getParcelableArrayListExtra<ModelRequest1>("model")
            constraintLayout7.visibility = View.VISIBLE
            mAdapter.update(selectedModel)
            add.visibility = View.GONE
            textAdding.visibility = View.GONE
            textDescription.visibility = View.GONE
            textSelected.visibility = View.VISIBLE
            request.visibility = View.VISIBLE
        }
    }

    override fun remove(position: Int, list: ArrayList<ModelRequest1>) {

        if (list.size > 0) {
            selectedModel = list
        } else {
            selectedModel = list
            constraintLayout7.visibility = View.GONE
            add.visibility = View.VISIBLE
            textAdding.visibility = View.VISIBLE
            textDescription.visibility = View.VISIBLE
            textSelected.visibility = View.GONE
            request.visibility = View.GONE
        }
    }
}