package com.upscribber.requestbusiness

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData

class RequestBusinessViewModel(application: Application) : AndroidViewModel(application) {

    var requestBusiness: RequestBusinessRepository = RequestBusinessRepository(application)

    fun setBusinessRequest(obj: String) {
        requestBusiness.sendBusinessRequest(obj)
    }

    fun getBusinessRequest(): LiveData<String> {
        return requestBusiness.requestBusinessResponse()
    }

}