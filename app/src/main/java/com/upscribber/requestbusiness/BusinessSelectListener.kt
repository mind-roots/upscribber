package com.upscribber.requestbusiness

interface BusinessSelectListener {

    fun onBusinessSelect(
        position: Int,
        isSelected: Boolean,
        items: ArrayList<ModelRequest1>
    )
}