package com.upscribber.requestbusiness

/**
 * Created by amrit on 14/3/19.
 */
interface RequestInterface {

    fun goNext(i: Int)
    fun goBack(i: Int)
    fun setSelectedBusiness(selectedModel: ArrayList<ModelRequest1>)
}