package com.upscribber.requestbusiness

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.TypedValue
import android.widget.TextView
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.home.ZoomOutPageTransformer
import kotlinx.android.synthetic.main.activity_request_success.*
import me.relex.circleindicator.Config

class RequestSuccessActivity : AppCompatActivity() {

    lateinit var textDone: TextView
    var myCustomPagerAdapter: ConfirmationPagerAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_request_success)
        intz()
        clickListeners()
        setAdapter()


    }

    private fun setAdapter() {
        if (intent.hasExtra("modelData")) {
            val selection = intent.getParcelableArrayListExtra<ModelRequest1>("modelData")
            myCustomPagerAdapter = ConfirmationPagerAdapter(this, this, selection)
            viewPager.adapter = myCustomPagerAdapter
            viewPager.setPageTransformer(true, ZoomOutPageTransformer())
            val indicatorWidth = (TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, 10f,
                resources.displayMetrics
            ) + 0.5f).toInt()
            val indicatorHeight = (TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, 4f,
                resources.displayMetrics
            ) + 0.5f).toInt()
            val indicatorMargin = (TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, 4f,
                resources.displayMetrics
            ) + 0.5f).toInt()


            val isSeller = Constant.getSharedPrefs(this).getBoolean(Constant.IS_SELLER, false)
            if (isSeller) {
                imageView48.setImageResource(R.drawable.bg_almost_there)
                val config = Config.Builder().width(indicatorWidth)
                    .height(indicatorHeight)
                    .margin(indicatorMargin)
                    .drawable(R.drawable.button_blue_padding)
                    .drawableUnselected(R.drawable.bg_grey)
                    .build()
                indicator_login.initialize(config)
            }else{
                imageView48.setImageResource(R.drawable.side_nav_bar)
                val config = Config.Builder().width(indicatorWidth)
                    .height(indicatorHeight)
                    .margin(indicatorMargin)
                    .drawable(R.drawable.black_radius_square)
                    .drawableUnselected(R.drawable.bg_grey)
                    .build()
                indicator_login.initialize(config)
            }

            viewPager!!.adapter = ConfirmationPagerAdapter(this, this,selection)
            indicator_login.setViewPager(viewPager)
        }

    }

    private fun clickListeners() {
        textDone.setOnClickListener {
            val sharedPreferences = getSharedPreferences("customers", Context.MODE_PRIVATE)
            val editor = sharedPreferences.edit()
            editor.putString("customer", "back")
            editor.apply()
            finish()
        }

    }

    private fun intz() {
        textDone = findViewById(R.id.textDone)
    }
}
