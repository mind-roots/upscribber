package com.upscribber.requestbusiness

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.upscribber.R

class SearchBusinessActivity : AppCompatActivity(), RequestInterface {

    private lateinit var fragmentManager: FragmentManager
    private lateinit var fragmentTransaction: FragmentTransaction

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_business)
        fragmentManager = supportFragmentManager
        this.fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.frameBusiness, RequestBusinessFragment2())
        fragmentTransaction.commit()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun goNext(i: Int) {

    }

    override fun goBack(i: Int) {
        finish()
    }

    override fun setSelectedBusiness(selectedModel: ArrayList<ModelRequest1>) {
        val intent = Intent()
        intent.putParcelableArrayListExtra("model", selectedModel)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }
}
