package com.upscribber.requestbusiness

import android.content.Context
import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.upscribber.R

class RequestBusiness : AppCompatActivity(), RequestInterface {
    private lateinit var fragmentManager: FragmentManager
    private lateinit var fragmentTransaction: FragmentTransaction

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_request_business)

        fragmentManager = supportFragmentManager
        this.fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.frameBusiness, RequestBusinessFragment1())
        fragmentTransaction.commit()

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onResume() {
        super.onResume()
        val sharedPreferences1 = getSharedPreferences("customers", Context.MODE_PRIVATE)
        if (!sharedPreferences1.getString("customer", "").isEmpty()) {
            finish()
        }
        val editor = sharedPreferences1.edit()
        editor.remove("customer")
        editor.apply()
    }

    override fun goBack(i: Int) {
        finish()
       /* when (i) {

            3 -> {
                fragmentManager = supportFragmentManager
                this.fragmentTransaction = fragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.frameBusiness, RequestBusinessFragment2())
                fragmentTransaction.commit()
            }
        }*/
    }


    override fun goNext(i: Int) {

       when (i) {

            3 -> {
                finish()
            }

        }
    }


    override fun setSelectedBusiness(selectedModel: ArrayList<ModelRequest1>) {

    }

}
