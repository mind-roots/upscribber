package com.upscribber.requestbusiness

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.R
import kotlinx.android.synthetic.main.business_selection_adapter.view.*

class SelectionAdapter(
    private val mContext: Context,
    fragContext: RequestBusinessFragment1
) :
    RecyclerView.Adapter<SelectionAdapter.MyViewHolder>() {
    var items = ArrayList<ModelRequest1>()
    var listen = fragContext as removeFromArray


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(mContext)
            .inflate(R.layout.business_selection_adapter, parent, false)

        return MyViewHolder(v)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = items[position]
        holder.itemView.nameRequestt.text = model.nameRequest
        holder.itemView.addressRequestt.text = model.addressRequest

        holder.itemView.imageCross.setOnClickListener {
            items.removeAt(position)
            listen.remove(position, items)
            notifyDataSetChanged()
        }

    }


    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    fun update(list: ArrayList<ModelRequest1>) {
        items = list
    }

    interface removeFromArray {
        fun remove(position: Int, list: ArrayList<ModelRequest1>)
    }


}