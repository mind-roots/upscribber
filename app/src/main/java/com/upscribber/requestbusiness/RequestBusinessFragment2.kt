package com.upscribber.requestbusiness

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.location.Location
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import kotlinx.android.synthetic.main.fragment_request_business2.*
import java.util.HashMap


class RequestBusinessFragment2 : Fragment(), BusinessSelectListener, OnMapReadyCallback,
    GoogleMap.OnMarkerClickListener {

    private lateinit var recyclerRequest: RecyclerView
    lateinit var update: RequestInterface
    lateinit var mBackOne: ImageView
    private lateinit var coordinate: CoordinatorLayout
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var mMap: GoogleMap
    private lateinit var lastLocation: Location
    lateinit var brandSearch: SearchView
    lateinit var mBusinessDone: TextView
    var selectedModel = ArrayList<ModelRequest1>()
    private var mData = ArrayList<ModelRequest1>()
    private lateinit var mAdapter: AdapterRequest1
    private lateinit var bottom_sheet: LinearLayout
    private lateinit var showHideBottomSheet: RelativeLayout
    private lateinit var line: TextView
    private lateinit var downImage: ImageView
    private var bottomSheetBehavior: BottomSheetBehavior<LinearLayout>? = null
    var shofull = 0
    var exist = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_request_business2, container, false)
        recyclerRequest = view.findViewById(R.id.recyclerRequest)
        coordinate = view.findViewById(R.id.coordinate)
        mBackOne = view.findViewById(R.id.backOne)
        brandSearch = view.findViewById(R.id.brand_search)
        mBusinessDone = view.findViewById(R.id.businessDone)
        bottom_sheet = view.findViewById(R.id.bottom_sheet)
        line = view.findViewById(R.id.line)
        downImage = view.findViewById(R.id.downImage)
        showHideBottomSheet = view.findViewById(R.id.showHideBottomSheet)
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        val isSeller = Constant.getSharedPrefs(context!!).getBoolean(Constant.IS_SELLER, false)
        if (isSeller){
           mBusinessDone.setBackgroundResource(R.drawable.bg_blue_corners)
            }else{
            mBusinessDone.setBackgroundResource(R.drawable.otp_next)
        }


        setAdapter()
        clickEvents()
        clicksEvents1()
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this.activity!!)
        return view
    }

    private fun clicksEvents1() {

        bottomSheetBehavior = BottomSheetBehavior.from(bottom_sheet)
        showHideBottomSheet.setOnClickListener {
            if (shofull == 0) {
                bottomSheetBehavior!!.state = BottomSheetBehavior.STATE_EXPANDED
                line.visibility = View.GONE
                downImage.visibility = View.VISIBLE
                shofull = 1
            } else {
                shofull = 0
                line.visibility = View.VISIBLE
                downImage.visibility = View.GONE
                bottomSheetBehavior!!.state =
                    BottomSheetBehavior.STATE_COLLAPSED
            }
        }
        bottomSheetBehavior!!.setBottomSheetCallback(object :
            BottomSheetBehavior.BottomSheetCallback() {
            @SuppressLint("SwitchIntDef")
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                // React to state change
                when (newState) {
                    BottomSheetBehavior.STATE_COLLAPSED -> {
                        nestedScroll.scrollTo(0, 0)
                        line.visibility = View.VISIBLE
                        downImage.visibility = View.GONE
                        shofull = 0
                    }

                    BottomSheetBehavior.STATE_DRAGGING -> {

                    }

                    BottomSheetBehavior.STATE_EXPANDED -> {
                        shofull = 1
                        line.visibility = View.GONE
                        downImage.visibility = View.VISIBLE
                    }

                    BottomSheetBehavior.STATE_HIDDEN -> {

                    }
                    BottomSheetBehavior.STATE_SETTLING -> {

                    }


                }
            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {
                // React to dragging events
                Log.e("onSlide", "onSlide")
            }
        })
    }


    private fun clickEvents() {
        mBackOne.setOnClickListener {
            update.goBack(2)
        }

        mBusinessDone.setOnClickListener {
            update.setSelectedBusiness(selectedModel)

        }


        brandSearch.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                hideKeyboard()
                coordinate.visibility = View.VISIBLE

                val url = getUrl(lastLocation.latitude, lastLocation.longitude, query.trim())
                val getNearbyPlacesData = GetNearbyPlacesData()
                getNearbyPlacesData.execute(mMap, url)

                return true
            }

            override fun onQueryTextChange(newText: String): Boolean {
                return true
            }

        })
    }


    private fun setAdapter() {
        recyclerRequest.layoutManager = LinearLayoutManager(activity)
        mAdapter = AdapterRequest1(this.activity!!, this)
        recyclerRequest.adapter = mAdapter

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        update = context as RequestInterface
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        update = activity as RequestInterface
    }

    override fun onBusinessSelect(
        position: Int,
        isSelected: Boolean,
        items: ArrayList<ModelRequest1>
    ) {

        if (!isSelected) {
            exist++
            selectedModel.add(mData[position])
        } else {
            exist--
            selectedModel.remove(mData[position])
        }
        if (exist > 0){
            mBusinessDone.visibility = View.VISIBLE
        }else{
            mBusinessDone.visibility = View.GONE
        }

        mAdapter.updateArray(position)

    }


    fun hideKeyboard() {
        val imm = activity!!.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view!!.windowToken, 0)
    }


    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap.uiSettings.isCompassEnabled = true
        setUpMap()
    }

    override fun onMarkerClick(p0: Marker?): Boolean {
        mMap.uiSettings.isZoomControlsEnabled = true
        mMap.setOnMarkerClickListener(this)
        return true
    }

    companion object {
        const val LOCATION_PERMISSION_REQUEST_CODE = 1
    }

    private fun setUpMap() {
        if (ActivityCompat.checkSelfPermission(
                this.activity!!,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this.activity!!,
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION), LOCATION_PERMISSION_REQUEST_CODE
            )
            return
        }
        fusedLocationClient.lastLocation.addOnSuccessListener(this.activity!!) { location ->
            // Got last known location. In some rare situations this can be null.
            if (location != null) {
                lastLocation = location

                val currentLatLng = LatLng(location.latitude, location.longitude)
                placeMarkerOnMap(currentLatLng)
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 12f))
            }
        }
    }

    //    custom marker
    private fun placeMarkerOnMap(location: LatLng) {
        mMap.addMarker(
            MarkerOptions()
                .position(location)
                .icon(bitmapDescriptorFromVector(this.activity!!, R.drawable.ic_location_blue_icon))
        )

    }

    //custom marker using bitmap
    private fun bitmapDescriptorFromVector(context: Context, vectorResId: Int): BitmapDescriptor {
        val vectorDrawable = ContextCompat.getDrawable(context, vectorResId)
        vectorDrawable!!.setBounds(0, 0, vectorDrawable.intrinsicWidth, vectorDrawable.intrinsicHeight)
        val bitmap =
            Bitmap.createBitmap(vectorDrawable.intrinsicWidth, vectorDrawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        vectorDrawable.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }

    private fun getUrl(latitude: Double, longitude: Double, nearbyPlace: String): String {

        val googlePlacesUrl = StringBuilder("https://maps.googleapis.com/maps/api/place/autocomplete/json?")
        googlePlacesUrl.append("location=$latitude,$longitude")
        googlePlacesUrl.append("&radius=500")
        googlePlacesUrl.append("&type=establishment")
        googlePlacesUrl.append("&sensor=true")
        googlePlacesUrl.append("&key=" + "AIzaSyAhmLGsI8BIN2vd__rKtn86RawUTtcitcs")
        googlePlacesUrl.append("&input=${nearbyPlace.trim()}")

        Log.d("getUrl", googlePlacesUrl.toString())
        return googlePlacesUrl.toString()
    }

    @SuppressLint("StaticFieldLeak")
    private inner class GetNearbyPlacesData : AsyncTask<Any, String, String>() {
        internal var url: String = ""
        internal lateinit var googlePlacesData: String
        internal lateinit var mMap: GoogleMap
        override fun doInBackground(vararg params: Any?): String {
            try {
                Log.d("GetNearbyPlacesData", "doInBackground entered")
                mMap = params[0] as GoogleMap
                url = params[1] as String
                val downloadUrl = DownloadUrl()
                googlePlacesData = downloadUrl.readUrl(url)
                Log.d("GooglePlacesReadTask", "doInBackground Exit")
            } catch (e: Exception) {
                Log.d("GooglePlacesReadTask", e.toString())
            }

            return googlePlacesData
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            Log.d("GooglePlacesReadTask", "onPostExecute Entered")
            var nearbyPlacesList: List<HashMap<String, String>>? = null
            val dataParser = DataParser()
            nearbyPlacesList = dataParser.parse(result)
            ShowNearbyPlaces(nearbyPlacesList!!)
            Log.d("GooglePlacesReadTask", "onPostExecute Exit")
        }

        private fun ShowNearbyPlaces(nearbyPlacesList: List<HashMap<String, String>>) {

            if (mData.size > 0) {
                mData.clear()
            }


            for (i in nearbyPlacesList.indices) {
                Log.d("onPostExecute", "Entered into showing locations")
                val googlePlace = nearbyPlacesList[i]
                val placeName = googlePlace["place_name"]
                val vicinity = googlePlace["reference"]
                val modelRequest1 = ModelRequest1()
                if (placeName != null) {
                    modelRequest1.nameRequest = placeName
                }
                if (vicinity != null) {
                    modelRequest1.addressRequest = vicinity
                }
                mData.add(modelRequest1)
            }
            mAdapter.update(mData)


        }


    }
}