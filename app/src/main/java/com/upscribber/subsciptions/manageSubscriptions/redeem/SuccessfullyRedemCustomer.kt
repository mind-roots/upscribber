package com.upscribber.subsciptions.manageSubscriptions.redeem

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.bumptech.glide.Glide
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.subsciptions.manageSubscriptions.ModelRedeemCode
import kotlinx.android.synthetic.main.activity_successfully_redem_customer.*
import java.text.SimpleDateFormat
import java.util.*

class SuccessfullyRedemCustomer : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_successfully_redem_customer)
        setData()
        clickListeners()
    }

    private fun clickListeners() {
        buttonDone1.setOnClickListener {
            finish()
        }

    }

    @SuppressLint("SimpleDateFormat")
    private fun setData() {
        if (intent.hasExtra("model")) {
            val model = intent.getParcelableExtra<ModelRedeemCode>("model")
            if (model.rtype =="2" || model.rtype =="3"){
                imageView41.setBackgroundResource(R.mipmap.bg_reviews)
                textView138.visibility = View.GONE
                textView139.visibility = View.GONE
            }else{
                imageView41.setBackgroundResource(R.mipmap.ms_bg)
                textView138.visibility = View.VISIBLE
                textView139.visibility = View.VISIBLE
            }

            val imagePath = Constant.getPrefs(this).getString(Constant.dataImagePath, "")
            Glide.with(this).load(imagePath + model.logo).placeholder(R.mipmap.circular_placeholder)
                .into(circleImageView3)
            if (model.team_member.isEmpty()) {
                textView135.text = ""
            } else {
                textView135.text = model.team_member
            }

            val calendar = Calendar.getInstance()
            val dateFormat =  SimpleDateFormat("MM/dd/yyyy")
            val date = dateFormat.format(calendar.time)

            textSubscription.text = model.campaign_name
            textView297.text = model.business_name
            textView298.text = Constant.formatPhoneNumber(model.contact_no)
            textView141.text = "$" + model.total_amount
            textView143.text = model.remaining_visits + " " +  model.unit
            textView139.text = model.locations[0].street + "," + model.locations[0].city
            val time = Constant.getTime(model.created_at)
            textView145.text = date
            textView147.text = time


        }

    }
}
