package com.upscribber.subsciptions.manageSubscriptions.redeem

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.subsciptions.mySubscriptionDetails.MySubscriptionsModel
import kotlinx.android.synthetic.main.redeem_recycler_layout.view.*
import kotlinx.android.synthetic.main.redeem_recycler_layout.view.ivLogo
import kotlinx.android.synthetic.main.redeem_recycler_layout.view.tvFab
import kotlinx.android.synthetic.main.redeem_recycler_layout.view.tvRedeemCost

class AdapterRedeem
    (val mContext: Context) : RecyclerView.Adapter<AdapterRedeem.MyViewHolder>() {

    var dismissSheet = mContext as DismissSheet
    var list: ArrayList<MySubscriptionsModel> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(mContext)
            .inflate(R.layout.redeem_recycler_layout, parent, false)

        return MyViewHolder(v)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = list[position]
        val imagePath = Constant.getPrefs(mContext).getString(Constant.dataImagePath, "")
        Glide.with(mContext).load(imagePath + model.feature_image).into(holder.itemView.ivLogo)

        if (model.status2 == "2"){
            holder.itemView.tvRedeem.text ="Paused"
            holder.itemView.tvRedeem.isClickable = false
            holder.itemView.tvRedeem.isEnabled = false
            holder.itemView.tvRedeem.setTextColor(mContext.resources.getColor(R.color.white))
            holder.itemView.tvRedeem.setBackgroundResource(R.drawable.ms_bg_yellow_pause)
        }else if (model.status2 == "3"){
            holder.itemView.tvRedeem.text ="Canceled"
            holder.itemView.tvRedeem.isClickable = false
            holder.itemView.tvRedeem.isEnabled = false
            holder.itemView.tvRedeem.setTextColor(mContext.resources.getColor(R.color.white))
            holder.itemView.tvRedeem.setBackgroundResource(R.drawable.ms_bg_pink_cancelled)
        }else{
            holder.itemView.tvRedeem.text ="Redeem"
            holder.itemView.tvRedeem.isClickable = true
            holder.itemView.tvRedeem.isEnabled = true
            holder.itemView.tvRedeem.setTextColor(mContext.resources.getColor(R.color.colorHeading))
            holder.itemView.tvRedeem.setBackgroundResource(R.drawable.ms_bg_red_outline)
        }

        var total = 0
        var whatToDo = ""

        if (model.free_trial_qty.toInt() > 0) {
            total = model.free_trial_qty.toInt() - model.redeem_count.toInt()
            whatToDo = "freeTrial"
            if (total <= 0) {
                whatToDo = "redemption"
                total = model.redeemtion_cycle_qty.toInt() - model.redeem_count.toInt()
            }

        } else {
            whatToDo = ""
            total = model.redeemtion_cycle_qty.toInt() - model.redeem_count.toInt()
        }

        if (total.toString() > "100"){
            holder.itemView. tvLogoNum.text = mContext.resources.getString(R.string.infinity)
        }else {
            holder.itemView.tvLogoNum.text = total.toString()
        }

        holder.itemView.tvFab.text = model.campaign_name
        holder.itemView.tvRedeemCost.text = model.business_name

        if (model.redeemtion_cycle_qty > "100") {
            holder.itemView.remainingQuantity.text = "Remaining Quantity - Unlimited"
        } else {
            holder.itemView.remainingQuantity.text = "Remaining Quantity - $total Left"
        }


        if (total.toString() == "0") {
            holder.itemView.tvRedeem.alpha = 0.4f
        }

        holder.itemView.tvRedeem.setOnClickListener {
//            val getRedmeenCount = holder.itemView.amount.text.toString()
            val countToDisplay2 = (model.redeemtion_cycle_qty.toInt() - model.redeem_count.toInt()).toString()
            if (countToDisplay2 == "0") {
                Toast.makeText(
                    mContext,
                    "You have have enough Quantity to redeem",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                if (model.status2 == "3") {
                    AlertDialog.Builder(mContext)
                        .setMessage("Canceled subscription cannot be redeemed.")
                        .setPositiveButton("Ok") { dialogInterface, i ->
                            dialogInterface.dismiss()
                        }
                        .show()
                } else {
//                    if (getRedmeenCount == "0") {
//                        Toast.makeText(mContext, "Please select quantity", Toast.LENGTH_SHORT)
//                            .show()
//                    } else {
                        dismissSheet.OpenSheet(position, model)
//                    }
                }
            }
        }

        val isSeller = Constant.getSharedPrefs(mContext).getBoolean(Constant.IS_SELLER, false)
        if (isSeller) {
            holder.itemView.tvFab.maxLines = 2
        } else {
            holder.itemView.tvFab.maxLines = 1
        }

//        holder.itemView.add.setOnClickListener {
//
//            when (whatToDo) {
//                "freeTrial" -> {
//                    val countToDisplay =
//                        (model.free_trial_qty.toInt() - model.redeem_count.toInt()).toString()
//                    var count = holder.itemView.amount.text.toString().toInt()
//                    if (countToDisplay.toInt() > count) {
//                        count += 1
//                        holder.itemView.amount.text = count.toString()
//                        notifyDataSetChanged()
//                    }
//                }
//                "redemption" -> {
//                    val countToDisplay =
//                        (model.redeemtion_cycle_qty.toInt() - model.redeem_count.toInt()).toString()
//                    var count = holder.itemView.amount.text.toString().toInt()
//                    if (countToDisplay.toInt() > count) {
//                        count += 1
//                        holder.itemView.amount.text = count.toString()
//                        notifyDataSetChanged()
//                    }
//                }
//                else -> {
//                    val countToDisplay =
//                        (model.redeemtion_cycle_qty.toInt() - model.redeem_count.toInt()).toString()
//                    var count = holder.itemView.amount.text.toString().toInt()
//                    if (countToDisplay.toInt() > count) {
//                        count += 1
//                        holder.itemView.amount.text = count.toString()
//                        notifyDataSetChanged()
//                    }
//                }
//            }
//
//        }

//        holder.itemView.remove.setOnClickListener {
//            var count = holder.itemView.amount.text.toString().toInt()
//            if (count > 0) {
//                count -= 1
//                holder.itemView.amount.text = count.toString()
//                notifyDataSetChanged()
//
//            }
//
//        }

    }

    fun update(it: ArrayList<MySubscriptionsModel>) {
        list = it
        notifyDataSetChanged()

    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
    interface DismissSheet {
        fun OpenSheet(
            position: Int,
            model: MySubscriptionsModel
        )

    }
}