package com.upscribber.subsciptions.manageSubscriptions.cancelSubscription

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.upscribber.commonClasses.Constant
import com.upscribber.commonClasses.ModelStatusMsg
import com.upscribber.R
import com.upscribber.commonClasses.WebServicesCustomers
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class RepositoryCancelReason(application: Application) {

    fun getList(): LiveData<ArrayList<ModelCancelReasons>> {
        val mData = MutableLiveData<ArrayList<ModelCancelReasons>>()

        val arrayList = ArrayList<ModelCancelReasons>()

        val modelCancelReasons = ModelCancelReasons()
        modelCancelReasons.image = R.drawable.ic_costs_too_much
        modelCancelReasons.textReason = "Costs too much"
        modelCancelReasons.status = false
        arrayList.add(modelCancelReasons)

        val modelCancelReasons1 =
            ModelCancelReasons()
        modelCancelReasons1.image = R.drawable.ic_dont_use_service
        modelCancelReasons1.textReason = "I don't use this service enough"
        modelCancelReasons1.status = false
        arrayList.add(modelCancelReasons1)

        val modelCancelReasons2 =
            ModelCancelReasons()
        modelCancelReasons2.image = R.drawable.ic_better_plan
        modelCancelReasons2.textReason = "I found a better plan"
        modelCancelReasons2.status = false
        arrayList.add(modelCancelReasons2)

        val modelCancelReasons3 =
            ModelCancelReasons()
        modelCancelReasons3.image = R.drawable.ic_bad_service
        modelCancelReasons3.textReason = "Bad customer service"
        modelCancelReasons3.status = false
        arrayList.add(modelCancelReasons3)

        val modelCancelReasons4 =
            ModelCancelReasons()
        modelCancelReasons4.image = R.drawable.other_reason
        modelCancelReasons4.textReason = "Others"
        modelCancelReasons4.status = false
        arrayList.add(modelCancelReasons4)

        mData.value = arrayList
        return mData

    }





    val mDataFailure = MutableLiveData<ModelStatusMsg>()
    val mDataCancel = MutableLiveData<ArrayList<ModelCancelReasons>>()
    private val mDataCancelCost = MutableLiveData<ArrayList<ModelCancelReasons>>()

    fun getCancelSubscription(auth: String, subscriptionId: String, cancelReason: String,type:String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.cancelSubscription(auth, subscriptionId, cancelReason,type)
        call.enqueue(object : Callback<ResponseBody> {



            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        val model =
                            ModelCancelReasons()
                        val arrayList = ArrayList<ModelCancelReasons>()
                        if (status == "true") {
                            val data = json.optJSONObject("data")
                            model.frequency_type = data.optString("frequency_type")
                            model.redeemtion_cycle_qty = data.optString("redeemtion_cycle_qty")
                            model.unit = data.optString("unit")
                            model.total = data.optString("total")
                            model.cycle_start_date = data.optString("cycle_start_date")
                            model.campaign_name = data.optString("campaign_name")
                            model.status1 = status
                            model.msg = msg
                            model.type = type
                            modelStatus.status = status
                            modelStatus.msg = msg
                            arrayList.add(model)
                            mDataCancel.value = arrayList
                        } else {
                            modelStatus.status = "false"
                            modelStatus.msg = msg
                            mDataFailure.value = modelStatus
                        }

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus

            }
        })
    }


    fun getmDataStatus(): LiveData<ModelStatusMsg> {
        return mDataFailure
    }

    fun getmDataCancel(): LiveData<ArrayList<ModelCancelReasons>> {
        return mDataCancel
    }


    fun getCancelCostTooSubscription(
        auth: String,
        subscriptionId: String,
        cancelReason: String,
        type: String
    ) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.cancelSubscription(auth, subscriptionId,cancelReason,type)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        val model =
                            ModelCancelReasons()
                        val arrayList = ArrayList<ModelCancelReasons>()
                        if (status == "true") {
                            val data = json.optJSONObject("data")
                            model.frequency_type = data.optString("frequency_type")
                            model.redeemtion_cycle_qty = data.optString("redeemtion_cycle_qty")
                            model.unit = data.optString("unit")
                            model.total = data.optString("total")
                            model.cycle_start_date = data.optString("cycle_start_date")
                            model.campaign_name = data.optString("campaign_name")
                            model.status1 = status
                            model.msg = msg
                            modelStatus.status = status
                            modelStatus.msg = msg
                            arrayList.add(model)
                            mDataCancelCost.value = arrayList
                        }else {
                            modelStatus.status = "false"
                            modelStatus.msg = msg
                            mDataFailure.value = modelStatus
                        }

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg ="Network Error!"
                mDataFailure.value = modelStatus
            }
        })
    }


    fun getmDataCancelCost(): LiveData<ArrayList<ModelCancelReasons>> {
        return mDataCancelCost
    }

    fun getListReasonsOptionFour(): LiveData<java.util.ArrayList<ModelCancelReasons>> {
        val mData10 = MutableLiveData<ArrayList<ModelCancelReasons>>()

        val arrayList10 = ArrayList<ModelCancelReasons>()

        val modelCancelReasons1 =
            ModelCancelReasons()
        modelCancelReasons1.image = R.drawable.ic_dont_use_service
        modelCancelReasons1.textReason = "I don't use this service enough"
        modelCancelReasons1.status = false
        arrayList10.add(modelCancelReasons1)

        val modelCancelReasons2 =
            ModelCancelReasons()
        modelCancelReasons2.image = R.drawable.ic_better_plan
        modelCancelReasons2.textReason = "I found a better plan"
        modelCancelReasons2.status = false
        arrayList10.add(modelCancelReasons2)

        val modelCancelReasons3 =
            ModelCancelReasons()
        modelCancelReasons3.image = R.drawable.ic_bad_service
        modelCancelReasons3.textReason = "Bad customer service"
        modelCancelReasons3.status = false
        arrayList10.add(modelCancelReasons3)

        val modelCancelReasons4 =
            ModelCancelReasons()
        modelCancelReasons4.image = R.drawable.other_reason
        modelCancelReasons4.textReason = "Others"
        modelCancelReasons4.status = false
        arrayList10.add(modelCancelReasons4)

        mData10.value = arrayList10
        return mData10

    }


}
