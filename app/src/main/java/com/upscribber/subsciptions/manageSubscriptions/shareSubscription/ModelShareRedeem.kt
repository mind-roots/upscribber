package com.upscribber.subsciptions.manageSubscriptions.shareSubscription

class ModelShareRedeem {

    var statusMain: String = ""
    var msg: String = ""
    var campaign_id: String = ""
    var price: String = ""
    var unit: String = ""
    var frequency_type: String = ""
    var frequency_value: String = ""
    var campaign_image: String = ""
    var campaign_name: String = ""
    var description: String = ""
    var status1: String = ""
    var redeemtion_cycle_qty: String = ""
    var business_name: String = ""
    var discount_price: String = ""
    var subscription_id: String = ""
    var is_public: String = ""
    var bought: String = ""

    var itemId : String = ""
    var status : Boolean = false
    var redeemLogo: Int = 0
    var logoCount: String = ""
    var fabNails: String = ""
    var nailsCost: String = ""
    var logoRedeem: String = ""
    var  remainingQuantity: String = ""
    var logoShare: String =""
    var sum : Int = 1

}
