package com.upscribber.subsciptions.manageSubscriptions.pauseSubscription

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.graphics.Outline
import android.os.Build
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.*
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.commonClasses.Constant
import com.upscribber.R
import com.upscribber.commonClasses.RoundedBottomSheetDialogFragment
import com.upscribber.subsciptions.extrasSubsription.ModelPause
import com.upscribber.subsciptions.manageSubscriptions.ManageSubscriptionViewModel
import com.upscribber.subsciptions.manageSubscriptions.ManageSubscriptionsActivity
import com.upscribber.subsciptions.mySubscriptionDetails.MySubscriptionsModel
import kotlinx.android.synthetic.main.activity_pause.*
import java.text.SimpleDateFormat
import java.util.*


class PauseActivity : AppCompatActivity() {

    private var dataGet = MySubscriptionsModel()
    private var daysSpan: String = ""
    //private var dataGet = MySubscriptionsModel()
    lateinit var mRecyclerView: RecyclerView
    lateinit var constraint: ConstraintLayout
    lateinit var mPauseButton: TextView
    lateinit var textView103: TextView
    lateinit var tvDate: TextView
    lateinit var txtPrice: TextView
    lateinit var progressBar15: ConstraintLayout
    lateinit var mViewModel: ManageSubscriptionViewModel
    var modelPause = ArrayList<ModelPause>()


    @RequiresApi(Build.VERSION_CODES.P)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pause)
        mViewModel = ViewModelProviders.of(this)[ManageSubscriptionViewModel::class.java]
        initz()
        setToolbar()
        clickListeners()
        setData()
        setSpannableText()
        observerInit()
    }

    @SuppressLint("SimpleDateFormat")
    private fun setData() {
        dataGet = intent.getParcelableExtra("model")

        if (dataGet.next_billing_date != "") {
            tvDate.text = Constant.getDateMonthYearComplete(dataGet.next_billing_date)
            val splitPos = dataGet.price.split(".")
            if (splitPos[1] == "00" || splitPos[1] == "0") {
                txtPrice.text = "$" + splitPos[0]
            } else {
                txtPrice.text = "$" + dataGet.price
            }

            val currentDateandTime = Constant.getDate(dataGet.next_billing_date)
            daysCalculation(currentDateandTime)
            /* val calServer = Calendar.getInstance()
             calServer.time = currentDateandTime
             val calLocalCurrent = Calendar.getInstance()
             val year = currentDateandTime.year - calLocalCurrent.get(Calendar.YEAR)
             val month = currentDateandTime.month - calLocalCurrent.get(Calendar.MONTH)
             val days = calServer.get(Calendar.DAY_OF_MONTH) - calLocalCurrent.get(Calendar.DAY_OF_MONTH)
             daysTotal.text = days.toString()*/

        }


    }

    private fun daysCalculation(currentDateandTime: Date) {
        val serverMillis = currentDateandTime.time
        val currentMillis = System.currentTimeMillis()
        val millis = serverMillis - currentMillis
        val secondMillis = 1000
        val minuteMillis = secondMillis * 60
        val hoursMillis = minuteMillis * 60
        val daysMillis = hoursMillis * 24

        val daysMil = millis / daysMillis
        val afterDays = millis - (daysMillis * daysMil)
        var hours = afterDays / hoursMillis
        var minutes = (afterDays - (hoursMillis * hours)) / minuteMillis

        daysSpan = daysMil.toString()
        if (daysSpan.isNotEmpty()) {
            if (daysSpan.toInt() <= 0) {
                daysSpan = "0"
                hours = 0
                minutes = 0
            }
        }
        daysTotal.text = daysSpan
        hoursTotal.text = hours.toString()
        minTotal.text = minutes.toString()
    }

    private fun observerInit() {
        mViewModel.getmDataStatus().observe(this, Observer {
            progressBar15.visibility = View.GONE
            if (it.msg == "Invalid auth code") {
                Constant.commonAlert(this)
            } else {
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
            }
        })


        mViewModel.getmDataPause().observe(this, Observer {
            // modelPause = it
            progressBar15.visibility = View.GONE
            dataGet.status2 = "0"
            val intent = Intent(this, ConfirmPauseActivity::class.java).putExtra(
                "model", dataGet
            )
            startActivity(intent)
            finish()
        })
    }

    private fun setSpannableText() {

        val spannableString =
            SpannableString("You have $daysSpan days until your subscription renews and to pause the next billing cycle")
        spannableString.setSpan(
            ForegroundColorSpan(Color.RED),
            9, (daysSpan.length) + 15,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        textView103.text = spannableString
    }

    private fun clickListeners() {
        val data = intent.getParcelableExtra<MySubscriptionsModel>("model")
        mPauseButton.setOnClickListener {
            val fragment = MenuFragment(mViewModel, progressBar15, data)
            fragment.show(supportFragmentManager, fragment.tag)
        }

    }

    private fun initz() {
        mPauseButton = findViewById(R.id.button)
        constraint = findViewById(R.id.constraint)
        textView103 = findViewById(R.id.textView103)
        progressBar15 = findViewById(R.id.progressBar15)
        tvDate = findViewById(R.id.tvDate)
        txtPrice = findViewById(R.id.txtPrice)

    }

    private fun setToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.msToolbar)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            val intent = Intent(this, ManageSubscriptionsActivity::class.java)
            intent.putExtra("modelDone", dataGet)
            startActivity(intent)
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent = Intent(this, ManageSubscriptionsActivity::class.java)
        intent.putExtra("modelDone", dataGet)
        startActivity(intent)
        finish()
    }


    class MenuFragment(
        var mViewModel: ManageSubscriptionViewModel,
        var progressBar15: ConstraintLayout,
        var data: MySubscriptionsModel
    ) : RoundedBottomSheetDialogFragment() {

        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val view = inflater.inflate(R.layout.pause_confirmation_sheet, container, false)

            val yes = view.findViewById<TextView>(R.id.yes)
            val no = view.findViewById<TextView>(R.id.no)
            val txtDate = view.findViewById<TextView>(R.id.txtDate)
            val imageView63 = view.findViewById<ImageView>(R.id.imageView63)
            val txtPrice = view.findViewById<TextView>(R.id.txtPrice)
            val auth = Constant.getPrefs(activity!!).getString(Constant.auth_code, "")
            val splitPos = data.price.split(".")
            if (splitPos[1] == "00" || splitPos[1] == "0") {
                txtPrice.text = "$" + splitPos[0]
            } else {
                txtPrice.text = "$" + data.price
            }
            txtDate.text = Constant.getDateMonthYearComplete(data.next_billing_date)
            roundImageView(imageView63)
            yes.setOnClickListener {
                mViewModel.getPauseSubscription(auth, "0", data.order_id, data)
                progressBar15.visibility = View.VISIBLE
                dismiss()
            }
            /*  mViewModel.getmDataPause().observe(this, Observer {
                  // modelPause = it
                  progressBar15.visibility = View.GONE
                  //Toast.makeText(this, it.msg, Toast.LENGTH_LONG).show()
                  val intent = Intent(activity!!, ConfirmPauseActivity::class.java).putExtra(
                      "model",
                      it
                  )
                  startActivity(intent)
                  activity!!.finish()
                  //setResult(123, intent)
              })*/

            no.setOnClickListener {
                dismiss()
            }

            return view
        }

        private fun roundImageView(imageView63: ImageView?) {
            val curveRadius = 50F

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                imageView63!!.outlineProvider = object : ViewOutlineProvider() {

                    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                    override fun getOutline(view: View?, outline: Outline?) {
                        outline?.setRoundRect(
                            0,
                            0,
                            view!!.width,
                            view.height + curveRadius.toInt(),
                            curveRadius
                        )
                    }
                }
                imageView63.clipToOutline = true

            }
        }

    }


}
