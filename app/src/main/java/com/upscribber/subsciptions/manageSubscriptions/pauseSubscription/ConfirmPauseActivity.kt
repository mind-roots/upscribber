package com.upscribber.subsciptions.manageSubscriptions.pauseSubscription

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.databinding.ActivityConfirmPuseBinding
import com.upscribber.subsciptions.extrasSubsription.ModelPause
import com.upscribber.subsciptions.manageSubscriptions.ManageSubscriptionsActivity
import com.upscribber.subsciptions.mySubscriptionDetails.MySubscriptionsModel
import kotlinx.android.synthetic.main.activity_confirm_puse.*

@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class ConfirmPauseActivity : AppCompatActivity() {

    private var data = MySubscriptionsModel()
    lateinit var mBinding: ActivityConfirmPuseBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_confirm_puse)
        if (intent.hasExtra("Unpause")){
            textView334.text = "Subscription UnPaused"
            textView335.text = "Your subscription is now unpaused"

        }else{
            textView334.text = "Subscription Paused"
            textView335.text = "Your subscription is now paused for the next billing cycle"
        }

        clickListeners()
        setData()
    }

    @SuppressLint("SetTextI18n")
    private fun setData() {
        val imagePath = Constant.getPrefs(this).getString(Constant.dataImagePath, "")
        if (intent.hasExtra("Unpause")){
            data = intent.getParcelableExtra("model")
            mBinding.txtRenewalDate.text = Constant.getDateMonthYearComplete(data.next_billing_date)
            val splitPos = data.price.split(".")
            if (splitPos[1] == "00" || splitPos[1] == "0") {
                mBinding.txtRenewalPrice.text = "$" + splitPos[0]
            } else {
                mBinding.txtRenewalPrice.text = "$" + data.price
            }

            mBinding.textView336.text = data.campaign_name
            Glide.with(this).load(imagePath + data.feature_image).placeholder(R.mipmap.circular_placeholder).into(imageView53)

        }else if (intent.hasExtra("model")){
            data = intent.getParcelableExtra("model")
            mBinding.txtRenewalDate.text = Constant.getDateMonthYearComplete(data.next_billing_date)
            val splitPos = data.price.split(".")
            if (splitPos[1] == "00" || splitPos[1] == "0") {
                mBinding.txtRenewalPrice.text = "$" + splitPos[0]
            } else {
                mBinding.txtRenewalPrice.text = "$" + data.price
            }
            mBinding.textView336.text = data.campaign_name
            Glide.with(this).load(imagePath + data.feature_image).placeholder(R.mipmap.circular_placeholder).into(imageView53)
        }

    }

    private fun clickListeners() {
        mBinding.donePaused.setOnClickListener {
            val intent = Intent(this, ManageSubscriptionsActivity::class.java)
            intent.putExtra("modelDone", data)
            startActivity(intent)
            finish()
        }

    }
}
