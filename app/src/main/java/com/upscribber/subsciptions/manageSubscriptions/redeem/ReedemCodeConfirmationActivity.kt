package com.upscribber.subsciptions.manageSubscriptions.redeem

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.databinding.ReddemCodeConfirmationSheetBinding
import com.upscribber.home.ModelHomeLocation
import com.upscribber.subsciptions.manageSubscriptions.ModelRedeemCode
import com.upscribber.upscribberSeller.customerBottom.scanning.ScanningViewModel
import com.upscribber.upscribberSeller.customerBottom.scanning.SuccessfullyRedemptionActivity
import kotlinx.android.synthetic.main.reddem_code_confirmation_sheet.*

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class ReedemCodeConfirmationActivity : AppCompatActivity() {

    lateinit var mBinding: ReddemCodeConfirmationSheetBinding
    lateinit var mViewModel: ScanningViewModel
    lateinit var arrayHomeLocation: ArrayList<ModelHomeLocation>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.reddem_code_confirmation_sheet)
        mViewModel = ViewModelProviders.of(this)[ScanningViewModel::class.java]
        setToolbar()
        showData()
        clickEvents()
        observerInit()
    }

    private fun observerInit() {
        mViewModel.getmScanBarCode().observe(this, Observer {
            progressBar.visibility = View.GONE
            if (it.status == "true") {
                arrayHomeLocation = it.locations
                startActivity(
                    Intent(this, SuccessfullyRedemptionActivity::class.java).putExtra("modelRedeem", it)
                        .putExtra("arrayHomeLocation", arrayHomeLocation).putExtra("manually", "bar")
                )
                finish()
            }
        })

        mViewModel.getmDataStatus().observe(this, Observer {
            progressBar.visibility = View.GONE
            if (it.msg == "Invalid auth code") {
                Constant.commonAlert(this)
            } else {
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
            }
        })


    }

    private fun showData() {
        if (intent.hasExtra("modelRedeem")) {
            val model = intent.getParcelableExtra<ModelRedeemCode>("modelRedeem")
            print(model)

            if (model.team_member.isEmpty()) {
                txtTeamMember.text = ""
            } else {
                txtTeamMember.text = model.team_member
            }

            txtSubscriptionName.text = model.campaign_name
            txtAmount.text = "$" + model.total_amount
            textQuantity.text = model.quantity
            val date = Constant.getDatee(model.created_date)
            val time = Constant.getTime(model.created_date)
            txtDate.text = date
            txtTime.text = time
            txtLocation.text = model.locations[0].street + "," + model.locations[0].city + "," + model.locations[0].state
            mBinding.yes.setOnClickListener {
                val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
                mViewModel.getReddeemCode(auth, model.code, model.quantity, model.order_id,"2")
                progressBar.visibility = View.VISIBLE
            }
        }

    }

    private fun clickEvents() {
        mBinding.no.setOnClickListener {
            finish()
        }

    }

    private fun setToolbar() {
        setSupportActionBar(mBinding.includeToolbar.toolbar)
        title = ""
        mBinding.includeToolbar.title.text = "Details"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

}
