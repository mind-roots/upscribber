package com.upscribber.subsciptions.manageSubscriptions.cancelSubscription

import android.content.Intent
import android.graphics.Outline
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.upscribber.commonClasses.Constant
import com.upscribber.R
import com.upscribber.databinding.ActivityCancelReasonsBinding
import android.view.animation.AnimationUtils.loadAnimation
import android.view.animation.Animation
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import com.upscribber.commonClasses.RoundedBottomSheetDialogFragment
import com.upscribber.subsciptions.mySubscriptionDetails.MySubscriptionsModel
import kotlinx.android.synthetic.main.activity_cancel_reasons.*
import kotlinx.android.synthetic.main.cost_too_much.*


@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class CancelReasonsActivity : AppCompatActivity(),
    AdapterCancelReasons.CancelSelection {

    private lateinit var backgroundChange: ArrayList<ModelCancelReasons>
    private lateinit var mAdapter: AdapterCancelReasons
    lateinit var mBinding: ActivityCancelReasonsBinding
    lateinit var mViewModel: ViewModelCancelReasons
    private var status: Boolean = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_cancel_reasons)
        mViewModel = ViewModelProviders.of(this)[ViewModelCancelReasons::class.java]
        setToolbar()
        setAdapter()
        clicksList()
        observerInit()

    }

    private fun observerInit() {
        mViewModel.getmDataCancel().observe(this, Observer {
            Toast.makeText(this, it[0].msg, Toast.LENGTH_SHORT).show()
            progressBar13.visibility = View.VISIBLE
            if (it[0].type == "1") {
                startActivity(
                    Intent(this, CancelSubsActivity::class.java).putExtra(
                        "accept", "others"
                    ).putExtra("arrayModel", it)
                )
            } else {
                startActivity(
                    Intent(this, CancelSubsActivity::class.java).putExtra(
                        "arrayModel",
                        it
                    )
                )
            }
            finish()
            mAdapter.update(it)
        })

        mViewModel.getmDataCancelCost().observe(this, Observer {
            Toast.makeText(this, it[0].msg, Toast.LENGTH_SHORT).show()
            progressBar13.visibility = View.VISIBLE
            startActivity(
                Intent(this, CancelSubsActivity::class.java).putExtra(
                    "accept", "others"
                ).putExtra("arrayModel", it)
            )
            finish()
            mAdapter.update(it)
        })


        mViewModel.getstatus().observe(this, Observer {
            if (it.msg.isNotEmpty()) {
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
                progressBar13.visibility = View.VISIBLE
//                tvNoData.visibility = View.VISIBLE
            }
        })
    }

    private fun clicksList() {
        mBinding.textView337.setOnClickListener {
            mBinding.textView338.visibility = View.GONE
            mBinding.editOtherReaons.visibility = View.GONE

            if (status) {
                status = false
                val animation: Animation =
                    loadAnimation(applicationContext, android.R.anim.slide_in_left)
                mBinding.scroll.startAnimation(animation)
                mBinding.scroll.visibility = View.VISIBLE

            } else {
                status = true
                mBinding.scroll.visibility = View.GONE
            }

        }

    }

    private fun setAdapter() {
        mBinding.recyclerReasons.layoutManager = LinearLayoutManager(this)
        mAdapter = AdapterCancelReasons(this)
        mBinding.recyclerReasons.adapter = mAdapter
        val model = intent.getParcelableExtra<MySubscriptionsModel>("model")
        if (model.is_cancelled != "2") {
            mViewModel.getListReasons().observe(this, Observer {
                backgroundChange = it
                mAdapter.update(backgroundChange)
            })
        } else {
            mViewModel.getListReasonsOptionFour().observe(this, Observer {
                backgroundChange = it
                mAdapter.update(backgroundChange)
            })
        }


    }


    private fun setToolbar() {
        setSupportActionBar(mBinding.include9.toolbar)
        mBinding.include9.toolbar.title = ""
        mBinding.include9.title.text = "Cancel Subscription"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }


    override fun ReasonCancel(
        position: Int,
        modelCancelReasons: ModelCancelReasons
    ) {

        mBinding.textView337.text = modelCancelReasons.textReason
        mBinding.textView337.setCompoundDrawablesWithIntrinsicBounds(
            modelCancelReasons.image,
            0,
            0,
            0
        )
        status = true
        val animation: Animation = loadAnimation(
            applicationContext, R.anim.slide_out_left

        )

        mBinding.scroll.startAnimation(animation)

        if (position == 4) {
            mBinding.textView338.visibility = View.VISIBLE
            mBinding.editOtherReaons.visibility = View.VISIBLE
            mBinding.scroll.visibility = View.GONE
        } else {
            mBinding.textView338.visibility = View.GONE
            mBinding.editOtherReaons.visibility = View.GONE
            mBinding.scroll.visibility = View.GONE
        }

        //  mAdapter.notifyDataSetChanged()
        clickListeners(position)

    }

    override fun onResume() {
        super.onResume()
        val sharedPreferences1 = Constant.getSharedPrefs(this)
        if (!sharedPreferences1.getString("customer", "").isEmpty()) {
            finish()
        }
    }

    private fun clickListeners(position: Int) {
        val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
        val model = intent.getParcelableExtra<MySubscriptionsModel>("model")
        mBinding.button.setOnClickListener {
            if (position == 0) {
                if (model.is_cancelled != "2") {
                    linearCostTooMuch.visibility = View.VISIBLE

                    acceptBtn.setOnClickListener {
                        progressBar13.visibility = View.VISIBLE
                        mViewModel.cancelSubscription(
                            auth,
                            model.order_id,
                            mBinding.textView337.text.toString().trim(),
                            "1"
                        )
                    }

                    textDismiss.setOnClickListener {
                        val fragment = MenuFragmentDelete(mViewModel, progressBar13, this)
                        fragment.show(supportFragmentManager, fragment.tag)
                    }


                } else {
                    Toast.makeText(this, "You have already take this discount!", Toast.LENGTH_LONG)
                        .show()
                }
            } else if (position == 4) {
                if (mBinding.editOtherReaons.text.isNotEmpty()) {
                    progressBar13.visibility = View.VISIBLE
                    mViewModel.cancelSubscription(
                        auth,
                        model.order_id,
                        mBinding.editOtherReaons.text.toString().trim(),
                        "2"
                    )
                } else {
                    Toast.makeText(
                        this,
                        "Please enter text in the provided field",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            } else {
                progressBar13.visibility = View.VISIBLE
                mViewModel.cancelSubscription(
                    auth,
                    model.order_id,
                    mBinding.textView337.text.toString().trim(),
                    "2"
                )

            }

        }
    }

    class MenuFragmentDelete(
        var mViewModel: ViewModelCancelReasons,
        var progressBar29: View,
        var mContext: CancelReasonsActivity
    ) : RoundedBottomSheetDialogFragment() {

        lateinit var yes: TextView
        lateinit var no: TextView
        lateinit var textDescription: TextView
        lateinit var textHeading: TextView
        lateinit var imageViewTop: ImageView

        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val view = inflater.inflate(R.layout.decline_sheet, container, false)
            val auth = Constant.getPrefs(mContext).getString(Constant.auth_code, "")
            yes = view.findViewById(R.id.yes)
            yes.background = mContext.resources.getDrawable(R.drawable.invite_button_background)
            no = view.findViewById(R.id.no)
            imageViewTop = view.findViewById(R.id.imageViewTop)
            textDescription = view.findViewById(R.id.textDescription)
            textHeading = view.findViewById(R.id.textHeading)

            roundedImage(imageViewTop)
            val model = mContext.intent.getParcelableExtra<MySubscriptionsModel>("model")

            no.setOnClickListener {
                dismiss()
                mContext.finish()
            }

            yes.setOnClickListener {
//                mViewModel.cancelSubscription(
//                    auth,
//                    model.id,
//                    "Costs too much",
//                    "1"
//                )
//                dismiss()
            }

            return view
        }

        private fun roundedImage(imageViewTop: ImageView) {
            val curveRadius = 50F

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                imageViewTop.outlineProvider = object : ViewOutlineProvider() {

                    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                    override fun getOutline(view: View?, outline: Outline?) {
                        outline?.setRoundRect(
                            0,
                            0,
                            view!!.width,
                            view.height + curveRadius.toInt(),
                            curveRadius
                        )
                    }
                }
                imageViewTop.clipToOutline = true
            }
        }


    }


}
