package com.upscribber.subsciptions.manageSubscriptions.cancelSubscription

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.R
import kotlinx.android.synthetic.main.cancel_resons_recycler_list.view.*


class AdapterCancelReasons(private val mContext: Context ) :
    RecyclerView.Adapter<AdapterCancelReasons.MyViewHolder>() {

    var listener = mContext as CancelSelection
    private lateinit var list: ArrayList<ModelCancelReasons>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val v = LayoutInflater.from(mContext)
            .inflate(R.layout.cancel_resons_recycler_list, parent, false)

        return MyViewHolder(v)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = list[position]
        holder.itemView.imageView47.setImageResource(model.image)
        holder.itemView.textView310.text = model.textReason

        holder.itemView.setOnClickListener {
            listener.ReasonCancel(position,list[position])
        }


//        if (model.status) {
//            holder.itemView.constrainnt.setBackgroundResource(R.drawable.login_background_gradient)
//        } else {
//            holder.itemView.constrainnt.setBackgroundResource(R.drawable.login_background_white)
//        }


    }

    fun update(backgroundChange: ArrayList<ModelCancelReasons>) {

        this.list = backgroundChange
        notifyDataSetChanged()

    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)


    interface CancelSelection {
        fun ReasonCancel(
            position: Int,
            modelCancelReasons: ModelCancelReasons
        )
    }
}

