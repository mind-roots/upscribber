package com.upscribber.subsciptions.manageSubscriptions.shareSubscription

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by Manisha Thakur on 28/3/19.
 */
class ModelShareData() : Parcelable{
    var status: String = ""
    var name: String = ""
    var contact_no: String = ""
    var total: String = ""
    var cycle_start_date: String = ""
    var next_billing_date: String = ""
    var service: String = ""
    var msg: String = ""
    var business_name: String = ""
    var feature_image: String = ""
    var team_member: String = ""

    constructor(parcel: Parcel) : this() {
        status = parcel.readString()
        name = parcel.readString()
        contact_no = parcel.readString()
        total = parcel.readString()
        cycle_start_date = parcel.readString()
        next_billing_date = parcel.readString()
        service = parcel.readString()
        msg = parcel.readString()
        business_name = parcel.readString()
        feature_image = parcel.readString()
        team_member = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(status)
        parcel.writeString(name)
        parcel.writeString(contact_no)
        parcel.writeString(total)
        parcel.writeString(cycle_start_date)
        parcel.writeString(next_billing_date)
        parcel.writeString(service)
        parcel.writeString(msg)
        parcel.writeString(business_name)
        parcel.writeString(feature_image)
        parcel.writeString(team_member)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ModelShareData> {
        override fun createFromParcel(parcel: Parcel): ModelShareData {
            return ModelShareData(parcel)
        }

        override fun newArray(size: Int): Array<ModelShareData?> {
            return arrayOfNulls(size)
        }
    }

}