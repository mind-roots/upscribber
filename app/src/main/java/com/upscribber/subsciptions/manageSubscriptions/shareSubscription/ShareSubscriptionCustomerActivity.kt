package com.upscribber.subsciptions.manageSubscriptions.shareSubscription

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.databinding.ActivityShareSubscriptionCustomerBinding
import com.upscribber.subsciptions.manageSubscriptions.ManageSubscriptionsActivity
import com.upscribber.subsciptions.mySubscriptionDetails.MySubscriptionsModel
import kotlinx.android.synthetic.main.activity_share_subscription_customer.*
import java.util.ArrayList

class ShareSubscriptionCustomerActivity : AppCompatActivity() {

    private var data = ArrayList<MySubscriptionsModel>()
    lateinit var mBinding: ActivityShareSubscriptionCustomerBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_share_subscription_customer)
        clickListeners()
        setData()

        if (intent.hasExtra("fromredeemshare")) {
            mBinding.textView335.visibility = View.GONE
            mBinding.clProfileData.visibility = View.VISIBLE
        } else if (intent.hasExtra("mySubscription")) {
            mBinding.clProfileData.visibility = View.GONE
            mBinding.textView335.visibility = View.VISIBLE
            mBinding.textView335.text = "Your subscription has been shared. The recipient can use the invite code to redeem."
        } else {
            mBinding.clProfileData.visibility = View.GONE
            mBinding.textView335.visibility = View.VISIBLE
            mBinding.textView335.text = "Your subscription has been shared. The recipient can use the invite code to redeem."
        }

    }

    @SuppressLint("SetTextI18n")
    private fun setData() {
        data = intent.getParcelableArrayListExtra<MySubscriptionsModel>("model")
        val dateStart = Constant.getDatee(data[0].share_date)
        val dateEnd = Constant.getDatee(data[0].next_billing_date)
        shareWithTxt.text = data[0].name
        txtBusiness.text = data[0].business_name
        txtPhone.text = Constant.formatPhoneNumber(data[0].contact_no)
        txtService.text = data[0].service
        txtAmount.text = "$" + data[0].total
        textView336.text = data[0].service
//        txtTemMember.text = data[0].team_member
        txDateFrom.text = dateStart
        txdateTo.text = dateEnd
    }

    private fun clickListeners() {
        mBinding.doneSharing.setOnClickListener {
            if (intent.hasExtra("mySubscription")) {
                finish()
            } else {
                val subId = intent.getParcelableExtra<MySubscriptionsModel>("baseModel")
                subId.share = "1"
                val intent = Intent(this, ManageSubscriptionsActivity::class.java)
                intent.putExtra("modelData", subId)
                startActivity(intent)
                finish()
            }


        }
    }
}
