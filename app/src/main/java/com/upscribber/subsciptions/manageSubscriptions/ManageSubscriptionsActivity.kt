package com.upscribber.subsciptions.manageSubscriptions

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Outline
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.commonClasses.RoundedBottomSheetDialogFragment
import com.upscribber.subsciptions.creditSubscriptions.CreditSubscriptionsActivity
import com.upscribber.subsciptions.extrasSubsription.ExtrasSubscriptionActivity
import com.upscribber.subsciptions.manageSubscriptions.cancelSubscription.CancelReasonsActivity
import com.upscribber.subsciptions.manageSubscriptions.pauseSubscription.ConfirmPauseActivity
import com.upscribber.subsciptions.manageSubscriptions.pauseSubscription.PauseActivity
import com.upscribber.subsciptions.manageSubscriptions.redeem.ReedemActivity
import com.upscribber.subsciptions.manageSubscriptions.shareSubscription.ShareInputActivity
import com.upscribber.subsciptions.mySubscriptionDetails.MySubscriptionsModel
import kotlinx.android.synthetic.main.activity_manage_subscriptions.*
import kotlinx.android.synthetic.main.redeem_sheet_quantity.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class ManageSubscriptionsActivity : AppCompatActivity() {

    private var subscriptionModel = MySubscriptionsModel()

    private lateinit var manageCredit: TextView
    lateinit var tVredeem: TextView
    private lateinit var tvExtras: TextView
    private lateinit var button: TextView
    private lateinit var cancel: TextView
    var dataModel = MySubscriptionsModel()
    lateinit var mViewModel: ManageSubscriptionViewModel

    companion object {
        private var dataSubModel = MySubscriptionsModel()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_manage_subscriptions)
        mViewModel = ViewModelProviders.of(this)[ManageSubscriptionViewModel::class.java]
        initz()
        click()
        setToolbar()
        roundedImage()
        setData()
        observerInit()


    }

    private fun setData() {
        if (intent.hasExtra("modelDone")) {
            val dd = intent.getParcelableExtra<MySubscriptionsModel>("modelDone")
            dd.share_setting = dataSubModel.share_setting
            dataSubModel = dd

            if (dataSubModel.status2 == "1") {
                button.setText(R.string.pause)
            } else {
                button.setText(R.string.unpause)
            }

            if (dataSubModel.share == "1") {
                shareSubscription.text = "Unshare"
            } else {
                shareSubscription.text = "Share"
            }

//            if (dataSubModel.share_setting == "0") {
//                shareSubscription.alpha = 0.4f
//            }

        }


        if (intent.hasExtra("modelData")) {
            val dd = intent.getParcelableExtra<MySubscriptionsModel>("modelData")
            dataSubModel = dd

            if (dataSubModel.status2 == "1") {
                button.setText(R.string.pause)
            } else {
                button.setText(R.string.unpause)
            }
            if (dataSubModel.share == "1") {
                shareSubscription.text = "Unshare"
            } else {
                shareSubscription.text = "Share"
            }

            if (dataSubModel.share_setting == "0") {
                shareSubscription.alpha = 0.4f
            }
        }

        if (intent.hasExtra("model")) {
            dataSubModel = intent.getParcelableExtra("model")
            if (dataSubModel.status2 == "1") {
                button.setText(R.string.pause)
            } else {
                button.setText(R.string.unpause)
            }

            if (dataSubModel.share == "1") {
                shareSubscription.text = "Unshare"
            } else {
                shareSubscription.text = "Share"
            }

            if (dataSubModel.share_setting == "0") {
                shareSubscription.alpha = 0.4f
            }


            if (dataSubModel.is_cancelled == "1") {
                cancel.alpha = 0.4f
            }

            if (dataSubModel.status2 == "3") {
                tVredeem.isEnabled = false
                shareSubscription.isEnabled = false
                button.isEnabled = false
                cancel.isEnabled = false
            } else {
                tVredeem.isEnabled = true
                shareSubscription.isEnabled = true
                button.isEnabled = true
                cancel.isEnabled = true
            }


            if (dataSubModel.redeemtion_cycle_qty != "" && dataSubModel.redeem_count != "") {
                val valueRedeemCount =
                    dataSubModel.redeemtion_cycle_qty.toInt() - dataSubModel.redeem_count.toInt()
                if (valueRedeemCount != 0) {
                    tVredeem.isEnabled = true
                } else {
                    tVredeem.isEnabled = false
                    tVredeem.alpha = 0.4F


                }


                when {
                    valueRedeemCount == 0 -> {
                        shareSubscription.alpha = 0.4f
                        shareSubscription.isEnabled = false
                    }
                    dataSubModel.is_shared == "1" -> {
                        shareSubscription.alpha = 0.4f
                        shareSubscription.isEnabled = false
                    }
                    dataModel.share_setting == "0" -> {
                        shareSubscription.alpha = 0.4f
                        shareSubscription.isEnabled = false
                    }
                    else -> {
                        shareSubscription.isEnabled = true
                    }
                }
            }
        }

    }


    private fun observerInit() {
        mViewModel.getmDataPause().observe(this, Observer {
            startActivity(
                Intent(this, ConfirmPauseActivity::class.java).putExtra(
                    "model", it
                ).putExtra("Unpause", "other")
            )
            finish()

        })

    }


    @SuppressLint("SetTextI18n")
    private fun initz() {
        manageCredit = findViewById(R.id.manageCredit)
        tVredeem = findViewById(R.id.tVredeem)
        tvExtras = findViewById(R.id.tvExtras)
        button = findViewById(R.id.button)
        cancel = findViewById(R.id.cancel)

        val arrayHomeProfile = Constant.getArrayListProfile(this, Constant.dataProfile)
        val splitPos = arrayHomeProfile.wallet.split(".")
        if (splitPos[1] == "00" || splitPos[1] == "0") {
            manageCredit.text = "$" + splitPos[0]
        } else {
            manageCredit.text = "$" + arrayHomeProfile.wallet
        }

    }

    override fun onResume() {
        super.onResume()
        val sharedPreferences1 = Constant.getSharedPrefs(this)
        if (!sharedPreferences1.getString("customer", "").isEmpty()) {
            finish()
        }
        val editor = sharedPreferences1.edit()
        editor.remove("customer")
        editor.apply()
        if (dataSubModel.redeemtion_cycle_qty != "" && dataSubModel.redeem_count != "") {


            if (dataSubModel.redeemtion_cycle_qty.toInt() - dataSubModel.redeem_count.toInt() == 0) {
                shareSubscription.alpha = 0.4f
                shareSubscription.isEnabled = false
                tVredeem.isEnabled = false
                tVredeem.alpha = 0.4F

            }
            if (dataSubModel.is_shared == "1") {
                shareSubscription.alpha = 0.4f
                shareSubscription.isEnabled = false
            }


            if (dataModel.share_setting == "0") {
                shareSubscription.alpha = 0.4f
                shareSubscription.isEnabled = false
            }


            if (dataSubModel.is_cancelled == "1") {
                cancel.alpha = 0.4f
                cancel.isEnabled = false
            }

        }


    }


    @SuppressLint("SimpleDateFormat")
    private fun click() {

        button.setOnClickListener {
            if (dataSubModel.status2 != "3") {
                val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
                if (dataSubModel.status2 == "1") {
                    val intent = Intent(this, PauseActivity::class.java)
                        .putExtra("model", dataSubModel)
                    startActivity(intent)
                    finish()

                } else {
                    val currentDate = SimpleDateFormat("yyyy-MM-dd")
                    val todayDate = Date()
                    val thisDate = currentDate.format(todayDate)
//                val nextBillingDate = Constant.getDateMonthYearComplete(dataSubModel.next_billing_date)
                    if (dataSubModel.next_billing_date > thisDate) {
                        mViewModel.getPauseSubscription(auth, "1", dataSubModel.order_id, dataSubModel)
                    } else {
                        val fragment = MenuFragmentUnpase(dataSubModel, mViewModel)
                        fragment.show(supportFragmentManager, fragment.tag)
                    }

                }
            } else {
                val alertDialog = android.app.AlertDialog.Builder(this)
                    .setMessage("Canceled subscription cannot be paused or unpaused.")
                    .setPositiveButton("Ok") { dialogInterface, i ->
                        dialogInterface.dismiss()
                    }
                    .show()
            }

        }



        manageCredit.setOnClickListener {
            startActivity(Intent(this, CreditSubscriptionsActivity::class.java))
        }

        tvExtras.setOnClickListener {
            startActivity(Intent(this, ExtrasSubscriptionActivity::class.java))
        }

        cancel.setOnClickListener {
            if (dataSubModel.status2 != "3") {
                if (dataSubModel.is_cancelled != "1") {
                    val fragment = MenuFragment(dataSubModel)
                    fragment.show(supportFragmentManager, fragment.tag)
                }
            } else {
                val alertDialog = android.app.AlertDialog.Builder(this)
                    .setMessage("You have already cancel this subscription.")
                    .setPositiveButton("Ok") { dialogInterface, i ->
                        dialogInterface.dismiss()
                    }
                    .show()
            }
        }

        shareSubscription.setOnClickListener {
            if (dataSubModel.status2 != "3") {
                val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
                if (dataSubModel.share_setting == "1") {
                    if (dataSubModel.share == "0") {
                        startActivityForResult(
                            Intent(this, ShareInputActivity::class.java).putExtra(
                                "model", dataSubModel
                            ).putExtra("manage", "123"), 100
                        )
                        finish()
                    } else {
                        mViewModel.getUnshare(auth, "0", dataSubModel.order_id)
                    }
                } else {
                    Toast.makeText(this, "shgduyg", Toast.LENGTH_LONG).show()
                }
            } else {
                val alertDialog = android.app.AlertDialog.Builder(this)
                    .setMessage("Canceled subscription cannot be shared.")
                    .setPositiveButton("Ok") { dialogInterface, i ->
                        dialogInterface.dismiss()
                    }
                    .show()
            }
        }

        mViewModel.getmDataState().observe(this, Observer {
            if (it.status1 == "true") {
                dataSubModel.share = "0"
                if (dataSubModel.share == "1") {
                    shareSubscription.text = "Unshare"
                } else {
                    shareSubscription.text = "Share"
                }
            }

        })

        tVredeem.setOnClickListener {
            if (dataSubModel.status2 != "3") {
                val fragment = MenuFragmentRedeem(dataSubModel)
                val bundle = Bundle()
                bundle.putParcelable("model", dataSubModel)
                fragment.arguments = bundle
                fragment.show(supportFragmentManager, fragment.tag)
            } else {
                val alertDialog = android.app.AlertDialog.Builder(this)
                    .setMessage("Canceled subscription cannot be redeemed.")
                    .setPositiveButton("Ok") { dialogInterface, i ->
                        dialogInterface.dismiss()
                    }
                    .show()
            }
        }

    }

    class MenuFragmentUnpase(
        var dataSubModel: MySubscriptionsModel,
        var mViewModel: ManageSubscriptionViewModel
    ) : RoundedBottomSheetDialogFragment() {

        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val view = inflater.inflate(R.layout.unpause_confirmation_sheet, container, false)

            val yes = view.findViewById<TextView>(R.id.yes)
            val no = view.findViewById<TextView>(R.id.no)
            val imageView63 = view.findViewById<ImageView>(R.id.imageView63)
            val txtPrice = view.findViewById<TextView>(R.id.txtPrice)
            val auth = Constant.getPrefs(activity!!).getString(Constant.auth_code, "")
            txtPrice.text = "$" + dataSubModel.price
            roundImageView(imageView63)
            yes.setOnClickListener {
                startActivity(
                    Intent(
                        context,
                        CompleteCheckOutUnpause::class.java
                    ).putExtra("modelSubscription", dataSubModel)
                )
                dismiss()
            }

            no.setOnClickListener {
                dismiss()
            }

            return view
        }

        private fun roundImageView(imageView63: ImageView?) {
            val curveRadius = 50F

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                imageView63!!.outlineProvider = object : ViewOutlineProvider() {

                    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                    override fun getOutline(view: View?, outline: Outline?) {
                        outline?.setRoundRect(
                            0,
                            0,
                            view!!.width,
                            view.height + curveRadius.toInt(),
                            curveRadius
                        )
                    }
                }
                imageView63.clipToOutline = true

            }
        }


    }

    private fun setToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.msToolbar)
        setSupportActionBar(toolbar)
        toolbar.title = ""
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white)
    }

    private fun roundedImage() {
        val image = findViewById<ImageView>(R.id.topBg)
        val curveRadius = 70F

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            image.outlineProvider = object : ViewOutlineProvider() {

                @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                override fun getOutline(view: View?, outline: Outline?) {
                    outline?.setRoundRect(
                        0,
                        -100,
                        view!!.width,
                        view.height,
                        curveRadius
                    )
                }
            }

            image.clipToOutline = true

        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    class MenuFragmentRedeem(var model: MySubscriptionsModel) : RoundedBottomSheetDialogFragment() {

        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val view = inflater.inflate(R.layout.redeem_sheet_quantity, container, false)

            val invite = view.findViewById<TextView>(R.id.invite)
            val etQuantity = view.findViewById<TextView>(R.id.etQuantity)
            val imageViewTop = view.findViewById<ImageView>(R.id.imageViewTop)
            roundImage(imageViewTop)

            invite.setOnClickListener {
                if (etQuantity.text.isEmpty()) {
                    Toast.makeText(activity, "Please select quantity first!", Toast.LENGTH_LONG)
                        .show()
                } else {
                    startActivity(
                        Intent(this.activity, ReedemActivity::class.java).putExtra(
                            "quantity",
                            etQuantity.text.toString()
                        )
                            .putExtra(
                                "model",
                                dataSubModel
                            )
                    )
                    dismiss()
                }
            }

            etQuantity.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(
                    s: CharSequence,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                    if (getTextFieldsData()) {
                        invite.setBackgroundResource(R.drawable.favorite_delete_background)
                    } else {
                        invite.setBackgroundResource(R.drawable.favorite_delete_background_opacity)
                    }
                }

                override fun afterTextChanged(s: Editable) {
                }
            })

            etQuantity.setOnClickListener {
                val choices = getList()
                if (choices.size > 0) {
                    val charSequenceItems =
                        choices.toArray(arrayOfNulls<CharSequence>(choices.size))
                    val mBuilder = AlertDialog.Builder(activity!!)
                    mBuilder.setSingleChoiceItems(charSequenceItems, -1) { dialogInterface, i ->
                        etQuantity.text = choices[i]
                        dialogInterface.dismiss()
                    }
                    val mDialog = mBuilder.create()
                    mDialog.show()
                } else {
                    Toast.makeText(
                        activity,
                        "You don't have enough quantity to reedem",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }

            return view
        }

        private fun getTextFieldsData(): Boolean {
            val quantity = etQuantity.text.toString().trim().length

            if (quantity == 0) {
                return false
            }

            return true

        }

        private fun roundImage(imageViewTop: ImageView) {
            val curveRadius = 50F

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                imageViewTop.outlineProvider = object : ViewOutlineProvider() {

                    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                    override fun getOutline(view: View?, outline: Outline?) {
                        outline?.setRoundRect(
                            0,
                            0,
                            view!!.width,
                            view.height + curveRadius.toInt(),
                            curveRadius

                        )
                    }
                }

                imageViewTop.clipToOutline = true
            }

        }

        private fun getList(): ArrayList<String> {
            val arrayList: ArrayList<String> = ArrayList()
            if (model.free_trial_qty.toInt() > 0) {
                for (i in 0 until (model.free_trial_qty.toInt() - model.redeem_count.toInt())) {

                    arrayList.add((i + 1).toString())
                }
                if (arrayList.size == 0) {
                    if (model.redeemtion_cycle_qty.toInt() > 100) {
                        for (i in 0 until 500) {
                            arrayList.add((i + 1).toString())
                        }
                    } else {
                        for (i in 0 until (model.redeemtion_cycle_qty.toInt() - model.redeem_count.toInt())) {

                            arrayList.add((i + 1).toString())
                        }
                    }
                }
            } else {
                if (model.redeemtion_cycle_qty.toInt() > 100) {
                    for (i in 0 until 500) {
                        arrayList.add((i + 1).toString())
                    }
                } else {
                    for (i in 0 until (model.redeemtion_cycle_qty.toInt() - model.redeem_count.toInt())) {

                        arrayList.add((i + 1).toString())
                    }
                }
            }

            return arrayList
        }

    }


    class MenuFragment(var dataSubModel: MySubscriptionsModel) :
        RoundedBottomSheetDialogFragment() {

        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val view = inflater.inflate(R.layout.cancel_sheet, container, false)

            val yes = view.findViewById<TextView>(R.id.yes)
            val no = view.findViewById<TextView>(R.id.no)

            yes.setOnClickListener {
                startActivity(
                    Intent(this.activity, CancelReasonsActivity::class.java).putExtra(
                        "model",
                        dataSubModel
                    )
                )

                dismiss()

            }
            no.setOnClickListener {
                dismiss()
            }

            return view
        }

    }
}
