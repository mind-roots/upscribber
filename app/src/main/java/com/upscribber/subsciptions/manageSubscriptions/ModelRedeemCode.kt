package com.upscribber.subsciptions.manageSubscriptions

import android.os.Parcel
import android.os.Parcelable
import com.upscribber.home.ModelHomeLocation
import java.util.ArrayList



class ModelRedeemCode() : Parcelable {

    var rtype: String = ""
    var logo: String = ""
    var member_name: String = ""
    var business_phone: String = ""
    var business_id: String = ""
    var description: String = ""
    var frequency_type: String = ""
    var frequency_value: String = ""
    var unit: String = ""
    var redeemtion_cycle_qty: String = ""
    var assign_to: String = ""
    var price: String = ""
    var campaign_name: String = ""
    var total_amount: String = ""
    var customer_name: String = ""
    var business_name: String = ""
    var team_member: String = ""
    var location_id: String = ""
    var contact_no: String = ""
    var profile_image: String = ""
    var created_at: String = ""
    var remaining_visits: String = ""
    var customer_id: String = ""
    var id: String = ""
    var user_id: String = ""
    var quantity: String = ""
    var order_id: String = ""
    var msg: String = ""
    var status: String = ""
    var code: String = ""
    var created_date: String = ""
    var locations: ArrayList<ModelHomeLocation> = ArrayList()

    constructor(parcel: Parcel) : this() {
        rtype = parcel.readString()
        logo = parcel.readString()
        member_name = parcel.readString()
        business_phone = parcel.readString()
        business_id = parcel.readString()
        description = parcel.readString()
        frequency_type = parcel.readString()
        frequency_value = parcel.readString()
        unit = parcel.readString()
        redeemtion_cycle_qty = parcel.readString()
        assign_to = parcel.readString()
        price = parcel.readString()
        campaign_name = parcel.readString()
        total_amount = parcel.readString()
        customer_name = parcel.readString()
        business_name = parcel.readString()
        team_member = parcel.readString()
        location_id = parcel.readString()
        contact_no = parcel.readString()
        profile_image = parcel.readString()
        created_at = parcel.readString()
        remaining_visits = parcel.readString()
        customer_id = parcel.readString()
        id = parcel.readString()
        user_id = parcel.readString()
        quantity = parcel.readString()
        order_id = parcel.readString()
        msg = parcel.readString()
        status = parcel.readString()
        code = parcel.readString()
        created_date = parcel.readString()
        locations = parcel.createTypedArrayList(ModelHomeLocation)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(rtype)
        parcel.writeString(logo)
        parcel.writeString(member_name)
        parcel.writeString(business_phone)
        parcel.writeString(business_id)
        parcel.writeString(description)
        parcel.writeString(frequency_type)
        parcel.writeString(frequency_value)
        parcel.writeString(unit)
        parcel.writeString(redeemtion_cycle_qty)
        parcel.writeString(assign_to)
        parcel.writeString(price)
        parcel.writeString(campaign_name)
        parcel.writeString(total_amount)
        parcel.writeString(customer_name)
        parcel.writeString(business_name)
        parcel.writeString(team_member)
        parcel.writeString(location_id)
        parcel.writeString(contact_no)
        parcel.writeString(profile_image)
        parcel.writeString(created_at)
        parcel.writeString(remaining_visits)
        parcel.writeString(customer_id)
        parcel.writeString(id)
        parcel.writeString(user_id)
        parcel.writeString(quantity)
        parcel.writeString(order_id)
        parcel.writeString(msg)
        parcel.writeString(status)
        parcel.writeString(code)
        parcel.writeString(created_date)
        parcel.writeTypedList(locations)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ModelRedeemCode> {
        override fun createFromParcel(parcel: Parcel): ModelRedeemCode {
            return ModelRedeemCode(parcel)
        }

        override fun newArray(size: Int): Array<ModelRedeemCode?> {
            return arrayOfNulls(size)
        }
    }


}
