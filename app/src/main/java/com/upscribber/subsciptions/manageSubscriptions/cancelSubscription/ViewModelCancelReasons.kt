package com.upscribber.subsciptions.manageSubscriptions.cancelSubscription

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.upscribber.commonClasses.ModelStatusMsg

class ViewModelCancelReasons(application: Application) : AndroidViewModel(application) {

    var repositoryCancelReason : RepositoryCancelReason =
        RepositoryCancelReason(application)

    fun getListReasons() : LiveData<ArrayList<ModelCancelReasons>> {
        return repositoryCancelReason.getList()
    }

    fun cancelSubscription(auth: String, subscriptionId: String, cancelReason: String,type:String) {
        repositoryCancelReason.getCancelSubscription(auth,subscriptionId,cancelReason,type)
    }

    fun getmDataCancel() :LiveData<ArrayList<ModelCancelReasons>>{
        return repositoryCancelReason.getmDataCancel()
    }

     fun getmDataCancelCost() :LiveData<ArrayList<ModelCancelReasons>>{
        return repositoryCancelReason.getmDataCancelCost()
    }


     fun getstatus() :LiveData<ModelStatusMsg>{
        return repositoryCancelReason.getmDataStatus()
    }


    fun costTooMuchSubscription(
        auth: String,
        subscriptionId: String,
        cancelReason: String,
        type:String
    ) {
        repositoryCancelReason.getCancelCostTooSubscription(auth,subscriptionId,cancelReason,type)

    }

    fun getListReasonsOptionFour(): LiveData<ArrayList<ModelCancelReasons>> {
        return repositoryCancelReason.getListReasonsOptionFour()
    }


}
