package com.upscribber.subsciptions.manageSubscriptions.cancelSubscription

import android.os.Parcel
import android.os.Parcelable

class ModelCancelReasons() : Parcelable{

    var type: String = ""
    var campaign_name: String = ""
    var status : Boolean = false
     var image : Int = 0
    var textReason : String = ""

    var msg: String = ""
    var status1 : String = ""
    var cycle_start_date: String = ""
    var total: String = ""
    var unit: String = ""
    var redeemtion_cycle_qty: String = ""
    var frequency_type: String = ""

    constructor(parcel: Parcel) : this() {
        type = parcel.readString()
        campaign_name = parcel.readString()
        status = parcel.readByte() != 0.toByte()
        image = parcel.readInt()
        textReason = parcel.readString()
        msg = parcel.readString()
        status1 = parcel.readString()
        cycle_start_date = parcel.readString()
        total = parcel.readString()
        unit = parcel.readString()
        redeemtion_cycle_qty = parcel.readString()
        frequency_type = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(type)
        parcel.writeString(campaign_name)
        parcel.writeByte(if (status) 1 else 0)
        parcel.writeInt(image)
        parcel.writeString(textReason)
        parcel.writeString(msg)
        parcel.writeString(status1)
        parcel.writeString(cycle_start_date)
        parcel.writeString(total)
        parcel.writeString(unit)
        parcel.writeString(redeemtion_cycle_qty)
        parcel.writeString(frequency_type)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ModelCancelReasons> {
        override fun createFromParcel(parcel: Parcel): ModelCancelReasons {
            return ModelCancelReasons(parcel)
        }

        override fun newArray(size: Int): Array<ModelCancelReasons?> {
            return arrayOfNulls(size)
        }
    }


}
