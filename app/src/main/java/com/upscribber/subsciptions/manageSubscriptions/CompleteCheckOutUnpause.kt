package com.upscribber.subsciptions.manageSubscriptions

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.upscribber.R
import com.upscribber.checkout.AdapterCards
import com.upscribber.checkout.CheckOutViewModel
import com.upscribber.checkout.cardSelected
import com.upscribber.commonClasses.Constant
import com.upscribber.payment.paymentCards.AddCardActivity
import com.upscribber.payment.paymentCards.PaymentCardModel
import com.upscribber.subsciptions.mySubscriptionDetails.MySubscriptionsModel
import kotlinx.android.synthetic.main.activity_complete_check_out_unpause.frequencyType
import kotlinx.android.synthetic.main.activity_complete_check_out_unpause.productOffer
import kotlinx.android.synthetic.main.activity_complete_check_out_unpause.productPrice
import kotlinx.android.synthetic.main.activity_complete_check_out_unpause.progressBar11
import kotlinx.android.synthetic.main.activity_complete_check_out_unpause.recyclerViewCards
import kotlinx.android.synthetic.main.activity_complete_check_out_unpause.roundedImageView
import kotlinx.android.synthetic.main.activity_complete_check_out_unpause.tVTrial
import kotlinx.android.synthetic.main.activity_complete_check_out_unpause.tViewText
import kotlinx.android.synthetic.main.activity_complete_check_out_unpause.tvCompanyName
import kotlinx.android.synthetic.main.activity_complete_check_out_unpause.tvName
import kotlinx.android.synthetic.main.activity_complete_check_out_unpause.tvVisits
import kotlinx.android.synthetic.main.checkout_two_total.*
import java.math.BigDecimal
import java.math.RoundingMode

@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class CompleteCheckOutUnpause : AppCompatActivity(), cardSelected {

    var typeDiscount = ""
    private var status: Boolean = true
    lateinit var toolbar: Toolbar
    lateinit var title: TextView
    lateinit var toolbarTitle: TextView
    private lateinit var categoriesList: ArrayList<PaymentCardModel>
    lateinit var mViewModel: CheckOutViewModel
    private lateinit var adapterCards: AdapterCards
    lateinit var tVAddCard: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_complete_check_out_unpause)
        setToolbar()
        adapters()
        setData()
        observerInit()

    }

    private fun observerInit() {
        mViewModel.getmDataRenewSubscription().observe(this, Observer {
            if (it.status == "true") {
//                startActivity(
//                    Intent(this, ConfirmPauseActivity::class.java).putExtra("model",it))
                finish()
            }
        })

    }

    private fun adapters() {
        recyclerViewCards.layoutManager =
            LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
        adapterCards = AdapterCards(this, 1)
        recyclerViewCards.adapter = adapterCards

    }

    private fun setToolbar() {
        mViewModel = ViewModelProviders.of(this)[CheckOutViewModel::class.java]
        toolbar = findViewById(R.id.toolbar)
        toolbarTitle = findViewById(R.id.title)
        title = findViewById(R.id.title)
        tVAddCard = findViewById(R.id.tVAdCard)
        setSupportActionBar(toolbar)
        title.text = ""
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)
    }

    @SuppressLint("SetTextI18n")
    private fun setData() {
        val dataSubscription =
            intent.getParcelableExtra<MySubscriptionsModel>("modelSubscription")

        val imagePath = Constant.getPrefs(this).getString(Constant.dataImagePath, "")
        val profileData = Constant.getArrayListProfile(this, Constant.dataProfile)
        val pricing = "$" + String.format(
            "%.2f",
            (dataSubscription.price.toDouble() - (dataSubscription.discount_price.toDouble() / 100 * dataSubscription.price.toDouble()))
        )

        Glide.with(this).load(imagePath + dataSubscription.feature_image)
            .placeholder(R.mipmap.placeholder_subscription_square).into(roundedImageView)

        if (dataSubscription.free_trial == "No free trial" && dataSubscription.introductory_days == "0") {
            tVTrial.visibility = View.GONE
        } else if (dataSubscription.free_trial != "No free trial") {
            tVTrial.visibility = View.VISIBLE
            tVTrial.text = dataSubscription.free_trial + " free trial"
        } else {
            tVTrial.visibility = View.VISIBLE
            tVTrial.text = dataSubscription.introductory_days + " Days Introductory Price"
        }
        val calculatedDiscount =
            (dataSubscription.discount_price.toDouble() / 100 * dataSubscription.price.toDouble())
        val disPrice = dataSubscription.price.toDouble() - calculatedDiscount
        val creditsAvailable = profileData.wallet.toDouble()
        val finalTax = (dataSubscription.tax.toDouble())
        tax.text = "$" + dataSubscription.tax
        val typeFrequency = dataSubscription.frequency_type
        tvName.text = dataSubscription.campaign_name.trim()
        tvCompanyName.text = dataSubscription.business_name.trim()
        productPrice.text =
            BigDecimal(disPrice).setScale(2, RoundingMode.HALF_UP).toString()
        productOffer.text =
            dataSubscription.discount_price.toDouble().toInt().toString() + "% off"
        subTotal.text = "$" + dataSubscription.price.toDouble().toString()
        var totalPrice = ""

        val separateFrequecy =
            typeFrequency.split("ly".toRegex()).dropLastWhile { it.isEmpty() }
                .toTypedArray()

        if (dataSubscription.free_trial != "No free trial") {
            val separated =
                dataSubscription.free_trial.split(" ".toRegex())
                    .dropLastWhile { it.isEmpty() }
                    .toTypedArray()

            if (separated[1] == "Year" && dataSubscription.frequency_value == "1" || separated[1] == "Years" && dataSubscription.frequency_value == "1") {
                tViewText.text =
                    " You will have " + (separated[0].toDouble() * 365.toDouble()).toInt() + " days to try the product for free.After this period you will be charged " + pricing + " every " + separateFrequecy[0]
            } else if (separated[1] == "Year" && dataSubscription.frequency_value != "1" || separated[1] == "Years" && dataSubscription.frequency_value != "1") {
                tViewText.text =
                    " You will have " + (separated[0].toDouble() * 365.toDouble()).toInt() + " days to try the product for free.After this period you will be charged " + pricing + " every " + dataSubscription.frequency_value + " " + separateFrequecy[0]
            } else if (separated[1] == "Week" && dataSubscription.frequency_value == "1" || separated[1] == "Weeks" && dataSubscription.frequency_value == "1") {
                tViewText.text =
                    " You will have " + (separated[0].toDouble() * 7.toDouble()).toInt() + " days to try the product for free.After this period you will be charged " + pricing + " every " + separateFrequecy[0]
            } else if (separated[1] == "Week" && dataSubscription.frequency_value != "1" || separated[1] == "Weeks" && dataSubscription.frequency_value != "1") {
                tViewText.text =
                    " You will have " + (separated[0].toDouble() * 7.toDouble()).toInt() + " days to try the product for free.After this period you will be charged " + pricing + " every " + dataSubscription.frequency_value + " " + separateFrequecy[0]
            } else if (separated[1] == "Month" && dataSubscription.frequency_value == "1" || separated[1] == "Months" && dataSubscription.frequency_value == "1") {
                tViewText.text =
                    " You will have " + (separated[0].toDouble() * 30.toDouble()).toInt() + " days to try the product for free.After this period you will be charged " + pricing + " every " + separateFrequecy[0]
            } else if (separated[1] == "Month" && dataSubscription.frequency_value != "1" || separated[1] == "Months" && dataSubscription.frequency_value != "1") {
                tViewText.text =
                    " You will have " + (separated[0].toDouble() * 30.toDouble()).toInt() + " days to try the product for free.After this period you will be charged " + pricing + " every " + dataSubscription.frequency_value + " " + separateFrequecy[0]
            }

        } else {
            val textIntroductoryPrice = dataSubscription.introductory_days

            if (textIntroductoryPrice == "1") {
                tViewText.text =
                    " You will have " + textIntroductoryPrice + " day to try the product for $" + dataSubscription.introductory_price + ".After this period you will be charged " + pricing + " every " + separateFrequecy[0]
            } else {
                tViewText.text =
                    " You will have " + textIntroductoryPrice + " days to try the product for $" + dataSubscription.introductory_price + ".After this period you will be charged " + pricing + " every " + separateFrequecy[0]
            }

        }




        if (dataSubscription.redeemtion_cycle_qty == "500000") {
            tvVisits.text = "Unlimited " + dataSubscription.unit
        } else {
            tvVisits.text =
                dataSubscription.redeemtion_cycle_qty + " " + dataSubscription.unit
        }
        frequencyType.text = dataSubscription.frequency_type

        if (dataSubscription.cancel_discount != "0" && dataSubscription.cancel_discount != "") {
            val twentyDiscount =
                calculatedDiscount + (dataSubscription.cancel_discount.toDouble() / 100 * disPrice)
            val disPriceFormated =
                BigDecimal(twentyDiscount).setScale(2, RoundingMode.HALF_UP)
            discountPriceee.text = "$$disPriceFormated"
            typeDiscount = "3"
            if (creditsAvailable >= disPrice) {
                subTotalPrice.text = "$0"
                totalPricee.text =
                    BigDecimal(finalTax).setScale(2, RoundingMode.HALF_UP).toString()
                val creditTobeFormat = disPrice
                val creditTobeApplied =
                    BigDecimal(creditTobeFormat).setScale(2, RoundingMode.HALF_UP)
                creditApplied.text = "$$creditTobeApplied"
                try {
                    val totalPri = "$" + String.format("%.2f", finalTax)
                    totalPrice = totalPri.replace("$", "")
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            } else {
                val ttt = dataSubscription.price.toDouble() - twentyDiscount
                val lastAmount = ttt - creditsAvailable
                val ttPrice = lastAmount + finalTax
                val amountFormated = BigDecimal(ttPrice).setScale(2, RoundingMode.HALF_UP)
                val amountFormated1 =
                    BigDecimal(lastAmount).setScale(2, RoundingMode.HALF_UP)
                discountPriceee.text = "$$disPriceFormated"
                subTotalPrice.text = "$ $amountFormated1"
                totalPricee.text = "$$amountFormated"
                creditApplied.text = "$$creditsAvailable"
                try {
                    val totalPri = "$" + String.format("%.2f", amountFormated)
                    totalPrice = totalPri.replace("$", "")
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }

            }
        } else if (dataSubscription.boom_discount != "0") {
            val twentyDiscount =
                calculatedDiscount + (dataSubscription.boom_discount.toDouble() / 100 * disPrice)
            val disPriceFormated =
                BigDecimal(twentyDiscount).setScale(2, RoundingMode.HALF_UP)
            discountPriceee.text = "$$disPriceFormated"
            typeDiscount = "1"
            if (creditsAvailable >= disPrice) {
                subTotalPrice.text = "$0"
                totalPricee.text =
                    BigDecimal(finalTax).setScale(2, RoundingMode.HALF_UP).toString()
                val creditTobeFormat = disPrice
                val creditTobeApplied =
                    BigDecimal(creditTobeFormat).setScale(2, RoundingMode.HALF_UP)
                creditApplied.text = "$$creditTobeApplied"
                try {
                    val totalPri = "$" + String.format("%.2f", finalTax)
                    totalPrice = totalPri.replace("$", "")
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            } else {
                val ttt = dataSubscription.price.toDouble() - twentyDiscount
                val lastAmount = ttt - creditsAvailable
                val ttPrice = lastAmount + finalTax
                val amountFormated = BigDecimal(ttPrice).setScale(2, RoundingMode.HALF_UP)
                val amountFormated1 =
                    BigDecimal(lastAmount).setScale(2, RoundingMode.HALF_UP)
                discountPriceee.text = "$$disPriceFormated"
                subTotalPrice.text = "$ $amountFormated1"
                totalPricee.text = "$$amountFormated"
                creditApplied.text = "$$creditsAvailable"
                try {
                    val totalPri = "$" + String.format("%.2f", amountFormated)
                    totalPrice = totalPri.replace("$", "")
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }

            }

        }
//        else if (dataSubscription.coupon != "0" && dataSubscription.coupon != "") {
//            val twentyDiscount = calculatedDiscount + (modelSubscription.coupon.toDouble() / 100 * disPrice)
//            val disPriceFormated = BigDecimal(twentyDiscount).setScale(2, RoundingMode.HALF_UP)
//            discountPriceee.text = "$$disPriceFormated"
//            discountedPrice = twentyDiscount.toString()
//            typeDiscount = "2"
//            if (creditsAvailable >= disPrice) {
//                subTotalPrice.text = "$ 0"
//                totalPricee.text =
//                    BigDecimal(finalTax).setScale(2, RoundingMode.HALF_UP).toString()
//                val creditTobeFormat = disPrice
//                val creditTobeApplied =
//                    BigDecimal(creditTobeFormat).setScale(2, RoundingMode.HALF_UP)
//                creditApplied.text = "$$creditTobeApplied"
//                try {
//                    val text = BigDecimal(finalTax).setScale(2, RoundingMode.HALF_UP).toString()
//                    val tttaxxx = (discountedPrice.toDouble() + text.toDouble())
//                    val totalPri = "$" + String.format("%.2f", finalTax)
//                    totalPrice = totalPri.replace("$", "")
//                } catch (e: java.lang.Exception) {
//                    e.printStackTrace()
//                }
//            } else {
//                val ttt = planData.price.toDouble() - twentyDiscount
//                val lastAmount = ttt - creditsAvailable
//                val ttPrice = lastAmount + finalTax
//                val amountFormated = BigDecimal(ttPrice).setScale(2, RoundingMode.HALF_UP)
//                val amountFormated1 = BigDecimal(lastAmount).setScale(2, RoundingMode.HALF_UP)
//                discountPriceee.text = "$$disPriceFormated"
//                discountedPrice = twentyDiscount.toString()
//                subTotalPrice.text = "$ $amountFormated1"
//                totalPricee.text = "$$amountFormated"
//                creditApplied.text = "$$creditsAvailable"
//                try {
//                    val text = BigDecimal(finalTax).setScale(2, RoundingMode.HALF_UP).toString()
//                    val tttaxxx = (discountedPrice.toDouble() + text.toDouble())
//                    val totalPri = "$" + String.format("%.2f", amountFormated)
//                    totalPrice = totalPri.replace("$", "")
//                } catch (e: java.lang.Exception) {
//                    e.printStackTrace()
//                }
//            }
//        }
        else {
            val lastAmount = (disPrice - creditsAvailable).toString()
            val amountFormated = BigDecimal(lastAmount).setScale(2, RoundingMode.HALF_UP)
            subTotalPrice.text = "$ $amountFormated"
            val disPriceFormated =
                BigDecimal(calculatedDiscount).setScale(2, RoundingMode.HALF_UP)
            discountPriceee.text = "$$disPriceFormated"
            val tttaxxx = lastAmount.toDouble() + finalTax
            val amountFormatedd = BigDecimal(tttaxxx).setScale(2, RoundingMode.HALF_UP)
            totalPricee.text = "$$amountFormatedd"
            creditApplied.text = "$$creditsAvailable"
            typeDiscount = ""
            try {
                val lastAmount1 = (disPrice - creditsAvailable)
                val totalPri = "$" + String.format("%.2f", (lastAmount1 + finalTax))
                totalPrice = totalPri.replace("$", "")
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
        }




        tVTrial.setOnClickListener {
            if (status) {
                status = false
                tViewText.visibility = View.VISIBLE
            } else {
                status = true
                tViewText.visibility = View.GONE
            }
        }


        tVAddCard.setOnClickListener {
            startActivity(Intent(this, AddCardActivity::class.java))
        }

        val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
        var totalAmountCredit = ""
        var credit = ""
        if (totalPrice.toDouble() >= profileData.wallet.toDouble()) {
            credit = profileData.wallet
            totalAmountCredit = (totalPrice.toDouble() - credit.toDouble()).toString()
        } else {
            credit = totalPrice
            totalAmountCredit = "0"
        }
        nextCheckout.setOnClickListener {
            var exist = 0
            var id = ""
            for (i in 0 until categoriesList.size) {
                if (categoriesList[i].card) {
                    id = categoriesList[i].id
                    exist = 1
                    break
                }
            }

            if (exist == 0) {
                Toast.makeText(this, "Please select card.", Toast.LENGTH_SHORT).show()
                return@setOnClickListener

            } else {
                progressBar11.visibility = View.VISIBLE
                mViewModel.getRenewCycle(
                    auth,
                    dataSubscription.order_id,
                    id,
                    credit,
                    totalAmountCredit,
                    typeDiscount
                )
            }
        }


    }

    override fun selectedCard(
        position: Int,
        modell: PaymentCardModel
    ) {
        val model = categoriesList[position]
        if (model.card) {
            model.card = false
        } else {
            for (child in categoriesList) {
                child.card = false
            }
            model.card = true
        }
        categoriesList[position] = model
        adapterCards.notifyDataSetChanged()

    }

    override fun editCard(position: Int, model: PaymentCardModel) {


    }

    override fun onResume() {
        super.onResume()
        val mPrefs = getSharedPreferences("ProductArray", MODE_PRIVATE)

        val totalAmounts = mPrefs.getString("addTotal", "")
        if (!totalAmounts.isEmpty()) {
            val editor = mPrefs.edit()
            editor.remove("addTotal")
            editor.apply()
        }

        categoriesList = Constant.cardArrayListData
        val dataSubscription = intent.getParcelableExtra<MySubscriptionsModel>("modelSubscription")
        for (i in 0 until categoriesList.size) {
            categoriesList[i].card = dataSubscription.card_id == categoriesList[i].id
        }

        adapterCards.update(Constant.cardArrayListData)

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }


}
