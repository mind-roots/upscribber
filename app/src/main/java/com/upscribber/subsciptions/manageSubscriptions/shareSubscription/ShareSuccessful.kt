package com.upscribber.subsciptions.manageSubscriptions.shareSubscription

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import com.upscribber.commonClasses.RoundedBottomSheetDialogFragment
import com.upscribber.R


class ShareSuccessful : AppCompatActivity() {

    lateinit var toolbar6: Toolbar
    lateinit var ll_success: LinearLayout
    lateinit var cl_profileData: ConstraintLayout
    lateinit var textView151: TextView
    lateinit var goToSubscription: TextView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_share_successful)
        toolbar6 = findViewById(R.id.toolbar6)
        cl_profileData = findViewById(R.id.cl_profileData)
        textView151 = findViewById(R.id.textView151)
        ll_success = findViewById(R.id.ll_success)
        goToSubscription = findViewById(R.id.goToSubscription)
        setSupportActionBar(toolbar6)
        title = ""
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white)

        if (intent.hasExtra("fromredeemshare")){
            cl_profileData.visibility = View.VISIBLE
            ll_success.visibility = View.GONE
            goToSubscription.text = "UnShare"
            goToSubscription.setOnClickListener {
                val fragment = ShareSuccessful.MenuFragment()
                fragment.show(supportFragmentManager, fragment.tag)
            }

        }else{
            cl_profileData.visibility = View.GONE
            ll_success.visibility = View.VISIBLE
            goToSubscription.setOnClickListener {

                finish()
            }
        }

    }
    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    class MenuFragment : RoundedBottomSheetDialogFragment() {

        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
            val view = inflater.inflate(R.layout.share_sucessful_sheet, container, false)

            val yes = view.findViewById<TextView>(R.id.yes)
            val no = view.findViewById<TextView>(R.id.no)

            yes.setOnClickListener {
                activity!!.finish()
            }
            no.setOnClickListener {
                dismiss()
            }

            return view
        }

    }

}
