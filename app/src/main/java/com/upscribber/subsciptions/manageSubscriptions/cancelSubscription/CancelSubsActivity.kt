package com.upscribber.subsciptions.manageSubscriptions.cancelSubscription

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.format.DateUtils
import android.view.View
import androidx.databinding.DataBindingUtil
import com.upscribber.commonClasses.Constant
import com.upscribber.R
import com.upscribber.databinding.ActivityCancelSubsBinding
import java.text.SimpleDateFormat
import java.util.*

class CancelSubsActivity : AppCompatActivity() {

    lateinit var mBinding : ActivityCancelSubsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this,R.layout.activity_cancel_subs)
        val data = intent.getParcelableArrayListExtra<ModelCancelReasons>("arrayModel")
        mBinding.txtBillingCycle.text = data[0].frequency_type
        if (data[0].redeemtion_cycle_qty > "100"){
            mBinding.txtBillingQuantity.text = "Unlimited " +  data[0].unit
        }else{
            mBinding.txtBillingQuantity.text = data[0].redeemtion_cycle_qty + " " + data[0].unit
        }

        mBinding.amount.text = "$" + data[0].total
        mBinding.textView336.text = data[0].campaign_name
//        mBinding.textView336.text = "$" + data[0].
        mBinding.txtBillingDate.text = Constant.getDateMonthh(data[0].cycle_start_date)


        if (intent.hasExtra("accept")){
            mBinding.textView334.text ="Thank You"
            mBinding.textView335.text =resources.getString(R.string.thank_you_for_continuing)
//            mBinding.imageView54.setImageResource(R.drawable.ic_thanks)

            mBinding.txtBilling.visibility  = View.GONE
            mBinding.txtBillingDate.visibility  = View.GONE

        }else{
            mBinding.textView334.text = resources.getString(R.string.subsrciption_cancelled)
            mBinding.textView335.text ="Your subscription has been canceled."
//            mBinding.imageView54.setImageResource(R.drawable.ic_cross_white_border)
            mBinding.txtBilling.visibility  = View.VISIBLE
            mBinding.txtBillingDate.visibility  = View.VISIBLE
            mBinding.txtBilling.text = "Canceled Date"

        }

        mBinding.doneSharing.setOnClickListener {
            val sharedPreferences = Constant.getSharedPrefs(this)
            val editor = sharedPreferences.edit()
            editor.putString("customer","back")
            editor.apply()
            finish()
        }
    }

}

