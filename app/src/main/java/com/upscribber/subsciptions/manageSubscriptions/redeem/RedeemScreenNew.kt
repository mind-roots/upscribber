package com.upscribber.subsciptions.manageSubscriptions.redeem

import android.content.Context
import android.content.Intent
import android.graphics.Outline
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.*
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.upscribber.commonClasses.Constant
import com.upscribber.R
import com.upscribber.databinding.ActivityRedeemScreenNewBinding
import com.upscribber.commonClasses.RoundedBottomSheetDialogFragment
import com.upscribber.subsciptions.manageSubscriptions.ModelRedeemCode
import com.upscribber.upscribberSeller.customerBottom.scanning.ScanningViewModel
import com.upscribber.upscribberSeller.customerBottom.scanning.customerlistselection.ScanCustomersActivity

@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class RedeemScreenNew : AppCompatActivity() {

    lateinit var mBinding: ActivityRedeemScreenNewBinding
    lateinit var mViewModel: ScanningViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_redeem_screen_new)
        mViewModel = ViewModelProviders.of(this)[ScanningViewModel::class.java]
        setToolbar()
        observerInit()

    }

    private fun observerInit() {
        mViewModel.getmDataStatus().observe(this, Observer {
            if (it.msg == "Invalid auth code") {
                Constant.commonAlert(this)
            } else {
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
                finish()
            }
        })

        mViewModel.getmDataRedeemQuantity().observe(this, Observer {
            startActivity(Intent(this, ReedemCodeConfirmationActivity::class.java).putExtra("modelRedeem",it))
//            val fragment = MenuFragmentRedeem2(it)
//            fragment.show(supportFragmentManager, fragment.tag)
        })

    }

    private fun setToolbar() {
        setSupportActionBar(mBinding.toolbar)
        title = ""
        mBinding.title.text = "Redeem"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onResume() {
        super.onResume()
        val sharedPreferences1 = getSharedPreferences("reedeem", Context.MODE_PRIVATE)
        if (!sharedPreferences1.getString("reedd", "").isEmpty()) {
            val editor = sharedPreferences1.edit()
            editor.remove("reedeem")
            editor.clear()
            editor.apply()
            finish()
        }
    }

    fun customers(v: View) {
        startActivity(Intent(this, ScanCustomersActivity::class.java).putExtra("redeem", "red"))
    }

    fun nextClick(v: View) {
        apiImplimentation()

    }

    private fun apiImplimentation() {
        val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
        mViewModel.getBarcodeQty(auth, mBinding.code.text.toString().trim())

    }


    class MenuFragmentRedeem2(var model : ModelRedeemCode) : RoundedBottomSheetDialogFragment() {

        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
            val view = inflater.inflate(R.layout.redeem_sheet_quantity, container, false)
            var quantity = ""

            val invite = view.findViewById<TextView>(R.id.invite)
            val etQuantity = view.findViewById<TextView>(R.id.etQuantity)
            val imageViewTop = view.findViewById<ImageView>(R.id.imageViewTop)
            val isSeller = Constant.getSharedPrefs(context!!).getBoolean(Constant.IS_SELLER, false)
            if (isSeller) {
                invite.setBackgroundResource(R.drawable.bg_seller_button_blue)
            } else {
                invite.setBackgroundResource(R.drawable.favorite_delete_background)
            }

            roundImage(imageViewTop)
            invite.setOnClickListener {

                dismiss()

            }

            etQuantity.setOnClickListener {
                val choices = getList(model.quantity)
                val charSequenceItems = choices.toArray(arrayOfNulls<CharSequence>(choices.size))
                val mBuilder = AlertDialog.Builder(activity!!)
                mBuilder.setSingleChoiceItems(charSequenceItems, -1) { dialogInterface, i ->
                    val quantityy = choices[i]
                    etQuantity.text = quantityy
                    quantity = quantityy
                    dialogInterface.dismiss()

                }
                val mDialog = mBuilder.create()
                mDialog.show()
            }

            return view
        }

        private fun roundImage(imageViewTop: ImageView) {
            val curveRadius = 50F

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                imageViewTop.outlineProvider = object : ViewOutlineProvider() {

                    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                    override fun getOutline(view: View?, outline: Outline?) {
                        outline?.setRoundRect(
                            0,
                            0,
                            view!!.width,
                            view.height + curveRadius.toInt(),
                            curveRadius

                        )
                    }
                }

                imageViewTop.clipToOutline = true

            }

        }

        private fun getList(quantity: String): ArrayList<String> {
            val arrayList: ArrayList<String> = ArrayList()
            for (i in 0 until quantity.toInt()) {
                arrayList.add((i + 1).toString())
            }
            return arrayList
        }

    }

}



