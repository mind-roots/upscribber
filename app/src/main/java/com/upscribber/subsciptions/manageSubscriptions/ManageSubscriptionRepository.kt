package com.upscribber.subsciptions.manageSubscriptions

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.upscribber.commonClasses.Constant
import com.upscribber.commonClasses.ModelStatusMsg
import com.upscribber.commonClasses.WebServicesCustomers
import com.upscribber.home.ModelHomeLocation
import com.upscribber.subsciptions.creditSubscriptions.CreditSubscriptionModel
import com.upscribber.subsciptions.extrasSubsription.ModelPause
import com.upscribber.subsciptions.manageSubscriptions.shareSubscription.ModelShareData
import com.upscribber.subsciptions.mySubscriptionDetails.MySubscriptionsModel
import com.upscribber.upscribberSeller.dashboardfragment.viewpagerFragments.subscriptions.ModelSubscription
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class ManageSubscriptionRepository(var application: Application) {

    private val mDataReactive = MutableLiveData<ModelPause>()
    private val mDataShare = MutableLiveData<ArrayList<MySubscriptionsModel>>()
    private val mData = MutableLiveData<MySubscriptionsModel>()
    private val mDataRedeem = MutableLiveData<ModelRedeemCode>()
    private val mDataRedeemConfirm = MutableLiveData<ModelRedeemCode>()
    private val mDataCancelRedeem = MutableLiveData<ModelRedeemCode>()
    private val mDataUnshare = MutableLiveData<ModelShareData>()
    val mDataFailure = MutableLiveData<ModelStatusMsg>()
    val mDataSuccess = MutableLiveData<MySubscriptionsModel>()
    val mDataCredit = MutableLiveData<ArrayList<CreditSubscriptionModel>>()

    //------------------------------ Pause Subscription----------------------------------//
    fun getPauseSubscription(auth: String, statuss: String, subscriptionId: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.pauseSubscription(auth, statuss, subscriptionId)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status1 = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        val arrayReactive = ArrayList<MySubscriptionsModel>()
                        val model = MySubscriptionsModel()
                        if (status1 == "true") {
                            val data = json.optJSONArray("data")
                            for (i in 0 until data.length()) {
                                val dataReactivate = data.optJSONObject(i)
                                model.id = dataReactivate.optString("id")
                                model.unit = dataReactivate.optString("unit")
                                model.campaign_id = dataReactivate.optString("campaign_id")
                                model.price = dataReactivate.optString("price")
                                model.discount_price = dataReactivate.optString("discount_price")
                                model.redeemtion_cycle_qty = dataReactivate.optString("redeemtion_cycle_qty")
                                model.introductory_price = dataReactivate.optString("introductory_price")
                                model.introductory_days = dataReactivate.optString("introductory_days")
                                model.merchant_id = dataReactivate.optString("merchant_id")
                                model.campaign_name = dataReactivate.optString("campaign_name")
                                model.merchant_name = dataReactivate.optString("merchant_name")
                                model.feature_image = dataReactivate.optString("feature_image")
                                model.frequency_type = dataReactivate.optString("frequency_type")
                                model.next_billing_date = dataReactivate.optString("next_billing_date")
                                model.frequency_value = dataReactivate.optString("frequency_value")
                                model.created_at = dataReactivate.optString("created_at")
                                model.updated_at = dataReactivate.optString("updated_at")
                                model.free_trial = dataReactivate.optString("free_trial")
                                model.free_trial_qty = dataReactivate.optString("free_trial_qty")
                                model.redeem_count = dataReactivate.optString("redeem_count")
                                model.updated_at = dataReactivate.optString("updated_at")
                                model.status2 = dataReactivate.optString("status")
                                model.share = dataReactivate.optString("share")
                                model.share_setting = dataReactivate.optString("share_setting")
                                model.bar_code = dataReactivate.optString("bar_code")
                                model.quantity = dataReactivate.optString("quantity")
                                model.likes = dataReactivate.optString("likes")
                                model.views = dataReactivate.optString("views")
                                model.bought = dataReactivate.optString("bought")

                                model.msg = msg
                                arrayReactive.add(model)
                                mData.value = model

                            }
                            modelStatus.status = status1
                            modelStatus.msg = msg

                        } else {
                            modelStatus.status = "false"
                            modelStatus.msg = msg
                            mDataFailure.value = modelStatus
                        }


                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus

            }
        })

    }

    fun getmDataFailure(): LiveData<ModelStatusMsg> {
        return mDataFailure
    }


    fun getmDataPause(): LiveData<MySubscriptionsModel> {
        return mData
    }

    //------------------------------ Redeem Subscription----------------------------------//
    fun getRedeemCode(auth: String, id: String, quantity: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.getBarCode(auth, id, quantity)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        val modelRedeemCode = ModelRedeemCode()

                        if (status == "true") {
                            val data = json.optJSONObject("data")
                            modelRedeemCode.created_date = data.optString("created_date")
                            modelRedeemCode.code = data.optString("code")
                            modelRedeemCode.order_id = data.optString("order_id")
                            modelRedeemCode.quantity = data.optString("quantity")
                            modelRedeemCode.created_date = data.optString("created_date")
                            modelRedeemCode.user_id = data.optString("created_date")
                            modelRedeemCode.status = status
                            modelRedeemCode.msg = msg
                            modelStatus.status = status
                            modelStatus.msg = msg
                            mDataRedeem.value = modelRedeemCode
                        } else {
                            modelStatus.status = "false"
                            modelStatus.msg = "Network Error!"
                            mDataFailure.value = modelStatus
                        }


                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus

            }
        })

    }

    fun getmDataRedeem(): LiveData<ModelRedeemCode> {
        return mDataRedeem
    }

    fun getmDataRedeemConfirm(): LiveData<ModelRedeemCode> {
        return mDataRedeemConfirm
    }


    fun getmDataCancelRedeem(): LiveData<ModelRedeemCode> {
        return mDataCancelRedeem
    }


    //------------------------------ Share Subscription----------------------------------//
    fun getShareSubscription(
        auth: String,
        fullName: String,
        contactNumber: String,
        email: String,
        subscriptionId: String,
        share: String
    ) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        var shareable = "0"
        if (share == "0") {
            shareable = "1"
        } else {
            shareable = "0"
        }

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> =
            service.shareSubscription(
                auth,
                fullName,
                contactNumber,
                email,
                subscriptionId,
                shareable
            )
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        val modelShareData = MySubscriptionsModel()
                        val arrayShare = ArrayList<MySubscriptionsModel>()
                        if (status == "true") {
                            val data = json.optJSONArray("data")

//                            if (shareable == "0") {
//                                modelShareData.status = status
//                                modelShareData.msg = msg
//
//                                mDataUnshare.value = modelShareData
//
//                            } else {
                            for (i in 0 until data.length()) {
                                val shareData = data.optJSONObject(i)
                                modelShareData.name = shareData.optString("name")
                                modelShareData.contact_no = shareData.optString("contact_no")
                                modelShareData.total = shareData.optString("total")
                                modelShareData.cycle_start_date =
                                    shareData.optString("cycle_start_date")
                                modelShareData.next_billing_date =
                                    shareData.optString("next_billing_date")
                                modelShareData.share_date = shareData.optString("share_date")
                                modelShareData.service = shareData.optString("service")
                                modelShareData.business_name = shareData.optString("business_name")
                                modelShareData.feature_image = shareData.optString("feature_image")
                                modelShareData.team_member = shareData.optString("team_member")
                                modelShareData.status1 = status
                                modelShareData.msg = msg
                                arrayShare.add(modelShareData)

                            }
                            mDataShare.value = arrayShare

                        } else {
                            modelStatus.msg = msg
                            modelStatus.status = "false"
                            mDataFailure.value = modelStatus
                        }


                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus
            }
        })
    }

    fun getmDataShare(): LiveData<ArrayList<MySubscriptionsModel>> {
        return mDataShare
    }

    fun getUnshare(auth: String, type: String, id: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> =
            service.unShareSubscription(auth, id, type)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        val modelShareData = MySubscriptionsModel()
                        val arrayShare = ArrayList<MySubscriptionsModel>()
                        if (status == "true") {
                            modelShareData.msg = msg
                            modelShareData.status1 = status
                            mDataSuccess.value = modelShareData

                        } else {
                            modelStatus.msg = msg
                            modelStatus.status = status
                            mDataFailure.value = modelStatus
                        }


                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus
            }
        })

    }

    fun getmDataSuccess(): LiveData<MySubscriptionsModel> {
        return mDataSuccess
    }

    fun getCredits(auth: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> =
            service.getCredit(auth)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        val arrayCredit = ArrayList<CreditSubscriptionModel>()
                        if (status == "true") {
                            val data = json.optJSONObject("data")
                            val history = data.optJSONArray("history")

                            for (i in 0 until history.length()) {
                                val creditData = history.optJSONObject(i)
                                val modelCredit = CreditSubscriptionModel()
                                modelCredit.id = creditData.optString("id")
                                modelCredit.customer_id = creditData.optString("customer_id")
                                modelCredit.source_type = creditData.optString("source_type")
                                modelCredit.type = creditData.optString("type")
                                modelCredit.amount = creditData.optString("amount")
                                modelCredit.refer_id = creditData.optString("refer_id")
                                modelCredit.created_at = creditData.optString("created_at")
                                modelCredit.updated_at = creditData.optString("updated_at")
                                arrayCredit.add(modelCredit)
                            }


                            modelStatus.msg = msg
                            modelStatus.status = "true"
                            mDataCredit.value = arrayCredit

                        } else {
                            modelStatus.msg = msg
                            modelStatus.status = "false"
                            mDataFailure.value = modelStatus
                        }


                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus
            }
        })

    }


    fun getCreditData(): LiveData<ArrayList<CreditSubscriptionModel>> {
        return mDataCredit
    }

    fun getCancelRedeem(auth: String, orderId: String, quantity: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.deleteBarCode(auth, orderId, quantity)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        val modelRedeemCode = ModelRedeemCode()

                        if (status == "true") {
                            modelRedeemCode.status = status
                            modelRedeemCode.msg = msg
                            modelStatus.status = status
                            modelStatus.msg = msg
                            mDataCancelRedeem.value = modelRedeemCode
                        } else {
                            modelStatus.status = status
                            modelStatus.msg = msg
                            mDataFailure.value = modelStatus
                        }


                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus

            }
        })

    }

    fun getRedeemResponse(auth: String, orderId: String, barCode: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.redeemResponse(auth, orderId, barCode)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        val modelRedeemCode = ModelRedeemCode()
                        val arrayLocations = ArrayList<ModelHomeLocation>()

                        if (status == "true") {
                            val data = json.optJSONObject("data")
                            val locations = data.optJSONArray("locations")
                            modelRedeemCode.created_at = data.optString("created_at")
                            modelRedeemCode.frequency_type = data.optString("frequency_type")
                            modelRedeemCode.frequency_value = data.optString("frequency_value")
                            modelRedeemCode.unit = data.optString("unit")
                            modelRedeemCode.redeemtion_cycle_qty =
                                data.optString("redeemtion_cycle_qty")
                            modelRedeemCode.assign_to = data.optString("assign_to")
                            modelRedeemCode.price = data.optString("price")
                            modelRedeemCode.campaign_name = data.optString("campaign_name")
                            modelRedeemCode.customer_id = data.optString("customer_id")
                            modelRedeemCode.total_amount = data.optString("total_amount")
                            modelRedeemCode.customer_name = data.optString("customer_name")
                            modelRedeemCode.business_name = data.optString("business_name")
                            modelRedeemCode.location_id = data.optString("location_id")
                            modelRedeemCode.contact_no = data.optString("contact_no")
                            modelRedeemCode.profile_image = data.optString("profile_image")
                            modelRedeemCode.remaining_visits = data.optString("remaining_visits")
                            modelRedeemCode.description = data.optString("description")
                            modelRedeemCode.business_id = data.optString("business_id")
                            modelRedeemCode.business_phone = data.optString("business_phone")
                            modelRedeemCode.team_member = data.optString("team_member")
                            modelRedeemCode.logo = data.optString("logo")
                            modelRedeemCode.rtype = data.optString("rtype")
                            modelRedeemCode.status = status
                            modelRedeemCode.msg = msg
                            modelStatus.status = status
                            modelStatus.msg = msg

                            for (i in 0 until locations.length()) {
                                val locationData = locations.optJSONObject(i)
                                val modelLocation = ModelHomeLocation()
                                modelLocation.id = locationData.optString("id")
                                modelLocation.business_id = locationData.optString("business_id")
                                modelLocation.user_id = locationData.optString("user_id")
                                modelLocation.street = locationData.optString("street")
                                modelLocation.city = locationData.optString("city")
                                modelLocation.state = locationData.optString("state")
                                modelLocation.zip_code = locationData.optString("zip_code")
                                modelLocation.lat = locationData.optString("lat")
                                modelLocation.long = locationData.optString("long")
                                modelLocation.store_timing = locationData.optString("store_timing")
                                modelLocation.created_at = locationData.optString("created_at")
                                modelLocation.updated_at = locationData.optString("updated_at")
                                arrayLocations.add(modelLocation)
                            }

                            modelRedeemCode.locations = arrayLocations
                            mDataRedeemConfirm.value = modelRedeemCode

                        }


                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                mDataFailure.value = modelStatus

            }
        })
    }


}