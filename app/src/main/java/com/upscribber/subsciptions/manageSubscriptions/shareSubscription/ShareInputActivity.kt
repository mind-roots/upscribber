package com.upscribber.subsciptions.manageSubscriptions.shareSubscription

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.graphics.Outline
import android.os.Build
import android.os.Bundle
import android.provider.ContactsContract
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.upscribber.commonClasses.RoundedBottomSheetDialogFragment
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.subsciptions.manageSubscriptions.ManageSubscriptionViewModel
import com.upscribber.subsciptions.manageSubscriptions.ManageSubscriptionsActivity
import com.upscribber.subsciptions.mySubscriptionDetails.MySubscriptionsModel
import kotlinx.android.synthetic.main.activity_share_input.*
import java.math.BigDecimal
import java.math.RoundingMode

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class ShareInputActivity : AppCompatActivity() {

    private var subscriptionmodel = MySubscriptionsModel()
    lateinit var shareToolbar: Toolbar
    lateinit var goToSubscription: TextView
    lateinit var ivLogo: ImageView
    lateinit var merchantName: TextView
    lateinit var txtFullName: EditText
    lateinit var textPrice: TextView
    lateinit var etPhone: EditText
    lateinit var txtEmail: EditText
    lateinit var constraintLayout13: ConstraintLayout
    val status: Boolean = true
    lateinit var fragment: MenuFragment
    lateinit var mViewModel: ManageSubscriptionViewModel
    private var dataSubModel = MySubscriptionsModel()
    var flag = 0
    private val PICK_CONTACT = 1000


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_share_input)
        mViewModel = ViewModelProviders.of(this)[ManageSubscriptionViewModel::class.java]
        init()
        setToolbar()
        clickListeners()
        setData()
        setButtonVisible()
        observerInit()
        if (intent.hasExtra("model")) {
            dataSubModel = intent.getParcelableExtra("model")
        }

    }

    private fun setButtonVisible() {
        txtFullName.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(
                s: CharSequence,
                start: Int,
                count: Int,
                after: Int
            ) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (getTextFieldsData()) {
                    goToSubscription.setBackgroundResource(R.drawable.invite_button_background)
                } else {
                    goToSubscription.setBackgroundResource(R.drawable.invite_button_background_opacity)
                }
            }

            override fun afterTextChanged(s: Editable) {
            }
        })
        etPhone.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(
                s: CharSequence,
                start: Int,
                count: Int,
                after: Int
            ) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                val text = etPhone.text.toString()
                val textLength = etPhone.text.length

                if (text.endsWith("-") || text.endsWith(" ") || text.endsWith(" "))
                    return

                if (textLength == 4) {
                    etPhone.setText(StringBuilder(text).insert(text.length - 1, " ").toString())
                    etPhone.setSelection(etPhone.text.length)
                }
                if (textLength == 8) {
                    etPhone.setText(StringBuilder(text).insert(text.length - 1, " ").toString())
                    etPhone.setSelection(etPhone.text.length)

                }

                if (getTextFieldsData()) {
                    goToSubscription.setBackgroundResource(R.drawable.invite_button_background)
                } else {
                    goToSubscription.setBackgroundResource(R.drawable.invite_button_background_opacity)
                }
            }

            override fun afterTextChanged(s: Editable) {
            }
        })


        txtEmail.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(
                s: CharSequence,
                start: Int,
                count: Int,
                after: Int
            ) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (getTextFieldsData()) {
                    goToSubscription.setBackgroundResource(R.drawable.invite_button_background)
                } else {
                    goToSubscription.setBackgroundResource(R.drawable.invite_button_background_opacity)
                }
            }

            override fun afterTextChanged(s: Editable) {
            }
        })

    }

    private fun getTextFieldsData(): Boolean {
        val txtFullName = txtFullName.text.toString().trim().length
        val etPhone = etPhone.text.toString().trim().length

        if (txtFullName == 0) {
            return false
        }
        if (etPhone < 12) {
            return false
        }
        return true
    }

    private fun clickListeners() {
        info1.setOnClickListener {
            Constant.CommonIAlert(
                this,
                "Share Subscription",
                "You must have at least one unused redemption to share. When you share a subscription, your recepient will get only one redemption. Once the recepient redeems the subscription, then you may share another redemption."
            )
        }

    }

    private fun observerInit() {
        mViewModel.getmDataShare().observe(this, Observer {
            if (it.size > 0) {
                progressBar20.visibility = View.GONE
                if (intent.hasExtra("mySubscription")) {
                    val intent =
                        Intent(this, ShareSubscriptionCustomerActivity::class.java).putExtra(
                            "model", it
                        ).putExtra(
                            "mySubscription",
                            intent.getParcelableExtra<MySubscriptionsModel>("mySubscription")
                        )
                    startActivity(intent)
                    finish()
                } else {
                    val intent =
                        Intent(this, ShareSubscriptionCustomerActivity::class.java).putExtra(
                            "model",
                            it
                        ).putExtra("baseModel", subscriptionmodel)
                    startActivity(intent)
                    finish()
                }
            }


        })

        mViewModel.getmDataStatus().observe(this, Observer {
            progressBar20.visibility = View.GONE
            if (it.msg == "Invalid auth code") {
                Constant.commonAlert(this)
            } else {
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
            }
        })


    }


    fun selectContacts(v: View) {
        val intent1 = Intent(Intent.ACTION_PICK)
        intent1.type = ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE
        startActivityForResult(intent1, PICK_CONTACT)
    }


    @SuppressLint("Recycle")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == PICK_CONTACT) {
            val contactData = data!!.data
            val projection = arrayOf(
                ContactsContract.CommonDataKinds.Phone.NUMBER,
                ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME
            )
            val cursor = contentResolver.query(contactData, projection, null, null, null)
            cursor!!.moveToFirst()
            val numberColumn = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)
            val nameColumn =
                cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)
            val number = cursor.getString(numberColumn)
            val name = cursor.getString(nameColumn)
            txtFullName.setText(name)
            etPhone.setText(Constant.getSplitPhoneNumberWithoutCode(number, this))

        }


    }

    @SuppressLint("SetTextI18n")
    private fun setData() {
        val model = intent.getParcelableExtra<MySubscriptionsModel>("model")
        val imagePath = Constant.getPrefs(this).getString(Constant.dataImagePath, "")
        Glide.with(this).load(imagePath + model.feature_image).into(ivLogo)
        merchantName.text = model.campaign_name

        if(model.price.contains(".")){
            val splitPos = model.price.split(".")
            if (splitPos[1] == "00" || splitPos[1] == "0") {
                textPrice.text = model.unit + " for" + " $" + splitPos[0]
            } else {
                textPrice.text = model.unit + " for" + " $" + BigDecimal(model.price).setScale(2, RoundingMode.HALF_UP)
            }
        }else{
            textPrice.text = model.unit + " for" + " $" + BigDecimal(model.price).setScale(2, RoundingMode.HALF_UP)
        }



        if (model.redeemtion_cycle_qty > "100") {
            tvLogoNum.text = resources.getString(R.string.infinity)
        } else {
            tvLogoNum.text = model.redeem_count
        }

    }

    private fun setToolbar() {
        setSupportActionBar(shareToolbar)
        title = ""
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)

    }


    fun init() {
        shareToolbar = findViewById(R.id.shareToolbar)
        goToSubscription = findViewById(R.id.goToSubscription)
        ivLogo = findViewById(R.id.ivLogo)
        merchantName = findViewById(R.id.merchantName)
        constraintLayout13 = findViewById(R.id.constraintLayout13)
        textPrice = findViewById(R.id.textPrice)
        txtFullName = findViewById(R.id.txtFullName)
        etPhone = findViewById(R.id.etPhone)
        txtEmail = findViewById(R.id.txtEmail)
        val modelSubscription = intent.getParcelableExtra<MySubscriptionsModel>("model")
        if (modelSubscription.share == "1") {
            goToSubscription.text = "Unshare"
        } else {
            goToSubscription.text = "Share"
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            if (intent.hasExtra("manage")) {
                val intent = Intent(this, ManageSubscriptionsActivity::class.java)
                intent.putExtra("modelData", dataSubModel)
                startActivity(intent)
                finish()
            } else {
                onBackPressed()
            }

        }
        return super.onOptionsItemSelected(item)
    }

    fun shareData(v: View) {
        val profileData = Constant.getArrayListProfile(this, Constant.dataProfile)
        val number = etPhone.text.toString().trim()
        val phoneNumber = number.replace(" ", "").replace("(","").replace(")","").replace("+91","").replace("+1","")

        if (txtFullName.text.toString().isEmpty() && etPhone.text.toString().isEmpty()) {
            Toast.makeText(this, "All fields are mandatory", Toast.LENGTH_LONG).show()
        } else if (txtFullName.text.toString().isEmpty()) {
            Toast.makeText(this, "Please enter your full name", Toast.LENGTH_LONG).show()
        } else if (phoneNumber.isEmpty()) {
            Toast.makeText(this, "Please enter 10 digit phone number", Toast.LENGTH_LONG).show()
        }  else if (profileData.contact_no.trim() == phoneNumber) {
            Toast.makeText(this, "You cannot share subscription with yourself!", Toast.LENGTH_LONG)
                .show()
        } else {
            val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
            subscriptionmodel = intent.getParcelableExtra("model")
            mViewModel.getShareSubcription(
                auth,
                txtFullName.text.toString().trim(),
                "+1" + phoneNumber,
                txtEmail.text.toString().trim(),
                subscriptionmodel.order_id,
                subscriptionmodel.share
            )
            progressBar20.visibility = View.VISIBLE
        }


    }

    fun getRedeem(v: View) {
//         fragment = MenuFragment()
//        fragment.show(supportFragmentManager, fragment.tag)
    }

    class MenuFragment : RoundedBottomSheetDialogFragment() {

        lateinit var rvRedeem: RecyclerView
        lateinit var cl_redeem: ConstraintLayout
        lateinit var image: ImageView

        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val view = inflater.inflate(R.layout.redeem_sheet, container, false)
            rvRedeem = view.findViewById(R.id.rvRedeem)
            image = view.findViewById(R.id.topBg)
            cl_redeem = view.findViewById(R.id.cl_redeem)
            setAdapter()
            roundedImage()
            return view
        }

        private fun setAdapter() {

            rvRedeem.layoutManager = LinearLayoutManager(activity)
            rvRedeem.adapter = AdapterShareRedeem(this.activity!!)

        }

        private fun roundedImage() {
            val curveRadius = 50F

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                image.outlineProvider = object : ViewOutlineProvider() {

                    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                    override fun getOutline(view: View?, outline: Outline?) {
                        outline?.setRoundRect(
                            0,
                            0,
                            view!!.width,
                            view.height + curveRadius.toInt(),
                            curveRadius
                        )
                    }
                }
                image.clipToOutline = true
            }
        }
    }
}