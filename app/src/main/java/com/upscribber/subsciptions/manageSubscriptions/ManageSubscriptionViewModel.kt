package com.upscribber.subsciptions.manageSubscriptions

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.upscribber.commonClasses.ModelStatusMsg
import com.upscribber.subsciptions.creditSubscriptions.CreditSubscriptionModel
import com.upscribber.subsciptions.extrasSubsription.ModelPause
import com.upscribber.subsciptions.manageSubscriptions.shareSubscription.ModelShareData
import com.upscribber.subsciptions.mySubscriptionDetails.MySubscriptionsModel

class ManageSubscriptionViewModel(application: Application) : AndroidViewModel(application) {


    var manageSubscriptionRepository: ManageSubscriptionRepository =
        ManageSubscriptionRepository(application)


    fun getPauseSubscription(
        auth: String,
        status: String,
        subscriptionId: String,
        model: MySubscriptionsModel
    ) {
        manageSubscriptionRepository.getPauseSubscription(auth, status, subscriptionId)

    }

    fun getmDataStatus(): LiveData<ModelStatusMsg> {
        return manageSubscriptionRepository.getmDataFailure()
    }

    fun getmDataPause(): LiveData<MySubscriptionsModel> {
        return manageSubscriptionRepository.getmDataPause()
    }


    fun getmDataRedeem(): LiveData<ModelRedeemCode> {
        return manageSubscriptionRepository.getmDataRedeem()
    }

    fun getmDataCancelRedeem(): LiveData<ModelRedeemCode> {
        return manageSubscriptionRepository.getmDataCancelRedeem()
    }


    fun getmDataRedeemConfirm(): LiveData<ModelRedeemCode> {
        return manageSubscriptionRepository.getmDataRedeemConfirm()
    }


    fun getRedeemSubscription(auth: String, id: String, quantity: String) {
        manageSubscriptionRepository.getRedeemCode(auth, id, quantity)
    }

    fun getShareSubcription(
        auth: String,
        fullName: String,
        contactNumber: String,
        email: String,
        subscriptionId: String,
        share: String
    ) {

        manageSubscriptionRepository.getShareSubscription(
            auth,
            fullName,
            contactNumber,
            email,
            subscriptionId,
            share
        )
    }

    fun getmDataShare(): LiveData<ArrayList<MySubscriptionsModel>> {
        return manageSubscriptionRepository.getmDataShare()
    }

    fun getUnshare(auth: String, type: String, id: String) {
        manageSubscriptionRepository.getUnshare(auth, type, id)

    }

    fun getmDataState(): LiveData<MySubscriptionsModel> {
        return manageSubscriptionRepository.getmDataSuccess()
    }

    fun getCreditData(): LiveData<ArrayList<CreditSubscriptionModel>> {
        return manageSubscriptionRepository.getCreditData()
    }


    fun getCreditHistory(auth: String) {
        manageSubscriptionRepository.getCredits(auth)

    }

    fun cancelRedeem(auth: String, orderId: String, quantity: String) {
        manageSubscriptionRepository.getCancelRedeem(auth, orderId, quantity)

    }

    fun getRedeemResponse(auth: String, orderId: String, barCode: String) {
        manageSubscriptionRepository.getRedeemResponse(auth, orderId, barCode)
    }

}