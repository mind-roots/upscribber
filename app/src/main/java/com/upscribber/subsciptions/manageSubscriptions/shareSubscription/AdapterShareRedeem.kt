package com.upscribber.subsciptions.manageSubscriptions.shareSubscription

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import kotlinx.android.synthetic.main.redeem_share_recycler.view.ivLogo
import kotlinx.android.synthetic.main.redeem_share_recycler.view.remainingQuantity
import kotlinx.android.synthetic.main.redeem_share_recycler.view.tvFab
import kotlinx.android.synthetic.main.redeem_share_recycler.view.tvLogoNum
import kotlinx.android.synthetic.main.redeem_share_recycler.view.tvRedeemCost
import kotlinx.android.synthetic.main.subscription_recycler_layout_invite.view.*

class AdapterShareRedeem
    (val mContext: Context) : RecyclerView.Adapter<AdapterShareRedeem.MyViewHolder>() {


    var hideLayout = mContext as HideLayout
    var list: ArrayList<ModelShareRedeem> = ArrayList()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(mContext)
            .inflate(R.layout.subscription_recycler_layout_invite, parent, false)

        return MyViewHolder(v)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = list[position]

        val imagePath  = Constant.getPrefs(mContext).getString(Constant.dataImagePath, "")
        Glide.with(mContext).load(imagePath + model.campaign_image).placeholder(R.mipmap.placeholder_subscription_square).into(holder.itemView.ivLogo)
        holder.itemView.tvLogoNum.text = model.redeemtion_cycle_qty
        holder.itemView.tvFab.text = model.campaign_name
        holder.itemView.tvRedeemCost.text = model.business_name
//        holder.itemView.tvRedeem.text = model.frequency_type + "ly"

        if (model.redeemtion_cycle_qty > "100"){
            holder.itemView.remainingQuantity.text =  "Up to Unlimited "  + model.unit + " Billed " + model.frequency_type
        }else{
            holder.itemView.remainingQuantity.text =  "Up to " + model.redeemtion_cycle_qty + " " + model.unit + " Billed " + model.frequency_type

        }


        holder.itemView.setOnClickListener {
            hideLayout.dismissSheet()
            hideLayout.setData(position,model)

        }



    }

    fun update(it: java.util.ArrayList<ModelShareRedeem>?) {
        list = it!!
        notifyDataSetChanged()

    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    interface HideLayout{
        fun dismissSheet()
        fun setData(position: Int, model: ModelShareRedeem)
    }
}
