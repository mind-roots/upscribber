package com.upscribber.subsciptions.manageSubscriptions.redeem

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.zxing.BarcodeFormat
import com.google.zxing.MultiFormatWriter
import com.google.zxing.WriterException
import com.journeyapps.barcodescanner.BarcodeEncoder
import com.upscribber.commonClasses.Constant
import com.upscribber.R
import com.upscribber.subsciptions.manageSubscriptions.ManageSubscriptionViewModel
import com.upscribber.subsciptions.mySubscriptionDetails.MySubscriptionsModel
import kotlinx.android.synthetic.main.activity_reedem.*
import kotlinx.android.synthetic.main.alert_redeem_quantity.*
import java.util.*

class ReedemActivity : AppCompatActivity() {

    lateinit var toolbar3: Toolbar
    lateinit var toolbarTitle: TextView
    lateinit var textView: TextView
    lateinit var mViewModel: ManageSubscriptionViewModel
    val multiFormatWriter = MultiFormatWriter()
    var codeRedeem = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reedem)
        mViewModel = ViewModelProviders.of(this)[ManageSubscriptionViewModel::class.java]
        setToolbar()
        sppannable()
        apiForConfirmation()
        observerInit()
        clickListeners()
        setData()
    }

    private fun apiForConfirmation() {
        val model = intent.getParcelableExtra<MySubscriptionsModel>("model")
        val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
        val t = Timer()
        //Set the schedule function and rate
        t.scheduleAtFixedRate(
            object : TimerTask() {
                override fun run() {
                    if (model.bar_code.isNotEmpty()) {
                        mViewModel.getRedeemResponse(auth, model.order_id, model.bar_code)
                    } else {
                        mViewModel.getRedeemResponse(auth, model.order_id, codeRedeem)
                    }

                }
            },
            0,
            3000
        )

    }

    private fun clickListeners() {
        buttonDone.setOnClickListener {
            val model = intent.getParcelableExtra<MySubscriptionsModel>("model")
            val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
            mViewModel.cancelRedeem(auth, model.order_id, model.bar_code)
            progressBar16.visibility = View.VISIBLE
        }

    }

    private fun setData() {
        val model = intent.getParcelableExtra<MySubscriptionsModel>("model")

        val splitPos = model.price.split(".")
        if (splitPos[1] == "00" || splitPos[1] == "0") {
            textDescription.text = "Enjoying " + model.unit + " for $" + splitPos[0]
        } else {
            textDescription.text = "Enjoying " + model.unit + " for $" + model.price
        }


        if (model.redeemtion_cycle_qty > "100") {
            textVisits.text = "Unlimited left"
        } else {
            textVisits.text = model.redeemtion_cycle_qty + " left"

        }
        textCampaignName.text = model.campaign_name
//        if (model.bar_code.isNotEmpty()) {
//            textCode.text = model.bar_code
//            buttonDone.visibility = View.VISIBLE
//            val mDialogView =
//                LayoutInflater.from(this).inflate(R.layout.alert_redeem_quantity, null)
//            val mBuilder = AlertDialog.Builder(this)
//                .setView(mDialogView)
//            val mAlertDialog = mBuilder.show()
//            mAlertDialog.setCancelable(false)
//            mAlertDialog.window.setBackgroundDrawableResource(R.drawable.bg_alert)
//            mAlertDialog.okBtn.setOnClickListener {
//                mAlertDialog.dismiss()
//            }
//
//            val generateCode = model.bar_code + "_" + model.quantity + "_" + model.order_id
//            try {
//                val bitMatrix =
//                    multiFormatWriter.encode(generateCode, BarcodeFormat.QR_CODE, 3000, 3000)
//                val barcodeEncoder = BarcodeEncoder()
//                val bitmap = barcodeEncoder.createBitmap(bitMatrix)
//                imageViewQRCode.setImageBitmap(bitmap)
//            } catch (e: WriterException) {
//                e.printStackTrace()
//            }
//
//        } else {
        apiImplimentation()
        buttonDone.visibility = View.GONE
//        }

    }

    private fun observerInit() {
        mViewModel.getmDataRedeem().observe(this, Observer {
            if (it.status == "true") {
                progressBar16.visibility = View.GONE
                textCode.text = it.code
                codeRedeem = it.code
                val generateCode = it.code + "_" + it.quantity + "_" + it.order_id
                try {
                    val bitMatrix =
                        multiFormatWriter.encode(generateCode, BarcodeFormat.QR_CODE, 2500, 2500)
                    val barcodeEncoder = BarcodeEncoder()
                    val bitmap = barcodeEncoder.createBitmap(bitMatrix)
                    imageViewQRCode.setImageBitmap(bitmap)
                } catch (e: WriterException) {
                    e.printStackTrace()
                }

            }
        })

        mViewModel.getmDataCancelRedeem().observe(this, Observer {
            if (it.status == "true") {
                progressBar16.visibility = View.GONE
                finish()
            }

        })

        mViewModel.getmDataRedeemConfirm().observe(this, Observer {
            if (it.status == "true") {
                startActivity(
                    Intent(this, SuccessfullyRedemCustomer::class.java).putExtra(
                        "model",
                        it
                    )
                )
                finish()
            }

        })




        mViewModel.getmDataStatus().observe(this, Observer {
            if (it.msg == "Invalid auth code") {
                Constant.commonAlert(this)
            } else {
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
            }
        })


    }

    private fun apiImplimentation() {
        val subscriptionId = intent.getParcelableExtra<MySubscriptionsModel>("model")
        val quantity = intent.getStringExtra("quantity")
        val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
        mViewModel.getRedeemSubscription(auth, subscriptionId.order_id, quantity)
        progressBar16.visibility = View.VISIBLE

    }

    private fun sppannable() {
        val model = intent.getParcelableExtra<MySubscriptionsModel>("model")
        val quantity = intent.getStringExtra("quantity")

        if (quantity == "1"){
            val spannableString = SpannableString("Redeeming " + quantity + " " + model.unit)
            spannableString.setSpan(
                ForegroundColorSpan(Color.RED),
                10, model.quantity.length + 10,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            )
            textView.text = spannableString
        }else{
            val spannableString = SpannableString("Redeeming " + quantity + " " + model.unit+ "s")
            spannableString.setSpan(
                ForegroundColorSpan(Color.RED),
                10, model.quantity.length + 10,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            )
            textView.text = spannableString
        }

    }

    private fun setToolbar() {
        toolbar3 = findViewById(R.id.include10)
        textView = findViewById(R.id.textView)
        toolbarTitle = findViewById(R.id.title)
        setSupportActionBar(toolbar3)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)
        toolbarTitle.text = ""

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}