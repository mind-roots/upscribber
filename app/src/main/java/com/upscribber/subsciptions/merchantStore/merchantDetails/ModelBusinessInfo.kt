package com.upscribber.subsciptions.merchantStore.merchantDetails

class ModelBusinessInfo {

    var twitter: String = ""
    var facebook: String = ""
    var instagram: String = ""
    var default_location: String = ""
    var about: String = ""
    var id: String = ""
    var business_name: String = ""
    var logo: String = ""
    var likes: String = ""
    var views: String = ""
    var bought: String = ""

}
