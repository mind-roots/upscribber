package com.upscribber.subsciptions.merchantStore.merchantDetails

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.Gravity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.kingfisher.easyviewindicator.RecyclerViewIndicator
import com.upscribber.R
import com.upscribber.categories.subCategories.SubCategoriesActivity
import com.upscribber.commonClasses.Constant
import com.upscribber.home.MapsPagerAdapter
import com.upscribber.subsciptions.merchantStore.*
import com.upscribber.subsciptions.merchantSubscriptionPages.similarBrands.SimilarBrandsActivity
import com.upscribber.subsciptions.reviews.ReviewActivity
import kotlinx.android.synthetic.main.activity_merchant_store.*
import kotlinx.android.synthetic.main.layout_information.*

@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class MerchantStoreActivity : AppCompatActivity() {

    private lateinit var mAdapterOperationHours: AdapterWorkingDays
    private lateinit var mrecyclerSubsList: RecyclerView
    private lateinit var mrecyclerSimilarList: RecyclerView
    private lateinit var viewPagerMaps: RecyclerView
    private lateinit var indicatorMaps: RecyclerViewIndicator
    private lateinit var tVSubscription: TextView
    private lateinit var tVInformation: TextView
    private lateinit var recyclerViewStaff: RecyclerView
    private lateinit var recyclerViewReviews: RecyclerView
    private lateinit var recyclerViewReviewDescription: RecyclerView
    private lateinit var recyclerWorkingDays: RecyclerView
    private lateinit var reviewViewAll: TextView
    private lateinit var toolbarTitle: TextView
    private lateinit var textView95: TextView
    private lateinit var call: TextView
    private lateinit var website: TextView
    private lateinit var directions: TextView
    private lateinit var textSimilar: TextView
    private lateinit var textView356: TextView
    lateinit var mChip: Chip
    lateinit var rvTag: ChipGroup
    var arrayTags: ArrayList<ModeTags> = ArrayList()
    lateinit var mViewModel: MerchantInfoViewModel
    lateinit var mAdapterSubscriptions: AdapterTwo
    lateinit var mAdapterSimilarSubscriptions: AdapterSimilarProducts
    lateinit var mAdapterStaffList: AdapterStaffList
    lateinit var mAdapterReviewsCount: AdapterReviews
    lateinit var mAdapterReviewsDescription: AdapterReviewDescription
    var modelfeatures: ModelBusinessInfo = ModelBusinessInfo()
    var myCustomPagerAdapter: MapsPagerAdapter? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_merchant_store)
        mViewModel = ViewModelProviders.of(this)[MerchantInfoViewModel::class.java]
        initz()
        setToolbar()
        clickEvents()
        setAdapter()
        apiInitiliazation()
        observerInitz()
    }

    private fun observerInitz() {


        mViewModel.getmDataSubscriptions().observe(this, Observer {
            progressBar6.visibility = View.GONE
            mAdapterSubscriptions.update(it)
        })

        mViewModel.getmDataSimilarSubscriptions().observe(this, Observer {
            //            progressBar6.visibility = View.GONE
//            if (it.size > 0){
//                textSimilar.visibility = View.GONE
//            }else{
//                textSimilar.visibility = View.VISIBLE
//            }
//
//            if(it.size > 5){
//                textView356.visibility = View.VISIBLE
//            }else{
//                textView356.visibility = View.GONE
//            }
//
//            mAdapterSimilarSubscriptions.update(it)
        })

        mViewModel.getmDataStaffList().observe(this, Observer {
            progressBar6.visibility = View.GONE
            mAdapterStaffList.update(it)
        })

        mViewModel.mDataBrandReviewsDescription().observe(this, Observer {
            progressBar6.visibility = View.GONE
            if (it.size >= 4) {
                reviewViewAll.visibility = View.VISIBLE
            } else {
                reviewViewAll.visibility = View.GONE
            }

            mAdapterReviewsDescription.update(it)
        })

        mViewModel.getmDataTags().observe(this, Observer {
            progressBar6.visibility = View.GONE
            arrayTags = ArrayList()
            rvTag.removeAllViews()
            chipData(it)

        })

        mViewModel.getmDataReviews().observe(this, Observer {
            mAdapterReviewsCount.update(it)
        })

        mViewModel.getmDataContacts().observe(this, Observer {
            progressBar6.visibility = View.GONE
            val calll = it.call
            val latlong = it.lat + "," + it.long
            val web = it.website

            call.setOnClickListener {
                startActivity(Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", calll, null)))
            }

            website.setOnClickListener {
                /* val emailIntent = Intent(
                     Intent.ACTION_SENDTO, Uri.fromParts(
                         "mailto", web, null
                     )
                 )
                 emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Feedback")
                 emailIntent.putExtra(
                     Intent.EXTRA_TEXT,
                     ""
                 )
                 this.startActivity(Intent.createChooser(emailIntent, "Send email"))*/
                if (web.isNotEmpty()) {
                    try {
                        if (!web.contains("http") || !web.contains("https")) {
                            val browserIntent =
                                Intent(Intent.ACTION_VIEW, Uri.parse("https://$web"))
                            startActivity(browserIntent)
                        } else {
                            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(web))
                            startActivity(browserIntent)
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }
            directions.setOnClickListener {
                val intent = Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("google.navigation:q=$latlong")
                )
                startActivity(intent)
            }

        })

        mViewModel.getmDataBusinessInfo().observe(this, Observer {
            progressBar6.visibility = View.GONE
            val imagePath = Constant.getPrefs(this).getString(Constant.dataImagePath, "")
            modelfeatures = it
            Glide.with(this).load(imagePath + it.logo).placeholder(R.mipmap.circular_placeholder)
                .into(imgView109)
            title1.text = it.business_name
            textView107.text = it.likes + "%"
            textView105.text = it.bought
            textView111.text = it.views

            if (it.about == "") {
                whatsInclude.visibility = View.GONE
                tvAbout.visibility = View.GONE
                view14.visibility = View.GONE
            } else {
                whatsInclude.visibility = View.VISIBLE
                tvAbout.visibility = View.VISIBLE
                view14.visibility = View.VISIBLE
                whatsInclude.text = it.about
            }

            imgFacebook.setOnClickListener{
                if (modelfeatures.facebook.isNotEmpty()){
                    try {
                        if (!modelfeatures.facebook.contains("http") || !modelfeatures.facebook.contains("https")) {
                            val browserIntent =
                                Intent(Intent.ACTION_VIEW, Uri.parse("https://" + modelfeatures.facebook))
                            startActivity(browserIntent)
                        } else {
                            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(modelfeatures.facebook))
                            startActivity(browserIntent)
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                }else{
                    Constant.commonErrorAlert(this, "Link is not available.")
                }
            }

            imgInstagram.setOnClickListener{
                if (modelfeatures.instagram.isNotEmpty()){
                    try {
                        if (!modelfeatures.instagram.contains("http") || !modelfeatures.instagram.contains("https")) {
                            val browserIntent =
                                Intent(Intent.ACTION_VIEW, Uri.parse("https://" + modelfeatures.instagram))
                            startActivity(browserIntent)
                        } else {
                            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(modelfeatures.instagram))
                            startActivity(browserIntent)
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                }else{
                    Constant.commonErrorAlert(this, "Link is not available.")
                }
            }

            imgTwitter.setOnClickListener{
                if (modelfeatures.twitter.isNotEmpty()){
                    try {
                        if (!modelfeatures.twitter.contains("http") || !modelfeatures.twitter.contains("https")) {
                            val browserIntent =
                                Intent(Intent.ACTION_VIEW, Uri.parse("https://" + modelfeatures.twitter))
                            startActivity(browserIntent)
                        } else {
                            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(modelfeatures.twitter))
                            startActivity(browserIntent)
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                }else{
                    Constant.commonErrorAlert(this, "Link is not available.")
                }
            }

        })

        mViewModel.getStatus().observe(this, Observer {
            progressBar6.visibility = View.GONE
            if (it.msg == "Invalid auth code") {
                Constant.commonAlert(this)
            } else {
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
            }

        })

        mViewModel.getmDataLocation().observe(this, Observer {
            viewPagerMaps.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
            myCustomPagerAdapter = MapsPagerAdapter(this, Constant.getDisplayMetrics(windowManager))
            myCustomPagerAdapter!!.setItemMargin(16)
            myCustomPagerAdapter!!.updateDisplayMetrics()
            indicatorMaps.setRecyclerView(viewPagerMaps)
            indicatorMaps.forceUpdateItemCount()
//            PagerSnapHelper().attachToRecyclerView(viewPagerMaps)
            viewPagerMaps.adapter = myCustomPagerAdapter
            myCustomPagerAdapter!!.update(it)
            mAdapterOperationHours.update(it)
        })


    }

    private fun apiInitiliazation() {
        val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
        val model = intent.getStringExtra("model")
        progressBar6.visibility = View.VISIBLE
        mViewModel.getMerchant(auth, model)

    }


    private fun initz() {
        mrecyclerSubsList = findViewById(R.id.recyclerSubsList)
        call = findViewById(R.id.callText)
        directions = findViewById(R.id.direc)
        website = findViewById(R.id.website)
        viewPagerMaps = findViewById(R.id.viewPagerMaps)
        indicatorMaps = findViewById(R.id.indicatorMaps)
        textSimilar = findViewById(R.id.textSimilar)
        textView356 = findViewById(R.id.textView356)
        mrecyclerSimilarList = findViewById(R.id.recyclerSimilarList)
        tVSubscription = findViewById(R.id.tVSubscription)
        tVInformation = findViewById(R.id.tVInformation)
        recyclerViewStaff = findViewById(R.id.recyclerViewStaff)
        recyclerViewReviews = findViewById(R.id.recyclerViewReviews)
        recyclerViewReviewDescription = findViewById(R.id.recyclerViewReviewDescription)
        recyclerWorkingDays = findViewById(R.id.recyclerWorkingDays)
        mChip = findViewById(R.id.chip)
        rvTag = findViewById(R.id.rvTags)
        reviewViewAll = findViewById(R.id.reviewViewAll)
        toolbarTitle = findViewById(R.id.title)
        textView95 = findViewById(R.id.textView95)

    }

    private fun setToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.mstoreToolbar)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        title = ""
        toolbarTitle.text = "Store"
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)
    }


    private fun clickEvents() {
        tVSubscription.setOnClickListener {
            tVSubscription.setBackgroundResource(R.drawable.bg_info)
            tVInformation.setBackgroundResource(R.drawable.bg_switch_fragments)
            tVSubscription.setTextColor(resources.getColor(R.color.colorWhite))
            tVInformation.setTextColor(resources.getColor(R.color.payments))
            laySubsciption.visibility = View.VISIBLE
            layInformation.visibility = View.GONE

        }

        tVInformation.setOnClickListener {
            tVSubscription.setBackgroundResource(R.drawable.bg_switch_fragments)
            tVInformation.setBackgroundResource(R.drawable.bg_info)
            tVSubscription.setTextColor(resources.getColor(R.color.payments))
            tVInformation.setTextColor(resources.getColor(R.color.colorWhite))
            laySubsciption.visibility = View.GONE
            layInformation.visibility = View.VISIBLE
        }

        reviewViewAll.setOnClickListener {
            val model = intent.getStringExtra("model")
            startActivity(
                Intent(this, ReviewActivity::class.java)
                    .putExtra("modelSubscriptionView", model)
            )

        }
    }


    @SuppressLint("SetTextI18n", "ResourceType")
    private fun chipData(it: ArrayList<ModeTags>) {

        for (i in 0 until it.size) {
            arrayTags.add(it[i])
        }
        for (index in arrayTags.indices) {
            val chip = Chip(rvTag.context)
            chip.text = "${arrayTags[index].name}"

            // necessary to get single selection working
            chip.isClickable = true
            chip.isCheckable = false
            chip.setTextAppearanceResource(R.style.ChipTextStyle_Selected)
            chip.setChipBackgroundColorResource(R.color.colorSkip)
            chip.chipEndPadding = 20.0f
            chip.includeFontPadding = false
            chip.gravity = Gravity.CENTER
            chip.setOnClickListener {
                startActivity(
                    Intent(this, SubCategoriesActivity::class.java)
                        .putExtra("modell", arrayTags[index].name)
                        .putExtra("chipss", arrayTags[index].id)
                )
            }
            rvTag.addView(chip)
        }

        rvTag.isSingleSelection = true

    }


    private fun setAdapter() {
        mrecyclerSimilarList.layoutManager =
            LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
        mrecyclerSimilarList.isNestedScrollingEnabled = false
        mAdapterSimilarSubscriptions = AdapterSimilarProducts(this, 1)
        mrecyclerSimilarList.adapter = mAdapterSimilarSubscriptions


        mrecyclerSubsList.layoutManager = LinearLayoutManager(this)
        mrecyclerSubsList.isNestedScrollingEnabled = false
        mAdapterSubscriptions = AdapterTwo(this)
        mrecyclerSubsList.adapter = mAdapterSubscriptions

        recyclerViewStaff.layoutManager = LinearLayoutManager(this)
        recyclerViewStaff.isNestedScrollingEnabled = false
        mAdapterStaffList = AdapterStaffList(this)
        recyclerViewStaff.adapter = mAdapterStaffList

        recyclerViewReviews.layoutManager =
            LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
        recyclerViewReviews.isNestedScrollingEnabled = false
        mAdapterReviewsCount = AdapterReviews(this)
        recyclerViewReviews.adapter = mAdapterReviewsCount


        recyclerViewReviewDescription.layoutManager = LinearLayoutManager(this)
        recyclerViewReviewDescription.isNestedScrollingEnabled = false
        mAdapterReviewsDescription = AdapterReviewDescription(this, 0)
        recyclerViewReviewDescription.adapter = mAdapterReviewsDescription



        recyclerWorkingDays.layoutManager = LinearLayoutManager(this)
        recyclerWorkingDays.isNestedScrollingEnabled = false
        mAdapterOperationHours = AdapterWorkingDays(this)
        recyclerWorkingDays.adapter = mAdapterOperationHours

        viewPagerMaps.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        myCustomPagerAdapter = MapsPagerAdapter(this, Constant.getDisplayMetrics(windowManager))
        myCustomPagerAdapter!!.setItemMargin(16)
        myCustomPagerAdapter!!.updateDisplayMetrics()
        indicatorMaps.setRecyclerView(viewPagerMaps)
        indicatorMaps.forceUpdateItemCount()
        PagerSnapHelper().attachToRecyclerView(viewPagerMaps)
        viewPagerMaps.adapter = myCustomPagerAdapter
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.merchant_store, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            onBackPressed()
        }
        if (item.itemId == R.id.merchantlike) {
            val inviteCode = Constant.getArrayListProfile(this, Constant.dataProfile)
            val message =
                "I want to share this awesome experience with you, Sign up  and use redeem code in-app to view your gift. Redeem Code ${inviteCode.invite_code} .https://play.google.com/apps/testing/com.upscribbr"
            val share = Intent(Intent.ACTION_SEND)
            share.type = "text/plain"
            share.putExtra(Intent.EXTRA_TEXT, message)
            startActivity(Intent.createChooser(share, "Share!"))
        }
        return super.onOptionsItemSelected(item)
    }

    fun mStoreSimilar(v: View) {
        val modelBusinessId = intent.getStringExtra("model")
        val intent =
            Intent(this, SimilarBrandsActivity::class.java).putExtra("model", modelBusinessId)
        startActivity(intent)
    }

}
