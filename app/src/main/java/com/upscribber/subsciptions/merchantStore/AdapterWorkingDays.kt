package com.upscribber.subsciptions.merchantStore

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.upscribberSeller.subsriptionSeller.location.DaysLogic.DayModel
import com.upscribber.upscribberSeller.subsriptionSeller.location.DaysLogic.ModelDays
import com.upscribber.upscribberSeller.subsriptionSeller.location.DaysLogic.ModelMain
import com.upscribber.upscribberSeller.subsriptionSeller.location.DaysLogic.ResultModel
import com.upscribber.upscribberSeller.subsriptionSeller.location.searchLoc.ModelSearchLocation
import kotlinx.android.synthetic.main.layout_working_days.view.days


class AdapterWorkingDays(
    private val mContext: Context
) :
    RecyclerView.Adapter<AdapterWorkingDays.MyViewHolder>()  {

    private var list: ArrayList<ModelSearchLocation> = ArrayList()
    var daysArray = ArrayList<DayModel>()
    var mainList = ArrayList<ModelMain>()
    var myModel = ArrayList<ModelDays>()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(mContext).inflate(R.layout.layout_working_days, parent, false)
        return MyViewHolder(v)

    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = list[position]


        myModel = ArrayList()
        daysArray = ArrayList()
        mainList = ArrayList()
        for(i in 0 until model.store_timing.size) {
            var model22 = ModelDays()
            if (i==0) {
                model22.startTime = model.store_timing[i].open
                model22.closeTime = model.store_timing[i].close
                model22.day = "Sunday"
                myModel.add(model22)
            }
            if (i==1) {
                model22.startTime = model.store_timing[i].open
                model22.closeTime = model.store_timing[i].close
                model22.day = "Monday"
                myModel.add(model22)
            }
            if (i==2) {
                model22 = ModelDays()
                model22.startTime = model.store_timing[i].open
                model22.closeTime = model.store_timing[i].close
                model22.day = "Tuesday"
                myModel.add(model22)
            }
            if (i==3) {
                model22 = ModelDays()
                model22.startTime = model.store_timing[i].open
                model22.closeTime = model.store_timing[i].close
                model22.day = "Wednesday"
                myModel.add(model22)
            }
            if (i==4) {
                model22 = ModelDays()
                model22.startTime = model.store_timing[i].open
                model22.closeTime = model.store_timing[i].close
                model22.day = "Thursday"
                myModel.add(model22)
            }
            if (i==5) {
                model22 = ModelDays()
                model22.startTime = model.store_timing[i].open
                model22.closeTime = model.store_timing[i].close
                model22.day = "Friday"
                myModel.add(model22)
            }
            if (i==6) {
                model22 = ModelDays()
                model22.startTime = model.store_timing[i].open
                model22.closeTime = model.store_timing[i].close
                model22.day = "Saturday"
                myModel.add(model22)
            }



        }
        val array2 = arrayListOf<DayModel>()
        for (i in 0 until myModel.size) {


            val model = DayModel()
            when (i) {
                0 -> {
                    model.day = "Sun"
                }
                1 -> {
                    model.day = "Mon"
                }
                2 -> {
                    model.day = "Tue"
                }
                3 -> {
                    model.day = "Wed"
                }
                4 -> {
                    model.day = "Thu"
                }
                5 -> {
                    model.day = "Fri"
                }
                6 -> {
                    model.day = "Sat"
                }
            }

            val startTime: String = myModel[i].startTime
            val endTime: String = myModel[i].closeTime
            if (startTime == endTime) {
                model.time = "closed"
            } else {
                model.time = "$startTime to $endTime"
            }
            array2.add(model)
        }

        for (modelDay in array2) {
            if (!checkIfAlreadyExist(modelDay.day)) {
                val subList = getDayTimeString(modelDay, array2)
                mainList.add(subList)
            }
        }
        val existdays = ArrayList<ResultModel>()
        for (index in mainList.indices) {
            val resultString = mainList[index].resultString
            Log.i("$index : ", resultString.day)
            existdays.add(resultString)

        }
        var daysList=""
        for(i in 0 until existdays.size){
            if (existdays[i].time!="closed") {
                daysList = if (daysList.isEmpty()) {
                    "" + existdays[i].day + " : " + existdays[i].time
                } else {
                    daysList + "\n" + existdays[i].day + " : " + existdays[i].time
                }
            }
        }
        holder.itemView.days.text = daysList

    }

    private fun getDayTimeString(timeToFind: DayModel, list: ArrayList<DayModel>): ModelMain {
        val modelMain = ModelMain()
        val subList = ArrayList<DayModel>()
        var timeToReturn = ""
        var lastIndex = -1
        for (i in list.indices) {
            val model = list[i]
            if (timeToFind.time == model.time) {

                subList.add(model)

                if (lastIndex != -1 && i - lastIndex == 1) {

                    if (timeToReturn.contains("-") && !timeToReturn.contains(",")) {
                        timeToReturn = "${timeToFind.day}-${model.day}"
                    } else {
                        timeToReturn = "$timeToReturn-${model.day}"
                    }

                } else {
                    if (timeToReturn.isEmpty()) {
                        timeToReturn = model.day
                    } else {
                        timeToReturn = "$timeToReturn, ${model.day}"
                    }

                }

                lastIndex = i
            }
        }

        modelMain.list = subList
        modelMain.resultString.day = timeToReturn
        modelMain.resultString.time = timeToFind.time

        return modelMain
    }

    fun checkIfAlreadyExist(day: String): Boolean {
        if (mainList.isNotEmpty()) {
            for (value in mainList) {
                val list = value.list
                for (data in list) {
                    if (data.day == day) {
                        return true
                    }
                }
            }
        }

        return false
    }

    fun update(it : ArrayList<ModelSearchLocation>) {
        list.clear()
        if (it.size >= 2) {
            val default = Constant.getPrefs(mContext).getString(Constant.default_loc, "")
            for (i in 0 until it.size) {
                if (it[i].id == default) {

                    list.add(it[i])
                }
            }
        }else{
            list = it
        }


        notifyDataSetChanged()

    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}