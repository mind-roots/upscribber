package com.upscribber.home

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.location.Location
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.facebook.FacebookSdk.getApplicationContext
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.upscribber.R
import com.upscribber.upscribberSeller.subsriptionSeller.location.searchLoc.ModelSearchLocation
import kotlinx.android.synthetic.main.multiple_maps.view.*


class MapsPagerAdapter(
    var context: Context,
    var metrics: DisplayMetrics

) :
    RecyclerView.Adapter<MapsPagerAdapter.MyViewHolder>() {
    var list: ArrayList<ModelSearchLocation> = ArrayList()
    private var itemMargin: Int = 0
    private var itemWidth: Int = 0
    private var positionAdapter = -1

    internal fun setItemMargin(itemMargin: Int) {
        this.itemMargin = itemMargin
    }

    internal fun updateDisplayMetrics() {
        itemWidth = metrics.widthPixels - itemMargin * 2
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(context).inflate(R.layout.multiple_maps, parent, false)
        return MyViewHolder(v, list)
    }

    class MyViewHolder(itemView: View, var list: ArrayList<ModelSearchLocation>) :
        RecyclerView.ViewHolder(itemView), OnMapReadyCallback {

        var mapView: MapView? = null
        var gMap: GoogleMap? = null
        var fusedLocationClient: FusedLocationProviderClient
        lateinit var lastLocation: Location
        var tViewLocation: TextView

        init {

            mapView = itemView.findViewById(R.id.map)
            tViewLocation = itemView.findViewById(R.id.tViewLocation)
            fusedLocationClient =
                LocationServices.getFusedLocationProviderClient(getApplicationContext())

            if (mapView != null) {
                mapView!!.onCreate(null)
                mapView!!.getMapAsync(this)
                mapView!!.onResume()
            }
        }

        override fun onMapReady(p0: GoogleMap?) {
            try {
                MapsInitializer.initialize(getApplicationContext())
            } catch (e: Exception) {
                e.printStackTrace()
            }
            gMap = p0!!
            setUpMap(adapterPosition)
        }

        fun setUpMap(position: Int) {
            if (ActivityCompat.checkSelfPermission(
                    getApplicationContext(),
                    android.Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(
                    getApplicationContext() as Activity,
                    arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                    1
                )
                return
            }

            if (gMap == null) {
                return
            }
//            fusedLocationClient.lastLocation.addOnSuccessListener { location ->
//                if (location != null) {
//                    lastLocation = location
//                    var currentLatLng = LatLng(location.latitude, location.longitude)

            if (position != -1) {
                if ((list[position].lat != "" && list[position].long != "") && (list[position].lat != "<null>" && list[position].long != "<null>")) {
                    val currentLatLng =
                        LatLng(list[position].lat.toDouble(), list[position].long.toDouble())

                    placeMarkerOnMap(currentLatLng)
                    gMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 12f))

                }
            }
        }


        private fun placeMarkerOnMap(currentLatLng: LatLng) {
            gMap!!.addMarker(
                MarkerOptions()
                    .position(currentLatLng)
                    .icon(
                        bitmapDescriptorFromVector(
                            getApplicationContext(),
                            R.drawable.ic_location_blue_icon
                        )
                    )
            )

        }

        private fun bitmapDescriptorFromVector(
            context: Context,
            vectorResId: Int
        ): BitmapDescriptor? {
            val vectorDrawable = ContextCompat.getDrawable(context, vectorResId)
            vectorDrawable!!.setBounds(
                0,
                0,
                vectorDrawable.intrinsicWidth,
                vectorDrawable.intrinsicHeight
            )
            val bitmap =
                Bitmap.createBitmap(
                    vectorDrawable.intrinsicWidth,
                    vectorDrawable.intrinsicHeight,
                    Bitmap.Config.ARGB_8888
                )
            val canvas = Canvas(bitmap)
            vectorDrawable.draw(canvas)
            return BitmapDescriptorFactory.fromBitmap(bitmap)

        }

    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int, payloads: MutableList<Any>) {
        super.onBindViewHolder(holder, position, payloads)

    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = list[position]
        var currentItemWidth = itemWidth
        if (position == 0) {
            currentItemWidth += itemMargin
            holder.itemView.setPadding(itemMargin, 0, 0, 0)
        } else if (position == itemCount - 1) {
            currentItemWidth += itemMargin
            holder.itemView.setPadding(0, 0, itemMargin, 0)
        }

//        if ((model.lat != "" && model.long != "") && (model.lat != "<null>" && model.long != "<null>")) {
//            val currentLatLng =
//                LatLng(model.lat.toDouble(), model.long.toDouble())
//            holder.gMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 12f))
//        }

        val height = holder.itemView.layoutParams.height
        holder.itemView.layoutParams = ViewGroup.LayoutParams(currentItemWidth, height)
        holder.tViewLocation.text = model.city + "," + model.state_code

        holder.setUpMap(position)

    }

    override fun onViewRecycled(holder: MyViewHolder) {
        super.onViewRecycled(holder)
        if (holder.gMap != null) {
            holder.gMap!!.mapType = GoogleMap.MAP_TYPE_SATELLITE
        }
    }

    fun update(it: java.util.ArrayList<ModelSearchLocation>?) {
        list = it!!
        notifyDataSetChanged()
    }


}