package com.upscribber.subsciptions.merchantStore

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import kotlinx.android.synthetic.main.layout_reviews_description.view.*

class AdapterReviewDescription(
    private val mContext: Context,
    var i: Int
) :
    RecyclerView.Adapter<AdapterReviewDescription.MyViewHolder>() {

    private var list: ArrayList<ModelReviewDescription> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(mContext)
            .inflate(
                R.layout.layout_reviews_description
                , parent, false
            )
        return MyViewHolder(v)
    }


    override fun getItemCount(): Int {
        return if (i == 1){
            list.size
        }else{
            if (list.size >= 4){
                4
            }else{
                return list.size
            }
        }

    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = list[position]
        val imagePath = Constant.getPrefs(mContext).getString(Constant.dataImagePath, "")
        Glide.with(mContext).load(imagePath + model.profile_image).placeholder(R.mipmap.circular_placeholder)
            .into(holder.itemView.imageView)
        holder.itemView.memberNameReview.text = model.name
        holder.itemView.tVDateTime.text = Constant.getDateTime(model.updated_at)
        holder.itemView.reviewText.text = model.review

        if (model.type == "2") {
            holder.itemView.imageViewGesture.setImageResource(R.drawable.ic_like)
        } else {
            holder.itemView.imageViewGesture.setImageResource(R.drawable.ic_dislike)
        }
    }

    fun update(it: java.util.ArrayList<ModelReviewDescription>?) {
        list = it as ArrayList<ModelReviewDescription>
        notifyDataSetChanged()

    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}