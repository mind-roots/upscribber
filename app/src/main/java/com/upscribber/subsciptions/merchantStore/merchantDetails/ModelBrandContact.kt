package com.upscribber.subsciptions.merchantStore.merchantDetails

class ModelBrandContact {

    var website: String = ""
    var call: String = ""
    var lat: String = ""
    var long: String = ""

}
