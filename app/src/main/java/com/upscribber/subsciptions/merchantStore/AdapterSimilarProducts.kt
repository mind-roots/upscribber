package com.upscribber.subsciptions.merchantStore

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.subsciptions.merchantStore.merchantDetails.MerchantStoreActivity
import kotlinx.android.synthetic.main.mechant_similar_subscriptions1.view.*


class AdapterSimilarProducts
    (
    private val mContext: Context,
    var i: Int

) :
    RecyclerView.Adapter<AdapterSimilarProducts.MyViewHolder>() {

    private var list: ArrayList<ModelSimilarSubsciptions> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val v = LayoutInflater.from(mContext)
            .inflate(
                R.layout.mechant_similar_subscriptions1
                , parent, false
            )
        return MyViewHolder(v)
    }

    override fun getItemCount(): Int {
        return if (i == 0){
            list.size
        }else{
            if (list.size > 5){
                5
            }else{
                return list.size
            }
        }
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = list[position]
        val imagePath = Constant.getPrefs(mContext).getString(Constant.dataImagePath, "")
        Glide.with(mContext).load(imagePath + model.cover_image).placeholder(R.mipmap.app_icon)
            .into(holder.itemView.imageSimilar)
        holder.itemView.textView112.text = model.business_name
//        holder.itemView.typesubs.text = model.typeSubscription

        holder.itemView.setOnClickListener {
            mContext.startActivity(Intent(mContext, MerchantStoreActivity::class.java).putExtra("model",model.business_id))

        }
    }

    fun update(it: ArrayList<ModelSimilarSubsciptions>?) {
        list = it as ArrayList<ModelSimilarSubsciptions>
        notifyDataSetChanged()


    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)


}