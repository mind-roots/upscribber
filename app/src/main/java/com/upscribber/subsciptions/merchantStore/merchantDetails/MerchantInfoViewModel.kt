package com.upscribber.subsciptions.merchantStore.merchantDetails

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.upscribber.commonClasses.ModelStatusMsg
import com.upscribber.home.ModelHomeLocation
import com.upscribber.subsciptions.merchantStore.*
import com.upscribber.upscribberSeller.subsriptionSeller.location.searchLoc.ModelSearchLocation

class MerchantInfoViewModel(application: Application) : AndroidViewModel(application) {
    var merchantInfoRepository: MerchantInfoRepository = MerchantInfoRepository(application)

    fun getMerchant(auth: String, model: String) {
        merchantInfoRepository.getDetailsMerchant(auth, model)
    }

    fun getStatus(): LiveData<ModelStatusMsg> {
        return merchantInfoRepository.getmDataFailure()
    }

    fun getmDataBusinessInfo(): LiveData<ModelBusinessInfo> {
        return merchantInfoRepository.getmDataBusinessInfo()
    }

    fun getmDataMain(): LiveData<ModelMerchantMain> {
        return merchantInfoRepository.getmDataMain()
    }

    fun getmDataSubscriptions(): LiveData<ArrayList<ModelBrandSubscription>> {
        return merchantInfoRepository.getmDataSubscriptions()
    }

    fun getmDataSimilarSubscriptions(): LiveData<ArrayList<ModelSimilarSubsciptions>> {
        return merchantInfoRepository.getmDataSimilarSubscriptions()
    }


    fun getmDataStaffList(): LiveData<ArrayList<ModelStaffList>> {
        return merchantInfoRepository.getmDataStaffList()
    }

    fun getmDataLocation(): LiveData<ArrayList<ModelSearchLocation>> {
        return merchantInfoRepository.getmDataLocation()
    }

    fun getmDataReviews(): LiveData<ArrayList<ModelReviews>> {
        return merchantInfoRepository.getmDataReviews()
    }

    fun mDataBrandReviewsDescription(): LiveData<ArrayList<ModelReviewDescription>> {
        return merchantInfoRepository.getmDataReviewsDescription()
    }
    
    fun getmDataTags(): LiveData<ArrayList<ModeTags>> {
        return merchantInfoRepository.getmDataTags()
    }

    fun getmDataContacts(): LiveData<ModelBrandContact> {
        return merchantInfoRepository.getmDataContacts()
    }

}