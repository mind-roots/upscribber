package com.upscribber.subsciptions.merchantStore.merchantDetails

import android.os.Parcel
import android.os.Parcelable

class ModeTags() : Parcelable{

    var parent_name: String = ""
    var updated_at: String = ""
    var created_at: String = ""
    var banner: String = ""
    var unselected_image: String = ""
    var selected_image: String = ""
    var subscription_count: String = ""
    var parent_id: String = ""
    var id: String = ""
    var name:String=""
    var parentId : String = ""

    constructor(parcel: Parcel) : this() {
        parent_name = parcel.readString()
        updated_at = parcel.readString()
        created_at = parcel.readString()
        banner = parcel.readString()
        unselected_image = parcel.readString()
        selected_image = parcel.readString()
        subscription_count = parcel.readString()
        parent_id = parcel.readString()
        id = parcel.readString()
        name = parcel.readString()
        parentId = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(parent_name)
        parcel.writeString(updated_at)
        parcel.writeString(created_at)
        parcel.writeString(banner)
        parcel.writeString(unselected_image)
        parcel.writeString(selected_image)
        parcel.writeString(subscription_count)
        parcel.writeString(parent_id)
        parcel.writeString(id)
        parcel.writeString(name)
        parcel.writeString(parentId)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ModeTags> {
        override fun createFromParcel(parcel: Parcel): ModeTags {
            return ModeTags(parcel)
        }

        override fun newArray(size: Int): Array<ModeTags?> {
            return arrayOfNulls(size)
        }
    }


}
