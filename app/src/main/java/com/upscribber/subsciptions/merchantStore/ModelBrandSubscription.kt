package com.upscribber.subsciptions.merchantStore

import android.os.Parcel
import android.os.Parcelable


class ModelBrandSubscription() : Parcelable{

    var like_percentage: String = ""
    var buy: String = ""
    var frequency_value: String = ""
    var updated_at: String = ""
    var frequency_type: String = ""
    var created_at: String = ""
    var unit: String = ""
    var free_trial: String = ""
    var subscription_id: String = ""
    var introductory_price: String = ""
    var campaign_id: String = ""
    var price: String = ""
    var discount_price: String = ""
    var name: String = ""
    var description: String = ""
    var campaign_image: String = ""
    var bought: String = ""

    constructor(parcel: Parcel) : this() {
        like_percentage = parcel.readString()
        buy = parcel.readString()
        frequency_value = parcel.readString()
        updated_at = parcel.readString()
        frequency_type = parcel.readString()
        created_at = parcel.readString()
        unit = parcel.readString()
        free_trial = parcel.readString()
        subscription_id = parcel.readString()
        introductory_price = parcel.readString()
        campaign_id = parcel.readString()
        price = parcel.readString()
        discount_price = parcel.readString()
        name = parcel.readString()
        description = parcel.readString()
        campaign_image = parcel.readString()
        bought = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(like_percentage)
        parcel.writeString(buy)
        parcel.writeString(frequency_value)
        parcel.writeString(updated_at)
        parcel.writeString(frequency_type)
        parcel.writeString(created_at)
        parcel.writeString(unit)
        parcel.writeString(free_trial)
        parcel.writeString(subscription_id)
        parcel.writeString(introductory_price)
        parcel.writeString(campaign_id)
        parcel.writeString(price)
        parcel.writeString(discount_price)
        parcel.writeString(name)
        parcel.writeString(description)
        parcel.writeString(campaign_image)
        parcel.writeString(bought)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ModelBrandSubscription> {
        override fun createFromParcel(parcel: Parcel): ModelBrandSubscription {
            return ModelBrandSubscription(parcel)
        }

        override fun newArray(size: Int): Array<ModelBrandSubscription?> {
            return arrayOfNulls(size)
        }
    }


}