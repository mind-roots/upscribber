package com.upscribber.subsciptions.merchantStore

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.squareup.picasso.Picasso
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import kotlinx.android.synthetic.main.layout_reviews_list.view.*

class AdapterReviews(
    private val mContext: Context
) :
    RecyclerView.Adapter<AdapterReviews.MyViewHolder>() {

    private var list: ArrayList<ModelReviews> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(mContext)
            .inflate(
                R.layout.layout_reviews_list
                , parent, false
            )
        return MyViewHolder(v)

    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = list[position]

        holder.itemView.noOfReview.text= model.count
        holder.itemView.categoryReview.text= model.name

        if (model.name == "Amazing Staff"){
            Glide.with(mContext).load(R.mipmap.amazing_staff).into(holder.itemView.imageView)
        }else if (model.name == "Excellent Value"){
            Glide.with(mContext).load(R.mipmap.excellent_value).into(holder.itemView.imageView)
        }else if (model.name == "Great Customer Service"){
            Glide.with(mContext).load(R.mipmap.great_customer_service).into(holder.itemView.imageView)
        }else if (model.name == "Profesional & Courteous"){
            Glide.with(mContext).load(R.mipmap.professional).into(holder.itemView.imageView)
        }else if (model.name == "Highly\nRecommended"){
            Glide.with(mContext).load(R.mipmap.highly_recommended).into(holder.itemView.imageView)
        }else if (model.name == "Quick & Easy"){
            Glide.with(mContext).load(R.mipmap.quick_easy).into(holder.itemView.imageView)
        }


    }

    fun update(it: java.util.ArrayList<ModelReviews>?) {
        list = it as ArrayList<ModelReviews>
        notifyDataSetChanged()

    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}