package com.upscribber.subsciptions.merchantStore

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.upscribber.subsciptions.merchantSubscriptionPages.MerchantSubscriptionViewActivity
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import kotlinx.android.synthetic.main.mechant_recycler1.view.*
import java.math.BigDecimal
import java.math.RoundingMode

class AdapterTwo(private val mContext: Context) :
RecyclerView.Adapter<AdapterTwo.MyViewHolder>() {

    private var list: ArrayList<ModelBrandSubscription> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val v = LayoutInflater.from(mContext)
            .inflate(
                R.layout.mechant_recycler1
                , parent, false)
        return MyViewHolder(v)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = list[position]
        val imagePath = Constant.getPrefs(mContext).getString(Constant.dataImagePath, "")

        holder.itemView.textView75.text = model.name
        holder.itemView.manageCredit.text = model.description
        holder.itemView.productOffer.text = model.discount_price.toDouble().toInt().toString() + "% off"
        holder.itemView.tv_person_count.text = model.bought

        val cost = Constant.getCalculatedPrice(model.discount_price, model.price)
        if (cost.contains(".")) {
            val splitPos = cost.split(".")
            if (splitPos[1] == "00" || splitPos[1] == "0") {
                holder.itemView.productPrice.text = "$" +splitPos[0]
            } else {
                holder.itemView.productPrice.text ="$" + BigDecimal(cost).setScale(2, RoundingMode.HALF_UP).toString()
            }
        }else{
            holder.itemView.productPrice.text = "$" +BigDecimal(cost).setScale(2, RoundingMode.HALF_UP).toString()
        }


        val actualCost = model.price.toDouble().toString()
        if (actualCost.contains(".")) {
            val splitActualCost = actualCost.split(".")
            if (splitActualCost[1] == "00" || splitActualCost[1] == "0") {
                holder.itemView.tv_last_text1.text = "$" + splitActualCost[0]
            } else {
                holder.itemView.tv_last_text1.text = "$" + actualCost.toInt()
            }
        }else{
            holder.itemView.tv_last_text1.text = "$" + actualCost.toInt()
        }

        if (model.like_percentage == "0" || model.like_percentage.isEmpty()) {
            holder.itemView.tv_percentage.visibility = View.GONE
        } else {
            holder.itemView.tv_percentage.visibility = View.VISIBLE
            holder.itemView.tv_percentage.text = model.like_percentage + "%"
        }

        holder.tv_last_text1.paintFlags =
            holder.tv_last_text1.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG



        if (model.free_trial == "No free trial" && (model.introductory_price == "0"||model.introductory_price == "0.00")  || model.free_trial == "0" && (model.introductory_price == "0"||model.introductory_price == "0.00")) {
            holder.itemView.tv_free_map.visibility = View.GONE
        } else if (model.free_trial != "No free trial") {
            holder.itemView.tv_free_map.visibility = View.VISIBLE
            holder.itemView.tv_free_map.text = "Free"
            holder.itemView.tv_free_map.setBackgroundResource(R.drawable.intro_price_background)
        } else {
            holder.itemView.tv_free_map.visibility = View.VISIBLE
            holder.itemView.tv_free_map.text = "Intro"
            holder.itemView.tv_free_map.setBackgroundResource(R.drawable.home_free_background)
        }


        Glide.with(mContext).load(imagePath + model.campaign_image)
            .placeholder(R.mipmap.placeholder_subscription_square)
            .into(holder.itemView.imgSubscription)

        holder.itemView.setOnClickListener {
            if (model.buy == "false") {
                mContext.startActivity(
                    Intent(
                        mContext,
                        MerchantSubscriptionViewActivity::class.java
                    ).putExtra("modelSubscriptionView", model.campaign_id)
                        .putExtra("modelSubscription", model)
                )
            } else {
                if (model.buy == "true") {
                    mContext.startActivity(
                        Intent(
                            mContext,
                            MerchantSubscriptionViewActivity::class.java
                        ).putExtra("modelSubscriptionView", model.campaign_id)
                            .putExtra("modelSubscription", model).putExtra("details", "buying")
                    )
                }
            }
        }
    }



    override fun getItemCount(): Int {
        return list.size
    }

    fun update(it: ArrayList<ModelBrandSubscription>?) {
       list = it as ArrayList<ModelBrandSubscription>
        notifyDataSetChanged()
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        var tv_last_text1: TextView = itemView.findViewById(R.id.tv_last_text1)
    }

}