package com.upscribber.subsciptions.merchantStore

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.upscribber.commonClasses.Constant
import com.upscribber.R
import kotlinx.android.synthetic.main.layout_staff_list.view.*

class AdapterStaffList(
    private val mContext: Context

) :
    RecyclerView.Adapter<AdapterStaffList.MyViewHolder>() {

    private var list: ArrayList<ModelStaffList> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val v = LayoutInflater.from(mContext)
            .inflate(
                R.layout.layout_staff_list
                , parent, false
            )
        return MyViewHolder(v)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = list[position]
        val imagePath  = Constant.getPrefs(mContext).getString(Constant.dataImagePath, "http://cloudart.com.au/projects/upscribbr/uploads/")
        Glide.with(mContext).load(imagePath  + model.image).placeholder(R.drawable.ic_male_dummy).into(holder.itemView.imageView)
        holder.itemView.memberName.text= model.name
        if (model.speciality.isEmpty()){
            holder.itemView.speciality.text= "No speciality added"
        }else{
            holder.itemView.speciality.text= model.speciality
        }

        holder.itemView.setOnClickListener {
          Constant.commonStaffAlert(mContext,model.image,model.name,model.speciality,model.facebook,model.instagram,model.twitter)
        }

    }

    fun update(it: java.util.ArrayList<ModelStaffList>?) {
        list = it as ArrayList<ModelStaffList>
        notifyDataSetChanged()
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}