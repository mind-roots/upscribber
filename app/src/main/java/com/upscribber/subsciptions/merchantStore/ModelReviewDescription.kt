package com.upscribber.subsciptions.merchantStore

/**
 * Created by amrit on 19/3/19.
 */
class ModelReviewDescription {

    var review: String = ""
    var type: String = ""
    var name: String = ""
    var profile_image: String = ""
    var updated_at: String = ""
//    var memberImage : Int = 0
//    var gestureImage : Int = 0
//    lateinit var userName : String
//    lateinit var dateTime : String
//    lateinit var review : String
}