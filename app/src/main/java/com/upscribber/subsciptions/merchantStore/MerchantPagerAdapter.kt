package com.upscribber.subsciptions.merchantStore

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.viewpager.widget.PagerAdapter
import com.upscribber.R

class MerchantPagerAdapter(var mData: ArrayList<ModelMerchantStore>) : PagerAdapter()  {

    override fun isViewFromObject(view: View, `object`: Any): Boolean {

        return view === `object`
    }

    override fun getCount(): Int {
        return mData.size
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val view = LayoutInflater.from(container.context)
            .inflate(R.layout.mechant_recycler, container, false)
        container.addView(view)
        bind(mData[position],view)
        return view
    }

    private fun bind(item: ModelMerchantStore, view: View) {
//        val titleTextView = view.findViewById(R.id.tv_logo_text) as TextView
//        val contentTextView = view.findViewById(R.id.iv_logo) as ImageView
        val contentImageView = view.findViewById(R.id.iv_background) as ImageView
//        titleTextView.text = item.name
//        contentTextView.setImageResource(item.icon)
        contentImageView.setImageResource(item.image)
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View?)
    }

}