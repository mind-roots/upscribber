package com.upscribber.subsciptions.merchantStore.merchantDetails

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.upscribber.commonClasses.Constant
import com.upscribber.commonClasses.ModelStatusMsg
import com.upscribber.commonClasses.WebServicesCustomers
import com.upscribber.home.ModelHomeLocation
import com.upscribber.subsciptions.merchantStore.*
import com.upscribber.upscribberSeller.subsriptionSeller.location.days.ModelSelectDays
import com.upscribber.upscribberSeller.subsriptionSeller.location.searchLoc.ModelSearchLocation
import com.upscribber.upscribberSeller.subsriptionSeller.location.searchLoc.ModelStoreLocation
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class MerchantInfoRepository(var application: Application) {

    private val mDataMain = MutableLiveData<ModelMerchantMain>()
    private var mDataSimilarSubsription = MutableLiveData<ArrayList<ModelSimilarSubsciptions>>()
    private val mDataTags = MutableLiveData<ArrayList<ModeTags>>()
    private val mDataBrandReviews = MutableLiveData<ArrayList<ModelReviews>>()
    private val mDataBrandStaff = MutableLiveData<ArrayList<ModelStaffList>>()
    private val mDataContact = MutableLiveData<ModelBrandContact>()
    private val mDataLocation = MutableLiveData<ArrayList<ModelSearchLocation>>()
    private val mDataBusinessInfo = MutableLiveData<ModelBusinessInfo>()
    val mDataFailure = MutableLiveData<ModelStatusMsg>()
    val mDataBrandSubscription = MutableLiveData<ArrayList<ModelBrandSubscription>>()
    val mDataBrandReviewsDescription = MutableLiveData<ArrayList<ModelReviewDescription>>()

    fun getDetailsMerchant(auth: String, model: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.merchantDetails(auth, model)
        call.enqueue(object : Callback<ResponseBody> {



            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")

                        //-----------------Models ----------------------//
                        val modelStatus = ModelStatusMsg()
                        val modelMain = ModelMerchantMain()
                        val modelBusinessInfo = ModelBusinessInfo()
                        val modelContact = ModelBrandContact()
                        val modelSimilarTags = SimilarTags()

                        //---------------------- Arrays-----------------------//
                        val arrayBrandSubscription = ArrayList<ModelBrandSubscription>()
                        val arrayStaff = ArrayList<ModelStaffList>()
                        val arrayReviews = ArrayList<ModelReviews>()
                        val arrayReviewsDescription = ArrayList<ModelReviewDescription>()
                        val arrayTags = ArrayList<ModeTags>()
                        val similarTags = ArrayList<SimilarTags>()
                        val arrayLocation = ArrayList<ModelSearchLocation>()
                        val arrayStoreTimings = ArrayList<ModelStoreLocation>()
                        val arrayListSimilarSubscription = ArrayList<ModelSimilarSubsciptions>()
                        if (status == "true") {
                            val data = json.getJSONObject("data")
                            val subscriptionInfo = data.getJSONObject("business_info")
                            val locations = data.getJSONArray("locations")

                            val similar_brands = data.getJSONArray("similar_brands")
                            val information = data.getJSONObject("information")
                            val subscriptions = data.getJSONArray("subscriptions")
                            val contact = information.getJSONObject("contact")
                            val staff = information.getJSONArray("staff")
                            val reviews = information.getJSONArray("reviews")
                            val latestReviews = information.getJSONArray("latest_reviews")
                            val tags = information.getJSONArray("tags")
                            modelBusinessInfo.id = subscriptionInfo.optString("id")
                            modelBusinessInfo.business_name = subscriptionInfo.optString("business_name")
                            modelBusinessInfo.logo = subscriptionInfo.optString("logo")
                            modelBusinessInfo.about = subscriptionInfo.optString("about")
                            modelBusinessInfo.likes = subscriptionInfo.optString("likes")
                            modelBusinessInfo.views = subscriptionInfo.optString("views")
                            modelBusinessInfo.bought = subscriptionInfo.optString("bought")
                            modelBusinessInfo.default_location = subscriptionInfo.optString("default_location")
                            modelBusinessInfo.twitter = subscriptionInfo.optString("twitter")
                            modelBusinessInfo.facebook = subscriptionInfo.optString("facebook")
                            modelBusinessInfo.instagram = subscriptionInfo.optString("instagram")

                            //------------For Subscription Info ------------------//
                            for (i in 0 until subscriptions.length()) {
                                val modelBrandSubscription = ModelBrandSubscription()
                                val subscriptionData = subscriptions.optJSONObject(i)
                                modelBrandSubscription.subscription_id = subscriptionData.optString("subscription_id")
                                modelBrandSubscription.campaign_id = subscriptionData.optString("campaign_id")
                                modelBrandSubscription.price = subscriptionData.optString("price")
                                modelBrandSubscription.discount_price = subscriptionData.optString("discount_price")
                                modelBrandSubscription.name = subscriptionData.optString("name")
                                modelBrandSubscription.description = subscriptionData.optString("description")
                                modelBrandSubscription.campaign_image = subscriptionData.optString("campaign_image")
                                modelBrandSubscription.bought = subscriptionData.optString("bought")
                                modelBrandSubscription.free_trial = subscriptionData.optString("free_trial")
                                modelBrandSubscription.introductory_price = subscriptionData.optString("introductory_price")
                                modelBrandSubscription.unit = subscriptionData.optString("unit")
                                modelBrandSubscription.created_at = subscriptionData.optString("created_at")
                                modelBrandSubscription.frequency_type = subscriptionData.optString("frequency_type")
                                modelBrandSubscription.updated_at = subscriptionData.optString("updated_at")
                                modelBrandSubscription.frequency_value = subscriptionData.optString("frequency_value")
                                modelBrandSubscription.like_percentage = subscriptionData.optString("like_percentage")
                                modelBrandSubscription.buy = subscriptionData.optString("buy")
                                arrayBrandSubscription.add(modelBrandSubscription)
                            }
                            //------------For Contact Details ------------------//

                            modelContact.website = contact.optString("website")
                            modelContact.call = contact.optString("call")
                            modelContact.lat = contact.optString("lat")
                            modelContact.long = contact.optString("long")

                            //------------For Staff List ------------------//
                            for (i in 0 until staff.length()) {
                                val modelStaff = ModelStaffList()
                                val staffData = staff.optJSONObject(i)
                                modelStaff.name = staffData.optString("name")
                                modelStaff.image = staffData.optString("image")
                                modelStaff.speciality = staffData.optString("speciality")
                                modelStaff.facebook = staffData.optString("facebook")
                                modelStaff.twitter = staffData.optString("twitter")
                                modelStaff.instagram = staffData.optString("instagram")
                                arrayStaff.add(modelStaff)
                            }

                            //------------For Reviews------------------//
                            for (i in 0 until reviews.length()) {
                                val reviewsmodel = ModelReviews()
                                val reviewsData = reviews.optJSONObject(i)
                                reviewsmodel.name = reviewsData.optString("name")
                                reviewsmodel.count = reviewsData.optString("count")
                                reviewsmodel.id = reviewsData.optString("id")
                                arrayReviews.add(reviewsmodel)

                            }

                            //------------For Reviews Description------------------//
                            for (i in 0 until latestReviews.length()) {
                                val modelLatestreviewsData = ModelReviewDescription()
                                val latestreviewsData = latestReviews.optJSONObject(i)
                                modelLatestreviewsData.review = latestreviewsData.optString("review")
                                modelLatestreviewsData.type = latestreviewsData.optString("type")
                                modelLatestreviewsData.name = latestreviewsData.optString("name")
                                modelLatestreviewsData.profile_image = latestreviewsData.optString("profile_image")
                                modelLatestreviewsData.updated_at = latestreviewsData.optString("updated_at")
                                arrayReviewsDescription.add(modelLatestreviewsData)
                            }

                            //------------For Tags------------------//
                            for (i in 0 until tags.length()) {
                                val modelTags = ModeTags()
                                val tagss = tags.optJSONObject(i)
                                modelTags.name= tagss.optString("name")
                                modelTags.id = tagss.optString("id")
                                arrayTags.add(modelTags)
                            }

                            //------------For Similar Brands------------------//
                            for (i in 0 until similar_brands.length()) {
                                val modelSimilarSubscrion = ModelSimilarSubsciptions()
                                val similarBrandsData = similar_brands.optJSONObject(i)
                                modelSimilarSubscrion.cover_image = similarBrandsData.optString("cover_image")
                                modelSimilarSubscrion.business_name = similarBrandsData.optString("business_name")
                                modelSimilarSubscrion.business_id = similarBrandsData.optString("business_id")
                                val tagss = similarBrandsData.getJSONArray("tags")
                                for (j in 0 until tagss.length()) {
                                    similarTags.add(modelSimilarTags)
                                }
                                arrayListSimilarSubscription.add(modelSimilarSubscrion)
                            }

                             for (loc in 0 until locations.length()) {
                                val modeLocation = ModelSearchLocation()
                                val locionData = locations.optJSONObject(loc)
                                 modeLocation.id = locionData.optString("id")
                                 modeLocation.business_id = locionData.optString("business_id")
                                 modeLocation.user_id = locionData.optString("user_id")
                                 modeLocation.street = locionData.optString("street")
                                 modeLocation.city = locionData.optString("city")
                                 modeLocation.state = locionData.optString("state")
                                 modeLocation.zip_code = locionData.optString("zip_code")
                                 modeLocation.created_at = locionData.optString("created_at")
                                 modeLocation.updated_at = locionData.optString("updated_at")
                                 modeLocation.lat = locionData.optString("lat")
                                 modeLocation.long = locionData.optString("long")
                                 modeLocation.state_code = locionData.optString("state_code")
                                 val store_timing = locionData.getJSONArray("store_timing")
                                 for (store in 0 until store_timing.length()){
                                     val modelStore = ModelStoreLocation()
                                     val storeData = store_timing.optJSONObject(store)
                                     modelStore.close = storeData.optString("close")
                                     modelStore.open = storeData.optString("open")
                                     modelStore.day = storeData.optString("day")
                                     modelStore.status = storeData.optString("status")
                                     modelStore.checked = storeData.optString("checked")
                                     modeLocation.store_timing.add(modelStore)
                                 }

                                 arrayLocation.add(modeLocation)
                            }

                            modelMain.status = status
                            modelMain.msg = msg
                            modelStatus.status = status
                            modelStatus.msg = msg

                            val editor = Constant.getPrefs(application).edit()
                            editor.putString(Constant.default_loc, modelBusinessInfo.default_location)
                            editor.apply()

                            mDataSimilarSubsription.value = arrayListSimilarSubscription
                            mDataTags.value = arrayTags
                            mDataMain.value = modelMain
                            mDataBusinessInfo.value = modelBusinessInfo
                            mDataBrandSubscription.value = arrayBrandSubscription
                            mDataBrandReviewsDescription.value = arrayReviewsDescription
                            mDataBrandReviews.value = arrayReviews
                            mDataBrandStaff.value = arrayStaff
                            mDataContact.value = modelContact
                            mDataLocation.value = arrayLocation
                        } else {
                            modelStatus.msg = msg
                        }

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus

            }
        })
    }

    fun getmDataFailure(): LiveData<ModelStatusMsg> {
        return mDataFailure
    }

    fun getmDataBusinessInfo(): LiveData<ModelBusinessInfo> {
        return mDataBusinessInfo
    }

    fun getmDataMain(): LiveData<ModelMerchantMain> {
        return mDataMain

    }

    fun getmDataSubscriptions(): LiveData<ArrayList<ModelBrandSubscription>> {
        return mDataBrandSubscription
    }

    fun getmDataSimilarSubscriptions(): LiveData<ArrayList<ModelSimilarSubsciptions>> {
        return mDataSimilarSubsription
    }

    fun getmDataContacts(): LiveData<ModelBrandContact> {
        return mDataContact
    }

    fun getmDataStaffList(): LiveData<ArrayList<ModelStaffList>> {
        return mDataBrandStaff
    }

    fun getmDataLocation(): LiveData<ArrayList<ModelSearchLocation>> {
        return mDataLocation
    }

    fun getmDataReviews(): LiveData<ArrayList<ModelReviews>> {
        return mDataBrandReviews
    }
    fun getmDataReviewsDescription(): LiveData<ArrayList<ModelReviewDescription>> {
        return mDataBrandReviewsDescription
    }

    fun getmDataTags() : LiveData<ArrayList<ModeTags>>{
        return mDataTags
    }

}