package com.upscribber.subsciptions.merchantSubscriptionPages

import android.annotation.SuppressLint
import android.graphics.Outline
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.Glide
import com.upscribber.commonClasses.Constant
import com.upscribber.R
import com.upscribber.databinding.ActivityBoomBinding
import com.upscribber.subsciptions.merchantSubscriptionPages.plan.PlanModel
import kotlinx.android.synthetic.main.sharing_congratulation_alert.*
import android.content.Intent
import android.graphics.Paint
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import java.math.BigDecimal
import java.math.RoundingMode


@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class BoomActivity : AppCompatActivity() {

    lateinit var mBinding: ActivityBoomBinding
    lateinit var mViewModel: MerchantSubscriptionViewViewModel
    var campaignId = ""
    var buy = "0"

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_boom)
        mViewModel = ViewModelProviders.of(this)[MerchantSubscriptionViewViewModel::class.java]

        if (intent.hasExtra("modelSubscriptionView") && intent.hasExtra("plan") && intent.hasExtra("campaignId")) {
            val model = intent.getParcelableExtra<ModelCampaignInfo>("modelSubscriptionView")
            val planModel = intent.getParcelableExtra<PlanModel>("plan")
            val imagePath = Constant.getPrefs(this).getString(Constant.dataImagePath, "")
            campaignId = intent.getStringExtra("campaignId")

            Glide.with(this).load(imagePath + model.feature_image)
                .placeholder(R.mipmap.placeholder_subscription).into(mBinding.roundedImageView3)
            Glide.with(this).load(imagePath + model.logo).placeholder(R.mipmap.circular_placeholder)
                .into(mBinding.logo)


            mBinding.textView303.text = model.name
            mBinding.tvBusinessName.text = model.company_name
            mBinding.textView107.text = model.likes + "%"
            mBinding.textView105.text = model.bought
            mBinding.textView111.text = model.views

            val cost = Constant.getCalculatedPrice(planModel.discount_price, planModel.price)
            val splitPos = cost.split(".")
            if (splitPos[1] == "00" || splitPos[1] == "0") {
                mBinding.tvCost.text = "$" + splitPos[0]
            } else {
                mBinding.tvCost.text = "$" + BigDecimal(cost).setScale(2, RoundingMode.HALF_UP)
            }
            val actualCost = planModel.price.toDouble().toString()
            val splitActualCost = actualCost.split(".")
            if (splitActualCost[1] == "00" || splitActualCost[1] == "0") {
                mBinding.tvLastText.text = "$" + splitActualCost[0]
            } else {
                mBinding.tvLastText.text = "$" + actualCost.toInt()
            }

            mBinding.txtOff.text = planModel.discount_price.toDouble().toInt().toString() + "% off"
            mBinding.tvLastText.paintFlags =
                mBinding.tvLastText.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
            if (planModel.free_trial == "No free trial" && planModel.introductory_price == "0") {
                mBinding.freeTrails.visibility = View.GONE
            } else if (planModel.free_trial != "No free trial") {
                mBinding.freeTrails.visibility = View.VISIBLE
                mBinding.freeTrails.text = "Free"
                mBinding.freeTrails.setBackgroundResource(R.drawable.intro_price_background)
            } else {
                mBinding.freeTrails.visibility = View.VISIBLE
                mBinding.freeTrails.text = "Intro"
                mBinding.freeTrails.setBackgroundResource(R.drawable.home_free_background)
            }


        }

        if (intent.hasExtra("MySubscriptionsAdapter")) {
            buy = "1"
        }
        roundImage()
        setToolbar()
        clickListeners()
        observerInit()
    }

    private fun observerInit() {
        mViewModel.getmDataBoom().observe(this, Observer {
            startActivity(Intent(this, SuccessfullShared::class.java).putExtra("buyValue",buy))
            finish()
        })


        mViewModel.getStatus().observe(this, Observer {
            if (it.msg == "Invalid auth code") {
                Constant.commonAlert(this)
            } else {
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
            }
        })

    }


    private fun clickListeners() {
        mBinding.button.setOnClickListener {
            val inviteCode = Constant.getArrayListProfile(this, Constant.dataProfile)
            val message =
                "I want to share this awesome experience with you, Sign up  and use redeem code in-app to view your gift. Redeem Code ${inviteCode.invite_code} .https://play.google.com/apps/testing/com.upscribbr"
            val share = Intent(Intent.ACTION_SEND)
            share.type = "text/plain"
            share.putExtra(Intent.EXTRA_TEXT, message)
            startActivityForResult(Intent.createChooser(share, "Share!"), 1)

//             val mDialogView = LayoutInflater.from(this).inflate(R.layout.sharing_congratulation_alert, null)
//             val mBuilder = AlertDialog.Builder(this)
//                 .setView(mDialogView)
//             val mAlertDialog = mBuilder.show()
//             mAlertDialog.setCancelable(false)
//             mAlertDialog.window.setBackgroundDrawableResource(R.drawable.bg_alert_transparent)
//             mAlertDialog.setCanceledOnTouchOutside(false)
//             mAlertDialog.textDismiss.setOnClickListener {
//                 val sharedPreferences = Constant.getPrefs(this)
//                 val editor = sharedPreferences.edit()
//                 editor.putString("spread", "back")
//                 editor.apply()
//                 mAlertDialog.dismiss()
//             }
        }

    }

    override fun onResume() {
        super.onResume()
        val sharedPreferences = Constant.getPrefs(this)
        if (!sharedPreferences.getString("spread", "").isEmpty()) {
            val editor = sharedPreferences.edit()
            editor.remove("spread")
            editor.apply()
            finish()
        }

    }

    fun shareKit(v: View) {
        val message =
            "Come abroad Upscribbr and experience this amazing subscription at a very attractive price.\nhttps://www.Upscribbr.com/sharablelink.php"
        val share = Intent(Intent.ACTION_SEND)
        share.type = "text/plain"
        share.putExtra(Intent.EXTRA_TEXT, message)
        startActivityForResult(Intent.createChooser(share, "Share!"), 1)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1) {
            val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
            mViewModel.setBoomSubscription(auth, campaignId, buy)

//            intent.putExtra("boomActivity", "boom")
//            finish()
        }
    }


    private fun setToolbar() {
        setSupportActionBar(mBinding.toolbar)
        mBinding.toolbar.title = ""
        mBinding.title.text = "Spread The Love"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white)

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun roundImage() {
        val curveRadius = 70F

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            mBinding.imageView45.outlineProvider = object : ViewOutlineProvider() {

                @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                override fun getOutline(view: View?, outline: Outline?) {
                    outline?.setRoundRect(
                        0,
                        -100,
                        view!!.width,
                        view.height,
                        curveRadius
                    )
                }
            }

            mBinding.imageView45.clipToOutline = true

        }

    }
}
