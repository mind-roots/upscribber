package com.upscribber.subsciptions.merchantSubscriptionPages.plan

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.R
import kotlinx.android.synthetic.main.choose_plan_two.view.*
import me.grantland.widget.AutofitHelper


class AdapterPlanTwo(val mContext: Context, val list: ArrayList<ModelChoosePlanTwo>) :
    RecyclerView.Adapter<AdapterPlanTwo.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val v = LayoutInflater.from(mContext)
            .inflate(
                R.layout.choose_plan_two
                , parent, false
            )
        return MyViewHolder(v)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = list[position]
        AutofitHelper.create(holder.itemView.tv_45)

        if (model.visitNumber == "N/A") {
            holder.itemView.tvAnnual.visibility = View.GONE
            holder.itemView.tvUpto.visibility = View.GONE
            holder.itemView.tv_45.visibility = View.GONE
            holder.itemView.tv_visit.visibility = View.GONE
        }

        holder.itemView.tvAnnual.text = model.trial
        holder.itemView.tvUpto.text = model.total


        holder.itemView.tv_45.text = model.visitNumber
        holder.itemView.tv_visit.text = model.visitLeft
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

}
