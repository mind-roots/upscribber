package com.upscribber.subsciptions.merchantSubscriptionPages

import android.os.Parcel
import android.os.Parcelable

class ModelImagesSubscriptionDetails() : Parcelable{

    var image: String = ""

    constructor(parcel: Parcel) : this() {
        image = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(image)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ModelImagesSubscriptionDetails> {
        override fun createFromParcel(parcel: Parcel): ModelImagesSubscriptionDetails {
            return ModelImagesSubscriptionDetails(parcel)
        }

        override fun newArray(size: Int): Array<ModelImagesSubscriptionDetails?> {
            return arrayOfNulls(size)
        }
    }
}
