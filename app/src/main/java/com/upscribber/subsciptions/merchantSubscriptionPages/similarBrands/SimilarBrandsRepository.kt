package com.upscribber.subsciptions.merchantSubscriptionPages.similarBrands

import android.app.Application
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.common.reflect.TypeToken
import com.google.gson.Gson
import com.upscribber.commonClasses.Constant
import com.upscribber.commonClasses.ModelStatusMsg
import com.upscribber.commonClasses.WebServicesCustomers
import com.upscribber.discover.DiscoverSecondModel
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class SimilarBrandsRepository(var application: Application) {

    private val mDataFavourite = MutableLiveData<DiscoverSecondModel>()
    private val mDataAllSubscription = MutableLiveData<ArrayList<DiscoverSecondModel>>()
    private val mDataAllBrands = MutableLiveData<ArrayList<SimilarBrandsModel>>()
    val mDataFailure = MutableLiveData<ModelStatusMsg>()

    //--------------------- get Similar Subscription Api-------------------------//
    fun getListSimilarSubscription(
        auth: String,
        modelBusinessId: String,
        page: String,
        type: String
    ) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> =
            service.viewAllSubscriprtion(auth, modelBusinessId, page, type)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val arrayAllSubscription = ArrayList<DiscoverSecondModel>()
                        val modelStatus = ModelStatusMsg()
                        if (status == "true") {
                            val data = json.getJSONObject("data")
                            val similar_subscriptions = data.getJSONArray("similar_subscriptions")
                            val editorFeatues = Constant.getPrefs(application).edit()
                            editorFeatues.putString(
                                Constant.dataSimilarSubscription,
                                similar_subscriptions.toString()
                            )
                            editorFeatues.apply()

                            modelStatus.status = status
                            modelStatus.msg = msg
                        } else {
                            modelStatus.status = status
                            modelStatus.msg = msg
                        }

                        mDataAllSubscription.value = arrayAllSubscription

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus

            }
        })
    }

    fun getmDataAllSubscription(): LiveData<ArrayList<DiscoverSecondModel>> {
        return mDataAllSubscription
    }

    fun getmDataFailure(): LiveData<ModelStatusMsg> {
        return mDataFailure
    }

    fun getmDataAllBrands(): LiveData<ArrayList<SimilarBrandsModel>> {
        return mDataAllBrands
    }


    fun getmDataFav(): LiveData<DiscoverSecondModel> {
        return mDataFavourite
    }

    //--------------------- get Similar Brands Api-------------------------//
    fun getListSimilarBrands(auth: String, modelBusinessId: String, page: String, type: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.viewAllBrands(auth, modelBusinessId, page, type)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val arrayAllBrands = ArrayList<SimilarBrandsModel>()
                        val modelStatus = ModelStatusMsg()
                        if (status == "true") {
                            val data = json.getJSONObject("data")
                            val similar_brands = data.getJSONArray("similar_brands")
                            for (i in 0 until similar_brands.length()) {
                                val similarBrandsModel = SimilarBrandsModel()
                                val similarBrandsData = similar_brands.optJSONObject(i)
                                similarBrandsModel.cover_image =
                                    similarBrandsData.optString("cover_image")
                                similarBrandsModel.business_name =
                                    similarBrandsData.optString("business_name")
                                similarBrandsModel.business_id =
                                    similarBrandsData.optString("business_id")
                                similarBrandsModel.likes = similarBrandsData.optString("likes")
                                similarBrandsModel.views = similarBrandsData.optString("views")
                                similarBrandsModel.bought = similarBrandsData.optString("bought")
                                similarBrandsModel.total_subscriptions =
                                    similarBrandsData.optString("total_subscriptions")
                                similarBrandsModel.status = status
                                similarBrandsModel.msg = msg

                                arrayAllBrands.add(similarBrandsModel)
                            }

                            modelStatus.status = status
                            modelStatus.msg = msg
                        } else {
                            modelStatus.status = status
                            modelStatus.msg = msg
                        }

                        mDataAllBrands.value = arrayAllBrands

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus

            }
        })

    }

    //----------------- set Favourite Api-----------------------//

    fun getFavourite(auth: String, discoverSecondModel: DiscoverSecondModel) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()
        var likeOrUnlike = "1"
        if (discoverSecondModel.like.equals("1")) {
            likeOrUnlike = "2"
        } else {
            likeOrUnlike = "1"
        }
        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> =
            service.setFavorite(auth, discoverSecondModel.campaign_id, likeOrUnlike)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        if (status == "true") {
                            discoverSecondModel.status = status
                            discoverSecondModel.msg = msg
                            var arrayList: ArrayList<String> = ArrayList()
                            val mPrefs = application.getSharedPreferences(
                                "data",
                                AppCompatActivity.MODE_PRIVATE
                            )
                            val gson = Gson()
                            val getFav = mPrefs.getString("favData", "")
                            if (getFav != "") {
                                arrayList = gson.fromJson<ArrayList<String>>(
                                    getFav,
                                    object : TypeToken<ArrayList<String>>() {

                                    }.type
                                )
                                var exist = 0
                                for (item in arrayList) {
                                    if (discoverSecondModel.campaign_id == item) {
                                        exist = 1
                                        break
                                    }
                                }
                                if (exist == 0) {
                                    arrayList.add(discoverSecondModel.campaign_id)
                                }else{
                                    if(msg.contains("Favorite removed successfully.")){
                                        arrayList.remove(discoverSecondModel.campaign_id)
                                    }
                                }
                            } else {
                                arrayList.add(discoverSecondModel.campaign_id)
                            }

                            val prefsEditor = mPrefs.edit()
                            val json = gson.toJson(arrayList)
                            prefsEditor.putString("favData", json)
                            prefsEditor.apply()
                            Log.e("favData", json)
                            if (discoverSecondModel.like == 1) {
                                discoverSecondModel.like = 2
                            } else {
                                discoverSecondModel.like = 1
                            }


                        } else {
                            modelStatus.msg = msg
                        }
                        mDataFavourite.value = discoverSecondModel
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus

            }

        })

    }


}