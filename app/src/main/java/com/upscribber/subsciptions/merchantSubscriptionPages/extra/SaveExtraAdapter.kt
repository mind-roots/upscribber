package com.upscribber.subsciptions.merchantSubscriptionPages.extra

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.R
import kotlinx.android.synthetic.main.save_extra_value.view.*


class SaveExtraAdapter (
    val mContext: Context,
    val list: ArrayList<SaveExtraModel>
) : RecyclerView.Adapter<SaveExtraAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(mContext)
            .inflate(
                R.layout.save_extra_value
                , parent, false
            )
        return MyViewHolder(v)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = list[position]

        holder.itemView.tvNails.text = model.textViewExtra
        holder.itemView.tvCost.text = model.costExtra
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }

}
