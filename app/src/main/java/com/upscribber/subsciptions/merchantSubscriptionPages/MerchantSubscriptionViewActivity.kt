package com.upscribber.subsciptions.merchantSubscriptionPages

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.Paint
import android.net.Uri
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.view.*
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.widget.NestedScrollView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.upscribber.categories.subCategories.SubCategoriesActivity
import com.upscribber.checkout.checkoutOne
import com.upscribber.discover.DiscoverActivity
import com.upscribber.subsciptions.extrasSubsription.ExtrasSubscriptionActivity
import com.upscribber.subsciptions.extrasSubsription.ModelProduct
import com.upscribber.subsciptions.manageSubscriptions.ManageSubscriptionsActivity
import com.upscribber.commonClasses.RoundedBottomSheetDialogFragment
import com.upscribber.subsciptions.merchantStore.merchantDetails.MerchantStoreActivity
import com.upscribber.subsciptions.merchantSubscriptionPages.deal.DealAdapter
import com.upscribber.subsciptions.merchantSubscriptionPages.deal.DealModel
import com.upscribber.subsciptions.merchantSubscriptionPages.extra.SaveExtraAdapter
import com.upscribber.subsciptions.merchantSubscriptionPages.extra.SaveExtraModel
import com.upscribber.subsciptions.merchantSubscriptionPages.plan.AdapterPlanTwo
import com.upscribber.subsciptions.merchantSubscriptionPages.plan.ModelChoosePlanTwo
import com.upscribber.subsciptions.merchantSubscriptionPages.plan.PlanAdapter
import com.upscribber.subsciptions.merchantSubscriptionPages.similarSubscriptions.SimilarProductsAdapter
import com.upscribber.subsciptions.merchantSubscriptionPages.staff.StaffFaqAdapter
import com.upscribber.subsciptions.merchantSubscriptionPages.staff.StaffFaqModel
import com.upscribber.subsciptions.reviews.ReviewActivity
import com.upscribber.upscribberSeller.subsriptionSeller.subscriptionView.CustomerPagerAdapterNew
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.kingfisher.easyviewindicator.RecyclerViewIndicator
import com.upscribber.commonClasses.Constant
import com.upscribber.R
import com.upscribber.home.MapsPagerAdapter
import com.upscribber.subsciptions.merchantStore.AdapterReviewDescription
import com.upscribber.subsciptions.merchantStore.AdapterReviews
import com.upscribber.subsciptions.merchantStore.AdapterStaffList
import com.upscribber.subsciptions.merchantStore.AdapterWorkingDays
import com.upscribber.subsciptions.merchantStore.merchantDetails.ModeTags
import com.upscribber.subsciptions.merchantSubscriptionPages.plan.PlanModel
import com.upscribber.subsciptions.mySubscriptionDetails.MySubscriptionsModel
import fr.castorflex.android.verticalviewpager.VerticalViewPager
import kotlinx.android.synthetic.main.activity_checkout.*
import kotlinx.android.synthetic.main.activity_merchant_subscription_view.*
import kotlinx.android.synthetic.main.merchant_second_background.*
import kotlinx.android.synthetic.main.plan_information_layout.*
import kotlinx.android.synthetic.main.plan_staff_layout.*
import me.relex.circleindicator.CircleIndicator
import java.math.BigDecimal
import java.math.RoundingMode
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


@Suppress(
    "RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS",
    "NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS"
)
class MerchantSubscriptionViewActivity : AppCompatActivity(),
    PlanAdapter.ShowBuyButton {

    lateinit var mAdapterOperationHours: AdapterWorkingDays
    private var campaignId: String = ""

    //    private var modelHomeDataSubscription = ModelHomeDataSubscriptions()
    private lateinit var planModel: PlanModel
    private lateinit var indicator_login: CircleIndicator
    private lateinit var indicatorMaps: RecyclerViewIndicator
    private lateinit var rvPlan: RecyclerView
    private lateinit var rvPlanTwo: RecyclerView
    private lateinit var rv_one_include: RecyclerView
    private lateinit var rv_Extraa: RecyclerView
    private lateinit var recyclerViewReviews: RecyclerView
    private lateinit var recyclerViewReviewDescription: RecyclerView
    private lateinit var recyclerWorkingDays: RecyclerView
    private lateinit var mrecyclerSimilarList: RecyclerView
    private lateinit var recyclerView2: RecyclerView
    private lateinit var viewPagerMaps: RecyclerView
    private lateinit var staffDescriptionRv: RecyclerView
    private lateinit var tvInformation: TextView
    private lateinit var textView95: TextView
    private lateinit var line: TextView
    private lateinit var downImage: ImageView
    private lateinit var linearFavorite: LinearLayout
    private lateinit var shareLike: LinearLayout
    private lateinit var tvStaff: TextView
    private lateinit var cardView7: CardView
    private lateinit var reviewViewAll: TextView
    private lateinit var manage: TextView
    private lateinit var tv_earn: TextView
    private lateinit var cl_extra: ConstraintLayout
    private lateinit var cl_button: ConstraintLayout
    private lateinit var bottom_sheet: LinearLayout
    private lateinit var relativeBoom: CardView
    private lateinit var relativeTool: RelativeLayout
    private lateinit var showHideBottomSheet: RelativeLayout
    private lateinit var btn_buy: TextView
    private lateinit var btn_manage: TextView
    private lateinit var textView122: TextView
    private lateinit var daysPlan: TextView
    private lateinit var viewAllSimilar: TextView
    private lateinit var nestedScrollView2: NestedScrollView
    private lateinit var progressBarSubscription: LinearLayout
    private lateinit var linearBack: LinearLayout
    private lateinit var shareFav: ImageView
    lateinit var mChip: Chip
    lateinit var rvTag: ChipGroup
    var arrayTags: ArrayList<ModeTags> = ArrayList()
    var status = true
    var shofull = 0
    var subscribeBtn = 0
    var click = 0
    var myCustomPagerAdapter: CustomerPagerAdapterNew? = null
    var mAdapterPager: MapsPagerAdapter? = null
    var viewPager: VerticalViewPager? = null
    lateinit var latitude: String
    lateinit var longitude: String
    lateinit var mViewModel: MerchantSubscriptionViewViewModel
    lateinit var mAdapterPlan: PlanAdapter
    lateinit var mAdapterPlanTwo: AdapterPlanTwo
    lateinit var mAdapterReviewsCount: AdapterReviews
    lateinit var mAdapterReviewsDescription: AdapterReviewDescription
    lateinit var mAdapterStaffList: AdapterStaffList
    lateinit var mAdapterSimilarSubscription: SimilarProductsAdapter
    var modelSubscription: ModelCampaignInfo = ModelCampaignInfo()
    var mySubscriptionModel: MySubscriptionsModel = MySubscriptionsModel()
    var planListData: ArrayList<PlanModel> = ArrayList()
    lateinit var progressBar25: ConstraintLayout

    companion object {
        var checkIntent = ""
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_merchant_subscription_view)
        mViewModel = ViewModelProviders.of(this)[MerchantSubscriptionViewViewModel::class.java]
        init()
        checkIntentFrom()
        setSpannnableText()
        myCustomPagerAdapter = CustomerPagerAdapterNew(this)
        viewPager!!.adapter = myCustomPagerAdapter
        clickEvents()
        bottomSheetInit()
        adapterSetLocation()
        observersInit()
        mAdapterPlan = PlanAdapter(this)
        rvPlan.layoutManager = LinearLayoutManager(this)
        rvPlan.isNestedScrollingEnabled = false
        rvPlan.adapter = mAdapterPlan


//        Glide.with(this).load(R.drawable.tap).into(gifImg)
    }

    private fun adapterSetLocation() {
        viewPagerMaps.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        mAdapterPager = MapsPagerAdapter(this, Constant.getDisplayMetrics(windowManager))
        mAdapterPager!!.setItemMargin(16)
        mAdapterPager!!.updateDisplayMetrics()
        indicatorMaps.setRecyclerView(viewPagerMaps)
        indicatorMaps.forceUpdateItemCount()
        PagerSnapHelper().attachToRecyclerView(viewPagerMaps)
        viewPagerMaps.adapter = mAdapterPager
    }

    private fun checkIntentFrom() {
        checkIntent = if (intent.hasExtra("MySubscriptionsAdapter")) {
            if (intent.hasExtra("cancelled")) {
                "cancelled"
            } else {
                "MySubscriptionsAdapter"
            }
        } else {
            "home"
        }
    }


    private fun setSpannnableText() {
        val spannableString =
            SpannableString("Get 20% off. Invite a Friend Now")
        spannableString.setSpan(
            ForegroundColorSpan(Color.BLACK),
            13, 32,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        tv_earn.text = spannableString

    }


    @SuppressLint("SetTextI18n", "InflateParams")
    private fun observersInit() {
        mViewModel.getStatus().observe(this, Observer {
            if (it.msg == "Invalid auth code") {
                Constant.commonAlert(this)
            } else {
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
            }
        })

        mViewModel.getmDataMain().observe(this, Observer {
            if (it.status == "true") {
                progressBarSubscription.visibility = View.GONE
//                linearViewGif.visibility = View.VISIBLE
            }
        })

        mViewModel.getmDataCampaignInfo().observe(this, Observer {
            progressBarSubscription.visibility = View.GONE
            val imagePath = Constant.getPrefs(this).getString(Constant.dataImagePath, "")
            modelSubscription = it
            if (it.logo.isEmpty()) {
                Glide.with(this).load(R.mipmap.app_icon).into(iv_logo)
            } else {
                Glide.with(this).load(imagePath + it.logo).into(iv_logo)
            }
            if (it.favourite == 1) {
                shareFav.setImageResource(R.drawable.ic_favorite_fill)
            } else {
                shareFav.setImageResource(R.drawable.ic_favorite_border_white)
            }

            if (it.business_about == "") {
                tvBusinessInfo.visibility = View.GONE
                textBusinessInfo.visibility = View.GONE
                viewwww.visibility = View.GONE
            } else {
                tvBusinessInfo.visibility = View.VISIBLE
                textBusinessInfo.visibility = View.VISIBLE
                viewwww.visibility = View.VISIBLE
                textBusinessInfo.text = it.business_about
            }

            if (it.is_discounted == "0") {
                relativeBoom.visibility = View.GONE
            } else {
                relativeBoom.visibility = View.VISIBLE
            }

            if (intent.hasExtra("MySubscriptionsAdapter")) {
                if (checkIntent == "cancelled") {
                    if (modelSubscription.boom_discount != "0") {
                        relativeBoom.isEnabled = false
                        tv_earn.setCompoundDrawablesWithIntrinsicBounds(
                            R.drawable.ic_ellipse,
                            0,
                            0,
                            0
                        )
                        val params = LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.WRAP_CONTENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT
                        )
                        params.setMargins(30, 10, 10, 10)
                        params.height = 60
                        tv_earn.textSize = 12f
                        params.gravity = Gravity.CENTER
                        tv_earn.text = "Congrats, you saved 20% off next month"
                        tv_earn.layoutParams = params
                        tv_earn.setTextColor(resources.getColor(R.color.colorWhite))
                        imageGift.visibility = View.GONE
                        relativeBoom.setCardBackgroundColor(resources.getColor(R.color.home_free))
                    } else {
                        imageGift.visibility = View.VISIBLE
                        relativeBoom.setCardBackgroundColor(resources.getColor(R.color.colorWhite))
                        relativeBoom.isEnabled = true
                    }
                } else {
                    if (modelSubscription.boom_status == "0") {
                        relativeBoom.isEnabled = false
                        tv_earn.text = "Congrats, you saved 20% off next month"
                        tv_earn.setCompoundDrawablesWithIntrinsicBounds(
                            R.drawable.ic_ellipse,
                            0,
                            0,
                            0
                        )
                        val params = LinearLayout.LayoutParams(
                            ViewGroup.LayoutParams.WRAP_CONTENT,
                            ViewGroup.LayoutParams.WRAP_CONTENT
                        )
                        params.setMargins(30, 50, 10, 50)
                        params.gravity = Gravity.CENTER
                        params.height = 60
                        tv_earn.layoutParams = params
                        tv_earn.textSize = 12f
                        tv_earn.setTextColor(resources.getColor(R.color.colorWhite))
                        relativeBoom.setCardBackgroundColor(resources.getColor(R.color.home_free))
                        imageGift.visibility = View.GONE
                    } else {
                        imageGift.visibility = View.VISIBLE
                        relativeBoom.setCardBackgroundColor(resources.getColor(R.color.colorWhite))
                        relativeBoom.isEnabled = true

                    }
                }
            } else {
                if (modelSubscription.boom_discount != "0") {
                    relativeBoom.isEnabled = false
                    tv_earn.setCompoundDrawablesWithIntrinsicBounds(
                        R.drawable.ic_ellipse,
                        0,
                        0,
                        0
                    )
                    val params = LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT
                    )
                    params.setMargins(30, 35, 10, 35)
                    params.gravity = Gravity.CENTER
                    params.height = 60
                    tv_earn.text = "Congrats, you saved 20% off next month"
                    tv_earn.layoutParams = params
                    tv_earn.textSize = 12f
                    tv_earn.setTextColor(resources.getColor(R.color.colorWhite))
                    imageGift.visibility = View.GONE
                    relativeBoom.setCardBackgroundColor(resources.getColor(R.color.home_free))
                } else {
                    imageGift.visibility = View.VISIBLE
                    relativeBoom.setCardBackgroundColor(resources.getColor(R.color.colorWhite))
                    relativeBoom.isEnabled = true
                }
            }

            textViewName.text = it.name.trim()
            textView107.text = it.likes + "%"
            tvBoughtt.text = it.bought
            tvView.text = it.views
            textView120.text = it.company_name.trim()

            if (it.remaining_subscriber_limit != "") {
                if (it.remaining_subscriber_limit.toInt() <= 0) {
                    textView122.text = "Plan Sold Out"
                }
            }

            endsInDays(it.campaign_end)

        })

        mViewModel.getmPlanData().observe(this, Observer {
            progressBarSubscription.visibility = View.GONE
            planListData = it
            if (intent.hasExtra("cancelled")) {
                val modelSubscriptionn =
                    intent.getParcelableExtra<MySubscriptionsModel>("cancelled")
                for (i in 0 until it.size) {
                    if (modelSubscriptionn.id == it[i].subscription_id) {
                        subscribeBtn = 1
                        it[i].isSelected = true
                        planModel = it[i]
                    }
                }
            }

//            if (planListData.size > 0) {
//                if (planListData[0].introductory_days == "0" && planListData[0].free_trial == "0" ||
//                    planListData[0].introductory_days == "0" && planListData[0].free_trial == "No free trial"
//                ) {
//                    daysPlan.visibility = View.GONE
//                } else if (planListData[0].introductory_days == "0") {
//                    daysPlan.visibility = View.GONE
//                    daysPlan.text = planListData[0].free_trial + " Free Trail"
//                } else if (planListData[0].introductory_days == "1") {
//                    daysPlan.visibility = View.GONE
//                    daysPlan.text = planListData[0].introductory_days + " Day Introductory Price"
//                } else {
//                    daysPlan.visibility = View.GONE
//                    daysPlan.text = planListData[0].introductory_days + " Days Introductory Price"
//                }
//            }
            mAdapterPlan.update(it)
        })

        mViewModel.getmSubData().observe(this, Observer {
            progressBarSubscription.visibility = View.GONE
            if (it.size > 0) {
                setAdapter(0)
                mySubscriptionModel = it[0]
            } else {
                setAdapter(1)
            }
        })

        mViewModel.getmDataStaff().observe(this, Observer {
            progressBarSubscription.visibility = View.GONE
            mAdapterStaffList.update(it)
        })

        mViewModel.getmDataSimilarSubscription().observe(this, Observer {
            //            if (it.size > 0) {
//                textView95.visibility = View.VISIBLE
//                recyclerSimilarList.visibility = View.VISIBLE
//            } else {
//                textView95.visibility = View.GONE
//                recyclerSimilarList.visibility = View.GONE
//            }
//
//            if (it.size >= 5) {
//                viewAllSimilar.visibility = View.VISIBLE
//            } else {
//                viewAllSimilar.visibility = View.GONE
//            }
//            mAdapterSimilarSubscription.update(it)
        })


        mViewModel.getmDataImages().observe(this, Observer {
            progressBarSubscription.visibility = View.GONE
            myCustomPagerAdapter!!.update(it)
        })

        mViewModel.getmDataHomeSubscription().observe(this, Observer {
            progressBarSubscription.visibility = View.GONE
            if (it.status == "true") {
                modelSubscription.favourite = it.favourite
                if (it.favourite == 1) {
                    shareFav.setImageResource(R.drawable.ic_favorite_fill)
                } else {
                    shareFav.setImageResource(R.drawable.ic_favorite_border_white)
                }
            }


        })

        mViewModel.getmDataInformation().observe(this, Observer {
            progressBarSubscription.visibility = View.GONE
            whatsInclude.text = it.about.replace("<br />", "").replace("<br/>", "")
            arrayTags = ArrayList()
            rvTag.removeAllViews()
            chipData(it.parentTags)

            if (it.modelLatestreviewsData.size >= 3) {
                reviewViewAll.visibility = View.VISIBLE
            } else {
                reviewViewAll.visibility = View.GONE
            }

            mAdapterReviewsDescription.update(it.modelLatestreviewsData)
            mAdapterReviewsCount.update(it.reviewsmodel)
            val calll = it.modelBrandContact.call
            val latlong = it.modelBrandContact.lat + "," + it.modelBrandContact.long
            val web = it.modelBrandContact.website
            val scheduleData = it.schedule
            if (scheduleData.isNotEmpty()) {
                schedule.visibility = View.VISIBLE
                scheduleAppointment.visibility = View.VISIBLE
            } else {
                schedule.visibility = View.GONE
                scheduleAppointment.visibility = View.GONE
            }


            call.setOnClickListener {
                startActivity(Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", calll, null)))
            }

            textView114.setOnClickListener {
                if (web.isNotEmpty()) {
                    try {
                        if (!web.contains("http") || !web.contains("https")) {
                            val browserIntent =
                                Intent(Intent.ACTION_VIEW, Uri.parse("https://$web"))
                            startActivity(browserIntent)
                        } else {
                            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(web))
                            startActivity(browserIntent)
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }


            scheduleAppointment.setOnClickListener {
                if (scheduleData.isNotEmpty()) {
                    try {
                        if (!scheduleData.contains("http") || !scheduleData.contains("https")) {
                            val browserIntent =
                                Intent(Intent.ACTION_VIEW, Uri.parse("https://$schedule"))
                            startActivity(browserIntent)
                        } else {
                            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(scheduleData))
                            startActivity(browserIntent)
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            direction.setOnClickListener {

                val intent = Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("google.navigation:q=$latlong")
                )
                startActivity(intent)
            }
        })

        mViewModel.getSuccessfulResponse().observe(this, Observer {
            progressBarSubscription.visibility = View.GONE
            if (it.status == "true") {
                progressBar25.visibility = View.GONE
                val mDialogView = LayoutInflater.from(this)
                    .inflate(R.layout.days_left_alert, null)
                val mBuilder = AlertDialog.Builder(this)
                    .setView(mDialogView)
                val mAlertDialog = mBuilder.show()
                val textView249: TextView =
                    mDialogView.findViewById(R.id.textView249)
                val okBtn: TextView = mDialogView.findViewById(R.id.okBtn)

                val title: TextView = mDialogView.findViewById(R.id.textViewTitle)
                title.text = "Sold Out"
                textView249.text =
                    "This plan is currently sold out, we'll let you know when it is available to purchase."
                mAlertDialog.setCancelable(false)
                mAlertDialog.window.setBackgroundDrawableResource(R.drawable.bg_alert_white)

                okBtn.setOnClickListener {
                    mAlertDialog.dismiss()
                    finish()
                }
            } else {
                progressBar25.visibility = View.GONE
            }
        })


        mViewModel.getmDataLocation().observe(this, Observer
        {
            viewPagerMaps.layoutManager =
                LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
            mAdapterPager = MapsPagerAdapter(this, Constant.getDisplayMetrics(windowManager))
            mAdapterPager!!.setItemMargin(16)
            mAdapterPager!!.updateDisplayMetrics()
            indicatorMaps.setRecyclerView(viewPagerMaps)
            indicatorMaps.forceUpdateItemCount()
//            PagerSnapHelper().attachToRecyclerView(viewPagerMaps)
            viewPagerMaps.adapter = mAdapterPager
            mAdapterPager!!.update(it)
            mAdapterOperationHours.update(it)
        })


    }

    @SuppressLint("SetTextI18n")
    private fun endsInDays(campaign_end: String) {
        if (campaign_end != "0000-00-00" && campaign_end != "") {
            val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            val c = Calendar.getInstance()
            try {
                c.time = sdf.parse("$campaign_end 23:59:59")
            } catch (e: Exception) {
                e.printStackTrace()
            }
            Log.e("checkTime", planListData[0].created_at)
            val output = sdf.format(c.time)
            val currentDateandTime = Constant.getDate(output)
            val daysSpan = daysCalculation(currentDateandTime)

            when {
                daysSpan.toInt() <= 0 -> {
                    return
                }
                daysSpan.toInt() <= 7 -> {
                    daysPlan.visibility = View.VISIBLE
                    try {
                        if (daysSpan.toInt() > 1) {
                            daysPlan.text =
                                "Ends in $daysSpan days"
                        } else {
                            daysPlan.text =
                                "Ends in $daysSpan day"
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                }
                else -> {
                    return
                }
            }
        }

    }

    private fun apiInitialization() {
        val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
        campaignId = intent.getStringExtra("modelSubscriptionView")
        progressBarSubscription.visibility = View.VISIBLE
        mViewModel.getSubscriptionDetails(auth, campaignId)

    }

    private fun bottomSheetInit() {
        val bottomSheetBehavior = BottomSheetBehavior.from(bottom_sheet)
        bottomSheetBehavior.peekHeight = 500
        showHideBottomSheet.setOnClickListener {
            if (shofull == 0) {
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
                line.visibility = View.GONE
                downImage.visibility = View.VISIBLE
                shofull = 1
                if (subscribeBtn == 1) {
                    btn_buy.visibility = View.VISIBLE
                } else {
                    btn_buy.visibility = View.GONE
                }

//                linearViewGifTap.setBackgroundColor(resources.getColor(R.color.blue))
            } else {
                shofull = 0
                line.visibility = View.VISIBLE
                downImage.visibility = View.GONE
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
                btn_buy.visibility = View.GONE

//                linearViewGifTap.setBackgroundColor(0)
            }
        }
        bottomSheetBehavior.setBottomSheetCallback(object :
            BottomSheetBehavior.BottomSheetCallback() {
            @SuppressLint("SwitchIntDef")
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                // React to state change
                when (newState) {
                    BottomSheetBehavior.STATE_COLLAPSED -> {
                        line.visibility = View.VISIBLE
                        downImage.visibility = View.GONE
//                        linearViewGifTap.setBackgroundColor(resources.getColor(R.color.blue))
                        nestedScrollView2.scrollTo(0, 0)
//                        linearViewGifTap.visibility = View.GONE
                        shofull = 0
                        btn_buy.visibility = View.GONE
                    }

                    BottomSheetBehavior.STATE_DRAGGING -> {

                    }

                    BottomSheetBehavior.STATE_EXPANDED -> {
                        shofull = 1
                        line.visibility = View.GONE
                        downImage.visibility = View.VISIBLE
                        if (subscribeBtn == 1) {
                            btn_buy.visibility = View.VISIBLE
                        } else {
                            btn_buy.visibility = View.GONE
                        }
//                        linearViewGifTap.visibility = View.VISIBLE
//                        linearViewGifTap.setBackgroundColor(0)
                    }

                    BottomSheetBehavior.STATE_HIDDEN -> {
                        btn_buy.visibility = View.GONE
                    }
                    BottomSheetBehavior.STATE_SETTLING -> {
                        btn_buy.visibility = View.GONE
                    }


                }
            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {
                // React to dragging events
                Log.e("onSlide", "onSlide")
            }
        })
//        }, 100)

    }


//    private fun setToolbar() {
//        val toolbar = findViewById<Toolbar>(R.id.msToolbar)
//        setSupportActionBar(toolbar)
//        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
//        title = ""
//        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white)
//    }


    private fun init() {
        showHideBottomSheet = findViewById(R.id.showHideBottomSheet)
        textView95 = findViewById(R.id.textView95)
        indicatorMaps = findViewById(R.id.indicatorMaps)
        shareFav = findViewById(R.id.shareFav)
        linearFavorite = findViewById(R.id.linearFavorite)
        shareLike = findViewById(R.id.linearShare)
        downImage = findViewById(R.id.downImage)
        relativeBoom = findViewById(R.id.relativeBoom)
        viewPagerMaps = findViewById(R.id.viewPagerMaps)
        linearBack = findViewById(R.id.linearBack)
        line = findViewById(R.id.line)
        bottom_sheet = findViewById(R.id.bottom_sheet)
        indicator_login = findViewById(R.id.indicator_login)
        rvPlan = findViewById(R.id.rvPlan)
        rv_one_include = findViewById(R.id.rv_one_include)
        recyclerViewReviews = findViewById(R.id.recyclerViewReviews)
        recyclerViewReviewDescription = findViewById(R.id.recyclerViewReviewDescription)
        recyclerWorkingDays = findViewById(R.id.recyclerWorkingDays)
        viewPager = findViewById(R.id.vpMerchant)
//        boomText = findViewById(R.id.boomText)
        mrecyclerSimilarList = findViewById(R.id.recyclerSimilarList)
        tvInformation = findViewById(R.id.tvInformation)
        tvStaff = findViewById(R.id.tvStaff)
        reviewViewAll = findViewById(R.id.reviewViewAll)
        recyclerView2 = findViewById(R.id.recyclerView2)
        rv_Extraa = findViewById(R.id.rv_Extraa)
        staffDescriptionRv = findViewById(R.id.staffDescriptionRv)
        cardView7 = findViewById(R.id.cardView7)
        nestedScrollView2 = findViewById(R.id.nestedScrollView2)
        cl_extra = findViewById(R.id.cl_extra)
        rvPlanTwo = findViewById(R.id.rvPlanTwo)
        btn_buy = findViewById(R.id.btn_buy)
        mChip = findViewById(R.id.chip)
        rvTag = findViewById(R.id.rvTags)
        cl_button = findViewById(R.id.cl_button)
        btn_manage = findViewById(R.id.btn_manage)
        viewAllSimilar = findViewById(R.id.viewAllSimilar)
        textView122 = findViewById(R.id.textView122)
        daysPlan = findViewById(R.id.daysPlan)
        manage = findViewById(R.id.manage)
        relativeTool = findViewById(R.id.relativeTool)
        progressBarSubscription = findViewById(R.id.progressBarSubscription)
        progressBar25 = findViewById(R.id.progressBar25)
        tv_earn = findViewById(R.id.tv_earn)

    }


    override fun onResume() {
        super.onResume()
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_FULLSCREEN
        actionBar?.show()
        try {
//            arrayTags.clear()
            val editor = getSharedPreferences("gifTutorial1", Context.MODE_PRIVATE)
            val gifData = editor.getString(Constant.gifTutorial1, "")
            if (gifData.isEmpty()) {
                linearViewGif.visibility = View.VISIBLE
                Glide.with(this).load(R.drawable.swipe_merchant_gif).into(imageGif)
            } else {
                linearViewGif.visibility = View.GONE
            }

            apiInitialization()
            btn_buy.visibility = View.GONE
            val mPrefs = getSharedPreferences("ProductArray", MODE_PRIVATE)
            val gson = Gson()

            val json = mPrefs.getString("myArray", "")
            if (json.isNotEmpty()) {
                val type = object : TypeToken<List<ModelProduct>>() {

                }.type
                val callLog: ArrayList<ModelProduct> = gson.fromJson(json, type)
                val newArray: ArrayList<SaveExtraModel> = ArrayList()



                for (i in 0 until callLog.size) {
                    tvExtrasPlan.visibility = View.VISIBLE
                    val modelStaffList =
                        SaveExtraModel()
                    modelStaffList.textViewExtra = callLog[i].name
                    modelStaffList.costExtra = callLog[i].amount
                    newArray.add(modelStaffList)
                }


                rv_Extraa.layoutManager = LinearLayoutManager(this)
                rv_Extraa.itemAnimator = DefaultItemAnimator()
                rv_Extraa.isNestedScrollingEnabled = false
                rv_Extraa.adapter =
                    SaveExtraAdapter(this, newArray)


            }
        } catch (e: Exception) {
        }
    }

    //    setAdapter
    private fun setAdapter(i: Int) {
        if (i == 0) {
            rvPlanTwo.visibility = View.VISIBLE
            rvPlan.visibility = View.GONE

            manage.visibility = View.VISIBLE

            daysPlan.visibility = View.GONE
            relativeTool.visibility = View.GONE
            textView122.visibility = View.GONE
            rvPlanTwo.layoutManager = LinearLayoutManager(this)
            rvPlanTwo.isNestedScrollingEnabled = false
            mAdapterPlanTwo = AdapterPlanTwo(this, planTwoList())
            rvPlanTwo.adapter = mAdapterPlanTwo

            if (intent.hasExtra("MySubscriptionsModel")) {
                if (intent.hasExtra("cancelled")) {
                    btn_manage.visibility = View.GONE
//                    btn_buy.visibility = View.VISIBLE
                } else {
                    btn_buy.visibility = View.GONE
                    btn_manage.visibility = View.VISIBLE
                }
            }

            btn_buy.setOnClickListener {
                if (modelSubscription.remaining_subscriber_limit != "") {
                    if (modelSubscription.remaining_subscriber_limit.toInt() <= 0) {
                        mViewModel.buyCampaignRequest(campaignId)
                        progressBar25.visibility = View.VISIBLE
                    } else {
                        val fragment = MenuFragment(modelSubscription, planModel)
                        fragment.show(supportFragmentManager, fragment.tag)
                    }
                }
            }

            btn_manage.setOnClickListener {
                if (intent.hasExtra("MySubscriptionsModel")) {
                    mySubscriptionModel = intent.getParcelableExtra("MySubscriptionsModel")
                }
                val intent = Intent(this, ManageSubscriptionsActivity::class.java)
                intent.putExtra("model", mySubscriptionModel)
                startActivity(intent)
            }


        } else {
            rvPlan.visibility = View.VISIBLE
            rvPlanTwo.visibility = View.GONE
            btn_manage.visibility = View.GONE
            manage.visibility = View.GONE
            daysPlan.visibility = View.VISIBLE
            relativeTool.visibility = View.VISIBLE
            textView122.visibility = View.VISIBLE

            btn_buy.setOnClickListener {
                if (modelSubscription.remaining_subscriber_limit != "") {
                    if (modelSubscription.remaining_subscriber_limit.toInt() <= 0) {
                        mViewModel.buyCampaignRequest(campaignId)
                        progressBar25.visibility = View.VISIBLE
                    } else {
                        val fragment = MenuFragment(modelSubscription, planModel)
                        fragment.show(supportFragmentManager, fragment.tag)
                    }
                }
            }


        }


        rv_one_include.layoutManager = LinearLayoutManager(this)
        rv_one_include.itemAnimator = DefaultItemAnimator()
        rv_one_include.isNestedScrollingEnabled = false
        rv_one_include.adapter =
            DealAdapter(this, dealList())

        recyclerViewReviews.layoutManager =
            LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
        recyclerViewReviews.isNestedScrollingEnabled = false
        mAdapterReviewsCount = AdapterReviews(this)
        recyclerViewReviews.adapter = mAdapterReviewsCount

        recyclerViewReviewDescription.layoutManager = LinearLayoutManager(this)
        recyclerViewReviewDescription.isNestedScrollingEnabled = false
        mAdapterReviewsDescription = AdapterReviewDescription(this, 0)
        recyclerViewReviewDescription.adapter = mAdapterReviewsDescription

        recyclerWorkingDays.layoutManager = LinearLayoutManager(this)
        recyclerWorkingDays.isNestedScrollingEnabled = false
        mAdapterOperationHours = AdapterWorkingDays(this)
        recyclerWorkingDays.adapter = mAdapterOperationHours

        mrecyclerSimilarList.layoutManager =
            LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
        mrecyclerSimilarList.itemAnimator = DefaultItemAnimator()
        mrecyclerSimilarList.isNestedScrollingEnabled = false
        mAdapterSimilarSubscription = SimilarProductsAdapter(this, 0)
        mrecyclerSimilarList.adapter = mAdapterSimilarSubscription


        recyclerView2.layoutManager = LinearLayoutManager(this)
        recyclerView2.isNestedScrollingEnabled = false
        mAdapterStaffList = AdapterStaffList(this)
        recyclerView2.adapter = mAdapterStaffList

        staffDescriptionRv.layoutManager = LinearLayoutManager(this)
        staffDescriptionRv.itemAnimator = DefaultItemAnimator()
        staffDescriptionRv.isNestedScrollingEnabled = false
        staffDescriptionRv.adapter = StaffFaqAdapter(this, stafFaqList())


    }


    @SuppressLint("SimpleDateFormat")
    private fun planTwoList(): ArrayList<ModelChoosePlanTwo> {
        val arrayList = ArrayList<ModelChoosePlanTwo>()

        if (planListData.isNotEmpty()) {
            var modelChoosePlanTwo = ModelChoosePlanTwo()
            if (planListData[0].free_trial == "No free trial" && planListData[0].introductory_days == "0") {
                modelChoosePlanTwo.visitNumber = "N/A"
            } else {
                var totalDaysToAdd = 0
                var title = ""
                var subtitle = ""
                var daysSpan: String = ""
                if (planListData[0].free_trial != "No free trial" && planListData[0].introductory_days == "0") {
                    when {
                        planListData[0].free_trial.contains("1 Week") -> {
                            totalDaysToAdd = 7
                        }
                        planListData[0].free_trial.contains("2 Weeks") -> {
                            totalDaysToAdd = 14
                        }
                        planListData[0].free_trial.contains("1 Month") -> {
                            totalDaysToAdd = 30
                        }
                        planListData[0].free_trial.contains("3 Months") -> {
                            totalDaysToAdd = 90
                        }
                        planListData[0].free_trial.contains("6 Months") -> {
                            totalDaysToAdd = 180
                        }
                        // number of days to add, can also use Calendar.DAY_OF_MONTH in place of Calendar.DATE
                    }
                    val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                    val c = Calendar.getInstance()
                    try {
                        c.time = sdf.parse(planListData[0].created_at)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    c.add(
                        Calendar.DATE,
                        totalDaysToAdd
                    )  // number of days to add, can also use Calendar.DAY_OF_MONTH in place of Calendar.DATE
                    val sdf1 = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                    val output = sdf1.format(c.time)
                    val currentDateandTime = Constant.getDate(output)
                    daysSpan = daysCalculation(currentDateandTime)
                    title = "Free Trial"
                    subtitle = planListData[0].free_trial + " of Free Trial"


                } else if (planListData[0].free_trial == "No free trial" && planListData[0].introductory_days != "0") {
                    var frequencyType = ""
                    if (planListData[0].frequency_type == "Monthly") {
                        totalDaysToAdd = planListData[0].introductory_days.toInt() * 30
                        frequencyType = if (planListData[0].introductory_days.toInt() == 1) {
                            "Month"
                        } else {
                            "Months"
                        }

                    } else if (planListData[0].frequency_type == "Yearly") {
                        totalDaysToAdd = planListData[0].introductory_days.toInt() * 365
                        frequencyType = if (planListData[0].introductory_days.toInt() == 1) {
                            "Year"
                        } else {
                            "Years"
                        }
                    }
                    val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                    val c = Calendar.getInstance()
                    try {
                        c.time = sdf.parse(planListData[0].created_at)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    c.add(
                        Calendar.DATE,
                        totalDaysToAdd
                    )  // number of days to add, can also use Calendar.DAY_OF_MONTH in place of Calendar.DATE
                    val sdf1 = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                    val output = sdf1.format(c.time)
                    val currentDateandTime = Constant.getDate(output)
                    daysSpan = daysCalculation(currentDateandTime)
                    title = "Intro Price"
                    subtitle =
                        planListData[0].introductory_days + " $frequencyType at Introductory Days"
                } else {

                    when {
                        planListData[0].free_trial.contains("1 Week") -> {
                            totalDaysToAdd = 7
                        }
                        planListData[0].free_trial.contains("2 Weeks") -> {
                            totalDaysToAdd = 14
                        }
                        planListData[0].free_trial.contains("1 Month") -> {
                            totalDaysToAdd = 30
                        }
                        planListData[0].free_trial.contains("3 Months") -> {
                            totalDaysToAdd = 90
                        }
                        planListData[0].free_trial.contains("6 Months") -> {
                            totalDaysToAdd = 180
                        }
                        // number of days to add, can also use Calendar.DAY_OF_MONTH in place of Calendar.DATE
                    }
                    val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                    val c = Calendar.getInstance()
                    try {
                        c.time = sdf.parse(planListData[0].created_at)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    c.add(
                        Calendar.DATE,
                        totalDaysToAdd
                    )  // number of days to add, can also use Calendar.DAY_OF_MONTH in place of Calendar.DATE
                    val sdf1 = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                    val output = sdf1.format(c.time)
                    val currentDateandTime = Constant.getDate(output)
                    daysSpan = daysCalculation(currentDateandTime)
                    title = "Free Trial"
                    subtitle = planListData[0].free_trial + " at Free Trial"
                }

                if (daysSpan.toInt() > 0) {
                    modelChoosePlanTwo.trial = title

                    if (daysSpan == "1") {
                        modelChoosePlanTwo.visitNumber = daysSpan
                        modelChoosePlanTwo.visitLeft = "Day left"
                    } else {
                        modelChoosePlanTwo.visitNumber = daysSpan
                        modelChoosePlanTwo.visitLeft = "Days left"
                    }
                    modelChoosePlanTwo.total = subtitle

                    arrayList.add(modelChoosePlanTwo)

                }

                Log.e("totalDaysToAdd", totalDaysToAdd.toString())
                Log.e("title", title)
                Log.e("daysSpan", daysSpan)
            }

            modelChoosePlanTwo = ModelChoosePlanTwo()
            modelChoosePlanTwo.trial = "Quantity"
            var quantity = ""

            if (planListData[0].redeemtion_cycle_qty == "500000") {
                quantity = "Unlimited"
                modelChoosePlanTwo.total = "Total of " + quantity + " " + planListData[0].unit
                modelChoosePlanTwo.visitNumber = "Unlimited"
                modelChoosePlanTwo.visitLeft = "visits left"
            } else {
                quantity = planListData[0].redeemtion_cycle_qty
                val total =
                    quantity.toInt() - planListData[0].redeem_count.toInt()

                modelChoosePlanTwo.total =
                    "Total of " + quantity + " " + planListData[0].unit + " " + "Service"


                when {
                    total == 1 -> {
                        modelChoosePlanTwo.visitNumber = total.toString()
                        modelChoosePlanTwo.visitLeft = "Visit left"
                    }
                    total <= 0 -> {
                        modelChoosePlanTwo.visitNumber = "0"
                        modelChoosePlanTwo.visitLeft = "Visit left"
                    }
                    else -> {
                        modelChoosePlanTwo.visitNumber = total.toString()
                        modelChoosePlanTwo.visitLeft = "Visits left"
                    }
                }
            }
            arrayList.add(modelChoosePlanTwo)
            modelChoosePlanTwo = ModelChoosePlanTwo()
            modelChoosePlanTwo.trial = "Frequency"

            val frequencyValue = planListData[0].frequency_value.toInt()
            var frequencyType = ""
            var totalDaysToAdd = 0


            if (planListData[0].frequency_type == "Monthly") {
                totalDaysToAdd = frequencyValue * 30
                frequencyType = "Monthly"
            } else if (planListData[0].frequency_type == "Yearly") {
                totalDaysToAdd = frequencyValue * 365
                frequencyType = "Yearly"
            }


            val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            val c = Calendar.getInstance()
            try {
                c.time = sdf.parse(planListData[0].created_at)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            c.add(
                Calendar.DATE,
                totalDaysToAdd
            )  // number of days to add, can also use Calendar.DAY_OF_MONTH in place of Calendar.DATE
            val sdf1 = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            val output = sdf1.format(c.time)
            val currentDateandTime = Constant.getDate(output)
            val daysSpan: String = daysCalculation(currentDateandTime)

            when {
                daysSpan == "1" -> {
                    modelChoosePlanTwo.visitNumber = daysSpan
                    modelChoosePlanTwo.visitLeft = "Day left"

                }
                daysSpan.toInt() <= 0 -> {
                    modelChoosePlanTwo.visitNumber = "0"
                    modelChoosePlanTwo.visitLeft = "Day left"
                }
                else -> {
                    modelChoosePlanTwo.visitNumber = daysSpan
                    modelChoosePlanTwo.visitLeft = "Days left"
                }
            }

            if (frequencyType == "Monthly" || frequencyType == "1") {
                modelChoosePlanTwo.total =
                    "Renew once per month"
            } else if (frequencyType == "Yearly" || frequencyType == "2") {
                modelChoosePlanTwo.total =
                    "Renew once per year"
            }


//        modelChoosePlanTwo.total = planListData.frequency_type
//        modelChoosePlanTwo.visitNumber = "45"
//        modelChoosePlanTwo.visitLeft = "visit left"
            arrayList.add(modelChoosePlanTwo)
        }
        return arrayList
    }

    @SuppressLint("SimpleDateFormat")
    fun showDialog() {
        if (modelSubscription.campaign_end != "0000-00-00" && modelSubscription.campaign_end != "") {
            val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            val c = Calendar.getInstance()
            try {
                c.time = sdf.parse(modelSubscription.campaign_end + " 23:59:59")
            } catch (e: Exception) {
                e.printStackTrace()
            }
            Log.e("checkTime", planListData[0].created_at)
            val output = sdf.format(c.time)
            val currentDateandTime = Constant.getDate(output)
            val daysSpan = daysCalculation(currentDateandTime)

            when {
                daysSpan.toInt() <= 0 -> {
                    return
                }
                daysSpan.toInt() <= 7 -> {
                    val mDialogView = LayoutInflater.from(this)
                        .inflate(R.layout.days_left_alert, null)
                    val mBuilder = AlertDialog.Builder(this)
                        .setView(mDialogView)
                    val mAlertDialog = mBuilder.show()
                    val textView249: TextView =
                        mDialogView.findViewById(R.id.textView249)
                    val okBtn: TextView = mDialogView.findViewById(R.id.okBtn)

                    val title: TextView = mDialogView.findViewById(R.id.textViewTitle)

                    title.text = "Subscription Expiring Soon"

                    try {
                        if (daysSpan.toInt() > 1) {
                            textView249.text =
                                "This subscription will expire in $daysSpan days. You will not be able to redeem your plan once it expires. Are you sure you want to continue with your purchase?"

                        } else {
                            textView249.text =
                                "This subscription will expire in $daysSpan day. You will not be able to redeem your plan once it expires. Are you sure you want to continue with your purchase?"

                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }


                    mAlertDialog.setCancelable(false)
                    mAlertDialog.window.setBackgroundDrawableResource(R.drawable.bg_alert_white)

                    okBtn.setOnClickListener {
                        mAlertDialog.dismiss()
                    }

                }
                else -> {
                    return
                }
            }
        }
    }

    private fun daysCalculation(currentDateandTime: Date): String {
        val serverMillis = currentDateandTime.time
        val currentMillis = System.currentTimeMillis()
        val millis = serverMillis - currentMillis
        val secondMillis = 1000
        val minuteMillis = secondMillis * 60
        val hoursMillis = minuteMillis * 60
        val daysMillis = hoursMillis * 24

        val daysMil = millis / daysMillis
        val afterDays = millis - (daysMillis * daysMil)
        val hours = afterDays / hoursMillis
        val minutes = (afterDays - (hoursMillis * hours)) / minuteMillis

        return daysMil.toString()
//        daysTotal.text = daysSpan
//        hoursTotal.text = hours.toString()
//        minTotal.text = minutes.toString()
    }


    private fun stafFaqList(): ArrayList<StaffFaqModel> {

        val arrayList: ArrayList<StaffFaqModel> = ArrayList()

        var modelStaffList = StaffFaqModel()
        modelStaffList.textQuestion = "How do Upscribbr subscriptions work?"
        modelStaffList.textAnswer =
            "Simple. You can either be invited directly by a business or use the “Discover” function in our app to find businesses and subscriptions.Subscriptions are made up of 3 key elements:\n1. Service \n2. Price \n3. Billing period (Weekly, Monthly, and Annually).You select the subscriptions that meet your needs and then you’re billed based on the frequency of your service. Pause, skip or cancel anytime — right from your phone."
        arrayList.add(modelStaffList)

        modelStaffList = StaffFaqModel()
        modelStaffList.textQuestion = "I can’t find a local business I love on Upscribbr!"
        modelStaffList.textAnswer =
            "Aw shucks! Let’s fix that! First, ask the business if they are on Upscribbr. If they are on the platform, ask them to send you an invite to purchase a subscription. If they are not on the platform yet, you can request to add them via our business request form — from there, our people will call your people."
        arrayList.add(modelStaffList)

        modelStaffList = StaffFaqModel()
        modelStaffList.textQuestion = "How do I pause a subscription?"
        modelStaffList.textAnswer =
            "Pausing subscriptions is simple. Within your “subscriptions” tab, select the subscription you’d like to pause and tap “manage subscription.” From here, you’ll be able to select “pause subscription” — this page will detail the time remaining before your next billing cycle and will just ask you to confirm that you’re ready to pause your subscription. Ta-da – it’s magic."
        arrayList.add(modelStaffList)

        modelStaffList = StaffFaqModel()
        modelStaffList.textQuestion = "Where are subscriptions available?"
        modelStaffList.textAnswer =
            "Currently, we’re headquartered in Minneapolis, MN and focused on growing our footprint here. However, businesses anywhere in the US can download and begin using Upscribbr now. If you want to bring your favorite business to Upscribbr sooner, you can add them to the platform here."
        arrayList.add(modelStaffList)


        modelStaffList = StaffFaqModel()
        modelStaffList.textQuestion = "How do I share a subscription?"
        modelStaffList.textAnswer =
            "To share a subscription, head to your “subscriptions” tab, select the subscription you’d like to share and tap “manage subscription.” From here, you’ll be able to select “share subscription” — you’ll be asked to provide the recipient’s full name, phone number and email address (optional). Your friends and family will receive a link to download the app and signup. Once they sign in to Upscribbr, they will receive an alert showing the shared subscription. The subscription can be redeemed by both the sharer and the receiver up to the end of the billing cycle or until there is no quantity left to be redeemed. Once the receiver redeems one quantity of the shared subscription, their subscription redeems bar code will be deactivated."
        arrayList.add(modelStaffList)

        modelStaffList = StaffFaqModel()
        modelStaffList.textQuestion = "How do I cancel a subscription?"
        modelStaffList.textAnswer =
            "You can cancel any subscription anytime – up to 24 hours before your subscription is set to be renewed. Simply navigate to your “subscriptions” tab, select the subscription you’d like to cancel and tap “manage subscription.” From here, you’ll be able to select “cancel a subscription.” It’s that simple!"
        arrayList.add(modelStaffList)




        return arrayList
    }


    //    About this Deal
    private fun dealList(): ArrayList<DealModel> {
        val arrayList: ArrayList<DealModel> = ArrayList()

        var dealModel = DealModel()
        dealModel.bullet = R.drawable.bullet_bg
        dealModel.includeText = "5 manicures"
        arrayList.add(dealModel)

        dealModel = DealModel()
        dealModel.bullet = R.drawable.bullet_bg
        dealModel.includeText = "5 manicures"
        arrayList.add(dealModel)


        dealModel = DealModel()
        dealModel.bullet = R.drawable.bullet_bg
        dealModel.includeText = "Free Coffee"
        arrayList.add(dealModel)
        arrayList.add(dealModel)



        dealModel = DealModel()
        dealModel.bullet = R.drawable.bullet_bg
        dealModel.includeText = "Food Massage"
        arrayList.add(dealModel)


        dealModel = DealModel()
        dealModel.bullet = R.drawable.bullet_bg
        dealModel.includeText = "5 manicures"
        arrayList.add(dealModel)

        return arrayList

    }

    private fun chipData(it: ArrayList<ModeTags>) {
        for (i in 0 until it.size) {
            arrayTags.add(it[i])
        }


        val categoryName = Constant.getPrefs(this).getString(Constant.tags, "")
        if (categoryName.isNotEmpty() && categoryName != "[]") {
            val gson = Gson()
            val json = Constant.getPrefs(this).getString(Constant.tags, null)
            val type = gson.fromJson<ArrayList<ModeTags>>(
                json,
                object : TypeToken<List<ModeTags>>() {

                }.type
            )
//            type.addAll(type)
            if (type.size > 0) {

                for (i in 0 until arrayTags.size) {

                    for (j in 0 until type.size) {
                        if (type[j].parent_id == arrayTags[i].parent_id) {

                            type.removeAt(j)
                            break
                        }
                    }
                    type.add(0, arrayTags[i])


                    if (type.size > 5) {
                        type.removeAt(type.size - 1)
                    }


                }
//                type.clear()
//                type.addAll(type2)
                val jsonData = gson.toJson(type)
                val editor = Constant.getPrefs(this).edit()
                editor.putString(Constant.tags, jsonData)
                editor.apply()
            }
        } else {
            val gson = Gson()
            val json = gson.toJson(arrayTags)
            val editor = Constant.getPrefs(this).edit()
            editor.putString(Constant.tags, json)
            editor.apply()
        }



        for (index in arrayTags.indices) {
            val chip = Chip(rvTag.context)
            chip.text = arrayTags[index].parent_name

            // necessary to get single selection working
            chip.isClickable = true
            chip.isCheckable = false
            chip.setTextAppearanceResource(R.style.ChipTextStyle_Selected)
            chip.setChipBackgroundColorResource(R.color.colorSkip)
            chip.chipEndPadding = 20.0f
            chip.includeFontPadding = false
            chip.gravity = Gravity.CENTER
            chip.setOnClickListener {
                startActivity(
                    Intent(this, SubCategoriesActivity::class.java)
                        .putExtra("model", arrayTags[index].parent_name).putExtra(
                            "chips",
                            arrayTags[index].parent_id
                        )
                )
            }
            rvTag.addView(chip)
        }
        rvTag.isSingleSelection = true

    }


    private fun clickEvents() {
        tvInformation.setOnClickListener {
            tvInformation.setBackgroundResource(R.drawable.bg_info)
            tvStaff.setBackgroundResource(R.drawable.bg_switch_fragments)
            tvInformation.setTextColor(resources.getColor(R.color.colorWhite))
            tvStaff.setTextColor(resources.getColor(R.color.payments))
            planInformation.visibility = View.VISIBLE
            planStaff.visibility = View.GONE
        }


        linearViewGif.setOnClickListener {
            val editor = getSharedPreferences("gifTutorial1", Context.MODE_PRIVATE).edit()
            editor.putString(Constant.gifTutorial1, "1")
            editor.apply()
            linearViewGif.visibility = View.GONE
        }

        linearBack.setOnClickListener {
            finish()
        }

        shareFav.setOnClickListener {
            try {
                val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
                mViewModel.setFavourite(auth, campaignId, modelSubscription.favourite)
            } catch (e: Exception) {
                e.printStackTrace()
            }


        }

        manage.setOnClickListener {
            if (intent.hasExtra("MySubscriptionsModel")) {
                mySubscriptionModel = intent.getParcelableExtra("MySubscriptionsModel")
            }
            val intent = Intent(this, ManageSubscriptionsActivity::class.java)
            intent.putExtra("model", mySubscriptionModel)
            startActivity(intent)
        }

        shareLike.setOnClickListener {
            val inviteCode = Constant.getArrayListProfile(this, Constant.dataProfile)
            val message =
                "I want to share this awesome experience with you, Sign up  and use redeem code in-app to view your gift. Redeem Code ${inviteCode.invite_code} .https://play.google.com/apps/testing/com.upscribbr"
            val share = Intent(Intent.ACTION_SEND)
            share.type = "text/plain"
            share.putExtra(Intent.EXTRA_TEXT, message)
            startActivity(Intent.createChooser(share, "Share!"))
        }


        cardView7.setOnClickListener {
            startActivity(
                Intent(this, MerchantStoreActivity::class.java).putExtra(
                    "model",
                    modelSubscription.business_id
                )
            )
        }

        tvStaff.setOnClickListener {
            tvInformation.setBackgroundResource(R.drawable.bg_switch_fragments)
            tvStaff.setBackgroundResource(R.drawable.bg_info)
            tvInformation.setTextColor(resources.getColor(R.color.payments))
            tvStaff.setTextColor(resources.getColor(R.color.colorWhite))
            planInformation.visibility = View.GONE
            planStaff.visibility = View.VISIBLE
        }

        reviewViewAll.setOnClickListener {
            val model = intent.getStringExtra("modelSubscriptionView")
            startActivity(
                Intent(this, ReviewActivity::class.java).putExtra("modelSubscriptionView", model)
                    .putExtra("type", "other")
            )

        }

        relativeBoom.setOnClickListener {
            if (intent.hasExtra("MySubscriptionsAdapter")) {
                if (click == 0) {
                    startActivity(
                        Intent(this, BoomActivity::class.java)
                            .putExtra("modelSubscriptionView", modelSubscription)
                            .putExtra("plan", planListData[0])
                            .putExtra("campaignId", campaignId)
                            .putExtra("MySubscriptionsAdapter", "123")
                    )
                }
            } else {
                if (click == 0) {
                    startActivity(
                        Intent(this, BoomActivity::class.java)
                            .putExtra("modelSubscriptionView", modelSubscription)
                            .putExtra("plan", planListData[0])
                            .putExtra("campaignId", campaignId)
                    )
                }
            }
        }

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }

        return super.onOptionsItemSelected(item)
    }

    fun getExtraActivity(v: View) {

        val intent = Intent(this, ExtrasSubscriptionActivity::class.java)
        startActivity(intent)

    }


    class MenuFragment(
        var modelSubscription: ModelCampaignInfo,
        var planListData: PlanModel
    ) : RoundedBottomSheetDialogFragment() {

        @SuppressLint("SetTextI18n")
        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val view = inflater.inflate(R.layout.activity_checkout, container, false)
            val proceedCheckout = view.findViewById<TextView>(R.id.proceedCheckout)
            val cl_addExtra = view.findViewById<ConstraintLayout>(R.id.cl_addExtra)
            val tvName = view.findViewById<TextView>(R.id.tvName)
            val textView140 = view.findViewById<TextView>(R.id.textView140)
            val txtIntroPrice = view.findViewById<TextView>(R.id.txtIntroPrice)
            val introPrice = view.findViewById<TextView>(R.id.introPrice)
            val tax = view.findViewById<TextView>(R.id.tax)
            val subTotal = view.findViewById<TextView>(R.id.subTotal)
            val subTotalPrice = view.findViewById<TextView>(R.id.subTotalPrice)
            val creditApplied = view.findViewById<TextView>(R.id.creditApplied)
            val credits = view.findViewById<TextView>(R.id.credits)
            val tvVisits = view.findViewById<TextView>(R.id.tvVisits)
            val totalPrice = view.findViewById<TextView>(R.id.totalPrice)
            val frequencyType = view.findViewById<TextView>(R.id.frequencyType)
            val discountPrice = view.findViewById<TextView>(R.id.discountPrice)
            val textView14 = view.findViewById<TextView>(R.id.textView14)
            val productPrice = view.findViewById<TextView>(R.id.productPrice)
            val productOffer = view.findViewById<TextView>(R.id.productOffer)
            val textFreeTrail = view.findViewById<TextView>(R.id.textFreeTrail)
            val tvOfferPrice = view.findViewById<TextView>(R.id.tv_last_text)
            val loveDiscountTxt = view.findViewById<TextView>(R.id.loveDiscountTxt)
            val textLoveDiscount = view.findViewById<TextView>(R.id.textLoveDiscount)
            val textViewSubPrice = view.findViewById<TextView>(R.id.textViewSubPrice)
            val txtSubsPrice = view.findViewById<TextView>(R.id.txtSubsPrice)
            val roundedImageView = view.findViewById<ImageView>(R.id.roundedImageView)
            val arrayHomeProfile = Constant.getArrayListProfile(activity!!, Constant.dataProfile)
//            val creditsAvailable = arrayHomeProfile.wallet.toDouble()
            var creditsAvailable = 0.00
            if (arrayHomeProfile.wallet.isNotEmpty()) {
                creditsAvailable = arrayHomeProfile.wallet.toDouble()
            }
            val imagePath = Constant.getPrefs(activity!!).getString(Constant.dataImagePath, "")
//            modelSubscription.boom_discount = "20.0"
            if (modelSubscription.logo.isEmpty()) {
                Glide.with(this).load(R.mipmap.placeholder_subscription_square)
                    .into(roundedImageView)
            } else {
                Glide.with(this).load(imagePath + modelSubscription.feature_image)
                    .into(roundedImageView)
            }

            tvName.text = modelSubscription.name.trim()

            if (planListData.introductory_days == "0" && planListData.free_trial == "0" ||
                planListData.introductory_days == "0" && planListData.free_trial == "No free trial"
            ) {
                textFreeTrail.visibility = View.GONE
            } else if (planListData.introductory_days == "0") {
                textFreeTrail.text = planListData.free_trial + " Free Trial"
            } else if (planListData.introductory_days == "1") {
                var introPriceTextToShow = ""
                if (planListData.introductory_price.contains(".")) {
                    val splitProductPrice = planListData.introductory_price.split(".")
                    if (splitProductPrice[1] == "00" || splitProductPrice[1] == "0") {
                        productPrice.text = splitProductPrice[0]
                    }else{
                        productPrice.text = BigDecimal(planListData.introductory_price).setScale(2, RoundingMode.HALF_UP).toString()
                    }
                    txtSubsPrice.text = BigDecimal(planListData.introductory_price).setScale(2, RoundingMode.HALF_UP).toString()
                    introPriceTextToShow = txtSubsPrice.text.toString()
                } else {
                    productPrice.text = planListData.introductory_price
                    txtSubsPrice.text = planListData.introductory_price
                    introPriceTextToShow = txtSubsPrice.text.toString()
                }
                textFreeTrail.text = "$" + checkIntroPrice(planListData.introductory_price) + " intro price"
            } else {
                textFreeTrail.text = "$" + checkIntroPrice(planListData.introductory_price) + " intro price"
            }

            textFreeTrail.setOnClickListener {
                var days = ""
                when (planListData.introductory_days) {
                    "0" -> {
                        days = planListData.free_trial + " Free Trail"
                    }
                    "1" -> {
                        days = planListData.introductory_days + " Day Introductory Price"
                    }
                    else -> {
                        days = planListData.introductory_days + " Days Introductory Price"
                    }
                }

                Constant.CommonIAlert(
                    context!!,
                    "Your Pricing Details:",
                    "You will get $days"
                )
            }
            val modelSubscriptionData = ModelSubscriptionData()
            var mainPrice = 0.00
            var discountedPrice = 0.0
            modelSubscriptionData.retailPrice = planListData.price
            if (planListData.introductory_days != "0") {
                mainPrice = planListData.introductory_price.toDouble()
                modelSubscriptionData.introDays = planListData.introductory_days
                modelSubscriptionData.introPrice = planListData.introductory_price
                modelSubscriptionData.subscriptionPrice = planListData.introductory_price
                productPrice.text = planListData.introductory_price.toDouble().toInt().toString()
                productOffer.visibility = View.GONE
                tvOfferPrice.visibility = View.GONE
                discountPrice.visibility = View.GONE
                textView14.visibility = View.GONE
                loveDiscountTxt.visibility = View.GONE
                textLoveDiscount.visibility = View.GONE
                textViewSubPrice.visibility = View.GONE
                txtSubsPrice.visibility = View.GONE
                textView140.visibility = View.VISIBLE
                subTotal.visibility = View.VISIBLE
                txtIntroPrice.visibility = View.VISIBLE
                introPrice.visibility = View.VISIBLE
                txtIntroPrice.text = "$" + planListData.introductory_price
                subTotal.text = "$" + planListData.price.toDouble().toString().replace(".0",".00")
            } else {
                txtIntroPrice.visibility = View.GONE
                introPrice.visibility = View.GONE
                textView140.visibility = View.VISIBLE
                subTotal.visibility = View.VISIBLE
                textViewSubPrice.visibility = View.GONE
                txtSubsPrice.visibility = View.GONE
                tvOfferPrice.visibility = View.GONE
                val productPP = Constant.getCalculatedPrice(planListData.discount_price, planListData.price)
                modelSubscriptionData.discountedPrice = productPP
                if (productPP.contains(".")) {
                    val splitProductPrice = productPP.split(".")
                    if (splitProductPrice[1] == "00" || splitProductPrice[1] == "0") {
                        productPrice.text = splitProductPrice[0]
                    }else{
                        productPrice.text =
                            BigDecimal(productPP).setScale(2, RoundingMode.HALF_UP).toString()
                    }
                    txtSubsPrice.text =
                        BigDecimal(productPP).setScale(2, RoundingMode.HALF_UP).toString()
                    modelSubscriptionData.subscriptionPrice = txtSubsPrice.text.toString()

                } else {
                    productPrice.text = productPP
                    txtSubsPrice.text = productPP
                    modelSubscriptionData.subscriptionPrice = txtSubsPrice.text.toString()
                }

                productOffer.text =
                    planListData.discount_price.toDouble().toInt().toString() + "% off"
                modelSubscriptionData.discountPercentage = planListData.discount_price
                tvOfferPrice.text = "$" + planListData.price.toDouble().toInt()

                tvOfferPrice.paintFlags = tvOfferPrice.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
                subTotal.text = "$" + planListData.price.toDouble().toString().replace(".0",".00")
                frequencyType.text = planListData.frequency_type
                tax.text = "To be calculated"

                mainPrice = BigDecimal(planListData.price).setScale(2,RoundingMode.HALF_UP).toDouble()
                if (planListData.discount_price != "0" || planListData.discount_price != "0.00") {
                    mainPrice = BigDecimal(Constant.getCalculatedPrice(
                        planListData.discount_price,
                        mainPrice.toString()
                    )).setScale(2,RoundingMode.HALF_UP).toDouble()
                }
            }

            if (modelSubscription.boom_discount != "0" || modelSubscription.boom_status == "0") {
                textLoveDiscount.text = "Discount - Share 20% off"
                discountedPrice = modelSubscription.boom_discount.toDouble()
                modelSubscriptionData.boomDiscount = modelSubscription.boom_discount
                modelSubscriptionData.boomStatus = modelSubscriptionData.boomStatus
            } else if (modelSubscription.coupon != "0") {
                textLoveDiscount.text = "Referral Discount"
                discountedPrice = 20.0
                modelSubscriptionData.couponDiscount = modelSubscription.coupon
            }


            if (discountedPrice != 0.0) {
                loveDiscountTxt.visibility = View.VISIBLE
                textLoveDiscount.visibility = View.VISIBLE
                val twentyDiscount = (discountedPrice / 100 * mainPrice)
                loveDiscountTxt.text =
                    "-$" + BigDecimal(twentyDiscount).setScale(2, RoundingMode.HALF_UP)
                discountPrice.text = "-$" + BigDecimal(
                    Constant.getDiscountPrice(
                        planListData.discount_price,
                        planListData.price
                    )
                ).setScale(2, RoundingMode.HALF_UP)
//                discountPrice.setTextColor(resources.getColor(R.color.accent))
                modelSubscriptionData.discountedPrice = twentyDiscount.toString()
                textView14.text = "Discount - ${planListData.discount_price.replace(".00", "")
                    .replace(".0", "")}% off"

                val finalPrice = (mainPrice - twentyDiscount).toString()
                modelSubscriptionData.priceAfterDiscount = finalPrice
                modelSubscriptionData.priceBeforeDiscount = mainPrice.toString()
                subTotalPrice.text =
                    "$" + BigDecimal(finalPrice).setScale(2, RoundingMode.HALF_UP)
                if (creditsAvailable == 0.00) {
                    creditApplied.visibility = View.GONE
                    credits.visibility = View.GONE
                    if (finalPrice.contains(".")) {

                            totalPrice.text =
                                "$" + BigDecimal(finalPrice).setScale(2, RoundingMode.HALF_UP)

                    } else {
                        totalPrice.text = "$" + BigDecimal(finalPrice.toDouble()).setScale(
                            2,
                            RoundingMode.HALF_UP
                        )
                    }
                } else {
                    creditApplied.visibility = View.VISIBLE
                    credits.visibility = View.VISIBLE
                    if (creditsAvailable >= finalPrice.toDouble()) {
//                        subTotalPrice.text = "$0.00"
                        totalPrice.text = "$0.00"
                        val creditTobeApplied =
                            BigDecimal(finalPrice.toDouble()).setScale(2, RoundingMode.HALF_UP)
                        creditApplied.text = "-$$creditTobeApplied"
                        modelSubscriptionData.credits =
                            creditApplied.text.toString().replace("$", "").replace("-", "")
//                        creditApplied.setTextColor(resources.getColor(R.color.accent))
                    } else {
                        creditApplied.text = "-$$creditsAvailable"
//                        creditApplied.setTextColor(resources.getColor(R.color.accent))
                        modelSubscriptionData.credits = creditApplied.text.toString()
                        val lastAmount = finalPrice.toDouble() - creditsAvailable
                        if (lastAmount.toString().contains(".")) {
                            val splitPos = lastAmount.toString().split(".")
//                            if (splitPos[1] == "00" || splitPos[1] == "0") {
//                                subTotalPrice.text = "$" + splitPos[0]
//                                totalPrice.text = "$" + splitPos[0]
//                            } else {
                                totalPrice.text =
                                    "$" + BigDecimal(lastAmount).setScale(2, RoundingMode.HALF_UP)
//                            }
                        } else {
                            totalPrice.text =
                                "$" + BigDecimal(lastAmount).setScale(2, RoundingMode.HALF_UP)
                        }
                    }
                }
            } else {
                loveDiscountTxt.visibility = View.GONE
                textLoveDiscount.visibility = View.GONE
                if (planListData.discount_price != "0.00" || planListData.discount_price != "0") {
                    discountPrice.text =
                        "-$" + BigDecimal(
                            Constant.getDiscountPrice(
                                planListData.discount_price,
                                planListData.price
                            )
                        ).setScale(2, RoundingMode.HALF_UP)
//                    discountPrice.setTextColor(resources.getColor(R.color.accent))
                    textView14.text = "Discount - ${planListData.discount_price.replace(".00", "")
                        .replace(".0", "")}% off"
                } else {
                    textView14.visibility = View.GONE
                    discountPrice.visibility = View.GONE
                }
                subTotalPrice.text =
                    "$" + BigDecimal(mainPrice).setScale(2, RoundingMode.HALF_UP)
                if (creditsAvailable == 0.00) {
                    creditApplied.visibility = View.GONE
                    credits.visibility = View.GONE
                    if (mainPrice.toString().contains(".")) {
//                        val splitPos = mainPrice.toString().split(".")
//                        if (splitPos[1] == "00" || splitPos[1] == "0") {
//                            totalPrice.text = "$" + splitPos[0]
//                        } else {
                            totalPrice.text =
                                "$" + BigDecimal(mainPrice).setScale(2, RoundingMode.HALF_UP)
//                        }
                    } else {
                        totalPrice.text =
                            "$" + BigDecimal(mainPrice.toDouble()).setScale(2, RoundingMode.HALF_UP)
                    }
                } else {
                    creditApplied.visibility = View.VISIBLE
                    credits.visibility = View.VISIBLE
                    if (creditsAvailable >= mainPrice) {
//                        subTotalPrice.text = "$0.00"
                        totalPrice.text = "$0.00"
                        val creditTobeApplied =
                            BigDecimal(mainPrice).setScale(2, RoundingMode.HALF_UP)
                        creditApplied.text = "-$$creditTobeApplied"
//                        creditApplied.setTextColor(resources.getColor(R.color.accent))
                        modelSubscriptionData.credits = creditApplied.text.toString()
                    } else {
                        creditApplied.text = "-$$creditsAvailable"
//                        creditApplied.setTextColor(resources.getColor(R.color.accent))
                        modelSubscriptionData.credits = creditApplied.text.toString()
                        val lastAmount = mainPrice.toDouble() - creditsAvailable
                        if (lastAmount.toString().contains(".")) {
                            val splitPos = lastAmount.toString().split(".")
//                            if (splitPos[1] == "00" || splitPos[1] == "0") {
//                                subTotalPrice.text = "$" + splitPos[0]
//                                totalPrice.text = "$" + splitPos[0]
//
//                            } else {
//                                subTotalPrice.text =
//                                    "$" + BigDecimal(lastAmount).setScale(2, RoundingMode.HALF_UP)
                                totalPrice.text =
                                    "$" + BigDecimal(lastAmount).setScale(2, RoundingMode.HALF_UP)
//                            }
                        } else {
//                            subTotalPrice.text =
//                                "$" + BigDecimal(lastAmount).setScale(2, RoundingMode.HALF_UP)
                            totalPrice.text =
                                "$" + BigDecimal(lastAmount).setScale(2, RoundingMode.HALF_UP)
                        }
                    }
                }
            }
            modelSubscriptionData.subTotal = subTotalPrice.text.toString().replace("$", "")
            modelSubscriptionData.mainPrice = totalPrice.text.toString().replace("$", "")
            tvVisits.setOnClickListener {
                var redemQuantity = ""
                redemQuantity = if (planListData.redeemtion_cycle_qty == "500000") {
                    "Unlimited"
                } else {
                    planListData.redeemtion_cycle_qty
                }
                Constant.CommonIAlert(
                    context!!,
                    "Your subscription includes:",
                    redemQuantity + " " + planListData.unit
                )
            }

            frequencyType.setOnClickListener {
                Constant.CommonIAlert(
                    context!!,
                    "Subscription is Billed " + planListData.frequency_type + " from the purchase date",
                    ""
                )
            }


            cl_addExtra.setOnClickListener {
                startActivity(Intent(this.activity!!, ExtrasSubscriptionActivity::class.java))
                onResume()
            }

            proceedCheckout.setOnClickListener {
                startActivity(
                    Intent(this.activity!!, checkoutOne::class.java).putExtra(
                        "allData", planListData
                    )
                        .putExtra("modelSubscription", modelSubscription)
                        .putExtra(
                            "totalPrice",
                            totalPrice.text.toString()
                        ).putExtra("subscriptionData", modelSubscriptionData)
                )
            }


            return view
        }

        fun checkIntroPrice(introPrice: String): String {
            var introPriceTextToShow = ""
            if (introPrice.contains(".")) {
//                val splitProductPrice = introPrice.split(".")
//                if (splitProductPrice[1] == "00" || splitProductPrice[1] == "0") {
//                    introPriceTextToShow = splitProductPrice[0]
//                } else {
                    introPriceTextToShow =
                        BigDecimal(introPrice).setScale(2, RoundingMode.HALF_UP).toString()
//                }
            } else {
                introPriceTextToShow = introPrice
            }
            return introPriceTextToShow
        }


        override fun onResume() {
            super.onResume()
            val mPrefs = context!!.getSharedPreferences("ProductArray", MODE_PRIVATE)

            val totalAmounts = mPrefs.getString("addTotal", "")
            if (!totalAmounts.isEmpty()) {

                textViewExtraa.visibility = View.GONE
                cl_total.visibility = View.VISIBLE
                tvTotal.text = totalAmounts
                val editor = mPrefs.edit()
                editor.remove("addTotal")
                editor.apply()
            } else {
                textViewExtraa.visibility = View.VISIBLE
                cl_total.visibility = View.GONE
            }
        }
    }


    fun viewAllSubscription(v: View) {
        val model = intent.getStringExtra("modelSubscriptionView")
        val intent = Intent(this, DiscoverActivity::class.java)
            .putExtra("modelSubscriptionView", model)
        startActivity(intent)
    }

    override fun showBuyButton(
        model: PlanModel,
        position: Int
    ) {
        planModel = model
        val modelHome = Constant.getArrayListProfile(this, Constant.dataProfile)
        if (modelSubscription.business_id == modelHome.business_id) {
            btn_buy.visibility = View.GONE
            subscribeBtn = 0
        } else {
            val model1 = planListData[position]
            if (model1.isSelected) {
                btn_buy.visibility = View.GONE
                subscribeBtn = 0
                model.isSelected = false
            } else {
                for (child in planListData) {
                    child.isSelected = false
                }
                btn_buy.visibility = View.VISIBLE
                subscribeBtn = 1
                model.isSelected = true
                showDialog()

            }
            planListData[position] = model
            mAdapterPlan.notifyDataSetChanged()

        }
    }
}
