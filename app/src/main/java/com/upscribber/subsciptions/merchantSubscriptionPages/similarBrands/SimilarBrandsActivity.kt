package com.upscribber.subsciptions.merchantSubscriptionPages.similarBrands

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.commonClasses.Constant
import com.upscribber.R

class SimilarBrandsActivity : AppCompatActivity() {

    lateinit var toolbar7: Toolbar
    lateinit var simialId: TextView
    private lateinit var recyclerSimilarBrands: RecyclerView
    lateinit var mViewModel: SimilarBrandsViewModel
    lateinit var mAdapterSimilar: SimilarBrandsAdapter
    lateinit var progressBar8: ProgressBar


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_similar_subscription)
        mViewModel = ViewModelProviders.of(this)[SimilarBrandsViewModel::class.java]
        init()
        setToolbar()
        apiImplimentation()
        observersInit()
        setAdapter()


    }

    private fun observersInit() {
        mViewModel.getStatus().observe(this, Observer {
            if (it.msg.isNotEmpty()) {
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
            }

        })

        mViewModel.getmDataAllBrands().observe(this, Observer {
            if (it.size > 0){
                progressBar8.visibility = View.GONE
                mAdapterSimilar.update(it)
            }
        })
    }

    private fun apiImplimentation() {
        val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
        val modelBusinessId = intent.getStringExtra("model")
        mViewModel.getListSimilarBrands(auth,modelBusinessId,"1","1")
        progressBar8.visibility = View.VISIBLE

    }

    private fun setAdapter() {
        recyclerSimilarBrands.layoutManager = LinearLayoutManager(this)
        mAdapterSimilar = SimilarBrandsAdapter(this)
        recyclerSimilarBrands.adapter = mAdapterSimilar
    }

    private fun setToolbar() {
        setSupportActionBar(toolbar7)
        title = ""
        simialId.text = "Similar Brands"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)

    }

    private fun init() {
        toolbar7 = findViewById(R.id.toolbar7)
        simialId = findViewById(R.id.simialId)
        recyclerSimilarBrands = findViewById(R.id.recyclerSimilarBrands)
        progressBar8 = findViewById(R.id.progressBar8)

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}
