package com.upscribber.subsciptions.merchantSubscriptionPages.similarSubscriptions

import android.os.Parcel
import android.os.Parcelable


class SimilarSubsciptionsModel() : Parcelable{
    var business_name: String = ""
    var campaign_id: String = ""
    var name: String = ""
    var feature_image: String = ""

    constructor(parcel: Parcel) : this() {
        business_name = parcel.readString()
        campaign_id = parcel.readString()
        name = parcel.readString()
        feature_image = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(business_name)
        parcel.writeString(campaign_id)
        parcel.writeString(name)
        parcel.writeString(feature_image)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SimilarSubsciptionsModel> {
        override fun createFromParcel(parcel: Parcel): SimilarSubsciptionsModel {
            return SimilarSubsciptionsModel(parcel)
        }

        override fun newArray(size: Int): Array<SimilarSubsciptionsModel?> {
            return arrayOfNulls(size)
        }
    }
}