package com.upscribber.subsciptions.merchantSubscriptionPages

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import com.upscribber.R
import com.upscribber.upscribberSeller.subsriptionSeller.subscriptionView.AdapterSubscriberList
import com.upscribber.upscribberSeller.subsriptionSeller.subscriptionView.ModelCampaignSubscriber
import kotlinx.android.synthetic.main.activity_sub_info_list.*

class SubInfoList : AppCompatActivity() {
    private lateinit var toolbar: Toolbar
    lateinit var mAdapterSubscriber: AdapterSubscriberList
    val layout = LinearLayoutManager(this)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sub_info_list)
        toolbar = findViewById(R.id.include99)
        setToolbar()
        if(intent.hasExtra("list")){
            val list = intent.getParcelableArrayListExtra<ModelCampaignSubscriber>("list")
            mAdapterSubscriber = AdapterSubscriberList(this,list)
            subInfo.layoutManager = layout
            subInfo.adapter = mAdapterSubscriber
            mAdapterSubscriber.update(list)
        }
    }

    private fun setToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(com.upscribber.R.drawable.ic_arrow_back_black_24dp)
        val title : TextView = toolbar.findViewById(R.id.title)
        title.text = "Subscribers"

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        if (item!!.itemId == android.R.id.home) {
            onBackPressed()
        }

        return super.onOptionsItemSelected(item)
    }
}
