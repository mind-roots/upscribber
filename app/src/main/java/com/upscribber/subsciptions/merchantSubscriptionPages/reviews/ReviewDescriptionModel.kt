package com.upscribber.subsciptions.merchantSubscriptionPages.reviews


class ReviewDescriptionModel {

    var memberImage : Int = 0
    var gestureImage : Int = 0
    lateinit var userName : String
    lateinit var dateTime : String
    lateinit var review : String

}