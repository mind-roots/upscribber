package com.upscribber.subsciptions.merchantSubscriptionPages.similarBrands

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.upscribber.subsciptions.merchantStore.merchantDetails.MerchantStoreActivity
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import kotlinx.android.synthetic.main.mechant_similar_brands.view.*

class SimilarBrandsAdapter(
    private val mContext: Context) :
    RecyclerView.Adapter<SimilarBrandsAdapter.MyViewHolder>() {

    private var list: ArrayList<SimilarBrandsModel> = ArrayList()

    override fun getItemCount(): Int {
        return list.size

    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = list[position]
        val imagePath  = Constant.getPrefs(mContext).getString(Constant.dataImagePath, "")
        Glide.with(mContext).load(imagePath + model.cover_image).placeholder(R.mipmap.app_icon).into(holder.itemView.image)

        holder.itemView.tVname.text = model.business_name
        holder.itemView.tVsubscriptions.text = model.total_subscriptions + " subscriptions"
//        holder.itemView.tVAddress.text = model.address
        holder.itemView.textLikes.text = model.likes
        holder.itemView.tvBought.text = model.bought
        holder.itemView.tvViews.text = model.views

        holder.itemView.setOnClickListener {
            mContext.startActivity(
                Intent(mContext,
                MerchantStoreActivity::class.java).putExtra("model",model.business_id)
            )

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val v = LayoutInflater.from(mContext)
            .inflate(
                R.layout.mechant_similar_brands
                , parent, false
            )
        return MyViewHolder(
            v
        )
    }

    fun update(it: java.util.ArrayList<SimilarBrandsModel>?) {
        list = it!!
        notifyDataSetChanged()

    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

}