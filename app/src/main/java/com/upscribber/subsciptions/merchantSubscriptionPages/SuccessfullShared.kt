package com.upscribber.subsciptions.merchantSubscriptionPages

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.loginSignUpScreen.LoginorSignUpActivity
import kotlinx.android.synthetic.main.activity_successfull_shared.*

class SuccessfullShared : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_successfull_shared)
        setData()
        clickListener()
    }

    private fun setData() {
        when {
            intent.hasExtra("email") -> {
                sharedText.text =
                    "Verification link has been sent to your provided email. Please click on the link to verify your email address."
                textView204.text = "Email Sent"
                doneBtn.setBackgroundResource(R.drawable.bg_seller_button_sea_green)
            }
            intent.hasExtra("deleteAccount") -> {
                sharedText.text =
                    "Your upscribbr account is now deleted. You can however create a new account."
                textView204.text = "Account Deleted"
                doneBtn.setBackgroundResource(R.drawable.invite_button_background)

            }
            intent.hasExtra("feedback") -> {
                sharedText.text = "Thank you for completing the survey.We really appreciate the time you have taken out to give us your precious feedback."
                textView204.text = "Thank You!"
                doneBtn.setBackgroundResource(R.drawable.bg_seller_button_sea_green)

            }
            else -> {
                val buyValue = intent.getStringExtra("buyValue")
                if (buyValue == "1") {
                    sharedText.text =
                        "Thank you for sharing! We'll apply 20% off to your next billing cycle."
                    textView204.text = "Successfully Shared!"
                } else {
                    sharedText.text =
                        "You have shared the subscription. Now avail extra discount when purchasing it."
                    textView204.text = "Successfully Shared!"
                }
                doneBtn.setBackgroundResource(R.drawable.bg_seller_button_sea_green)

            }
        }
    }

    private fun clickListener() {
        doneBtn.setOnClickListener {
            if (intent.hasExtra("deleteAccount")) {
                Constant.getPrefs(this).edit().clear().apply()
                Constant.getSharedPrefs(this).edit().clear().apply()
                Constant.getPrefs1(this).edit().clear().apply()
                val intent = Intent(this, LoginorSignUpActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                finish()
            } else {
                finish()
            }

        }

    }
}
