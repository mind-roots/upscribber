package com.upscribber.subsciptions.merchantSubscriptionPages.similarBrands

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.upscribber.commonClasses.ModelStatusMsg
import com.upscribber.discover.DiscoverSecondModel

class SimilarBrandsViewModel(application: Application) : AndroidViewModel(application) {

    var repositorySimilarBrands: SimilarBrandsRepository = SimilarBrandsRepository(application)

    fun getListSimilarSubscription(
        auth: String,
        modelBusinessId: String,
        page: String,
        type: String
    ) {
        repositorySimilarBrands.getListSimilarSubscription(auth, modelBusinessId, page, type)

    }

    fun getListSimilarBrands(
        auth: String,
        modelBusinessId: String,
        page: String,
        type: String
    ) {
        repositorySimilarBrands.getListSimilarBrands(auth, modelBusinessId, page, type)

    }

    fun getStatus(): LiveData<ModelStatusMsg> {
        return repositorySimilarBrands.getmDataFailure()
    }

    fun getmDataAllBrands(): LiveData<ArrayList<SimilarBrandsModel>> {
        return repositorySimilarBrands.getmDataAllBrands()
    }

    fun getmDataAllSubscription(): LiveData<ArrayList<DiscoverSecondModel>> {
        return repositorySimilarBrands.getmDataAllSubscription()
    }

    fun setFavourite(auth: String, discoverSecondModel: DiscoverSecondModel) {
        repositorySimilarBrands.getFavourite(auth,discoverSecondModel)

    }

    fun getmDataFav(): LiveData<DiscoverSecondModel> {
        return repositorySimilarBrands.getmDataFav()
    }

}