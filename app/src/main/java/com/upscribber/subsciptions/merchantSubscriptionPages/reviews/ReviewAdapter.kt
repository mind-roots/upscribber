package com.upscribber.subsciptions.merchantSubscriptionPages.reviews

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.subsciptions.merchantStore.ModelReviews
import kotlinx.android.synthetic.main.layout_reviews_list.view.*

class ReviewAdapter(
    val mContext: Context,
    val list: ArrayList<ReviewModel>,
    val modelReviews: ArrayList<ModelReviews>
) : RecyclerView.Adapter<ReviewAdapter.MyViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(mContext)
            .inflate(
                R.layout.layout_reviews_list
                , parent, false
            )
        return MyViewHolder(v)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = list[position]
        val imagePath = Constant.getPrefs(mContext).getString(Constant.dataImagePath, "")
        Glide.with(mContext).load(model.imageCategory).into(holder.itemView.imageView)
        try {
            holder.itemView.noOfReview.text = modelReviews[position].count
            holder.itemView.categoryReview.text = modelReviews[position].name
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}