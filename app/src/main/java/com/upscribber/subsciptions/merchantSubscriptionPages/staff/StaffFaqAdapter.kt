package com.upscribber.subsciptions.merchantSubscriptionPages.staff

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.R
import kotlinx.android.synthetic.main.faq_recycler_layout.view.*

class StaffFaqAdapter(
    val mContext: Context,
    val list: ArrayList<StaffFaqModel>
) : RecyclerView.Adapter<StaffFaqAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(mContext)
            .inflate(
                R.layout.faq_recycler_layout
                , parent, false
            )
        return MyViewHolder(v)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = list[position]

        holder.itemView.tvQuestion.text = model.textQuestion
        holder.itemView.tvAnswer.text = model.textAnswer
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }

}
