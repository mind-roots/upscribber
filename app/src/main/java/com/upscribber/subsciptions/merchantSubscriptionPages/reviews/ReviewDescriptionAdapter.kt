package com.upscribber.subsciptions.merchantSubscriptionPages.reviews

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.subsciptions.merchantStore.ModelReviewDescription
import kotlinx.android.synthetic.main.layout_reviews_description.view.*

class ReviewDescriptionAdapter
    (private val mContext: Context,
private val list: ArrayList<ModelReviewDescription>) :
RecyclerView.Adapter<ReviewDescriptionAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(mContext)
            .inflate(
                R.layout.layout_reviews_description
                , parent, false
            )
        return MyViewHolder(v)

    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val reviewModel = list[position]


        val imagePath  = Constant.getPrefs(mContext).getString(Constant.dataImagePath, "")
        Glide.with(mContext).load(imagePath + reviewModel.profile_image).into(holder.itemView.imageView)

        if (reviewModel.type == "1"){
            holder.itemView.imageViewGesture.setImageResource(R.drawable.ic_like)
        }else{
            holder.itemView.imageViewGesture.setImageResource(R.drawable.ic_dislike)
        }

        holder.itemView.memberNameReview.text= reviewModel.name
        holder.itemView.tVDateTime.text= reviewModel.updated_at
        holder.itemView.reviewText.text= reviewModel.review
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}