package com.upscribber.subsciptions.merchantSubscriptionPages

import android.os.Parcel
import android.os.Parcelable

class ModelCampaignInfo() : Parcelable {
    var schedule: String = ""
    var msg: String = ""
    var status: String = ""
    var default_location: String = ""
    var remaining_subscriber_limit: String = ""
    var business_about: String = ""
    var cancel_discount: String = ""
    var coupon: String = "0"
    var about: String = ""
    var products: String = ""
    var is_discounted: String = ""
    var location_id: String = ""
    var campaign_start: String = ""
    var team_member: String = ""
    var description: String = ""
    var feature_image: String = ""
    var favourite: Int = 0
    var name: String = ""
    var business_id: String = ""
    var company_name: String = ""
    var logo: String = ""
    var likes: String = ""
    var views: String = ""
    var bought: String = ""
    var boom_status: String = ""
    var boom_discount: String = ""
    var campaign_end: String = ""
    var discountedAmount  = ""

    constructor(parcel: Parcel) : this() {
        schedule = parcel.readString()
        msg = parcel.readString()
        status = parcel.readString()
        default_location = parcel.readString()
        remaining_subscriber_limit = parcel.readString()
        business_about = parcel.readString()
        cancel_discount = parcel.readString()
        coupon = parcel.readString()
        about = parcel.readString()
        products = parcel.readString()
        is_discounted = parcel.readString()
        location_id = parcel.readString()
        campaign_start = parcel.readString()
        team_member = parcel.readString()
        description = parcel.readString()
        feature_image = parcel.readString()
        favourite = parcel.readInt()
        name = parcel.readString()
        business_id = parcel.readString()
        company_name = parcel.readString()
        logo = parcel.readString()
        likes = parcel.readString()
        views = parcel.readString()
        bought = parcel.readString()
        boom_status = parcel.readString()
        boom_discount = parcel.readString()
        campaign_end = parcel.readString()
        discountedAmount = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(schedule)
        parcel.writeString(msg)
        parcel.writeString(status)
        parcel.writeString(default_location)
        parcel.writeString(remaining_subscriber_limit)
        parcel.writeString(business_about)
        parcel.writeString(cancel_discount)
        parcel.writeString(coupon)
        parcel.writeString(about)
        parcel.writeString(products)
        parcel.writeString(is_discounted)
        parcel.writeString(location_id)
        parcel.writeString(campaign_start)
        parcel.writeString(team_member)
        parcel.writeString(description)
        parcel.writeString(feature_image)
        parcel.writeInt(favourite)
        parcel.writeString(name)
        parcel.writeString(business_id)
        parcel.writeString(company_name)
        parcel.writeString(logo)
        parcel.writeString(likes)
        parcel.writeString(views)
        parcel.writeString(bought)
        parcel.writeString(boom_status)
        parcel.writeString(boom_discount)
        parcel.writeString(campaign_end)
        parcel.writeString(discountedAmount)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ModelCampaignInfo> {
        override fun createFromParcel(parcel: Parcel): ModelCampaignInfo {
            return ModelCampaignInfo(parcel)
        }

        override fun newArray(size: Int): Array<ModelCampaignInfo?> {
            return arrayOfNulls(size)
        }
    }


}
