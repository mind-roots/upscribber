package com.upscribber.subsciptions.merchantSubscriptionPages.similarBrands

class SimilarBrandsModel {

    var cover_image: String = ""
    var business_name: String = ""
    var business_id: String = ""
    var likes: String = ""
    var views: String = ""
    var bought: String = ""
    var total_subscriptions: String = ""
    var status: String = ""
    var msg : String = ""

}