package com.upscribber.subsciptions.merchantSubscriptionPages.plan

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by Manisha Thakur on 20/3/19.
 */
class PlanModel() : Parcelable{
    var redeem_count: String = ""
    var subscription_id: String = ""
    var campaign_id: String = ""
    var redeemtion_cycle_qty: String = ""
    var introductory_days: String = ""
    var introductory_price: String = ""
    var frequency_value: String = ""
    var views: String = ""
    var bought: String = ""
    var created_at: String = ""
    var updated_at: String = ""
    var likes: String = ""
    var unit: String = ""
    var price: String = ""
    var discount_price: String = ""
    var frequency_type: String = ""
    var free_trial: String = ""
    var free_trial_qty: String = ""
    var isSelected: Boolean = false
    var imageRight: Int = 0

    constructor(parcel: Parcel) : this() {
        redeem_count = parcel.readString()
        subscription_id = parcel.readString()
        campaign_id = parcel.readString()
        redeemtion_cycle_qty = parcel.readString()
        introductory_days = parcel.readString()
        introductory_price = parcel.readString()
        frequency_value = parcel.readString()
        views = parcel.readString()
        bought = parcel.readString()
        created_at = parcel.readString()
        updated_at = parcel.readString()
        likes = parcel.readString()
        unit = parcel.readString()
        price = parcel.readString()
        discount_price = parcel.readString()
        frequency_type = parcel.readString()
        free_trial = parcel.readString()
        free_trial_qty = parcel.readString()
        isSelected = parcel.readByte() != 0.toByte()
        imageRight = parcel.readInt()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(redeem_count)
        parcel.writeString(subscription_id)
        parcel.writeString(campaign_id)
        parcel.writeString(redeemtion_cycle_qty)
        parcel.writeString(introductory_days)
        parcel.writeString(introductory_price)
        parcel.writeString(frequency_value)
        parcel.writeString(views)
        parcel.writeString(bought)
        parcel.writeString(created_at)
        parcel.writeString(updated_at)
        parcel.writeString(likes)
        parcel.writeString(unit)
        parcel.writeString(price)
        parcel.writeString(discount_price)
        parcel.writeString(frequency_type)
        parcel.writeString(free_trial)
        parcel.writeString(free_trial_qty)
        parcel.writeByte(if (isSelected) 1 else 0)
        parcel.writeInt(imageRight)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PlanModel> {
        override fun createFromParcel(parcel: Parcel): PlanModel {
            return PlanModel(parcel)
        }

        override fun newArray(size: Int): Array<PlanModel?> {
            return arrayOfNulls(size)
        }
    }


}
