package com.upscribber.subsciptions.merchantSubscriptionPages.plan

import android.content.Context
import android.graphics.Color
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.upscribber.commonClasses.Constant
import com.upscribber.R
import kotlinx.android.synthetic.main.choose_plan.view.*
import java.math.BigDecimal
import java.math.RoundingMode

class PlanAdapter(
    private val mContext: Context

) :
    RecyclerView.Adapter<PlanAdapter.MyViewHolder>() {

    var list: ArrayList<PlanModel> = ArrayList()
    var show = mContext as ShowBuyButton


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val v = LayoutInflater.from(mContext)
            .inflate(
                R.layout.choose_plan
                , parent, false
            )
        return MyViewHolder(v)
    }

    override fun getItemCount(): Int {

        return list.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = list[position]

        holder.itemView.annual.text = model.frequency_type + " Subscription"
        holder.itemView.tv_off.text = model.discount_price.toDouble().toInt().toString() + "% off"
        holder.itemView.tv_last_text.paintFlags =
            holder.itemView.tv_last_text.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
//        Glide.with(mContext).load(R.drawable.tap).into(holder.itemView.gifImg)

        val cost = Constant.getCalculatedPrice(model.discount_price, model.price)
        val splitPos = cost.split(".")
        if (splitPos[1] == "00" || splitPos[1] == "0") {
            holder.itemView.tv_cost.text = "$" + splitPos[0]
        } else {
            holder.itemView.tv_cost.text = "$" + BigDecimal(cost).setScale(2, RoundingMode.HALF_UP)
        }
        val actualCost = model.price.toDouble().toString()
        val splitActualCost = actualCost.split(".")
        if (splitActualCost[1] == "00" || splitActualCost[1] == "0") {
            holder.itemView.tv_last_text.text = "$" + splitActualCost[0]
        } else {
            holder.itemView.tv_last_text.text = "$" + actualCost.toInt()
        }


        var frequency = ""
        frequency = when (model.frequency_type) {
            "Monthly" -> {
                "Month"
            }
            "Yearly" -> {
                "Year"
            }
            "1" -> {
                "Month"
            }
            "2" -> {
                "Year"
            }
            else -> {
                "Week"
            }
        }


        var introPriced = ""
        if (model.introductory_price.contains(".")) {
            val splitPoss = model.introductory_price.split(".")
            if (splitPoss[1] == "00" || splitPoss[1] == "0") {
                introPriced = "$" + splitPoss[0]
            } else {
                introPriced =
                    "$" + BigDecimal(model.introductory_price).setScale(2, RoundingMode.HALF_UP)
            }
        } else {
            introPriced = model.introductory_price
        }



        if (model.introductory_days == "0" && model.free_trial == "0" || model.introductory_days == "0.00" && model.free_trial == "0" ||
            model.introductory_days == "0" && model.free_trial == "No free trial" || model.introductory_days == "0.00" && model.free_trial == "No free trial"
        ) {
            holder.itemView.daysPlan.visibility = View.GONE
        } else if (model.introductory_days == "0" || model.introductory_days == "0.00") {
            holder.itemView.daysPlan.visibility = View.VISIBLE
            holder.itemView.daysPlan.text = "Try this plan " + model.free_trial + " for free!"
        } else if (model.introductory_days == "1") {
            holder.itemView.daysPlan.visibility = View.VISIBLE
            holder.itemView.daysPlan.text =
                "Introductory price of " + introPriced + " for " + model.introductory_days + " " + frequency + "!"
        } else {
            holder.itemView.daysPlan.visibility = View.VISIBLE
            holder.itemView.daysPlan.text =
                "Introductory price of " + introPriced + " for " + model.introductory_days + " " + frequency + "s!"
        }


        when (model.redeemtion_cycle_qty) {
            "500000" -> {
                holder.itemView.upto.text = "Unlimited " + model.unit + " per " + frequency
            }
            "1" -> {
                holder.itemView.upto.text = "1 " + model.unit + " Build " + frequency
            }
            else -> {
                holder.itemView.upto.text =
                    model.redeemtion_cycle_qty + "X " + model.unit + " per " + frequency
            }
        }

        if (model.isSelected) {
            holder.itemView.cl_plan.setBackgroundResource(R.drawable.signup_invite_code)
            holder.itemView.check.visibility = View.VISIBLE
            holder.itemView.annual.setTextColor(Color.parseColor("#0c0d2c"))
            holder.itemView.upto.setTextColor(Color.parseColor("#0c0d2c"))
            holder.itemView.daysPlan.setTextColor(Color.parseColor("#0c0d2c"))
            holder.itemView.tv_cost.setTextColor(Color.parseColor("#0c0d2c"))
            holder.itemView.tv_off.setTextColor(Color.parseColor("#ff3f55"))
            holder.itemView.tv_off.setBackgroundResource(R.drawable.favorite_off)
            holder.itemView.tv_last_text.setTextColor(Color.parseColor("#ff3f55"))
            holder.itemView.card.cardElevation = 0f
        } else {
            holder.itemView.cl_plan.setBackgroundResource(0)
            holder.itemView.card.cardElevation = 10f
            holder.itemView.check.visibility = View.GONE
            holder.itemView.annual.setTextColor(Color.parseColor("#8f909e"))
            holder.itemView.upto.setTextColor(Color.parseColor("#8f909e"))
            holder.itemView.daysPlan.setTextColor(Color.parseColor("#8f909e"))
            holder.itemView.tv_cost.setTextColor(Color.parseColor("#8f909e"))
            holder.itemView.tv_off.setTextColor(Color.parseColor("#aab5bd"))
            holder.itemView.tv_off.setBackgroundResource(R.drawable.bg_off_non_selected)
            holder.itemView.tv_last_text.setTextColor(Color.parseColor("#aab5bd"))
        }

        holder.itemView.setOnClickListener {
            show.showBuyButton(model, position)
        }
    }

    fun update(it: java.util.ArrayList<PlanModel>?) {
        list = it as ArrayList<PlanModel>
        notifyDataSetChanged()

    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    interface ShowBuyButton {
        fun showBuyButton(
            model: PlanModel,
            position: Int
        )
    }

}
