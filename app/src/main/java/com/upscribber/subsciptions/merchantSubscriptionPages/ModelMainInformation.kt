package com.upscribber.subsciptions.merchantSubscriptionPages

import com.upscribber.subsciptions.merchantStore.ModelReviewDescription
import com.upscribber.subsciptions.merchantStore.ModelReviews
import com.upscribber.subsciptions.merchantStore.merchantDetails.ModeTags
import com.upscribber.subsciptions.merchantStore.merchantDetails.ModelBrandContact

class ModelMainInformation {

    var modelBrandContact : ModelBrandContact = ModelBrandContact()
    var reviewsmodel : ArrayList<ModelReviews> = ArrayList()
    var modelLatestreviewsData : ArrayList<ModelReviewDescription> = ArrayList()
    var modelTags : ArrayList<ModeTags> = ArrayList()
    var modelTagsNew : ArrayList<ModeTags> = ArrayList()
    var parentTags : ArrayList<ModeTags> = ArrayList()
    var about : String =""
    var schedule : String =""


}
