package com.upscribber.subsciptions.merchantSubscriptionPages.staff

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.R
import kotlinx.android.synthetic.main.layout_staff_list.view.*

class StaffAdapter(
    private val mContext: Context,
    private val list: ArrayList<StaffModel>
) :
    RecyclerView.Adapter<StaffAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val v = LayoutInflater.from(mContext)
            .inflate(
                R.layout.layout_staff_list
                , parent, false
            )
        return MyViewHolder(v)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = list[position]

        holder.itemView.imageView.setImageResource(model.imageStaff)
        holder.itemView.memberName.text= model.nameStaffMember
        holder.itemView.speciality.text= model.speciality

        holder.itemView.setOnClickListener {
//            Constant.commonStaffAlert(mContext,model.image,model.name,model.speciality)
        }

    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}