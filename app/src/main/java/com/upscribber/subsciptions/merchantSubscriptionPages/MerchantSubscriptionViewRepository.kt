package com.upscribber.subsciptions.merchantSubscriptionPages

import android.app.Application
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.common.reflect.TypeToken
import com.google.gson.Gson
import com.upscribber.commonClasses.Constant
import com.upscribber.commonClasses.ModelStatusMsg
import com.upscribber.commonClasses.WebServicesCustomers
import com.upscribber.home.ModelHomeDataSubscriptions
import com.upscribber.subsciptions.merchantStore.ModelReviewDescription
import com.upscribber.subsciptions.merchantStore.ModelReviews
import com.upscribber.subsciptions.merchantStore.ModelStaffList
import com.upscribber.subsciptions.merchantStore.merchantDetails.ModeTags
import com.upscribber.subsciptions.merchantStore.merchantDetails.ModelBrandContact
import com.upscribber.subsciptions.merchantSubscriptionPages.plan.PlanModel
import com.upscribber.subsciptions.merchantSubscriptionPages.similarSubscriptions.SimilarSubsciptionsModel
import com.upscribber.subsciptions.mySubscriptionDetails.MySubscriptionsModel
import com.upscribber.upscribberSeller.subsriptionSeller.location.searchLoc.ModelSearchLocation
import com.upscribber.upscribberSeller.subsriptionSeller.location.searchLoc.ModelStoreLocation
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class MerchantSubscriptionViewRepository(var application: Application) {

    private val mDataInformation = MutableLiveData<ModelMainInformation>()
    private val mDataSimilarSubscription = MutableLiveData<ArrayList<SimilarSubsciptionsModel>>()
    private val mDataStaff = MutableLiveData<ArrayList<ModelStaffList>>()
    private val mDataPlanArray = MutableLiveData<ArrayList<PlanModel>>()
    private val mDataSubArray = MutableLiveData<ArrayList<MySubscriptionsModel>>()
    private val mDataLocation = MutableLiveData<ArrayList<ModelSearchLocation>>()
    private val mDataImagesArrays = MutableLiveData<ArrayList<ModelImagesSubscriptionDetails>>()
    private val mDataMain = MutableLiveData<ModelSubscriptionDetailsMain>()
    val mDataFailure = MutableLiveData<ModelStatusMsg>()
    val mDataBoom = MutableLiveData<ModelStatusMsg>()
    val mbuyCampaignRequest = MutableLiveData<ModelStatusMsg>()
    private val mDataCampaignInfo = MutableLiveData<ModelCampaignInfo>()
    val mDataSubscription = MutableLiveData<ModelCampaignInfo>()

    fun getDetailsSubscriptions(auth: String, model: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.subscriptionDetails(auth, model)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")

                        //-------------------------Models---------------------------//
                        val modelCampaignInfo = ModelCampaignInfo()
                        val modelStatus = ModelStatusMsg()
                        val modelMain = ModelSubscriptionDetailsMain()
                        val modelContact = ModelBrandContact()
                        val modelMainInformation = ModelMainInformation()


                        //---------------------Arrays-----------------------------------//
                        val imagesArray = ArrayList<ModelImagesSubscriptionDetails>()
                        val arrayPlan = ArrayList<PlanModel>()
                        val arraySubs = ArrayList<MySubscriptionsModel>()
                        val arrayReviews = ArrayList<ModelReviews>()
                        val arrayReviewsDescription = ArrayList<ModelReviewDescription>()
                        val arrayTags = ArrayList<ModeTags>()
                        val arrayTagsNew = ArrayList<ModeTags>()
                        val arrayParentTagsNew = ArrayList<ModeTags>()
                        val arrayStaff = ArrayList<ModelStaffList>()
                        val similarTagsArray = ArrayList<String>()
                        val arraysimilarSubscription = ArrayList<SimilarSubsciptionsModel>()
                        val arrayLocation = ArrayList<ModelSearchLocation>()

                        if (status == "true") {
                            val data = json.getJSONObject("data")
                            //--------------- Get data from Object data----------//
                            val campaignInfo = data.getJSONObject("campaign_info")
                            val plan = data.getJSONArray("subscription")
                            val locations = data.getJSONArray("locations")
                            val information = data.getJSONObject("information")
                            val staff = data.getJSONArray("staff")
                            val similar_subscriptions = data.getJSONArray("similar_subscriptions")
                            val bought_subscription = data.optJSONArray("bought_subscription")

                            //------------ Getting Data for Similar subscriptions-----------------//
                            for (i in 0 until similar_subscriptions.length()) {
                                val similarSubsciptionsModel = SimilarSubsciptionsModel()
                                val similarSubscriptionData = similar_subscriptions.optJSONObject(i)
                                similarSubsciptionsModel.feature_image =
                                    similarSubscriptionData.optString("feature_image")
                                similarSubsciptionsModel.name =
                                    similarSubscriptionData.optString("name")
                                similarSubsciptionsModel.campaign_id =
                                    similarSubscriptionData.optString("campaign_id")
                                similarSubsciptionsModel.business_name =
                                    similarSubscriptionData.optString("business_name")
                                val tagss = similarSubscriptionData.optJSONArray("tags")
                                for (j in 0 until tagss.length()) {
                                    similarTagsArray.add(tagss[j] as String)

                                }
                                arraysimilarSubscription.add(similarSubsciptionsModel)
                            }

                            //------------------- Getting Data from Information-----------------------//
                            //------------For Contact Details ------------------//

                            val contact = information.getJSONObject("contact")
                            modelContact.website = contact.optString("website")
                            modelContact.call = contact.optString("call")
                            modelContact.lat = contact.optString("lat")
                            modelContact.long = contact.optString("long")
                            modelMainInformation.modelBrandContact = modelContact
                            //------------ For About Data -------------------------------//
                            modelMainInformation.about = information.optString("about")
                            modelMainInformation.schedule = information.optString("schedule")

                            //-----------------For Reviews Data-------------------//
                            val reviews = information.getJSONArray("reviews")
                            for (i in 0 until reviews.length()) {
                                val reviewsmodel = ModelReviews()
                                val reviewsData = reviews.optJSONObject(i)
                                reviewsmodel.name = reviewsData.optString("name")
                                reviewsmodel.count = reviewsData.optString("count")
                                reviewsmodel.id = reviewsData.optString("id")
                                arrayReviews.add(reviewsmodel)
                            }
                            modelMainInformation.reviewsmodel = arrayReviews
                            //------------------------ Getting Reviews Description Data----------//
                            val latestReviews = information.getJSONArray("latest_reviews")
                            for (i in 0 until latestReviews.length()) {
                                val modelLatestreviewsData = ModelReviewDescription()
                                val latestreviewsData = latestReviews.optJSONObject(i)
                                modelLatestreviewsData.review =
                                    latestreviewsData.optString("review")
                                modelLatestreviewsData.type = latestreviewsData.optString("type")
                                modelLatestreviewsData.name = latestreviewsData.optString("name")
                                modelLatestreviewsData.profile_image =
                                    latestreviewsData.optString("profile_image")
                                modelLatestreviewsData.updated_at =
                                    latestreviewsData.optString("updated_at")
                                arrayReviewsDescription.add(modelLatestreviewsData)
                            }
                            modelMainInformation.modelLatestreviewsData = arrayReviewsDescription
                            //------------For Tags------------------//
                            val tags = information.getJSONArray("tags")
                            for (i in 0 until tags.length()) {
                                val modelTags = ModeTags()
                                modelTags.name = tags[i].toString()
                                arrayTags.add(modelTags)
                            }
                            modelMainInformation.modelTags = arrayTags

                            val tagsInfo = data.getJSONArray("taginfo")
                            for (i in 0 until tagsInfo.length()) {
                                val tagInfo = tagsInfo.getJSONObject(i)
                                val modelTagss = ModeTags()
                                modelTagss.id = tagInfo.optString("id")
                                modelTagss.name = tagInfo.optString("name")
                                modelTagss.parent_id = tagInfo.optString("parent_id")
                                modelTagss.subscription_count =
                                    tagInfo.optString("subscription_count")
                                modelTagss.selected_image = tagInfo.optString("selected_image")
                                modelTagss.unselected_image = tagInfo.optString("unselected_image")
                                modelTagss.banner = tagInfo.optString("banner")
                                modelTagss.created_at = tagInfo.optString("created_at")
                                modelTagss.updated_at = tagInfo.optString("updated_at")
                                arrayTagsNew.add(modelTagss)
                            }

                            modelMainInformation.modelTagsNew = arrayTagsNew

                            val parentTagInfo = data.getJSONArray("parentTags")
                            for(i in 0 until parentTagInfo.length()){
                                val parentInfo = parentTagInfo.getJSONObject(i)
                                val modelTags = ModeTags()
                                modelTags.parent_id = parentInfo.optString("id")
                                modelTags.parent_name = parentInfo.optString("name")
                                arrayParentTagsNew.add(modelTags)
                            }
                            modelMainInformation.parentTags = arrayParentTagsNew


                            //------------- Getting Data for staff----------//
                            for (i in 0 until staff.length()) {
                                val modelStaff = ModelStaffList()
                                val staffData = staff.optJSONObject(i)
                                modelStaff.name = staffData.optString("name")
                                modelStaff.image = staffData.optString("image")
                                modelStaff.speciality = staffData.optString("speciality")
                                modelStaff.facebook = staffData.optString("facebook")
                                modelStaff.twitter = staffData.optString("twitter")
                                modelStaff.instagram = staffData.optString("instagram")
                                arrayStaff.add(modelStaff)
                            }

                            //---------------- Getting Data from plan--------------------//
                            for (i in 0 until plan.length()) {
                                val planModel = PlanModel()
                                val planData = plan.optJSONObject(i)
                                planModel.price = planData.optString("price")
                                planModel.discount_price = planData.optString("discount_price")
                                planModel.frequency_type = planData.optString("frequency_type")
                                planModel.free_trial = planData.optString("free_trial")
                                planModel.free_trial_qty = planData.optString("free_trial_qty")
                                planModel.subscription_id = planData.optString("subscription_id")
                                planModel.campaign_id = planData.optString("campaign_id")
                                planModel.unit = planData.optString("unit")
                                planModel.frequency_type = planData.optString("frequency_type")
                                planModel.free_trial = planData.optString("free_trial")
                                planModel.redeemtion_cycle_qty =
                                    planData.optString("redeemtion_cycle_qty")
                                planModel.redeem_count = planData.optString("redeem_count")
                                planModel.introductory_days =
                                    planData.optString("introductory_days")
                                planModel.introductory_price =
                                    planData.optString("introductory_price")
                                planModel.frequency_value = planData.optString("frequency_value")
                                planModel.created_at = planData.optString("created_at")
                                planModel.updated_at = planData.optString("updated_at")
                                planModel.likes = planData.optString("likes")
                                planModel.views = planData.optString("views")
                                planModel.bought = planData.optString("bought")
                                arrayPlan.add(planModel)
                            }
                            for (i in 0 until bought_subscription.length()) {
                                val mySubscriptionModel = MySubscriptionsModel()
                                val planData = plan.optJSONObject(i)
                                mySubscriptionModel.price = planData.optString("price")
                                mySubscriptionModel.subscription_id =
                                    planData.optString("subscription_id")
                                mySubscriptionModel.discount_price =
                                    planData.optString("discount_price")
                                mySubscriptionModel.frequency_type =
                                    planData.optString("frequency_type")
                                mySubscriptionModel.free_trial = planData.optString("free_trial")
                                mySubscriptionModel.free_trial_qty =
                                    planData.optString("free_trial_qty")
                                mySubscriptionModel.campaign_id = planData.optString("campaign_id")
                                mySubscriptionModel.unit = planData.optString("unit")
                                mySubscriptionModel.frequency_type =
                                    planData.optString("frequency_type")
                                mySubscriptionModel.redeemtion_cycle_qty =
                                    planData.optString("redeemtion_cycle_qty")
                                mySubscriptionModel.redeem_count =
                                    planData.optString("redeem_count")
                                mySubscriptionModel.introductory_days =
                                    planData.optString("introductory_days")
                                mySubscriptionModel.introductory_price =
                                    planData.optString("introductory_price")
                                mySubscriptionModel.frequency_value =
                                    planData.optString("frequency_value")
                                mySubscriptionModel.created_at = planData.optString("created_at")
                                mySubscriptionModel.updated_at = planData.optString("updated_at")
                                mySubscriptionModel.likes = planData.optString("like")
                                mySubscriptionModel.views = planData.optString("views")
                                mySubscriptionModel.bought = planData.optString("bought")
                                mySubscriptionModel.bar_code = planData.optString("bar_code")
                                mySubscriptionModel.quantity = planData.optString("quantity")
                                mySubscriptionModel.share = planData.optString("share")
                                mySubscriptionModel.status2 = planData.optString("status")
                                mySubscriptionModel.is_shared = planData.optString("is_shared")
                                mySubscriptionModel.order_id = planData.optString("order_id")
                                mySubscriptionModel.boom_discount =
                                    planData.optString("boom_discount")
                                mySubscriptionModel.cancel_discount =
                                    planData.optString("cancel_discount")
                                mySubscriptionModel.tax = planData.optString("tax")
                                mySubscriptionModel.next_billing_date =
                                    planData.optString("next_billing_date")
                                arraySubs.add(mySubscriptionModel)
                            }

                            //---------------------- Getting Data from campaignInfo----------------//
                            modelCampaignInfo.name = campaignInfo.optString("name")
                            modelCampaignInfo.business_id = campaignInfo.optString("business_id")
                            modelCampaignInfo.company_name = campaignInfo.optString("company_name")
                            modelCampaignInfo.logo = campaignInfo.optString("logo")
                            modelCampaignInfo.likes = campaignInfo.optString("likes")
                            modelCampaignInfo.views = campaignInfo.optString("views")
                            modelCampaignInfo.bought = campaignInfo.optString("bought")
                            modelCampaignInfo.boom_status = campaignInfo.optString("boom_status")
                            modelCampaignInfo.boom_discount =
                                campaignInfo.optString("boom_discount")
                            modelCampaignInfo.favourite = campaignInfo.optInt("like")
                            modelCampaignInfo.feature_image =
                                campaignInfo.optString("feature_image")
                            modelCampaignInfo.description = campaignInfo.optString("description")
                            modelCampaignInfo.team_member = campaignInfo.optString("team_member")
                            modelCampaignInfo.campaign_start =
                                campaignInfo.optString("campaign_start")
                            modelCampaignInfo.location_id = campaignInfo.optString("location_id")
                            modelCampaignInfo.is_discounted =
                                campaignInfo.optString("is_discounted")
                            modelCampaignInfo.products = campaignInfo.optString("products")
                            modelCampaignInfo.about = campaignInfo.optString("about")

                            modelCampaignInfo.boom_status = campaignInfo.optString("boom_status")
                            modelCampaignInfo.boom_discount =
                                campaignInfo.optString("boom_discount")
                            modelCampaignInfo.coupon = campaignInfo.optString("coupon")
                            modelCampaignInfo.feature_image =
                                campaignInfo.optString("feature_image")
                            modelCampaignInfo.cancel_discount =
                                campaignInfo.optString("cancel_discount")
                            modelCampaignInfo.campaign_end = campaignInfo.optString("campaign_end")
                            modelCampaignInfo.business_about = campaignInfo.optString("business_about")
                            modelCampaignInfo.remaining_subscriber_limit = campaignInfo.optString("remaining_subscriber_limit")
                            modelCampaignInfo.default_location = campaignInfo.optString("default_location")

                            val editor = Constant.getPrefs(application).edit()
                            editor.putString(Constant.default_loc, modelCampaignInfo.default_location)
                            editor.apply()

                            val images = campaignInfo.getJSONArray("image")
                            for (j in 0 until images.length()) {
                                val modelImages = ModelImagesSubscriptionDetails()
                                modelImages.image = images[j].toString()
                                imagesArray.add(modelImages)
                            }

                            for (loc in 0 until locations.length()) {
                                val modeLocation = ModelSearchLocation()
                                val locionData = locations.optJSONObject(loc)
                                modeLocation.id = locionData.optString("id")
                                modeLocation.business_id = locionData.optString("business_id")
                                modeLocation.user_id = locionData.optString("user_id")
                                modeLocation.street = locionData.optString("street")
                                modeLocation.city = locionData.optString("city")
                                modeLocation.state = locionData.optString("state")
                                modeLocation.zip_code = locionData.optString("zip_code")
                                modeLocation.created_at = locionData.optString("created_at")
                                modeLocation.updated_at = locionData.optString("updated_at")
                                modeLocation.lat = locionData.optString("lat")
                                modeLocation.long = locionData.optString("long")
                                modeLocation.state_code = locionData.optString("state_code")
                                val store_timing = locionData.getJSONArray("store_timing")
                                for (store in 0 until store_timing.length()){
                                    val modelStore = ModelStoreLocation()
                                    val storeData = store_timing.optJSONObject(store)
                                    modelStore.close = storeData.optString("close")
                                    modelStore.open = storeData.optString("open")
                                    modelStore.day = storeData.optString("day")
                                    modelStore.status = storeData.optString("status")
                                    modelStore.checked = storeData.optString("checked")
                                    modeLocation.store_timing.add(modelStore)
                                }
                                arrayLocation.add(modeLocation)
                            }
                            modelMain.status = status
                            modelMain.msg = msg
                            modelStatus.status = status
                            modelStatus.msg = msg
                        } else {
                            modelStatus.msg = msg
                        }

                        mDataPlanArray.value = arrayPlan
                        mDataSubArray.value = arraySubs
                        mDataImagesArrays.value = imagesArray
                        mDataMain.value = modelMain
                        mDataCampaignInfo.value = modelCampaignInfo
                        mDataLocation.value = arrayLocation
                        mDataStaff.value = arrayStaff
                        mDataSimilarSubscription.value = arraysimilarSubscription
                        mDataInformation.value = modelMainInformation

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus

            }
        })
    }


    // ---------------------------------- Fav Api -------------------------//
    fun setFavouriteData(auth: String, model: String,  favourite: Int) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()
        var likeOrUnlike = 1
        likeOrUnlike = if (favourite == 1) {
            2
        } else {
            1
        }
        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> =
            service.setFavorite(auth, model, likeOrUnlike.toString())
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        val model1 = ModelCampaignInfo()

                        if (status == "true") {
                            model1.status = status
                            model1.msg = msg
//                            likeOrUnlike = if (favourite == 1) {
//                                2
//                            } else {
//                                1
//                            }
                            var arrayList : ArrayList<String> = ArrayList()
                            val mPrefs = application.getSharedPreferences(
                                "data",
                                AppCompatActivity.MODE_PRIVATE
                            )
                            val gson = Gson()
                            val getFav = mPrefs.getString("favData", "")
                            if (getFav != "") {
                                arrayList = gson.fromJson<ArrayList<String>>(
                                    getFav,
                                    object : TypeToken<ArrayList<String>>() {
                                    }.type
                                )
                                var exist = 0
                                for (item in arrayList) {
                                    if (model == item) {
                                        exist = 1
                                        break
                                    }
                                }
                                if (exist == 0) {
                                    arrayList.add(model)
                                }else{
                                    if(msg.contains("Favorite removed successfully.")){
                                        arrayList.remove(model)
                                    }
                                }
                            } else {
                                arrayList.add(model)
                            }
                            val prefsEditor = mPrefs.edit()
                            val json = gson.toJson(arrayList)
                            prefsEditor.putString("favData", json)
                            prefsEditor.apply()
                            Log.e("favData", json)
                            model1.favourite = likeOrUnlike

                            mDataSubscription.value = model1
                        } else {

                            modelStatus.msg = msg
                        }

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus

            }

        })
    }

    fun boomSubscription(auth: String, campaign_id: String, buy: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.boomSubscription(auth, campaign_id, buy)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        if (status == "true") {
                            modelStatus.status = status
                            modelStatus.msg = msg
                            mDataBoom.value = modelStatus
                        } else {
                            mDataFailure.value = modelStatus
                            modelStatus.msg = msg
                        }

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus

            }

        })
    }

    fun buyCampaignRequest(campaign_id: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val auth = Constant.getPrefs(application).getString(Constant.auth_code, "")
        val call: Call<ResponseBody> = service.buyCampaignRequest(auth, campaign_id)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        if (status == "true") {
                            modelStatus.status = status
                            modelStatus.msg = msg
                            mbuyCampaignRequest.value = modelStatus
                        } else {
                            modelStatus.msg = msg
                        }

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus

            }

        })
    }


    fun getmPlanData(): LiveData<ArrayList<PlanModel>> {
        return mDataPlanArray
    }

    fun getmSubData(): LiveData<ArrayList<MySubscriptionsModel>> {
        return mDataSubArray
    }

    fun getmDataCampaignInfo(): LiveData<ModelCampaignInfo> {
        return mDataCampaignInfo
    }

    fun getmDataFailure(): LiveData<ModelStatusMsg> {
        return mDataFailure
    }

    fun getmDataBoom (): LiveData<ModelStatusMsg> {
        return mDataBoom
    }



    fun getmmbuyCampaignRequestSuccess(): LiveData<ModelStatusMsg> {
        return mbuyCampaignRequest
    }

    fun getmDataMain(): LiveData<ModelSubscriptionDetailsMain> {
        return mDataMain
    }

    fun getmDataStaff(): LiveData<ArrayList<ModelStaffList>> {
        return mDataStaff
    }

    fun getmDataSimilarSubscription(): LiveData<ArrayList<SimilarSubsciptionsModel>> {
        return mDataSimilarSubscription
    }

    fun getmDataImages(): LiveData<ArrayList<ModelImagesSubscriptionDetails>> {
        return mDataImagesArrays
    }

    fun getmDataInformation(): LiveData<ModelMainInformation> {
        return mDataInformation
    }

    fun getmDataLocation(): LiveData<ArrayList<ModelSearchLocation>> {
        return mDataLocation
    }


    fun getmHomeSubscription(): LiveData<ModelCampaignInfo> {
        return mDataSubscription
    }

    fun setFavouriteData1(auth: String, model1: String, favourite: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}