package com.upscribber.subsciptions.merchantSubscriptionPages

import android.os.Parcel
import android.os.Parcelable

class ModelSubscriptionData() :Parcelable{
    var introPrice = "0.00"
    var introDays = "0.00"
    var couponDiscount = "0"
    var boomDiscount = "0.00"
    var boomStatus = ""
    var mainPrice = "0.00"
    var discountedPrice = "0.00"
    var priceBeforeDiscount = "0.00"
    var priceAfterDiscount = "0.00"
    var credits = "0.00"
    var subTotal = "0.00"
    var subscriptionPrice = "0.00"
    var discountPercentage = "0.00"
    var retailPrice = "0.00"

    constructor(parcel: Parcel) : this() {
        introPrice = parcel.readString()
        introDays = parcel.readString()
        couponDiscount = parcel.readString()
        boomDiscount = parcel.readString()
        boomStatus = parcel.readString()
        mainPrice = parcel.readString()
        discountedPrice = parcel.readString()
        priceBeforeDiscount = parcel.readString()
        priceAfterDiscount = parcel.readString()
        credits = parcel.readString()
        subTotal = parcel.readString()
        subscriptionPrice = parcel.readString()
        discountPercentage = parcel.readString()
        retailPrice = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(introPrice)
        parcel.writeString(introDays)
        parcel.writeString(couponDiscount)
        parcel.writeString(boomDiscount)
        parcel.writeString(boomStatus)
        parcel.writeString(mainPrice)
        parcel.writeString(discountedPrice)
        parcel.writeString(priceBeforeDiscount)
        parcel.writeString(priceAfterDiscount)
        parcel.writeString(credits)
        parcel.writeString(subTotal)
        parcel.writeString(subscriptionPrice)
        parcel.writeString(discountPercentage)
        parcel.writeString(retailPrice)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ModelSubscriptionData> {
        override fun createFromParcel(parcel: Parcel): ModelSubscriptionData {
            return ModelSubscriptionData(parcel)
        }

        override fun newArray(size: Int): Array<ModelSubscriptionData?> {
            return arrayOfNulls(size)
        }
    }


}
