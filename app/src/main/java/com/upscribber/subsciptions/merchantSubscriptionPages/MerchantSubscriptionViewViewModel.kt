package com.upscribber.subsciptions.merchantSubscriptionPages

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.upscribber.commonClasses.ModelStatusMsg
import com.upscribber.home.ModelHomeDataSubscriptions
import com.upscribber.subsciptions.merchantStore.ModelStaffList
import com.upscribber.subsciptions.merchantSubscriptionPages.plan.PlanModel
import com.upscribber.subsciptions.merchantSubscriptionPages.similarSubscriptions.SimilarSubsciptionsModel
import com.upscribber.subsciptions.mySubscriptionDetails.MySubscriptionsModel
import com.upscribber.upscribberSeller.subsriptionSeller.location.searchLoc.ModelSearchLocation

class MerchantSubscriptionViewViewModel(application: Application) : AndroidViewModel(application) {

    var merchantSubscriptionViewRepository: MerchantSubscriptionViewRepository =
        MerchantSubscriptionViewRepository(application)

    fun getSubscriptionDetails(auth: String, model: String) {
        merchantSubscriptionViewRepository.getDetailsSubscriptions(auth, model)
    }

    fun getStatus(): LiveData<ModelStatusMsg> {
        return merchantSubscriptionViewRepository.getmDataFailure()
    }

    fun getmDataBoom(): LiveData<ModelStatusMsg> {
        return merchantSubscriptionViewRepository.getmDataBoom()
    }


    fun getmDataMain(): LiveData<ModelSubscriptionDetailsMain> {
        return merchantSubscriptionViewRepository.getmDataMain()
    }

    fun getmDataCampaignInfo(): LiveData<ModelCampaignInfo> {
        return merchantSubscriptionViewRepository.getmDataCampaignInfo()
    }

    fun getmPlanData(): LiveData<ArrayList<PlanModel>> {
        return merchantSubscriptionViewRepository.getmPlanData()
    }

    fun getmSubData(): LiveData<ArrayList<MySubscriptionsModel>> {
        return merchantSubscriptionViewRepository.getmSubData()
    }

    fun getmDataStaff(): LiveData<ArrayList<ModelStaffList>> {
        return merchantSubscriptionViewRepository.getmDataStaff()
    }

    fun getmDataSimilarSubscription(): LiveData<ArrayList<SimilarSubsciptionsModel>> {
        return merchantSubscriptionViewRepository.getmDataSimilarSubscription()
    }

    fun getmDataImages(): LiveData<ArrayList<ModelImagesSubscriptionDetails>> {
        return merchantSubscriptionViewRepository.getmDataImages()
    }

    fun getmDataInformation(): LiveData<ModelMainInformation> {
        return merchantSubscriptionViewRepository.getmDataInformation()
    }


    fun getmDataLocation(): LiveData<ArrayList<ModelSearchLocation>> {
        return merchantSubscriptionViewRepository.getmDataLocation()
    }


    fun setFavourite(auth: String, model1: String, favourite: Int) {
        merchantSubscriptionViewRepository.setFavouriteData(auth, model1,favourite)
    }

    fun setBoomSubscription(auth: String, campaign_id: String, buy: String) {

        merchantSubscriptionViewRepository.boomSubscription(auth, campaign_id, buy)
    }


    fun getmDataHomeSubscription(): LiveData<ModelCampaignInfo> {
        return merchantSubscriptionViewRepository.getmHomeSubscription()
    }



    fun buyCampaignRequest(campaign_id: String) {
        merchantSubscriptionViewRepository.buyCampaignRequest(campaign_id)
    }

    fun getSuccessfulResponse(): LiveData<ModelStatusMsg> {
        return merchantSubscriptionViewRepository.getmmbuyCampaignRequestSuccess()
    }

}