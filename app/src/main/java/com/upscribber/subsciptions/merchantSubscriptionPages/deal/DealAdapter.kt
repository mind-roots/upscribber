package com.upscribber.subsciptions.merchantSubscriptionPages.deal

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.R
import kotlinx.android.synthetic.main.deal_recycler.view.*

class DealAdapter(
    private val mContext: Context,
    private val dealList: ArrayList<DealModel>
) :
    RecyclerView.Adapter<DealAdapter.MyViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(mContext)
            .inflate(
                R.layout.deal_recycler
                , parent, false
            )
        return MyViewHolder(v)
    }

    override fun getItemCount(): Int {
        return dealList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = dealList[position]

        holder.itemView.ll_bullet.setImageResource(model.bullet)
        holder.itemView.tvInclude.text= model.includeText
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

}
