package com.upscribber.subsciptions.merchantSubscriptionPages.similarSubscriptions

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.upscribber.subsciptions.merchantSubscriptionPages.MerchantSubscriptionViewActivity
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import kotlinx.android.synthetic.main.mechant_similar_subscriptions.view.*

class SimilarProductsAdapter
    (
    private val mContext: Context,
    var i : Int
) :
    RecyclerView.Adapter<SimilarProductsAdapter.MyViewHolder>() {

    private var list: ArrayList<SimilarSubsciptionsModel> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val v = LayoutInflater.from(mContext)
            .inflate(
                R.layout.mechant_similar_subscriptions
                , parent, false
            )
        return MyViewHolder(
            v
        )
    }

    override fun getItemCount(): Int {
        return if (i == 1){
            list.size
        }else{
            if (list.size > 5){
                5
            }else{
                return list.size
            }
        }
    }

    override fun onBindViewHolder(holder : MyViewHolder, position: Int) {
        val model = list[position]
        val imagePath  = Constant.getPrefs(mContext).getString(Constant.dataImagePath, "")

        if (model.feature_image.isEmpty()){
            Glide.with(mContext).load(R.mipmap.placeholder_subscription).into(holder.itemView.imageSimilar)
        }else{
            Glide.with(mContext).load(imagePath + model.feature_image).into(holder.itemView.imageSimilar)
        }
//        holder.itemView.tv_percentage.text = model.profit_percentage
//        holder.itemView.tv_free.text = model.freeText
        holder.itemView.textView112.text = model.name
        holder.itemView.typesubs.text = model.business_name

        holder.itemView.setOnClickListener {
            mContext.startActivity(Intent(mContext,
                MerchantSubscriptionViewActivity::class.java).putExtra("modelSubscriptionView",model.campaign_id)
                .putExtra("modelSubscriptionn",model))

        }

    }

    fun update(it: java.util.ArrayList<SimilarSubsciptionsModel>?) {
        list = it as ArrayList<SimilarSubsciptionsModel>
        notifyDataSetChanged()

    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)


}