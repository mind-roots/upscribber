package com.upscribber.subsciptions.mySubscriptionDetails

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.upscribber.subsciptions.manageSubscriptions.ManageSubscriptionsActivity
import com.upscribber.subsciptions.merchantSubscriptionPages.MerchantSubscriptionViewActivity
import com.daimajia.swipe.SwipeLayout
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter
import com.upscribber.commonClasses.Constant
import com.upscribber.R
import com.upscribber.subsciptions.manageSubscriptions.shareSubscription.ShareInputActivity
import kotlinx.android.synthetic.main.mysubscriptions_layout.view.*
import java.math.BigDecimal
import java.math.RoundingMode


class MySubscriptionsAdapter(
    private val mContext: Context,
    subscriptionsFragment: SubscriptionsFragment
) :
    RecyclerSwipeAdapter<MySubscriptionsAdapter.MyViewHolder>() {

    var listener = subscriptionsFragment as ShowSheet
    private var list: ArrayList<MySubscriptionsModel> = ArrayList()

    override fun getSwipeLayoutResourceId(position: Int): Int {
        return R.id.swipe

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v =
            LayoutInflater.from(mContext).inflate(R.layout.mysubscriptions_layout, parent, false)
        return MyViewHolder(v)

    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = list[position]

        val imagePath = Constant.getPrefs(mContext).getString(Constant.dataImagePath, "")
        Glide.with(mContext).load(imagePath + model.feature_image).placeholder(R.mipmap.placeholder_subscription_square).into(holder.itemView.imgSubscription)

        holder.itemView.textSubName.text = model.campaign_name.trim()
        holder.itemView.textMerName.text = model.business_name.trim()
        holder.itemView.productOffer.text = model.discount_price.toDouble().toInt().toString() + "% off"

        val cost = Constant.getCalculatedPrice(model.discount_price, model.price)
        val splitPos = cost.split(".")
        if (splitPos[1] == "00" || splitPos[1] == "0") {
            holder.itemView.productPrice.text = splitPos[0]
        }else{
            holder.itemView.productPrice.text = "" + BigDecimal(cost).setScale(2, RoundingMode.HALF_UP)
        }
        val actualCost = model.price.toDouble().toString()
        val splitActualCost = actualCost.split(".")
        if (splitActualCost[1] == "00" || splitActualCost[1] == "0") {
            holder.itemView.tv_last_text1.text = "$" + splitActualCost[0]
        }else{
            holder.itemView.tv_last_text1.text = "$" + actualCost.toInt()
        }

        holder.tv_last_text1.paintFlags =
            holder.tv_last_text1.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG

        if (model.free_trial == "No free trial" && (model.introductory_price == "0" ||model.introductory_price == "0.00")) {
            holder.itemView.tv_free_map.visibility = View.GONE
        } else if (model.free_trial != "No free trial") {
            holder.itemView.tv_free_map.visibility = View.VISIBLE
            holder.itemView.tv_free_map.text = "Free"
            holder.itemView.tv_free_map.setBackgroundResource(R.drawable.intro_price_background)
        } else {
            holder.itemView.tv_free_map.visibility = View.VISIBLE
            holder.itemView.tv_free_map.text = "Intro"
            holder.itemView.tv_free_map.setBackgroundResource(R.drawable.home_free_background)
        }

        //            holder.itemView.swipe.toggle(false)
        if (model.status2 == "3") {
            holder.itemView.shareRedeem.visibility = View.VISIBLE
            holder.itemView.shareRedeem.text = "Resubscribe"
            holder.itemView.cancelTxt.visibility = View.VISIBLE
            holder.itemView.cardView4.alpha = 0.4f
            holder.itemView.shareRedeem.setTextColor(Color.WHITE)
            holder.itemView.shareRedeem.setBackgroundResource(R.drawable.bg_black_rounded)
        }else if (model.share == "1") {
            holder.itemView.shareRedeem.visibility = View.VISIBLE
            holder.itemView.shareRedeem.text = "Shared"
            holder.itemView.cancelTxt.visibility = View.GONE
            holder.itemView.cardView4.alpha = 1f
            holder.itemView.shareRedeem.setTextColor(Color.WHITE)
            holder.itemView.shareRedeem.setBackgroundResource(R.drawable.shared_bg)
        } else if (model.is_shared == "1") {
            holder.itemView.shareRedeem.visibility = View.VISIBLE
            holder.itemView.shareRedeem.text = "Shared with me"
            holder.itemView.cancelTxt.visibility = View.GONE
            holder.itemView.cardView4.alpha = 1f
            holder.itemView.shareRedeem.setTextColor(Color.WHITE)
            holder.itemView.shareRedeem.setBackgroundResource(R.drawable.shared_with_me_bg)
        }else if (model.redeem_count != "0") {
            holder.itemView.shareRedeem.visibility = View.VISIBLE
            holder.itemView.shareRedeem.text = "Redeem"
            holder.itemView.cardView4.alpha = 1f
            holder.itemView.cancelTxt.visibility = View.GONE
            holder.itemView.shareRedeem.setTextColor(Color.WHITE)
            holder.itemView.shareRedeem.setBackgroundResource(R.drawable.bg_pink_rounded)
        } else if (model.status2 == "2") {
            holder.itemView.shareRedeem.visibility = View.VISIBLE
            holder.itemView.shareRedeem.text = "PAUSED"
            holder.itemView.cardView4.alpha = 1f
            holder.itemView.cancelTxt.visibility = View.GONE
            holder.itemView.shareRedeem.setTextColor(Color.WHITE)
            holder.itemView.shareRedeem.setBackgroundResource(R.drawable.bg_yellow_rounded)
        } else{
            holder.itemView.cancelTxt.visibility = View.GONE
            holder.itemView.shareRedeem.visibility = View.GONE
        }

        val valueRedeemCount = model.redeemtion_cycle_qty.toInt() - model.redeem_count.toInt()

        if (model.is_shared == "1" ) {
            holder.itemView.share.visibility = View.GONE
        } else if (model.is_shared == "0") {
            if (model.share_setting == "1") {
                holder.itemView.share.visibility = View.VISIBLE
            } else {
                holder.itemView.share.visibility = View.GONE
            }
        }

        if (valueRedeemCount != 0) {
            holder.redeem.isEnabled
            holder.itemView.share.isEnabled
        } else {
            holder.redeem.alpha = 0.4f
            holder.itemView.share.alpha = 0.4f
        }



        holder.itemView.swipe.addSwipeListener(object : SwipeLayout.SwipeListener {
            override fun onOpen(layout: SwipeLayout?) {
                mItemManger.closeAllExcept(layout)

            }

            override fun onUpdate(layout: SwipeLayout?, leftOffset: Int, topOffset: Int) {
            }

            override fun onStartOpen(layout: SwipeLayout?) {

            }

            override fun onStartClose(layout: SwipeLayout?) {

            }

            override fun onHandRelease(layout: SwipeLayout?, xvel: Float, yvel: Float) {
                layout?.close()

            }

            override fun onClose(layout: SwipeLayout?) {

            }
        })

        mItemManger.bindView(holder.itemView, position)

        holder.setting.setOnClickListener {
            mContext.startActivity(
                Intent(mContext, ManageSubscriptionsActivity::class.java)
                    .putExtra("model", model)
            )
            holder.itemView.swipe.toggle(true)
        }

        holder.redeem.setOnClickListener {
            if (model.status2 == "3") {
                val alertDialog = AlertDialog.Builder(mContext)
                    .setMessage("Canceled subscription cannot be redeemed.")
                    .setPositiveButton("Ok") { dialogInterface, i ->
                        dialogInterface.dismiss()
                    }
                    .show()
            } else {
                if (valueRedeemCount != 0) {
                    listener.OpenSheet(position, model)
                    holder.itemView.swipe.toggle(true)
                }

            }

        }

        holder.itemView.card1.setOnClickListener {
            if (model.status2 == "3"){
                mContext.startActivity(
                    Intent(mContext, MerchantSubscriptionViewActivity::class.java)
                        .putExtra("modelSubscriptionView", model.campaign_id)
                        .putExtra("MySubscriptionsAdapter", "123")
                        .putExtra("MySubscriptionsModel", model)
                        .putExtra("details", "buying")
                        .putExtra("modelSubscription", model)
                        .putExtra("cancelled",model)
                )
            }else {
                mContext.startActivity(
                    Intent(mContext, MerchantSubscriptionViewActivity::class.java)
                        .putExtra("modelSubscriptionView", model.campaign_id)
                        .putExtra("MySubscriptionsAdapter", "123")
                        .putExtra("MySubscriptionsModel", model)
                        .putExtra("details", "buying")
                        .putExtra("modelSubscription", model)
                )
            }
//            holder.itemView.swipe.toggle(false)
        }



        if (model.share == "1") {
            holder.itemView.tvShareText.text = "Unshare"
            holder.itemView.imageShare.setImageResource(R.drawable.ic_share_subs_shared)
        } else {
            holder.itemView.imageShare.setImageResource(R.drawable.ic_share_subs)
            holder.itemView.tvShareText.text = mContext.resources.getText(R.string.share_redeem)
        }

        holder.itemView.share.setOnClickListener {
            val valueRedeemCount1 = model.redeemtion_cycle_qty.toInt() - model.redeem_count.toInt()
            if (model.status2 == "3") {
                val alertDialog = AlertDialog.Builder(mContext)
                    .setMessage("Canceled subscription cannot be shared.")
                    .setPositiveButton("Ok") { dialogInterface, i ->
                        dialogInterface.dismiss()
                    }
                    .show()

            } else {
                if (model.share == "1") {
                    listener.unshareApi(model, position)
                } else {
                    if (valueRedeemCount1 != 0) {
                        mContext.startActivity(
                            Intent(mContext, ShareInputActivity::class.java)
                                .putExtra("model", model)
                                .putExtra("mySubscription", "other")
                        )

                    }
                }
                holder.itemView.swipe.toggle(true)
            }


        }

    }

    fun update(array: ArrayList<MySubscriptionsModel>) {
        this.list = array
        notifyDataSetChanged()
    }


    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tv_last_text1: TextView = itemView.findViewById(R.id.tv_last_text1)
        var setting: LinearLayout = itemView.findViewById(R.id.setting)
        var redeem: LinearLayout = itemView.findViewById(R.id.redeem)
    }

    interface ShowSheet {
        fun OpenSheet(position: Int, model: MySubscriptionsModel)
        fun unshareApi(
            model: MySubscriptionsModel,
            position: Int
        )
    }

}