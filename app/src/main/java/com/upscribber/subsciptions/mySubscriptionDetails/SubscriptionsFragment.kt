package com.upscribber.subsciptions.mySubscriptionDetails

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Outline
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.upscribber.commonClasses.Constant
import com.upscribber.R
import com.upscribber.commonClasses.GetSatertedClick
import com.upscribber.subsciptions.manageSubscriptions.redeem.ReedemActivity
import com.upscribber.commonClasses.RoundedBottomSheetDialogFragment
import com.upscribber.signUp.InviteCodeActivity
import com.upscribber.subsciptions.manageSubscriptions.ManageSubscriptionViewModel
import kotlinx.android.synthetic.main.redeem_sheet_quantity.*


@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class SubscriptionsFragment : Fragment(), MySubscriptionsAdapter.ShowSheet {

    private lateinit var recyclerSubscriptions: RecyclerView
    private lateinit var progressBar12: LinearLayout
    private lateinit var linearViewGif: LinearLayout
    private lateinit var dataSubModel: ArrayList<MySubscriptionsModel>
    private lateinit var tvNoData: TextView
    private lateinit var tvNoDataDescription: TextView
    private lateinit var inviteCode: TextView
    private lateinit var getStarted: TextView
    private lateinit var imageView82: ImageView
    private lateinit var imageGif: ImageView
    private lateinit var adapter: MySubscriptionsAdapter
    lateinit var mViewModel: MySubscriptionViewModel
    lateinit var mViewModelManage: ManageSubscriptionViewModel
    lateinit var update : GetSatertedClick

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_subscriptions, container, false)
        mViewModel = ViewModelProviders.of(this)[MySubscriptionViewModel::class.java]
        mViewModelManage = ViewModelProviders.of(this)[ManageSubscriptionViewModel::class.java]
        initz(view)
        clickListeners()
        setAdapter()
        observerInit()
        return view
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        update = context as GetSatertedClick
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        update = activity as GetSatertedClick
    }

    private fun clickListeners() {
        linearViewGif.setOnClickListener {
            val editor = activity!!.getSharedPreferences("gifTutorial2" , Context.MODE_PRIVATE).edit()
            editor.putString(Constant.gifTutorial2, "1")
            editor.apply()
            linearViewGif.visibility = View.GONE
        }

        getStarted.setOnClickListener {
            update.getStarted(1)
        }

        inviteCode.setOnClickListener {
            startActivity(Intent(activity,InviteCodeActivity::class.java))
        }

    }

    private fun initz(view: View) {
        recyclerSubscriptions = view.findViewById(R.id.recyclerSubscriptions)
        progressBar12 = view.findViewById(R.id.progressBar12)
        tvNoData = view.findViewById(R.id.tvNoData)
        imageView82 = view.findViewById(R.id.imageView82)
        tvNoDataDescription = view.findViewById(R.id.tvNoDataDescription)
        inviteCode = view.findViewById(R.id.inviteCode)
        linearViewGif = view.findViewById(R.id.linearViewGif)
        imageGif = view.findViewById(R.id.imageGif)
        getStarted = view.findViewById(R.id.getStarted)
    }

    private fun observerInit() {
        mViewModel.getStatus().observe(this, Observer {
            if (it.msg == "Invalid auth code") {
                Constant.commonAlert(context!!)
            } else if (it.msg != "No subscription exists.") {
                Toast.makeText(activity, it.msg, Toast.LENGTH_SHORT).show()
            }
            progressBar12.visibility = View.GONE
            tvNoData.visibility = View.VISIBLE
            getStarted.visibility = View.GONE
            tvNoDataDescription.visibility = View.VISIBLE
            imageView82.visibility = View.VISIBLE
            recyclerSubscriptions.visibility = View.GONE
        })

        mViewModel.getmDataSubscriptions().observe(this, Observer {
            if (it.size > 0) {
                adapter.update(it)
                dataSubModel = it
                val editor = activity!!.getSharedPreferences("gifTutorial2" , Context.MODE_PRIVATE)
                val gifData = editor.getString(Constant.gifTutorial2, "")
                if (gifData.isEmpty()){
                    linearViewGif.visibility = View.VISIBLE
                    Glide.with(this).load(R.drawable.swipe_subscription_gif).into(imageGif)
                }else{
                    linearViewGif.visibility = View.GONE
                }
                tvNoData.visibility = View.GONE
                tvNoDataDescription.visibility = View.GONE
                getStarted.visibility = View.GONE
                imageView82.visibility = View.GONE
                recyclerSubscriptions.visibility = View.VISIBLE
                progressBar12.visibility = View.GONE
            } else {
                progressBar12.visibility = View.GONE
                recyclerSubscriptions.visibility = View.GONE
                tvNoData.visibility = View.VISIBLE
                tvNoDataDescription.visibility = View.VISIBLE
                getStarted.visibility = View.GONE
                imageView82.visibility = View.VISIBLE
            }
        })


        mViewModelManage.getmDataState().observe(this, Observer {
            adapter.notifyDataSetChanged()
            apiImplimentation()
        })
    }




    private fun apiImplimentation() {
        val auth = Constant.getPrefs(activity!!).getString(Constant.auth_code, "")
        mViewModel.getMySubscriptionList(auth, "")
        progressBar12.visibility = View.VISIBLE
    }

    private fun setAdapter() {
        recyclerSubscriptions.layoutManager = LinearLayoutManager(activity)
        adapter = MySubscriptionsAdapter(this.activity!!, this@SubscriptionsFragment)
        recyclerSubscriptions.adapter = adapter
    }

    override fun onResume() {
        super.onResume()
        apiImplimentation()

    }

    override fun unshareApi(
        model: MySubscriptionsModel,
        position: Int
    ) {
        val auth = Constant.getPrefs(activity!!).getString(Constant.auth_code, "")
        mViewModelManage.getUnshare(auth, "0", dataSubModel[position].order_id)

    }

    override fun OpenSheet(
        position: Int,
        model: MySubscriptionsModel
    ) {
        val fragment = MenuFragmentRedeem1(model)
        fragment.show(fragmentManager!!, fragment.tag)

    }

    class MenuFragmentRedeem1(var model: MySubscriptionsModel) :
        RoundedBottomSheetDialogFragment() {

        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val view = inflater.inflate(R.layout.redeem_sheet_quantity, container, false)

            val invite = view.findViewById<TextView>(R.id.invite)
            val etQuantity = view.findViewById<TextView>(R.id.etQuantity)
            val imageViewTop = view.findViewById<ImageView>(R.id.imageViewTop)

            val isSeller = Constant.getSharedPrefs(activity!!).getBoolean(Constant.IS_SELLER, false)
            if (isSeller) {
                imageViewTop.setImageResource(R.mipmap.bg_popup)
            } else {
                imageViewTop.setImageResource(R.mipmap.bg_popup_customer)
            }
            roundImage(imageViewTop)
            invite.setOnClickListener {
                if (etQuantity.text.isEmpty()) {
                    Toast.makeText(activity, "Please select quantity first!", Toast.LENGTH_LONG)
                        .show()
                } else {
                    startActivity(
                        Intent(this.activity, ReedemActivity::class.java).putExtra(
                            "quantity",
                            etQuantity.text.toString()
                        )
                            .putExtra("model", model)
                    )
                    dismiss()
                }
            }

            etQuantity.setOnClickListener {
                val choices = getList()
                val charSequenceItems = choices.toArray(arrayOfNulls<CharSequence>(choices.size))
                val mBuilder = AlertDialog.Builder(activity!!)
                mBuilder.setSingleChoiceItems(charSequenceItems, -1) { dialogInterface, i ->
                    etQuantity.text = choices[i]
                    dialogInterface.dismiss()

                }
                val mDialog = mBuilder.create()
                mDialog.show()
            }

            etQuantity.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(
                    s: CharSequence,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                    if (getTextFieldsData()) {
                        invite.setBackgroundResource(R.drawable.favorite_delete_background)
                    } else {
                        invite.setBackgroundResource(R.drawable.favorite_delete_background_opacity)
                    }
                }

                override fun afterTextChanged(s: Editable) {
                }
            })

            return view
        }

        private fun getTextFieldsData(): Boolean {
            val quantity = etQuantity.text.toString().trim().length

            if (quantity == 0) {
                return false
            }

            return true

        }

        private fun roundImage(imageViewTop: ImageView) {
            val curveRadius = 50F

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                imageViewTop.outlineProvider = object : ViewOutlineProvider() {

                    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                    override fun getOutline(view: View?, outline: Outline?) {
                        outline?.setRoundRect(
                            0,
                            0,
                            view!!.width,
                            view.height + curveRadius.toInt(),
                            curveRadius

                        )
                    }
                }

                imageViewTop.clipToOutline = true

            }

        }

        private fun getList(): ArrayList<String> {
            val arrayList: ArrayList<String> = ArrayList()

            if (model.free_trial_qty.toInt() > 0) {
                for (i in 0 until (model.free_trial_qty.toInt() - model.redeem_count.toInt())) {

                    arrayList.add((i + 1).toString())
                }

                if (arrayList.size == 0) {
                    if (model.redeemtion_cycle_qty.toInt() > 100) {
                        for (i in 0 until 500) {
                            arrayList.add((i + 1).toString())
                        }
                    } else {
                        for (i in 0 until (model.redeemtion_cycle_qty.toInt() - model.redeem_count.toInt())) {

                            arrayList.add((i + 1).toString())
                        }
                    }
                }
            } else {
                if (model.redeemtion_cycle_qty.toInt() > 100) {
                    for (i in 0 until 500) {
                        arrayList.add((i + 1).toString())
                    }
                } else {
                    for (i in 0 until (model.redeemtion_cycle_qty.toInt() - model.redeem_count.toInt())) {

                        arrayList.add((i + 1).toString())
                    }
                }
            }





            return arrayList
        }

    }
}