package com.upscribber.subsciptions.mySubscriptionDetails

import android.os.Parcel
import android.os.Parcelable
import com.upscribber.profile.ModelGetProfile

/**
 * Created by amrit on 15/3/19.
 */
class MySubscriptionsModel() : Parcelable {

    var profileData: ModelGetProfile = ModelGetProfile()
    var cancel_discount: String = ""
    var boom_discount: String = ""
    var card_id: String = ""
    var tax: String = ""
    var is_cancelled: String = ""
    var share_date: String = ""
    var share_setting: String = ""
    var wallet: String = ""
    var team_member: String = ""
    var business_name: String = ""
    var service: String = ""
    var cycle_start_date: String = ""
    var contact_no: String = ""
    var name: String = ""
    var order_id: String = ""
    var feature_image: String = ""
    var msg: String = ""
    var status1: String = ""
    var status: Boolean = false
    var id: String = ""
    var unit: String = ""
    var campaign_id: String = ""
    var price: String = ""
    var discount_price: String = ""
    var redeemtion_cycle_qty: String = ""
    var introductory_price: String = ""
    var introductory_days: String = ""
    var merchant_id: String = ""
    var campaign_name: String = ""
    var merchant_name: String = ""
    var frequency_type: String = ""
    var next_billing_date: String = ""
    var frequency_value: String = ""
    var created_at: String = ""
    var updated_at: String = ""
    var free_trial: String = ""
    var redeem_count: String = ""
    var status2: String = ""
    var is_shared: String = ""
    var share: String = ""
    var bar_code: String = ""
    var quantity: String = ""
    var total: String = ""
    var likes: String = ""
    var views: String = ""
    var bought: String = ""
    var itemId: String = ""
    var free_trial_qty: String = ""
    var subscription_id: String = ""

    constructor(parcel: Parcel) : this() {
        cancel_discount = parcel.readString()
        boom_discount = parcel.readString()
        card_id = parcel.readString()
        tax = parcel.readString()
        is_cancelled = parcel.readString()
        share_date = parcel.readString()
        share_setting = parcel.readString()
        wallet = parcel.readString()
        team_member = parcel.readString()
        business_name = parcel.readString()
        service = parcel.readString()
        cycle_start_date = parcel.readString()
        contact_no = parcel.readString()
        name = parcel.readString()
        order_id = parcel.readString()
        feature_image = parcel.readString()
        msg = parcel.readString()
        status1 = parcel.readString()
        status = parcel.readByte() != 0.toByte()
        id = parcel.readString()
        unit = parcel.readString()
        campaign_id = parcel.readString()
        price = parcel.readString()
        discount_price = parcel.readString()
        redeemtion_cycle_qty = parcel.readString()
        introductory_price = parcel.readString()
        introductory_days = parcel.readString()
        merchant_id = parcel.readString()
        campaign_name = parcel.readString()
        merchant_name = parcel.readString()
        frequency_type = parcel.readString()
        next_billing_date = parcel.readString()
        frequency_value = parcel.readString()
        created_at = parcel.readString()
        updated_at = parcel.readString()
        free_trial = parcel.readString()
        redeem_count = parcel.readString()
        status2 = parcel.readString()
        is_shared = parcel.readString()
        share = parcel.readString()
        bar_code = parcel.readString()
        quantity = parcel.readString()
        total = parcel.readString()
        likes = parcel.readString()
        views = parcel.readString()
        bought = parcel.readString()
        itemId = parcel.readString()
        free_trial_qty = parcel.readString()
        subscription_id = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(cancel_discount)
        parcel.writeString(boom_discount)
        parcel.writeString(card_id)
        parcel.writeString(tax)
        parcel.writeString(is_cancelled)
        parcel.writeString(share_date)
        parcel.writeString(share_setting)
        parcel.writeString(wallet)
        parcel.writeString(team_member)
        parcel.writeString(business_name)
        parcel.writeString(service)
        parcel.writeString(cycle_start_date)
        parcel.writeString(contact_no)
        parcel.writeString(name)
        parcel.writeString(order_id)
        parcel.writeString(feature_image)
        parcel.writeString(msg)
        parcel.writeString(status1)
        parcel.writeByte(if (status) 1 else 0)
        parcel.writeString(id)
        parcel.writeString(unit)
        parcel.writeString(campaign_id)
        parcel.writeString(price)
        parcel.writeString(discount_price)
        parcel.writeString(redeemtion_cycle_qty)
        parcel.writeString(introductory_price)
        parcel.writeString(introductory_days)
        parcel.writeString(merchant_id)
        parcel.writeString(campaign_name)
        parcel.writeString(merchant_name)
        parcel.writeString(frequency_type)
        parcel.writeString(next_billing_date)
        parcel.writeString(frequency_value)
        parcel.writeString(created_at)
        parcel.writeString(updated_at)
        parcel.writeString(free_trial)
        parcel.writeString(redeem_count)
        parcel.writeString(status2)
        parcel.writeString(is_shared)
        parcel.writeString(share)
        parcel.writeString(bar_code)
        parcel.writeString(quantity)
        parcel.writeString(total)
        parcel.writeString(likes)
        parcel.writeString(views)
        parcel.writeString(bought)
        parcel.writeString(itemId)
        parcel.writeString(free_trial_qty)
        parcel.writeString(subscription_id)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<MySubscriptionsModel> {
        override fun createFromParcel(parcel: Parcel): MySubscriptionsModel {
            return MySubscriptionsModel(parcel)
        }

        override fun newArray(size: Int): Array<MySubscriptionsModel?> {
            return arrayOfNulls(size)
        }
    }


}