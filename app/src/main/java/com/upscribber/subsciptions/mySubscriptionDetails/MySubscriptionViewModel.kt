package com.upscribber.subsciptions.mySubscriptionDetails

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.upscribber.commonClasses.ModelStatusMsg

class MySubscriptionViewModel(application: Application) : AndroidViewModel(application) {

    var mySubscriptionRepository: MySubscriptionRepository = MySubscriptionRepository(application)

    fun getMySubscriptionList(auth: String, type: String) {
        mySubscriptionRepository.getMySubscriptionList(auth,type)
    }

    fun getStatus(): LiveData<ModelStatusMsg> {
        return mySubscriptionRepository.getmDataFailure()
    }

    fun getmDataSubscriptions(): LiveData<ArrayList<MySubscriptionsModel>> {
        return mySubscriptionRepository.getmDataSubscriptions()
    }

}