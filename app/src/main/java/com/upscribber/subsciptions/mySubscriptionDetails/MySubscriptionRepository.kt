package com.upscribber.subsciptions.mySubscriptionDetails

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.upscribber.commonClasses.Constant
import com.upscribber.commonClasses.ModelStatusMsg
import com.upscribber.commonClasses.WebServicesCustomers
import com.upscribber.profile.ModelGetProfile
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class MySubscriptionRepository(var application: Application) {

    private val mDataMySubscription = MutableLiveData<ArrayList<MySubscriptionsModel>>()
    private val mDataSub = MutableLiveData<MySubscriptionsModel>()
    val mDataFailure = MutableLiveData<ModelStatusMsg>()

    fun getMySubscriptionList(auth: String, type: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.mySubscriptions(auth,type)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status1 = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        val modelSubscription = MySubscriptionsModel()
                        val arrayMySubscription = ArrayList<MySubscriptionsModel>()
                        if (status1 == "true") {
                            val data = json.getJSONObject("data")
                            val subscriptions = data.optJSONArray("subscriptions")
                            for (i in 0 until subscriptions.length()) {
                                val mySubscriptions = subscriptions.optJSONObject(i)
                                val modelMySubscription = MySubscriptionsModel()
                                modelMySubscription.id = mySubscriptions.optString("id")
                                modelMySubscription.unit = mySubscriptions.optString("unit")
                                modelMySubscription.campaign_id = mySubscriptions.optString("campaign_id")
                                modelMySubscription.price = mySubscriptions.optString("price")
                                modelMySubscription.discount_price = mySubscriptions.optString("discount_price")
                                modelMySubscription.redeemtion_cycle_qty = mySubscriptions.optString("redeemtion_cycle_qty")
                                modelMySubscription.introductory_price = mySubscriptions.optString("introductory_price")
                                modelMySubscription.share_setting = mySubscriptions.optString("share_setting")
                                modelMySubscription.introductory_days = mySubscriptions.optString("introductory_days")
                                modelMySubscription.merchant_id = mySubscriptions.optString("merchant_id")
                                modelMySubscription.campaign_name = mySubscriptions.optString("campaign_name")
                                modelMySubscription.merchant_name = mySubscriptions.optString("merchant_name")
                                modelMySubscription.business_name = mySubscriptions.optString("business_name")
                                modelMySubscription.frequency_type = mySubscriptions.optString("frequency_type")
                                modelMySubscription.next_billing_date = mySubscriptions.optString("next_billing_date")
                                modelMySubscription.frequency_value = mySubscriptions.optString("frequency_value")
                                modelMySubscription.created_at = mySubscriptions.optString("created_at")
                                modelMySubscription.updated_at = mySubscriptions.optString("updated_at")
                                modelMySubscription.order_id = mySubscriptions.optString("order_id")
                                modelMySubscription.is_cancelled = mySubscriptions.optString("is_cancelled")
                                modelMySubscription.free_trial = mySubscriptions.optString("free_trial")
                                modelMySubscription.redeem_count = mySubscriptions.optString("redeem_count")
                                modelMySubscription.is_shared = mySubscriptions.optString("is_shared")
                                modelMySubscription.status2 = mySubscriptions.optString("status")
                                modelMySubscription.share = mySubscriptions.optString("share")
                                modelMySubscription.bar_code = mySubscriptions.optString("bar_code")
                                modelMySubscription.quantity = mySubscriptions.optString("quantity")
                                modelMySubscription.total = mySubscriptions.optString("total")
                                modelMySubscription.likes = mySubscriptions.optString("likes")
                                modelMySubscription.views = mySubscriptions.optString("views")
                                modelMySubscription.bought = mySubscriptions.optString("bought")
                                modelMySubscription.feature_image = mySubscriptions.optString("feature_image")
                                modelMySubscription.free_trial_qty = mySubscriptions.optString("free_trial_qty")
                                modelMySubscription.cycle_start_date = mySubscriptions.optString("cycle_start_date")
                                modelMySubscription.tax = mySubscriptions.optString("tax")
                                modelMySubscription.card_id = mySubscriptions.optString("card_id")
                                modelMySubscription.boom_discount = mySubscriptions.optString("boom_discount")
                                modelMySubscription.cancel_discount = mySubscriptions.optString("cancel_discount")
                                modelMySubscription.status1 = status1
                                modelMySubscription.msg = msg
                                arrayMySubscription.add(modelMySubscription)
                            }

                            val dataProfile = data.optJSONObject("profile_data")
                            val modelProfile = ModelGetProfile()
                            modelProfile.profile_image = dataProfile.optString("profile_image")
                            modelProfile.street = dataProfile.optString("street")
                            modelProfile.state = dataProfile.optString("state")
                            modelProfile.city = dataProfile.optString("city")
                            modelProfile.zip_code = dataProfile.optString("zip_code")
                            modelProfile.name = dataProfile.optString("name")
                            modelProfile.contact_no = dataProfile.optString("contact_no")
                            modelProfile.email = dataProfile.optString("email")
                            modelProfile.gender = dataProfile.optString("gender")
                            modelProfile.stripe_customer_id = dataProfile.optString("stripe_customer_id")
                            modelProfile.birthdate = dataProfile.optString("birthdate")
                            modelProfile.tax = dataProfile.optInt("tax")
                            modelProfile.first_time_coupon = dataProfile.optString("first_time_coupon")
                            modelProfile.renew_coupon = dataProfile.optString("renew_coupon")
                            modelProfile.wallet = dataProfile.optString("wallet")
                            modelProfile.last_name = dataProfile.optString("last_name")
                            modelProfile.is_merchant = dataProfile.optString("is_merchant")
                            modelProfile.steps = dataProfile.optString("steps")
                            modelProfile.business_id = dataProfile.optString("business_id")
                            modelProfile.primary_address.street = dataProfile.optString("street")
                            modelProfile.primary_address.city = dataProfile.optString("city")
                            modelProfile.primary_address.state = dataProfile.optString("state")
                            modelProfile.primary_address.zip_code = dataProfile.optString("zip_code")
                            modelProfile.billing_address.billing_street = dataProfile.optString("street")
                            modelProfile.billing_address.billing_city = dataProfile.optString("city")
                            modelProfile.billing_address.billing_state = dataProfile.optString("state")
                            modelProfile.billing_address.billing_zipcode = dataProfile.optString("zip_code")


                                //PENDING PROFILE DATA//
                            modelSubscription.profileData = modelProfile
                            modelSubscription.wallet = data.optString("wallet")
                            val editor = Constant.getPrefs(application).edit()
                            editor.putString(Constant.wallet, modelSubscription.wallet)
                            editor.apply()

                            mDataMySubscription.value = arrayMySubscription
                            modelStatus.status = status1
                            modelStatus.msg = msg
                        } else {
                            modelStatus.status = status1
                            modelStatus.msg = msg
                            mDataFailure.value = modelStatus

                        }

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus

            }
        })
    }

    fun getmDataFailure(): LiveData<ModelStatusMsg> {
        return mDataFailure
    }

    fun getmDataSubscriptions(): LiveData<ArrayList<MySubscriptionsModel>> {
        return mDataMySubscription
    }


}