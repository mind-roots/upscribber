package com.upscribber.subsciptions.extrasSubsription

import android.os.Parcel
import android.os.Parcelable
import com.upscribber.subsciptions.mySubscriptionDetails.MySubscriptionsModel

/**
 * Created by Manisha Thakur on 16/3/19.
 */
class ModelPause() : Parcelable{
    var id: String = ""
    var unit: String = ""
    var campaign_id: String = ""
    var price: String = ""
    var discount_price: String = ""
    var redeemtion_cycle_qty: String = ""
    var introductory_price: String = ""
    var introductory_days: String = ""
    var merchant_id: String = ""
    var campaign_name: String = ""
    var merchant_name: String = ""
    var feature_image: String = ""
    var business_name: String = ""
    var frequency_type: String = ""
    var next_billing_date: String = ""
    var frequency_value: String = ""
    var created_at: String = ""
    var updated_at: String = ""
    var free_trial: String = ""
    var redeem_count: String = ""
    var status2: String = ""
    var share: String = ""
    var bar_code: String = ""
    var quantity: String = ""
    var likes: String = ""
    var views: String = ""
    var bought: String = ""
    var msg: String = ""
    var status: String = ""
    var logo : Int = 0
    var logoText : String = ""
     var logoDate : String = ""
    var data : ArrayList<MySubscriptionsModel> = ArrayList()

    constructor(parcel: Parcel) : this() {
        id = parcel.readString()
        unit = parcel.readString()
        campaign_id = parcel.readString()
        price = parcel.readString()
        discount_price = parcel.readString()
        redeemtion_cycle_qty = parcel.readString()
        introductory_price = parcel.readString()
        introductory_days = parcel.readString()
        merchant_id = parcel.readString()
        campaign_name = parcel.readString()
        merchant_name = parcel.readString()
        feature_image = parcel.readString()
        business_name = parcel.readString()
        frequency_type = parcel.readString()
        next_billing_date = parcel.readString()
        frequency_value = parcel.readString()
        created_at = parcel.readString()
        updated_at = parcel.readString()
        free_trial = parcel.readString()
        redeem_count = parcel.readString()
        status2 = parcel.readString()
        share = parcel.readString()
        bar_code = parcel.readString()
        quantity = parcel.readString()
        likes = parcel.readString()
        views = parcel.readString()
        bought = parcel.readString()
        msg = parcel.readString()
        status = parcel.readString()
        logo = parcel.readInt()
        logoText = parcel.readString()
        logoDate = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(unit)
        parcel.writeString(campaign_id)
        parcel.writeString(price)
        parcel.writeString(discount_price)
        parcel.writeString(redeemtion_cycle_qty)
        parcel.writeString(introductory_price)
        parcel.writeString(introductory_days)
        parcel.writeString(merchant_id)
        parcel.writeString(campaign_name)
        parcel.writeString(merchant_name)
        parcel.writeString(feature_image)
        parcel.writeString(business_name)
        parcel.writeString(frequency_type)
        parcel.writeString(next_billing_date)
        parcel.writeString(frequency_value)
        parcel.writeString(created_at)
        parcel.writeString(updated_at)
        parcel.writeString(free_trial)
        parcel.writeString(redeem_count)
        parcel.writeString(status2)
        parcel.writeString(share)
        parcel.writeString(bar_code)
        parcel.writeString(quantity)
        parcel.writeString(likes)
        parcel.writeString(views)
        parcel.writeString(bought)
        parcel.writeString(msg)
        parcel.writeString(status)
        parcel.writeInt(logo)
        parcel.writeString(logoText)
        parcel.writeString(logoDate)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ModelPause> {
        override fun createFromParcel(parcel: Parcel): ModelPause {
            return ModelPause(parcel)
        }

        override fun newArray(size: Int): Array<ModelPause?> {
            return arrayOfNulls(size)
        }
    }


}