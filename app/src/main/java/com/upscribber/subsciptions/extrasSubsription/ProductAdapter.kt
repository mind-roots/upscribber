package com.upscribber.subsciptions.extrasSubsription

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.R

class ProductAdapter(var context: Context, var mData: ArrayList<ModelProduct>) :
    RecyclerView.Adapter<ProductAdapter.ViewHolder>() {

    var addSheet = context as AddSheet
     var sum : String = "0"
    var totalCount : Int = 0



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.product_item, parent, false))
    }

    override fun getItemCount(): Int {
        return mData.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = mData[position]
        holder.tvAmount.text = model.amount
        holder.name.text = model.name
        holder.countNumber.text = model.quantity.toString()

        if (model.isSelected) {
            holder.countNumber.visibility = View.VISIBLE
            holder.addProduct.visibility = View.VISIBLE
            holder.remove.visibility = View.VISIBLE
            holder.itemView.setBackgroundResource(R.drawable.signup_invite_code)
            holder.add.setImageResource(R.drawable.ic_check_accent)


         }
        else {
            holder.itemView.setBackgroundResource(0)
            holder.add.setImageResource(R.drawable.ic_ms_update)
            holder.countNumber.visibility = View.INVISIBLE
            holder.addProduct.visibility = View.INVISIBLE
            holder.remove.visibility = View.INVISIBLE
        }
//        increament and decrement the quantity

        holder.addProduct.setOnClickListener {
            model.quantity = model.quantity + 1
            holder.countNumber.text = model.quantity.toString()
            holder.tvAmount.text = model.amount
            sum = (sum.toFloat()+ model.amount.toFloat()).toString()

            addSheet.totalAmount(position,sum)

        }
        holder.remove.setOnClickListener {
            if (model.quantity > 1) {
                model.quantity = model.quantity - 1
                holder.countNumber.text = model.quantity.toString()
                sum = (sum.toFloat() - model.amount.toFloat()).toString()
                addSheet.totalAmount(position,sum)
            }
            else{
                totalCount -= 1
                addSheet.showTotal(totalCount)
                addSheet.deleteSheetMethod(position)

            }
        }

        holder.add.setOnClickListener {
            if (!model.isSelected) {
                addSheet.addSheet(position)
                totalCount += 1
                addSheet.showTotal(totalCount)
            }
        }
    }

    fun getCheckedValues(): ArrayList<ModelProduct> {
        var value= ArrayList<ModelProduct>()
        for (i in 0 until mData.size){
            if (mData[i].isSelected){
                value.add(mData[i])
            }
        }
        return value
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        var tvAmount = itemView.findViewById<TextView>(R.id.tvAmount)
        var name = itemView.findViewById<TextView>(R.id.name)
        var countNumber = itemView.findViewById<TextView>(R.id.countNumber)
        var addProduct = itemView.findViewById<ImageView>(R.id.addProduct)
        var remove = itemView.findViewById<ImageView>(R.id.remove)
        var add = itemView.findViewById<ImageView>(R.id.add)

    }

    interface AddSheet {

        fun addSheet(position: Int)
        fun deleteSheetMethod(position: Int)
        fun totalAmount(position: Int, sum: String)
        fun showTotal(totalCount : Int)
    }

}
