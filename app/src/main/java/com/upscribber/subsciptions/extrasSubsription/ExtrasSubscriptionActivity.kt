package com.upscribber.subsciptions.extrasSubsription

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import com.upscribber.commonClasses.RoundedBottomSheetDialogFragment
import com.google.gson.Gson
import com.upscribber.R
import kotlinx.android.synthetic.main.activity_extras_subscription.*

class ExtrasSubscriptionActivity : AppCompatActivity(), ProductAdapter.AddSheet {


    var list = ArrayList<ModelProduct>()
    lateinit var totalAmount : TextView
    lateinit var tvTotalCount : TextView
    lateinit var buttonSave : TextView
    //lateinit var productAdapter: ProductAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_extras_subscription)
        totalAmount = findViewById(R.id.totalAmount)
        tvTotalCount = findViewById(R.id.tvTotalCount)
        buttonSave = findViewById(R.id.buttonSave)


        setToolbar()
        setAdapter()
        buttonSave.setOnClickListener {




            var selectedValues:ArrayList<ModelProduct> =  adapter.getCheckedValues()
            val mPrefs = getSharedPreferences("ProductArray", MODE_PRIVATE)
            val prefsEditor = mPrefs.edit()
            val gson = Gson()
            val json = gson.toJson(selectedValues)
            prefsEditor.putString("myArray", json)

            val totalAmounts : String = totalAmount.text.toString()
            prefsEditor.putString("addTotal", totalAmounts)


            prefsEditor.apply()
            finish()
//            val intent = Intent(this, MerchantSubscriptionViewActivity::class.java)
//            startActivity(intent)

        }
    }

    private fun setToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.msExtraToolbar)
        setSupportActionBar(toolbar)
        title = ""
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    private lateinit var adapter: ProductAdapter

    fun setAdapter() {

        productList.layoutManager = LinearLayoutManager(this)
        adapter = ProductAdapter(this, dummyProducts())
        productList.adapter = adapter

    }


    fun dummyProducts(): ArrayList<ModelProduct> {


        var model = ModelProduct()
        model.name = "Gel Nails"
        model.amount = "4.99"
        model.isSelected = false
        model.quantity = 1
        list.add(model)

        model = ModelProduct()
        model.name = "Shave"
        model.amount = "7.99"
        list.add(model)

        model = ModelProduct()
        model.name = "Facial"
        model.amount = "9.99"
        model.isSelected = false
        model.quantity = 2
        list.add(model)

        model = ModelProduct()
        model.name = "French Tips"
        model.amount = "3.99"
        list.add(model)

        return list
    }

    override fun addSheet(position: Int) {

        val fragment = ExtrasSubscriptionActivity.MenuFragment(position, list, adapter)
        fragment.show(supportFragmentManager, fragment.tag)

    }

    override fun deleteSheetMethod(position: Int) {

        val fragment = ExtrasSubscriptionActivity.MenuFragmentDelete(position, list, adapter)
        fragment.show(supportFragmentManager, fragment.tag)
    }

    override fun totalAmount(position: Int, sum: String) {

        totalAmount.text = sum
    }

    override fun showTotal(totalCount : Int) {
        tvTotalCount.text = totalCount.toString()
    }


    @SuppressLint("ValidFragment")
    class MenuFragment(
        var position: Int,
        var list: ArrayList<ModelProduct>,
        var adapter: ProductAdapter
    ) : RoundedBottomSheetDialogFragment() {

        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
            val view = inflater.inflate(R.layout.add_extra_sheet, container, false)

            val yes = view.findViewById<TextView>(R.id.yes)
            val no = view.findViewById<TextView>(R.id.no)

            yes.setOnClickListener {
                list[position].isSelected = true
                adapter.notifyDataSetChanged()
                dismiss()
            }
            no.setOnClickListener {
                list[position].isSelected = false
                adapter.notifyDataSetChanged()
                dismiss()
            }

            return view
        }

    }

    @SuppressLint("ValidFragment")
    class MenuFragmentDelete(
        var position: Int,
        var list: ArrayList<ModelProduct>,
        var adapter: ProductAdapter
    ) : RoundedBottomSheetDialogFragment() {

        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
            val view = inflater.inflate(R.layout.remove_extra_sheet, container, false)

            val yes = view.findViewById<TextView>(R.id.yes)
            val no = view.findViewById<TextView>(R.id.no)

            yes.setOnClickListener {
                list[position].isSelected = false
                adapter.notifyDataSetChanged()
                dismiss()
            }
            no.setOnClickListener {
                dismiss()
            }

            return view
        }

    }
}
