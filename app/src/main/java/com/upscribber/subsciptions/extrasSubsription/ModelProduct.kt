package com.upscribber.subsciptions.extrasSubsription

class ModelProduct {
    var isSelected: Boolean = false
    var name : String = ""
    var quantity : Int = 1
    var amount : String = ""
}