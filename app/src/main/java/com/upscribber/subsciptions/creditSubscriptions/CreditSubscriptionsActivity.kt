package com.upscribber.subsciptions.creditSubscriptions

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.commonClasses.RoundedBottomSheetDialogFragment
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.subsciptions.manageSubscriptions.ManageSubscriptionViewModel
import kotlinx.android.synthetic.main.activity_credit_subscriptions.*

class CreditSubscriptionsActivity : AppCompatActivity() {

    lateinit var mViewModel : ManageSubscriptionViewModel
    lateinit var mAdapter : CreditSubscriptionAdapter

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_credit_subscriptions)
        mViewModel = ViewModelProviders.of(this)[ManageSubscriptionViewModel::class.java]
        val wallet = Constant.getPrefs(this).getString(Constant.wallet, "")
        textCreditRemaining.text = "$$wallet"

        setToolbar()
        setAdapter()
        clickListeners()
        apiImplimentation()
        observerInit()

    }

    private fun observerInit() {
        mViewModel.getCreditData().observe(this, Observer {
            progress.visibility = View.GONE
            if (it.size > 0){
                noData.visibility = View.GONE
                textView96.visibility = View.VISIBLE
                textView74.visibility = View.VISIBLE
                textView76.visibility = View.VISIBLE
                type.visibility = View.VISIBLE
                mAdapter.update(it)
            }else{
                noData.visibility = View.VISIBLE
                textView96.visibility = View.GONE
                textView74.visibility = View.GONE
                textView76.visibility = View.GONE
                type.visibility = View.GONE
            }

        })


        mViewModel.getmDataStatus().observe(this, Observer {
            progress.visibility = View.GONE
            if (it.msg == "Invalid auth code") {
                Constant.commonAlert(this)
            } else {
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
            }
        })

    }

    private fun apiImplimentation() {
        val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
        mViewModel.getCreditHistory(auth)
        progress.visibility = View.VISIBLE

    }

    private fun clickListeners() {
        tv_apply_credit.setOnClickListener {
            val fragment = MenuFragment()
            fragment.show(supportFragmentManager, fragment.tag)
        }

    }


    private fun setToolbar() {
        setSupportActionBar(creditToolbar)
        title = ""
//        toolbarTitle.text = "Available Credits"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home){
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setAdapter() {
        creditRecycler.layoutManager = LinearLayoutManager(this)
        mAdapter = CreditSubscriptionAdapter(this)
        creditRecycler.adapter = mAdapter
    }



    class MenuFragment : RoundedBottomSheetDialogFragment() {


        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
            val view = inflater.inflate(R.layout.credit_confirmation_sheet, container, false)

            val yes = view.findViewById<TextView>(R.id.yes)
            val no = view.findViewById<TextView>(R.id.no)

            yes.setOnClickListener {
                dismiss()
            }
            no.setOnClickListener {
                dismiss()
            }

            return view
        }

    }


}
