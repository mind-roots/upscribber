package com.upscribber.subsciptions.creditSubscriptions


class CreditSubscriptionModel {

    var id: String = ""
    var customer_id: String = ""
    var source_type: String = ""
    var type: String = ""
    var amount: String = ""
    var refer_id: String = ""
    var created_at: String = ""
    var updated_at: String = ""

}