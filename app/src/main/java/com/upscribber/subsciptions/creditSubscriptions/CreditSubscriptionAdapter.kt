package com.upscribber.subsciptions.creditSubscriptions

import android.content.Context
import android.graphics.Color
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.R
import kotlinx.android.synthetic.main.credit_recycler_layout.view.*
import kotlinx.android.synthetic.main.mysubscriptions_layout.view.*
import kotlinx.android.synthetic.main.mysubscriptions_layout.view.imgSubscription
import kotlinx.android.synthetic.main.mysubscriptions_layout.view.productOffer
import kotlinx.android.synthetic.main.mysubscriptions_layout.view.productPrice
import kotlinx.android.synthetic.main.mysubscriptions_layout.view.tv_last_text1
import org.checkerframework.checker.signedness.qual.Constant

class CreditSubscriptionAdapter
    (private val mContext: Context) :
    RecyclerView.Adapter<CreditSubscriptionAdapter.MyViewHolder>()
    {

         private var list: ArrayList<CreditSubscriptionModel> =  ArrayList()

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            val v = LayoutInflater.from(mContext)
                .inflate(R.layout.credit_recycler_layout, parent, false)

            return MyViewHolder(v)

        }

        override fun getItemCount(): Int {

            return list.size
        }

        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            val model = list[position]
            holder.itemView.textCreditAmount.text = "$" +model.amount

            val date = com.upscribber.commonClasses.Constant.getDatee(model.created_at)
            holder.itemView.textCreditDate.text = date

            if (model.source_type == "1"){
                holder.itemView.creditType.text = "REFUND"
                holder.itemView.textCreditAmount.setTextColor(Color.parseColor("#02CBCD"))
                holder.itemView.creditType.setBackgroundResource(R.drawable.bg_refund)
            }else if (model.source_type == "2"){
                holder.itemView.creditType.text = "PURCHASE"
                holder.itemView.creditType.setBackgroundResource(R.drawable.bg_sea_green_rounded)
                holder.itemView.textCreditAmount.setTextColor(Color.parseColor("#ff3f55"))
            }


        }

        fun update(it: java.util.ArrayList<CreditSubscriptionModel>?) {
            list = it!!
            notifyDataSetChanged()

        }

        class MyViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView)


    }