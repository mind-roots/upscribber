package com.upscribber.subsciptions.reviews

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.commonClasses.Constant
import com.upscribber.subsciptions.merchantStore.AdapterReviewDescription
import com.upscribber.subsciptions.merchantStore.AdapterReviews
import com.upscribber.R
import kotlinx.android.synthetic.main.activity_review.*

class ReviewActivity : AppCompatActivity() {

    lateinit var toolbar: Toolbar
    lateinit var toolbarTitle: TextView
    lateinit var reviewRecycler: RecyclerView
    lateinit var reviewDescList: RecyclerView
    lateinit var mViewModel : ReviewsViewModel
    lateinit var mAdapterReviewsCount: AdapterReviews
    lateinit var mAdapterReviewsDescription: AdapterReviewDescription
    var type : String = "1"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_review)
        mViewModel = ViewModelProviders.of(this)[ReviewsViewModel::class.java]
        initz()
        setToolbar()
        apiImplimentation()
        observersInit()
        setAdapter()
    }

    private fun observersInit() {
        mViewModel.getmDataInformation().observe(this, Observer {
            progressBar7.visibility = View.GONE
            mAdapterReviewsDescription.update(it.modelLatestreviewsData)
            mAdapterReviewsCount.update(it.reviewsmodel)
        })

        mViewModel.getStatus().observe(this, Observer {
            if (it.msg.isNotEmpty()) {
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
            }

        })



    }

    private fun apiImplimentation() {
        if (intent.hasExtra("type")){
            type = "1"
            val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
            val campaignId = intent.getStringExtra("modelSubscriptionView")
            mViewModel.getAllReviews(auth,campaignId,"1",type)
        }else{
            val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
            val businessId = intent.getStringExtra("modelSubscriptionView")
            type = "2"
            mViewModel.getAllReviewsMerchantDetails(auth,businessId,"1",type)
        }
        progressBar7.visibility = View.VISIBLE

    }

    private fun setToolbar() {
        setSupportActionBar(toolbar)
        title = ""
        toolbarTitle.text = "Reviews"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)

    }

    private fun initz() {
        toolbar = findViewById(R.id.reviewToolbar)
        toolbarTitle = findViewById(R.id.titleReview)
        reviewRecycler = findViewById(R.id.reviewRecycler)
        reviewDescList = findViewById(R.id.reviewDescList)

    }

    private fun setAdapter() {
        reviewRecycler.layoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
        reviewRecycler.isNestedScrollingEnabled = false
        mAdapterReviewsCount = AdapterReviews(this)
        reviewRecycler.adapter = mAdapterReviewsCount


        reviewDescList.layoutManager = LinearLayoutManager(this)
        reviewDescList.isNestedScrollingEnabled = false
        mAdapterReviewsDescription = AdapterReviewDescription(this,1)
        reviewDescList.adapter = mAdapterReviewsDescription

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }

        return super.onOptionsItemSelected(item)
    }
}
