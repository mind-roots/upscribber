package com.upscribber.subsciptions.reviews

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.upscribber.commonClasses.Constant
import com.upscribber.commonClasses.ModelStatusMsg
import com.upscribber.commonClasses.WebServicesCustomers
import com.upscribber.subsciptions.merchantStore.ModelReviewDescription
import com.upscribber.subsciptions.merchantStore.ModelReviews
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import java.lang.Exception

class ReviewsRepository(application: Application) {

    val mDataFailure = MutableLiveData<ModelStatusMsg>()
    private val mDataReviewsAll = MutableLiveData<ModelMainViewAllReviews>()

    fun getAllReviews(auth: String, campaignId: String, page: String, type: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.viewAll(auth, campaignId, page,type)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val model = ModelMainViewAllReviews()
                        val modelStatus = ModelStatusMsg()
                        val arrayReviews = ArrayList<ModelReviews>()
                        val arrayReviewsDescription = ArrayList<ModelReviewDescription>()

                        if (status == "true") {
                            val data = json.getJSONObject("data")
                            val reviews = data.getJSONArray("reviews")
                            val latestReviews = data.getJSONArray("latest_reviews")
                            val reviewsmodel = ModelReviews()

                            //------------------- For Reviews List-------------------------//
                            for (i in 0 until reviews.length()) {
                                val reviewsData = reviews.optJSONObject(i)
                                reviewsmodel.name = reviewsData.optString("name")
                                reviewsmodel.count = reviewsData.optString("count")
                                reviewsmodel.id = reviewsData.optString("id")
                                arrayReviews.add(reviewsmodel)
                            }
                            model.reviewsmodel = arrayReviews

                            //----------------------For Latest Reviews--------------------------//
                            for (i in 0 until latestReviews.length()) {
                                val modelLatestreviewsData = ModelReviewDescription()
                                val latestreviewsData = latestReviews.optJSONObject(i)
                                modelLatestreviewsData.review = latestreviewsData.optString("review")
                                modelLatestreviewsData.type = latestreviewsData.optString("type")
                                modelLatestreviewsData.name = latestreviewsData.optString("name")
                                modelLatestreviewsData.profile_image = latestreviewsData.optString("profile_image")
                                modelLatestreviewsData.updated_at = latestreviewsData.optString("updated_at")
                                arrayReviewsDescription.add(modelLatestreviewsData)
                            }
                            model.modelLatestreviewsData = arrayReviewsDescription
                            model.status = status
                            model.msg = msg
                            modelStatus.status = status
                            modelStatus.msg = msg
                        } else {
                            modelStatus.msg = msg
                        }

                        mDataReviewsAll.value = model

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    val modelStatus = ModelStatusMsg()
                    modelStatus.status = "false"
                    modelStatus.msg = "Network Error!"
                    mDataFailure.value = modelStatus
                }

        })
    }

    fun getmDataReviews() : LiveData<ModelMainViewAllReviews> {
        return mDataReviewsAll
    }

    fun getmDataFailure(): LiveData<ModelStatusMsg> {
        return mDataFailure
    }

    fun getAllReviewsMerchantDetails(auth: String, businessId: String, page: String, type: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.viewAllStore(auth, businessId, page,type)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val model = ModelMainViewAllReviews()
                        val modelStatus = ModelStatusMsg()
                        val arrayReviews = ArrayList<ModelReviews>()
                        val arrayReviewsDescription = ArrayList<ModelReviewDescription>()

                        if (status == "true") {
                            val data = json.getJSONObject("data")
                            val reviews = data.getJSONArray("reviews")
                            val latestReviews = data.getJSONArray("latest_reviews")
                            val reviewsmodel = ModelReviews()

                            //------------------- For Reviews List-------------------------//
                            for (i in 0 until reviews.length()) {
                                val reviewsData = reviews.optJSONObject(i)
                                reviewsmodel.name = reviewsData.optString("name")
                                reviewsmodel.count = reviewsData.optString("count")
                                reviewsmodel.id = reviewsData.optString("id")
                                arrayReviews.add(reviewsmodel)
                            }
                            model.reviewsmodel = arrayReviews

                            //----------------------For Latest Reviews--------------------------//
                            for (i in 0 until latestReviews.length()) {
                                val modelLatestreviewsData = ModelReviewDescription()
                                val latestreviewsData = latestReviews.optJSONObject(i)
                                modelLatestreviewsData.review = latestreviewsData.optString("review")
                                modelLatestreviewsData.type = latestreviewsData.optString("type")
                                modelLatestreviewsData.name = latestreviewsData.optString("name")
                                modelLatestreviewsData.profile_image = latestreviewsData.optString("profile_image")
                                modelLatestreviewsData.updated_at = latestreviewsData.optString("updated_at")
                                arrayReviewsDescription.add(modelLatestreviewsData)
                            }
                            model.modelLatestreviewsData = arrayReviewsDescription
                            model.status = status
                            model.msg = msg
                            modelStatus.status = status
                            modelStatus.msg = msg
                        } else {
                            modelStatus.msg = msg
                        }

                        mDataReviewsAll.value = model

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus
            }

        })

    }
}