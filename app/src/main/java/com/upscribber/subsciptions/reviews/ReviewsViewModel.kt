package com.upscribber.subsciptions.reviews

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.upscribber.commonClasses.ModelStatusMsg

class ReviewsViewModel(application: Application) : AndroidViewModel(application) {


    var reviewsRepository : ReviewsRepository = ReviewsRepository(application)

    fun getAllReviews(auth: String, campaignId: String, page: String, type: String) {
        reviewsRepository.getAllReviews(auth,campaignId,page,type)
    }


    fun getmDataInformation(): LiveData<ModelMainViewAllReviews> {
        return reviewsRepository.getmDataReviews()
    }

    fun getAllReviewsMerchantDetails(auth: String, businessId: String, page: String, type: String) {
        reviewsRepository.getAllReviewsMerchantDetails(auth,businessId,page,type)

    }

    fun getStatus(): LiveData<ModelStatusMsg> {
        return reviewsRepository.getmDataFailure()
    }
}