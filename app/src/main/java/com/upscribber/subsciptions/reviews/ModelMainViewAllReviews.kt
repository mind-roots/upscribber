package com.upscribber.subsciptions.reviews

import com.upscribber.subsciptions.merchantStore.ModelReviewDescription
import com.upscribber.subsciptions.merchantStore.ModelReviews

class ModelMainViewAllReviews {

    var status: String = ""
    var msg: String = ""
    var reviewsmodel : ArrayList<ModelReviews> = ArrayList<ModelReviews>()
    var modelLatestreviewsData : ArrayList<ModelReviewDescription> = ArrayList<ModelReviewDescription>()

}
