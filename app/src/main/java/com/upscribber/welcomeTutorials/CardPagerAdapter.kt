package com.upscribber.welcomeTutorials

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.viewpager.widget.PagerAdapter
import com.upscribber.R

class CardPagerAdapter(var mData: ArrayList<CardModel>) : PagerAdapter() {

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun getCount(): Int {
        return mData.size
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val view = LayoutInflater.from(container.context)
            .inflate(R.layout.cardview_adapter, container, false)
        container.addView(view)
        bind(mData[position],view)
        return view
    }

    private fun bind(item: CardModel, view: View) {
        val titleTextView = view.findViewById(R.id.textView_title) as TextView
        val contentTextView = view.findViewById(R.id.textView_des) as TextView
        val contentImageView = view.findViewById(R.id.imageView) as ImageView
        titleTextView.text = item.mTextResource
        contentTextView.text = item.mTitleResource
        contentImageView.setImageResource(item.mImageResource)
    }

}