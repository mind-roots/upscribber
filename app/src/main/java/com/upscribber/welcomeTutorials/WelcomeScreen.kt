package com.upscribber.welcomeTutorials

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.TypedValue
import android.view.View
import androidx.viewpager.widget.ViewPager
import com.upscribber.R
import me.relex.circleindicator.CircleIndicator
import me.relex.circleindicator.Config

class WelcomeScreen : AppCompatActivity() {

    private lateinit var mViewPager: ViewPager
    private lateinit var mCardAdapter: CardPagerAdapter
    private lateinit var indicator : CircleIndicator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome_screen)
        mViewPager = findViewById(R.id.viewPager)
        indicator = findViewById(R.id.indicator)



        val carditem = ArrayList<CardModel>()

        carditem.add(
            CardModel(
               R.drawable.ic_upscriber_logo,
                getString(R.string.earn),
                getString(R.string.text_1)
            )
        )
        carditem.add(
            CardModel(
                R.drawable.ic_upscriber_logo,
                getString(R.string.earn),
                getString(R.string.text_1)
            )
        )
        carditem.add(
            CardModel(
                R.drawable.ic_upscriber_logo,
                getString(R.string.earn),
                getString(R.string.text_1)
            )

        )


        mCardAdapter = CardPagerAdapter(carditem)
        mViewPager.adapter = mCardAdapter

        val indicatorWidth = (TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, 10f,
            resources.displayMetrics
        ) + 0.5f).toInt()
        val indicatorHeight = (TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, 4f,
            resources.displayMetrics
        ) + 0.5f).toInt()
        val indicatorMargin = (TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, 6f,
            resources.displayMetrics
        ) + 0.5f).toInt()



        val config = Config.Builder().width(indicatorWidth)
            .height(indicatorHeight)
            .margin(indicatorMargin)
            .animator(R.animator.indicator_animator)
            .animatorReverse(R.animator.indicator_animator_reverse)
            .drawable(R.drawable.black_radius_square)
            .build()
        indicator.initialize(config)

        mViewPager.adapter = CardPagerAdapter(carditem)
        indicator.setViewPager(mViewPager)
    }






}
