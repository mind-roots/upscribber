package com.upscribber.categories

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.upscribber.commonClasses.Constant
import com.upscribber.commonClasses.ModelStatusMsg
import com.upscribber.commonClasses.WebServicesCustomers
import com.upscribber.search.TopCategoriesSearchModel
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class CategoriesRepository {

    private val mDataCategories = MutableLiveData<ArrayList<TopCategoriesSearchModel>>()
    val mDataFailure = MutableLiveData<ModelStatusMsg>()

    fun getAllCategoriesData(auth: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.getCategories(auth ,"")
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        val arrayCategories= ArrayList<TopCategoriesSearchModel>()
                        if (status == "true") {
                            val data = json.getJSONArray("data")
                            for (i in 0 until data.length()){
                                val dataCategries = data.getJSONObject(i)
                                val modelCategories = TopCategoriesSearchModel()
                                modelCategories.name = dataCategries.optString("name")
                                modelCategories.id = dataCategries.optString("id")
                                modelCategories.subscription_count = dataCategries.optString("subscription_count")
                                modelCategories.selected_image = dataCategries.optString("selected_image")
                                modelCategories.unselected_image = dataCategries.optString("unselected_image")
                                modelCategories.banner = dataCategries.optString("banner")
                                modelCategories.status = status
                                modelCategories.msg = msg
                                arrayCategories.add(modelCategories)
                                modelStatus.status = status
                                modelStatus.msg = msg
                                mDataCategories.value = arrayCategories
                            }
                        } else {
                            modelStatus.status = "false"
                            mDataFailure.value = modelStatus
                        }


                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus

            }
        })

    }


    fun getmDataFailure(): LiveData<ModelStatusMsg> {
        return mDataFailure
    }

    fun getmDataCategories(): LiveData<ArrayList<TopCategoriesSearchModel>> {
        return mDataCategories
    }

}