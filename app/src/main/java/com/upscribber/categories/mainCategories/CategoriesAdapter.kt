package com.upscribber.categories.mainCategories

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.upscribber.R
import com.upscribber.categories.subCategories.SubCategoriesActivity
import com.upscribber.commonClasses.Constant
import com.upscribber.search.TopCategoriesSearchModel
import kotlinx.android.synthetic.main.category_layout.view.*

class CategoriesAdapter(private val mContext: Context) :
    RecyclerView.Adapter<CategoriesAdapter.MyViewHolder>() {

    private var list: ArrayList<TopCategoriesSearchModel> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(mContext).inflate(R.layout.category_layout, parent, false)
        return MyViewHolder(v)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = list[position]

        Glide.with(mContext).load(model.banner).placeholder(R.mipmap.placeholder_subscription).into(holder.itemView.image_category)

        holder.itemView.title_category.text = model.name
        holder.itemView.subscription_category.text = model.subscription_count + " subscriptions"

        holder.itemView.setOnClickListener {
            mContext.startActivity(Intent(mContext, SubCategoriesActivity::class.java)
                .putExtra("model",model.name).putExtra("allCategories",model))
        }

    }

    fun update(it: java.util.ArrayList<TopCategoriesSearchModel>?) {
        list = it!!
        notifyDataSetChanged()

    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

}
