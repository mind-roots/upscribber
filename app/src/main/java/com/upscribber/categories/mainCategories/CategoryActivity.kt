package com.upscribber.categories.mainCategories

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.R
import com.upscribber.categories.CategoriesViewModel
import com.upscribber.commonClasses.Constant
import kotlinx.android.synthetic.main.activity_category.*

class CategoryActivity : AppCompatActivity() {

    lateinit var mRecyclerview: RecyclerView
    lateinit var toolbarCategory: Toolbar
    lateinit var title: TextView
    lateinit var mViewModel : CategoriesViewModel
    lateinit var mAdapterCategories : CategoriesAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_category)
        mViewModel = ViewModelProviders.of(this)[CategoriesViewModel::class.java]
        initz()
        setToolbar()
        setAdapter()
        apiImplimentation()
        observerInit()
    }

    private fun observerInit() {
        mViewModel.getStatus().observe(this, Observer {
            if (it.msg == "Invalid auth code"){
                Constant.commonAlert(this)
            }else{
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
            }
        })

        mViewModel.getmDataCategories().observe(this, Observer {
            if (it.size > 0 ){
                progressBar18.visibility =  View.GONE
                mAdapterCategories.update(it)

            }
        })

    }

    private fun apiImplimentation() {
        val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
        mViewModel.getAllCategories(auth)
        progressBar18.visibility =  View.VISIBLE

    }

    private fun setToolbar() {
        setSupportActionBar(toolbarCategory)
        title.text = "Categories"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)
    }

    private fun initz() {
        toolbarCategory = findViewById(R.id.toolbarCategory)
        title = findViewById(R.id.title)
        mRecyclerview = findViewById(R.id.recyclerview)

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setAdapter() {
        mRecyclerview.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        mRecyclerview.isNestedScrollingEnabled = false
        mAdapterCategories = CategoriesAdapter(this)
        mRecyclerview.adapter = mAdapterCategories

    }

}
