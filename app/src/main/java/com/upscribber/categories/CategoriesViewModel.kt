package com.upscribber.categories

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.upscribber.commonClasses.ModelStatusMsg
import com.upscribber.search.TopCategoriesSearchModel

class CategoriesViewModel(application: Application) : AndroidViewModel(application) {

    var categoriesRepository : CategoriesRepository = CategoriesRepository()

    fun getAllCategories(auth: String) {
        categoriesRepository.getAllCategoriesData(auth)
    }

    fun getStatus(): LiveData<ModelStatusMsg> {
        return categoriesRepository.getmDataFailure()
    }

    fun getmDataCategories(): LiveData<ArrayList<TopCategoriesSearchModel>> {
        return categoriesRepository.getmDataCategories()
    }



}