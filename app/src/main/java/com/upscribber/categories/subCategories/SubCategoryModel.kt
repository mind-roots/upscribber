package com.upscribber.categories.subCategories

import android.os.Parcel
import android.os.Parcelable
class SubCategoryModel() : Parcelable{

    var favorite: Boolean = false
    var bought: String = ""
    var tags: String = ""
    var views: String = ""
    var campaign_image: String = ""
    var unit: String = ""
    var created_at: String = ""
    var frequency_type: String = ""
    var introductory_price: String = ""
    var frequency_value: String = ""
    var free_trial: String = ""
    var updated_at: String = ""
    var status: String = ""
    var subscription_id  : String = ""
    var campaign_id  : String = ""
    var price  : String = ""
    var discount_price  : String = ""
    var location_id  : String = ""
    var subscriber  : String = ""
    var description  : String = ""
    var campaign_name  : String = ""
    var business_logo  : String = ""
    var like  : Int = 0
    var like_percentage  : String = ""
    var msg  : String = ""
    var type : Int = 0
    var location :String = ""

    constructor(parcel: Parcel) : this() {
        favorite = parcel.readByte() != 0.toByte()
        bought = parcel.readString()
        tags = parcel.readString()
        views = parcel.readString()
        campaign_image = parcel.readString()
        unit = parcel.readString()
        created_at = parcel.readString()
        frequency_type = parcel.readString()
        frequency_value = parcel.readString()
        free_trial = parcel.readString()
        updated_at = parcel.readString()
        status = parcel.readString()
        subscription_id = parcel.readString()
        campaign_id = parcel.readString()
        price = parcel.readString()
        discount_price = parcel.readString()
        location_id = parcel.readString()
        subscriber = parcel.readString()
        description = parcel.readString()
        campaign_name = parcel.readString()
        business_logo = parcel.readString()
        like = parcel.readInt()
        like_percentage = parcel.readString()
        msg = parcel.readString()
        type = parcel.readInt()
        location = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeByte(if (favorite) 1 else 0)
        parcel.writeString(bought)
        parcel.writeString(tags)
        parcel.writeString(views)
        parcel.writeString(campaign_image)
        parcel.writeString(unit)
        parcel.writeString(created_at)
        parcel.writeString(frequency_type)
        parcel.writeString(frequency_value)
        parcel.writeString(free_trial)
        parcel.writeString(updated_at)
        parcel.writeString(status)
        parcel.writeString(subscription_id)
        parcel.writeString(campaign_id)
        parcel.writeString(price)
        parcel.writeString(discount_price)
        parcel.writeString(location_id)
        parcel.writeString(subscriber)
        parcel.writeString(description)
        parcel.writeString(campaign_name)
        parcel.writeString(business_logo)
        parcel.writeInt(like)
        parcel.writeString(like_percentage)
        parcel.writeString(msg)
        parcel.writeInt(type)
        parcel.writeString(location)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SubCategoryModel> {
        override fun createFromParcel(parcel: Parcel): SubCategoryModel {
            return SubCategoryModel(parcel)
        }

        override fun newArray(size: Int): Array<SubCategoryModel?> {
            return arrayOfNulls(size)
        }
    }


}