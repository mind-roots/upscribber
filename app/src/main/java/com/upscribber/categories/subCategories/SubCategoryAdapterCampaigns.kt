package com.upscribber.categories.subCategories

import android.content.Context
import android.content.Intent
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.upscribber.home.FavoriteInterface
import com.upscribber.subsciptions.merchantSubscriptionPages.MerchantSubscriptionViewActivity
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import kotlinx.android.synthetic.main.home_whats_new_recycler_two.view.tv_brazillian
import kotlinx.android.synthetic.main.home_whats_new_recycler_two.view.tv_percentage
import kotlinx.android.synthetic.main.subcategory_recycler_layout.view.*
import org.json.JSONArray
import java.math.BigDecimal
import java.math.RoundingMode


class SubCategoryAdapterCampaigns(
    private val mContext: Context,
    listen: SubCategoriesActivity
) :
    RecyclerView.Adapter<SubCategoryAdapterCampaigns.MyViewHolder>() {

    var listener = listen as FavoriteInterface
    private var list: ArrayList<SubCategoryModel> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(mContext).inflate(R.layout.subcategory_recycler_layout, parent, false)
        return MyViewHolder(v)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        val model = list[position]
        val imagePath = Constant.getPrefs(mContext).getString(Constant.dataImagePath, "")
        try {
            holder.itemView.tv_off.text = model.discount_price.toDouble().toInt().toString() + "% off"
            Glide.with(mContext).load(model.like).placeholder(R.mipmap.placeholder_subscription).into(holder.itemView.favorite_image)
            holder.itemView.tv_brazillian.text = model.campaign_name
            holder.itemView.tv_person_count.text = (model.subscriber + " subscribers")
            holder.itemView.tv_long_text.text = model.description
            holder.itemView.textView36.text = getLocation(model.location)


            val cost = Constant.getCalculatedPrice(model.discount_price, model.price)
            val splitPos = cost.split(".")
            if (splitPos[1] == "00" || splitPos[1] == "0") {
                holder.itemView.tv_cost.text = "$" + splitPos[0]
            }else{
                holder.itemView.tv_cost.text = "$" + BigDecimal(cost).setScale(2, RoundingMode.HALF_UP)
            }
            val actualCost = model.price.toDouble().toString()
            val splitActualCost = actualCost.split(".")
            if (splitActualCost[1] == "00" || splitActualCost[1] == "0") {
                holder.itemView.tv_last_text.text = "$" + splitActualCost[0]
            }else{
                holder.itemView.tv_last_text.text = "$" + actualCost.toInt()
            }


            if (model.campaign_image.isEmpty()){
                Glide.with(mContext).load(model.campaign_image).placeholder(R.mipmap.placeholder_subscription).into(holder.itemView.favorite_image)
            }else{
                Glide.with(mContext).load(imagePath + model.campaign_image).into(holder.itemView.favorite_image)
            }

            if (model.like_percentage == "0" || model.like_percentage.isEmpty()) {
                holder.itemView.tv_percentage.visibility = View.GONE
            } else {
                holder.itemView.tv_percentage.visibility = View.VISIBLE
                holder.itemView.tv_percentage.text = model.like_percentage + "%"
            }

            Glide.with(mContext).load(imagePath + model.business_logo).placeholder(R.mipmap.app_icon)
                .into(holder.itemView.imgLogo)


            if (model.free_trial == "No free trial" && (model.introductory_price == "0" || model.introductory_price == "0.00")) {
                holder.itemView.tv_free_map.visibility = View.GONE
            } else if (model.free_trial != "No free trial"){
                holder.itemView.tv_free_map.visibility = View.VISIBLE
                holder.itemView.tv_free_map.text = "Free"
                holder.itemView.tv_free_map.setBackgroundResource(R.drawable.intro_price_background)
            }else{
                holder.itemView.tv_free_map.visibility = View.VISIBLE
                holder.itemView.tv_free_map.text = "Intro"
                holder.itemView.tv_free_map.setBackgroundResource(R.drawable.home_free_background)
            }


        }catch (e: Exception){
            e.printStackTrace()
        }

        val like = Constant.checkFav(model.campaign_id, mContext)
        if (like) {
            holder.itemView.addFav.setImageResource(R.drawable.ic_favorite_fill)
        } else {
            holder.itemView.addFav.setImageResource(R.drawable.ic_favorite_border_black_24dp)

        }

        holder.itemView.addFavv.setOnClickListener {
            listener.addToFav(position)
            if (like) {
                holder.itemView.addFav.setImageResource(R.drawable.ic_favorite_fill)
            } else {
                holder.itemView.addFav.setImageResource(R.drawable.ic_favorite_border_black_24dp)

            }
        }


        holder.itemView.addFavv.setOnClickListener {
            listener.addToFav(position)
        }

        holder.itemView.setOnClickListener {
            mContext.startActivity(Intent(mContext, MerchantSubscriptionViewActivity::class.java).putExtra("modelSubscriptionView", model.campaign_id)
                .putExtra("modelSubscription",model))

        }



        holder.tv_last_text.paintFlags = holder.tv_last_text.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG

    }

    private fun getLocation(location: String): String {
        var resultLocation = ""
        val loc = JSONArray(location)
        if (loc.length() > 1) {
            for (i in 0 until loc.length()) {
                val obj = loc.optJSONObject(i)
                resultLocation = if (resultLocation.isEmpty()) {
                    obj.optString("city")
                } else {
                    "$resultLocation, ${obj.optString("city")}"
                }
            }
        } else {
            val locationObject = loc.optJSONObject(0)
            resultLocation =
                "${locationObject.optString("city")}, ${locationObject.optString("state_code")}"
        }

        return resultLocation
    }

    fun update(it: java.util.ArrayList<SubCategoryModel>?) {
        list = it!!
        notifyDataSetChanged()

    }



    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var tv_last_text: TextView = itemView.findViewById(R.id.tv_last_text)


    }

}
