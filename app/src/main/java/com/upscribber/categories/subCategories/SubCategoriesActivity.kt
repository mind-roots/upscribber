package com.upscribber.categories.subCategories

import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Handler
import android.text.TextUtils
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import com.upscribber.commonClasses.Constant
import com.upscribber.filters.*
import com.upscribber.filters.Redemption.RedemptionFilterFragment
import com.upscribber.home.FavoriteInterface
import com.upscribber.notification.notifications.NotificationActivity
import com.upscribber.R
import com.upscribber.dashboard.DashboardActivity
import com.upscribber.dashboard.SearchActivity
import com.upscribber.home.HomeFragment
import com.upscribber.search.SearchViewModel
import com.upscribber.search.TopCategoriesSearchModel
import kotlinx.android.synthetic.main.activity_dashboard.drawerLayout
import kotlinx.android.synthetic.main.activity_sub_categories.*
import kotlinx.android.synthetic.main.filter_header.*
import org.json.JSONObject

@Suppress(
    "NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS",
    "RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS"
)
class SubCategoriesActivity : AppCompatActivity(), FavoriteInterface
    , FilterInterface, DrawerLayout.DrawerListener, FilterFragment.SetFilterHeaderBackground,
    BrandAdapter.Selection,
    SortByAdapter.Selection {


    private var modelData = ModelData()
    private lateinit var mRvTwo: RecyclerView
    private lateinit var homeWhatsNewTwoAdapter: SubCategoryAdapterCampaigns
    lateinit var toolbar: Toolbar
    lateinit var title: TextView
    lateinit var searchView: SearchView
    private lateinit var filter_done: TextView
    var drawerOpen = 0
    private lateinit var fragmentManager: FragmentManager
    private lateinit var fragmentTransaction: FragmentTransaction
    private lateinit var mreset: TextView
    private lateinit var btnFilter: TextView
    private lateinit var mfilters: TextView
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var mViewModel: SearchViewModel
    private lateinit var checkFragment: Fragment
    private var arrayHomeSubscription = ArrayList<SubCategoryModel>()
    var query = ""
    var auth = ""
    lateinit var currentLatLng: LatLng
    var position1 = 0

    companion object {
        var businessId = ""
        var businessName = "All"
        var sortName = ""
        var checkedItems = ArrayList<ModelSubCategory>()
    }


    override fun onDrawerStateChanged(newState: Int) {


    }

    override fun onDrawerSlide(drawerView: View, slideOffset: Float) {


    }

    override fun onDrawerClosed(drawerView: View) {
        drawerOpen = 0
    }

    override fun onDrawerOpened(drawerView: View) {
        drawerOpen = 1
        drawerLayout.openDrawer(GravityCompat.END)
        inflateFragment(FilterFragment(this))
        filterImgBack.visibility = View.GONE
        mfilters.text = "Filters"
        mreset.visibility = View.VISIBLE
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sub_categories)
        mViewModel = ViewModelProviders.of(this)[SearchViewModel::class.java]
        initzz()
        setToolbar()
        currentLocation()


        if (intent.hasExtra("model")) {
            title.text = intent.getStringExtra("model")
            query = intent.getStringExtra("model")
        } else if (intent.hasExtra("modell")) {
            title.text = intent.getStringExtra("modell")
        }

        SearchActivity.businessName = ""
        SearchActivity.sortName = ""
        DashboardActivity.businessId = ""
        DashboardActivity.sortName = ""

        setFilters()
        setLayout()
        drawerLayout.addDrawerListener(this)
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, GravityCompat.END)
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, GravityCompat.START)

        observerInit()

    }


    private fun observerInit() {
        mViewModel.getmDataSearch().observe(this, Observer { it ->
            progressBar.visibility = View.GONE
            if (it.size > 0) {
                rvTwo.visibility = View.VISIBLE
                imageView82.visibility = View.GONE
                tvNoData.visibility = View.GONE
                tvNoDataDescription.visibility = View.GONE
                arrayHomeSubscription = it
                homeWhatsNewTwoAdapter.update(arrayHomeSubscription)
            } else {
                rvTwo.visibility = View.GONE
                imageView82.visibility = View.VISIBLE
                tvNoData.visibility = View.VISIBLE
                tvNoDataDescription.visibility = View.VISIBLE
            }
        })

        mViewModel.getmDataFav().observe(this, Observer {
            if (it.status == "true") {
                arrayHomeSubscription[position1] = it
//                val editorFeatues = Constant.getPrefs(this).edit()
//                editorFeatues.putString(Constant.dataLatestSubs, arrayHomeSubscription.toString())
//                editorFeatues.apply()
                homeWhatsNewTwoAdapter.update(arrayHomeSubscription)
            }
        })


        mViewModel.getStatus().observe(this, Observer {
            progressBar.visibility = View.GONE
            if (it.msg == "Invalid auth code") {
                Constant.commonAlert(this)
            } else {
                rvTwo.visibility = View.GONE
                tvNoData.visibility = View.VISIBLE
                imageView82.visibility = View.VISIBLE
                tvNoDataDescription.visibility = View.VISIBLE
            }
        })


    }

    @SuppressLint("NewApi")
    private fun currentLocation() {
        if (ActivityCompat.checkSelfPermission(
                this,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions(
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                HomeFragment.LOCATION_PERMISSION_REQUEST_CODE
            )
            return
        }
        fusedLocationClient.lastLocation.addOnSuccessListener(this) { location ->
            // Got last known location. In some rare situations this can be null.
            if (location != null) {
                currentLatLng = LatLng(location.latitude, location.longitude)
                apiImplimentation(currentLatLng)
                clickListeners()
            } else {
                currentLatLng = LatLng(46.7296, 94.6859)
                apiImplimentation(currentLatLng)
                clickListeners()
            }
        }

    }

    private fun apiImplimentation(currentLatLng: LatLng) {
        auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
        when {
            intent.hasExtra("chips") -> {
                val chipId = intent.getStringExtra("chips")
                query = chipId
                mViewModel.getSearchData(
                    auth,
                    query,
                    currentLatLng.latitude,
                    jObjectFilter(checkedItems, modelData, currentLatLng),
                    jObjectSort(sortName),
                    currentLatLng.longitude,
                    2
                )
            }
            intent.hasExtra("modell") -> {
                val chipId = intent.getStringExtra("chipss")
                query = chipId
                mViewModel.getSearchData(
                    auth,
                    query,
                    currentLatLng.latitude,
                    jObjectFilter(checkedItems, modelData, currentLatLng),
                    jObjectSort(sortName),
                    currentLatLng.longitude,
                    2
                )
            }
            intent.hasExtra("typingQuery") -> {
                query = intent.getStringExtra("model")
                mViewModel.getSearchData(
                    auth,
                    query,
                    currentLatLng.latitude,
                    jObjectFilter(checkedItems, modelData, currentLatLng),
                    jObjectSort(sortName),
                    currentLatLng.longitude,
                    1
                )
            }
            intent.hasExtra("topCategory") -> {
                val topCategoryId =
                    intent.getParcelableExtra<TopCategoriesSearchModel>("topCategory")
                query = topCategoryId.id
                mViewModel.getSearchData(
                    auth,
                    query,
                    currentLatLng.latitude,
                    jObjectFilter(checkedItems, modelData, currentLatLng),
                    jObjectSort(sortName),
                    currentLatLng.longitude,
                    2
                )
            }
            else -> {
                val allCategoryId =
                    intent.getParcelableExtra<TopCategoriesSearchModel>("allCategories")
                query = allCategoryId.id
                mViewModel.getSearchData(
                    auth,
                    query,
                    currentLatLng.latitude,
                    jObjectFilter(checkedItems, modelData, currentLatLng),
                    jObjectSort(sortName),
                    currentLatLng.longitude,
                    2
                )
            }
        }

        progressBar.visibility = View.VISIBLE
    }

    private fun initzz() {
        toolbar = findViewById(R.id.toolbar)
        title = findViewById(R.id.title)
        btnFilter = findViewById(R.id.btnFilter3)
        mreset = findViewById(R.id.reset)
        mfilters = findViewById(R.id.filters)
        filter_done = findViewById(R.id.filter_done)
        mRvTwo = findViewById(R.id.rvTwo)
        searchView = findViewById(R.id.searchView)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
    }

    private fun setToolbar() {
        setSupportActionBar(toolbar)
        title = toolbar.findViewById(R.id.title) as TextView
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)

    }

    private fun clickListeners() {

        mreset.setOnClickListener {
            val mPrefs = getSharedPreferences("data", MODE_PRIVATE)
            val prefsEditor = mPrefs.edit()
            prefsEditor.putString("filterData", "")
            prefsEditor.apply()
            businessName = "All"
            businessId = ""
            sortName = "No Filters Selected"
            checkedItems.clear()
            mViewModel.getSearchData(
                auth,
                query,
                currentLatLng.latitude,
                jObjectFilter(checkedItems, modelData, currentLatLng),
                jObjectSort(sortName),
                currentLatLng.longitude,
                1
            )
            inflateFragment(FilterFragment(this))
        }
        filter_done.setOnClickListener {
            if (checkFragment is FilterFragment) {
                modelData = (checkFragment as FilterFragment).getData()
                val mPrefs = getSharedPreferences("data", MODE_PRIVATE)
                val prefsEditor = mPrefs.edit()
                val gson = Gson()
                val json = gson.toJson(modelData)
                prefsEditor.putString("filterData", json)
                prefsEditor.apply()
                if (modelData.latitude != "" && modelData.longitude != "") {
                    if (businessId != "") {
                        mViewModel.getSearchData(
                            auth,
                            businessId,
                            modelData.latitude.toDouble(),
                            jObjectFilter(checkedItems, modelData, currentLatLng),
                            jObjectSort(sortName),
                            modelData.longitude.toDouble(),
                            1
                        )
                    } else {
                        mViewModel.getSearchData(
                            auth,
                            query,
                            modelData.latitude.toDouble(),
                            jObjectFilter(checkedItems, modelData, currentLatLng),
                            jObjectSort(sortName),
                            modelData.longitude.toDouble(),
                            1
                        )
                    }

                } else {
                    if (businessId != "") {
                        mViewModel.getSearchData(
                            auth,
                            businessId,
                            currentLatLng.latitude.toDouble(),
                            jObjectFilter(checkedItems, modelData, currentLatLng),
                            jObjectSort(sortName),
                            currentLatLng.longitude.toDouble(),
                            1
                        )
                    } else {
                        mViewModel.getSearchData(
                            auth,
                            query,
                            currentLatLng.latitude.toDouble(),
                            jObjectFilter(checkedItems, modelData, currentLatLng),
                            jObjectSort(sortName),
                            currentLatLng.longitude.toDouble(),
                            1
                        )
                    }
                }
            }
            onBackPressed()
        }


        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {


            override fun onQueryTextSubmit(query: String): Boolean {
                Constant.hideKeyboard(this@SubCategoriesActivity, searchView)
                return false
            }

            override fun onQueryTextChange(changedText: String): Boolean {
                businessName = ""
                businessId = ""
                progressBar.visibility = View.VISIBLE
                Handler().postDelayed({

                    mViewModel.getSearchData(
                        auth,
                        changedText,
                        currentLatLng.latitude,
                        jObjectFilter(checkedItems, modelData, currentLatLng),
                        jObjectSort(sortName),
                        currentLatLng.longitude,
                        1
                    )

                }, 500)

                return false
            }
        })
    }

    private fun setFilters() {

        btnFilter.setOnClickListener {


            Constant.hideKeyboard(this, searchView)
//            if (searchView != null) {
//                searchView.setQuery("", false)
//                searchView.clearFocus()
//            }
            drawerOpen = 1
            drawerLayout.openDrawer(GravityCompat.END)
            inflateFragment(FilterFragment(this))
            filterImgBack.visibility = View.GONE
            mfilters.text = "Filters"
            mreset.visibility = View.VISIBLE
        }
    }

    private fun inflateFragment(fragment: Fragment) {
        checkFragment = fragment
        fragmentManager = supportFragmentManager
        this.fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.containerSide2, fragment)
        fragmentTransaction.commit()
    }

    override fun onBackPressed() {

        Constant.hideKeyboard(this, searchView)
        if (drawerOpen == 1) {
            drawerOpen = 0
            drawerLayout.closeDrawer(GravityCompat.END)
        } else {
            val mPrefs = getSharedPreferences("data", MODE_PRIVATE)
            val prefsEditor = mPrefs.edit()
            prefsEditor.putString("filterData", "")
            prefsEditor.apply()
            finish()
            super.onBackPressed()
        }
    }

    override fun onResume() {
        super.onResume()
        if (intent.hasExtra("model")) {
            title.text = intent.getStringExtra("model")
            searchView.setQuery(intent.getStringExtra("model"), false)

        }

        Constant.hideKeyboard(this, searchView)
    }


    private fun jObjectFilter(
        checkedItems: ArrayList<ModelSubCategory>,
        modelData: ModelData,
        currentLatLng: LatLng
    ): JSONObject {

        var jArrayCategory = ""
        val selectedCategory = ArrayList<String>()
        if (checkedItems.size > 0) {
            for (i in 0 until checkedItems.size) {
                selectedCategory.add(checkedItems[i].id)
            }
            jArrayCategory = TextUtils.join(",", selectedCategory)
        }

        if (modelData.latitude != "" && modelData.longitude != "") {
            val jsonObject = JSONObject()
            jsonObject.put("tags", jArrayCategory)
            jsonObject.put("min_price", modelData.priceMin)
            jsonObject.put("max_price", modelData.priceMax)
            jsonObject.put("min_distance", modelData.distanceMin)
            jsonObject.put("max_distance", modelData.distanceMax)
            jsonObject.put("lat", modelData.latitude)
            jsonObject.put("long", modelData.longitude)
            return jsonObject
        } else {
            val jsonObject = JSONObject()
            jsonObject.put("tags", jArrayCategory)
            jsonObject.put("min_price", modelData.priceMin)
            jsonObject.put("max_price", modelData.priceMax)
            jsonObject.put("min_distance", modelData.distanceMin)
            jsonObject.put("max_distance", modelData.distanceMax)
            jsonObject.put("lat", currentLatLng.latitude)
            jsonObject.put("long", currentLatLng.longitude)
            return jsonObject
        }


    }

    private fun jObjectSort(sortBy: String): JSONObject {
        val jsonObject = JSONObject()
        when (sortBy) {
            "Price Low to High" -> {

                jsonObject.put("price", "1")

            }
            "Price High to Low" -> {
                jsonObject.put("price", "2")
            }
            "Distance" -> {
                jsonObject.put("distance", "1")
            }
            "Rating" -> {
                jsonObject.put("rating", "2")
            }

            else -> {
                jsonObject.put("", "")
            }
        }
        return jsonObject

    }


    override fun filterHeaderBackground(s: String) {

        if (s == "new") {
            filterHeader.setBackgroundDrawable(getDrawable(R.drawable.filter_header_select_category))


        } else {
            filterHeader.setBackgroundDrawable(getDrawable(R.drawable.filter_header))

        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.dashboard, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            val mPrefs = getSharedPreferences("data", MODE_PRIVATE)
            val prefsEditor = mPrefs.edit()
            prefsEditor.putString("filterData", "")
            prefsEditor.apply()
            finish()
        }

        if (item.itemId == R.id.notification) {
            val intent = Intent(applicationContext, NotificationActivity::class.java)
            startActivity(intent)
        }
        return super.onOptionsItemSelected(item)
    }


    fun setLayout() {

        mRvTwo.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        mRvTwo.isNestedScrollingEnabled = false
        homeWhatsNewTwoAdapter = SubCategoryAdapterCampaigns(this, this)
        mRvTwo.adapter = homeWhatsNewTwoAdapter


    }


    override fun addToFav(position: Int) {
        position1 = position
        val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
        mViewModel.setFavourite(auth, arrayHomeSubscription[position])
//        mViewModel.setFavourite(auth, arraySubCategory[position])

//        val model = homeIcon[position]
//        model.favorite = !model.favorite
//
//
//        homeIcon[position] = model
//        homeWhatsNewTwoAdapter.notifyDataSetChanged()
    }


    override fun nextFragments(i: Int) {
        when (i) {
            0 -> {
                inflateFragment(FilterFragment(this))
                mreset.visibility = View.VISIBLE
                filterImgBack.visibility = View.GONE
                filters.text = "Filters"
                filter_done.setOnClickListener {
                    if (checkFragment is FilterFragment) {
                        modelData = (checkFragment as FilterFragment).getData()
                        val mPrefs = getSharedPreferences("data", MODE_PRIVATE)
                        val prefsEditor = mPrefs.edit()
                        val gson = Gson()
                        val json = gson.toJson(modelData)
                        prefsEditor.putString("filterData", json)
                        prefsEditor.apply()
                        if (modelData.latitude != "" && modelData.longitude != "") {
                            if (businessId != "") {
                                mViewModel.getSearchData(
                                    auth,
                                    businessName,
                                    modelData.latitude.toDouble(),
                                    jObjectFilter(checkedItems, modelData, currentLatLng),
                                    jObjectSort(sortName),
                                    modelData.longitude.toDouble(),
                                    1
                                )
                            } else {
                                mViewModel.getSearchData(
                                    auth,
                                    query,
                                    modelData.latitude.toDouble(),
                                    jObjectFilter(checkedItems, modelData, currentLatLng),
                                    jObjectSort(sortName),
                                    modelData.longitude.toDouble(),
                                    1
                                )
                            }

                        } else {
                            if (businessId != "") {
                                mViewModel.getSearchData(
                                    auth,
                                    businessName,
                                    currentLatLng.latitude,
                                    jObjectFilter(checkedItems, modelData, currentLatLng),
                                    jObjectSort(sortName),
                                    currentLatLng.longitude,
                                    1
                                )
                            } else {
                                mViewModel.getSearchData(
                                    auth,
                                    query,
                                    currentLatLng.latitude,
                                    jObjectFilter(checkedItems, modelData, currentLatLng),
                                    jObjectSort(sortName),
                                    currentLatLng.longitude,
                                    1
                                )
                            }
                        }
                    }
                    if (drawerOpen == 1) {
                        drawerOpen = 0
                        drawerLayout.closeDrawer(GravityCompat.END)
                    }
                }
            }
            1 -> {
                inflateFragment(SortByFragment())
                mreset.visibility = View.GONE
                filterImgBack.visibility = View.VISIBLE
                filters.text = "Sort By"

                filterImgBack.setOnClickListener {
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"
                }

                filter_done.setOnClickListener {
                    progressBar.visibility = View.VISIBLE

                    if (businessId != "") {
                        mViewModel.getSearchData(
                            auth,
                            businessName,
                            currentLatLng.latitude,
                            jObjectFilter(checkedItems, modelData, currentLatLng),
                            jObjectSort(sortName),
                            currentLatLng.longitude,
                            1
                        )
                    } else {
                        mViewModel.getSearchData(
                            auth,
                            query,
                            currentLatLng.latitude,
                            jObjectFilter(checkedItems, modelData, currentLatLng),
                            jObjectSort(sortName),
                            currentLatLng.longitude,
                            1
                        )
                    }

                    onBackPressed()
                }
            }


            2 -> {
                inflateFragment(CategoryFilterFragment(this))
                mreset.visibility = View.GONE
                filterImgBack.visibility = View.VISIBLE
                filters.text = "Category"
//                checkedItems.clear()

                filterImgBack.setOnClickListener {
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"
                }
                filter_done.setOnClickListener {
                    //                    inflateFragment(FilterFragment())
//                    mreset.visibility = View.VISIBLE
//                    filterImgBack.visibility = View.GONE
//                    filters.text = "Filters"
                    if (checkFragment is CategoryFilterFragment) {
                        checkedItems =
                            (checkFragment as CategoryFilterFragment).getCheckedCategories()
                    }

                    if (businessId != "") {
                        mViewModel.getSearchData(
                            auth,
                            businessName,
                            currentLatLng.latitude,
                            jObjectFilter(checkedItems, modelData, currentLatLng),
                            jObjectSort(sortName),
                            currentLatLng.longitude,
                            1
                        )
                    } else {
                        mViewModel.getSearchData(
                            auth,
                            query,
                            currentLatLng.latitude,
                            jObjectFilter(checkedItems, modelData, currentLatLng),
                            jObjectSort(sortName),
                            currentLatLng.longitude,
                            1
                        )
                    }


                    progressBar.visibility = View.VISIBLE
                    onBackPressed()
                }
            }


            3 -> {
                inflateFragment(BrandsFilterFragment())
                mreset.visibility = View.GONE
                filterImgBack.visibility = View.VISIBLE
                filters.text = "Brands"

                filterImgBack.setOnClickListener {
                    Constant.hideKeyboard(this, searchView)
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"

                }

                filter_done.setOnClickListener {
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"
                }
            }
            4 -> {
                inflateFragment(RedemptionFilterFragment())
                mreset.visibility = View.GONE
                filterImgBack.visibility = View.VISIBLE
                filters.text = "Redemption"

                filterImgBack.setOnClickListener {
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"

                }
                filter_done.setOnClickListener {
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"
                }
            }
        }

    }

    @SuppressLint("SetTextI18n")
    override fun getSelectedBrand(
        id: String,
        position: Int,
        businessNameData: String
    ) {
        /*inflateFragment(FilterFragment())

        mreset.visibility = View.VISIBLE
        filterImgBack.visibility = View.GONE
        filters.text = "Filters"*/
        nextFragments(0)

        businessName = businessNameData
        businessId = id
        if (checkFragment is FilterFragment) {
            (checkFragment as FilterFragment).updateBrandData(businessName)
        }

    }

    override fun getSelectedSort(name: String, position: Int) {
        sortName = name

        //jObjectSort(name)


    }
}
