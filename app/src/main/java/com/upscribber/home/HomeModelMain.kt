package com.upscribber.home

import android.os.Parcel
import android.os.Parcelable
import com.upscribber.profile.ModelGetProfile


class HomeModelMain() : Parcelable {
    var arrayInvited: ArrayList<ModelHomeDataSubscriptions> = ArrayList()
    var msg: String = ""
    var status: String = ""
    var subscriptions: ArrayList<ModelHomeDataSubscriptions> = ArrayList()
    var profileData: ModelGetProfile = ModelGetProfile()

    constructor(parcel: Parcel) : this() {
        msg = parcel.readString()
        status = parcel.readString()
        subscriptions = parcel.createTypedArrayList(ModelHomeDataSubscriptions.CREATOR)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(msg)
        parcel.writeString(status)
        parcel.writeTypedList(subscriptions)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<HomeModelMain> {
        override fun createFromParcel(parcel: Parcel): HomeModelMain {
            return HomeModelMain(parcel)
        }

        override fun newArray(size: Int): Array<HomeModelMain?> {
            return arrayOfNulls(size)
        }
    }
}
