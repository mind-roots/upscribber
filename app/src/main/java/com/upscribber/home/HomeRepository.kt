package com.upscribber.home

import android.app.Application
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.common.reflect.TypeToken
import com.google.gson.Gson
import com.upscribber.commonClasses.Constant
import com.upscribber.commonClasses.ModelStatusMsg
import com.upscribber.commonClasses.WebServicesCustomers
import com.upscribber.notification.ModelNotificationCustomer
import com.upscribber.payment.paymentCards.PaymentCardModel
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class HomeRepository(var application: Application) {

    val mDataFailure = MutableLiveData<ModelStatusMsg>()
    val mDataDeclined = MutableLiveData<ModelStatusMsg>()
    val mDataHome = MutableLiveData<HomeModelMain>()
    val mDataSubscription = MutableLiveData<ModelHomeDataSubscriptions>()
    val mDataSubscriptions = MutableLiveData<ArrayList<ModelHomeDataSubscriptions>>()
    private val mDataCategories = MutableLiveData<ArrayList<WhatsNewModelOne>>()
    val mRetrieveCard = MutableLiveData<ArrayList<PaymentCardModel>>()
    val mDataNotification = MutableLiveData<ModelNotificationCustomer>()

    fun getHomeData(
        tag: String,
        city: String,
        sortBy: JSONObject,
        filterBy: JSONObject,
        type: Int
    ) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val auth1 = Constant.getPrefs(application).getString(Constant.auth_code, "")
        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> =
            service.Home(auth1, tag, city, sortBy, filterBy, type.toString())
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        val homeModelMain = HomeModelMain()
                        val arrayLatestSubscription = ArrayList<ModelHomeDataSubscriptions>()
                        val arrayInvited = ArrayList<ModelHomeDataSubscriptions>()
                        val arrayImages = ArrayList<ModelStatusMsg>()
                        val arrayLocation = ArrayList<ModelHomeLocation>()
                        if (status == "true") {
                            val data = json.getJSONObject("data")
                            homeModelMain.status = status
                            val featured_business = data.optJSONArray("featured_business")
                            val latest_subscription = data.optJSONArray("latest_subscription")
                            val invited_subscription = data.optJSONArray("invited_subscription")
                            val profile_data = data.optJSONObject("profile_data")
                            val image_path = data.optString("image_path")

                            for (i in 0 until latest_subscription.length()) {
                                val latestSubsriptions = latest_subscription.getJSONObject(i)
                                val modelSubscriptions = ModelHomeDataSubscriptions()
                                modelSubscriptions.campaign_image = latestSubsriptions.optString("campaign_image")
                                modelSubscriptions.unit = latestSubsriptions.optString("unit")
                                modelSubscriptions.created_at = latestSubsriptions.optString("created_at")
                                modelSubscriptions.frequency_type = latestSubsriptions.optString("frequency_type")
                                modelSubscriptions.frequency_value = latestSubsriptions.optString("frequency_value")
                                modelSubscriptions.free_trial = latestSubsriptions.optString("free_trial")
                                modelSubscriptions.free_trial_qty = latestSubsriptions.optString("free_trial_qty")
                                modelSubscriptions.updated_at = latestSubsriptions.optString("updated_at")
                                modelSubscriptions.status = status
                                modelSubscriptions.subscription_id = latestSubsriptions.optString("subscription_id")
                                modelSubscriptions.campaign_id = latestSubsriptions.optString("campaign_id")
                                modelSubscriptions.price = latestSubsriptions.optString("price")
                                modelSubscriptions.discount_price = latestSubsriptions.optString("discount_price")
                                modelSubscriptions.location_id = latestSubsriptions.optString("location_id")
                                modelSubscriptions.subscriber = latestSubsriptions.optString("bought")
                                modelSubscriptions.description = latestSubsriptions.optString("description")
                                modelSubscriptions.campaign_name = latestSubsriptions.optString("campaign_name")
                                modelSubscriptions.business_logo = latestSubsriptions.optString("business_logo")
                                modelSubscriptions.like_percentage = latestSubsriptions.optString("like_percentage")
                                modelSubscriptions.remaining_subscriber_limit = latestSubsriptions.optString("remaining_subscriber_limit")
                                modelSubscriptions.business_name = latestSubsriptions.optString("business_name")
                                modelSubscriptions.tags = latestSubsriptions.optString("tags")
                                modelSubscriptions.business_phone = latestSubsriptions.optString("business_phone")
                                modelSubscriptions.website = latestSubsriptions.optString("website")
                                modelSubscriptions.business_id = latestSubsriptions.optString("business_id")
                                modelSubscriptions.campaign_start = latestSubsriptions.optString("campaign_start")
                                modelSubscriptions.introductory_price = latestSubsriptions.optString("introductory_price")
                                modelSubscriptions.location = latestSubsriptions.optJSONArray("location").toString()
                                modelSubscriptions.msg = msg
                                modelSubscriptions.like = latestSubsriptions.optInt("like")
                                modelSubscriptions.type = latestSubsriptions.optInt("type")
                                modelSubscriptions.views = latestSubsriptions.optString("views")

//                                val locations = latestSubsriptions.optJSONArray("location")
//                                    for (j in 0 until locations.length()) {
//                                        val locationData = locations.optJSONObject(j)
//                                        val modelLocation = ModelHomeLocation()
//                                        modelLocation.id = locationData.optString("id")
//                                        modelLocation.business_id = locationData.optString("business_id")
//                                        modelLocation.user_id = locationData.optString("user_id")
//                                        modelLocation.street = locationData.optString("street")
//                                        modelLocation.city = locationData.optString("city")
//                                        modelLocation.state = locationData.optString("state")
//                                        modelLocation.zip_code = locationData.optString("zip_code")
//                                        modelLocation.lat = locationData.optString("lat")
//                                        modelLocation.long = locationData.optString("long")
//                                        modelLocation.store_timing = locationData.optString("store_timing")
//                                        modelLocation.created_at = locationData.optString("created_at")
//                                        modelLocation.updated_at = locationData.optString("updated_at")
//                                        arrayLocation.add(modelLocation)
//                                    }

                                val image = latestSubsriptions.getJSONArray("image")
                                for (k in 0 until image.length()) {
                                    val modelImages = ModelStatusMsg()
                                    modelImages.image = image[k].toString()
                                    arrayImages.add(modelImages)
                                }
                                modelSubscriptions.images = arrayImages
//                                modelSubscriptions.locations = arrayLocation
                                arrayLatestSubscription.add(modelSubscriptions)

                            }

                            for (i in 0 until invited_subscription.length()) {
                                val invitedSubscriptionData = invited_subscription.getJSONObject(i)
                                val modelInvitedSubscriptions = ModelHomeDataSubscriptions()
                                modelInvitedSubscriptions.campaign_image = invitedSubscriptionData.optString("campaign_image")
                                modelInvitedSubscriptions.id = invitedSubscriptionData.optString("id")
                                modelInvitedSubscriptions.campaign_id = invitedSubscriptionData.optString("campaign_id")
                                modelInvitedSubscriptions.frequency_type = invitedSubscriptionData.optString("frequency_type")
                                modelInvitedSubscriptions.frequency_value = invitedSubscriptionData.optString("frequency_value")
                                modelInvitedSubscriptions.unit = invitedSubscriptionData.optString("unit")
                                modelInvitedSubscriptions.campaign_name = invitedSubscriptionData.optString("campaign_name")
                                modelInvitedSubscriptions.business_name = invitedSubscriptionData.optString("business_name")
                                modelInvitedSubscriptions.business_id = invitedSubscriptionData.optString("business_id")
                                modelInvitedSubscriptions.typeCheck = invitedSubscriptionData.optString("type")
                                modelInvitedSubscriptions.request_type = invitedSubscriptionData.optString("request_type")
                                modelInvitedSubscriptions.created_at = invitedSubscriptionData.optString("created_at")
                                modelInvitedSubscriptions.order_id = invitedSubscriptionData.optString("order_id")
                                modelInvitedSubscriptions.redeemtion_cycle_qty = invitedSubscriptionData.optString("redeemtion_cycle_qty")
                                modelInvitedSubscriptions.assign_to = invitedSubscriptionData.optString("assign_to")
                                modelInvitedSubscriptions.price = invitedSubscriptionData.optString("price")
                                modelInvitedSubscriptions.customer_id = invitedSubscriptionData.optString("customer_id")
                                modelInvitedSubscriptions.total_amount = invitedSubscriptionData.optString("total_amount")
                                modelInvitedSubscriptions.customer_name = invitedSubscriptionData.optString("customer_name")
                                modelInvitedSubscriptions.contact_no = invitedSubscriptionData.optString("contact_no")
                                modelInvitedSubscriptions.profile_image = invitedSubscriptionData.optString("profile_image")
                                modelInvitedSubscriptions.remaining_visits = invitedSubscriptionData.optString("remaining_visits")
                                modelInvitedSubscriptions.logo = invitedSubscriptionData.optString("logo")
                                modelInvitedSubscriptions.business_phone = invitedSubscriptionData.optString("business_phone")
                                modelInvitedSubscriptions.description = invitedSubscriptionData.optString("description")
                                modelInvitedSubscriptions.team_member = invitedSubscriptionData.optString("team_member")
                                modelInvitedSubscriptions.redeem_id = invitedSubscriptionData.optString("redeem_id")
                                modelInvitedSubscriptions.name = invitedSubscriptionData.optString("name")
                                modelInvitedSubscriptions.c_type = invitedSubscriptionData.optString("c_type")
                                arrayInvited.add(modelInvitedSubscriptions)
                            }

                            val id = profile_data.optString("is_merchant")

                            homeModelMain.subscriptions = arrayLatestSubscription
                            homeModelMain.arrayInvited = arrayInvited

                            val editorFeatues = Constant.getPrefs(application).edit()
                            editorFeatues.putString(Constant.dataFeatues, featured_business.toString())
                            editorFeatues.putString(Constant.dataProfile, profile_data.toString())
                            editorFeatues.putString(Constant.new_notification_status, profile_data.optString("new_notification_status"))
                            editorFeatues.putString(Constant.dataImagePath, image_path)
                            editorFeatues.putString(Constant.isMerchant, id)
                            editorFeatues.putString(Constant.dataLatestSubs, latest_subscription.toString())
                            editorFeatues.putString(Constant.type, profile_data.optString("type"))
                            editorFeatues.apply()

                            Constant.feedback = profile_data.optString("feedback")

                            mDataHome.value = homeModelMain
                            modelStatus.status = status
                            modelStatus.msg = msg
                        } else {
                            modelStatus.msg = msg
                            modelStatus.status = status
                            mDataFailure.value = modelStatus
                        }

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus

            }

        })

    }

    // ---------------------------------- Fav Api -------------------------//
    fun setFavouriteData(auth: String, model: ModelHomeDataSubscriptions) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()
        var likeOrUnlike = 1
        if (model.like == 1) {
            likeOrUnlike = 2
        } else {
            likeOrUnlike = 1
        }
        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> =
            service.setFavorite(auth, model.campaign_id, likeOrUnlike.toString())
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        if (status == "true") {
                            model.status = status
                            model.msg = msg
                            var arrayList : ArrayList<String> = ArrayList()
                            val mPrefs = application.getSharedPreferences(
                                "data",
                                AppCompatActivity.MODE_PRIVATE
                            )
                            val gson = Gson()
                            val getFav = mPrefs.getString("favData", "")
                            if (getFav != "") {
                                arrayList = gson.fromJson<ArrayList<String>>(
                                    getFav,
                                    object : TypeToken<ArrayList<String>>() {
                                    }.type
                                )
                                var exist = 0
                                for (item in arrayList) {
                                    if (model.campaign_id == item) {
                                        exist = 1
                                        break
                                    }
                                }
                                if (exist == 0) {
                                    arrayList.add(model.campaign_id)
                                }else{
                                    if(msg.contains("Favorite removed successfully.")){
                                        arrayList.remove(model.campaign_id)
                                    }
                                }
                            } else {
                                arrayList.add(model.campaign_id)
                            }

                            val prefsEditor = mPrefs.edit()
                            val json = gson.toJson(arrayList)
                            prefsEditor.putString("favData", json)
                            prefsEditor.apply()
                            Log.e("favData", json)
                            if (model.like == 1) {
                                model.like = 2
                            } else {
                                model.like = 1
                            }

                            mDataSubscription.value = model
                        } else {
                            modelStatus.msg = msg
                        }

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus

            }

        })
    }

    fun getProfileDetail(): MutableLiveData<HomeModelMain> {
        return mDataHome
    }

    fun getmHomeSubscription(): LiveData<ModelHomeDataSubscriptions> {
        return mDataSubscription
    }

    fun getmDataSubscriptions(): LiveData<ArrayList<ModelHomeDataSubscriptions>> {
        return mDataSubscriptions
    }


    fun getmDataFailure(): LiveData<ModelStatusMsg> {
        return mDataFailure
    }


    fun getmDataDeclined(): LiveData<ModelStatusMsg> {
        return mDataDeclined
    }


    fun getStepsData(auth: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()


        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.getStepsHistory(auth)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()

                        if (status == "true") {
                            var data = JSONObject()
                            try {
                                data = json.optJSONObject("data")
                            } catch (e: Exception) {
                                e.printStackTrace()
                            }

                            if (data.length() > 0) {
                                val type = Constant.getPrefs(application).getString(Constant.type, "")
                                val editorFeatures = Constant.getPrefs(application).edit()
                                try {
                                    if (type == "1") {
                                        val step1 = data.optJSONArray("step1")
                                        editorFeatures.putString("step1", step1.toString())
                                    } else {
                                        val step1 = data.optJSONObject("step1")
                                        editorFeatures.putString("step1", step1.toString())
                                    }

                                }catch (e: Exception){
                                    e.printStackTrace()
                                }

                                val step2 = data.optJSONObject("step2")
                                val step3 = data.optJSONObject("step3")
                                val step4 = data.optJSONObject("step4")



                                if (step2 != null) {
                                    editorFeatures.putString("step2", step2.toString())
                                } else {
                                    editorFeatures.remove("step2")
                                }

                                if (step3 != null) {
                                    editorFeatures.putString("step3", step3.toString())
                                } else {
                                    editorFeatures.remove("step3")
                                }

                                if (step4 != null) {
                                    editorFeatures.putString("step4", step4.toString())
                                } else {
                                    editorFeatures.remove("step4")
                                }

                                editorFeatures.apply()
                            } else {
                                val editorFeatures = Constant.getPrefs(application).edit()
                                editorFeatures.remove("step1")
                                editorFeatures.remove("step2")
                                editorFeatures.remove("step3")
                                editorFeatures.remove("step4")
                                editorFeatures.apply()
                            }
                        } else {
                            modelStatus.msg = msg
                        }

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus
            }
        })
    }

    fun getAllCategoriesData(auth: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.getCategories(auth,"1")
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        val arrayCategories = ArrayList<WhatsNewModelOne>()
                        if (status == "true") {
                            val data = json.getJSONArray("data")
                            val editorFeatues = Constant.getPrefs(application).edit()
                            editorFeatues.putString(Constant.categories, data.toString())
                            editorFeatues.apply()
                            val backgroundChange =
                                Constant.getCategories(
                                    application,
                                    Constant.categories
                                ) as ArrayList<WhatsNewModelOne>
                            val model = WhatsNewModelOne()
                            model.background = false
                            model.name = "All"
                            model.id = "-1"
                            backgroundChange.add(0, model)
                            val gson = Gson()
                            val catData = gson.toJson(backgroundChange)
                            editorFeatues.putString(Constant.categories, catData)
                            editorFeatues.apply()
                            mDataCategories.value = arrayCategories
                        } else {
                            modelStatus.status = "false"
                            mDataFailure.value = modelStatus
                        }


                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus

            }
        })

    }


    fun getmDataCategories(): LiveData<ArrayList<WhatsNewModelOne>> {
        return mDataCategories
    }


    fun getRetrieveCards() {
        val homeProfileData = Constant.getArrayListProfile(application, Constant.dataProfile)
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.stripeBaseUrl)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> =
            service.stripeRetrieve(homeProfileData.stripe_customer_id, "100")
        call.enqueue(object : Callback<ResponseBody> {


            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {

                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)

                        val data = json.optJSONArray("data")

                        Constant.cardArrayListData.clear()
//                        Constant.cardArrayListDataMerchant.clear()

                        for (i in 0 until data.length()) {
                            val stripeData = data.getJSONObject(i)
                            val cardModel = PaymentCardModel()
                            cardModel.id = stripeData.optString("id")
                            cardModel.brand = stripeData.optString("brand")
                            cardModel.country = stripeData.optString("country")
                            cardModel.last4 = stripeData.optString("last4")
                            cardModel.account_holder_name = stripeData.optString("account_holder_name")
                            cardModel.name = stripeData.optString("name")
                            Constant.cardArrayListData.add(cardModel)
//                            Constant.cardArrayListDataMerchant.add(cardModel)
                            val editor = Constant.getPrefs(application).edit()
                            editor.putString(Constant.stripe_customer_name, cardModel.name)
                            editor.apply()
                        }

                        mRetrieveCard.value = Constant.cardArrayListData

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {

                Toast.makeText(application, "Error!", Toast.LENGTH_LONG).show()
            }
        })

    }

    fun getDeclinedInvites(auth: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.declinedInvitedSubs(auth)
        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        if (status == "true") {
                            modelStatus.status = status
                            mDataDeclined.value = modelStatus
                        } else {
                            modelStatus.status = status
                            modelStatus.msg = msg
                            mDataFailure.value = modelStatus
                        }


                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus

            }
        })

    }

    fun getBusinessLinkInfo(auth: String, businessId: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.getBusinessLinkInfo(auth, businessId)
        call.enqueue(object : Callback<ResponseBody> {


            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val data = json.optJSONObject("data")
                        val notificationModel = ModelNotificationCustomer()
                        notificationModel.business_name = data.optString("business_name")
                        notificationModel.city = data.optString("city")
                        notificationModel.logo = data.optString("logo")
                        notificationModel.msg = data.optString("msg")
                        notificationModel.state = data.optString("state")
                        notificationModel.street = data.optString("street")
                        notificationModel.steps = data.optString("steps")
                        notificationModel.type = data.optString("type")
                        notificationModel.business_id = data.optString("business_id")
                        notificationModel.msgg = msg
                        notificationModel.status = status

                        mDataNotification.value = notificationModel
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.msg = "Network Error!"
                modelStatus.status = "false"
                mDataFailure.value = modelStatus
            }
        })

    }

    fun getmDataNotification(): LiveData<ModelNotificationCustomer> {
        return mDataNotification
    }


}
