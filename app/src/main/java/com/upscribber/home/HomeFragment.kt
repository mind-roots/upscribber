package com.upscribber.home

import android.app.Activity
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.content.Context
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.text.TextUtils
import android.util.Log
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat.checkSelfPermission
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.viewpager.widget.ViewPager
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.*
import com.google.android.gms.maps.model.LatLng
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.upscribber.commonClasses.Constant
import com.upscribber.dashboard.FilterHomeInterface
import com.upscribber.R
import com.upscribber.commonClasses.GetSatertedClick
import com.upscribber.dashboard.DashboardActivity
import com.upscribber.filters.ModelData
import com.upscribber.filters.ModelSubCategory
import com.upscribber.loginSignUpScreen.LoginorSignUpActivity
import com.upscribber.notification.reviews.ReviesFromCustomer
import com.upscribber.payment.paymentCards.AddCardActivity
import com.upscribber.payment.paymentCards.WebPagesOpenActivity
import com.upscribber.profile.InterfaceSwitch
import com.upscribber.subsciptions.merchantSubscriptionPages.MerchantSubscriptionViewActivity
import com.upscribber.upscribberSeller.accountLinking.LinkingRequestActivity
import com.upscribber.upscribberSeller.manageTeam.AddMemberActivity
import kotlinx.android.synthetic.main.alert_already_linked.*
import kotlinx.android.synthetic.main.fragment_home.*
import org.json.JSONObject
import kotlin.collections.ArrayList

@Suppress(
    "NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS",
    "RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS"
)
class HomeFragment(val homeData: ArrayList<ModelHomeDataSubscriptions>) : Fragment(),
    FavoriteInterface, BackGroundInterface
    , HomePagesClicks, HomeWhatsNewTwoAdapter.SwitchSeller {

    private var arrayListOfMapAdapter = ArrayList<ModelHomeDataSubscriptions>()
    private var currentLatLng = LatLng(46.7296, 94.6859)
    private var arrayHomeSubscription = ArrayList<ModelHomeDataSubscriptions>()
    private var googleApiClient: GoogleApiClient? = null
    private lateinit var mRvWhatsNewTwo: RecyclerView
    private lateinit var mRvTopBrands: RecyclerView
    private lateinit var includeLayoutProgress: LinearLayout
    private lateinit var linearViewPager: LinearLayout
    private lateinit var homeWhatsNewTwoAdapter: HomeWhatsNewTwoAdapter
    private lateinit var homeTopBrandAdapter: HomeTopBrandAdapter
    private lateinit var mRvWhatsNewOne: RecyclerView
    private lateinit var btnFilter: TextView
    private lateinit var dismissText: TextView
    private lateinit var textView319: TextView
    private lateinit var floatingButton: TextView
    lateinit var update: FilterHomeInterface
    lateinit var updateClick: GetSatertedClick
    lateinit var updateCard: InterfaceSwitch
    private lateinit var homeWhatsNewOneAdapter: HomeWhatsNewOneAdapter
    private lateinit var viewPager: ViewPager
    private lateinit var backgroundChange: ArrayList<WhatsNewModelOne>
    lateinit var mViewModel: HomeViewModel
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    var myCustomPagerAdapter: PagerAdapterCards? = null
    var position1 = 0
    var type = 2
    private lateinit var swipeRefresh: SwipeRefreshLayout
    private lateinit var listener: apiData
    lateinit var progressBar25: ConstraintLayout


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_home, container, false)
        mViewModel = ViewModelProviders.of(this)[HomeViewModel::class.java]
        initz(view)
        currentLocationOfUser()
        clickListener()
        setLayout()

        observersInit()
        try {
            includeLayoutProgress.visibility = View.GONE
            arrayHomeSubscription = Constant.mDataHome.subscriptions
            homeWhatsNewTwoAdapter.update(Constant.mDataHome.subscriptions)

            val arrayHomeFeature = getArrayList(Constant.dataFeatues)
            homeTopBrandAdapter.update(arrayHomeFeature)
            homeWhatsNewTwoAdapter.update(arrayHomeSubscription)
            backgroundChange = Constant.getCategories(activity!!, Constant.categories) as ArrayList<WhatsNewModelOne>
            homeWhatsNewOneAdapter.update(backgroundChange)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return view
    }

    private fun initz(view: View) {
        textView319 = view.findViewById(R.id.textView319)
        mRvTopBrands = view.findViewById(R.id.rvTopBrands)
        mRvWhatsNewTwo = view.findViewById(R.id.rvWhatsNewTwo)
        linearViewPager = view.findViewById(R.id.linearViewPager)
        mRvWhatsNewOne = view.findViewById(R.id.rvWhatsNewOne)
        btnFilter = view.findViewById(R.id.btnFilter)
        floatingButton = view.findViewById(R.id.floatingButton)
        includeLayoutProgress = view.findViewById(R.id.includeLayoutProgress)
        viewPager = view.findViewById(R.id.viewPager)
        dismissText = view.findViewById(R.id.dismissText)
        swipeRefresh = view.findViewById(R.id.swipeRefresh)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(context!!)
        progressBar25 = view.findViewById(R.id.progressBar25)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        val mPrefs = context?.getSharedPreferences("data", AppCompatActivity.MODE_PRIVATE)
        val gson = Gson()
        val data = mPrefs?.getString("categoryData", "")
        if (data != "") {
            val model = gson.fromJson(data, WhatsNewModelOne::class.java)
            for (items in 0 until backgroundChange.size) {
                backgroundChange[items].background = backgroundChange[items].id == model.id
            }
            homeWhatsNewOneAdapter.update(backgroundChange)
            val dataFilter = mPrefs?.getString("filterData", "")
            var modelFilter = ModelData()
            if (dataFilter != "") {
                modelFilter = gson.fromJson(dataFilter, ModelData::class.java)
            }

            if (homeData.isNotEmpty()) {
                textTrending.visibility = View.VISIBLE
                textView24.visibility = View.VISIBLE
                relativeView.visibility = View.VISIBLE
                val arrayHomeSubscription2 = ArrayList<ModelHomeDataSubscriptions>()
                arrayHomeSubscription2.addAll(homeData)
                arrayHomeSubscription.clear()
                arrayHomeSubscription.addAll(arrayHomeSubscription2)
                homeWhatsNewTwoAdapter.update(arrayHomeSubscription)
                arrayListOfMapAdapter.addAll(arrayHomeSubscription)
            } else {
                if (ActivityCompat.checkSelfPermission(context!!, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(
                        arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                        LOCATION_PERMISSION_REQUEST_CODE
                    )
                    return
                }
                fusedLocationClient.lastLocation.addOnSuccessListener(activity!!) { location ->
                    // Got last known location. In some rare situations this can be null.
                    if (location != null) {
                        currentLatLng = LatLng(location.latitude, location.longitude)
                        progressBar25.visibility = View.VISIBLE
                        mViewModel.getHomeData(
                            model.name,
                            "minnesota",
                            jObjectSort(DashboardActivity.sortName),
                            jObjectFilter(
                                DashboardActivity.checkedItems,
                                modelFilter,
                                currentLatLng
                            ),
                            type
                        )
                    }
                }
            }
        } else {
            if (homeData.isNotEmpty()) {
                textTrending.visibility = View.VISIBLE
                textView24.visibility = View.VISIBLE
                relativeView.visibility = View.VISIBLE
                val arrayHomeSubscription2 = ArrayList<ModelHomeDataSubscriptions>()
                arrayHomeSubscription2.addAll(homeData)
                arrayHomeSubscription.clear()
                arrayHomeSubscription.addAll(arrayHomeSubscription2)
                homeWhatsNewTwoAdapter.update(arrayHomeSubscription)
                arrayListOfMapAdapter.addAll(arrayHomeSubscription)
            } else {
                currentLocation()
            }

        }
        apiImplimantation()

    }

    override fun swichSellerFromCard() {
        checkchangelistener(true)
    }

    private fun checkchangelistener(p1: Boolean) {
        updateCard.switchingProjects(p1)

    }

    private fun apiImplimantation() {
        val auth = Constant.getPrefs(activity!!).getString(Constant.auth_code, "")
        mViewModel.getStepsData(auth)
//        mViewModel.getCategories(auth)
    }


    private fun enableLoc(boolean: Boolean) {
        if (googleApiClient == null) {
            googleApiClient = GoogleApiClient.Builder(activity!!)
                .addApi(LocationServices.API!!)
                .addConnectionCallbacks(object : GoogleApiClient.ConnectionCallbacks {
                    override fun onConnected(bundle: Bundle?) {

                    }

                    override fun onConnectionSuspended(i: Int) {
                        googleApiClient!!.connect()
                    }
                })
                .addOnConnectionFailedListener { connectionResult ->
                    Log.d(
                        "Location error",
                        "Location error " + connectionResult.errorCode
                    )
                }
                .build()
            googleApiClient!!.connect()
            val locationRequest = LocationRequest.create()
            locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            locationRequest.interval = (30 * 1000).toLong()
            locationRequest.fastestInterval = (5 * 1000).toLong()
            val builder = LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest)

            builder.setAlwaysShow(true)

            val result =
                LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build())
            result.setResultCallback { result ->
                val status = result.status
                when (status.statusCode) {
                    LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> try {
                        status.startResolutionForResult(activity, 1)

                    } catch (e: IntentSender.SendIntentException) {
                        e.printStackTrace()
                    }

                }
            }
        }


    }


    companion object {
        const val LOCATION_PERMISSION_REQUEST_CODE = 1
    }

    private fun currentLocation() {
        if (ActivityCompat.checkSelfPermission(
                context!!,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions(
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                LOCATION_PERMISSION_REQUEST_CODE
            )
            return
        }
        fusedLocationClient.lastLocation.addOnSuccessListener(activity!!) { location ->
            // Got last known location. In some rare situations this can be null.
            if (location != null) {
                currentLatLng = LatLng(location.latitude, location.longitude)
                RunningApi(currentLatLng.latitude, currentLatLng.longitude)
            } else {
                currentLatLng = LatLng(46.7296, 94.6859)
                RunningApi(currentLatLng.latitude, currentLatLng.longitude)
            }
        }
    }

    private fun currentLocationOfUser() {
        if (checkSelfPermission(
                context!!,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions(
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                LOCATION_PERMISSION_REQUEST_CODE
            )
            return

        }
        fusedLocationClient.lastLocation.addOnSuccessListener(activity!!) { location ->
            // Got last known location. In some rare situations this can be null.
            if (location != null) {
                currentLatLng = LatLng(location.latitude, location.longitude)

            }
        }
    }

    override fun clickInvitation(mData: ModelHomeDataSubscriptions) {
        val auth = Constant.getPrefs(activity!!).getString(Constant.auth_code, "")
        mViewModel.getDeclinedApi(auth)
        startActivity(
            Intent(
                context,
                MerchantSubscriptionViewActivity::class.java
            ).putExtra("modelSubscriptionView", mData.campaign_id).putExtra(
                "modelSubscription", mData
            )
        )
    }

    override fun clickJoin(mData: ModelHomeDataSubscriptions) {
        val auth = Constant.getPrefs(activity!!).getString(Constant.auth_code, "")
        mViewModel.getDeclinedApi(auth)
        val homeProfile = Constant.getArrayListProfile(activity!!, Constant.dataProfile)
        if (homeProfile.type == "1" && homeProfile.steps == "6" || homeProfile.type == "2" && homeProfile.steps == "4" || homeProfile.type == "3" && homeProfile.steps == "1") {
            val mDialogView = LayoutInflater.from(activity)
                .inflate(R.layout.alert_already_linked, null)
            val mBuilder =
                android.app.AlertDialog.Builder(activity).setView(mDialogView)
            val mAlertDialog = mBuilder.show()
            mAlertDialog.setCancelable(false)
            mDialogView.setBackgroundResource(R.drawable.bg_alert_new)
            mAlertDialog.window.setBackgroundDrawableResource(R.drawable.bg_alert_transparent)
            if (homeProfile.type == "1" && homeProfile.steps == "6") {
                if (mData.c_type == "1") {
                    mAlertDialog.textInfo.setText(R.string.merchant_info)
                } else {
                    mAlertDialog.textInfo.setText(R.string.merchant_info1)
                }
            } else if (homeProfile.type == "2" && homeProfile.steps == "4") {
                if (mData.c_type == "1") {
                    mAlertDialog.textInfo.setText(R.string.contractor_info1)
                } else {
                    mAlertDialog.textInfo.setText(R.string.contractor_info)
                }
            } else if (homeProfile.type == "3" && homeProfile.steps == "1") {
                if (mData.c_type == "1") {
                    mAlertDialog.textInfo.setText(R.string.non_contractor_info)
                } else {
                    mAlertDialog.textInfo.setText(R.string.non_contractor_info1)
                }
            }

            mAlertDialog.okBttn.setOnClickListener {
                mAlertDialog.dismiss()
            }
        } else {
            mViewModel.getBusinessLinkInfo(auth, mData.business_id)
        }

    }

    override fun clickJoinTeam(mData: ModelHomeDataSubscriptions) {
        val auth = Constant.getPrefs(activity!!).getString(Constant.auth_code, "")
        mViewModel.getDeclinedApi(auth)
        val homeProfile = Constant.getArrayListProfile(activity!!, Constant.dataProfile)
        if (homeProfile.type == "1" && homeProfile.steps == "6" || homeProfile.type == "2" && homeProfile.steps == "4" || homeProfile.type == "3") {
            val mDialogView = LayoutInflater.from(activity)
                .inflate(R.layout.alert_already_linked, null)
            val mBuilder =
                android.app.AlertDialog.Builder(activity).setView(mDialogView)
            val mAlertDialog = mBuilder.show()
            mAlertDialog.setCancelable(false)
            mDialogView.setBackgroundResource(R.drawable.bg_alert_new)
            mAlertDialog.window.setBackgroundDrawableResource(R.drawable.bg_alert_transparent)
            if (homeProfile.type == "1" && homeProfile.steps == "6") {
                if (mData.c_type == "1") {
                    mAlertDialog.textInfo.setText(R.string.merchant_info)
                } else {
                    mAlertDialog.textInfo.setText(R.string.merchant_info1)
                }
            } else if (homeProfile.type == "2" && homeProfile.steps == "4") {
                if (mData.c_type == "1") {
                    mAlertDialog.textInfo.setText(R.string.contractor_info1)
                } else {
                    mAlertDialog.textInfo.setText(R.string.contractor_info)
                }
            } else if (homeProfile.type == "3" && homeProfile.steps == "1") {
                if (mData.c_type == "1") {
                    mAlertDialog.textInfo.setText(R.string.non_contractor_info)
                } else {
                    mAlertDialog.textInfo.setText(R.string.non_contractor_info1)
                }
            }

            mAlertDialog.okBttn.setOnClickListener {
                mAlertDialog.dismiss()
            }
        } else {
            startActivity(Intent(activity, AddMemberActivity::class.java).putExtra("home", mData))
        }

    }

    override fun clickDeclinePayments(mData: ArrayList<ModelHomeDataSubscriptions>) {
        val auth = Constant.getPrefs(activity!!).getString(Constant.auth_code, "")
        mViewModel.getDeclinedApi(auth)

    }

    override fun addCard(modelHomeDataSubscriptions: ModelHomeDataSubscriptions) {
        val auth = Constant.getPrefs(activity!!).getString(Constant.auth_code, "")
        mViewModel.getDeclinedApi(auth)
        startActivity(
            Intent(context, AddCardActivity::class.java).putExtra(
                "profilePayment",
                "payments"
            )
        )

    }

    override fun subscriptionDetails(modelHomeDataSubscriptions: ModelHomeDataSubscriptions) {
        val auth = Constant.getPrefs(activity!!).getString(Constant.auth_code, "")
        mViewModel.getDeclinedApi(auth)
        startActivity(
            Intent(
                context,
                MerchantSubscriptionViewActivity::class.java
            ).putExtra("modelSubscriptionView", modelHomeDataSubscriptions.campaign_id)
        )

    }


    override fun rateMerchant(mData: ModelHomeDataSubscriptions) {
        val auth = Constant.getPrefs(activity!!).getString(Constant.auth_code, "")
        mViewModel.getDeclinedApi(auth)
        startActivity(
            Intent(context, ReviesFromCustomer::class.java).putExtra(
                "modelReviews",
                mData
            )
        )

    }


    override fun orderSubscription(modelHomeDataSubscriptions: ModelHomeDataSubscriptions) {
        val auth = Constant.getPrefs(activity!!).getString(Constant.auth_code, "")
        mViewModel.getDeclinedApi(auth)
        startActivity(
            Intent(
                context,
                MerchantSubscriptionViewActivity::class.java
            ).putExtra("modelSubscriptionView", modelHomeDataSubscriptions.campaign_id)
        )

    }

    override fun unsharedSubscription(modelHomeDataSubscriptions: ModelHomeDataSubscriptions) {
        val auth = Constant.getPrefs(activity!!).getString(Constant.auth_code, "")
        mViewModel.getDeclinedApi(auth)
        updateClick.getStarted(2)
    }

    override fun learnFaq(modelHomeDataSubscriptions: ModelHomeDataSubscriptions) {
        val auth = Constant.getPrefs(activity!!).getString(Constant.auth_code, "")
        mViewModel.getDeclinedApi(auth)
        startActivity(
            Intent(context, WebPagesOpenActivity::class.java).putExtra(
                "help",
                "help"
            )
        )
    }

    override fun limitExceed(modelHomeDataSubscriptions: ModelHomeDataSubscriptions) {
        val auth = Constant.getPrefs(activity!!).getString(Constant.auth_code, "")
        mViewModel.getDeclinedApi(auth)
        startActivity(
            Intent(
                context,
                MerchantSubscriptionViewActivity::class.java
            ).putExtra("modelSubscriptionView", modelHomeDataSubscriptions.campaign_id)
        )

    }

    private fun RunningApi(latitude: Double, longitude: Double) {
        try {
            val prefs = Constant.getPrefs(context!!)
            val json = prefs.getString(Constant.dataFeatues, "")
            val json1 = prefs.getString(Constant.dataLatestSubs, "")
            val json2 = prefs.getString(Constant.categories, "")
            val auth = Constant.getPrefs(activity!!).getString(Constant.auth_code, "")
            val mPrefs = context?.getSharedPreferences("data", AppCompatActivity.MODE_PRIVATE)
            val gson = Gson()
            val dataa = mPrefs?.getString("categoryData", "")
            if (dataa!!.isEmpty()) {
                mViewModel.getCategories(auth)
            }
//            homeWhatsNewOneAdapter.update(backgroundChange)


            if (json.isEmpty() && json1.isEmpty() && json2.isEmpty()) {
                includeLayoutProgress.visibility = View.VISIBLE
            } else {
                try {
                    includeLayoutProgress.visibility = View.GONE
                    arrayHomeSubscription = Constant.mDataHome.subscriptions
                    val arrayHomeFeature = getArrayList(Constant.dataFeatues)
                    homeTopBrandAdapter.update(arrayHomeFeature)
                    homeWhatsNewTwoAdapter.update(Constant.mDataHome.subscriptions)
                    homeWhatsNewTwoAdapter.update(arrayHomeSubscription)
                    backgroundChange = Constant.getCategories(
                        activity!!,
                        Constant.categories
                    ) as ArrayList<WhatsNewModelOne>
                    homeWhatsNewOneAdapter.update(backgroundChange)
                } catch (e: Exception) {
                    e.printStackTrace()
                }


            }

            val data = mPrefs.getString("filterData", "")
            var model = ModelData()
            if (data != "") {
                model = gson.fromJson(data, ModelData::class.java)
            }

            val tagName = mPrefs.getString("categoryData", "")
            if (tagName != "") {
                val modelTags = gson.fromJson(tagName, WhatsNewModelOne::class.java)
                for (items in 0 until backgroundChange.size) {
                    backgroundChange[items].background = backgroundChange[items].id == modelTags.id
                }
                homeWhatsNewOneAdapter.update(backgroundChange)
                mViewModel.getHomeData(
                    modelTags.name,
                    "minnesota",
                    jObjectSort(DashboardActivity.sortName),
                    jObjectFilter(DashboardActivity.checkedItems, model, currentLatLng),
                    type
                )

            } else {
                mViewModel.getHomeData(
                    "",
                    "minnesota",
                    jObjectSort(DashboardActivity.sortName),
                    jObjectFilter(DashboardActivity.checkedItems, model, currentLatLng),
                    type
                )
            }
            progressBar25.visibility = View.VISIBLE

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun observersInit() {

        mViewModel.getmDataHomeSubscription().observe(activity!!, Observer {
            if (it.status == "true") {
                arrayHomeSubscription[position1] = it
                val editorFeatues = Constant.getPrefs(activity!!).edit()
                editorFeatues.putString(Constant.dataLatestSubs, arrayHomeSubscription.toString())
                editorFeatues.apply()
                homeWhatsNewTwoAdapter.update(arrayHomeSubscription)
            }
        })

        mViewModel.getmDataCategories().observe(this, Observer {
            includeLayoutProgress.visibility = View.GONE
            progressBar25.visibility = View.GONE
            backgroundChange = Constant.getCategories(
                activity!!,
                Constant.categories
            ) as ArrayList<WhatsNewModelOne>
            homeWhatsNewOneAdapter.update(backgroundChange)

        })

        mViewModel.getHomeDetail().observe(this, Observer {
            if (it.status == "true") {
                progressBar25.visibility = View.GONE
                Constant.mDataHome = it
                includeLayoutProgress.visibility = View.GONE
                arrayHomeSubscription.clear()
                arrayHomeSubscription.addAll(it.subscriptions)
                homeWhatsNewTwoAdapter.update(it.subscriptions)
                arrayListOfMapAdapter = it.subscriptions
                textTrending.visibility = View.VISIBLE
                textView24.visibility = View.VISIBLE
                relativeView.visibility = View.VISIBLE

                getHomeAllResponse()
                mViewModel.getRetrieveCards()
                if (it.arrayInvited.size > 0) {
                    linearViewPager.visibility = View.GONE
//                    it.arrayInvited.reverse()
                    myCustomPagerAdapter = PagerAdapterCards(activity!!, this, it.arrayInvited)
                    viewPager.adapter = myCustomPagerAdapter
                    viewPager.setPageTransformer(true, ZoomOutPageTransformer())
                } else {
                    linearViewPager.visibility = View.GONE
                }

            } else {
                val editor = Constant.getPrefs(context!!).edit()
                val editor1 = Constant.getSharedPrefs(context!!).edit()
                editor.clear()
                editor1.clear()
                editor.apply()
                editor1.apply()
                val intent = Intent(context, LoginorSignUpActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
            }

        })

        mViewModel.getmDataStatus().observe(this, Observer {
            progressBar25.visibility = View.GONE
            if (it.msg == "Invalid auth code") {
                textTrending.visibility = View.GONE
                textView24.visibility = View.GONE
                relativeView.visibility = View.GONE
                Constant.commonAlert(activity!!)
            } else {
                Toast.makeText(activity, it.msg, Toast.LENGTH_SHORT).show()
            }
        })

        mViewModel.getmDataDeclined().observe(this, Observer {
            if (it.status == "true") {
                linearViewPager.visibility = View.GONE
            }
        })

        mViewModel.getmDataNotification().observe(this, Observer {
            startActivity(
                Intent(activity!!, LinkingRequestActivity::class.java).putExtra(
                    "individualList",
                    "other"
                ).putExtra("fromNotification", "fromNotification").putExtra("model", it)
            )
        })
    }

    private fun clickListener() {
        if (Constant.list.isEmpty()) {
            textView319.text = "Newest First"
        } else {
            textView319.text = Constant.list
        }
        btnFilter.setOnClickListener {
            update.openFilter(1)
        }

        floatingButton.setOnClickListener {
            startActivity(
                Intent(context, MapsSearchActivity::class.java).putExtra(
                    "homeData",
                    arrayListOfMapAdapter
                )
            )
        }

        textView319.setOnClickListener {
            val choices = getListFirst()
            val charSequenceItems = choices.toArray(arrayOfNulls<CharSequence>(choices.size))
            val mBuilder = AlertDialog.Builder(activity!!)
            mBuilder.setSingleChoiceItems(charSequenceItems, -1) { dialogInterface, i ->
                Constant.list = choices[i]
                if (i == 0) {
                    type = 2
                } else {
                    type = 1
                }
                textView319.text = Constant.list
                dialogInterface.dismiss()
                currentLocation()
            }
            val mDialog = mBuilder.create()
            mDialog.show()
        }

        dismissText.setOnClickListener {
            val auth = Constant.getPrefs(activity!!).getString(Constant.auth_code, "")
            mViewModel.getDeclinedApi(auth)
        }

        swipeRefresh.setOnRefreshListener {
            val mPrefs = context?.getSharedPreferences("data", AppCompatActivity.MODE_PRIVATE)
            val gson = Gson()
            val data = mPrefs?.getString("categoryData", "")
            val dataFilter = mPrefs?.getString("filterData", "")
            var modelFilter = ModelData()
            if (dataFilter != "") {
                modelFilter = gson.fromJson(data, ModelData::class.java)
            }

            if (data != "") {
                val model = gson.fromJson(data, WhatsNewModelOne::class.java)
                for (items in 0 until backgroundChange.size) {
                    backgroundChange[items].background = backgroundChange[items].id == model.id
                }
                homeWhatsNewOneAdapter.update(backgroundChange)
                progressBar25.visibility = View.VISIBLE
                mViewModel.getHomeData(
                    model.name,
                    "minnesota",
                    jObjectSort(DashboardActivity.sortName),
                    jObjectFilter(DashboardActivity.checkedItems, modelFilter, currentLatLng),
                    type
                )
            } else {
                currentLocation()

            }

            swipeRefresh.isRefreshing = false

        }


    }


    private fun getListFirst(): ArrayList<String> {
        val arrayList: ArrayList<String> = ArrayList()

        for (i in 0 until 2) {
            if (i == 0) {
                arrayList.add("Newest First")
            } else {

                arrayList.add("Oldest First")
            }
        }
        return arrayList
    }


    private fun setLayout() {

        // Top Brands adapter //
        mRvTopBrands.layoutManager =
            LinearLayoutManager(
                this.activity,
                RecyclerView.HORIZONTAL,
                false
            ) as RecyclerView.LayoutManager?
        mRvTopBrands.isNestedScrollingEnabled = false
        LinearLayoutManager(this.activity, RecyclerView.HORIZONTAL, false)
        homeTopBrandAdapter = HomeTopBrandAdapter(this.activity!!)
        mRvTopBrands.adapter = homeTopBrandAdapter


        // categories Adapter //
        mRvWhatsNewOne.layoutManager =
            LinearLayoutManager(this.activity!!, RecyclerView.HORIZONTAL, false)
        mRvWhatsNewOne.isNestedScrollingEnabled = false
        homeWhatsNewOneAdapter = HomeWhatsNewOneAdapter(this.activity!!, this)
        mRvWhatsNewOne.adapter = homeWhatsNewOneAdapter


        // subscriptions adapter //
        mRvWhatsNewTwo.layoutManager = LinearLayoutManager(activity!!, RecyclerView.VERTICAL, false)
        homeWhatsNewTwoAdapter = HomeWhatsNewTwoAdapter(this.activity!!, this)
        mRvWhatsNewTwo.isNestedScrollingEnabled = false
        mRvWhatsNewTwo.adapter = homeWhatsNewTwoAdapter
    }

    override fun ChangeBackground(position: Int) {
        val mPrefs = context?.getSharedPreferences("data", AppCompatActivity.MODE_PRIVATE)
        val gson = Gson()
        val data = mPrefs?.getString("filterData", "")
        var model = ModelData()
        if (data != "") {
            model = gson.fromJson(data, ModelData::class.java)
        }

        currentLocationOfUser()

        val whatsNewModelOne = backgroundChange[position]
        if (whatsNewModelOne.background) {
            val prefsEditor = mPrefs?.edit()
            prefsEditor?.putString("categoryData", "")
            prefsEditor?.apply()
            progressBar25.visibility = View.VISIBLE
            mViewModel.getHomeData(
                "",
                "minnesota",
                jObjectSort(DashboardActivity.sortName),
                jObjectFilter(DashboardActivity.checkedItems, model, currentLatLng),
                type
            )
        } else {
            val prefsEditor = mPrefs?.edit()
            val putCategory = gson.toJson(whatsNewModelOne)
            prefsEditor?.putString("categoryData", putCategory)
            prefsEditor?.apply()
            progressBar25.visibility = View.VISIBLE
            mViewModel.getHomeData(
                whatsNewModelOne.name,
                "minnesota",
                jObjectSort(DashboardActivity.sortName),
                jObjectFilter(DashboardActivity.checkedItems, model, currentLatLng),
                type
            )

        }

        if (whatsNewModelOne.background) {
            whatsNewModelOne.background = false
            mRvWhatsNewOne.scrollToPosition(0)
        } else {
            for (notSelected in backgroundChange) {
                notSelected.background = false
            }
            whatsNewModelOne.background = true
        }
        backgroundChange[position] = whatsNewModelOne
        homeWhatsNewOneAdapter.notifyDataSetChanged()
    }


    override fun addToFav(position: Int) {
        position1 = position
        val auth = Constant.getPrefs(activity!!).getString(Constant.auth_code, "")
        mViewModel.setFavourite(auth, arrayHomeSubscription[position])

    }

    override fun onResume() {
        super.onResume()
        homeWhatsNewTwoAdapter.notifyDataSetChanged()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        update = context as FilterHomeInterface
        updateCard = context as InterfaceSwitch
        updateClick = context as GetSatertedClick
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        update = activity as FilterHomeInterface
        updateCard = activity as InterfaceSwitch
        updateClick = activity as GetSatertedClick
        listener = activity as apiData

    }

    fun getArrayList(key: String): ArrayList<ModelFeaturedBusiness> {
        val prefs = Constant.getPrefs(activity!!)
        val gson = Gson()
        val json = prefs.getString(key, null)
        val type = gson.fromJson<ArrayList<ModelFeaturedBusiness>>(
            json,
            object : TypeToken<List<ModelFeaturedBusiness>>() {

            }.type
        )
        return type
    }


    fun getHomeAllResponse() {
        val arrayHomeFeature = getArrayList(Constant.dataFeatues)
        val arrayProfile = Constant.getArrayListProfile(activity!!,Constant.dataProfile)
        homeTopBrandAdapter.update(arrayHomeFeature)
        var banners = 0
        banners = if (arrayProfile.steps == "6" && arrayProfile.type == "1"){
            1
        }else{
            0
        }


        includeLayoutProgress.visibility = View.GONE
        if (arrayHomeSubscription.size > 1) {
            if (arrayProfile.steps != "6"){
                banners++
                val model = ModelHomeDataSubscriptions()
                model.subscription_id = "-1"
                arrayHomeSubscription.add(1, model)
            }

        }
        if (arrayHomeSubscription.size > 5) {
            banners++
            val model = ModelHomeDataSubscriptions()
            model.subscription_id = "-2"
            arrayHomeSubscription.add(5, model)
        }

        if (arrayHomeSubscription.size > 9) {
            banners++
            val model = ModelHomeDataSubscriptions()
            model.subscription_id = "-3"
            arrayHomeSubscription.add(9, model)
        }
        when (banners) {
            0 -> {
                var model = ModelHomeDataSubscriptions()
                model.subscription_id = "-1"
                arrayHomeSubscription.add(model)

                model = ModelHomeDataSubscriptions()
                model.subscription_id = "-2"
                arrayHomeSubscription.add(model)

                model = ModelHomeDataSubscriptions()
                model.subscription_id = "-3"
                arrayHomeSubscription.add(model)
            }
            1 -> {
                var model = ModelHomeDataSubscriptions()
                model.subscription_id = "-2"
                arrayHomeSubscription.add(model)

                model = ModelHomeDataSubscriptions()
                model.subscription_id = "-3"
                arrayHomeSubscription.add(model)
            }
            2 -> {
                val model = ModelHomeDataSubscriptions()
                model.subscription_id = "-3"
                arrayHomeSubscription.add(model)
            }
        }



        homeWhatsNewTwoAdapter.update(arrayHomeSubscription)
        listener.sendData(arrayHomeSubscription)
    }


    private fun jObjectFilter(
        checkedItems: ArrayList<ModelSubCategory>,
        modelData: ModelData,
        currentLatLng: LatLng
    ): JSONObject {

        var jArrayCategory = ""
        val selectedCategory = ArrayList<String>()
        if (checkedItems.size > 0) {
            for (i in 0 until checkedItems.size) {
                selectedCategory.add(checkedItems[i].id)
            }
            jArrayCategory = TextUtils.join(",", selectedCategory)
        }

        if (modelData.latitude != "" && modelData.longitude != "") {
            val jsonObject = JSONObject()
            jsonObject.put("tags", jArrayCategory)
            jsonObject.put("business_id", DashboardActivity.businessId)
            jsonObject.put("min_price", modelData.priceMin)
            jsonObject.put("max_price", modelData.priceMax)
            jsonObject.put("min_distance", modelData.distanceMin)
            jsonObject.put("max_distance", modelData.distanceMax)
            jsonObject.put("lat", modelData.latitude)
            jsonObject.put("long", modelData.longitude)
            return jsonObject
        } else {
            val jsonObject = JSONObject()
            jsonObject.put("tags", jArrayCategory)
            jsonObject.put("business_id", DashboardActivity.businessId)
            jsonObject.put("min_price", modelData.priceMin)
            jsonObject.put("max_price", modelData.priceMax)
            jsonObject.put("min_distance", modelData.distanceMin)
            jsonObject.put("max_distance", modelData.distanceMax)
            jsonObject.put("lat", currentLatLng.latitude)
            jsonObject.put("long", currentLatLng.longitude)
            return jsonObject
        }


    }


    private fun jObjectSort(sortBy: String): JSONObject {
        val jsonObject = JSONObject()
        when (sortBy) {
            "Price Low to High" -> {
                jsonObject.put("price", "1")
            }
            "Price High to Low" -> {
                jsonObject.put("price", "2")
            }
            "Distance" -> {
                jsonObject.put("distance", "1")
            }
            "Rating" -> {
                jsonObject.put("rating", "2")
            }

            else -> {
                jsonObject.put("", "")
            }
        }
        return jsonObject

    }


    fun runApi(context: Context) {
        val mPrefs = context.getSharedPreferences("data", AppCompatActivity.MODE_PRIVATE)
        val gson = Gson()
        val data = mPrefs.getString("filterData", "")
        var model = ModelData()
        if (data != "") {
            model = gson.fromJson(data, ModelData::class.java)
        }
        fusedLocationClient.lastLocation.addOnSuccessListener(activity!!) { location ->
            // Got last known location. In some rare situations this can be null.
            if (location != null) {
                currentLatLng = LatLng(location.latitude, location.longitude)
                progressBar25.visibility = View.VISIBLE
                val tagName = mPrefs?.getString("categoryData", "")
                var modelTags = WhatsNewModelOne()
                if (tagName != "") {
                    modelTags = gson.fromJson(tagName, WhatsNewModelOne::class.java)
                }
                mViewModel.getHomeData(
                    modelTags.name,
                    "minnesota",
                    jObjectSort(DashboardActivity.sortName),
                    jObjectFilter(DashboardActivity.checkedItems, model, currentLatLng),
                    type
                )
            }
        }
    }

    fun runApiFirst(context: Context) {
        currentLocation()
    }


    interface apiData {
        fun sendData(homeModelMain: ArrayList<ModelHomeDataSubscriptions>)
    }


}