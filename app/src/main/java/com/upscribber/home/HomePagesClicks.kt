package com.upscribber.home

interface HomePagesClicks {

    fun clickInvitation(mData: ModelHomeDataSubscriptions)
    fun clickJoin(mData: ModelHomeDataSubscriptions)
    fun clickJoinTeam(mData: ModelHomeDataSubscriptions)
    fun rateMerchant(mData: ModelHomeDataSubscriptions)
    fun clickDeclinePayments(mData: ArrayList<ModelHomeDataSubscriptions>)
    fun addCard(modelHomeDataSubscriptions: ModelHomeDataSubscriptions)
    fun subscriptionDetails(modelHomeDataSubscriptions: ModelHomeDataSubscriptions)
    fun orderSubscription(modelHomeDataSubscriptions: ModelHomeDataSubscriptions)
    fun unsharedSubscription(modelHomeDataSubscriptions: ModelHomeDataSubscriptions)
    fun learnFaq(modelHomeDataSubscriptions: ModelHomeDataSubscriptions)
    fun limitExceed(modelHomeDataSubscriptions: ModelHomeDataSubscriptions)
}