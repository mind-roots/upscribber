package com.upscribber.home

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.upscribber.subsciptions.merchantStore.merchantDetails.MerchantStoreActivity
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import kotlinx.android.synthetic.main.home_top_brand_recycler.view.*

@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class HomeTopBrandAdapter(val mContext: Context) : RecyclerView.Adapter<HomeTopBrandAdapter.MyViewHolder>() {

     var list: ArrayList<ModelFeaturedBusiness> = ArrayList()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(mContext)
            .inflate(R.layout.home_top_brand_recycler, parent, false)
        return MyViewHolder(v)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = list[position]

        val imagePath = Constant.getPrefs(mContext).getString(Constant.dataImagePath, "http://cloudart.com.au/projects/upscribbr/uploads/")

        Glide.with(mContext).load(imagePath+model.cover_image).placeholder(R.mipmap.placeholder_subscription).into(holder.itemView.iv_background)
        Glide.with(mContext).load(imagePath + model.logo).placeholder(R.mipmap.app_icon).into(holder.itemView.iv_logo)
        holder.itemView.tv_logo_text.text = model.business_name

        holder.itemView.setOnClickListener {
            mContext.startActivity(Intent(mContext, MerchantStoreActivity::class.java).putExtra("model",model.id))
        }
    }

    fun update(it: ArrayList<ModelFeaturedBusiness>) {
        this.list = it
        notifyDataSetChanged()

    }


    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

}
