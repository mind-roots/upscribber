package com.upscribber.home

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by Loveleen on 5/3/19.
 */
class WhatsNewModelOne() : Parcelable{

    var id: String = ""
    var unselected_image: String = ""
    var msg: String = ""
    var status: String = ""
    var selected_image: String = ""
    var subscription_count: String = ""
    var name: String = ""



    var background : Boolean = false

    constructor(parcel: Parcel) : this() {
        id = parcel.readString()
        unselected_image = parcel.readString()
        msg = parcel.readString()
        status = parcel.readString()
        selected_image = parcel.readString()
        subscription_count = parcel.readString()
        name = parcel.readString()
        background = parcel.readByte() != 0.toByte()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(unselected_image)
        parcel.writeString(msg)
        parcel.writeString(status)
        parcel.writeString(selected_image)
        parcel.writeString(subscription_count)
        parcel.writeString(name)
        parcel.writeByte(if (background) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<WhatsNewModelOne> {
        override fun createFromParcel(parcel: Parcel): WhatsNewModelOne {
            return WhatsNewModelOne(parcel)
        }

        override fun newArray(size: Int): Array<WhatsNewModelOne?> {
            return arrayOfNulls(size)
        }
    }
}