package com.upscribber.home

import android.os.Parcel
import android.os.Parcelable
import org.json.JSONArray
import java.util.ArrayList
import com.upscribber.commonClasses.ModelStatusMsg as ModelStatusMsg1

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class ModelHomeDataSubscriptions() : Parcelable {

    var name: String = ""
    var c_type: String = ""
    var images: ArrayList<ModelStatusMsg1> = ArrayList()
    //    var locations: ArrayList<ModelHomeLocation> = ArrayList()
    var campaign_start: String = ""
    var website: String = ""
    var tags: String = ""
    var redeem_id: String = ""
    var team_member: String = ""
    var business_phone: String = ""
    var logo: String = ""
    var remaining_visits: String = ""
    var profile_image: String = ""
    var contact_no: String = ""
    var customer_name: String = ""
    var total_amount: String = ""
    var customer_id: String = ""
    var assign_to: String = ""
    var order_id: String = ""
    var introductory_days: String = ""
    var redeemtion_cycle_qty: String = ""
    var remaining_subscriber_limit: String = ""
    var introductory_price: String = ""
    var request_type: String = ""
    var typeCheck: String = ""
    var business_id: String = ""
    var business_name: String = ""
    var id: String = ""
    var campaign_image: String = ""
    var unit: String = ""
    var created_at: String = ""
    var frequency_type: String = ""
    var frequency_value: String = ""
    var free_trial: String = ""
    var updated_at: String = ""
    var status: String = ""
    var subscription_id: String = ""
    var campaign_id: String = ""
    var price: String = ""
    var discount_price: String = ""
    var location_id: String = ""
    var subscriber: String = ""
    var description: String = ""
    var campaign_name: String = ""
    var business_logo: String = ""
    var like: Int = 0
    var like_percentage: String = ""
    var msg: String = ""
    var type: Int = 0
    var location: String = ""
    var free_trial_qty: String = ""
    var views: String = ""

    var transaction_id: String = ""
    var transaction_status: String = ""
    var transaction_type: String = ""
    var card_id: String = ""
    var merchant_id: String = ""
    var feature_image: String = ""
    var order_status: String = ""
    var is_refunded: String = ""
    var types: String = ""

    constructor(parcel: Parcel) : this() {
        name = parcel.readString()
        c_type = parcel.readString()
        images = parcel.createTypedArrayList(ModelStatusMsg1.CREATOR)
        campaign_start = parcel.readString()
        website = parcel.readString()
        tags = parcel.readString()
        redeem_id = parcel.readString()
        team_member = parcel.readString()
        business_phone = parcel.readString()
        logo = parcel.readString()
        remaining_visits = parcel.readString()
        profile_image = parcel.readString()
        contact_no = parcel.readString()
        customer_name = parcel.readString()
        total_amount = parcel.readString()
        customer_id = parcel.readString()
        assign_to = parcel.readString()
        order_id = parcel.readString()
        introductory_days = parcel.readString()
        redeemtion_cycle_qty = parcel.readString()
        remaining_subscriber_limit = parcel.readString()
        introductory_price = parcel.readString()
        request_type = parcel.readString()
        typeCheck = parcel.readString()
        business_id = parcel.readString()
        business_name = parcel.readString()
        id = parcel.readString()
        campaign_image = parcel.readString()
        unit = parcel.readString()
        created_at = parcel.readString()
        frequency_type = parcel.readString()
        frequency_value = parcel.readString()
        free_trial = parcel.readString()
        updated_at = parcel.readString()
        status = parcel.readString()
        subscription_id = parcel.readString()
        campaign_id = parcel.readString()
        price = parcel.readString()
        discount_price = parcel.readString()
        location_id = parcel.readString()
        subscriber = parcel.readString()
        description = parcel.readString()
        campaign_name = parcel.readString()
        business_logo = parcel.readString()
        like = parcel.readInt()
        like_percentage = parcel.readString()
        msg = parcel.readString()
        type = parcel.readInt()
        location = parcel.readString()
        free_trial_qty = parcel.readString()
        views = parcel.readString()
        transaction_id = parcel.readString()
        transaction_status = parcel.readString()
        transaction_type = parcel.readString()
        card_id = parcel.readString()
        merchant_id = parcel.readString()
        feature_image = parcel.readString()
        order_status = parcel.readString()
        is_refunded = parcel.readString()
        types = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeString(c_type)
        parcel.writeTypedList(images)
        parcel.writeString(campaign_start)
        parcel.writeString(website)
        parcel.writeString(tags)
        parcel.writeString(redeem_id)
        parcel.writeString(team_member)
        parcel.writeString(business_phone)
        parcel.writeString(logo)
        parcel.writeString(remaining_visits)
        parcel.writeString(profile_image)
        parcel.writeString(contact_no)
        parcel.writeString(customer_name)
        parcel.writeString(total_amount)
        parcel.writeString(customer_id)
        parcel.writeString(assign_to)
        parcel.writeString(order_id)
        parcel.writeString(introductory_days)
        parcel.writeString(redeemtion_cycle_qty)
        parcel.writeString(remaining_subscriber_limit)
        parcel.writeString(introductory_price)
        parcel.writeString(request_type)
        parcel.writeString(typeCheck)
        parcel.writeString(business_id)
        parcel.writeString(business_name)
        parcel.writeString(id)
        parcel.writeString(campaign_image)
        parcel.writeString(unit)
        parcel.writeString(created_at)
        parcel.writeString(frequency_type)
        parcel.writeString(frequency_value)
        parcel.writeString(free_trial)
        parcel.writeString(updated_at)
        parcel.writeString(status)
        parcel.writeString(subscription_id)
        parcel.writeString(campaign_id)
        parcel.writeString(price)
        parcel.writeString(discount_price)
        parcel.writeString(location_id)
        parcel.writeString(subscriber)
        parcel.writeString(description)
        parcel.writeString(campaign_name)
        parcel.writeString(business_logo)
        parcel.writeInt(like)
        parcel.writeString(like_percentage)
        parcel.writeString(msg)
        parcel.writeInt(type)
        parcel.writeString(location)
        parcel.writeString(free_trial_qty)
        parcel.writeString(views)
        parcel.writeString(transaction_id)
        parcel.writeString(transaction_status)
        parcel.writeString(transaction_type)
        parcel.writeString(card_id)
        parcel.writeString(merchant_id)
        parcel.writeString(feature_image)
        parcel.writeString(order_status)
        parcel.writeString(is_refunded)
        parcel.writeString(types)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ModelHomeDataSubscriptions> {
        override fun createFromParcel(parcel: Parcel): ModelHomeDataSubscriptions {
            return ModelHomeDataSubscriptions(parcel)
        }

        override fun newArray(size: Int): Array<ModelHomeDataSubscriptions?> {
            return arrayOfNulls(size)
        }
    }


}
