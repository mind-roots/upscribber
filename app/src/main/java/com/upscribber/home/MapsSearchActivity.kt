package com.upscribber.home

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Html
import android.text.Spannable
import android.text.SpannableString
import android.text.TextUtils
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.widget.SearchView
import androidx.cardview.widget.CardView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.discover.DiscoverFirstAdapter
import com.upscribber.filters.ModelData
import com.upscribber.filters.ModelSubCategory
import com.upscribber.filters.*
import com.upscribber.filters.Redemption.RedemptionFilterFragment
import com.upscribber.requestbusiness.*
import com.upscribber.subsciptions.merchantStore.merchantDetails.MerchantStoreActivity
import com.upscribber.subsciptions.merchantSubscriptionPages.MerchantSubscriptionViewActivity
import com.upscribber.upscribberSeller.subsriptionSeller.location.searchLoc.ModelSearchLocation
import com.upscribber.upscribberSeller.subsriptionSeller.location.searchLoc.ModelStoreLocation
import kotlinx.android.synthetic.main.activity_maps_search.*
import kotlinx.android.synthetic.main.filter_header.*
import kotlinx.android.synthetic.main.marker_result_multipleview.*
import kotlinx.android.synthetic.main.request_business_sheet.*
import kotlinx.android.synthetic.main.request_business_sheet.recyclerRequest
import kotlinx.android.synthetic.main.search_one_result.*
import org.json.JSONArray
import org.json.JSONObject
import java.io.IOException

class MapsSearchActivity : AppCompatActivity(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener,
    BackGroundInterface, DrawerLayout.DrawerListener, FilterFragment.SetFilterHeaderBackground,
    FilterInterface, SortByAdapter.Selection, BrandAdapter.Selection, AdapterMaps.layoutClick {

    private lateinit var mAdapterImages: RecyclerViewMapImages
    private var modelData = ModelData()
    private lateinit var checkFragment: Fragment
    private lateinit var currentLatLng: LatLng
    private lateinit var setLocationData: ArrayList<ModelSearchLocation>
    private lateinit var discoverFirstAdapter: DiscoverFirstAdapter
    private var backgroundChange: ArrayList<WhatsNewModelOne> = ArrayList()
    private var selectedModel: ModelRequest1? = null
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var mMap: GoogleMap
    private lateinit var lastLocation: Location
    private lateinit var linearBack: LinearLayout
    private lateinit var mData: ArrayList<ModelRequest1>
    private lateinit var mAdapter: AdapterMaps
    private lateinit var bottom_sheet: RelativeLayout
    private lateinit var cardInvite: CardView
    private lateinit var showHideBottomSheet: RelativeLayout
    private lateinit var line: TextView
    private lateinit var tv_earn: TextView
    private lateinit var found: TextView
    private lateinit var downImage: ImageView
    private var bottomSheetBehavior: BottomSheetBehavior<RelativeLayout>? = null
    var shofull = 0
    lateinit var brandSearch: SearchView
    lateinit var recyclerCategories: RecyclerView
    private var arrayHomeSubscription = ArrayList<ModelHomeDataSubscriptions>()
    private var arrayHomeSubscriptionBackup = ArrayList<ModelHomeDataSubscriptions>()
    lateinit var mViewModel: HomeViewModel
    private lateinit var mfilters: TextView
    var drawerOpen = 0
    private lateinit var btnFilter: TextView
    private lateinit var filter_done: TextView
    private lateinit var fragmentManager: FragmentManager
    private lateinit var fragmentTransaction: FragmentTransaction
    private lateinit var mreset: TextView
    private var imagePath = ""

    companion object {
        private const val LOCATION_PERMISSION_REQUEST_CODE = 1
        var businessId = ""
        var businessNames = "All"
        var sortName = "No Filter Selected"
        var checkedItems = ArrayList<ModelSubCategory>()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps_search)
        initz()
        clickListeners()
        setSheet()
        setAdapter()
        setFilters()
        apiImplimentation()
        observerInit()

        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        drawerLayout1.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, GravityCompat.END)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        spannableText()
        if (intent.hasExtra("homeData")) {
            arrayHomeSubscription = intent.getParcelableArrayListExtra("homeData")
            val arrayHomeSubscription1 = ArrayList<ModelHomeDataSubscriptions>()
            for (items in arrayHomeSubscription) {
                if (items.campaign_id != "") {
                    arrayHomeSubscription1.add(items)
                }
            }
            arrayHomeSubscription.clear()
            arrayHomeSubscription.addAll(arrayHomeSubscription1)
            mAdapter.update(arrayHomeSubscription)
            arrayHomeSubscriptionBackup.addAll(arrayHomeSubscription)
        }
    }

    private fun checkPrefilledCategory() {
        val mPrefs = getSharedPreferences("data", MODE_PRIVATE)
        val gson = Gson()
        val data = mPrefs?.getString("categoryData", "")
        if (data != "") {
            val model = gson.fromJson(data, WhatsNewModelOne::class.java)
            for (items in 0 until backgroundChange.size) {
                backgroundChange[items].background = backgroundChange[items].id == model.id
            }
            discoverFirstAdapter.update(backgroundChange)
        } else {
//            if (backgroundChange.size > 0) {
//                backgroundChange[0].background = true
//            }
            fusedLocationClient.lastLocation.addOnSuccessListener(this) { location ->
                // Got last known location. In some rare situations this can be null.cu
                if (location != null) {
                    lastLocation = location
                    currentLatLng = LatLng(location.latitude, location.longitude)
                    val modelData = ModelData()
                    placeMarkerOnMap(currentLatLng, "")
                    progressBar.visibility = View.VISIBLE
                    mViewModel.getHomeData(
                        "",
                        "minnesota",
                        jObjectSort(""),
                        jObjectFilter(checkedItems, modelData, currentLatLng),
                        0
                    )
                }

            }
        }
    }

    private fun setFilters() {
        btnFilter.setOnClickListener {
            drawerOpen = 1
            drawerLayout1.openDrawer(GravityCompat.END)
            inflateFragment(FilterFragment(this))
            filterImgBack.visibility = View.GONE
            mfilters.text = "Filters"
            mreset.visibility = View.VISIBLE
        }

        filter_done.setOnClickListener {
            if (checkFragment is FilterFragment) {
                modelData = (checkFragment as FilterFragment).getData()
                val mPrefs = getSharedPreferences("data", MODE_PRIVATE)
                val prefsEditor = mPrefs.edit()
                val gson = Gson()
                val json = gson.toJson(modelData)
                prefsEditor.putString("filterDataMaps", json)
                prefsEditor.apply()
                runApi()
                onBackPressed()
            }
        }

        reset.setOnClickListener {
            businessId = ""
            businessNames = "All"
            sortName = "No Filter Selected"
            checkedItems.clear()
            val mPrefs = getSharedPreferences("data", MODE_PRIVATE)
            val prefsEditor = mPrefs.edit()
            prefsEditor.putString("filterDataMaps", "")
            prefsEditor.apply()
            runApi()
            runApi()
            onBackPressed()
        }

    }

    private fun inflateFragment(fragment: Fragment) {
        checkFragment = fragment
        fragmentManager = supportFragmentManager
        this.fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.containerSide1, fragment)
        fragmentTransaction.commit()
    }


    override fun onBackPressed() {
        if (drawerOpen == 1) {
            drawerOpen = 0
            drawerLayout1.closeDrawer(GravityCompat.END)
        } else {
            sortName = ""
            businessId = ""
            businessNames = "All"
            val mPrefs = getSharedPreferences("data", MODE_PRIVATE)
            val prefsEditor = mPrefs.edit()
            prefsEditor.putString("filterDataMaps", "")
            prefsEditor.apply()

            super.onBackPressed()
        }
    }

    private fun observerInit() {
        mViewModel.getmDataCategories().observe(this, Observer {
            progressBar.visibility = View.GONE
            backgroundChange = Constant.getCategories(
                this,
                Constant.categories
            ) as ArrayList<WhatsNewModelOne>
            discoverFirstAdapter.update(backgroundChange)
            checkPrefilledCategory()
        })

        mViewModel.getHomeDetail().observe(this, Observer {
            progressBar.visibility = View.GONE
            if (it.status == "true") {
                arrayHomeSubscription.clear()
                if (it.subscriptions.size > 0) {
                    recyclerRequest.visibility = View.VISIBLE
                    noDataFound.visibility = View.GONE
                    arrayHomeSubscription.addAll(it.subscriptions)
                    recyclerRequest.layoutManager = LinearLayoutManager(this)
                    mAdapter = AdapterMaps(this, this, 1)
                    recyclerRequest.isNestedScrollingEnabled = false
                    recyclerRequest.adapter = mAdapter
                    mAdapter.update(it.subscriptions)
                    mMap.clear()
                    setUpMap()

                } else {
                    arrayHomeSubscription.clear()
                    mMap.clear()
                    setUpMap()
                    recyclerRequest.visibility = View.GONE
                    noDataFound.visibility = View.VISIBLE
                }

            } else {
                arrayHomeSubscription.clear()
                mMap.clear()
                setUpMap()
                recyclerRequest.visibility = View.GONE
                noDataFound.visibility = View.VISIBLE
            }
        })

    }


    private fun apiImplimentation() {
        val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
        mViewModel.getCategories(auth)
        progressBar.visibility = View.VISIBLE

    }

    private fun spannableText() {
        val spannableString =
            SpannableString("Get 20% off.Invite a Friend Now")

        spannableString.setSpan(
            ForegroundColorSpan(Color.BLACK),
            12, 31,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        tv_earn.text = spannableString

    }

    private fun setSheet() {
        bottomSheetBehavior = BottomSheetBehavior.from(bottom_sheet)
        showHideBottomSheet.setOnClickListener {
            if (shofull == 0) {
                (bottomSheetBehavior as BottomSheetBehavior<LinearLayout>?)!!.state =
                    BottomSheetBehavior.STATE_EXPANDED
                line.visibility = View.GONE
                downImage.visibility = View.VISIBLE
                shofull = 1
            } else {
                shofull = 0
                line.visibility = View.VISIBLE
                downImage.visibility = View.GONE
                (bottomSheetBehavior as BottomSheetBehavior<LinearLayout>?)!!.state =
                    BottomSheetBehavior.STATE_COLLAPSED
            }
        }
        (bottomSheetBehavior as BottomSheetBehavior<LinearLayout>?)!!.setBottomSheetCallback(
            object :
                BottomSheetBehavior.BottomSheetCallback() {
                override fun onStateChanged(bottomSheet: View, newState: Int) {
                    // React to state change
                    when (newState) {
                        BottomSheetBehavior.STATE_COLLAPSED -> {
                            cardInvite.visibility = View.VISIBLE
                            line.visibility = View.VISIBLE
                            downImage.visibility = View.GONE
                            shofull = 0
                        }

                        BottomSheetBehavior.STATE_DRAGGING -> {
                            cardInvite.visibility = View.GONE
                        }

                        BottomSheetBehavior.STATE_EXPANDED -> {
                            shofull = 1
                            line.visibility = View.GONE
                            cardInvite.visibility = View.GONE
                            downImage.visibility = View.VISIBLE
                        }

                        BottomSheetBehavior.STATE_HIDDEN -> {

                        }
                        BottomSheetBehavior.STATE_SETTLING -> {
                        }


                    }
                }

                override fun onSlide(bottomSheet: View, slideOffset: Float) {
                    // React to dragging events
                    Log.e("onSlide", "onSlide")
                }
            })

    }

    fun locationData(location: String): ArrayList<ModelSearchLocation> {
        val gson = Gson()
        /*val model: ArrayList<ModelSearchLocation> =
            gson.fromJson(
                location,
                object : TypeToken<ArrayList<ModelSearchLocation>>() {}.type
            )*/
        val locationDataArray = ArrayList<ModelSearchLocation>()
        if (location != "" && location.isNotEmpty()) {
            val loc = JSONArray(location)
            if (loc.length() > 0) {
                for (i in 0 until loc.length()) {
                    val obj = loc.optJSONObject(i)
                    val modelSearchLocation = ModelSearchLocation()
                    modelSearchLocation.id = obj.optString("id")
                    modelSearchLocation.business_id = obj.optString("business_id")
                    modelSearchLocation.user_id = obj.optString("user_id")
                    modelSearchLocation.street = obj.optString("street")
                    modelSearchLocation.city = obj.optString("city")
                    modelSearchLocation.state = obj.optString("state")
                    modelSearchLocation.zip_code = obj.optString("zip_code")
                    modelSearchLocation.lat = obj.optString("lat")
                    modelSearchLocation.long = obj.optString("long")
                    modelSearchLocation.suite = obj.optString("suite")
                    modelSearchLocation.created_at = obj.optString("created_at")
                    modelSearchLocation.updated_at = obj.optString("updated_at")
                    val storeTiming = obj.optString("store_timing")

                    if (storeTiming == "" || storeTiming == "[]") {
                        val storeModel = ModelStoreLocation()
                        storeModel.day = ""
                        storeModel.open = ""
                        storeModel.close = ""
                        storeModel.checked = ""
                        storeModel.status = ""
                        modelSearchLocation.store_timing.add(storeModel)
                    } else {
                        val model: ArrayList<ModelStoreLocation> =
                            gson.fromJson(
                                storeTiming,
                                object : TypeToken<ArrayList<ModelStoreLocation>>() {}.type
                            )
                        modelSearchLocation.store_timing = model

                        /*if (storeArray.length() > 0) {
                        for (j in 0 until storeArray.length()) {
                            val storeData = storeArray.optJSONObject(j)
                            val storeModel = ModelStoreLocation()
                            storeModel.day = storeData.optString("days")
                            storeModel.open = storeData.optString("open")
                            storeModel.close = storeData.optString("close")
                            storeModel.checked = storeData.optString("checked")
                            storeModel.status = storeData.optString("status")
                            modelSearchLocation.store_timing.add(storeModel)
                        }
                    }*/
                    }
                    locationDataArray.add(modelSearchLocation)
                }
            }
        }


        return locationDataArray
    }

    private fun clickListeners() {
        linearBack.setOnClickListener {
            sortName = ""
            businessId = ""
            businessNames = "All"
            checkedItems.clear()
            val mPrefs = getSharedPreferences("data", MODE_PRIVATE)
            val prefsEditor = mPrefs.edit()
            prefsEditor.putString("filterDataMaps", "")
            prefsEditor.apply()
            finish()
        }



        filter_done.setOnClickListener {
            onBackPressed()
        }

        cardInvite.setOnClickListener {
            startActivity(Intent(this, InviteFriendMerchantActivity::class.java))
        }

        imageBack.setOnClickListener {
            val place =
                "Found " + "<b>" + arrayHomeSubscription.size + "</b>" + " places near " + "<b>" + getAddress(
                    currentLatLng
                ) + "</b>"
            mAdapter.update(arrayHomeSubscription)

            found.text = Html.fromHtml(place)
            imageBack.visibility = View.GONE
            recyclerCategories.visibility = View.VISIBLE
        }
        crossView.setOnClickListener {
            recyclerCategories.visibility = View.VISIBLE
            recyclerRequest.visibility = View.VISIBLE
            found.visibility = View.VISIBLE
            linear.visibility = View.VISIBLE
            nestedFirst.visibility = View.VISIBLE
            searchMultiple.visibility = View.GONE
            searchOneResult.visibility = View.GONE

            val spannableString = SpannableString(
                "Found " + "<b>" + arrayHomeSubscription.size + "</b>" + " places near " + "<b>" + getAddress(
                    currentLatLng
                ) + "</b>"
            )
            spannableString.setSpan(
                ForegroundColorSpan(Color.RED),
                8, arrayHomeSubscription.size + 6,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            )


            val place = spannableString
            mAdapter.update(arrayHomeSubscription)

            found.text = Html.fromHtml(place.toString())
            imageBack.visibility = View.GONE
        }


        imageView86.setOnClickListener {
            recyclerCategories.visibility = View.VISIBLE
            recyclerRequest.visibility = View.VISIBLE
            searchMultiple.visibility = View.GONE
            searchOneResult.visibility = View.GONE
            found.visibility = View.VISIBLE
            linear.visibility = View.VISIBLE
            nestedFirst.visibility = View.VISIBLE

            fusedLocationClient.lastLocation.addOnSuccessListener(this) { location ->
                // Got last known location. In some rare situations this can be null.
                if (location != null) {
                    lastLocation = location
                    currentLatLng = LatLng(location.latitude, location.longitude)

                    val place =
                        "Found " + "<b>" + arrayHomeSubscription.size + "</b>" + " places near " + "<b>" + getAddress(
                            currentLatLng
                        ) + "</b>"
                    mAdapter.update(arrayHomeSubscription)
                    found.text = Html.fromHtml(place)

                }
            }



            imageBack.visibility = View.GONE
        }

        currentLocation.setOnClickListener {
            if (ActivityCompat.checkSelfPermission(
                    this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                    RequestBusinessFragment2.LOCATION_PERMISSION_REQUEST_CODE
                )

            } else {
                fusedLocationClient.lastLocation.addOnSuccessListener(this) { location ->
                    // Got last known location. In some rare situations this can be null.
                    if (location != null) {
                        lastLocation = location
                        currentLatLng = LatLng(location.latitude, location.longitude)
                        placeMarkerOnMap(currentLatLng, "")
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 14f))

                    }
                }
            }

        }


    }


    private fun setAdapter() {
        recyclerRequest.layoutManager = LinearLayoutManager(this)
        mAdapter = AdapterMaps(this, this, 1)
        recyclerRequest.isNestedScrollingEnabled = false
        recyclerRequest.adapter = mAdapter

        recyclerImages.layoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
        mAdapterImages = RecyclerViewMapImages(this)
        recyclerImages.adapter = mAdapterImages


        recyclerCategories.layoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
        discoverFirstAdapter = DiscoverFirstAdapter(this)
        recyclerCategories.adapter = discoverFirstAdapter

    }


    override fun ChangeBackground(position: Int) {
        val model = backgroundChange[position]
        val modelData = ModelData()
        if (model.background) {
            progressBar.visibility = View.VISIBLE
            model.background = false
            mViewModel.getHomeData(
                "",
                "minnesota",
                jObjectSort(""),
                jObjectFilter(checkedItems, modelData, currentLatLng),
                0
            )

        } else {
            progressBar.visibility = View.VISIBLE
            for (notSelected in backgroundChange) {
                notSelected.background = false
            }
            model.background = true
            mViewModel.getHomeData(
                model.name,
                "minnesota",
                jObjectSort(""),
                jObjectFilter(checkedItems, modelData, currentLatLng),
                0
            )

        }
        backgroundChange[position] = model
        discoverFirstAdapter.notifyDataSetChanged()

    }

    private fun jObjectSort(sortBy: String): JSONObject {
        val jsonObject = JSONObject()
        when (sortBy) {
            "Price Low to High" -> {
                jsonObject.put("price", "1")
            }
            "Price High to Low" -> {
                jsonObject.put("price", "2")
            }
            "Distance" -> {
                jsonObject.put("distance", "1")
            }
            "Rating" -> {
                jsonObject.put("rating", "2")
            }

            else -> {
                jsonObject.put("", "")
            }
        }
        return jsonObject

    }

    private fun jObjectFilter(
        checkedItems: ArrayList<ModelSubCategory>,
        modelData: ModelData,
        currentLatLng: LatLng
    ): JSONObject {
        try {
            var jArrayCategory = ""
            val selectedCategory = ArrayList<String>()
            if (checkedItems.size > 0) {
                for (i in 0 until checkedItems.size) {
                    selectedCategory.add(checkedItems[i].id)
                }
                jArrayCategory = TextUtils.join(",", selectedCategory)
            }

            if (modelData.latitude != "" && modelData.longitude != "") {
                val jsonObject = JSONObject()
                jsonObject.put("tags", jArrayCategory)
                jsonObject.put("business_id", "")
                jsonObject.put("min_price", modelData.priceMin)
                jsonObject.put("max_price", modelData.priceMax)
                jsonObject.put("min_distance", modelData.distanceMin)
                jsonObject.put("max_distance", modelData.distanceMax)
                jsonObject.put("lat", modelData.latitude)
                jsonObject.put("long", modelData.longitude)
                return jsonObject
            } else {
                val jsonObject = JSONObject()
                jsonObject.put("tags", jArrayCategory)
                jsonObject.put("min_price", modelData.priceMin)
                jsonObject.put("max_price", modelData.priceMax)
                jsonObject.put("min_distance", modelData.distanceMin)
                jsonObject.put("max_distance", modelData.distanceMax)
                jsonObject.put("lat", currentLatLng.latitude)
                jsonObject.put("long", currentLatLng.longitude)
                return jsonObject
            }
        } catch (e: Exception) {
            e.printStackTrace()
            return JSONObject("")
        }


    }


    private fun initz() {
        linearBack = findViewById(R.id.linearBack)
        bottom_sheet = findViewById(R.id.bottom_sheet)
        line = findViewById(R.id.line)
        downImage = findViewById(R.id.downImage)
        brandSearch = findViewById(R.id.brand_search)
        showHideBottomSheet = findViewById(R.id.showHideBottomSheet)
        tv_earn = findViewById(R.id.tv_earn)
        found = findViewById(R.id.found)
        recyclerCategories = findViewById(R.id.recyclerCategories)
        cardInvite = findViewById(R.id.cardInvite)
        btnFilter = findViewById(R.id.btnFilter1)
        mreset = findViewById(R.id.reset)
        mfilters = findViewById(R.id.filters)
        filter_done = findViewById(R.id.filter_done)
        mViewModel = ViewModelProviders.of(this)[HomeViewModel::class.java]
        drawerLayout1.addDrawerListener(this)
        imagePath = Constant.getPrefs(this).getString(Constant.dataImagePath, "")

    }


    override fun onMapReady(p0: GoogleMap?) {
        mMap = p0!!
        mMap.uiSettings.isCompassEnabled = true
        setUpMap()

    }

    @SuppressLint("SetTextI18n")
    private fun setUpMap() {
        if (ActivityCompat.checkSelfPermission(
                this,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                RequestBusinessFragment2.LOCATION_PERMISSION_REQUEST_CODE
            )
            return
        }
        fusedLocationClient.lastLocation.addOnSuccessListener(this) { location ->
            // Got last known location. In some rare situations this can be null.
            if (location != null) {
                lastLocation = location
                currentLatLng = LatLng(location.latitude, location.longitude)
//                placeMarkerOnMap(currentLatLng)
                val mPrefs = getSharedPreferences("data", MODE_PRIVATE)
                val gson = Gson()
                val data = mPrefs.getString("filterData", "")
                var model = ModelData()
                if (data != "") {
                    model = gson.fromJson(data, ModelData::class.java)
                }

                if (model.latitude != "" && model.longitude != "") {
                    currentLatLng =
                        LatLng(model.latitude.toDouble(), model.longitude.toDouble())


                    val place =
                        "Found " + "<b>" + arrayHomeSubscription.size + "</b>" + " places near " + "<b>" + getAddress(
                            currentLatLng
                        ) + "</b>"
                    found.text = Html.fromHtml(place)

                } else {
                    val place =
                        "Found " + "<b>" + arrayHomeSubscription.size + "</b>" + " places near " + "<b>" + getAddress(
                            currentLatLng
                        ) + "</b>"
                    found.text = Html.fromHtml(place)
                }

            }

            mMap.setOnMarkerClickListener(this)
            if (arrayHomeSubscription.size > 0) {
                for (item in arrayHomeSubscription) {
                    setLocationData = locationData(item.location)
                    for (locationDataModel in setLocationData) {
                        if (locationDataModel.lat != "" && locationDataModel.long != "") {
                            val latLng = LatLng(
                                locationDataModel.lat.toDouble(),
                                locationDataModel.long.toDouble()
                            )
                            placeMarkerOnMap(latLng, locationDataModel.id)
                            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 14f))
                        }
                    }
                }
            } else {
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 14f))
            }
        }


    }

    private fun getAddress(latLng: LatLng): String {
        // 1
        val geocoder = Geocoder(this)
        val addresses: List<Address>?
        val address: Address?
        var addressText = ""

        try {
            // 2
            addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1)
            // 3

            if (null != addresses && addresses.isNotEmpty()) {
                addressText = addresses[0].locality + " " + addresses[0].adminArea
                // for (i in 0 until address.maxAddressLineIndex) {
                //      addressText += if (i == 0) address.getAddressLine(i) else "\n" + address.getAddressLine(i)
//                }
            }
        } catch (e: IOException) {
            Log.e("MapsActivity", e.localizedMessage)
        }

        return addressText
    }

    private fun placeMarkerOnMap(
        location: LatLng,
        locationName: String
    ) {
        mMap.addMarker(
            MarkerOptions()
                .position(location)
                .title(locationName)
                .icon(bitmapDescriptorFromVector(this, R.drawable.ic_location_blue_icon))
        )

    }

    private fun bitmapDescriptorFromVector(
        context: Context,
        vectorResId: Int
    ): BitmapDescriptor {
        val vectorDrawable = ContextCompat.getDrawable(context, vectorResId)
        vectorDrawable!!.setBounds(
            0,
            0,
            vectorDrawable.intrinsicWidth,
            vectorDrawable.intrinsicHeight
        )
        val bitmap =
            Bitmap.createBitmap(
                vectorDrawable.intrinsicWidth,
                vectorDrawable.intrinsicHeight,
                Bitmap.Config.ARGB_8888
            )
        val canvas = Canvas(bitmap)
        vectorDrawable.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }


    @SuppressLint("SetTextI18n")
    override fun onMarkerClick(p0: Marker): Boolean {
        mMap.uiSettings.isZoomControlsEnabled = true
        val arrayHomeSubscriptionFilter = ArrayList<ModelHomeDataSubscriptions>()
        for (item in arrayHomeSubscription) {
            setLocationData = locationData(item.location)
            for (location in setLocationData) {
                if (location.lat != "" && location.long != "") {
                    if (p0.title == location.id) {
                        arrayHomeSubscriptionFilter.add(item)
                    }
                }
            }
        }


        when {
            arrayHomeSubscriptionFilter.size == 1 -> {
                searchOneResult.visibility = View.VISIBLE
                searchMultiple.visibility = View.GONE
                found.visibility = View.GONE
                linear.visibility = View.GONE
                nestedFirst.visibility = View.GONE
                recyclerRequest.visibility = View.GONE
                val model = arrayHomeSubscriptionFilter[0]
                textView214.text = model.campaign_name

                textView214.setOnClickListener {
                    val intent = Intent(this, MerchantSubscriptionViewActivity::class.java)
                    intent.putExtra("modelSubscriptionView", model.campaign_id)
                    intent.putExtra("modelSubscriptionData", model)
                    startActivity(intent)

                }
                website.text = model.website
                phone.text = Constant.formatPhoneNumber(model.business_phone)
                textView163.text = model.business_name

                val cost = Constant.getCalculatedPrice(model.discount_price, model.price)
                val splitPos = cost.split(".")
                if (splitPos[1] == "00" || splitPos[1] == "0") {
                    tv_cost.text = "$" + splitPos[0]
                } else {
                    tv_cost.text = "$$cost"
                }
                val actualCost = model.price.toDouble().toString()
                val splitActualCost = actualCost.split(".")
                if (splitActualCost[1] == "00" || splitActualCost[1] == "0") {
//                    tv_last_text.text = "$" + splitActualCost[0]
                } else {
//                    tv_last_text.text = "$" + actualCost.toInt()
                }

//                tv_off.text = model.discount_price.toDouble().toInt().toString() + "% off"
//                tv_last_text.paintFlags = tv_last_text.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
                Glide.with(this).load(imagePath + model.business_logo)
                    .placeholder(R.mipmap.circular_placeholder)
                    .into(circleImageView7)

                var latitute = ""
                var longitude = ""

                if (model.like_percentage == "") {
                    textView107.text = "0" + "%"

                } else {
                    textView107.text = model.like_percentage + "%"
                }

                if (model.subscriber == "") {
                    tvBoughtt.text = "0"

                } else {
                    tvBoughtt.text = model.subscriber
                }

                if (model.views == "") {
                    tvView.text = "0"

                } else {
                    tvView.text = model.views
                }
                if (model.location != "" || model.location != "[]") {
                    val addressData = locationData(model.location)
                    if (addressData.size > 0) {
                        latitute = addressData[0].lat
                        longitude = addressData[0].long
                        if (addressData[0].suite.isEmpty()) {
                            address.text =
                                addressData[0].street + " " + addressData[0].locationName + "," + addressData[0].city + "," + addressData[0].state
                        } else {
                            address.text =
                                addressData[0].suite + "," + addressData[0].street + " " + addressData[0].locationName + "," + addressData[0].city + "," + addressData[0].state

                        }
                    }
                }

                website.text = model.website

                btn_buy.setOnClickListener {
                    val intent = Intent(this, MerchantSubscriptionViewActivity::class.java)
                    intent.putExtra("modelSubscriptionView", model.campaign_id)
                    intent.putExtra("modelSubscriptionData", model)
                    startActivity(intent)

                }

                direction.setOnClickListener {

                    if (latitute != "" && longitude != "") {
                        val latlong = "$latitute,$longitude"
                        val intent = Intent(
                            android.content.Intent.ACTION_VIEW,
                            Uri.parse("google.navigation:q=$latlong")
                        )
                        startActivity(intent)
                    }
                }

                textView114.setOnClickListener {
                    //            val emailIntent = Intent(
//                Intent.ACTION_SENDTO, Uri.fromParts(
//                    "mailto", model.website, null
//                )
//            )
//            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Feedback")
//            emailIntent.putExtra(
//                Intent.EXTRA_TEXT,
//                ""
//            )
//            this.startActivity(Intent.createChooser(emailIntent, "Send email"))
                    if (model.website.isNotEmpty()) {
                        try {
                            if (!model.website.contains("http") || !model.website.contains("https")) {
                                val browserIntent =
                                    Intent(
                                        Intent.ACTION_VIEW,
                                        Uri.parse("https://" + model.website)
                                    )
                                startActivity(browserIntent)
                            } else {
                                val browserIntent =
                                    Intent(Intent.ACTION_VIEW, Uri.parse(model.website))
                                startActivity(browserIntent)
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }


                }

                textView163.setOnClickListener {
                    startActivity(
                        Intent(this, MerchantStoreActivity::class.java).putExtra(
                            "model",
                            model.business_id
                        )
                    )
                }



                call.setOnClickListener {
                    startActivity(
                        Intent(
                            Intent.ACTION_DIAL,
                            Uri.fromParts("tel", model.business_phone, null)
                        )
                    )
                }

                mAdapterImages.update(model.images)
            }
            arrayHomeSubscriptionFilter.size > 1 -> {
                recyclerRequest.visibility = View.GONE
                found.visibility = View.GONE
                searchMultiple.visibility = View.VISIBLE
                businessNamee.text = arrayHomeSubscriptionFilter[0].business_name
                Glide.with(this).load(imagePath + arrayHomeSubscriptionFilter[0].business_logo)
                    .placeholder(R.mipmap.circular_placeholder).into(circleImageView6)
                textView162.text = arrayHomeSubscriptionFilter.size.toString() + " subscriptions"
                val manager = LinearLayoutManager(this)
                recyclerViewMultiple.visibility = View.VISIBLE
                recyclerViewMultiple.layoutManager = manager
                mAdapter = AdapterMaps(this, this, 2)
                recyclerViewMultiple.adapter = mAdapter
                mAdapter.update(arrayHomeSubscriptionFilter)
            }
            else -> {
                imageBack.visibility = View.VISIBLE
                recyclerRequest.visibility = View.VISIBLE
                searchMultiple.visibility = View.GONE
                searchOneResult.visibility = View.GONE
                val place =
                    "Found " + "<b>" + arrayHomeSubscriptionFilter.size + "</b>" + " places near " + "<b>" + getAddress(
                        p0.position
                    ) + "</b>"
                found.text = Html.fromHtml(place)
                mAdapter.update(arrayHomeSubscriptionFilter)
            }
        }


//        imageBack.visibility = View.VISIBLE
        recyclerCategories.visibility = View.GONE



        return true
    }

    fun onBusinessSelect(position: Int, selected: Boolean) {
        selectedModel = null

        selectedModel = mData[position]


        //mAdapter.updateArray(position)
    }


    override fun onDrawerStateChanged(newState: Int) {


    }

    override fun onDrawerSlide(drawerView: View, slideOffset: Float) {

    }

    override fun onDrawerClosed(drawerView: View) {
        drawerOpen = 0
    }

    override fun onDrawerOpened(drawerView: View) {
        drawerOpen = 1
        drawerLayout1.openDrawer(GravityCompat.END)
        inflateFragment(FilterFragment(this))
        filterImgBack.visibility = View.GONE
        mfilters.text = "Filters"
        mreset.visibility = View.VISIBLE
    }

    override fun filterHeaderBackground(s: String) {
        if (s == "new") {
            filterHeader.setBackgroundDrawable(getDrawable(R.drawable.filter_header_select_category))
        } else {
            filterHeader.setBackgroundDrawable(getDrawable(R.drawable.filter_header))

        }
    }


    override fun nextFragments(i: Int) {
        when (i) {
            1 -> {
                inflateFragment(SortByFragment())
                mreset.visibility = View.GONE
                filterImgBack.visibility = View.VISIBLE
                filters.text = "Sort By"

                filterImgBack.setOnClickListener {
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"
                }

                filter_done.setOnClickListener {
                    //  inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"
                    if (checkFragment is FilterFragment) {
                        modelData = (checkFragment as FilterFragment).getData()
                        val mPrefs = getSharedPreferences("data", MODE_PRIVATE)
                        val prefsEditor = mPrefs.edit()
                        val gson = Gson()
                        val json = gson.toJson(modelData)
                        prefsEditor.putString("filterDataMaps", json)
                        prefsEditor.apply()
                    }
                    runApi()
                    onBackPressed()
                }

                reset.setOnClickListener {
                    businessId = ""
                    businessNames = "All"
                    sortName = "No Filter Selected"
                    checkedItems.clear()
                    val mPrefs = getSharedPreferences("data", MODE_PRIVATE)
                    val prefsEditor = mPrefs.edit()
                    prefsEditor.putString("filterDataMaps", "")
                    prefsEditor.apply()
                    runApi()
                    onBackPressed()
                }
            }


            2 -> {
                inflateFragment(CategoryFilterFragment(this))
                mreset.visibility = View.GONE
                filterImgBack.visibility = View.VISIBLE
                filters.text = "Category"

                filterImgBack.setOnClickListener {
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"

                }
                filter_done.setOnClickListener {
                    if (checkFragment is CategoryFilterFragment) {
                        checkedItems =
                            (checkFragment as CategoryFilterFragment).getCheckedCategories()
                    }
                    //inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"
                    if (checkFragment is FilterFragment) {
                        modelData = (checkFragment as FilterFragment).getData()
                        val mPrefs = getSharedPreferences("data", MODE_PRIVATE)
                        val prefsEditor = mPrefs.edit()
                        val gson = Gson()
                        val json = gson.toJson(modelData)
                        prefsEditor.putString("filterDataMaps", json)
                        prefsEditor.apply()
                    }
                    runApi()
                    onBackPressed()
                }

                reset.setOnClickListener {
                    businessId = ""
                    businessNames = "All"
                    sortName = "No Filter Selected"
                    checkedItems.clear()
                    val mPrefs = getSharedPreferences("data", MODE_PRIVATE)
                    val prefsEditor = mPrefs.edit()
                    prefsEditor.putString("filterDataMaps", "")
                    prefsEditor.apply()
                    runApi()
                    onBackPressed()
                }
            }

            3 -> {
                inflateFragment(BrandsFilterFragment())
                mreset.visibility = View.GONE
                filterImgBack.visibility = View.VISIBLE
                filters.text = "Brands"

                filterImgBack.setOnClickListener {
                    Constant.hideKeyboard(this,brandSearch)
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"

                }

                filter_done.setOnClickListener {
                    // inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"
                    if (checkFragment is FilterFragment) {
                        modelData = (checkFragment as FilterFragment).getData()
                        val mPrefs = getSharedPreferences("data", MODE_PRIVATE)
                        val prefsEditor = mPrefs.edit()
                        val gson = Gson()
                        val json = gson.toJson(modelData)
                        prefsEditor.putString("filterDataMaps", json)
                        prefsEditor.apply()
                    }
                    runApi()
                    onBackPressed()

                }

                reset.setOnClickListener {
                    businessId = ""
                    businessNames = "All"
                    sortName = "No Filter Selected"
                    checkedItems.clear()
                    val mPrefs = getSharedPreferences("data", MODE_PRIVATE)
                    val prefsEditor = mPrefs.edit()
                    prefsEditor.putString("filterDataMaps", "")
                    prefsEditor.apply()
                    runApi()
                    onBackPressed()
                }
            }
            4 -> {
                inflateFragment(RedemptionFilterFragment())
                mreset.visibility = View.GONE
                filterImgBack.visibility = View.VISIBLE
                filters.text = "Redemption"

                filterImgBack.setOnClickListener {
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"

                }
                filter_done.setOnClickListener {
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"
                }
                reset.setOnClickListener {
                    businessId = ""
                    businessNames = "All"
                    sortName = "No Filter Selected"
                    checkedItems.clear()
                    val mPrefs = getSharedPreferences("data", MODE_PRIVATE)
                    val prefsEditor = mPrefs.edit()
                    prefsEditor.putString("filterDataMaps", "")
                    prefsEditor.apply()
                    runApi()
                    onBackPressed()
                }

            }
        }
    }

    override fun getSelectedSort(name: String, position: Int) {
        sortName = name
    }

    override fun getSelectedBrand(
        id: String,
        position: Int,
        businessName: String
    ) {
        inflateFragment(FilterFragment(this))
        businessId = id
        businessNames = businessName
        mreset.visibility = View.VISIBLE
        filterImgBack.visibility = View.GONE
        filters.text = "Filters"
        if (checkFragment is FilterFragment) {
            (checkFragment as FilterFragment).updateBrandData(businessNames)
        }
    }

    fun runApi() {
        val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
        val mPrefs = getSharedPreferences("data", MODE_PRIVATE)
        val gson = Gson()
        val data = mPrefs.getString("filterDataMaps", "")
        var model = ModelData()
        if (data != "") {
            model = gson.fromJson(data, ModelData::class.java)
        }
        mViewModel.getHomeData(
            "",
            "minnesota",
            jObjectSort(sortName),
            jObjectFilter(checkedItems, model, currentLatLng),
            0
        )

    }

    @SuppressLint("SetTextI18n")
    override fun multipleClick(position: Int, model: ModelHomeDataSubscriptions) {
        searchOneResult.visibility = View.VISIBLE
        found.visibility = View.GONE
        linear.visibility = View.GONE
        nestedFirst.visibility = View.GONE
        searchMultiple.visibility = View.GONE
        recyclerRequest.visibility = View.GONE
        textView163.text = model.business_name
        textView214.text = model.campaign_name
        website.text = model.website
        phone.text = Constant.formatPhoneNumber(model.business_phone)
        val cost = Constant.getCalculatedPrice(model.discount_price, model.price)
        val splitPos = cost.split(".")
        if (splitPos[1] == "00" || splitPos[1] == "0") {
            tv_cost.text = "$" + splitPos[0]
        } else {
            tv_cost.text = "$$cost"
        }

        val actualCost = model.price.toDouble().toString()
        val splitActualCost = actualCost.split(".")
        if (splitActualCost[1] == "00" || splitActualCost[1] == "0") {
//            tv_last_text.text = "$" + splitActualCost[0]
        } else {
//            tv_last_text.text = "$" + actualCost.toInt()
        }

//        tv_off.text = model.discount_price.toDouble().toInt().toString() + "% off"


        Glide.with(this).load(imagePath + model.business_logo)
            .placeholder(R.mipmap.circular_placeholder)
            .into(circleImageView7)

        var latitute = ""
        var longitude = ""

        if (model.like_percentage == "") {
            textView107.text = "0" + "%"

        } else {
            textView107.text = model.like_percentage + "%"
        }

        if (model.subscriber == "") {
            tvBoughtt.text = "0"

        } else {
            tvBoughtt.text = model.subscriber
        }

        if (model.views == "") {
            tvView.text = "0"

        } else {
            tvView.text = model.views
        }
        if (model.location != "" || model.location != "[]") {
            val addressData = locationData(model.location)
            if (addressData.size > 0) {
                latitute = addressData[0].lat
                longitude = addressData[0].long
                if (addressData[0].suite.isEmpty()) {
                    address.text =
                        addressData[0].street + " " + addressData[0].locationName + "," + addressData[0].city + "," + addressData[0].state
                } else {
                    address.text =
                        addressData[0].suite + "," + addressData[0].street + " " + addressData[0].locationName + "," + addressData[0].city + "," + addressData[0].state

                }

            }
        }

        website.text = model.website
        phone.text = Constant.formatPhoneNumber(model.business_phone)

        btn_buy.setOnClickListener {
            val intent = Intent(this, MerchantSubscriptionViewActivity::class.java)
            intent.putExtra("modelSubscriptionView", model.campaign_id)
            intent.putExtra("modelSubscriptionData", model)
            startActivity(intent)

        }

        textView214.setOnClickListener {
            val intent = Intent(this, MerchantSubscriptionViewActivity::class.java)
            intent.putExtra("modelSubscriptionView", model.campaign_id)
            intent.putExtra("modelSubscriptionData", model)
            startActivity(intent)

        }


        direction.setOnClickListener {

            if (latitute != "" && longitude != "") {
                val latlong = "$latitute,$longitude"
                val intent = Intent(
                    android.content.Intent.ACTION_VIEW,
                    Uri.parse("google.navigation:q=$latlong")
                )
                startActivity(intent)
            }
        }

        textView114.setOnClickListener {
            //            val emailIntent = Intent(
//                Intent.ACTION_SENDTO, Uri.fromParts(
//                    "mailto", model.website, null
//                )
//            )
//            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Feedback")
//            emailIntent.putExtra(
//                Intent.EXTRA_TEXT,
//                ""
//            )
//            this.startActivity(Intent.createChooser(emailIntent, "Send email"))
            if (model.website.isNotEmpty()) {
                try {
                    if (!model.website.contains("http") || !model.website.contains("https")) {
                        val browserIntent =
                            Intent(Intent.ACTION_VIEW, Uri.parse("https://" + model.website))
                        startActivity(browserIntent)
                    } else {
                        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(model.website))
                        startActivity(browserIntent)
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

        }

        call.setOnClickListener {
            startActivity(
                Intent(
                    Intent.ACTION_DIAL,
                    Uri.fromParts("tel", model.business_phone, null)
                )
            )
        }

        textView163.setOnClickListener {
            startActivity(
                Intent(this, MerchantStoreActivity::class.java).putExtra(
                    "model",
                    model.business_id
                )
            )
        }

        mAdapterImages.update(model.images)
    }
}
