package com.upscribber.home

import android.os.Parcel
import android.os.Parcelable
import com.upscribber.upscribberSeller.subsriptionSeller.location.days.ModelSelectDays
import com.upscribber.upscribberSeller.subsriptionSeller.location.searchLoc.ModelSearchLocation
import com.upscribber.upscribberSeller.subsriptionSeller.location.searchLoc.ModelStoreLocation
import java.util.ArrayList

class ModelHomeLocation() : Parcelable{

    var state_code: String = ""
    var id: String = ""
    var business_id: String = ""
    var street: String = ""
    var user_id: String = ""
    var city: String = ""
    var state: String = ""
    var zip_code: String = ""
    var lat: String = ""
    var long: String = ""
    var suite: String = ""
    var created_at: String = ""
    var updated_at: String = ""
    var store_timingg : ArrayList<ModelStoreLocation> = ArrayList()
    var store_timing :String = ""

    constructor(parcel: Parcel) : this() {
        state_code = parcel.readString()
        id = parcel.readString()
        business_id = parcel.readString()
        street = parcel.readString()
        user_id = parcel.readString()
        city = parcel.readString()
        state = parcel.readString()
        zip_code = parcel.readString()
        lat = parcel.readString()
        long = parcel.readString()
        suite = parcel.readString()
        created_at = parcel.readString()
        updated_at = parcel.readString()
        store_timingg = parcel.createTypedArrayList(ModelStoreLocation)
        store_timing = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(state_code)
        parcel.writeString(id)
        parcel.writeString(business_id)
        parcel.writeString(street)
        parcel.writeString(user_id)
        parcel.writeString(city)
        parcel.writeString(state)
        parcel.writeString(zip_code)
        parcel.writeString(lat)
        parcel.writeString(long)
        parcel.writeString(suite)
        parcel.writeString(created_at)
        parcel.writeString(updated_at)
        parcel.writeTypedList(store_timingg)
        parcel.writeString(store_timing)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ModelHomeLocation> {
        override fun createFromParcel(parcel: Parcel): ModelHomeLocation {
            return ModelHomeLocation(parcel)
        }

        override fun newArray(size: Int): Array<ModelHomeLocation?> {
            return arrayOfNulls(size)
        }
    }


}
