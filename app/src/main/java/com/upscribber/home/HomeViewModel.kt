package com.upscribber.home

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.upscribber.commonClasses.ModelStatusMsg
import com.upscribber.notification.ModelNotificationCustomer
import com.upscribber.payment.paymentCards.PaymentCardModel
import org.json.JSONObject

class HomeViewModel(application: Application) : AndroidViewModel(application) {

    var homeRepository: HomeRepository = HomeRepository(application)

    fun getHomeData(
        tag: String,
        city: String,
        sortBy: JSONObject,
        filterBy: JSONObject,
        type: Int
    ) {
        homeRepository.getHomeData(tag, city, sortBy, filterBy, type)
    }


    fun getHomeDetail(): LiveData<HomeModelMain> {
        return homeRepository.getProfileDetail()
    }

    fun getmDataHomeSubscription(): LiveData<ModelHomeDataSubscriptions> {
        return homeRepository.getmHomeSubscription()
    }

    fun getmDataSubscriptions(): LiveData<ArrayList<ModelHomeDataSubscriptions>> {
        return homeRepository.getmDataSubscriptions()
    }


    fun getmDataStatus(): LiveData<ModelStatusMsg> {
        return homeRepository.getmDataFailure()
    }


    fun getmDataDeclined(): LiveData<ModelStatusMsg> {
        return homeRepository.getmDataDeclined()
    }


    fun setFavourite(auth: String, favorite: ModelHomeDataSubscriptions) {
        homeRepository.setFavouriteData(auth, favorite)

    }

    fun getStepsData(auth: String) {
        homeRepository.getStepsData(auth)
    }

    fun getCategories(auth: String) {
        homeRepository.getAllCategoriesData(auth)
    }

    fun getmDataCategories(): LiveData<ArrayList<WhatsNewModelOne>> {
        return homeRepository.getmDataCategories()
    }

    fun getmDataNotification(): LiveData<ModelNotificationCustomer> {
        return homeRepository.getmDataNotification()
    }


    fun getRetrieveCards() {
        homeRepository.getRetrieveCards()
    }

    fun getDeclinedApi(auth: String) {
        homeRepository.getDeclinedInvites(auth)

    }

    fun getBusinessLinkInfo(auth: String, businessId: String) {
        homeRepository.getBusinessLinkInfo(auth, businessId)

    }


}
