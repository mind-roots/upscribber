package com.upscribber.home

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.databinding.DataBindingUtil
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.databinding.ActivityInviteFriendMerchantBinding

class InviteFriendMerchantActivity : AppCompatActivity() {

    lateinit var mBinding: ActivityInviteFriendMerchantBinding
    var inviteCode = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_invite_friend_merchant)
        setToolbar()
        if (intent.hasExtra("invite")) {
            inviteCode = intent.getStringExtra("invite")
        }
        clickListener()

    }

    fun clickListener() {
        mBinding.invite.setOnClickListener {
            val message =
                "Hey, I want to share this awesome deal I found on Upscribbr with you. Also, you'll get 20% your 1st purchase when you sign up if you use invite code $inviteCode .https://play.google.com/apps/testing/com.upscribbr"
            val share = Intent(Intent.ACTION_SEND)
            share.type = "text/plain"
            share.putExtra(Intent.EXTRA_TEXT, message)
            startActivityForResult(Intent.createChooser(share, "Share!"), 1)
        }

    }

    fun shareClick(v: View) {
        val inviteCode = Constant.getArrayListProfile(this, Constant.dataProfile)
        val message =
            "Hey, I want to share this awesome deal I found on Upscribbr with you. Also, you'll get 20% your 1st purchase when you sign up if you use invite code ${inviteCode.invite_code} .https://play.google.com/apps/testing/com.upscribbr"
        val share = Intent(Intent.ACTION_SEND)
        share.type = "text/plain"
        share.putExtra(Intent.EXTRA_TEXT, message)
        startActivityForResult(Intent.createChooser(share, "Share!"), 1)
    }


    fun fbclick(v: View) {

//         var  urlFb = "fb://page/"+ yourpageid
//        Intent intent = new Intent(Intent.ACTION_VIEW)
//        intent.setData(Uri.parse(urlFb));
//
//        // If a Facebook app is installed, use it. Otherwise, launch
//        // a browser
//        final PackageManager packageManager = getPackageManager();
//        List<ResolveInfo> list =
//        packageManager.queryIntentActivities(intent,
//            PackageManager.MATCH_DEFAULT_ONLY);
//        if (list.size() == 0) {
//            final String urlBrowser = "https://www.facebook.com/pages/"+pageid
//            intent.setData(Uri.parse(urlBrowser));
//        }
//
//        startActivity(intent);
    }





    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1) {
            finish()
        }
    }


    private fun setToolbar() {
        setSupportActionBar(mBinding.include8.toolbar)
        title = ""
        mBinding.include8.title.text = "Invite a Friend"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}
