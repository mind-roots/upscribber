package com.upscribber.home

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.upscribber.R
import kotlinx.android.synthetic.main.home_whats_new_recycler.view.*

class HomeWhatsNewOneAdapter
    (
    private val mContext: Context,
    listen: HomeFragment

) :
    RecyclerView.Adapter<HomeWhatsNewOneAdapter.MyViewHolder>() {

    var listener = listen as BackGroundInterface
    private var list: ArrayList<WhatsNewModelOne> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(mContext)
            .inflate(R.layout.home_whats_new_recycler, parent, false)
        return HomeWhatsNewOneAdapter.MyViewHolder(v)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = list[position]
        if (position == 0){
            holder.itemView.tv_skinCare.text = model.name
            Glide.with(mContext).load(R.mipmap.circular_placeholder).placeholder(R.mipmap.circular_placeholder).into(holder.itemView.img_icons)
            Glide.with(mContext).load(R.mipmap.circularprogress_black).placeholder(R.mipmap.circular_placeholder).into(holder.itemView.img_icons1)
        }else{
            holder.itemView.tv_skinCare.text = model.name
            Glide.with(mContext).load(model.selected_image).placeholder(R.mipmap.circular_placeholder).into(holder.itemView.img_icons)
            Glide.with(mContext).load(model.unselected_image).placeholder(R.mipmap.circular_placeholder).into(holder.itemView.img_icons1)
        }

        if (model.background) {
            holder.linear1.setBackgroundResource(R.drawable.home_whats_new_background_pink)
            holder.img_icons.visibility = View.GONE
            holder.img_icons1.visibility = View.VISIBLE
            holder.tv_skinCare.setTextColor(Color.WHITE)

        } else {
            holder.linear1.setBackgroundResource(R.drawable.home_whts_new_bg_nonselected)
            holder.img_icons.visibility = View.VISIBLE
            holder.img_icons1.visibility = View.GONE
            holder.tv_skinCare.setTextColor(Color.parseColor("#8d8e9c"))

        }

        holder.itemView.setOnClickListener {
            listener.ChangeBackground(position)

        }


    }

    fun update(it: java.util.ArrayList<WhatsNewModelOne>?) {
        list = it!!
        notifyDataSetChanged()

    }


    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tv_skinCare: TextView = itemView.findViewById(R.id.tv_skinCare)
        var img_icons: ImageView = itemView.findViewById(R.id.img_icons)
        var img_icons1: ImageView = itemView.findViewById(R.id.img_icons1)
        var linear1: LinearLayout = itemView.findViewById(R.id.linear1)
    }

}