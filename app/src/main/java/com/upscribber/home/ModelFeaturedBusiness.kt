package com.upscribber.home

import android.os.Parcel
import android.os.Parcelable

class ModelFeaturedBusiness() : Parcelable{

    var business_name: String = ""
    var logo: String = ""
    var cover_image: String = ""
    var status: String = ""
    var id: String = ""

    constructor(parcel: Parcel) : this() {
        business_name = parcel.readString()
        logo = parcel.readString()
        cover_image = parcel.readString()
        status = parcel.readString()
        id = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(business_name)
        parcel.writeString(logo)
        parcel.writeString(cover_image)
        parcel.writeString(status)
        parcel.writeString(id)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ModelFeaturedBusiness> {
        override fun createFromParcel(parcel: Parcel): ModelFeaturedBusiness {
            return ModelFeaturedBusiness(parcel)
        }

        override fun newArray(size: Int): Array<ModelFeaturedBusiness?> {
            return arrayOfNulls(size)
        }
    }
}
