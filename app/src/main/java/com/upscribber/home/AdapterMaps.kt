package com.upscribber.home

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.subsciptions.merchantSubscriptionPages.MerchantSubscriptionViewActivity
import com.upscribber.upscribberSeller.subsriptionSeller.location.searchLoc.ModelSearchLocation
import com.upscribber.upscribberSeller.subsriptionSeller.location.searchLoc.ModelStoreLocation
import kotlinx.android.synthetic.main.request_layout.view.imgRequest
import kotlinx.android.synthetic.main.request_layout_maps.view.*
import org.json.JSONArray
import java.math.BigDecimal
import java.math.RoundingMode

class AdapterMaps(
    private val mContext: Context,
    listen: MapsSearchActivity,
    var positionData: Int
) : RecyclerView.Adapter<AdapterMaps.MyViewHolder>() {

    val listener = mContext as layoutClick

    var selectListener: MapsSearchActivity = listen
    private var list: ArrayList<ModelHomeDataSubscriptions> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(mContext)
            .inflate(R.layout.request_layout_maps, parent, false)

        return MyViewHolder(v)

    }

    override fun getItemCount(): Int {
        return list.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = list[position]
        val imagePath = Constant.getPrefs(mContext).getString(Constant.dataImagePath, "")
        try {
            val getLocationArray = locationData(model.location)

            if (getLocationArray.size > 1) {
                holder.itemView.textView321.text = "Multiple Locations"
            } else {
                holder.itemView.textView321.text = getLocationArray[0].city + "," +getLocationArray[0].state_code
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }


            holder.itemView.layout.setOnClickListener {
                listener.multipleClick(position, model)

//                val intent = Intent(mContext, MerchantSubscriptionViewActivity::class.java)
//                intent.putExtra("modelSubscriptionView", model.campaign_id)
//                intent.putExtra("modelSubscriptionData", model)
//                mContext.startActivity(intent)
        }


        holder.itemView.tvPrice.paintFlags = holder.itemView.tvPrice.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG

        val cost = Constant.getCalculatedPrice(model.discount_price, model.price)
        if (cost.contains(".")){
            val splitPos = cost.split(".")
            if (splitPos[1] == "00" || splitPos[1] == "0") {
                holder.itemView.tv_cost.text = "$" + splitPos[0]
            }else{
                holder.itemView.tv_cost.text = "$" + BigDecimal(cost).setScale(2, RoundingMode.HALF_UP).toString()
            }
        }else{
            holder.itemView.tv_cost.text = "$" + BigDecimal(cost).setScale(2, RoundingMode.HALF_UP).toString()

        }


        val actualCost = model.price.toDouble().toString()

        if (actualCost.contains(".")) {
            val splitActualCost = actualCost.split(".")
            if (splitActualCost[1] == "00" || splitActualCost[1] == "0") {
                holder.itemView.tvPrice.text = "$" + splitActualCost[0]
            } else {
                holder.itemView.tvPrice.text = "$" + actualCost.toInt()
            }
        }else{
            holder.itemView.tvPrice.text = "$" + actualCost.toInt()
        }

        holder.itemView.tv_off.text = model.discount_price.toDouble().toInt().toString() + "% off"
        Glide.with(mContext).load(imagePath + model.campaign_image)
            .placeholder(R.mipmap.placeholder_subscription_square)
            .into(holder.itemView.imgRequest)
        holder.itemView.textView320.text = model.campaign_name

        if (model.like_percentage == "") {
            holder.itemView.textView322.text = "0" + "%"

        } else {
            holder.itemView.textView322.text = model.like_percentage + "%"
        }

        if (model.subscriber == "") {
            holder.itemView.textView325.text = "0"

        } else {
            holder.itemView.textView325.text = model.subscriber
        }

        if (model.views == "") {
            holder.itemView.textView323.text = "0"

        } else {
            holder.itemView.textView323.text = model.views
        }


        if (model.free_trial == "No free trial" && model.introductory_price == "0") {
            holder.itemView.tv_free_map.visibility = View.GONE
        } else if (model.free_trial != "No free trial") {
            holder.itemView.tv_free_map.visibility = View.VISIBLE
            holder.itemView.tv_free_map.text = "Free"
            holder.itemView.tv_free_map.setBackgroundResource(R.drawable.intro_price_background)
        } else {
            holder.itemView.tv_free_map.visibility = View.VISIBLE
            holder.itemView.tv_free_map.text = "Intro"
            holder.itemView.tv_free_map.setBackgroundResource(R.drawable.home_free_background)
        }

        holder.itemView.textView324


        /*holder.itemView.setOnClickListener {
            if (list[position].isSelected) {
                selectListener.onBusinessSelect(position, list[position].isSelected)
            } else {
                selectListener.onBusinessSelect(position, list[position].isSelected)
            }
        }*/


    }

    /*  fun updateArray(position: Int) {
          val selectedModel = list[position]
          if (selectedModel.isSelected) {
              selectedModel.isSelected = false

          } else {
              for (child in list) {
                  child.isSelected = false
              }
              list[position].isSelected = true
          }
          notifyDataSetChanged()
      }*/

    fun update(it: ArrayList<ModelHomeDataSubscriptions>?) {
        this.list = it as ArrayList<ModelHomeDataSubscriptions>
        notifyDataSetChanged()

    }

    fun locationData(location: String): ArrayList<ModelSearchLocation> {
        val gson = Gson()
        /*val model: ArrayList<ModelSearchLocation> =
            gson.fromJson(
                location,
                object : TypeToken<ArrayList<ModelSearchLocation>>() {}.type
            )*/
        val locationDataArray = ArrayList<ModelSearchLocation>()
        val loc = JSONArray(location)
        if (loc.length() > 0) {
            for (i in 0 until loc.length()) {
                val obj = loc.optJSONObject(i)
                val modelSearchLocation = ModelSearchLocation()
                modelSearchLocation.id = obj.optString("id")
                modelSearchLocation.business_id = obj.optString("business_id")
                modelSearchLocation.user_id = obj.optString("user_id")
                modelSearchLocation.street = obj.optString("street")
                modelSearchLocation.city = obj.optString("city")
                modelSearchLocation.state_code = obj.optString("state_code")
                modelSearchLocation.state = obj.optString("state")
                modelSearchLocation.zip_code = obj.optString("zip_code")
                modelSearchLocation.lat = obj.optString("lat")
                modelSearchLocation.long = obj.optString("long")
                modelSearchLocation.suite = obj.optString("suite")
                modelSearchLocation.created_at = obj.optString("created_at")
                modelSearchLocation.updated_at = obj.optString("updated_at")
                val storeTiming = obj.optString("store_timing")

                if (storeTiming == "" || storeTiming == "[]") {
                    val storeModel = ModelStoreLocation()
                    storeModel.day = ""
                    storeModel.open = ""
                    storeModel.close = ""
                    storeModel.checked = ""
                    storeModel.status = ""
                    modelSearchLocation.store_timing.add(storeModel)
                } else {
                    val model: ArrayList<ModelStoreLocation> =
                        gson.fromJson(
                            storeTiming,
                            object : TypeToken<ArrayList<ModelStoreLocation>>() {}.type
                        )
                    modelSearchLocation.store_timing = model

                    /*if (storeArray.length() > 0) {
                        for (j in 0 until storeArray.length()) {
                            val storeData = storeArray.optJSONObject(j)
                            val storeModel = ModelStoreLocation()
                            storeModel.day = storeData.optString("days")
                            storeModel.open = storeData.optString("open")
                            storeModel.close = storeData.optString("close")
                            storeModel.checked = storeData.optString("checked")
                            storeModel.status = storeData.optString("status")
                            modelSearchLocation.store_timing.add(storeModel)
                        }
                    }*/
                }
                locationDataArray.add(modelSearchLocation)
            }
        }
        return locationDataArray
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    interface layoutClick {
        fun multipleClick(position: Int, model: ModelHomeDataSubscriptions)
    }
}