package com.upscribber.home

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.commonClasses.ModelStatusMsg
import kotlinx.android.synthetic.main.home_top_brand_recycler.view.*
import kotlinx.android.synthetic.main.map_campaign_images.view.*

class RecyclerViewMapImages(
    private val mContext: Context
) :
    RecyclerView.Adapter<RecyclerViewMapImages.MyViewHolder>() {

    private var list: ArrayList<ModelStatusMsg> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(mContext)
            .inflate(R.layout.map_campaign_images, parent, false)
        return MyViewHolder(v)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = list[position]
        val imagePath = Constant.getPrefs(mContext).getString(Constant.dataImagePath, "")

        Glide.with(mContext).load(imagePath + model.image)
            .placeholder(R.mipmap.placeholder_subscription_square).into(holder.itemView.images)
    }

    fun update(images: java.util.ArrayList<ModelStatusMsg>) {
        list = images
        notifyDataSetChanged()
    }


    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

}