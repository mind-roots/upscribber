package com.upscribber.home

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.upscribber.subsciptions.merchantSubscriptionPages.MerchantSubscriptionViewActivity
import com.upscribber.R
import com.upscribber.becomeseller.sellerWelcomeScreens.sellerWelcomActivity
import com.upscribber.commonClasses.Constant
import com.upscribber.requestbusiness.RequestBusiness
import com.upscribber.surveyFeedback.SurveyFeedbackActivity
import kotlinx.android.synthetic.main.home_whats_new_recycler_two.view.*
import org.json.JSONArray
import java.math.BigDecimal
import java.math.RoundingMode

@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class HomeWhatsNewTwoAdapter(
    private val mContext: Context, listen: HomeFragment
) :
    RecyclerView.Adapter<HomeWhatsNewTwoAdapter.MyViewHolder>() {

    private var list: ArrayList<ModelHomeDataSubscriptions> = ArrayList()

    var listener = listen as FavoriteInterface
    var listener1 = listen as SwitchSeller

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val v = LayoutInflater.from(mContext)
            .inflate(R.layout.home_whats_new_recycler_two, parent, false)
        return MyViewHolder(v)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        val model = list[position]
        val imagePath = Constant.getPrefs(mContext).getString(Constant.dataImagePath, "")


        if (model.subscription_id == "-1" || model.subscription_id == "-2" || model.subscription_id == "-3") {
            holder.itemView.constraintBanner.visibility = View.VISIBLE
            holder.itemView.constraint.visibility = View.GONE

            when (model.subscription_id) {
                "-1" -> holder.itemView.img.setImageResource(R.mipmap.img_banner2)
                "-2" -> holder.itemView.img.setImageResource(R.mipmap.img_banner1)
                "-3" -> holder.itemView.img.setImageResource(R.mipmap.img_banner3)
            }

        } else {
            try {
                holder.itemView.tv_off.text =
                    model.discount_price.toDouble().toInt().toString() + "% off"
                holder.itemView.constraintBanner.visibility = View.GONE
                holder.itemView.constraint.visibility = View.VISIBLE
                Glide.with(mContext).load(model.like).placeholder(R.mipmap.placeholder_subscription)
                    .into(holder.itemView.favorite_image)
                holder.itemView.tv_brazillian.text = model.business_name
                holder.itemView.tv_person_count.text = (model.subscriber + " subscribers")
                holder.itemView.tv_long_text.text = model.campaign_name
                val cost = Constant.getCalculatedPrice(model.discount_price, model.price)
                val splitPos = cost.split(".")
                if (splitPos[1] == "00" || splitPos[1] == "0") {
                    holder.itemView.tv_cost.text = "$" + splitPos[0]
                } else {
                    holder.itemView.tv_cost.text =
                        "$" + BigDecimal(cost).setScale(2, RoundingMode.HALF_UP)
                }
                val actualCost = model.price.toDouble().toString()
                val splitActualCost = actualCost.split(".")
                if (splitActualCost[1] == "00" || splitActualCost[1] == "0") {
                    holder.itemView.tv_last_text.text = "$" + splitActualCost[0]
                } else {
                    holder.itemView.tv_last_text.text = "$" + actualCost.toInt()
                }

                if (model.campaign_image.isEmpty()) {
                    Glide.with(mContext).load(model.campaign_image)
                        .placeholder(R.mipmap.placeholder_subscription)
                        .into(holder.itemView.favorite_image)
                } else {
                    Glide.with(mContext).load(imagePath + model.campaign_image)
                        .into(holder.itemView.favorite_image)
                }

                Glide.with(mContext).load(imagePath + model.campaign_image)
                    .into(holder.itemView.imgLogo)
                holder.itemView.textView36.text = getLocation(model.location)

                Glide.with(mContext).load(imagePath + model.business_logo)
                    .placeholder(R.mipmap.app_icon)
                    .into(holder.itemView.imgLogo)

                if (model.like_percentage == "0" || model.like_percentage.isEmpty()) {
                    holder.itemView.tv_percentage.visibility = View.GONE
                } else {
                    holder.itemView.tv_percentage.visibility = View.VISIBLE
                    holder.itemView.tv_percentage.text = model.like_percentage + "%"
                }


                if (model.free_trial == "No free trial" && (model.introductory_price == "0" || model.introductory_price == "0.00")) {
                    holder.itemView.tv_free_map.visibility = View.GONE
                } else if (model.free_trial != "No free trial") {
                    holder.itemView.tv_free_map.visibility = View.VISIBLE
                    holder.itemView.tv_free_map.text = "Free"
                    holder.itemView.tv_free_map.setBackgroundResource(R.drawable.intro_price_background)
                } else {
                    holder.itemView.tv_free_map.visibility = View.VISIBLE
                    holder.itemView.tv_free_map.text = "Intro"
                    holder.itemView.tv_free_map.setBackgroundResource(R.drawable.home_free_background)
                }

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        val like = Constant.checkFav(model.campaign_id, mContext)
        if (like) {
            holder.addFav.setImageResource(R.drawable.ic_favorite_fill)
        } else {
            holder.addFav.setImageResource(R.drawable.ic_favorite_border_black_24dp)

        }

        holder.itemView.addFav.setOnClickListener {
            listener.addToFav(position)
            if (like) {
                holder.addFav.setImageResource(R.drawable.ic_favorite_fill)
            } else {
                holder.addFav.setImageResource(R.drawable.ic_favorite_border_black_24dp)

            }
        }

        holder.itemView.constraintBanner.setOnClickListener {
            if (model.subscription_id == "-1") {
                val profileData = Constant.getArrayListProfile(mContext, Constant.dataProfile)
                if (profileData.steps == "6" && profileData.type == "1") {
                    listener1.swichSellerFromCard()
//                    (mContext as Activity).finish()
                } else if (profileData.steps == "4" && profileData.type == "2") {
                    listener1.swichSellerFromCard()
//                    (mContext as Activity).finish()
                } else {
                    mContext.startActivity(Intent(mContext, sellerWelcomActivity::class.java))
                }

            } else if (model.subscription_id == "-2") {
                mContext.startActivity(Intent(mContext, RequestBusiness::class.java))
            } else if (model.subscription_id == "-3") {
                mContext.startActivity(Intent(mContext, SurveyFeedbackActivity::class.java))
            }

        }

        holder.itemView.constraint.setOnClickListener {

            val sharedPref = mContext.getSharedPreferences("FromWhere", 0)
            mContext.startActivity(
                Intent(mContext, MerchantSubscriptionViewActivity::class.java)
                    .putExtra("modelSubscriptionView", model.campaign_id)
                    .putExtra("modelSubscriptionData", model)
            )

        }

        holder.tv_last_text.paintFlags =
            holder.tv_last_text.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
//        if (list.size <= 2 && list[0].subscription_id == "-1" && list[1].subscription_id == "-1") {
//            if (position == 0 || position == 1) {
//                holder.itemView.constraintBanner.visibility = View.VISIBLE
//                holder.itemView.constraint.visibility = View.GONE
//                if (position == 0) {
//                    holder.itemView.img.setImageResource(R.mipmap.img_banner1)
//                } else {
//                    holder.itemView.img.setImageResource(R.mipmap.img_banner2)
//                }
//            }
//        }
    }

    private fun removeData() {
        try {
            val sharedPreferences1 =
                mContext.getSharedPreferences("updateProfile", Context.MODE_PRIVATE)
            val editor = sharedPreferences1.edit()
            editor.remove("social")
            editor.apply()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun update(it: ArrayList<ModelHomeDataSubscriptions>?) {
        this.list = it as ArrayList<ModelHomeDataSubscriptions>
        notifyDataSetChanged()

    }

    private fun getLocation(location: String): String {
        var resultLocation = ""
        val loc = JSONArray(location)
        if (loc.length() > 1) {
            for (i in 0 until loc.length()) {
                val obj = loc.optJSONObject(i)
                resultLocation = if (resultLocation.isEmpty()) {
                    obj.optString("city")
                } else {
                    "$resultLocation, ${obj.optString("city")}"
                }
            }
        } else {
            val locationObject = loc.optJSONObject(0)
            resultLocation =
                "${locationObject.optString("city")}, ${locationObject.optString("state_code")}"
        }

        return resultLocation
    }


    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var tv_last_text: TextView = itemView.findViewById(R.id.tv_last_text)
        var addFav: ImageView = itemView.findViewById(R.id.addFav1)


    }

    interface SwitchSeller {
        fun swichSellerFromCard()
    }
}