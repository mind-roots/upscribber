package com.upscribber.home

class ModelSteps {

    var business_name: String = ""
    var ein: String = ""
    var business_email: String = ""
    var legal_name: String = ""
    var email: String = ""
    var business_phone: String = ""
    var billing_address: String = ""
    var business_address: String = ""
    var website: String = ""
    var billing_city: String = ""
    var billing_state: String = ""
    var billing_zipcode: String = ""
    var billing_street: String = ""
    var street: String = ""
    var city: String = ""
    var state: String = ""
    var zip_code: String = ""
    var suite: String = ""


    //-------------Step 4-------------

    var first_name: String = ""
    var birthdate: String = ""
    var ssn: String = ""
    var contact_no: String = ""
    var last_name: String = ""
    var home_address: String = ""
    var home_city: String = ""
    var home_state: String = ""
    var home_street: String = ""
    var home_zipcode: String = ""
    var home_suite: String = ""

}
