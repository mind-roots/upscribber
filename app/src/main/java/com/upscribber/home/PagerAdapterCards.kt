package com.upscribber.home

import android.content.Context
import android.view.View
import androidx.viewpager.widget.PagerAdapter
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.bumptech.glide.Glide
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import de.hdodenhof.circleimageview.CircleImageView

class PagerAdapterCards(
    var context: Context,
    var contextt: HomeFragment,
    var mData: ArrayList<ModelHomeDataSubscriptions>
) :
    PagerAdapter() {

    var listener = contextt as HomePagesClicks
    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun getCount(): Int {
        return mData.size
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val itemView = LayoutInflater.from(container.context)
            .inflate(R.layout.alert_home, container, false)

        val textDescription = itemView.findViewById(R.id.textDescription) as TextView
        val requestTxt = itemView.findViewById(R.id.requestTxt) as TextView
        val constraintMain = itemView.findViewById(R.id.constraintMain) as ConstraintLayout
        val imgShow = itemView.findViewById(R.id.imgShow) as ImageView
        val imgShowRevies = itemView.findViewById(R.id.imgShowRevies) as CircleImageView
        val btnAccept = itemView.findViewById(R.id.btnAccept) as TextView
        val buttnUpdate = itemView.findViewById(R.id.buttnUpdate) as TextView

        when (mData[position].typeCheck) {

            "1" -> {
                btnAccept.visibility = View.VISIBLE
                imgShow.visibility = View.VISIBLE
                buttnUpdate.visibility = View.GONE
                imgShowRevies.visibility = View.GONE
                requestTxt.setTextColor(context.resources.getColor(R.color.colorWhite))
                textDescription.setTextColor(context.resources.getColor(R.color.colorWhite))
                constraintMain.setBackgroundResource(R.drawable.side_nav_bar_curve)
                imgShow.setImageResource(R.drawable.ic_almost_there)
                textDescription.text =
                   mData[position].business_name + " has invited you to join a VIP subscription. Click below to start saving."
                requestTxt.text = "Exclusive Invitation"

            }
            "2" -> {
                btnAccept.visibility = View.GONE
                imgShow.visibility = View.VISIBLE
                imgShowRevies.visibility = View.GONE
                buttnUpdate.visibility = View.VISIBLE
                requestTxt.setTextColor(context.resources.getColor(R.color.colorHeading))
                textDescription.setTextColor(context.resources.getColor(R.color.colorHeading))
                constraintMain.setBackgroundResource(R.drawable.white_home_popup)
                imgShow.setImageResource(R.drawable.ic_payment_declined)
                requestTxt.text = "Payment Declined"
                textDescription.text =
                    "Your payment for " + mData[position].campaign_name + " subscription for the amount of $" + mData[position].price + " was declined. Please update your payment method in order to continue the services by the Merchant."
            }

            "3" -> {
                btnAccept.visibility = View.VISIBLE
                imgShow.visibility = View.VISIBLE
                buttnUpdate.visibility = View.GONE
                imgShowRevies.visibility = View.GONE
                constraintMain.setBackgroundResource(R.drawable.side_nav_bar_curve)
                requestTxt.setTextColor(context.resources.getColor(R.color.colorWhite))
                textDescription.setTextColor(context.resources.getColor(R.color.colorWhite))
                imgShow.setImageResource(R.drawable.ic_almost_there)
                if (mData[position].c_type == "1") {
                    requestTxt.text = "Invite to join team"
                    textDescription.text = mData[position].business_name + " wants you to join their team. Please click below to view details."
                } else {
                    requestTxt.text = "Employee Request"
                    textDescription.text =
                        "You have a pending request to join " + mData[position].business_name + ". Please click below to view details."
                }
            }

            "4" -> {
                val imagePath = Constant.getPrefs(context).getString(Constant.dataImagePath, "")
                btnAccept.visibility = View.VISIBLE
                imgShowRevies.visibility = View.VISIBLE
                imgShow.visibility = View.GONE
                btnAccept.setBackgroundResource(R.drawable.bg_seller_button_sea_green)
                btnAccept.text = "Rate Now"
                buttnUpdate.visibility = View.GONE
                constraintMain.setBackgroundResource(R.drawable.side_nav_bar_curve)
                requestTxt.setTextColor(context.resources.getColor(R.color.colorWhite))
                textDescription.setTextColor(context.resources.getColor(R.color.colorWhite))
                Glide.with(context).load(imagePath + mData[position].logo)
                    .placeholder(R.mipmap.circular_placeholder).into(imgShowRevies)
                textDescription.text =
                    "How was your experience at " + mData[position].business_name + "? You can provide a rating for this merchant & let us know how you feel."
                requestTxt.text = "Provide Feedback"
            }


            "5" -> {
                val imagePath = Constant.getPrefs(context).getString(Constant.dataImagePath, "")
                btnAccept.visibility = View.VISIBLE
                imgShowRevies.visibility = View.GONE
                imgShow.setImageResource(R.drawable.ic_almost_there)
                imgShow.visibility = View.VISIBLE
                btnAccept.setBackgroundResource(R.drawable.bg_seller_button_sea_green)
                btnAccept.text = "View Details"
                buttnUpdate.visibility = View.GONE
                constraintMain.setBackgroundResource(R.drawable.side_nav_bar_curve)
                requestTxt.setTextColor(context.resources.getColor(R.color.colorWhite))
                textDescription.setTextColor(context.resources.getColor(R.color.colorWhite))
                Glide.with(context).load(imagePath + mData[position].logo)
                    .placeholder(R.mipmap.circular_placeholder).into(imgShowRevies)
                textDescription.text =
                    mData[position].name + " has shared a subscription with you."
                requestTxt.text = "Shared Subscription"
            }

            "7" -> {
                val imagePath = Constant.getPrefs(context).getString(Constant.dataImagePath, "")
                btnAccept.visibility = View.VISIBLE
                imgShowRevies.visibility = View.VISIBLE
                imgShow.visibility = View.GONE
                btnAccept.setBackgroundResource(R.drawable.bg_seller_button_sea_green)
                btnAccept.text = "View Detail"
                buttnUpdate.visibility = View.GONE
                constraintMain.setBackgroundResource(R.drawable.side_nav_bar_curve)
                requestTxt.setTextColor(context.resources.getColor(R.color.colorWhite))
                textDescription.setTextColor(context.resources.getColor(R.color.colorWhite))
                Glide.with(context).load(imagePath + mData[position].logo)
                    .placeholder(R.mipmap.circular_placeholder).into(imgShowRevies)
                textDescription.text = "Get ready to save big! Start by redeeming your subscription with " + mData[position].business_name + " for " +  mData[position].campaign_name
                requestTxt.text = "You're Upscribbed!"
            }

            "8" -> {
                val imagePath = Constant.getPrefs(context).getString(Constant.dataImagePath, "")
                btnAccept.visibility = View.VISIBLE
                imgShowRevies.visibility = View.GONE
                imgShow.visibility = View.VISIBLE
                btnAccept.setBackgroundResource(R.drawable.bg_seller_button_sea_green)
                btnAccept.text = "View Details"
                buttnUpdate.visibility = View.GONE
                constraintMain.setBackgroundResource(R.drawable.side_nav_bar_curve)
                requestTxt.setTextColor(context.resources.getColor(R.color.colorWhite))
                textDescription.setTextColor(context.resources.getColor(R.color.colorWhite))
//                Glide.with(context).load(imagePath + mData[position].logo)
//                    .placeholder(R.mipmap.circular_placeholder).into(imgShowRevies)
                textDescription.text =
                    mData[position].name + " has unshared a subscription with you."
                requestTxt.text = "Unshared Subscription"
            }

            "9" -> {
                val imagePath = Constant.getPrefs(context).getString(Constant.dataImagePath, "")
                btnAccept.visibility = View.VISIBLE
                imgShowRevies.visibility = View.VISIBLE
                imgShow.visibility = View.GONE
                btnAccept.setBackgroundResource(R.drawable.bg_selected)
                btnAccept.text = "Proceed"
                buttnUpdate.visibility = View.GONE
                constraintMain.setBackgroundResource(R.drawable.side_nav_bar_curve)
                requestTxt.setTextColor(context.resources.getColor(R.color.colorWhite))
                textDescription.setTextColor(context.resources.getColor(R.color.colorWhite))
                Glide.with(context).load(imagePath + mData[position].logo)
                    .placeholder(R.mipmap.circular_placeholder).into(imgShowRevies)
                textDescription.text =
                    "You’ve reached your subscription limit for " + mData[position].business_name + ". Learn how to increase your subscription limit with our “FAQs”"
                mData[position].business_name + " has unshared a subscription with you."
                requestTxt.text = "Subscription Limit Reached"
            }
            "11" -> {
                val imagePath = Constant.getPrefs(context).getString(Constant.dataImagePath, "")
                btnAccept.visibility = View.VISIBLE
                imgShowRevies.visibility = View.GONE
                imgShow.visibility = View.VISIBLE
                imgShow.setImageResource(R.drawable.ic_dollar_min)
                btnAccept.setBackgroundResource(R.drawable.bg_seller_button_sea_green)
                btnAccept.text = "Proceed"
                buttnUpdate.visibility = View.GONE
                constraintMain.setBackgroundResource(R.drawable.side_nav_bar_curve)
                requestTxt.setTextColor(context.resources.getColor(R.color.colorWhite))
                textDescription.setTextColor(context.resources.getColor(R.color.colorWhite))
                Glide.with(context).load(imagePath + mData[position].logo)
                    .placeholder(R.mipmap.circular_placeholder).into(imgShowRevies)
                textDescription.text = "Great news! You are now able to join. Sign up while you still can!"
                mData[position].business_name + " has unshared a subscription with you."
                requestTxt.text = "You can join"
            }

        }

        btnAccept.setOnClickListener {
            when (mData[position].typeCheck) {
                "1" -> {
                    listener.clickInvitation(mData[position])
                }
                "2" -> {
                    listener.addCard(mData[position])
                }
                "3" -> {
                    if (mData[position].c_type == "1") {
                        listener.clickJoin(mData[position])
                    } else {
                        listener.clickJoinTeam(mData[position])
                    }
                }
                "4" -> {
                    listener.rateMerchant(mData[position])
                }

                "5" -> {
                    listener.subscriptionDetails(mData[position])
                }
                "7" -> {
                    listener.orderSubscription(mData[position])
                }
                "8" -> {
                    listener.unsharedSubscription(mData[position])
                }
                "9" -> {
                    listener.learnFaq(mData[position])
                }
                "11" -> {
                    listener.limitExceed(mData[position])
                }
            }
        }


        container.addView(itemView)
        return itemView
    }


    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View?)
    }


}