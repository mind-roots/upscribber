package com.upscribber.loginSignUpScreen

import android.app.ProgressDialog
import android.content.ContentValues
import android.content.Intent
import android.os.Bundle
import android.util.TypedValue
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager
import com.upscribber.R
import com.upscribber.login.LoginActivity
import com.upscribber.signUp.SignUpActivity
import me.relex.circleindicator.CircleIndicator
import me.relex.circleindicator.Config
import com.facebook.login.LoginResult
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.ViewModelProviders
import com.facebook.*
import com.facebook.login.LoginManager
import com.google.firebase.iid.FirebaseInstanceId
import com.upscribber.commonClasses.Constant
import com.upscribber.dashboard.DashboardActivity
import com.upscribber.signUp.SignUpViewModel
import kotlinx.android.synthetic.main.activity_login_or_signup.*
import java.util.*


class LoginorSignUpActivity : AppCompatActivity() {

    private lateinit var textView28: TextView
    private lateinit var tv_login: TextView
    private lateinit var textView331: TextView
    private lateinit var indicator_login: CircleIndicator
    var viewPager: ViewPager? = null
    var images = intArrayOf(R.drawable.gif_splash, R.drawable.gif_splash2, R.drawable.gif_splash3, R.drawable.gif_splash4, R.drawable.gif_splash5)
    var myCustomPagerAdapter: CustomPagerAdapter? = null
    private var callbackManager: CallbackManager? = null
    private var url: String = ""
    lateinit var dialog: ProgressDialog
    var userEmail: String = ""
    var fbId: String = ""
    lateinit var mViewModel: SignUpViewModel
    var userName: String = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_or_signup)
        mViewModel = ViewModelProviders.of(this)[SignUpViewModel::class.java]
        FacebookSdk.sdkInitialize(FacebookSdk.getApplicationContext())
        callbackManager = CallbackManager.Factory.create()
        val accessToken = AccessToken.getCurrentAccessToken()
        FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener(
            this
        ) { instanceIdResult ->
            val newToken = instanceIdResult.token
            Log.e("newToken", newToken)
            val editor = Constant.getPrefs(this).edit()
            editor.putString("deviceId", newToken)
            Log.d(ContentValues.TAG, "Refreshed token: $newToken")
            editor.apply()
        }
        dialog = ProgressDialog(this)
        initilization()
        clickEvents()

        myCustomPagerAdapter = CustomPagerAdapter(this,images)
        viewPager!!.adapter = myCustomPagerAdapter
        settingIndicators()
        apiImplimentation()
        pagerListener()
        getFbLogin()
        observerInit()

    }

    private fun apiImplimentation() {
        mViewModel.getCountryNameCodes()

    }

    private fun observerInit() {
        mViewModel.getVerifyFacebook().observe(this, androidx.lifecycle.Observer {
            if (it.status == "true") {
                dialog.dismiss()
                startActivity(
                    Intent(this@LoginorSignUpActivity, SignUpActivity::class.java)
                        .putExtra("name", userName)
                        .putExtra("email", userEmail)
                        .putExtra("id", fbId)
                        .putExtra("login", "signup")
                )
            }
        })

        mViewModel.getStatus().observe(this, androidx.lifecycle.Observer {
            dialog.dismiss()
            if (it.msg == "User exists.") {
                startActivity(Intent(this@LoginorSignUpActivity, DashboardActivity::class.java))
            } else {
                Toast.makeText(this, it.msg, Toast.LENGTH_LONG).show()
            }
        })

    }

    private fun pagerListener() {
        viewPager!!.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {/*empty*/

                when (position) {
                    0 -> textView331.text = "Manage all your\nsubscriptions in one place"
                    1 -> textView331.text = "Discover exclusive\nsubscriptions and save"
                    2 -> textView331.text = "Invite your favorite\nbusinesses to Upscribbr"
                    3 -> textView331.text = "Share or gift your\nunused subscriptions"
                    else -> textView331.text = "Become a seller\n& advertise for free"
                }


            }

            override fun onPageSelected(position: Int) {
                when (position) {
                    0 -> textView331.text = "Manage all your\nsubscriptions in one place"
                    1 -> textView331.text = "Discover exclusive\nsubscriptions and save"
                    2 -> textView331.text = "Invite your favorite\nbusinesses to Upscribbr"
                    3 -> textView331.text = "Share or gift your\nunused subscriptions"
                    else -> textView331.text = "Become a seller\n& advertise for free"
                }
            }

            override fun onPageScrollStateChanged(state: Int) {/*empty*/
            }
        })

    }

//    ------------------------------------------------------------facebook Login---------------------------------------------------------------------------//

    fun getFbLogin() {

        tvFbLogin.setOnClickListener {
            LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"))
            LoginManager.getInstance().registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
                override fun onSuccess(result: LoginResult?) {
                    val accessToken = result!!.accessToken
                    val request = GraphRequest.newMeRequest(accessToken)
                    { user, graphResponse ->

                        url =
                            "https://graph.facebook.com/${user.optString("id")}/picture?type=large&redirect=true&width=600&height=600"
                        mViewModel.getLoginId(user.optString("id"))
                        userName = user.optString("name")
                        userEmail = user.optString("email")
                        fbId = user.optString("id")
                        dialog.setMessage("Loading...")
                        dialog.show()
                        dialog.setCancelable(false)
                    }.executeAsync()

                }


                override fun onCancel() {
                }

                override fun onError(error: FacebookException?) {
                    dialog.dismiss()
                    Log.i("FACEBOOK Error: ", error!!.message)
                        if (AccessToken.getCurrentAccessToken() != null) {
                            LoginManager.getInstance().logOut()
                        }
                    }


            })
        }


    }

    override fun onResume() {
        super.onResume()
        dialog.dismiss()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        callbackManager!!.onActivityResult(requestCode, resultCode, data)
    }


    private fun initilization() {
        viewPager = findViewById(R.id.viewPager2)
//        invitecode = findViewById(R.id.invitecode)
        indicator_login = findViewById(R.id.indicator_login)
        textView331 = findViewById(R.id.textView331)
        textView28 = findViewById(R.id.phoneNumber)
        tv_login = findViewById(R.id.tv_login)

    }

    private fun settingIndicators() {
        val indicatorWidth = (TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, 10f,
            resources.displayMetrics
        ) + 0.5f).toInt()
        val indicatorHeight = (TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, 4f,
            resources.displayMetrics
        ) + 0.5f).toInt()
        val indicatorMargin = (TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, 4f,
            resources.displayMetrics
        ) + 0.5f).toInt()

        val config = Config.Builder().width(indicatorWidth)
            .height(indicatorHeight)
            .margin(indicatorMargin)
//            .animatorReverse(R.animator.indicator_animator_reverse)
            .drawable(R.drawable.black_radius_square)
            .drawableUnselected(R.drawable.bg_grey)
            .build()
        indicator_login.initialize(config)
        viewPager!!.adapter = CustomPagerAdapter(this,images)
        indicator_login.setViewPager(viewPager)
    }


    private fun clickEvents() {
        textView28.setOnClickListener {
            val intent = Intent(applicationContext, SignUpActivity::class.java)
            startActivity(intent)
        }
        tv_login.setOnClickListener {
            val intent = Intent(applicationContext, LoginActivity::class.java)
            startActivity(intent)
        }

//        invitecode.setOnClickListener{
//            val intent = Intent(applicationContext, InviteCodeActivity::class.java)
//            startActivity(intent)
//        }

    }

}
