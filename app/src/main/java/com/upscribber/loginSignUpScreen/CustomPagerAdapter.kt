package com.upscribber.loginSignUpScreen

import android.content.Context
import android.view.View
import androidx.viewpager.widget.PagerAdapter
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.upscribber.R

class CustomPagerAdapter(var context: Context,var mData: IntArray) : PagerAdapter(){


    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun getCount(): Int {
        return mData.size
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val itemView = LayoutInflater.from(container.context)
            .inflate(R.layout.image_adapter_layout, container, false)

        val img_pager = itemView.findViewById(R.id.img_pager) as ImageView

        Glide.with(context).load(mData[position]).into(img_pager)

//        img_pager.setImageResource(mData[position])
        container.addView(itemView)


        return itemView
    }



    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View?)
    }
}