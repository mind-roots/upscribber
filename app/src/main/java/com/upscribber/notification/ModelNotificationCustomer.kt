package com.upscribber.notification

import android.os.Parcel
import android.os.Parcelable

class ModelNotificationCustomer() : Parcelable{

    var rtype: String = ""
    var redeem_id: String = ""
    var team_member: String = ""
    var last_4: String = ""
    var payment_method_name: String = ""
    var payment_type: String = ""
    var business_phone: String = ""
    var remaining_visits: String = ""
    var price: String = ""
    var location_id: String = ""
    var assign_to: String = ""
    var frequency_value: String = ""
    var reqtype: String = ""
    var name: String = ""
    var contact_no: String = ""
    var stripe_fee: String = ""
    var merchant_amount: String = ""
    var admin_amount: String = ""
    var redeem_count: String = ""
    var subscription_id: String = ""
    var redeemtion_cycle_qty: String = ""
    var customer_name: String = ""
    var recipient_id: String = ""
    var campaign_name: String = ""
    var unit: String = ""
    var total_amount: String = ""
    var description: String = ""
    var free_trial: String = ""
    var frequency_type: String = ""
    var sender_id: String = ""
    var read: String = ""
    var reference_id: String = ""
    var notification_type: String = ""
    var created_at: String = ""
    var updated_at: String = ""
    var typeCheck: String = ""
    var id: String = ""
    var action_taken: String =""
    var type2: String = ""
    var steps: String = ""
    var msgg: String = ""
    var status: String = ""
    var business_id: String = ""
    var business_name: String = ""
    var city: String = ""
    var logo: String = ""
    var msg: String = ""
    var state: String = ""
    var street: String = ""
    var type: String = ""
    var type1 : String =""
    var order_id : String = ""

    var transaction_id : String = ""
    var customer_id : String = ""
    var transaction_status : String = ""
    var card_id : String = ""
    var merchant_id : String = ""
    var feature_image : String = ""
    var profile_image : String = ""
    var order_status : String = ""
    var is_refunded : String = ""
    var campaign_id : String = ""
    var staff_id : String = ""
    var transaction_type : String = ""
    var count : Int = 0

    constructor(parcel: Parcel) : this() {
        rtype = parcel.readString()
        redeem_id = parcel.readString()
        team_member = parcel.readString()
        last_4 = parcel.readString()
        payment_method_name = parcel.readString()
        payment_type = parcel.readString()
        business_phone = parcel.readString()
        remaining_visits = parcel.readString()
        price = parcel.readString()
        location_id = parcel.readString()
        assign_to = parcel.readString()
        frequency_value = parcel.readString()
        reqtype = parcel.readString()
        name = parcel.readString()
        contact_no = parcel.readString()
        stripe_fee = parcel.readString()
        merchant_amount = parcel.readString()
        admin_amount = parcel.readString()
        redeem_count = parcel.readString()
        subscription_id = parcel.readString()
        redeemtion_cycle_qty = parcel.readString()
        customer_name = parcel.readString()
        recipient_id = parcel.readString()
        campaign_name = parcel.readString()
        unit = parcel.readString()
        total_amount = parcel.readString()
        description = parcel.readString()
        free_trial = parcel.readString()
        frequency_type = parcel.readString()
        sender_id = parcel.readString()
        read = parcel.readString()
        reference_id = parcel.readString()
        notification_type = parcel.readString()
        created_at = parcel.readString()
        updated_at = parcel.readString()
        typeCheck = parcel.readString()
        id = parcel.readString()
        action_taken = parcel.readString()
        type2 = parcel.readString()
        steps = parcel.readString()
        msgg = parcel.readString()
        status = parcel.readString()
        business_id = parcel.readString()
        business_name = parcel.readString()
        city = parcel.readString()
        logo = parcel.readString()
        msg = parcel.readString()
        state = parcel.readString()
        street = parcel.readString()
        type = parcel.readString()
        type1 = parcel.readString()
        order_id = parcel.readString()
        transaction_id = parcel.readString()
        customer_id = parcel.readString()
        transaction_status = parcel.readString()
        card_id = parcel.readString()
        merchant_id = parcel.readString()
        feature_image = parcel.readString()
        profile_image = parcel.readString()
        order_status = parcel.readString()
        is_refunded = parcel.readString()
        campaign_id = parcel.readString()
        staff_id = parcel.readString()
        transaction_type = parcel.readString()
        count = parcel.readInt()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(rtype)
        parcel.writeString(redeem_id)
        parcel.writeString(team_member)
        parcel.writeString(last_4)
        parcel.writeString(payment_method_name)
        parcel.writeString(payment_type)
        parcel.writeString(business_phone)
        parcel.writeString(remaining_visits)
        parcel.writeString(price)
        parcel.writeString(location_id)
        parcel.writeString(assign_to)
        parcel.writeString(frequency_value)
        parcel.writeString(reqtype)
        parcel.writeString(name)
        parcel.writeString(contact_no)
        parcel.writeString(stripe_fee)
        parcel.writeString(merchant_amount)
        parcel.writeString(admin_amount)
        parcel.writeString(redeem_count)
        parcel.writeString(subscription_id)
        parcel.writeString(redeemtion_cycle_qty)
        parcel.writeString(customer_name)
        parcel.writeString(recipient_id)
        parcel.writeString(campaign_name)
        parcel.writeString(unit)
        parcel.writeString(total_amount)
        parcel.writeString(description)
        parcel.writeString(free_trial)
        parcel.writeString(frequency_type)
        parcel.writeString(sender_id)
        parcel.writeString(read)
        parcel.writeString(reference_id)
        parcel.writeString(notification_type)
        parcel.writeString(created_at)
        parcel.writeString(updated_at)
        parcel.writeString(typeCheck)
        parcel.writeString(id)
        parcel.writeString(action_taken)
        parcel.writeString(type2)
        parcel.writeString(steps)
        parcel.writeString(msgg)
        parcel.writeString(status)
        parcel.writeString(business_id)
        parcel.writeString(business_name)
        parcel.writeString(city)
        parcel.writeString(logo)
        parcel.writeString(msg)
        parcel.writeString(state)
        parcel.writeString(street)
        parcel.writeString(type)
        parcel.writeString(type1)
        parcel.writeString(order_id)
        parcel.writeString(transaction_id)
        parcel.writeString(customer_id)
        parcel.writeString(transaction_status)
        parcel.writeString(card_id)
        parcel.writeString(merchant_id)
        parcel.writeString(feature_image)
        parcel.writeString(profile_image)
        parcel.writeString(order_status)
        parcel.writeString(is_refunded)
        parcel.writeString(campaign_id)
        parcel.writeString(staff_id)
        parcel.writeString(transaction_type)
        parcel.writeInt(count)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ModelNotificationCustomer> {
        override fun createFromParcel(parcel: Parcel): ModelNotificationCustomer {
            return ModelNotificationCustomer(parcel)
        }

        override fun newArray(size: Int): Array<ModelNotificationCustomer?> {
            return arrayOfNulls(size)
        }
    }


}