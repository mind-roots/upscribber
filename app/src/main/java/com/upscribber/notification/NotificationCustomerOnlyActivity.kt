package com.upscribber.notification

import android.os.Bundle
import android.view.MenuItem
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.R
import com.upscribber.notification.notifications.AdapterNotificationCustomer


class NotificationCustomerOnlyActivity :  AppCompatActivity()  {
    lateinit var rvNotification : RecyclerView
    lateinit var toolbar: Toolbar
    lateinit var toolbarTitle: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notification_customer_only)

        rvNotification = findViewById(R.id.rvNotification)
        toolbar = findViewById(R.id.include)
        toolbarTitle = findViewById(R.id.title4)
        setSupportActionBar(toolbar)
        title = ""
        toolbarTitle.text = "Notifications"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)

        setAdapter()

    }


    private fun setAdapter() {
        rvNotification.layoutManager = LinearLayoutManager(this)
        rvNotification.adapter =
            AdapterNotificationCustomer(this)
    }



    private fun getList(): ArrayList<ModelNotificationCustomer> {


        val arrayList: ArrayList<ModelNotificationCustomer> = ArrayList()



        return arrayList

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }


}