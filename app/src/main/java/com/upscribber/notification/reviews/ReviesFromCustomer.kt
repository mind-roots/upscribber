package com.upscribber.notification.reviews

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.view.Gravity
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.google.android.material.chip.Chip
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.databinding.ActivityReviesFromCustomerBinding
import com.upscribber.notification.ModelNotificationCustomer
import kotlinx.android.synthetic.main.thumbs_down_layout.*
import kotlinx.android.synthetic.main.thumbs_down_layout.view.*
import kotlinx.android.synthetic.main.thumbs_up_layout.view.*
import com.upscribber.home.ModelHomeDataSubscriptions
import kotlinx.android.synthetic.main.activity_revies_from_customer.*


class ReviesFromCustomer : AppCompatActivity(),
    AdapterReviewsFromCustomers.selectionReviews {

    private var pos: Int = -1
    private var chipPos: Int = 0
    private var once = 0
    private lateinit var backgroundChange: ArrayList<ModelReviewsFromCustomers>
    lateinit var mBinding: ActivityReviesFromCustomerBinding
    lateinit var mViewModel: ViewModelReviewFromCustomers
    lateinit var mAdapter: AdapterReviewsFromCustomers
    private var status: Boolean = true
    private var status1: Int = -1
    var arrayList = ArrayList<PositiveModel>()
    var arrayListNegative = ArrayList<PositiveModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_revies_from_customer)
        mViewModel = ViewModelProviders.of(this)[ViewModelReviewFromCustomers::class.java]
        mViewModel.getData()
        setToolbar()
        clickListeners()
        observer()

        if (intent.hasExtra("model")) {
           val modelNotification = intent.getParcelableExtra<ModelNotificationCustomer>("model")
            textView306.text = "How was your service with " + modelNotification.business_name
            val imagePath = Constant.getPrefs(this).getString(Constant.dataImagePath, "")
            Glide.with(this).load(imagePath + modelNotification.logo).placeholder(R.mipmap.circular_placeholder).into(mBinding.logo)
        }else if (intent.hasExtra("modelReviews")){
           val modelNotification1 = intent.getParcelableExtra<ModelHomeDataSubscriptions>("modelReviews")
            textView306.text = "How was your service with " + modelNotification1.business_name
            val imagePath = Constant.getPrefs(this).getString(Constant.dataImagePath, "")
            Glide.with(this).load(imagePath + modelNotification1.logo).placeholder(R.mipmap.circular_placeholder).into(mBinding.logo)
        }
    }

    private fun setToolbar() {
        setSupportActionBar(mBinding.include7.toolbar)
        val actionbar: ActionBar? = supportActionBar
        actionbar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)
        }
        title = ""
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }


    private fun clickListeners() {
        mBinding.thumbUp.setOnClickListener {
            status1 = 0
            mBinding.layoutThumbsDown.visibility = View.GONE
            mBinding.layoutThumbsUp.visibility = View.VISIBLE
            mBinding.layoutThumbsUp.editTextThank.visibility = View.GONE
            mBinding.layoutThumbsUp.textCompliment.visibility = View.GONE
            mBinding.thumbDown.setImageResource(R.drawable.ic_thumbs_down_grey)
            mBinding.thumbUp.setImageResource(R.drawable.ic_thumbs_up)
            if (status) {
                status = false
//                mBinding.thumbUp.setImageResource(R.drawable.ic_thumbs_up)
                mBinding.layoutThumbsUp.textCompliment.visibility = View.GONE
                mBinding.layoutThumbsUp.recyclerReviews.visibility = View.VISIBLE
                mBinding.layoutThumbsUp.textCompliment1.visibility = View.VISIBLE
                setAdapter()

            } else {
                status = true
                mBinding.thumbUp.setImageResource(R.drawable.ic_thumbs_up_grey)
                mBinding.layoutThumbsUp.textCompliment.visibility = View.VISIBLE
                mBinding.layoutThumbsUp.recyclerReviews.visibility = View.GONE
                mBinding.layoutThumbsUp.textCompliment1.visibility = View.GONE
//
            }
        }

        mBinding.layoutThumbsUp.textCompliment1.setOnClickListener {

            mBinding.layoutThumbsUp.editTextThank.visibility = View.VISIBLE
            mBinding.layoutThumbsUp.textCompliment1.visibility = View.GONE
        }

        mBinding.thumbDown.setOnClickListener {
            status = true
            status1 = 1
            mBinding.layoutThumbsDown.visibility = View.VISIBLE
            mBinding.layoutThumbsUp.visibility = View.GONE
            mBinding.thumbDown.setImageResource(R.drawable.ic_thumbs_down)
            mBinding.thumbUp.setImageResource(R.drawable.ic_thumbs_up_grey)
            if (once == 0) {
                chipData()
            }
            once = 1

        }

        mBinding.button.setOnClickListener {
            if (status1 == 0) {

                if (pos != -1) {
                    val positiveTagList = ArrayList<String>()
                    var positiveSelectedMessages = ""
                    if (backgroundChange.isNotEmpty()) {
                        for (k in 0 until backgroundChange.size) {
                            if (backgroundChange[k].status) {
                                positiveTagList.add(arrayList[k].id)
                            }
                        }
                        positiveSelectedMessages = TextUtils.join(",", positiveTagList)
                    }
                    val message = mBinding.layoutThumbsUp.editTextThank.text.toString()
                    if (intent.hasExtra("model")) {
                        val modelNotification = intent.getParcelableExtra<ModelNotificationCustomer>("model")
                        mViewModel.saveReview(modelNotification.order_id, "2", positiveSelectedMessages, message,modelNotification.id)
                    }else if (intent.hasExtra("modelReviews")){
                        val modelNotification1 = intent.getParcelableExtra<ModelHomeDataSubscriptions>("modelReviews")
                        mViewModel.saveReview(modelNotification1.order_id, "2", positiveSelectedMessages, message,"")
                    }


                    startActivity(Intent(this, ReviewConfirmationScreen::class.java))
                    finish()
                } else {
                    Toast.makeText(this, "Please select message", Toast.LENGTH_SHORT).show()
                }


            } else if (status1 == 1) {

                if (chipPos >= 1) {
                    val negativeTagList = ArrayList<String>()
                    var negativeSelectedMessages = ""
                    if (arrayListNegative.isNotEmpty()) {
                        for (k in 0 until arrayListNegative.size) {
                            if (arrayListNegative[k].status) {
                                negativeTagList.add(arrayList[k].id)
                            }
                        }
                        negativeSelectedMessages = TextUtils.join(",", negativeTagList)
                    }
                    val id = arrayListNegative[chipPos].id
                    val message = mBinding.layoutThumbsDown.Feedback.text.toString()
                    if (intent.hasExtra("model")) {
                        val modelNotification = intent.getParcelableExtra<ModelNotificationCustomer>("model")
                        mViewModel.saveReview(modelNotification.order_id, "1", negativeSelectedMessages, message,modelNotification.id)
                    }else if (intent.hasExtra("modelReviews")){
                        val modelNotification1 = intent.getParcelableExtra<ModelHomeDataSubscriptions>("modelReviews")
                        mViewModel.saveReview(modelNotification1.order_id, "1", negativeSelectedMessages, message,"")
                    }

                    startActivity(Intent(this, ReviewConfirmationScreen::class.java).putExtra("ThumbsDown", "ThumbsUp"))
                    finish()
                } else {
                    Toast.makeText(this, "Please select message", Toast.LENGTH_SHORT).show()
                }

            }

        }


    }

    fun observer() {
        mViewModel.getPositive().observe(this, Observer {
            arrayList = it
        })
        mViewModel.getNegative().observe(this, Observer {
            arrayListNegative = it
        })


    }

    private fun chipData() {
        val array_name = ArrayList<String>()

        for (i in 0 until arrayListNegative.size) {
            array_name.add(arrayListNegative[i].name)
        }
        for (index in array_name.indices) {
            val chip = Chip(mBinding.layoutThumbsDown.rvTags.context)
            chip.text = "${array_name[index]}"

            // necessary to get single selection working
            chip.isClickable = true
            chip.isCheckable = false
            rvTags.isSingleSelection = true
            chip.chipBackgroundColor =
                resources.getColorStateList(R.color.chipColor)
            chip.setTextColor(resources.getColor(R.color.colorCardDesc))
//            chip.chipStartPadding = 10.0f
            chip.chipEndPadding = 20.0f
            chip.gravity = Gravity.CENTER
            mBinding.layoutThumbsDown.rvTags.addView(chip)
            chip.setOnClickListener {

                if (!arrayListNegative[index].status) {
                    chipPos++
                    arrayListNegative[index].status = true
                    chip.chipBackgroundColor = resources.getColorStateList(R.color.colorAccent)
                    chip.setTextColor(resources.getColor(R.color.colorWhite))
                } else {
                    chipPos--
                    arrayListNegative[index].status = false
                    chip.chipBackgroundColor =
                        resources.getColorStateList(R.color.chipColor)
                    chip.setTextColor(resources.getColor(R.color.colorCardDesc))
                }
            }
            rvTags.isSingleSelection = true
            rvTags.setOnCheckedChangeListener { chipGroup, i ->
                val chip1 = chipGroup.findViewById<Chip>(i)
                if (chip1 != null) {
                    Toast.makeText(this, "hguyguy", Toast.LENGTH_LONG).show()
                }
            }
            mBinding.layoutThumbsDown.rvTags.isSingleSelection = true
        }

    }

    private fun setAdapter() {
        mBinding.layoutThumbsUp.recyclerReviews.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        mBinding.layoutThumbsUp.recyclerReviews.isNestedScrollingEnabled = false
        mAdapter = AdapterReviewsFromCustomers(this, arrayList)
        mBinding.layoutThumbsUp.recyclerReviews.adapter = mAdapter

        backgroundChange = mViewModel.getList()
        mAdapter.update(backgroundChange)
    }


    override fun ReviewSelection(position: Int) {

        pos = position
        val model = backgroundChange[position]
        model.status = !model.status

        //            for (notSelected in backgroundChange) {
//                notSelected.status = false
//            }
        backgroundChange[position] = model
        mAdapter.notifyDataSetChanged()
    }


    override fun onResume() {
        super.onResume()
        val sharedPreferences1 = getSharedPreferences("reviews", Context.MODE_PRIVATE)
        if (!sharedPreferences1.getString("reviews", "").isEmpty()) {
            finish()
        }
    }
}
