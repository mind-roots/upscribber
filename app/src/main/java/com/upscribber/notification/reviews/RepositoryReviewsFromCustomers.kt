package com.upscribber.notification.reviews

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.upscribber.commonClasses.Constant
import com.upscribber.commonClasses.ModelStatusMsg
import com.upscribber.commonClasses.WebServicesCustomers
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class RepositoryReviewsFromCustomers(var application: Application) {


    val mDataPositive = MutableLiveData<ArrayList<PositiveModel>>()
    val mDataNegative = MutableLiveData<ArrayList<PositiveModel>>()
    val mDataFailure = MutableLiveData<ModelStatusMsg>()



    fun getListReviews(): ArrayList<ModelReviewsFromCustomers> {

        val arrayList: ArrayList<ModelReviewsFromCustomers> = ArrayList()

        val modelReviews1 = ModelReviewsFromCustomers()
        modelReviews1.imageCategory1 = "http://cloudart.com.au/projects/upscribbr/assets/img/m1.png"
        modelReviews1.numberOfReviews1 = "50"
        modelReviews1.reviewCategory1 = "Great Seller"
        modelReviews1.status = false
        arrayList.add(modelReviews1)

        val modelReviews2 = ModelReviewsFromCustomers()
        modelReviews2.imageCategory1 = "http://cloudart.com.au/projects/upscribbr/assets/img/m2.png"
        modelReviews2.numberOfReviews1 = "150"
        modelReviews2.reviewCategory1 = "Great Discount"
        modelReviews2.status = false
        arrayList.add(modelReviews2)

        val modelReviews3 = ModelReviewsFromCustomers()
        modelReviews3.imageCategory1 = "http://cloudart.com.au/projects/upscribbr/assets/img/m3.png"
        modelReviews3.numberOfReviews1 = "350"
        modelReviews3.reviewCategory1 = "Awesome"
        modelReviews3.status = false
        arrayList.add(modelReviews3)

        val modelReviews4 = ModelReviewsFromCustomers()
        modelReviews4.imageCategory1 = "http://cloudart.com.au/projects/upscribbr/assets/img/m1.png"
        modelReviews4.numberOfReviews1 = "90"
        modelReviews4.reviewCategory1 = "Super Seller"
        modelReviews4.status = false
        arrayList.add(modelReviews4)

        val modelReviews5 = ModelReviewsFromCustomers()
        modelReviews5.imageCategory1 = "http://cloudart.com.au/projects/upscribbr/assets/img/m2.png"
        modelReviews5.numberOfReviews1 = "500"
        modelReviews5.reviewCategory1 = "Amazing"
        modelReviews5.status = false
        arrayList.add(modelReviews5)

        return arrayList

    }


    fun getRatingResponse() {
        val auth = Constant.getPrefs(application).getString(Constant.auth_code, "")

        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.getRatingTags(auth)
        call.enqueue(object : Callback<ResponseBody> {


            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val data = json.optJSONObject("data")
                        val positive = data.optJSONArray("positive")
                        val arrayListPositive = ArrayList<PositiveModel>()
                        val arrayListNegative = ArrayList<PositiveModel>()
                        for (i in 0 until positive.length()) {
                            val ratingData = positive.optJSONObject(i)
                            val positiveModel = PositiveModel()
                            positiveModel.id = ratingData.optString("id")
                            positiveModel.name = ratingData.optString("name")
                            positiveModel.type = ratingData.optString("type")
                            positiveModel.count = ratingData.optString("count")
                            arrayListPositive.add(positiveModel)
                        }

                        mDataPositive.value = arrayListPositive

                        val negative = data.optJSONArray("negative")
                        for (i in 0 until negative.length()) {
                            val ratingData = negative.optJSONObject(i)
                            val negativeModel = PositiveModel()
                            negativeModel.id = ratingData.optString("id")
                            negativeModel.name = ratingData.optString("name")
                            negativeModel.type = ratingData.optString("type")
                            negativeModel.count = ratingData.optString("count")
                            arrayListNegative.add(negativeModel)
                        }

                        mDataNegative.value = arrayListNegative

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {

            }
        })


    }


    fun positiveData():MutableLiveData<ArrayList<PositiveModel>>{
        return mDataPositive
    }


    fun negativeData(): MutableLiveData<ArrayList<PositiveModel>> {
        return mDataNegative
    }


    fun saveReview(
        orderId: String,
        type: String,
        messageId: String,
        message: String,
        id: String
    ) {

        val auth = Constant.getPrefs(application).getString(Constant.auth_code, "")

        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.saveReview(auth, orderId, type, messageId, message,id)
        call.enqueue(object : Callback<ResponseBody> {


            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.msg = "Network Error!"
                modelStatus.status = "false"
                mDataFailure.value = modelStatus

            }


        })


    }

    fun getmDataFailure(): LiveData<ModelStatusMsg> {
        return mDataFailure
    }

}
