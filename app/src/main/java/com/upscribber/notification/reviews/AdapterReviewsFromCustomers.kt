package com.upscribber.notification.reviews

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.databinding.RecyclerListReviesFromCustomersBinding
import kotlinx.android.synthetic.main.recycler_list_revies_from_customers.view.*

class AdapterReviewsFromCustomers(var context: Context, var arrayList: ArrayList<PositiveModel>) :
    RecyclerView.Adapter<AdapterReviewsFromCustomers.MyViewHolder>() {

    var listener = context as selectionReviews
    private lateinit var binding: RecyclerListReviesFromCustomersBinding
    private var items = ArrayList<ModelReviewsFromCustomers>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        binding = RecyclerListReviesFromCustomersBinding.inflate(LayoutInflater.from(context), parent, false)
        return MyViewHolder(binding)

    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = items[position]
        holder.bind(model)

        if (arrayList.size > 0) {
            holder.itemView.categoryReview.text = arrayList[position].name

        }


        if (model.status) {
            holder.itemView.imageView.visibility = View.VISIBLE
        } else {
            holder.itemView.imageView.visibility = View.INVISIBLE
        }

        holder.itemView.contraint.setOnClickListener {
            listener.ReviewSelection(position)

        }

    }

    class MyViewHolder(var binding: RecyclerListReviesFromCustomersBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(obj: ModelReviewsFromCustomers) {
            binding.model = obj
            binding.executePendingBindings()
        }
    }

    fun update(items: ArrayList<ModelReviewsFromCustomers>) {
        this.items = items
        notifyDataSetChanged()
    }

    interface selectionReviews {
        fun ReviewSelection(position: Int)
    }


}