package com.upscribber.notification.reviews

class PositiveModel {

    var id: String = ""
    var name: String = ""
    var type: String = ""
    var count: String = ""
    var status : Boolean = false
}