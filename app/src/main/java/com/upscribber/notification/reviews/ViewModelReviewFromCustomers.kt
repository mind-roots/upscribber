package com.upscribber.notification.reviews

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.upscribber.commonClasses.ModelStatusMsg

class ViewModelReviewFromCustomers(application: Application) : AndroidViewModel(application) {


    var repositoryReviewsFromCustomers: RepositoryReviewsFromCustomers =
        RepositoryReviewsFromCustomers(application)

    fun getList(): ArrayList<ModelReviewsFromCustomers> {
        return repositoryReviewsFromCustomers.getListReviews()

    }

    fun getData() {
        repositoryReviewsFromCustomers.getRatingResponse()

    }

    fun getPositive(): MutableLiveData<ArrayList<PositiveModel>> {
        return repositoryReviewsFromCustomers.positiveData()

    }


    fun getNegative(): MutableLiveData<ArrayList<PositiveModel>> {

        return repositoryReviewsFromCustomers.negativeData()

    }

    fun getStatus(): LiveData<ModelStatusMsg> {
        return repositoryReviewsFromCustomers.getmDataFailure()
    }


    fun saveReview(
        orderId: String,
        type: String,
        messageId: String,
        message: String,
        id: String
    ) {
        repositoryReviewsFromCustomers.saveReview(orderId, type, messageId, message,id)
    }

}
