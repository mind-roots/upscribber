package com.upscribber.notification.reviews

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.databinding.ActivityReviewConfirmationScreenBinding

class ReviewConfirmationScreen : AppCompatActivity() {

    lateinit var mBinding : ActivityReviewConfirmationScreenBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this,R.layout.activity_review_confirmation_screen)
        if (intent.hasExtra("ThumbsDown")){
            mBinding.imageView56.setImageResource(R.drawable.ic_successful_redeem)
            mBinding.textView93.text = "We enjoyed your Feedback!"
            mBinding.textView339.text = "We will work on making super your experience is better next time"
            mBinding.textView340.visibility = View.GONE
            mBinding.linear1.visibility = View.GONE
            mBinding.linear2.visibility = View.GONE
        }else{
            mBinding.imageView56.setImageResource(R.drawable.ic_thumbs_details)
            mBinding.textView93.text = "Glad You Had A Great Experience!"
            mBinding.textView339.text = "Share your experience with your friends for a chance to win a free month of service"
            mBinding.textView340.visibility = View.VISIBLE
            mBinding.linear1.visibility = View.VISIBLE
            mBinding.linear2.visibility = View.VISIBLE
        }

        mBinding.doneSharing.setOnClickListener {
            val sharedPreferences = getSharedPreferences("reviews", Context.MODE_PRIVATE)
            val editor = sharedPreferences.edit()
            editor.putString("reviews", "Okk")
            editor.apply()
            finish()
        }

        mBinding.linear2.setOnClickListener{
            val inviteCode = Constant.getArrayListProfile(this, Constant.dataProfile)
            val message =
                "I want to share this awesome experience with you, Sign up  and use redeem code in-app to view your gift. Redeem Code ${inviteCode.invite_code} .https://play.google.com/apps/testing/com.upscribbr"
            val share = Intent(Intent.ACTION_SEND)
            share.type = "text/plain"
            share.putExtra(Intent.EXTRA_TEXT, message)
            startActivity(Intent.createChooser(share, "Share!"))
        }


    }
}
