package com.upscribber.notification.notifications

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.upscribber.commonClasses.Constant
import com.upscribber.commonClasses.ModelStatusMsg
import com.upscribber.commonClasses.WebServicesCustomers
import com.upscribber.notification.ModelNotificationCustomer
import com.upscribber.profile.ModelGetProfile
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class NotificationsRepository(var application: Application) {

    val mDataFailure = MutableLiveData<ModelStatusMsg>()
    val mDataUpdateNotificationToggle = MutableLiveData<ModelGetProfile>()
    val mDataNotification = MutableLiveData<ArrayList<ModelNotificationCustomer>>()
    val mDataNotification1 = MutableLiveData<ModelNotificationCustomer>()
    val arrayListNotifications = ArrayList<ModelNotificationCustomer>()

    fun getAllNotifications(type: String, page: Int) {
        val auth = Constant.getPrefs(application).getString(Constant.auth_code, "")

        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.getUserNotifications(auth, type, page)
        call.enqueue(object : Callback<ResponseBody> {


            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {

                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")

                        if (page == 0) {
                            arrayListNotifications.clear()
                        }


//                        {"status":"true","msg":"","data":{"notification":[{"id":"167","msg":"Upscribbr LLC wants to relink with you.","recipient_id":"11","sender_id":"0","read":"0",
//                            "data":{"business_name":"Upscribbr LLC","business_id":"6","staff_id":"8","request_type":1,"reqtype":"relink"},"reference_id":"0","type":"4","notification_type":"2","action_taken":"1",
//                            "created_at":"2020-03-27 19:49:40","updated_at":"2020-03-27 19:49:40"},{"id":"130","msg":"Upscribbr LLC wants to relink with you.","recipient_id":"11","sender_id":"0","read":"0",
//                            "data":{"business_name":"Upscribbr LLC","business_id":"6","staff_id":"8","request_type":1,"reqtype":"relink"},"reference_id":"0","type":"4","notification_type":"2","action_taken":"1",
//                            "created_at":"2020-03-26 01:14:00","updated_at":"2020-03-26 01:14:00"},{"id":"95","msg":"Upscribbr LLC wants to relink with you.","recipient_id":"11","sender_id":"0","read":"1",
//                            "data":{"business_name":"Upscribbr LLC","business_id":"6","staff_id":"8","request_type":1,"reqtype":"relink"},"reference_id":"0","type":"4","notification_type":"2",
//                            "action_taken":"1","created_at":"2020-03-21 22:47:53","updated_at":"2020-03-21 22:47:53"}],"count":3},
//                            "request":{"auth_code":"14cca5c6185f13031530da0b5928e4f1","type":"2","page":"0"}}



                        val data = json.optJSONObject("data")
                        val data1 = data.optJSONArray("notification")
                        val count = data.optInt("count")
                        if (data1.length() > 0) {
                            for (i in 0 until data1.length()) {
                                val notificationData = data1.optJSONObject(i)
                                val data = notificationData.optJSONObject("data")

                                val notificationModel = ModelNotificationCustomer()
                                notificationModel.business_name = notificationData.optString("business_name")
                                notificationModel.id = notificationData.optString("id")
                                notificationModel.city = notificationData.optString("city")
                                notificationModel.logo = notificationData.optString("logo")
                                notificationModel.msg = notificationData.optString("msg")
                                notificationModel.state = notificationData.optString("state")
                                notificationModel.street = notificationData.optString("street")
                                notificationModel.steps = notificationData.optString("steps")
                                notificationModel.type = notificationData.optString("type")
                                notificationModel.action_taken = notificationData.optString("action_taken")
                                notificationModel.recipient_id = notificationData.optString("recipient_id")
                                notificationModel.sender_id = notificationData.optString("sender_id")
                                notificationModel.read = notificationData.optString("read")
                                notificationModel.reference_id = notificationData.optString("reference_id")
                                notificationModel.notification_type = notificationData.optString("notification_type")
                                notificationModel.created_at = notificationData.optString("created_at")
                                notificationModel.updated_at = notificationData.optString("updated_at")
                                notificationModel.count = count
                                if (data != null && data.length() > 0) {
                                    if (type == "1") {
                                        if (notificationModel.type == "1" || notificationModel.type == "4") {
                                            notificationModel.business_id = data.optString("business_id")
                                            notificationModel.type1 = data.optString("type")
                                            notificationModel.staff_id = data.optString("staff_id")
                                            notificationModel.campaign_id = data.optString("campaign_id")
                                            notificationModel.subscription_id = data.optString("subscription_id")
                                            notificationModel.campaign_name = data.optString("campaign_name")
                                            notificationModel.business_name = data.optString("business_name")
                                            notificationModel.name = data.optString("name")
                                            notificationModel.reqtype = data.optString("reqtype")
                                        }else if (notificationModel.type == "3") {
                                            notificationModel.campaign_id = data.optString("campaign_id")
                                            notificationModel.subscription_id = data.optString("subscription_id")
                                            notificationModel.campaign_name = data.optString("campaign_name")
                                            notificationModel.business_name = data.optString("business_name")
                                        } else if (notificationModel.type == "3") {
                                            notificationModel.order_id = data.optString("order_id")
                                            notificationModel.logo = data.optString("logo")
                                            notificationModel.business_name = data.optString("business_name")
                                            notificationModel.business_id = data.optString("business_id")
                                            notificationModel.campaign_id = data.optString("campaign_id")
                                            notificationModel.subscription_id = data.optString("subscription_id")
                                            notificationModel.customer_name = data.optString("customer_name")
                                            notificationModel.staff_id = data.optString("staff_id")
                                            notificationModel.campaign_name = data.optString("campaign_name")
                                            notificationModel.name = data.optString("name")
                                            notificationModel.reqtype = data.optString("reqtype")
                                        }else{
                                            notificationModel.campaign_id = data.optString("campaign_id")
                                            notificationModel.redeemtion_cycle_qty = data.optString("redeemtion_cycle_qty")
                                            notificationModel.order_id = data.optString("order_id")
                                            notificationModel.customer_name = data.optString("customer_name")
                                            notificationModel.campaign_name = data.optString("campaign_name")
                                            notificationModel.frequency_type = data.optString("frequency_type")
                                            notificationModel.unit = data.optString("unit")
                                            notificationModel.total_amount = data.optString("total_amount")
                                            notificationModel.business_name = data.optString("business_name")
                                            notificationModel.description = data.optString("description")
                                            notificationModel.free_trial = data.optString("free_trial")
                                            notificationModel.campaign_id = data.optString("campaign_id")
                                            notificationModel.customer_name = data.optString("customer_name")
                                            notificationModel.business_id = data.optString("business_id")
                                            notificationModel.staff_id = data.optString("staff_id")
                                            notificationModel.name = data.optString("name")
                                            notificationModel.reqtype = data.optString("reqtype")
                                        }

                                    } else {
                                        notificationModel.redeemtion_cycle_qty = data.optString("redeemtion_cycle_qty")
                                        notificationModel.order_id = data.optString("order_id")
                                        notificationModel.customer_name = data.optString("customer_name")
                                        notificationModel.campaign_name = data.optString("campaign_name")
                                        notificationModel.frequency_type = data.optString("frequency_type")
                                        notificationModel.unit = data.optString("unit")
                                        notificationModel.total_amount = data.optString("total_amount")
                                        notificationModel.business_name = data.optString("business_name")
                                        notificationModel.description = data.optString("description")
                                        notificationModel.free_trial = data.optString("free_trial")
                                        notificationModel.campaign_id = data.optString("campaign_id")
                                        notificationModel.customer_name = data.optString("customer_name")
                                        notificationModel.business_id = data.optString("business_id")
                                        notificationModel.staff_id = data.optString("staff_id")
                                        notificationModel.name = data.optString("name")
                                        notificationModel.reqtype = data.optString("reqtype")
                                        notificationModel.frequency_value = data.optString("frequency_value")
                                        notificationModel.assign_to = data.optString("assign_to")
                                        notificationModel.price = data.optString("price")
                                        notificationModel.customer_id = data.optString("customer_id")
                                        notificationModel.business_name = data.optString("business_name")
                                        notificationModel.location_id = data.optString("location_id")
                                        notificationModel.contact_no = data.optString("contact_no")
                                        notificationModel.profile_image = data.optString("profile_image")
                                        notificationModel.remaining_visits = data.optString("remaining_visits")
                                        notificationModel.business_id = data.optString("business_id")
                                        notificationModel.business_phone = data.optString("business_phone")
                                        notificationModel.logo = data.optString("logo")
                                        notificationModel.payment_type = data.optString("payment_type")
                                        notificationModel.payment_method_name = data.optString("payment_method_name")
                                        notificationModel.last_4 = data.optString("last_4")
                                        notificationModel.merchant_id = data.optString("merchant_id")
                                        notificationModel.team_member = data.optString("team_member")
                                        notificationModel.redeem_id = data.optString("redeem_id")
                                        notificationModel.rtype = data.optString("rtype")
                                        notificationModel.transaction_id = data.optString("transaction_id")


                                        val transaction_data = data.optJSONObject("transaction_data")
                                        if (transaction_data != null && transaction_data.length() > 0) {
                                            notificationModel.transaction_id = transaction_data.optString("transaction_id")
                                            notificationModel.order_id = transaction_data.optString("order_id")
                                            notificationModel.customer_id = transaction_data.optString("customer_id")
                                            notificationModel.total_amount = transaction_data.optString("total_amount")
                                            notificationModel.transaction_status = transaction_data.optString("transaction_status")
                                            notificationModel.campaign_name = transaction_data.optString("campaign_name")
                                            notificationModel.customer_name = transaction_data.optString("customer_name")
                                            notificationModel.frequency_type = transaction_data.optString("frequency_type")
                                            notificationModel.business_name = transaction_data.optString("business_name")
                                            notificationModel.unit = transaction_data.optString("unit")
                                            notificationModel.redeemtion_cycle_qty = transaction_data.optString("redeemtion_cycle_qty")
                                            notificationModel.card_id = transaction_data.optString("card_id")
                                            notificationModel.merchant_id = transaction_data.optString("merchant_id")
                                            notificationModel.feature_image = transaction_data.optString("merchant_id")
                                            notificationModel.description = transaction_data.optString("description")
                                            notificationModel.profile_image = transaction_data.optString("profile_image")
                                            notificationModel.order_status = transaction_data.optString("order_status")
                                            notificationModel.is_refunded = transaction_data.optString("is_refunded")
                                            notificationModel.transaction_type = transaction_data.optString("transaction_type")
//                                            notificationModel.created_at = transaction_data.optString("created_at")
                                            notificationModel.redeem_count = transaction_data.optString("redeem_count")
                                            notificationModel.admin_amount = transaction_data.optString("admin_amount")
                                            notificationModel.merchant_amount = transaction_data.optString("merchant_amount")
                                            notificationModel.stripe_fee = transaction_data.optString("stripe_fee")
                                            notificationModel.contact_no = transaction_data.optString("contact_no")
//                                        notificationModel.created_at = transaction_data.optString("created_at")
                                        }
                                    }
                                }



                                notificationModel.msgg = msg
                                notificationModel.typeCheck = type
                                notificationModel.status = status
                                arrayListNotifications.add(notificationModel)
                            }
                        }

                        mDataNotification.value = arrayListNotifications
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.msg = "Network Error!"
                modelStatus.status = "false"
                mDataFailure.value = modelStatus
            }
        })
    }


    fun getmDataFailure(): LiveData<ModelStatusMsg> {
        return mDataFailure
    }


     fun getmDataUpdateNotificationToggle(): LiveData<ModelGetProfile> {
        return mDataUpdateNotificationToggle
    }


    fun mDataNotification(): LiveData<ArrayList<ModelNotificationCustomer>> {
        return mDataNotification
    }


    fun mDataNotifications(): LiveData<ModelNotificationCustomer> {
        return mDataNotification1
    }


    fun getBusinessLinkInfo(
        type: String,
        businessId: String,
        id: String,
        reqtype: String
    ) {
        val auth = Constant.getPrefs(application).getString(Constant.auth_code, "")

        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.getBusinessLinkInfo(auth, businessId)
        call.enqueue(object : Callback<ResponseBody> {


            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val statuss = json.optString("status")
                        val msg = json.optString("msg")
                        val data = json.optJSONObject("data")
                        val notificationModel = ModelNotificationCustomer()
                        val arrayListNotifications = ArrayList<ModelNotificationCustomer>()

                        notificationModel.business_name = data.optString("business_name")
                        notificationModel.city = data.optString("city")
                        notificationModel.logo = data.optString("logo")
                        notificationModel.msg = data.optString("msg")
                        notificationModel.state = data.optString("state")
                        notificationModel.street = data.optString("street")
                        notificationModel.steps = data.optString("steps")
                        notificationModel.type = data.optString("type")
                        notificationModel.business_id = data.optString("business_id")
                        notificationModel.id = id
                        notificationModel.reqtype = reqtype
                        notificationModel.msgg = msg
                        notificationModel.type2 = type
                        notificationModel.status = statuss
                        //arrayListNotifications.add(notificationModel)


                        mDataNotification1.value = notificationModel
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.msg = "Network Error!"
                modelStatus.status = "false"
                mDataFailure.value = modelStatus
            }
        })
    }

    fun updateProfile(auth: String, pushSetting: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()
        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody>
        call = service.updateNotificationToggle(auth, pushSetting,"0")


        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelUpdateProfile = ModelGetProfile()
                        val modelStatusMsg = ModelStatusMsg()
                        if (status == "true") {
                            modelUpdateProfile.status = status
                            modelUpdateProfile.msg = msg
                            modelUpdateProfile.pushValue = pushSetting
                            mDataUpdateNotificationToggle.value = modelUpdateProfile
                            var push = ""
                            push = if (pushSetting == "0"){
                                "1"
                            }else{
                                "0"
                            }

                            val editor = Constant.getPrefs(application).edit()
                            editor.putString(Constant.new_notification_status, push)
                            editor.apply()

                        } else {
                            modelStatusMsg.msg = msg
                            modelStatusMsg.status = status
                            mDataFailure.value = modelStatusMsg
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus
            }
        })

    }

    fun getReadNotification() {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val auth = Constant.getPrefs(application).getString(Constant.auth_code, "")

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.readNewNotification(auth)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        val model = ModelGetProfile()

                        if (status == "true") {
                            model.status = status
                            model.msg = msg
                            model.new_notification_status = "0"

                            val editor = Constant.getPrefs(application).edit()
                            editor.putString(Constant.new_notification_status, model.new_notification_status)
                            editor.apply()

                        } else {
                            modelStatus.status = status
                            modelStatus.msg = msg
                            mDataFailure.value = modelStatus
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg ="Network Error!"
                mDataFailure.value = modelStatus
            }
        })
    }

}