package com.upscribber.notification.notifications

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SwitchCompat
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.R
import com.upscribber.becomeseller.individual.verificationSSNScreensActivity
import com.upscribber.commonClasses.CloseListPagination
import com.upscribber.commonClasses.Constant
import com.upscribber.notification.ModelNotificationCustomer
import com.upscribber.notification.reviews.ReviesFromCustomer
import com.upscribber.profile.ActivityMerchantDetailPage
import com.upscribber.subsciptions.merchantSubscriptionPages.MerchantSubscriptionViewActivity
import com.upscribber.upscribberSeller.accountLinking.LinkingRequestActivity
import com.upscribber.upscribberSeller.customerBottom.profile.activity.ProfileActivityListActivity
import com.upscribber.upscribberSeller.customerBottom.scanning.SuccessfullyRedemptionActivity
import com.upscribber.upscribberSeller.manageTeam.AddMemberActivity
import com.upscribber.upscribberSeller.subsriptionSeller.subscriptionView.ActivitySubscriptionSeller
import kotlinx.android.synthetic.main.activity_notification.*
import kotlinx.android.synthetic.main.alert_already_linked.*
import kotlinx.android.synthetic.main.no_notifications.*
import kotlin.collections.ArrayList

class NotificationActivity : AppCompatActivity(), AdapterNotificationCustomer.GetData {


    private var isClicked: Boolean = false
    lateinit var toolbarTitle: TextView
    lateinit var tvBuyer: TextView
    lateinit var tvSeller: TextView
    lateinit var toolbar8: Toolbar
    lateinit var vieww1: View
    lateinit var sellingSwitchMini: SwitchCompat
    lateinit var rvNotification: RecyclerView
    lateinit var rvNotificationSeller: RecyclerView
    lateinit var noDataText: TextView
    lateinit var mViewModel: NotificationViewModel
    lateinit var mAdapterNotificationsSeller: AdapterNotificationCustomer
    lateinit var mAdapterNotificationsBuyer: AdapterNotificationCustomer
    var arrayList = ArrayList<ModelNotificationCustomer>()
    var status: Boolean = true

    //    var type = ""
    var isLastPageDataBuyer: Boolean = false
    var isLastPageDataSeller: Boolean = false
    var isLoadingMoreItemsSeller: Boolean = false
    var isLoadingMoreItemsBuyer: Boolean = false
    var pageBuyer = 1
    var pageSeller = 1
    var typed = 1
    var layoutpag = LinearLayoutManager(this)
    var layoutpag1 = LinearLayoutManager(this)
    var buyerArrayList = ArrayList<ModelNotificationCustomer>()
    var sellerArrayList = ArrayList<ModelNotificationCustomer>()
    var typeCheck: String = "1"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notification)
        mViewModel = ViewModelProviders.of(this)[NotificationViewModel::class.java]
        init()
        setToolbar()
        apiInitialization()
        clickListeners()
        observerInit()
        setAdapter()
        setBackground()
        scrollListener()

    }

    private fun clickListeners() {
        sellingSwitchMini.setOnCheckedChangeListener { buttonView, isChecked ->
            val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
            if (isChecked) {
                mViewModel.updateProfile(auth, "1")
            } else {
                mViewModel.updateProfile(auth, "0")
            }
        }

    }

    private fun setAdapter() {
        //buyer
        rvNotification.layoutManager = layoutpag
        mAdapterNotificationsBuyer = AdapterNotificationCustomer(this)
        rvNotification.adapter = mAdapterNotificationsBuyer

        //seller
        rvNotificationSeller.layoutManager = layoutpag1
        mAdapterNotificationsSeller = AdapterNotificationCustomer(this)
        rvNotificationSeller.adapter = mAdapterNotificationsSeller
    }

    private fun scrollListener() {
        rvNotificationSeller.addOnScrollListener(object : CloseListPagination(layoutpag1) {

            override fun loadMoreItems() {

                if (typeCheck == "2") {
                    isLoadingMoreItemsSeller = true
                    progressBar_pagination.visibility = View.VISIBLE
                    pageSeller++
                    //**********api of load more

                    if (intent.hasExtra("user")) {
                        mViewModel.getNotifications("2", pageSeller)
                    } else {
                        mViewModel.getNotifications("2", pageSeller)
                    }
                } else {
                    return
                }


            }

            override fun isLastPage(): Boolean {
                return isLastPageDataSeller
            }

            override fun isLoading(): Boolean {
                return isLoadingMoreItemsSeller

            }

        })

        rvNotification.addOnScrollListener(object : CloseListPagination(layoutpag) {
            override fun loadMoreItems() {
                if (typeCheck == "1") {
                    isLoadingMoreItemsBuyer = true
                    progressBar_pagination.visibility = View.VISIBLE
                    pageBuyer++

                    if (intent.hasExtra("user")) {
                        mViewModel.getNotifications("1", pageBuyer)
                    } else {
                        mViewModel.getNotifications("1", pageBuyer)
                    }
                } else {
                    return
                }

            }

            override fun isLastPage(): Boolean {
                return isLastPageDataBuyer
            }

            override fun isLoading(): Boolean {
                return isLoadingMoreItemsBuyer
            }
        })
    }


    private fun observerInit() {
        mViewModel.getStatus().observe(this, Observer {
            progressBar.visibility = View.GONE
            progressBar_pagination.visibility = View.GONE
            if (it.msg == "Invalid auth code") {
                Constant.commonAlert(this)
            } else {
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
            }
        })


        mViewModel.getmDataUpdateNotificationToggle().observe(this, Observer {
            if (it.status == "true") sellingSwitchMini.isChecked = it.pushValue != "0"
        })

        mViewModel.mDataNotification().observe(this, Observer {
            progressBar.visibility = View.GONE
            progressBar_pagination.visibility = View.GONE
            if (it.size > 0) {
                if (it[0].typeCheck == "1") {
                    typeCheck = "1"
                    if (it.size > 0) {
                        rvNotification.visibility = View.VISIBLE
                        rvNotificationSeller.visibility = View.GONE
                        noDataText.visibility = View.GONE
                        imgNoNotifications.visibility = View.GONE
                        noDataTextDEscription.visibility = View.GONE
                        buyerArrayList = it
                        isLoadingMoreItemsBuyer = false
                        isLastPageDataBuyer = buyerArrayList.size >= it[0].count
                        rvNotification.recycledViewPool.clear()
                        mAdapterNotificationsBuyer.update(it)
                    } else {
                        noDataText.visibility = View.VISIBLE
                        imgNoNotifications.visibility = View.VISIBLE
                        noDataTextDEscription.visibility = View.VISIBLE
                        rvNotification.visibility = View.GONE
                        rvNotificationSeller.visibility = View.GONE
                    }
                } else if (it[0].typeCheck == "2") {
                    typeCheck = "2"
                    if (it.size > 0) {
                        imgNoNotifications.visibility = View.GONE
                        noDataTextDEscription.visibility = View.GONE
                        rvNotification.visibility = View.GONE
                        rvNotificationSeller.visibility = View.VISIBLE
                        noDataText.visibility = View.GONE
                        sellerArrayList = it
                        isLoadingMoreItemsSeller = false
                        isLastPageDataSeller = sellerArrayList.size >= it[0].count
                        rvNotificationSeller.recycledViewPool.clear();
                        mAdapterNotificationsSeller.update(it)
                    } else {
                        noDataText.visibility = View.VISIBLE
                        rvNotificationSeller.visibility = View.GONE
                        imgNoNotifications.visibility = View.VISIBLE
                        noDataTextDEscription.visibility = View.VISIBLE
                        rvNotification.visibility = View.GONE
                    }
                }

//                it.reverse()

                arrayList = it

            } else {
                noDataText.visibility = View.VISIBLE
                rvNotificationSeller.visibility = View.GONE
                rvNotification.visibility = View.GONE
                imgNoNotifications.visibility = View.VISIBLE
                noDataTextDEscription.visibility = View.VISIBLE
                rvNotification.visibility = View.GONE
            }

        })

    }

    private fun apiInitialization() {
        mViewModel.getReadNotification()

        val isSeller = Constant.getSharedPrefs(this).getBoolean(Constant.IS_SELLER, false)
        if (isSeller) {
            typed = 2
            if (typed == 1) {
                if (intent.hasExtra("user")) {
                    mViewModel.getNotifications("1", pageBuyer)
                } else {
                    mViewModel.getNotifications("1", pageBuyer)
                }
            } else {
                if (intent.hasExtra("user")) {
                    mViewModel.getNotifications("2", pageSeller)
                } else {
                    mViewModel.getNotifications("2", pageSeller)
                }
            }


            progressBar.visibility = View.VISIBLE

        } else {
            typed = 1
            progressBar.visibility = View.VISIBLE
            if (typed == 1) {
                if (intent.hasExtra("user")) {
                    mViewModel.getNotifications("1", pageBuyer)
                } else {
                    mViewModel.getNotifications("1", pageBuyer)
                }
            } else {
                if (intent.hasExtra("user")) {
                    mViewModel.getNotifications("2", pageSeller)
                } else {
                    mViewModel.getNotifications("2", pageSeller)
                }
            }
        }

    }


    private fun setToolbar() {
        setSupportActionBar(toolbar8)
        title = ""
        toolbarTitle.text = "Notifications"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)

    }


    private fun init() {
        toolbar8 = findViewById(R.id.toolbar8)
        toolbarTitle = findViewById(R.id.title4)
        sellingSwitchMini = findViewById(R.id.sellingSwitchMini)
        rvNotification = findViewById(R.id.rvNotification)
        tvBuyer = findViewById(R.id.tvBuyer)
        tvSeller = findViewById(R.id.tvSeller)
        rvNotificationSeller = findViewById(R.id.rvNotificationSeller)
        noDataText = findViewById(R.id.noDataText)
        vieww1 = findViewById(R.id.vieww1)
        progressBar_pagination.visibility = View.GONE

        val profileData = Constant.getArrayListProfile(application, Constant.dataProfile)
        if (profileData.type == "1" && profileData.steps == "6" || profileData.type == "2" && profileData.steps == "4" || profileData.type == "3") {
            tvBuyer.visibility = View.VISIBLE
            tvSeller.visibility = View.VISIBLE
            vieww1.visibility = View.VISIBLE
            rvNotificationSeller.visibility = View.VISIBLE
        } else {
            tvBuyer.visibility = View.GONE
            tvSeller.visibility = View.GONE
            vieww1.visibility = View.GONE
            rvNotificationSeller.visibility = View.GONE
        }


        val isSeller = Constant.getSharedPrefs(this).getBoolean(Constant.IS_SELLER, false)
        if (isSeller) {
            tvSeller.setBackgroundResource(R.drawable.bg_info_blue)
            tvSeller.setTextColor(resources.getColor(R.color.colorWhite))
            tvBuyer.setTextColor(resources.getColor(R.color.payments))
            tvBuyer.setBackgroundResource(R.drawable.bg_switch_fragments)
            rvNotification.visibility = View.GONE
            rvNotificationSeller.visibility = View.VISIBLE
        } else {
            tvSeller.setBackgroundResource(R.drawable.bg_switch_fragments)
            tvSeller.setTextColor(resources.getColor(R.color.payments))
            tvBuyer.setTextColor(resources.getColor(R.color.colorWhite))
            tvBuyer.setBackgroundResource(R.drawable.bg_info)
            rvNotification.visibility = View.VISIBLE
            rvNotificationSeller.visibility = View.GONE
        }


        sellingSwitchMini.isChecked = profileData.push_setting != "0"

    }

    private fun setBackground() {
        tvSeller.setOnClickListener {
            pageBuyer = 0
            buyerArrayList.clear()
            rvNotificationSeller.visibility = View.VISIBLE
            rvNotification.visibility = View.GONE
//            mAdapterNotificationsSeller.update(arrayList)
            progressBar.visibility = View.VISIBLE
            typed = 2
            if (intent.hasExtra("user")) {
                mViewModel.getNotifications("2", pageSeller)
            } else {
                mViewModel.getNotifications("2", pageSeller)
            }

            val isSeller = Constant.getSharedPrefs(this).getBoolean(Constant.IS_SELLER, false)
            if (isSeller) {
                tvSeller.setBackgroundResource(R.drawable.bg_info_blue)
                tvBuyer.setBackgroundResource(R.drawable.bg_switch_fragments)
                tvSeller.setTextColor(resources.getColor(R.color.colorWhite))
                tvBuyer.setTextColor(resources.getColor(R.color.payments))
            } else {
                tvSeller.setBackgroundResource(R.drawable.bg_info)
                tvBuyer.setBackgroundResource(R.drawable.bg_switch_fragments)
                tvSeller.setTextColor(resources.getColor(R.color.colorWhite))
                tvBuyer.setTextColor(resources.getColor(R.color.payments))

            }
        }

        tvBuyer.setOnClickListener {
            pageSeller = 0
            sellerArrayList.clear()
            rvNotificationSeller.visibility = View.GONE
            rvNotification.visibility = View.VISIBLE
//            mAdapterNotificationsBuyer.update(arrayList)
            progressBar.visibility = View.VISIBLE
            typed = 1
            if (intent.hasExtra("user")) {
                mViewModel.getNotifications("1", pageBuyer)
            } else {
                mViewModel.getNotifications("1", pageBuyer)
            }

            val isSeller = Constant.getSharedPrefs(this).getBoolean(Constant.IS_SELLER, false)
            if (isSeller) {
                tvBuyer.setBackgroundResource(R.drawable.bg_info_blue)
                tvSeller.setBackgroundResource(R.drawable.bg_switch_fragments)
                tvBuyer.setTextColor(resources.getColor(R.color.colorWhite))
                tvSeller.setTextColor(resources.getColor(R.color.payments))
            } else {
                tvBuyer.setBackgroundResource(R.drawable.bg_info)
                tvSeller.setBackgroundResource(R.drawable.bg_switch_fragments)
                tvBuyer.setTextColor(resources.getColor(R.color.colorWhite))
                tvSeller.setTextColor(resources.getColor(R.color.payments))
            }

        }

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun loadData(
        position: Int,
        model: ModelNotificationCustomer
    ) {

        val isSeller = Constant.getSharedPrefs(this).getBoolean(Constant.IS_SELLER, false)
        if (model.type == "5") {
            val intent = Intent(this, ActivityMerchantDetailPage::class.java)
                .putExtra("notificationData", model)
            startActivity(intent)

        } else if (model.type == "14" ||model.type == "26" ) {
            val intent = Intent(this, ActivitySubscriptionSeller::class.java)
                .putExtra("campaignId", model.campaign_id)
            startActivity(intent)
        } else if (model.type == "11") {
            val intent = Intent(this, ProfileActivityListActivity::class.java)
            startActivity(intent)
        } else if (model.type == "15" || model.type == "1" || model.type == "9") {
            val intent = Intent(this, MerchantSubscriptionViewActivity::class.java)
                .putExtra("modelSubscriptionView", model.campaign_id)
            startActivity(intent)
        } else if (!isSeller && model.type == "1") {
            mViewModel.getBusinessLinkInfo(
                arrayList[position].type1,
                arrayList[position].business_id, arrayList[position].id, arrayList[position].reqtype
            )
            isClicked = true

            getClickedIntent(model)

        } else if (model.type == "4") {
            if (arrayList[position].type1 == "") {
                mViewModel.getBusinessLinkInfo(
                    arrayList[position].type,
                    arrayList[position].business_id,
                    arrayList[position].id,
                    arrayList[position].reqtype
                )
            } else {
                mViewModel.getBusinessLinkInfo(
                    arrayList[position].type1,
                    arrayList[position].business_id,
                    arrayList[position].id,
                    arrayList[position].reqtype
                )
            }
            isClicked = true
            getClickedIntent(model)

        } else if (!isSeller && model.type == "2") {
            mViewModel.getBusinessLinkInfo(
                arrayList[position].type1,
                arrayList[position].business_id,
                arrayList[position].id,
                arrayList[position].reqtype
            )
            isClicked = true
            getClickedIntent(model)

        } else if (model.type == "3") {
            if (typed != 2) {
                val intent = Intent(this, ReviesFromCustomer::class.java)
                    .putExtra("model", model)
                startActivity(intent)
            }else{
                startActivity(Intent(this,SuccessfullyRedemptionActivity::class.java).putExtra("infoNotification","other").putExtra("modelRedeemNotification",model))
            }

        } else if (model.type == "23") {
            startActivity(Intent(this, verificationSSNScreensActivity::class.java))
        }



    }


    private fun getClickedIntent(model: ModelNotificationCustomer) {

        mViewModel.mDataNotifications().observe(this, Observer {
            if (it.status == "true") {
                when (it.type2) {
                    "1" -> {
                        val homeProfile = Constant.getArrayListProfile(this, Constant.dataProfile)
                        if (homeProfile.type == "1" && homeProfile.steps == "6" || homeProfile.type == "2" && homeProfile.steps == "4" || homeProfile.type == "3") {
                            commonAlertConditions(it)

                        } else {
                            if (isClicked) {
                                isClicked = false
                                startActivity(
                                    Intent(this, LinkingRequestActivity::class.java)
                                        .putExtra("individualList", "other")
                                        .putExtra("fromNotification", "fromNotification")
                                        .putExtra("model", it)
                                )
                            }
                        }

                    }
                    "4" -> {
                        if (it.reqtype == "relink") {
                            if (isClicked) {
                                isClicked = false
                                val intent = Intent(this, LinkingRequestActivity::class.java)
                                    .putExtra("relink", "relink")
                                    .putExtra("fromNotification", "fromNotification")
                                    .putExtra("model", it)
                                    .putExtra("staffId", model.staff_id)
                                startActivity(intent)
                            }
                        } else {

                            val homeProfile =
                                Constant.getArrayListProfile(this, Constant.dataProfile)
                            if (homeProfile.type == "1" && homeProfile.steps == "6" || homeProfile.type == "2" && homeProfile.steps == "4" || homeProfile.type == "3") {
                                commonAlertConditions(it)
                            } else {
                                if (isClicked) {
                                    isClicked = false
                                    val intent = Intent(this, LinkingRequestActivity::class.java)
                                        .putExtra("relink", "relink")
                                        .putExtra("fromNotification", "fromNotification")
                                        .putExtra("model", it)
                                        .putExtra("staffId", model.staff_id)
                                    startActivity(intent)
                                }
                            }
                        }
                    }
                    "2" -> {
                        val homeProfile = Constant.getArrayListProfile(this, Constant.dataProfile)
                        if (homeProfile.type == "1" && homeProfile.steps == "6" || homeProfile.type == "2" && homeProfile.steps == "4" || homeProfile.type == "3") {
                            commonAlertConditions(it)
                        } else {
                            if (isClicked) {
                                isClicked = false

                                startActivity(
                                    Intent(
                                        this,
                                        AddMemberActivity::class.java
                                    ).putExtra("model", model)
                                )
                            }
                        }
                    }
                }
            }

        })

    }

    private fun commonAlertConditions(it: ModelNotificationCustomer) {
        val homeProfile = Constant.getArrayListProfile(this, Constant.dataProfile)
        if (isClicked) {
            isClicked = false
            val mDialogView = LayoutInflater.from(this)
                .inflate(R.layout.alert_already_linked, null)
            val mBuilder =
                android.app.AlertDialog.Builder(this).setView(mDialogView)
            val mAlertDialog = mBuilder.show()
            mAlertDialog.setCancelable(false)
            mDialogView.setBackgroundResource(R.drawable.bg_alert_new)
            mAlertDialog.window.setBackgroundDrawableResource(R.drawable.bg_alert_transparent)
            if (homeProfile.type == "1" && homeProfile.steps == "6") {
                if (it.type2 == "1") {
                    mAlertDialog.textInfo.setText(R.string.merchant_info)
                } else {
                    mAlertDialog.textInfo.setText(R.string.merchant_info1)
                }
            } else if (homeProfile.type == "2" && homeProfile.steps == "4") {
                if (it.type2 == "1") {
                    mAlertDialog.textInfo.setText(R.string.contractor_info1)
                } else {
                    mAlertDialog.textInfo.setText(R.string.contractor_info)
                }
            } else if (homeProfile.type == "3") {
                if (it.type2 == "1") {
                    mAlertDialog.textInfo.setText(R.string.non_contractor_info)
                } else {
                    mAlertDialog.textInfo.setText(R.string.non_contractor_info1)
                }
            }

            mAlertDialog.okBttn.setOnClickListener {
                mAlertDialog.dismiss()
            }
        }

    }

    override fun onResume() {
        super.onResume()
        val sharedPreferences1 = getSharedPreferences("linkingNotification", Context.MODE_PRIVATE)
        if (!sharedPreferences1.getString("linkingNotification", "").isEmpty()) {
            val editor = sharedPreferences1.edit()
            editor.remove("linkingNotification")
            editor.apply()
            finish()
        }


        val sharedPreferences2 = getSharedPreferences("reviews", Context.MODE_PRIVATE)
        if (!sharedPreferences2.getString("reviews", "").isEmpty()) {
            val editor = sharedPreferences2.edit()
            editor.remove("reviews")
            editor.apply()
            finish()
        }

        val sharedPreferences3 = getSharedPreferences("individual", Context.MODE_PRIVATE)
        if (!sharedPreferences3.getString("individual", "").isEmpty()) {
            val editor = sharedPreferences3.edit()
            editor.remove("individual")
            editor.apply()
            finish()
        }


    }


    override fun takenAction() {
        AlertDialog.Builder(this)
            // .setTitle("Alert!")
            .setMessage("You have already taken action")
            .setPositiveButton("OK") { dialogInterface: DialogInterface, i: Int ->
                dialogInterface.dismiss()
            }
            .show()

    }

}