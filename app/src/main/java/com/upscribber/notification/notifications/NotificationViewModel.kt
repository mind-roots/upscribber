package com.upscribber.notification.notifications

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.upscribber.commonClasses.ModelStatusMsg
import com.upscribber.notification.ModelNotificationCustomer
import com.upscribber.profile.ModelGetProfile

class NotificationViewModel(application: Application) : AndroidViewModel(application) {

    fun getNotifications(type: String, page: Int) {
        repositoryNotification.getAllNotifications(type,page)
    }

    var repositoryNotification: NotificationsRepository = NotificationsRepository(application)

    fun getStatus(): LiveData<ModelStatusMsg> {
        return repositoryNotification.getmDataFailure()
    }

    fun mDataNotification(): LiveData<ArrayList<ModelNotificationCustomer>> {
        return repositoryNotification.mDataNotification()
    }

      fun getmDataUpdateNotificationToggle(): LiveData<ModelGetProfile> {
        return repositoryNotification.getmDataUpdateNotificationToggle()
    }




    fun getBusinessLinkInfo(
        type: String,
        businessId: String,
        id: String,
        reqtype: String
    ) {
        repositoryNotification.getBusinessLinkInfo(type, businessId,id,reqtype)
    }

    fun mDataNotifications(): LiveData<ModelNotificationCustomer> {
        return repositoryNotification.mDataNotifications()
    }

    fun updateProfile(auth: String, push_setting: String) {
        repositoryNotification.updateProfile(auth,push_setting)

    }

    fun getReadNotification() {
        repositoryNotification.getReadNotification()
    }


}