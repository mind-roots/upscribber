package com.upscribber.notification.notifications

import android.annotation.SuppressLint
import android.content.Context
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.notification.ModelNotificationCustomer
import kotlinx.android.synthetic.main.notification_customer_recycler.view.*


class AdapterNotificationCustomer(
    private val mContext: Context

) :
    RecyclerView.Adapter<AdapterNotificationCustomer.MyViewHolder>() {

    private var list: ArrayList<ModelNotificationCustomer> = ArrayList()

    var data = mContext as GetData

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(mContext)
            .inflate(R.layout.notification_customer_recycler, parent, false)

        return MyViewHolder(v)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        val model = list[position]

//        val imagePath = Constant.getPrefs(mContext)
//            .getString(Constant.dataImagePath, "http://cloudart.com.au/projects/upscribbr/uploads/")
//        holder.itemView.tvLogoDesc.text = model.msg
//        Glide.with(mContext).load(imagePath + model.logo).placeholder(R.mipmap.circular_placeholder)
//            .into(holder.itemView.ivLogo)


        getNameImageType(model, holder)

        holder.itemView.setOnClickListener {
//            val handler = Handler()
//            holder.itemView.mainConstraint.setBackgroundResource(R.drawable.bg_seller_button_purple)
//            holder.itemView.tvLogoDesc.setTextColor(mContext.resources.getColor(R.color.colorHeading))
//            holder.itemView.tvDate.setTextColor(mContext.resources.getColor(R.color.colorCardDesc))
//
//            handler.postDelayed(Runnable {
//                if (model.action_taken == "0") {
//                    data.loadData(position, model)
//                } else {
//                    data.takenAction()
//
//                }
//                holder.itemView.mainConstraint.setBackgroundResource(0)
//                holder.itemView.tvLogoDesc.setTextColor(mContext.resources.getColor(R.color.colorHeading))
//                holder.itemView.tvDate.setTextColor(mContext.resources.getColor(R.color.colorCardDesc))
//            }, 500)
            if (model.action_taken == "0") {
                data.loadData(position, model)
            } else {
                data.takenAction()

            }

        }
    }

    @SuppressLint("SetTextI18n")
    private fun getNameImageType(
        model: ModelNotificationCustomer,
        holder: MyViewHolder
    ) {

        val imagePath = Constant.getPrefs(mContext)
            .getString(Constant.dataImagePath, "http://cloudart.com.au/projects/upscribbr/uploads/")

        val isSeller = Constant.getSharedPrefs(mContext).getBoolean(Constant.IS_SELLER, false)

        when {

            model.type == "1" -> {
                holder.itemView.tvLogoDesc.text = model.business_name  + " invited you to join a VIP subscription."
                holder.itemView.ivLogo.setImageResource(R.drawable.ic_merchant_customer_invite_notification)
                holder.itemView.tvDate.text =
                    "${Constant.getDatt(model.created_at)}   ${Constant.getTime(model.created_at)}"

            }
            model.type == "2" -> {
                holder.itemView.tvLogoDesc.text =
                    "Merchant account accepted or rejected by stripe"
                holder.itemView.ivLogo.setImageResource(R.drawable.ic_invite_notification)
                holder.itemView.tvDate.text =
                    "${Constant.getDatt(model.created_at)}   ${Constant.getTime(model.created_at)}"
            }
            model.type == "3" -> {
                holder.itemView.tvLogoDesc.text = "You have redeemed your subscription"
                holder.itemView.ivLogo.setImageResource(R.drawable.ic_redeem_notification)
                holder.itemView.tvDate.text =
                    "${Constant.getDatt(model.created_at)}   ${Constant.getTime(model.created_at)}"

            }

            model.type == "6" -> {
                holder.itemView.tvLogoDesc.text =
                    model.customer_name + " has purchased your subscription " + model.campaign_name
                holder.itemView.ivLogo.setImageResource(R.drawable.ic_walletpayouts)
                holder.itemView.tvDate.text =
                    "${Constant.getDatt(model.created_at)}   ${Constant.getTime(model.created_at)}"

            }
            model.type == "7" -> {
                holder.itemView.tvLogoDesc.text = model.campaign_name + "has been renewed"
                holder.itemView.ivLogo.setImageResource(R.drawable.ic_subscription_renewal_notification)
                holder.itemView.tvDate.text =
                    "${Constant.getDatt(model.created_at)}   ${Constant.getTime(model.created_at)}"

            }
            model.type == "8" -> {
                holder.itemView.tvLogoDesc.text = "Order cancel"
                holder.itemView.ivLogo.setImageResource(R.drawable.ic_link_denied_notification)
                holder.itemView.tvDate.text =
                    "${Constant.getDatt(model.created_at)}   ${Constant.getTime(model.created_at)}"

            }
            model.type == "9" -> {
//                holder.itemView.tvLogoDesc.text = "Shared"
                holder.itemView.tvLogoDesc.text = model.msg
                holder.itemView.ivLogo.setImageResource(R.drawable.ic_share_notification)
                holder.itemView.tvDate.text =
                    "${Constant.getDatt(model.created_at)}   ${Constant.getTime(model.created_at)}"

            }
            model.type == "10" -> {
                holder.itemView.tvLogoDesc.text = "Pause Subscription"
                holder.itemView.ivLogo.setImageResource(R.drawable.ic_pause_subscription_notification)
                holder.itemView.tvDate.text =
                    "${Constant.getDatt(model.created_at)}   ${Constant.getTime(model.created_at)}"

            }
            model.type == "11" -> {
                holder.itemView.tvLogoDesc.text = model.msg
                holder.itemView.ivLogo.setImageResource(R.drawable.ic_share_notification)
                holder.itemView.tvDate.text =
                    "${Constant.getDatt(model.created_at)}   ${Constant.getTime(model.created_at)}"

            }
            model.type == "12" -> {
                holder.itemView.tvLogoDesc.text = model.name + "'s account is now linked to your business."
                holder.itemView.ivLogo.setImageResource(R.drawable.ic_account_verified_notification)
                holder.itemView.tvDate.text =
                    "${Constant.getDatt(model.created_at)}   ${Constant.getTime(model.created_at)}"

            }
            model.type == "13" -> {
                holder.itemView.tvLogoDesc.text = "User rejected your business request"
                holder.itemView.ivLogo.setImageResource(R.drawable.ic_link_denied_notification)
                holder.itemView.tvDate.text =
                    "${Constant.getDatt(model.created_at)}   ${Constant.getTime(model.created_at)}"


            }
            model.type == "14" -> {
                holder.itemView.tvLogoDesc.text = "Request to Join, Subscriber Limit Reached"
                holder.itemView.ivLogo.setImageResource(R.drawable.ic_subscription_limiter_notification)

                holder.itemView.tvDate.text =
                    "${Constant.getDatt(model.created_at)}   ${Constant.getTime(model.created_at)}"


            }
            model.type == "15" -> {
                holder.itemView.tvLogoDesc.text = "Limit extended: you can now join " + model.campaign_name
                holder.itemView.ivLogo.setImageResource(R.drawable.ic_merchant_customer_invite_notification)
                holder.itemView.tvDate.text =
                    "${Constant.getDatt(model.created_at)}   ${Constant.getTime(model.created_at)}"


            }
            model.type == "16" -> {
                holder.itemView.tvLogoDesc.text = "Subscription unredeemed. " + model.business_name + " has unredeemed " + model.campaign_name
                holder.itemView.ivLogo.setImageResource(R.drawable.ic_redeem_notification)
                holder.itemView.tvDate.text =
                    "${Constant.getDatt(model.created_at)}   ${Constant.getTime(model.created_at)}"


            }
            model.type == "17" -> {
//                if (model.msg.isEmpty()) {
                holder.itemView.tvLogoDesc.text = "Campaign limit reached"
                holder.itemView.ivLogo.setImageResource(R.drawable.ic_subscription_limiter_notification)
                holder.itemView.tvDate.text =
                    "${Constant.getDatt(model.created_at)}   ${Constant.getTime(model.created_at)}"


            }
            model.type == "18" -> {
//                if (model.msg.isEmpty()) {
                holder.itemView.tvLogoDesc.text = "Free trial end"
                holder.itemView.ivLogo.setImageResource(R.drawable.ic_free_trial_end_notifications)
                holder.itemView.tvDate.text =
                    "${Constant.getDatt(model.created_at)}   ${Constant.getTime(model.created_at)}"
            }
            model.type == "19" -> {
                holder.itemView.tvLogoDesc.text = model.business_name + " has declined a refund for " + model.campaign_name
                holder.itemView.ivLogo.setImageResource(R.drawable.ic_link_denied_notification)
                holder.itemView.tvDate.text =
                    "${Constant.getDatt(model.created_at)}   ${Constant.getTime(model.created_at)}"


            }
            model.type == "20" -> {
                holder.itemView.tvLogoDesc.text = model.business_name +  " has approved a refund for " + model.campaign_name
                holder.itemView.ivLogo.setImageResource(R.drawable.ic_refund_notification)
                holder.itemView.tvDate.text =
                    "${Constant.getDatt(model.created_at)}   ${Constant.getTime(model.created_at)}"

            }
            model.type == "21" -> {
                holder.itemView.tvLogoDesc.text = "Update Upscribbr payment information"
                holder.itemView.ivLogo.setImageResource(R.drawable.ic_dollar_notification)
                holder.itemView.tvDate.text =
                    "${Constant.getDatt(model.created_at)}   ${Constant.getTime(model.created_at)}"


            }
            model.type == "22" -> {
                holder.itemView.tvLogoDesc.text = model.name + " is now unlinked as a contractor."
                holder.itemView.ivLogo.setImageResource(R.drawable.ic_manage_payments_notification)
                holder.itemView.tvDate.text =
                    "${Constant.getDatt(model.created_at)}   ${Constant.getTime(model.created_at)}"
            }

            model.type == "23" -> {
                holder.itemView.tvLogoDesc.text = "Stripe verification"
                holder.itemView.ivLogo.setImageResource(R.drawable.ic_share_notification)
                holder.itemView.tvDate.text =
                    "${Constant.getDatt(model.created_at)}   ${Constant.getTime(model.created_at)}"
            }


             model.type == "24" -> {
                holder.itemView.tvLogoDesc.text = "Payout Received"
                holder.itemView.ivLogo.setImageResource(R.drawable.ic_manage_payments_notification)
                holder.itemView.tvDate.text =
                    "${Constant.getDatt(model.created_at)}   ${Constant.getTime(model.created_at)}"
            }

            model.type == "25" -> {
                holder.itemView.tvLogoDesc.text = "You’ve been signed up for " + model.name  + " with " + model.business_name
                holder.itemView.ivLogo.setImageResource(R.drawable.ic_merchant_customer_invite_notification)
                holder.itemView.tvDate.text =
                    "${Constant.getDatt(model.created_at)}   ${Constant.getTime(model.created_at)}"

            }
            model.type == "27" -> {
                holder.itemView.tvLogoDesc.text = "Payment Declined"
                holder.itemView.ivLogo.setImageResource(R.drawable.ic_link_denied_notification)
                holder.itemView.tvDate.text =
                    "${Constant.getDatt(model.created_at)}   ${Constant.getTime(model.created_at)}"

            }

            model.type == "26" -> {
                holder.itemView.tvLogoDesc.text = "You’ve reached your subscription limit for " + model.campaign_name
                holder.itemView.ivLogo.setImageResource(R.drawable.ic_subscription_limiter_notification)
                holder.itemView.tvDate.text =
                    "${Constant.getDatt(model.created_at)}   ${Constant.getTime(model.created_at)}"

            }
            isSeller -> {
                if (model.type == "20") {
                    holder.itemView.tvLogoDesc.text =
                        model.customer_name + " has requested for a Refund for " + model.campaign_name
                    holder.itemView.ivLogo.setImageResource(R.drawable.ic_refund_notification)
                    holder.itemView.tvDate.text =
                        "${Constant.getDatt(model.created_at)}   ${Constant.getTime(model.created_at)}"
                } else if (model.type == "5") {
                    holder.itemView.tvLogoDesc.text =
                        model.customer_name + " has requested for a Refund for " + model.campaign_name
                    holder.itemView.ivLogo.setImageResource(R.drawable.ic_refund_notification)
                    holder.itemView.tvDate.text =
                        "${Constant.getDatt(model.created_at)}   ${Constant.getTime(model.created_at)}"
                } else if (model.type == "4") {
                    if (model.type1 == "1"){
                        holder.itemView.tvLogoDesc.text = "You have been invited to join " + model.business_name + " on Upscribbr."
                    }else if(model.type1 == "2"){
                        holder.itemView.tvLogoDesc.text = "You have a pending staff request from " + model.business_name
                    }else{
                        holder.itemView.tvLogoDesc.text = model.msg
                    }
                    holder.itemView.ivLogo.setImageResource(R.drawable.ic_account_link_notification)

                    holder.itemView.tvDate.text =
                        "${Constant.getDatt(model.created_at)}   ${Constant.getTime(model.created_at)}"

                }
            }
            !isSeller -> {
                if (model.type == "20") {
                    holder.itemView.tvLogoDesc.text =
                        model.customer_name + " has requested for a Refund for " + model.campaign_name
                    holder.itemView.ivLogo.setImageResource(R.drawable.ic_refund_notification)
                    holder.itemView.tvDate.text =
                        "${Constant.getDatt(model.created_at)}   ${Constant.getTime(model.created_at)}"
                } else if (model.type == "5") {
                    holder.itemView.tvLogoDesc.text =
                        model.customer_name + " has requested for a Refund for " + model.campaign_name
                    holder.itemView.ivLogo.setImageResource(R.drawable.ic_refund_notification)
                    holder.itemView.tvDate.text =
                        "${Constant.getDatt(model.created_at)}   ${Constant.getTime(model.created_at)}"
                } else if (model.type == "4") {
                    if (model.type1 == "1"){
                        holder.itemView.tvLogoDesc.text = "You have been invited to join " + model.business_name + " on Upscribbr."

                    }else if(model.type1 == "2"){
                        holder.itemView.tvLogoDesc.text = "You have a pending staff request from " + model.business_name
                    }else{
                        holder.itemView.tvLogoDesc.text = model.msg
                    }
                    holder.itemView.ivLogo.setImageResource(R.drawable.ic_account_link_notification)

                    holder.itemView.tvDate.text =
                        "${Constant.getDatt(model.created_at)}   ${Constant.getTime(model.created_at)}"
                }

            }

            else -> {
                holder.itemView.tvLogoDesc.text = model.msg
                holder.itemView.ivLogo.setImageResource(R.drawable.ic_link_denied_notification)
                holder.itemView.tvDate.text =
                    "${Constant.getDatt(model.created_at)}   ${Constant.getTime(model.created_at)}"

            }
        }

    }

    fun update(it: ArrayList<ModelNotificationCustomer>?) {
        list = it!!
        notifyDataSetChanged()

    }

    interface GetData {
        fun loadData(
            position: Int,
            model: ModelNotificationCustomer
        )

        fun takenAction()

    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

}