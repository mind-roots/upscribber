package com.upscribber.forgotPassword

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.chaos.view.PinView
import com.upscribber.commonClasses.Constant
import com.upscribber.R
import com.upscribber.signUp.SignUpViewModel


class ForgotPasswordOtp : Fragment() {

    private lateinit var mPinView: PinView
    lateinit var update: clicksPassword
    private lateinit var mTvInviteCode: TextView
    private lateinit var progressBar: LinearLayout
    private lateinit var btn_resend: TextView
    private lateinit var tv_number: TextView
    private lateinit var mBackToPhone: ImageView
    private lateinit var mPinOk: ImageView
    lateinit var mViewModel: SignUpViewModel
    var flag = 0


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view: View = inflater.inflate(R.layout.fragment_forget_otp, container, false)
        mViewModel = ViewModelProviders.of(this)[SignUpViewModel::class.java]
        mTvInviteCode = view.findViewById(R.id.tv_next)
        mPinView = view.findViewById(R.id.secondPinView)
        progressBar = view.findViewById(R.id.progressBar)
        mPinOk = view.findViewById(R.id.pin_ok)
        btn_resend = view.findViewById(R.id.btn_resend)
        mBackToPhone = view.findViewById(R.id.back_to_phone)
        tv_number = view.findViewById(R.id.tv_number)
        setNumber()
        ObserversInit()
        clickListeners()
        return view
    }

    private fun ObserversInit() {
        mViewModel.getmDataOtp().observe(this, Observer {
            progressBar.visibility = View.GONE
            if (it.status == "true") {
                update.setOTP(it.otp)
            }
        })

        mViewModel.getStatus().observe(this, Observer {
            progressBar.visibility = View.GONE
            if (it.msg.isNotEmpty()) {
                Toast.makeText(context, it.msg, Toast.LENGTH_SHORT).show()
            }

        })

    }

    private fun setNumber() {
        tv_number.text = update.getNumber()
        if (flag == 1) {
            mTvInviteCode.setBackgroundResource(R.drawable.otp_next)
        } else {
            mTvInviteCode.setBackgroundResource(R.drawable.otp_next_opacity)
        }


    }

    private fun clickListeners() {
        mTvInviteCode.setOnClickListener {
            if (flag == 1) {
                if (mPinView.text.toString() == update.getOtp()) {
                    progressBar.visibility = View.VISIBLE
                    update.next(3)
                } else {
                    Toast.makeText(context, "Otp does not match", Toast.LENGTH_LONG).show()
                }
            } else {
                Toast.makeText(context, "Please Enter the Otp First!", Toast.LENGTH_LONG).show()
            }

        }

        mBackToPhone.setOnClickListener {
            update.back(2)
        }

        mPinView.setTextColor(
            ResourcesCompat.getColor(resources, R.color.line_color, context!!.theme)
        )
        mPinView.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (s.length == 4) {
                    mViewModel.getOtpVerified(update.getNumber(), update.getOtp())
                    mTvInviteCode.setBackgroundResource(R.drawable.otp_next)
                    mPinOk.visibility = View.VISIBLE
                    flag = 1
                } else {
                    mPinOk.visibility = View.GONE
                    mTvInviteCode.setBackgroundResource(R.drawable.otp_next_opacity)
                    flag = 0
                }

                if (mPinView.text.toString() == update.getOtp()) {
                    mPinOk.setImageResource(R.drawable.ic_done_black_24dp)
                } else
                {
                    mPinOk.setImageResource(R.drawable.ic_cross_pink)
                }
            }



            override fun afterTextChanged(s: Editable) {

            }
        })
        mPinView.setHideLineWhenFilled(false)

        btn_resend.setOnClickListener {
            mViewModel.getOtp("+1" + update.getNumber(), "2")
//            val mDialogView =
//                LayoutInflater.from(activity).inflate(R.layout.alert_resend_code, null)
//            val mBuilder = AlertDialog.Builder(activity!!)
//                .setView(mDialogView)
//            val mAlertDialog = mBuilder.show()
//            mAlertDialog.setCancelable(false)
//            mAlertDialog.window.setBackgroundDrawableResource(R.drawable.bg_alert1)
//            mAlertDialog.textDone.setOnClickListener {
//                mAlertDialog.dismiss()
//            }

        }


    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        update = context as clicksPassword
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        update = activity as clicksPassword
    }
}