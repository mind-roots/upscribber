package com.upscribber.forgotPassword

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.dashboard.DashboardActivity
import com.upscribber.login.LoginActivity
import com.upscribber.signUp.SignUpViewModel

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class ChangePasswordFragment : Fragment() {

    lateinit var mViewModel: SignUpViewModel
    lateinit var mbackToOtp: ImageView
    lateinit var changePasswordBtn: TextView
    lateinit var etNewPassword: EditText
    lateinit var etConfirmPassword: EditText
    lateinit var update: clicksPassword


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_forgot_password, container, false)
        mViewModel = ViewModelProviders.of(this)[SignUpViewModel::class.java]
        mbackToOtp = view.findViewById(R.id.back_to_otp)
        changePasswordBtn = view.findViewById(R.id.changePasswordBtn)
        etConfirmPassword = view.findViewById(R.id.etConfirmPassword)
        etNewPassword = view.findViewById(R.id.etNewPassword)
        clickListeners()
        observerInit()
        return view
    }

    private fun observerInit() {
        mViewModel.getmVerifyGenerateNewPassword().observe(this, Observer {
            if (it.status == "true"){
                startActivity(Intent(activity,DashboardActivity::class.java))
                activity!!.finish()
            }
        })

    }

    private fun clickListeners() {
        mbackToOtp.setOnClickListener {
            update.back(3)
        }


        changePasswordBtn.setOnClickListener {
            val number = update.getNumber()
            val phoneNumber = number.replace(" ", "")
            val fireBaseToken : String = Constant.getPrefs(activity!!).getString("deviceId", "")

            if (etNewPassword.text.toString().trim().isEmpty()){
                Toast.makeText(activity,"Please Enter New Password",Toast.LENGTH_SHORT).show()
            }else if (etConfirmPassword.text.toString().trim().isEmpty()){
                Toast.makeText(activity,"Please Confirm Password",Toast.LENGTH_SHORT).show()
            }else if (etNewPassword.text.toString().trim() != etConfirmPassword.text.toString().trim()){
                Toast.makeText(activity,"New Password and Confirm Password does not match",Toast.LENGTH_SHORT).show()
            }else if (etNewPassword.text.toString().trim().length < 4) {
                Toast.makeText(activity, "Password should be greater than 4 digits", Toast.LENGTH_LONG)
                    .show()
            }else if (etConfirmPassword.text.toString().trim().length < 4) {
                Toast.makeText(activity, "Password should be greater than 4 digits", Toast.LENGTH_LONG)
                    .show()
            }
            else{
                mViewModel.setNewPasswordApi(phoneNumber,etConfirmPassword.text.toString().trim(),fireBaseToken)
            }

        }

    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        update = context as clicksPassword
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        update = activity as clicksPassword
    }

}
