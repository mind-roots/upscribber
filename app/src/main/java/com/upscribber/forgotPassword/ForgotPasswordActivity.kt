package com.upscribber.forgotPassword

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.upscribber.R
import com.upscribber.signUp.finalLogin.SignUpWelcomeFragment
import com.upscribber.signUp.signupInfo.SignUpFragment


class ForgotPasswordActivity : AppCompatActivity(), clicksPassword{

    private var password: String = ""
    private var email: String = ""
    private lateinit var fragmentManager: FragmentManager
    private lateinit var fragmentTransaction: FragmentTransaction
    var numberd: String = ""
    var otpp: String = ""
    var namee: String = ""
    var id: String = ""
    var emailLogin: String = ""
    var fromLogin: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        fragmentManager = supportFragmentManager
        this.fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.container, ForgotPhoneFragment())
        fragmentTransaction.commit()


    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun setNumber(i: String) {
        numberd = i
    }

    override fun getNumber(): String {
        return numberd
    }

    override fun getName(): String {
        return namee

    }

    override fun login(): String {
        return fromLogin
    }

    override fun setLogin(i: String) {

    }

    override fun getLoginEmail(i: String) {

    }

    override fun emailLogin(): String {
        return emailLogin
    }

    override fun setName(i: String) {
        namee = i
    }

    override fun loginIddd(): String {
        return id

    }

    override fun loginId(i: String) {


    }

    override fun getPassword(): String {
        return password
    }

    override fun setPassword(i: String) {
        password = i
    }

    override fun getEmail(): String {
        return email

    }

    override fun setEmail(i: String) {
        email = i
    }

    override fun getOtp(): String {

        return otpp
    }

    override fun setOTP(i: String) {
        otpp = i
    }

    override fun back(i: Int) {
        when (i) {
            1 -> {

                finish()

            }
            2 -> {
                fragmentManager = supportFragmentManager
                this.fragmentTransaction = fragmentManager.beginTransaction()
                fragmentTransaction.replace(
                    R.id.container,
                    ForgotPhoneFragment()
                )
                fragmentTransaction.commit()
            }
            3 -> {
                fragmentManager = supportFragmentManager
                this.fragmentTransaction = fragmentManager.beginTransaction()
                fragmentTransaction.replace(
                    R.id.container,
                    ForgotPasswordOtp()
                )
                fragmentTransaction.commit()
            }

        }
    }

    override fun next(i: Int) {
        when (i) {
            1 -> {
                fragmentManager = supportFragmentManager
                this.fragmentTransaction = fragmentManager.beginTransaction()
                fragmentTransaction.replace(
                    R.id.container,
                    ForgotPhoneFragment()
                )
                fragmentTransaction.commit()
            }
            2 -> {
                fragmentManager = supportFragmentManager
                this.fragmentTransaction = fragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.container, ForgotPasswordOtp())
                fragmentTransaction.commit()
            }
            3 -> {
                fragmentManager = supportFragmentManager
                this.fragmentTransaction = fragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.container, ChangePasswordFragment())
                fragmentTransaction.commit()
            }
        }


    }


}
