package com.upscribber.util

import android.annotation.SuppressLint
import android.content.Context
import android.widget.TextView
import com.github.mikephil.charting.components.MarkerView
import com.github.mikephil.charting.data.CandleEntry
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.utils.Utils
import com.upscribber.R

@SuppressLint("ViewConstructor")
class MyMarkerView(context: Context, layoutResource: Int) : MarkerView(context, layoutResource) {

    private var tvContent: TextView

   init {
       tvContent = findViewById<TextView>(R.id.tvContent)
   }


    @Override
    override fun refreshContent(e: Entry, highlight: Highlight) {

        if (e is CandleEntry) {

            val ce = e

            tvContent.text = Utils.formatNumber(ce.high, 0, true)
        } else {

            tvContent.text = Utils.formatNumber(e.y, 0, true)
        }

        super.refreshContent(e, highlight)
    }


    fun getXOffset(xpos: Float): Int {
        // this will center the marker-view horizontally
        return -(width / 2)
    }

    fun getYOffset(ypos: Float): Int {
        // this will cause the marker-view to be above the selected value
        return -height
    }
}