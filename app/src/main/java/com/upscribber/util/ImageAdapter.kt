package com.upscribber.util

import android.content.Context
import android.net.Uri
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.makeramen.roundedimageview.RoundedImageView
import de.hdodenhof.circleimageview.CircleImageView
import android.view.View
import com.bumptech.glide.Glide
import com.squareup.picasso.Picasso
import com.upscribber.R
import com.upscribber.commonClasses.Constant


@BindingAdapter("imageLink")
fun imageSetting(imageView: CircleImageView, int: Int) {
        imageView.setImageResource(int)
}

@BindingAdapter("imageLinkString")
fun imageSettingString(imageView: CircleImageView, string: String) {
    val imagePath  = Constant.getPrefs(imageView.context).getString(Constant.dataImagePath, "")
    Glide.with(imageView.context).load(imagePath + string).placeholder(R.mipmap.circular_placeholder).into(imageView)
}



@BindingAdapter("imageReview")
fun imageReview(imageView: CircleImageView, int: String) {

//    Glide.with(imageView.context)
//        .load(int)
//        .override(100,100)
//        .override(100)
//        .into(imageView)
    Picasso.get()
        .load(int)
        .resize(100,100)
        .centerCrop()
        .into(imageView)

}


@BindingAdapter("imageLinkk")
fun imageSetting1(imageView: ImageView, int: Int) {
    imageView.setImageResource(int)
}


@BindingAdapter("imageLinkRounded")
fun imageSettingRounded(imageView: RoundedImageView, int: Int) {
    imageView.setImageResource(int)
}

@BindingAdapter("imageCampaigns")
fun imageCampaigns(imageView: RoundedImageView, string: String) {
    val imagePath  = Constant.getPrefs(imageView.context).getString(Constant.dataImagePath, "")
    Glide.with(imageView.context).load(imagePath+string).placeholder(R.mipmap.placeholder_subscription_square).into(imageView)
}




@BindingAdapter("imageLinkUri")
fun imageSettingUri(imageView: CircleImageView, uri: Uri?) {
    Picasso.get()
        .load(uri)
        .resize(300,300)
        .centerCrop()
        .into(imageView)

}
@BindingAdapter("store")
fun imageSettingUriTwo(imageView: RoundedImageView, uri: Uri?) {
    Picasso.get()
        .load(uri)
        .resize(300, 300)
        .centerCrop()
        .into(imageView)


}
@BindingAdapter("setvisibility")
fun setVisibility(view: View, value: Boolean) {
    view.visibility = if (value) View.VISIBLE else View.GONE
}