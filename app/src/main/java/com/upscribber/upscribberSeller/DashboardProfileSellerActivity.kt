package com.upscribber.upscribberSeller

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import com.upscribber.R
import com.upscribber.profile.EditProfileActivity
import kotlinx.android.synthetic.*

class DashboardProfileSellerActivity : AppCompatActivity() {

    lateinit var dashProfile : Fragment
    lateinit var toolbarProfileSeller: Toolbar
    lateinit var title: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard_profile_seller)
        initz()
        setToolbar()
    }

    private fun setToolbar() {

        setSupportActionBar(toolbarProfileSeller)
        title.text = "Profile"

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)
    }

    private fun initz() {
        dashProfile.clearFindViewByIdCache()
        toolbarProfileSeller = findViewById(R.id.toolbarProfileSeller)
        title = findViewById(R.id.title)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.nav_edit_profile, menu)
        return super.onCreateOptionsMenu(menu)

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        if (item.itemId == R.id.edit){
            startActivity(Intent(this, EditProfileActivity::class.java))
        }
        return super.onOptionsItemSelected(item)
    }
}
