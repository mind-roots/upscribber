package com.upscribber.upscribberSeller.dashboardfragment.reporting

interface ReportingEventListener {
    fun onClick()
}