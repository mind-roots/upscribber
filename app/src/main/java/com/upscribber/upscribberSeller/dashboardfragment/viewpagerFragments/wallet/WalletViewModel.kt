package com.upscribber.upscribberSeller.dashboardfragment.viewpagerFragments.wallet

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData

class WalletViewModel(application: Application): AndroidViewModel(application) {

    var mData: LiveData<List<ModelPaymentHistory>>
    var mRepository: WalletRepository = WalletRepository(application)

    init {
        mData = mRepository.getHistory()
    }


    fun getPaymentHistory():LiveData<List<ModelPaymentHistory>> {
        return mData
    }


}