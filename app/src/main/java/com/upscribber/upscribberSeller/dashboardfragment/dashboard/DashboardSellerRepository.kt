package com.upscribber.upscribberSeller.dashboardfragment.dashboard

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.upscribber.commonClasses.Constant
import com.upscribber.commonClasses.ModelStatusMsg
import com.upscribber.commonClasses.WebServicesCustomers
import com.upscribber.home.ModelHomeDataSubscriptions
import com.upscribber.notification.ModelNotificationCustomer
import com.upscribber.profile.ModelGetProfile
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class DashboardSellerRepository(application: Application)  {


    val mData = MutableLiveData<ArrayList<ModelNotificationCustomer>>()
    val mDataFailure = MutableLiveData<ModelStatusMsg>()
    val mDataDeclined = MutableLiveData<ModelStatusMsg>()


    fun getSellerDashboardData(auth : String){
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.dashboard(auth)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        val data = json.optJSONObject("data")
                        if (status == "true") {

                            val invited_data = data.optJSONArray("invited_subscription")
                            val modelHomeArrayList = ArrayList<ModelNotificationCustomer>()

                            for (i in 0 until invited_data.length()){

                                val invited_subscription = invited_data.optJSONObject(i)

                                val modelGetProfile = ModelNotificationCustomer()

                                modelGetProfile.transaction_id = invited_subscription.optString("transaction_id")
                                modelGetProfile.order_id = invited_subscription.optString("order_id")
                                modelGetProfile.customer_id = invited_subscription.optString("customer_id")
                                modelGetProfile.name = invited_subscription.optString("name")
                                modelGetProfile.total_amount = invited_subscription.optString("total_amount")
                                modelGetProfile.transaction_status = invited_subscription.optString("transaction_status")
                                modelGetProfile.transaction_type = invited_subscription.optString("transaction_type")
                                modelGetProfile.created_at = invited_subscription.optString("created_at")
                                modelGetProfile.campaign_name = invited_subscription.optString("campaign_name")
                                modelGetProfile.customer_name = invited_subscription.optString("customer_name")
                                modelGetProfile.frequency_type = invited_subscription.optString("frequency_type")
                                modelGetProfile.business_name = invited_subscription.optString("business_name")
                                modelGetProfile.unit = invited_subscription.optString("unit")
                                modelGetProfile.redeemtion_cycle_qty = invited_subscription.optString("redeemtion_cycle_qty")
                                modelGetProfile.card_id = invited_subscription.optString("card_id")
                                modelGetProfile.merchant_id = invited_subscription.optString("merchant_id")
                                modelGetProfile.feature_image = invited_subscription.optString("feature_image")
                                modelGetProfile.description = invited_subscription.optString("description")
                                modelGetProfile.profile_image = invited_subscription.optString("profile_image")
                                modelGetProfile.order_status = invited_subscription.optString("order_status")
                                modelGetProfile.is_refunded = invited_subscription.optString("is_refunded")
                                modelGetProfile.id = invited_subscription.optString("id")
                                modelGetProfile.type = invited_subscription.optString("type")
                                modelGetProfile.status = status
                                modelHomeArrayList.add(modelGetProfile)
                            }

                            mData.value = modelHomeArrayList

                        } else {
                            modelStatus.msg = msg
                        }

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus

            }
        })
    }



    fun mDataHomePager(): MutableLiveData<ArrayList<ModelNotificationCustomer>> {
        return mData
    }


    fun getmDataFailure(): LiveData<ModelStatusMsg> {
        return mDataFailure
    }


    fun getDeclinedInvites(auth: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.declinedInvitedSubs(auth)
        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        if (status == "true") {
                            modelStatus.status = status
                            mDataDeclined.value = modelStatus
                        } else {
                            modelStatus.status = status
                            modelStatus.msg = msg
                            mDataFailure.value = modelStatus
                        }


                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus

            }
        })

    }

    fun getmDataDeclined(): LiveData<ModelStatusMsg> {
        return mDataDeclined
    }


}
