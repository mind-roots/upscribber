package com.upscribber.upscribberSeller.dashboardfragment.reporting

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

class ReportingPagerAdapter(mcontext: Context, fm: FragmentManager) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        when (position) {
            0 -> return ReportingViewsFragment()
            1 -> return ReportingSalesFragment()
//            2 -> return ReportingProductsFragment()
//            2 -> return ReportingProductsSharedFragment()
//            3 -> return ReportingSubscriotionsFragment()
        }
        return null!!
    }

    override fun getCount(): Int {
        return 2
    }

}