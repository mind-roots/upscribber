package com.upscribber.upscribberSeller.dashboardfragment.dashboard

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.upscribber.commonClasses.ModelStatusMsg
import com.upscribber.home.ModelHomeDataSubscriptions
import com.upscribber.notification.ModelNotificationCustomer

class DashboardSellerViewModel(application: Application) : AndroidViewModel(application) {


    var customerBottomRepository = DashboardSellerRepository(application)

    fun getSellerPagerData(auth : String){

        customerBottomRepository.getSellerDashboardData(auth)

    }

    fun mDataNotification() : MutableLiveData<ArrayList<ModelNotificationCustomer>>{
        return customerBottomRepository.mDataHomePager()
    }

    fun getDeclinedApi(auth: String) {
        customerBottomRepository.getDeclinedInvites(auth)

    }


    fun getmDataDeclined(): LiveData<ModelStatusMsg> {
        return customerBottomRepository.getmDataDeclined()
    }



}
