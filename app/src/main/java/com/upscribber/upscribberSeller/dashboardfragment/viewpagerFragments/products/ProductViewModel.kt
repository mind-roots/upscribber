package com.upscribber.upscribberSeller.dashboardfragment.viewpagerFragments.products

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData

class ProductViewModel(application: Application) : AndroidViewModel(application) {
    var mProducts: LiveData<List<ModelProduct>>
    var mRepository: ProductRepository = ProductRepository(application)

    init {
        mProducts = mRepository.getProductList()
    }

    fun getProducts(): LiveData<List<ModelProduct>> {
        return mProducts
    }


}