package com.upscribber.upscribberSeller.dashboardfragment.viewpagerFragments.products

interface ProductListener {
   fun onViewDetail()
}