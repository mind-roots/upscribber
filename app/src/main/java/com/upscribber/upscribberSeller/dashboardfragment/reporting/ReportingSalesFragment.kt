package com.upscribber.upscribberSeller.dashboardfragment.reporting


import android.content.Intent
import android.graphics.Color
import android.graphics.DashPathEffect
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.upscribber.util.MyMarkerView
import com.upscribber.upscribberSeller.reportingDetail.ReportingActivity
import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.IAxisValueFormatter
import com.github.mikephil.charting.formatter.IFillFormatter
import com.github.mikephil.charting.formatter.LargeValueFormatter
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import com.upscribber.R
import com.upscribber.databinding.ReportingSalesLayoutBinding
import kotlin.math.roundToInt

class ReportingSalesFragment : Fragment() , ReportingEventListener{
    private lateinit var set1: LineDataSet
    lateinit var mBinding: ReportingSalesLayoutBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(inflater, R.layout.reporting_sales_layout, container, false)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        val model = setGraphData()
        setChart()
//        mBinding.tvSalesPrice.text = model.totalSalePrice
       mBinding.click = this
//        setChart(model.graphPlotPricex.toInt(), model.graphPlotPricey.toInt())
    }


    @RequiresApi(Build.VERSION_CODES.HONEYCOMB)
    private fun setChart() {
        val myMarkerView = MyMarkerView(this.activity!!, R.layout.custom_marker_view)
        myMarkerView.chartView = mBinding.chartReporting
        mBinding.chartReporting.marker = myMarkerView
        mBinding.chartReporting.description.isEnabled = false

        val xAxis: XAxis = mBinding.chartReporting.xAxis

        mBinding.chartReporting.xAxis.isEnabled = true
        xAxis.position = XAxis.XAxisPosition.BOTTOM
        xAxis.setDrawAxisLine(true)

        xAxis.textSize = 6f
        xAxis.textColor = Color.parseColor("#94BAF8")

        val listAmount = ArrayList<String>()
        listAmount.add("1k")
        listAmount.add("2k")
        listAmount.add("3k")
        listAmount.add("4k")
        listAmount.add("10k")
        listAmount.add("15k")
        listAmount.add("22k")

        val listDays = ArrayList<String>()
        listDays.add("MON")
        listDays.add("TUE")
        listDays.add("WED")
        listDays.add("THUR")
        listDays.add("FRI")
        listDays.add("SAT")
        listDays.add("SUN")


        val yAxis: YAxis = mBinding.chartReporting.axisLeft
        mBinding.chartReporting.axisRight.isEnabled = false
        mBinding.chartReporting.axisLeft.isEnabled = true

        xAxis.axisMaximum = 6f
        xAxis.axisMinimum = 0f
        yAxis.granularity = 1f
        yAxis.axisMinimum = 0f
        yAxis.axisMaximum = 6f
        yAxis.textSize = 8f
        yAxis.textColor = Color.WHITE
        xAxis.textColor = Color.WHITE
        xAxis.valueFormatter = IAxisValueFormatter { value, axis -> listDays[value.roundToInt()] }
        yAxis.valueFormatter = IAxisValueFormatter { value, axis -> listAmount[value.roundToInt()] }

        mBinding.chartReporting.axisLeft.setDrawGridLines(true)
        mBinding.chartReporting.axisLeft.axisLineColor = resources.getColor(R.color.fulltransparentcolor)
        mBinding.chartReporting.axisRight.axisLineColor = resources.getColor(R.color.colorWhite)
        mBinding.chartReporting.xAxis.setDrawGridLines(false)
        mBinding.chartReporting.axisLeft.gridColor = resources.getColor(R.color.fulltransparentcolor)
        yAxis.setLabelCount(5, true)

        yAxis.setDrawLimitLinesBehindData(false)
        xAxis.setDrawLimitLinesBehindData(false)
        setData()

        mBinding.chartReporting.animateX(500)

        val legend = mBinding.chartReporting.legend
        legend.form = Legend.LegendForm.NONE
    }

    private fun setData() {
        val valuess = ArrayList<Entry>()
        valuess.add(Entry(0f, 0000f/1000))
        valuess.add(Entry(2f, 2000f/1000))
        valuess.add(Entry(3f, 3000f/1000))
        valuess.add(Entry(5f, 4000f/1000))
        valuess.add(Entry(6f, 5000f/1000))
        valuess.add(Entry(7f, 6000f/1000))
        valuess.add(Entry(8f, 7000f/1000))
        valuess.add(Entry(9f, 9000f/1000))
//        valuess.add(Entry(1000f, 9000f/1000))
//        valuess.add(Entry(1100f, 10000f/1000))
//        valuess.add(Entry(1300f, 11000f/1000))
//        valuess.add(Entry(1300f, 12000f/1000))
//        valuess.add(Entry(1400f, 13000f/1000))
//        valuess.add(Entry(150f, 14000f/1000))

        val set1 = LineDataSet(valuess, "")
        set1.setDrawValues(false)

        mBinding.chartReporting.setLayerType(View.LAYER_TYPE_SOFTWARE, null)
        mBinding.chartReporting.setScaleEnabled(false)
        set1.color = Color.WHITE
        set1.setDrawCircles(false)
        set1.lineWidth = 2f
        set1.setDrawCircleHole(false)
        set1.formLineWidth = 1f
        set1.formLineDashEffect = DashPathEffect(floatArrayOf(10f, 5f), 0f)
        set1.formSize = 15f
        set1.valueTextSize = 10f
        set1.circleRadius = 4.5f
        set1.setCircleColor(Color.parseColor("#7760f2"))
        set1.setDrawHorizontalHighlightIndicator(false)
        set1.setDrawVerticalHighlightIndicator(false)
        set1.setDrawFilled(true)
        set1.fillColor = Color.parseColor("#7760f2")
        set1.fillFormatter = IFillFormatter { _,
                                              _ ->
            mBinding.chartReporting.axisLeft.axisMinimum
        }

        val dataSets = ArrayList<ILineDataSet>()
        dataSets.add(set1)

        val data = LineData(dataSets)
        data.setValueFormatter(LargeValueFormatter())
        mBinding.chartReporting.xAxis.axisMaximum = 6f + 0.2f
        mBinding.chartReporting.xAxis.axisMinimum = 0f - 0.2f
        mBinding.chartReporting.data = data
        mBinding.chartReporting.invalidate()

    }

    override fun onClick() {
        startActivity(Intent(activity, ReportingActivity::class.java).putExtra("value", 1))
    }
}
