package com.upscribber.upscribberSeller.dashboardfragment.reporting


import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.formatter.LargeValueFormatter
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.formatter.IAxisValueFormatter
import com.github.mikephil.charting.utils.ColorTemplate
import com.upscribber.R
import com.upscribber.databinding.ReportingViewsLayoutBinding
import com.upscribber.upscribberSeller.reportingDetail.ReportingActivity
import com.upscribber.upscribberSeller.reportingDetail.teamPerformance.TeamViewModel
import kotlin.math.roundToInt


class ReportingViewsFragment : Fragment(), ReportingEventListener {
    lateinit var mBinding: ReportingViewsLayoutBinding
    val listAmount = ArrayList<String>()
    private lateinit var mAdapter: ViewsAdapter
    lateinit var mViewModel: TeamViewModel

    val TEAM_COLORS = intArrayOf(
        ColorTemplate.rgb("#7760f2"),
        ColorTemplate.rgb("#02cbcd"),
        ColorTemplate.rgb("#ff3f55"),
        ColorTemplate.rgb("#fcc117")
    )

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(inflater, R.layout.reporting_views_layout, container, false)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mViewModel = ViewModelProviders.of(this).get(TeamViewModel::class.java)
        mBinding.click = this
        setChart()
        mBinding.recyclerViews.visibility = View.GONE

    }

    private fun setAdapter() {

        val manager = LinearLayoutManager(activity)
        manager.orientation = LinearLayout.HORIZONTAL
        mBinding.recyclerViews.layoutManager = manager
        mAdapter = ViewsAdapter(activity!!)
        mBinding.recyclerViews.adapter = mAdapter
        mViewModel.getData().observe(this, Observer {
            mAdapter.updateArray(it)
        })

    }

    override fun onClick() {
        startActivity(Intent(activity, ReportingActivity::class.java).putExtra("value", 3))
    }

    @RequiresApi(Build.VERSION_CODES.HONEYCOMB)
    private fun setChart() {
//        val myMarkerView = MyMarkerView((this.activity as Context?)!!, R.layout.custom_marker_view)
//        myMarkerView.chartView = mBinding.chartReporting
//        mBinding.chartReporting.marker = myMarkerView
        mBinding.chartReporting.description.isEnabled = false
        mBinding.chartReporting.setTouchEnabled(true)


        val xAxis: XAxis = mBinding.chartReporting.xAxis
        val yAxis: YAxis = mBinding.chartReporting.axisLeft
        listAmount.add("0")
        listAmount.add("5K")
        listAmount.add("10K")
        listAmount.add("15K")
        listAmount.add("20K")

        val listDays = ArrayList<String>()
        listDays.add("MON")
        listDays.add("TUE")
        listDays.add("WED")
        listDays.add("THUR")
        listDays.add("FRI")
        listDays.add("SAT")
        listDays.add("SUN")

        mBinding.chartReporting.xAxis.isEnabled = true
        xAxis.position = XAxis.XAxisPosition.BOTTOM
        xAxis.setDrawAxisLine(false)
        xAxis.axisMinimum = 0f
        xAxis.axisMaximum = 6f
        xAxis.textSize = 6f
        xAxis.valueFormatter = IAxisValueFormatter { value, axis -> listDays[value.roundToInt()] }

        // Y axis data
        mBinding.chartReporting.axisRight.isEnabled = false
        mBinding.chartReporting.axisLeft.isEnabled = true

        mBinding.chartReporting.axisLeft.granularity = 1f
        yAxis.axisMaximum = 4f
        yAxis.axisMinimum = 0f
        mBinding.chartReporting.axisLeft.setDrawGridLines(false)
        mBinding.chartReporting.xAxis.setDrawGridLines(false)
        mBinding.chartReporting.axisLeft.setDrawGridLines(true)
        mBinding.chartReporting.xAxis.setDrawGridLines(false)
        mBinding.chartReporting.axisLeft.gridColor = resources.getColor(R.color.colorWhite)
        mBinding.chartReporting.axisLeft.axisLineColor = resources.getColor(R.color.fulltransparentcolor)
        mBinding.chartReporting.axisRight.axisLineColor = resources.getColor(R.color.colorWhite)
        xAxis.setAvoidFirstLastClipping(true)
        yAxis.setDrawLimitLinesBehindData(true)
        xAxis.setDrawLimitLinesBehindData(true)
        yAxis.textColor = Color.WHITE
        xAxis.textColor = Color.WHITE
        yAxis.textSize = 8f
        yAxis.valueFormatter = IAxisValueFormatter { value, axis ->
            listAmount[value.roundToInt()]
        }

        yAxis.setDrawLimitLinesBehindData(true)
        xAxis.setDrawLimitLinesBehindData(true)

        setData(1)

        mBinding.chartReporting.animateX(500)


        val legend = mBinding.chartReporting.legend
        legend.form = Legend.LegendForm.NONE

    }

    private fun setData(i: Int) {

        when (i) {
            1 -> {
                var groupSpace = 0.2f
                var barSpace = 0.1f// x4 DataSet

                val values1 = ArrayList<BarEntry>()
                val values2 = ArrayList<BarEntry>()


                values1.add(BarEntry(1f, 1700f / 1000))
                values1.add(BarEntry(2f, 1600f / 1000))
                values1.add(BarEntry(3f, 800f / 1000))
                values1.add(BarEntry(5f, 1000f / 1000))
                values1.add(BarEntry(6f, 900f / 1000))
                values1.add(BarEntry(7f, 1400f / 1000))
                values1.add(BarEntry(8f, 1500f / 1000))


                values2.add(BarEntry(1f, 1000f / 1000))
                values2.add(BarEntry(200f, 45f / 100))
                values2.add(BarEntry(300f, 800f / 1000))
                values2.add(BarEntry(50f, 1000f / 1000))
                values2.add(BarEntry(60f, 900f / 1000))
                values2.add(BarEntry(70f, 1400f / 1000))
                values2.add(BarEntry(80f, 1500f / 1000))

                val set1 = BarDataSet(values1, "")
                set1.color = ColorTemplate.rgb("#7760f2")

                val set2 = BarDataSet(values2, "")
                set2.color = Color.rgb(2, 203, 205)
                set1.setDrawValues(false)
                set2.setDrawValues(false)
                set1.formLineWidth = 1f
                set1.formSize = 15f
                set1.valueTextSize = 5f
                set1.isHighlightEnabled = false

                set2.isHighlightEnabled = false
                set2.formLineWidth = 1f
                set2.formSize = 15f
                set2.valueTextSize = 5f

                val data = BarData(set1, set2)
                //        val data = BarData(set1)
                data.setValueFormatter(LargeValueFormatter())
                data.barWidth = .28f
                data.groupBars(-0.2f, groupSpace, barSpace)
                //        mBinding.chartReporting.data = data
                //        mBinding.chartReporting.xAxis.axisMaximum = 6f
                //        mBinding.chartReporting.xAxis.axisMinimum = 0f
                mBinding.chartReporting.setLayerType(View.LAYER_TYPE_SOFTWARE, null)
                mBinding.chartReporting.setScaleEnabled(false)
                mBinding.chartReporting.invalidate()
                // data.setValueTypeface(tfLight)
                mBinding.chartReporting.xAxis.axisMaximum = 6f + 0.5f
                mBinding.chartReporting.xAxis.axisMinimum = 0f - 0.15f
                mBinding.chartReporting.data = data


                //  mBinding.chartReporting.groupBars(startYear.toFloat(), groupSpace, barSpace)
                mBinding.chartReporting.invalidate()

            }
            2 -> {
                val values1 = ArrayList<BarEntry>()
                val values2 = ArrayList<BarEntry>()
                val values3 = ArrayList<BarEntry>()
                val values4 = ArrayList<BarEntry>()
                for (k in 0 until 7) {
                    values1.add(BarEntry(k.toFloat(), (Math.random() * 3).toFloat()))
                    values2.add(BarEntry(k.toFloat(), (Math.random() * 4).toFloat()))
                    values3.add(BarEntry(k.toFloat(), (Math.random() * 2).toFloat()))
                    values4.add(BarEntry(k.toFloat(), (Math.random() * 1).toFloat()))
                }
                //            values1.add(BarEntry(0f, 170f / 100))
                //            values1.add(BarEntry(1f, 90f / 100))
                //            values1.add(BarEntry(2f, 150f / 100))
                //            values1.add(BarEntry(3f, 300f / 100))
                //            values1.add(BarEntry(4f, 350f / 100))
                //            values1.add(BarEntry(5f, 380f / 100))
                //            values1.add(BarEntry(6f, 500f / 100))
                //
                //                values2.add(BarEntry(10f, 180f / 100))
                //                values2.add(BarEntry(20f, 100f / 100))
                //                values2.add(BarEntry(20f, 160f / 100))
                //                values2.add(BarEntry(30f, 200f / 100))
                //                values2.add(BarEntry(40f, 300f / 100))
                //                values2.add(BarEntry(50f, 400f / 100))
                //                values2.add(BarEntry(60f, 420f / 100))

                val set1 = BarDataSet(values1, "")
                val set2 = BarDataSet(values2, "")
                val set3 = BarDataSet(values3, "")
                val set4 = BarDataSet(values4, "")

                set1.color = ColorTemplate.rgb("#7760f2")
                set2.color = ColorTemplate.rgb("#02cbcd")
                set3.color = ColorTemplate.rgb("#ff3f55")
                set4.color = ColorTemplate.rgb("#fcc117")
                //            set1.color = TEAM_COLORS[0]
                //            set2.color = TEAM_COLORS[1]
                //            set3.color = TEAM_COLORS[2]
                //            set4.color = TEAM_COLORS[3]

                set1.setDrawValues(false)
                set2.setDrawValues(false)
                set3.setDrawValues(false)
                set4.setDrawValues(false)

                set1.formLineWidth = 1f
                set2.formLineWidth = 1f
                set3.formLineWidth = 1f
                set4.formLineWidth = 1f

                set1.formSize = 15f
                set2.formSize = 15f
                set3.formSize = 15f
                set4.formSize = 15f

                set1.valueTextSize = 5f
                set2.valueTextSize = 5f
                set3.valueTextSize = 5f
                set4.valueTextSize = 5f

                set1.isHighlightEnabled = false
                set2.isHighlightEnabled = false
                set3.isHighlightEnabled = false
                set4.isHighlightEnabled = false

                val data = BarData(set1, set2, set3, set4)
                data.setValueFormatter(LargeValueFormatter())
                data.barWidth = .4f

                mBinding.chartReporting.setLayerType(View.LAYER_TYPE_SOFTWARE, null)
                mBinding.chartReporting.setScaleEnabled(false)
                mBinding.chartReporting.invalidate()
                mBinding.chartReporting.xAxis.axisMaximum = 6f + 0.3f
                mBinding.chartReporting.xAxis.axisMinimum = 0f - 0.3f
                mBinding.chartReporting.data = data
                mBinding.chartReporting.invalidate()

            }
            3 -> {
                val values1 = ArrayList<BarEntry>()
                val values2 = ArrayList<BarEntry>()
                val values3 = ArrayList<BarEntry>()
                val values4 = ArrayList<BarEntry>()
                for (j in 0 until 14) {
                    values1.add(BarEntry(j.toFloat(), (Math.random() * 2).toFloat()))
                    values2.add(BarEntry(j.toFloat(), (Math.random() * 3).toFloat()))
                    values3.add(BarEntry(j.toFloat(), (Math.random() * 4).toFloat()))
                    values4.add(BarEntry(j.toFloat(), (Math.random() * 1).toFloat()))
                }


                //            values1.add(BarEntry(0f, 2000f / 1000))
                //            values1.add(BarEntry(0f, 70f / 100))
                //            values1.add(BarEntry(1f, 90f / 100))
                //            values1.add(BarEntry(2f, 100f / 100))
                //            values1.add(BarEntry(3f, 150f / 100))
                //            values1.add(BarEntry(4f, 200f / 100))
                //            values1.add(BarEntry(5f, 230f / 100))
                //            values1.add(BarEntry(6f, 400f / 100))

                val set1 = BarDataSet(values1, "")
                val set2 = BarDataSet(values2, "")
                val set3 = BarDataSet(values3, "")
                val set4 = BarDataSet(values4, "")

                set1.color = TEAM_COLORS[0]
                set2.color = TEAM_COLORS[1]
                set3.color = TEAM_COLORS[2]
                set4.color = TEAM_COLORS[3]

                set1.setDrawValues(false)
                set2.setDrawValues(false)
                set3.setDrawValues(false)
                set4.setDrawValues(false)

                set1.formLineWidth = 1f
                set2.formLineWidth = 1f
                set3.formLineWidth = 1f
                set4.formLineWidth = 1f

                set1.formSize = 15f
                set2.formSize = 15f
                set3.formSize = 15f
                set4.formSize = 15f

                set1.valueTextSize = 5f
                set2.valueTextSize = 5f
                set3.valueTextSize = 5f
                set4.valueTextSize = 5f

                set1.isHighlightEnabled = false
                set2.isHighlightEnabled = false
                set3.isHighlightEnabled = false
                set4.isHighlightEnabled = false

                val data = BarData(set1, set3, set2, set4)

                data.setValueFormatter(LargeValueFormatter())
                data.barWidth = .4f

                mBinding.chartReporting.setLayerType(View.LAYER_TYPE_SOFTWARE, null)
                mBinding.chartReporting.setScaleEnabled(false)
                mBinding.chartReporting.invalidate()
                mBinding.chartReporting.xAxis.axisMaximum = 6f + 0.3f
                mBinding.chartReporting.xAxis.axisMinimum = 0f - 0.3f
                mBinding.chartReporting.data = data
                mBinding.chartReporting.invalidate()
            }


            //        mBinding.chartReporting.setOnChartValueSelectedListener(object : OnChartValueSelectedListener {
            //            override fun onNothingSelected() {
            //
            //            }
            //
            //            override fun onValueSelected(e: Entry?, h: Highlight?) {
            //                Log.e("tag", "entryValue = " + h!!.dataIndex)
            //                if (h.dataSetIndex == 0) {
            //                    setData(2)
            //                    mBinding.imageView58.visibility = View.VISIBLE
            //                } else if (h.dataSetIndex == 1) {
            //                    setData(3)
            //                    mBinding.imageView58.visibility = View.VISIBLE
            //                }
            //
            //            }
            //
            //        })
        }


//        mBinding.chartReporting.setOnChartValueSelectedListener(object : OnChartValueSelectedListener {
//            override fun onNothingSelected() {
//
//            }
//
//            override fun onValueSelected(e: Entry?, h: Highlight?) {
//                Log.e("tag", "entryValue = " + h!!.dataIndex)
//                if (h.dataSetIndex == 0) {
//                    setData(2)
//                    mBinding.imageView58.visibility = View.VISIBLE
//                } else if (h.dataSetIndex == 1) {
//                    setData(3)
//                    mBinding.imageView58.visibility = View.VISIBLE
//                }
//
//            }
//
//        })
        mBinding.product1.setOnClickListener {
            setData(2)
            mBinding.imageView58.visibility = View.VISIBLE
            mBinding.recyclerViews.visibility = View.VISIBLE
            mBinding.textView.visibility = View.VISIBLE
            mBinding.textView173.visibility = View.GONE
            mBinding.imageView44.visibility = View.GONE
            mBinding.product1.visibility = View.GONE
            mBinding.product2.visibility = View.GONE
            setAdapter()
        }

        mBinding.product2.setOnClickListener {
            setData(3)
            mBinding.imageView58.visibility = View.VISIBLE
            mBinding.recyclerViews.visibility = View.VISIBLE
            mBinding.textView.visibility = View.VISIBLE
            mBinding.textView173.visibility = View.GONE
            mBinding.imageView44.visibility = View.GONE
            mBinding.product1.visibility = View.GONE
            mBinding.product2.visibility = View.GONE
            setAdapter()
        }


        mBinding.imageView58.setOnClickListener {
            mBinding.imageView58.visibility = View.GONE
            mBinding.recyclerViews.visibility = View.GONE
            mBinding.textView.visibility = View.GONE
            mBinding.textView173.visibility = View.VISIBLE
            mBinding.imageView44.visibility = View.VISIBLE
            mBinding.product1.visibility = View.VISIBLE
            mBinding.product2.visibility = View.VISIBLE
            setData(1)

        }


    }

}
