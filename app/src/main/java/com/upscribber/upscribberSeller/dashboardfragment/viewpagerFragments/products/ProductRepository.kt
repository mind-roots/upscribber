package com.upscribber.upscribberSeller.dashboardfragment.viewpagerFragments.products

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

class ProductRepository(application: Application) {
    var mData = MutableLiveData<List<ModelProduct>>()

    init {
        mData.value = dummyData()
    }
    fun getProductList(): LiveData<List<ModelProduct>> {
        return mData
    }

    private fun dummyData():List<ModelProduct>{
        val list = ArrayList<ModelProduct>()
        for (i in 1 until 10){
            val model = ModelProduct()
            model.productName = "Product $i"
            model.productPrice = "${15*i}"
            model.productOffer = "${5*i}% OFF"
            model.productBought = "${12*i}"
            model.productSales = "$${23*i}"
            if (i == 3 || i == 7){
                model.productSaleIncreasing = false
            }
            list.add(model)
        }

        return list
    }

}