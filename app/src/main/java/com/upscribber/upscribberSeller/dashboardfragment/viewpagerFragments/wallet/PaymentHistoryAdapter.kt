package com.upscribber.upscribberSeller.dashboardfragment.viewpagerFragments.wallet

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.payment.PaymentTransModel
import kotlinx.android.synthetic.main.instant_payouts_tran_list.view.*
import kotlin.collections.ArrayList


class PaymentHistoryAdapter(
    var context: Context,
    var type: Int
) :
    RecyclerView.Adapter<PaymentHistoryAdapter.MyViewHolder>() {

    var mData: ArrayList<PaymentTransModel> = ArrayList()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(context).inflate(R.layout.instant_payouts_tran_list, parent, false)
        return MyViewHolder(v)


    }

    override fun getItemCount(): Int {
        return if (type == 1) {
            3
        } else mData.size

    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        val imagePath = Constant.getPrefs(context).getString(Constant.dataImagePath, "")


            val model = mData[position]
            val date = Constant.getDateMonthYearCompletePayout(model.created_at)

            Glide.with(context).load(imagePath + model.profile_image).placeholder(R.mipmap.circular_placeholder).into(holder.itemView.imageView)
            holder.itemView.personName.text = model.customer_name
            holder.itemView.amount.text = "$" + model.total_amount
            holder.itemView.tranDateTime.text = date




//        if(model.transaction_type == "4"){
//            holder.itemView.transType.text = "REFUND"
//            holder.itemView.transType.setBackgroundResource(R.drawable.bg_sea_green_rounded)
//        }else if(model.transaction_type == "2"){
//            holder.itemView.transType.setBackgroundResource(R.drawable.bg_pink_rounded)
//            holder.itemView.transType.text = "PAYMENT"
//
//        }

    }

    fun update(it: ArrayList<PaymentTransModel>) {
        mData = it
        notifyDataSetChanged()

    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}