package com.upscribber.upscribberSeller.dashboardfragment.viewpagerFragments.wallet

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.upscribber.upscribberSeller.dashboardfragment.dashboard.EventHideTitle
import com.squareup.otto.Subscribe
import com.upscribber.upscribberSeller.navigationWallet.WalletSellerActivity
import com.upscribber.commonClasses.BusProvider
import com.upscribber.R
import com.upscribber.databinding.DashboardFragmentWalletBinding

class FragmentWallet: Fragment() {

    lateinit var mBinding: DashboardFragmentWalletBinding
    lateinit var mModel : WalletViewModel
    override fun onCreateView(
        inflater: LayoutInflater, container:
        ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.dashboard_fragment_wallet, container, false)
        mModel = ViewModelProviders.of(this).get(WalletViewModel::class.java)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mBinding.listHistory.layoutManager = LinearLayoutManager(activity)
        mModel.getPaymentHistory().observe(this, Observer {
//            mBinding.listHistory.adapter = PaymentHistoryAdapter(activity!!, it, 1)
        })

        mBinding.constraintGetPaid.setOnClickListener {
            activity!!.startActivity(Intent(context,WalletSellerActivity::class.java))
        }

    }

    @Subscribe
    public fun hideTitle(event: EventHideTitle) {
        if (event.position == 4)
            mBinding.title.visibility = View.GONE
        else
            mBinding.title.visibility = View.VISIBLE
    }

    override fun onResume() {
        BusProvider.getInstance().register(this)
        super.onResume()
    }

    override fun onPause() {
        BusProvider.getInstance().unregister(this)
        super.onPause()
    }
}