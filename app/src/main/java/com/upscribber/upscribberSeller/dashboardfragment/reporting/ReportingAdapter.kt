package com.upscribber.upscribberSeller.dashboardfragment.reporting

import android.content.Context
import android.graphics.Color
import android.graphics.DashPathEffect
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.util.MyMarkerView
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.IFillFormatter
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import com.upscribber.R
import kotlinx.android.synthetic.main.reporting_sales_layout.view.*

class ReportingAdapter(private val mContext: Context, private val list: ArrayList<ReportingModel>) :
    RecyclerView.Adapter<ReportingAdapter.MyViewHolder>() {
    internal lateinit var set1: LineDataSet

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(mContext).inflate(R.layout.reporting_sales_layout, parent, false)
        return MyViewHolder(v)

    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = list[position]


        holder.itemView.tvSalesPrice.text = model.totalSalePrice
//        holder.itemView.chartReporting = model.graphPlotPrice

        setChart(model.graphPlotPricex.toInt(), model.graphPlotPricey.toInt(), holder)
    }


    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    @RequiresApi(Build.VERSION_CODES.HONEYCOMB)
    private fun setChart(a: Int, b: Int, holder: MyViewHolder) {
        val myMarkerView = MyMarkerView(mContext, R.layout.custom_marker_view)
        myMarkerView.chartView = holder.itemView.chartReporting!!
        holder.itemView.chartReporting!!.marker = myMarkerView
        holder.itemView.chartReporting!!.description.isEnabled = false

        val xAxis: XAxis

        xAxis = holder.itemView.chartReporting!!.xAxis
        holder.itemView.chartReporting!!.xAxis.isEnabled = true
        xAxis.position = XAxis.XAxisPosition.BOTTOM
        xAxis.setDrawAxisLine(false)

        val listDays = ArrayList<String>()
        listDays.add("Mon")
        listDays.add("Tue")
        listDays.add("Wed")
        listDays.add("Thur")
        listDays.add("Fri")
        listDays.add("Sat")
        listDays.add("Sun")


        xAxis.granularity = 1f
//        xAxis.setCenterAxisLabels(true)
//        xAxis.labelRotationAngle = -90f
//        xAxis.valueFormatter = IAxisValueFormatter { value, axis ->
//
//        }

            val yAxis : YAxis
                yAxis = holder.itemView.chartReporting!!.axisLeft
        holder.itemView.chartReporting!!.axisRight.isEnabled = false
        holder.itemView.chartReporting!!.axisLeft.isEnabled = false

        yAxis.axisMaximum = 150f
        yAxis.axisMinimum = -60f

        holder.itemView.chartReporting!!.axisLeft.setDrawGridLines(false)
        holder.itemView.chartReporting!!.xAxis.setDrawGridLines(false)


        yAxis.setDrawLimitLinesBehindData(true)
        xAxis.setDrawLimitLinesBehindData(true)

        setData(a, b, holder)

        holder.itemView.chartReporting!!.animateX(500)


        val legend = holder.itemView.chartReporting!!.legend
        legend.form = Legend.LegendForm.NONE

    }

    private fun setData(count: Int, range: Int, holder: MyViewHolder) {
        val values = ArrayList<Entry>()
        var cnt = 0f
        var cnt2 = 0f
        var dd: Float
        for (i in 0 until count) {
            cnt++
            cnt2--
            //float dd= (float) Math.random();
            if (i % 2 == 0) {
                dd = cnt / 5
            } else {
                dd = cnt2 / 20
            }
            val `val` = dd * range - 30
            values.add(Entry(i.toFloat(), `val`))
        }


        set1 = LineDataSet(values, "")
        set1.setDrawValues(false)

        holder.itemView.chartReporting!!.setLayerType(View.LAYER_TYPE_SOFTWARE, null)
//        holder.itemView.chartReporting!!.renderer.paintRender.setShadowLayer(4f, 5f, 5f,Color.WHITE)

        holder.itemView.chartReporting!!.setScaleEnabled(false)
        set1.color = Color.WHITE
        set1.setDrawCircles(true)
        set1.lineWidth = 4f
        set1.setDrawCircleHole(false)
        set1.formLineWidth = 1f
        set1.formLineDashEffect = DashPathEffect(floatArrayOf(10f, 5f), 0f)
        set1.formSize = 15f
        set1.valueTextSize = 10f
        set1.circleRadius = 4.5f
        set1.setCircleColor(Color.parseColor("#02CBCD"))
        set1.setDrawHorizontalHighlightIndicator(false)
        set1.setDrawVerticalHighlightIndicator(false)
        set1.setDrawFilled(false)
        set1.fillFormatter = IFillFormatter { dataSet,
                                              dataProvider ->
            holder.itemView.chartReporting!!.axisLeft.axisMinimum
        }

        val dataSets = java.util.ArrayList<ILineDataSet>()
        dataSets.add(set1)
        val data = LineData(dataSets)
        holder.itemView.chartReporting!!.data = data
    }

}