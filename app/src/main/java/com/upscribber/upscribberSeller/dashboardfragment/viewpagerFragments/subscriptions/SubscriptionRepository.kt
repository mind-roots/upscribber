package com.upscribber.upscribberSeller.dashboardfragment.viewpagerFragments.subscriptions

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

class SubscriptionRepository(application: Application) {

    var mData = MutableLiveData<List<ModelSubscription>>()

    init {
        mData.value = dummyData()
    }


    fun getSubscriptions(): LiveData<List<ModelSubscription>> {
        return mData
    }

    private fun dummyData():List<ModelSubscription>{
        val list = ArrayList<ModelSubscription>()
        for (i in 1 until 10){
            val model = ModelSubscription()
            model.subscriptionName = "Subscription $i"
            model.subscriptionPrice = "${15*i}"
            model.subscriptionOffer = "${5*i}% OFF"
            model.subscriptionBought = "${12*i}"
            model.subscriptionSales = "$${23*i}"
            if (i == 3 || i == 7){
                model.subscriptionSaleIncreasing = false
            }
            list.add(model)
        }

        return list
    }


}