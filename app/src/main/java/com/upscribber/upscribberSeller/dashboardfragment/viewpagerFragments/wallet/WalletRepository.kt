package com.upscribber.upscribberSeller.dashboardfragment.viewpagerFragments.wallet

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

class WalletRepository(application: Application) {

    var mData = MutableLiveData<List<ModelPaymentHistory>>()

    init {
        mData.value = dummyData()
    }

    private fun dummyData(): List<ModelPaymentHistory>? {
        val list = ArrayList<ModelPaymentHistory>()
        for (i in 1 until 10){
            val model = ModelPaymentHistory()
            model.amount = "$${i*5},000 "
            model.date = "${10+i} Dec 2018"
            list.add(model)
        }
        return list
    }

    fun getHistory(): LiveData<List<ModelPaymentHistory>> {
        return mData
    }
}