package com.upscribber.upscribberSeller.dashboardfragment.viewpagerFragments.reviews

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

class ReviewsRepository(application: Application) {

    var mData = MutableLiveData<List<ModelReviewSeller>>()

    init {
        mData.value = dummyData()
    }

    fun getReviews(): LiveData<List<ModelReviewSeller>> {
        return mData
    }

    private fun dummyData(): List<ModelReviewSeller> {
        val list = ArrayList<ModelReviewSeller>()

//        for (i in 1 until 10){

        var model = ModelReviewSeller()
        model.reviewText = "Excellent Value"
        model.reviewCount = "24"
        model.reviewImage = "http://cloudart.com.au/projects/upscribbr/assets/img/m1.png"
        list.add(model)

        model = ModelReviewSeller()
        model.reviewText = "Professional & Courteous"
        model.reviewCount = "48"
        model.reviewImage = "http://cloudart.com.au/projects/upscribbr/assets/img/m2.png"
        list.add(model)

        model = ModelReviewSeller()
        model.reviewText = "Great Customer Service"
        model.reviewCount = "72"
        model.reviewImage = "http://cloudart.com.au/projects/upscribbr/assets/img/m1.png"
        list.add(model)

        model = ModelReviewSeller()
        model.reviewText = "Super Seller"
        model.reviewCount = "96"
        model.reviewImage = "http://cloudart.com.au/projects/upscribbr/assets/img/m2.png"
        list.add(model)

        model = ModelReviewSeller()
        model.reviewText = "Excellent Value"
        model.reviewCount = "24"
        model.reviewImage = "http://cloudart.com.au/projects/upscribbr/assets/img/m3.png"
        list.add(model)

        model = ModelReviewSeller()
        model.reviewText = "Professional & Courteous"
        model.reviewCount = "48"
        model.reviewImage = "http://cloudart.com.au/projects/upscribbr/assets/img/m2.png"
        list.add(model)

        model = ModelReviewSeller()
        model.reviewText = "Great Customer Service"
        model.reviewCount = "72"
        model.reviewImage = "http://cloudart.com.au/projects/upscribbr/assets/img/m1.png"
        list.add(model)

        model = ModelReviewSeller()
        model.reviewText = "Super Seller"
        model.reviewCount = "96"
        model.reviewImage = "http://cloudart.com.au/projects/upscribbr/assets/img/m2.png"
        list.add(model)
//        }

        return list
    }

}