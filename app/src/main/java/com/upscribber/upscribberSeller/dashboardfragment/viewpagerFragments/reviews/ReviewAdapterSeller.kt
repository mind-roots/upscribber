package com.upscribber.upscribberSeller.dashboardfragment.viewpagerFragments.reviews

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.R
import com.upscribber.databinding.ReviewListItemBinding

class ReviewAdapterSeller(var context: Context, var mData: List<ModelReviewSeller>?, var type: Int) : RecyclerView.Adapter<ReviewAdapterSeller.ViewHolder>() {
    lateinit var mBinding: ReviewListItemBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.review_list_item, parent, false)
        return ViewHolder(mBinding.root)
    }

    override fun getItemCount(): Int {
      return if (type == 1){
            4
        }else mData!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = mData!![position]
        mBinding.review = model

    }

    class ViewHolder(item: View): RecyclerView.ViewHolder(item)
}
