package com.upscribber.upscribberSeller.dashboardfragment.reporting

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.upscribber.upscribberSeller.dashboardfragment.dashboard.EventHideTitle
import com.squareup.otto.Subscribe
import com.upscribber.commonClasses.BusProvider
import com.upscribber.R
import com.upscribber.databinding.FragmentReportingBinding


class FragmentReporting : Fragment() {

    lateinit var mBinding: FragmentReportingBinding

    private  val MIN_SCALE = 0.85f
    private  val MIN_ALPHA = 0.5f

    override fun onCreateView(
        inflater: LayoutInflater, container:
        ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_reporting, container, false)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setAdapter()
    }

    @Subscribe
    public fun hideTitle(event: EventHideTitle) {
        if (event.position == 1)
            mBinding.title.visibility = View.GONE
        else
            mBinding.title.visibility = View.VISIBLE
    }

    override fun onResume() {
        BusProvider.getInstance().register(this)
        super.onResume()
    }

    override fun onPause() {
        BusProvider.getInstance().unregister(this)
        super.onPause()
    }

    private fun setAdapter() {
       // mBinding.viewPager.pageMargin = 15
       // mBinding.viewPager.clipChildren = false
      //  mBinding.viewPager.clipToPadding = false
     //   mBinding.viewPager.setPadding(10,0,20,0)
       // mBinding.viewPager.pageMargin = 10
        try{
            mBinding.viewPager.adapter = ReportingPagerAdapter(this.activity!!, this.fragmentManager!!)
            mBinding.viewPager.setAnimationEnabled(true)
            mBinding.viewPager.setFadeEnabled(true)
            mBinding.viewPager.setFadeFactor(0.6f)
        }catch (e: Exception){

        }

      //  mBinding.viewPager.currentItem = 1
        mBinding.viewPager.adapter = ReportingPagerAdapter(this.activity!!, this.fragmentManager!!)
        mBinding.viewPager.setAnimationEnabled(true)
        mBinding.viewPager.setFadeEnabled(false)
        mBinding.viewPager.setFadeFactor(0.6f)
        mBinding.viewPager.currentItem = 1
       // mBinding.viewPager.setPageTransformer(true, ZoomOutPageTransformer())

    }



  inner class ZoomOutPageTransformer : ViewPager.PageTransformer {

        override fun transformPage(view: View, position: Float) {
            view.apply {
                val pageWidth = width
                val pageHeight = height
                when {
                    position < -1 -> { // [-Infinity,-1)
                        // This page is way off-screen to the left.
                        alpha = 0f
                    }
                    position <= 1 -> { // [-1,1]
                        // Modify the default slide transition to shrink the page as well
                        val scaleFactor = Math.max(MIN_SCALE, 1 - Math.abs(position))
                        val vertMargin = pageHeight * (1 - scaleFactor) / 2
                        val horzMargin = pageWidth * (1 - scaleFactor) / 2
                        translationX = if (position < 0) {
                            horzMargin - vertMargin / 2
                        } else {
                            horzMargin + vertMargin / 2
                        }

                        // Scale the page down (between MIN_SCALE and 1)
                        scaleX = scaleFactor
                        scaleY = scaleFactor

                        // Fade the page relative to its size.
                        alpha = (MIN_ALPHA +
                                (((scaleFactor - MIN_SCALE) / (1 - MIN_SCALE)) * (1 - MIN_ALPHA)))
                    }
                    else -> { // (1,+Infinity]
                        // This page is way off-screen to the right.
                        alpha = 0f
                    }
                }
            }
        }
    }
}
