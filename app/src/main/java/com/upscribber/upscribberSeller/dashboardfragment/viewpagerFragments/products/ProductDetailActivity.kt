package com.upscribber.upscribberSeller.dashboardfragment.viewpagerFragments.products

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.RadioButton
import android.widget.TextView
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.Toolbar
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.upscribber.R
import com.upscribber.databinding.ActivityProductDetailBinding

class ProductDetailActivity : AppCompatActivity() {

    lateinit var mBinding: ActivityProductDetailBinding

    private lateinit var mModel: ProductViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_product_detail)
        mModel = ViewModelProviders.of(this).get(ProductViewModel::class.java)

        mBinding.recyclerProducts.layoutManager = LinearLayoutManager(this)
        mModel.getProducts().observe(this, Observer {
            mBinding.recyclerProducts.adapter = ProductsAdapter(this@ProductDetailActivity, it, 2)
        })

        setToolbar()
        clickListener()
    }

    private fun clickListener() {
        mBinding.sortView.setOnClickListener {
            val inflater = layoutInflater
            val alertLayout = inflater.inflate(R.layout.select_filter_days, null)
            val radioButton = alertLayout.findViewById<RadioButton>(R.id.seven)
            val radioButton2 = alertLayout.findViewById<RadioButton>(R.id.thirty)
            val radioButton3 = alertLayout.findViewById<RadioButton>(R.id.ninety)
            val radioButton4 = alertLayout.findViewById<RadioButton>(R.id.ytd)
            val done = alertLayout.findViewById<TextView>(R.id.done)
            val cancel = alertLayout.findViewById<TextView>(R.id.cancel)
            val alert = AlertDialog.Builder(this)
            alert.setView(alertLayout)
            alert.setCancelable(false)
            val dialog = alert.create()
            done.setOnClickListener {
                if (radioButton.isChecked) {
                    mBinding.sortView.setText("" + radioButton.text.toString())
                } else if(radioButton2.isChecked){
                    mBinding.sortView.setText("" + radioButton2.text.toString())
                }
                else if(radioButton3.isChecked) {
                    mBinding.sortView.setText("" + radioButton3.text.toString())
                }else{
                    mBinding.sortView.setText("" + radioButton4.text.toString())
                }
                dialog.dismiss()
            }

            cancel.setOnClickListener {
                dialog.dismiss()
            }
            dialog.show()
        }


        mBinding.filterView.setOnClickListener {
            val inflater = layoutInflater
            val alertLayout = inflater.inflate(R.layout.select_sorting, null)
            val radioButton = alertLayout.findViewById<RadioButton>(R.id.lowHigh)
            val radioButton2 = alertLayout.findViewById<RadioButton>(R.id.HighLow)
            val done = alertLayout.findViewById<TextView>(R.id.done)
            val cancel = alertLayout.findViewById<TextView>(R.id.cancel)
            val alert = AlertDialog.Builder(this)
            alert.setView(alertLayout)
            alert.setCancelable(false)
            val dialog = alert.create()
            done.setOnClickListener {
                if (radioButton.isChecked) {
                    mBinding.filterView.setText("" + radioButton.text.toString())
                } else {
                    mBinding.filterView.setText("" + radioButton2.text.toString())
                }
                dialog.dismiss()
            }

            cancel.setOnClickListener {
                dialog.dismiss()
            }
            dialog.show()
        }


    }

    private fun setToolbar() {
       val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        val actionbar: ActionBar? = supportActionBar
        actionbar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)
        }
        title = ""
        val toolbarTitle = toolbar.findViewById(R.id.title) as TextView
        toolbarTitle.text = "products"
        supportActionBar!!.elevation = 0f
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}
