package com.upscribber.upscribberSeller.dashboardfragment.viewpagerFragments.subscriptions

interface SubscriptionListener {
    fun onViewDetail()
}