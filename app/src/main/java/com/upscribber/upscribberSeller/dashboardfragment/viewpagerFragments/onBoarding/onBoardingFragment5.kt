package com.upscribber.upscribberSeller.dashboardfragment.viewpagerFragments.onBoarding

import android.os.Bundle
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.upscribber.upscribberSeller.onBoardings.onBoardAdapter
import com.upscribber.upscribberSeller.onBoardings.onBoardModel
import com.upscribber.R
import com.upscribber.databinding.FragmentOnboarding5Binding
import me.relex.circleindicator.Config

class onBoardingFragment5 : Fragment(){

    lateinit var mBinding: FragmentOnboarding5Binding
    var onBoardAdapter: onBoardAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_onboarding5, container, false)
        return mBinding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val array1 = ArrayList<onBoardModel>()

        array1.add(
            onBoardModel(
                R.mipmap.on_board5,
                "Staff",
                "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem"
            )
        )
        array1.add(
            onBoardModel(
                R.mipmap.on_board5,
                "Add Team Members",
                "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem"
            )
        )
        array1.add(
            onBoardModel(
                R.mipmap.on_board5,
                "Assign Customers To Team Members",
                "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem"
            )

        )


        onBoardAdapter = onBoardAdapter(array1)
        mBinding.viewPagerboard1.adapter = onBoardAdapter

        val indicatorWidth = (TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, 9.5f,
            resources.displayMetrics
        ) + 0.5f).toInt()
        val indicatorHeight = (TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, 2f,
            resources.displayMetrics
        ) + 0.5f).toInt()
        val indicatorMargin = (TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, 6f,
            resources.displayMetrics
        ) + 0.5f).toInt()

        val config = Config.Builder().width(indicatorWidth)
            .height(indicatorHeight)
            .margin(indicatorMargin)
            .drawable(R.drawable.bg_seller_button_blue)
            .build()

        mBinding.circleIndi1.initialize(config)
        mBinding.viewPagerboard1.adapter = onBoardAdapter(array1)
        mBinding.circleIndi1.setViewPager(mBinding.viewPagerboard1)

        mBinding.viewPagerboard1.setOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {


            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
                when (position) {
                    0 -> {
                        mBinding.tvGetStarted.visibility = View.GONE
                        mBinding.tvGetStarted.text = "Get Started"

                    }
                    1 -> {
                        mBinding.tvGetStarted.visibility = View.VISIBLE
                        mBinding.tvGetStarted.text = "Add"
                    }
                    2 -> {
                        mBinding.tvGetStarted.visibility = View.VISIBLE
                        mBinding.tvGetStarted.text = "Assign"
                    }
                }

            }

            override fun onPageSelected(position: Int) {

            }

        }

        )
    }

//
//    @Subscribe
//    fun hideTitle(event: EventHideTitle) {
//        if (event.position == 9)
//            mBinding.title.visibility = View.GONE
//        else
//            mBinding.title.visibility = View.VISIBLE
//    }
//
//    override fun onResume() {
//        BusProvider.getInstance().register(this)
//        super.onResume()
//    }
//
//    override fun onPause() {
//        BusProvider.getInstance().unregister(this)
//        super.onPause()
//    }
}