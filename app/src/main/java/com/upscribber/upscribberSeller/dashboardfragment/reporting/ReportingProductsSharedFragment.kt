package com.upscribber.upscribberSeller.dashboardfragment.reporting


import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.graphics.Typeface
import android.os.Build
import android.os.Bundle
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.text.style.RelativeSizeSpan
import android.text.style.StyleSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.upscribber.upscribberSeller.reportingDetail.ReportingActivity
import com.github.mikephil.charting.data.*
import com.github.mikephil.charting.formatter.IAxisValueFormatter
import com.github.mikephil.charting.formatter.LargeValueFormatter
import com.github.mikephil.charting.utils.ColorTemplate
import com.upscribber.R
import com.upscribber.databinding.ReportingProductsLayoutBinding
import com.upscribber.databinding.ReportingProductsSharedLayoutBinding
import com.upscribber.upscribberSeller.reportingDetail.productPerformance.ProductPerfAdapter
import com.upscribber.upscribberSeller.reportingDetail.productPerformance.ProductPerformanceViewModel
import kotlin.math.roundToInt


class ReportingProductsSharedFragment : Fragment(), ReportingEventListener {

    private lateinit var mAdapter: ProductPerfAdapter
    lateinit var mBinding: ReportingProductsSharedLayoutBinding
    lateinit var mViewModel: ProductPerformanceViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(inflater, R.layout.reporting_products_shared_layout, container, false)
        return mBinding.root
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mViewModel = ViewModelProviders.of(this).get(ProductPerformanceViewModel::class.java)
        mBinding.click = this
        setChart()
        setAdapter()
    }

    private fun setAdapter() {
        val manager = LinearLayoutManager(activity)
        manager.orientation = LinearLayout.HORIZONTAL
        mBinding.recyclerProducts.layoutManager = manager
        mAdapter = ProductPerfAdapter(activity!!)
        mBinding.recyclerProducts.adapter = mAdapter
        mViewModel.getTeamsData2().observe(this, Observer {
            mAdapter.updateArray(it)
        })

    }

    override fun onClick() {
        startActivity(Intent(activity, ReportingActivity::class.java).putExtra("value", 5))
    }

    @RequiresApi(Build.VERSION_CODES.HONEYCOMB)
    private fun setChart() {
        mBinding.chartReporting.description.isEnabled = false

        val xAxis: XAxis = mBinding.chartReporting.xAxis

        val listAmount = ArrayList<String>()
        listAmount.add("Mon")
        listAmount.add("Tues")
        listAmount.add("Wed")
        listAmount.add("Thur")
        listAmount.add("Fri")
        listAmount.add("Sat")
        listAmount.add("Sun")

        mBinding.chartReporting.xAxis.isEnabled = false
        xAxis.position = XAxis.XAxisPosition.BOTTOM
        xAxis.setDrawAxisLine(false)
        xAxis.axisMaximum = 68f
        xAxis.axisMinimum = -5f

        // Y axis data
        val yAxis: YAxis = mBinding.chartReporting.axisRight
        mBinding.chartReporting.axisRight.isEnabled = true
        mBinding.chartReporting.axisLeft.isEnabled = false

        yAxis.axisMaximum = 6f
        yAxis.axisMinimum = 0f
        yAxis.textColor = Color.WHITE
        yAxis.textSize = 7f
        yAxis.valueFormatter = IAxisValueFormatter { value, axis -> listAmount[value.roundToInt()] }
        yAxis.labelCount = 7
        mBinding.chartReporting.axisRight.setDrawGridLines(true)
        mBinding.chartReporting.xAxis.setDrawGridLines(false)

        mBinding.chartReporting.axisLeft.axisLineColor = resources.getColor(R.color.invite_edit_text)
        mBinding.chartReporting.axisRight.axisLineColor = resources.getColor(R.color.fulltransparentcolor)
        mBinding.chartReporting.axisRight.gridColor = resources.getColor(R.color.invite_edit_text)
        yAxis.setDrawLimitLinesBehindData(true)
        xAxis.setDrawLimitLinesBehindData(true)
        yAxis.labelCount = 6
        xAxis.labelCount = 8
        setData()

        mBinding.chartReporting.animateX(500)


        val legend = mBinding.chartReporting.legend
        legend.form = Legend.LegendForm.NONE

    }

    private fun setData() {
        val values0 = ArrayList<BarEntry>()
        val values50 = ArrayList<BarEntry>()
        val values40 = ArrayList<BarEntry>()
        val values42 = ArrayList<BarEntry>()
        val values56 = ArrayList<BarEntry>()
        val values70 = ArrayList<BarEntry>()
        val values71 = ArrayList<BarEntry>()

        values0.add(BarEntry(0f, 700f / 1))
        values50.add(BarEntry(10f, 900f / 1))
        values40.add(BarEntry(20f, 1000f / 1))
        values42.add(BarEntry(30f, 1500f / 1))
        values56.add(BarEntry(40f, 1700f / 1))
        values70.add(BarEntry(50f, 1900f / 1))
        values71.add(BarEntry(60f, 2000f / 1))

        val set1 = BarDataSet(values0, "")
        val set2 = BarDataSet(values50, "")
        val set3 = BarDataSet(values40, "")
        val set4 = BarDataSet(values42, "")
        val set5 = BarDataSet(values56, "")
        val set6 = BarDataSet(values70, "")
        val set7 = BarDataSet(values71, "")

        set1.color = ColorTemplate.rgb("#6981ec")
        set2.color = ColorTemplate.rgb("#89939a")
        set3.color = ColorTemplate.rgb("#C5CAE9")
        set4.color = ColorTemplate.rgb("#fcc117")
        set5.color = ColorTemplate.rgb("#02CBCD")
        set6.color = ColorTemplate.rgb("#ff3f55")
        set7.color = ColorTemplate.rgb("#F8BBD0")


        set1.setDrawValues(false)
        set2.setDrawValues(false)
        set3.setDrawValues(false)
        set4.setDrawValues(false)
        set5.setDrawValues(false)
        set6.setDrawValues(false)
        set7.setDrawValues(false)
        mBinding.chartReporting.setLayerType(View.LAYER_TYPE_SOFTWARE, null)
        mBinding.chartReporting.setScaleEnabled(false)

        set1.formLineWidth = 1f
        set1.formSize = 10f
        set1.valueTextSize = 5f
        set1.isHighlightEnabled = false

        set2.formLineWidth = 1f
        set2.formSize = 10f
        set2.valueTextSize = 5f
        set2.isHighlightEnabled = false

        set3.formLineWidth = 1f
        set3.formSize = 10f
        set3.valueTextSize = 5f
        set3.isHighlightEnabled = false

        set4.formLineWidth = 1f
        set4.formSize = 10f
        set4.valueTextSize = 5f
        set4.isHighlightEnabled = false

        set5.formLineWidth = 1f
        set5.formSize = 10f
        set5.valueTextSize = 5f
        set5.isHighlightEnabled = false

        set6.formLineWidth = 1f
        set6.formSize = 10f
        set6.valueTextSize = 5f
        set6.isHighlightEnabled = false

        set7.formLineWidth = 1f
        set7.formSize = 10f
        set7.valueTextSize = 5f
        set7.isHighlightEnabled = false


        val data = BarData(set1, set2, set3, set4, set5, set6, set7)
        data.setValueFormatter(LargeValueFormatter())
        data.barWidth = 5f
//            data.groupBars(2f, groupSpace, barSpace)
        mBinding.chartReporting.data = data
        mBinding.chartReporting.invalidate()
    }




}
