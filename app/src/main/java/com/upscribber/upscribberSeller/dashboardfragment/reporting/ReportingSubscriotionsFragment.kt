package com.upscribber.upscribberSeller.dashboardfragment.reporting

import android.content.Intent
import android.graphics.Color
import android.graphics.DashPathEffect
import android.graphics.Typeface
import android.os.Build
import android.os.Bundle
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.text.style.RelativeSizeSpan
import android.text.style.StyleSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.upscribber.upscribberSeller.reportingDetail.ReportingActivity
import com.github.mikephil.charting.data.*
import com.github.mikephil.charting.formatter.LargeValueFormatter
import com.github.mikephil.charting.utils.ColorTemplate
import com.upscribber.R
import com.upscribber.databinding.ReportingSubscriptionsLayoutBinding
import com.upscribber.upscribberSeller.reportingDetail.teamPerformance.TeamViewModel

class ReportingSubscriotionsFragment : Fragment(), ReportingEventListener {
    lateinit var mBinding: ReportingSubscriptionsLayoutBinding
    private lateinit var mAdapter: ViewsAdapter
    lateinit var mViewModel: TeamViewModel


    val SUBSCRIPTION_COLORS = intArrayOf(
        ColorTemplate.rgb("#7760f2"),
        ColorTemplate.rgb("#fcc117"),
        ColorTemplate.rgb("#02cbcd"),
        ColorTemplate.rgb("#ee9ae5"),
        ColorTemplate.rgb("#ff3f55"),
        ColorTemplate.rgb("#1391ff")
    )
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(inflater, R.layout.reporting_subscriptions_layout, container, false)
        mViewModel = ViewModelProviders.of(this).get(TeamViewModel::class.java)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mBinding.click = this
        setChart()
        setAdapter()
    }

    private fun setAdapter() {

            val manager = LinearLayoutManager(activity)
            manager.orientation = LinearLayout.HORIZONTAL
            mBinding.recyclerItems.layoutManager = manager
            mAdapter = ViewsAdapter(activity!!)
            mBinding.recyclerItems.adapter = mAdapter
            mViewModel.getProductsData().observe(this, Observer {
                mAdapter.updateArray(it)
            })
    }

    override fun onClick() {
        startActivity(Intent(activity, ReportingActivity::class.java).putExtra("value", 2))
    }

    @RequiresApi(Build.VERSION_CODES.HONEYCOMB)
    private fun setChart() {
        mBinding.chartReporting.description.isEnabled = false

        setData()

        mBinding.chartReporting.animateX(500)

        mBinding.chartReporting.centerText = generateCenterSpannableText()
        mBinding.chartReporting.holeRadius = 75f
        mBinding.chartReporting.setHoleColor(Color.BLACK)
        mBinding.chartReporting.centerTextRadiusPercent = 80f
        mBinding.chartReporting.isRotationEnabled = false

        val legend = mBinding.chartReporting.legend
        legend.isEnabled = false
    }

    private fun setData() {


        val values1 = ArrayList<PieEntry>()
        val values2 = ArrayList<PieEntry>()


        for (i in 0 until 7) {
            values1.add(PieEntry(i.toFloat(), (Math.random() * 140).toFloat()))
            values2.add(PieEntry(i.toFloat(), (Math.random() * 140).toFloat()))
        }

        val set1 = PieDataSet(values1, "Legend 1")
        set1.color = Color.rgb(119, 96, 242)

        val set2 = PieDataSet(values2, "")
        set2.color = Color.rgb(2,203,205)
        set1.setDrawValues(false)
        set2.setDrawValues(false)

        mBinding.chartReporting.setLayerType(View.LAYER_TYPE_SOFTWARE, null)
        //  mBinding.chartReporting.setScaleEnabled(false)

        set1.formLineWidth = 1f
        //  set1.formLineDashEffect = DashPathEffect(floatArrayOf(10f, 5f), 0f)
        set1.formSize = 15f
        set1.valueTextSize = 5f


        set2.formLineWidth = 1f
        //  set2.formLineDashEffect = DashPathEffect(floatArrayOf(10f, 5f), 0f)
        set2.formSize = 15f
        set2.valueTextSize = 5f


        val colors = ArrayList<Int>()

        for (c in SUBSCRIPTION_COLORS)
            colors.add(c)

        set1.colors = colors




        val data = PieData()
        data.dataSet = set1
        //data.dataSet = set2
        data.setValueFormatter(LargeValueFormatter())
        mBinding.chartReporting.data = data

        //   mBinding.chartReporting.groupBars(startYear.toFloat(), groupSpace, barSpace)
        mBinding.chartReporting.invalidate()
    }


    private fun generateCenterSpannableText(): SpannableString {

        val s = SpannableString("100\nTotal")
        s.setSpan(RelativeSizeSpan(1.5f), 0, s.length-5, 0)
        s.setSpan(StyleSpan(Typeface.BOLD),   0, s.length - 5, 0)
        s.setSpan(ForegroundColorSpan(Color.WHITE),   0, s.length - 5, 0)
        s.setSpan(ForegroundColorSpan(Color.WHITE), s.length-5, s.length, 0)
        return s
    }

}
