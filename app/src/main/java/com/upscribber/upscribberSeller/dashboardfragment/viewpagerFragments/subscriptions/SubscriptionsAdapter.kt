package com.upscribber.upscribberSeller.dashboardfragment.viewpagerFragments.subscriptions

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.upscribberSeller.subsriptionSeller.subscriptionView.ActivitySubscriptionSeller
import com.upscribber.R
import com.upscribber.databinding.SubscriptionRecyclerLayoutBinding

class SubscriptionsAdapter(var context: Context, var mData: List<ModelSubscription>?, var type: Int) :
    RecyclerView.Adapter<SubscriptionsAdapter.ViewHolder>() {
    lateinit var mBinding: SubscriptionRecyclerLayoutBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        mBinding =
            DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.subscription_recycler_layout, parent, false)
        return ViewHolder(mBinding.root)
    }

    override fun getItemCount(): Int {
        return if (type == 1) {
            4
        } else mData!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = mData!![position]
        mBinding.subscription = model

        holder.itemView.setOnClickListener {
            context.startActivity(Intent(context,ActivitySubscriptionSeller::class.java))
        }
    }

    class ViewHolder(item: View) : RecyclerView.ViewHolder(item)
}
