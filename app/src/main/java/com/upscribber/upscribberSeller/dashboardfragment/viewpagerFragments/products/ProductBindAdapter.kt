package com.upscribber.upscribberSeller.dashboardfragment.viewpagerFragments.products

import android.widget.ImageView
import androidx.databinding.BindingAdapter

@BindingAdapter("drawableIcon")
fun addIndicatorIcons(image: ImageView, res : Int){
    image.setImageResource(res)
}