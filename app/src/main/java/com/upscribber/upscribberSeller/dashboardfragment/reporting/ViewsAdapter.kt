package com.upscribber.upscribberSeller.dashboardfragment.reporting

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.databinding.SubscriptionDashRecyclerLayoutBinding
import com.upscribber.upscribberSeller.reportingDetail.subscription.ModelSubscriptionRecycler

class ViewsAdapter(var context: Context) : RecyclerView.Adapter<ViewsAdapter.ViewHolder>() {

    lateinit var mBinding : SubscriptionDashRecyclerLayoutBinding
    var mData: List<ModelSubscriptionRecycler> = ArrayList()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        mBinding = SubscriptionDashRecyclerLayoutBinding.inflate(LayoutInflater.from(context), parent, false)
        return  ViewHolder(mBinding)
    }

    override fun getItemCount(): Int {
        return mData.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model =mData[position]
        holder.bind(model)
    }

    fun updateArray(it: List<ModelSubscriptionRecycler>?) {
        mData = it!!
        notifyDataSetChanged()
    }

    class ViewHolder(var binding: SubscriptionDashRecyclerLayoutBinding): RecyclerView.ViewHolder(binding.root){
        fun bind(model : ModelSubscriptionRecycler){
            binding.team = model
            binding.executePendingBindings()
        }
    }
}
