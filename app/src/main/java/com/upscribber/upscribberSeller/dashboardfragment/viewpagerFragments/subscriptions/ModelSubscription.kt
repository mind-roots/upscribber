package com.upscribber.upscribberSeller.dashboardfragment.viewpagerFragments.subscriptions

import com.upscribber.R


class ModelSubscription {
    lateinit var subscriptionName: String
    lateinit var subscriptionPrice: String
    lateinit var subscriptionOffer: String
    lateinit var subscriptionBought: String
    var subscriptionSaleIncreasing: Boolean = true
    lateinit var subscriptionSales: String

    fun getIcon(): Int{
        if (subscriptionSaleIncreasing){
            return R.drawable.ic_add_green
        }
        return R.drawable.ic_minus_red
    }
}