package com.upscribber.upscribberSeller.dashboardfragment.viewpagerFragments.products

import com.upscribber.R


class ModelProduct {
    lateinit var productName: String
    lateinit var productPrice: String
    lateinit var productOffer: String
    lateinit var productBought: String
    var productSaleIncreasing: Boolean = true
    lateinit var productSales: String

    fun getIcon(): Int{
        if (productSaleIncreasing){
            return R.drawable.ic_add_green
        }
        return R.drawable.ic_minus_red
    }
}