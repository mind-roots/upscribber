package com.upscribber.upscribberSeller.dashboardfragment.viewpagerFragments.subscriptions

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.upscribber.R
import com.upscribber.databinding.SubscriptionLayoutBinding

class FragmentSubscription: Fragment(), SubscriptionListener {


    private lateinit var mBinding: SubscriptionLayoutBinding
    private lateinit var mViewModel: SubscriptionViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container:
        ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.subscription_layout, container, false)
        mViewModel = ViewModelProviders.of(this).get(SubscriptionViewModel::class.java)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mBinding.recyclerSubscriptionsDashboard.layoutManager = LinearLayoutManager(activity)
        mViewModel.getSubscriptionList().observe(this, Observer {
            mBinding.recyclerSubscriptionsDashboard.adapter = SubscriptionsAdapter(activity!!, it, 1)
        })

        mBinding.event = this
    }

//    @Subscribe
//    public fun hideTitle(event: EventHideTitle) {
//        if (event.position == 2)
//            mBinding.title.visibility = View.GONE
//        else
//            mBinding.title.visibility = View.VISIBLE
//    }
//
//    override fun onResume() {
//        BusProvider.getInstance().register(this)
//        super.onResume()
//    }
//
//    override fun onPause() {
//        BusProvider.getInstance().unregister(this)
//        super.onPause()
//    }

    override fun onViewDetail() {
        startActivity(Intent(activity, subscriptionDetailActivity::class.java))
    }
}