package com.upscribber.upscribberSeller.dashboardfragment.dashboard

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.upscribber.upscribberSeller.dashboardfragment.reporting.FragmentReporting
import com.upscribber.upscribberSeller.dashboardfragment.viewpagerFragments.EmptyFragment
import com.upscribber.upscribberSeller.dashboardfragment.viewpagerFragments.FragmentTop
import com.upscribber.upscribberSeller.dashboardfragment.viewpagerFragments.customers.MainPagerCustomerFragment
import com.upscribber.upscribberSeller.dashboardfragment.viewpagerFragments.onBoarding.*
import com.upscribber.upscribberSeller.dashboardfragment.viewpagerFragments.reviews.FragmentDashboardReviews
import com.upscribber.upscribberSeller.dashboardfragment.viewpagerFragments.wallet.FragmentWallet

class FragmentAdapter(fm: FragmentManager, var ratio: Float) : FragmentPagerAdapter(fm) {

    var fr1 = FragmentTop()
    var fr2 = FragmentReporting()
    //    var fr3 = FragmentSubscription()
//    var fr4 = FragmentProducts()
    var fr5 = FragmentDashboardReviews()
    var fr6 = MainPagerCustomerFragment()
    var fr7 = FragmentWallet()
    var fr8 = onBoardingFragment1()
    var fr9 = onBoardingFragment2()
    var fr10 = onBoardingFragment3()
    var fr11 = onBoardingFragment4()
    var fr12 = onBoardingFragment5()
    var fr13 = EmptyFragment()


    override fun getItem(position: Int): Fragment {
        when (position) {
            0 -> return fr1
            1 -> return fr2
//            2 -> return fr3
//            3 -> return fr4
            2 -> return fr5
            3 -> return fr6
            4 -> return fr7
            5 -> return fr8
            6 -> return fr9
            7 -> return fr10
            8 -> return fr11
            9 -> return fr12
            10 -> return fr13

        }
        return null!!
    }

    override fun getCount(): Int {
        return 11
    }

    override fun getPageWidth(position: Int): Float {
        return if (ratio < 1.8) {
            .90f
        } else if (ratio >= 1.8 && ratio < 2.0) {
            .85f
        } else if (ratio >= 2.0 && ratio < 2.5) {
            0.89f
        }else{
            .67f
        }
    }

}

