package com.upscribber.upscribberSeller.dashboardfragment.viewpagerFragments.products

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.R
import com.upscribber.databinding.ProductListItemBinding
import com.upscribber.upscribberSeller.store.storeInfo.StoreProductInfoActivity

class ProductsAdapter(var context: Context, var mData: List<ModelProduct>?, var type: Int) : RecyclerView.Adapter<ProductsAdapter.ViewHolder>() {
    lateinit var mBinding: ProductListItemBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.product_list_item, parent, false)
        return ViewHolder(mBinding.root)
    }

    override fun getItemCount(): Int {
      return if (type == 1){
            4
        }else mData!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = mData!![position]
        mBinding.product = model

        holder.itemView.setOnClickListener {
            context.startActivity(Intent(context, StoreProductInfoActivity::class.java))
        }

    }

    class ViewHolder(item: View): RecyclerView.ViewHolder(item)
}
