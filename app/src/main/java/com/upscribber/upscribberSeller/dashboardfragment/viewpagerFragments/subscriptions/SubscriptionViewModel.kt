package com.upscribber.upscribberSeller.dashboardfragment.viewpagerFragments.subscriptions

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData

class SubscriptionViewModel(application: Application) : AndroidViewModel(application) {
    var mSubscriptions: LiveData<List<ModelSubscription>>
    var mRepository: SubscriptionRepository = SubscriptionRepository(application)

    init {
        mSubscriptions = mRepository.getSubscriptions()
    }
    fun getSubscriptionList(): LiveData<List<ModelSubscription>> {
        return mSubscriptions
    }
}