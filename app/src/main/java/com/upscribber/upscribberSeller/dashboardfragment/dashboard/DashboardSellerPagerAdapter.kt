package com.upscribber.upscribberSeller.dashboardfragment.dashboard

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.viewpager.widget.PagerAdapter
import com.upscribber.R
import com.upscribber.notification.ModelNotificationCustomer
import de.hdodenhof.circleimageview.CircleImageView

class DashboardSellerPagerAdapter(
    var context: Context,
    var contextt: DashboardFragment,
    var mData: ArrayList<ModelNotificationCustomer>
) :
    PagerAdapter() {

    var listener = contextt as SellerHomePagerClicks
    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun getCount(): Int {
        return mData.size
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val itemView = LayoutInflater.from(container.context)
            .inflate(R.layout.alert_home, container, false)

        val model = mData[position]
        val textDescription = itemView.findViewById(R.id.textDescription) as TextView
        val requestTxt = itemView.findViewById(R.id.requestTxt) as TextView
        val constraintMain = itemView.findViewById(R.id.constraintMain) as ConstraintLayout
        val imgShow = itemView.findViewById(R.id.imgShow) as ImageView
        val imgShowRevies = itemView.findViewById(R.id.imgShowRevies) as CircleImageView
        val btnAccept = itemView.findViewById(R.id.btnAccept) as TextView
        val buttnUpdate = itemView.findViewById(R.id.buttnUpdate) as TextView

        if (model.type == "6") {
            btnAccept.visibility = View.VISIBLE
            imgShow.visibility = View.VISIBLE
            buttnUpdate.visibility = View.GONE
            imgShowRevies.visibility = View.GONE
            constraintMain.setBackgroundResource(R.drawable.gradident_popups_merchant)
            requestTxt.setTextColor(context.resources.getColor(R.color.colorWhite))
            textDescription.setTextColor(context.resources.getColor(R.color.colorWhite))
            imgShow.setImageResource(R.drawable.ic_dollar_min)
            textDescription.text =
                model.customer_name + " has requested for Refund for " + model.campaign_name + " for the amount $" + model.total_amount
            requestTxt.text = "Refund Request"
            btnAccept.text = "Proceed"

        } else if (model.type == "9") {
            btnAccept.visibility = View.VISIBLE
            imgShow.visibility = View.VISIBLE
            buttnUpdate.visibility = View.GONE
            imgShowRevies.visibility = View.GONE
            constraintMain.setBackgroundResource(R.drawable.gradident_popups_merchant)
            requestTxt.setTextColor(context.resources.getColor(R.color.colorWhite))
            textDescription.setTextColor(context.resources.getColor(R.color.colorWhite))
            imgShow.setImageResource(R.mipmap.circular_placeholder)
            textDescription.text =
                "You are so popular! Subscriber limit reached. Please click below to increase the limit for " + model.name +"."
            requestTxt.text = "Subscriber Limit Reached"
            btnAccept.text = "Increase Limit"
        }else if (model.type == "10") {
            btnAccept.visibility = View.VISIBLE
            imgShow.visibility = View.VISIBLE
            buttnUpdate.visibility = View.GONE
            imgShowRevies.visibility = View.GONE
            constraintMain.setBackgroundResource(R.drawable.gradident_popups_merchant)
            requestTxt.setTextColor(context.resources.getColor(R.color.colorWhite))
            textDescription.setTextColor(context.resources.getColor(R.color.colorWhite))
            imgShow.setImageResource(R.drawable.ic_dollar_min)
            textDescription.text =
                model.name + " has requested to relink as staff with your business account"
            requestTxt.text = "Staff link request"
            btnAccept.text = "Proceed"
        }

        btnAccept.setOnClickListener {
            when (model.type) {
                "6" -> {
                    listener.getSellerPaggerData(model)
                }
                "9" -> {
                    listener.getIncreaseLimit(model)
                }
                "10" -> {
                    listener.getSellerRelink(model)
                }

            }
        }


        container.addView(itemView)
        return itemView
    }


    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View?)
    }

    interface SellerHomePagerClicks {
        fun getSellerPaggerData(modelHomeDataSubscriptions: ModelNotificationCustomer)
        fun getSellerRelink(model: ModelNotificationCustomer)
        fun getIncreaseLimit(model: ModelNotificationCustomer)
    }

}

