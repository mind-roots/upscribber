package com.upscribber.upscribberSeller.dashboardfragment;

import android.content.Context;
import android.view.View;
import androidx.core.view.ViewCompat;
import androidx.viewpager.widget.ViewPager;

public class CarouselEffectTransformer implements ViewPager.PageTransformer {

    private int maxTranslateOffsetX;
    private ViewPager viewPager;

    public CarouselEffectTransformer(Context context) {
        this.maxTranslateOffsetX = dp2px(context, 180);
    }

    public void transformPage(View view, float position) {
        if (viewPager == null) {
            viewPager = (ViewPager) view.getParent();
        }

        int leftInScreen = view.getTop() - viewPager.getScrollY();
        int centerXInViewPager = leftInScreen + view.getMeasuredHeight() / 2;
        int offsetX = centerXInViewPager - viewPager.getMeasuredHeight() / 2;
        float offsetRate = (float) offsetX * 0.38f / viewPager.getMeasuredHeight();
        float scaleFactor = 1 - Math.abs(offsetRate);

        if (scaleFactor > 0) {
            view.setScaleX(scaleFactor);
            view.setScaleY(scaleFactor);
            view.setTranslationY(-maxTranslateOffsetX * offsetRate);
            //ViewCompat.setElevation(view, 0.0f);
        }
        ViewCompat.setElevation(view, scaleFactor);

    }

    /**
     * Dp to pixel conversion
     */
    private int dp2px(Context context, float dipValue) {
        float m = context.getResources().getDisplayMetrics().density;
        return (int) (dipValue * m + 0.5f);
    }

}