package com.upscribber.upscribberSeller.dashboardfragment.viewpagerFragments.reviews

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData

class ReviewViewModel(application: Application) : AndroidViewModel(application) {

    var mRepository = ReviewsRepository(application)
    var mReviews: LiveData<List<ModelReviewSeller>>

    init {
        mReviews = mRepository.getReviews()
    }

    fun getReviewsList(): LiveData<List<ModelReviewSeller>> {
        return mReviews
    }


}