package com.upscribber.upscribberSeller.dashboardfragment.viewpagerFragments.products

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.upscribber.R
import com.upscribber.databinding.DashboardProductLayoutBinding

/**
 * Created by amrit on 6/4/19.
 */
class FragmentProducts : Fragment() , ProductListener{
    lateinit var mViewModel: ProductViewModel
    lateinit var mBinding: DashboardProductLayoutBinding
    override fun onCreateView(
        inflater: LayoutInflater, container:
        ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.dashboard_product_layout, container, false)
        mViewModel = ViewModelProviders.of(this).get(ProductViewModel::class.java)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mBinding.recyclerProducts.layoutManager = LinearLayoutManager(activity)
        mViewModel.getProducts().observe(this, Observer {
            mBinding.recyclerProducts.adapter = ProductsAdapter(activity!!, it, 1)
        })
        mBinding.listener = this
    }

//    @Subscribe
//    public fun hideTitle(event: EventHideTitle) {
//        if (event.position == 3)
//            mBinding.title.visibility = View.GONE
//        else
//            mBinding.title.visibility = View.VISIBLE
//    }
//
//    override fun onResume() {
//        BusProvider.getInstance().register(this)
//        super.onResume()
//    }
//
//    override fun onPause() {
//        BusProvider.getInstance().unregister(this)
//        super.onPause()
//    }
//
    override fun onViewDetail() {
        startActivity(Intent(activity, ProductDetailActivity::class.java))
    }
}