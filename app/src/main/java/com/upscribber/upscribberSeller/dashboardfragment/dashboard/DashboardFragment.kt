package com.upscribber.upscribberSeller.dashboardfragment.dashboard

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.*
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.viewpager.widget.ViewPager
import com.upscribber.R
import com.upscribber.commonClasses.BusProvider
import com.upscribber.commonClasses.Constant
import com.upscribber.home.ZoomOutPageTransformer
import com.upscribber.notification.ModelNotificationCustomer
import com.upscribber.notification.notifications.NotificationActivity
import com.upscribber.notification.notifications.NotificationViewModel
import com.upscribber.profile.ActivityMerchantDetailPage
import com.upscribber.upscribberSeller.accountLinking.LinkingRequestActivity
import com.upscribber.upscribberSeller.subsriptionSeller.subscriptionView.ActivitySubscriptionSeller
import fr.castorflex.android.verticalviewpager.VerticalViewPager
import kotlinx.android.synthetic.main.alert_already_linked.*
import kotlinx.android.synthetic.main.fragment_dashboard.*
import kotlin.math.abs
import kotlin.math.max

class DashboardFragment : Fragment(), DashboardSellerPagerAdapter.SellerHomePagerClicks {

    private val MIN_SCALE = 1f
    private val MIN_ALPHA = 1f
    private lateinit var pager: VerticalViewPager
    var myCustomPagerAdapter: DashboardSellerPagerAdapter? = null
    private lateinit var viewPager: ViewPager
    private lateinit var dismissText: TextView
    private lateinit var viewModel: DashboardSellerViewModel
    private lateinit var viewModel1: NotificationViewModel
    private lateinit var mAdapter: FragmentAdapter
    private var isClicked: Boolean = false
    var modelNotifications = ModelNotificationCustomer()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val mView = inflater.inflate(R.layout.fragment_dashboard, container, false)
        viewModel = ViewModelProviders.of(this)[DashboardSellerViewModel::class.java]
        viewModel1 = ViewModelProviders.of(this)[NotificationViewModel::class.java]
        pager = mView.findViewById(R.id.verticalViewPager)
        viewPager = mView.findViewById(R.id.viewPager)
        dismissText = mView.findViewById(R.id.dismissText)

        Handler().postDelayed({
            pager.adapter = mAdapter


            pager.offscreenPageLimit = 12
            // pager.setPageTransformer(false, SlideUpTransformer())
            pager.setPageTransformer(
                true
            ) { view, position ->
                val pageWidth = view.width
                val pageHeight = view.height

                when {
                    position < -1 -> // [-Infinity,-1)
                        view.alpha = 0f
                    position <= 1 -> { // [-1,1]
                        // Modify the default slide transition to shrink the page as well

//                        view.alpha = 0f
                        val scaleFactor = max(MIN_SCALE, 1 - abs(position))
                        val vertMargin = pageHeight * (1 - scaleFactor) / 2
                        val horzMargin = pageWidth * (1 - scaleFactor) / 2
                        if (position < 0) {
                            view.translationY = vertMargin - horzMargin / 3
                        } else {
                            view.translationY = -vertMargin + horzMargin / 3
                        }

                        // Scale the page down (between MIN_SCALE and 1)
                        view.scaleX = scaleFactor
                        view.scaleY = scaleFactor

                        // Fade the page relative to its size.
                        view.alpha =
                            MIN_ALPHA + (scaleFactor - MIN_SCALE) / (1 - MIN_SCALE) * (1 - MIN_ALPHA)

                    }
                    else -> // (1,+Infinity]
                        // This page is way off-screen to the right.
                        view.alpha = 0f
                }
            }

        }, 300)

        pager.setOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                BusProvider.getInstance().post(EventUpdateTitle(position))
                BusProvider.getInstance().post(EventHideTitle(position))

            }

        })
        apiInit()
        setObserver()
        clickListener()
        observerInit()

        return mView
    }

    private fun observerInit() {
        viewModel1.mDataNotifications().observe(this, Observer {
            if (it.status == "true") {
                val homeProfile = Constant.getArrayListProfile(activity!!, Constant.dataProfile)
                if (homeProfile.type == "1" && homeProfile.steps == "6" || homeProfile.type == "2" && homeProfile.steps == "4" || homeProfile.type == "3") {
                    commonAlertConditions(it)
                } else {
                    if (isClicked) {
                        isClicked = false
                        if (isClicked) {
                            isClicked = false
                            val intent = Intent(activity, LinkingRequestActivity::class.java)
                                .putExtra("relink", "relink")
                                .putExtra("fromNotification", "fromNotification")
                                .putExtra("model", it)
                                .putExtra("staffId", modelNotifications.staff_id)
                            startActivity(intent)
                        }
                    }
                }
            }
        })
    }

    private fun commonAlertConditions(it: ModelNotificationCustomer) {
        val homeProfile = Constant.getArrayListProfile(activity!!, Constant.dataProfile)
        if (isClicked) {
            isClicked = false
            val mDialogView = LayoutInflater.from(activity)
                .inflate(R.layout.alert_already_linked, null)
            val mBuilder =
                android.app.AlertDialog.Builder(activity).setView(mDialogView)
            val mAlertDialog = mBuilder.show()
            mAlertDialog.setCancelable(false)
            mDialogView.setBackgroundResource(R.drawable.bg_alert_new)
            mAlertDialog.window.setBackgroundDrawableResource(R.drawable.bg_alert_transparent)
            if (homeProfile.type == "1" && homeProfile.steps == "6") {
                if (it.type2 == "1") {
                    mAlertDialog.textInfo.setText(R.string.merchant_info)
                } else {
                    mAlertDialog.textInfo.setText(R.string.merchant_info1)
                }
            } else if (homeProfile.type == "2" && homeProfile.steps == "4") {
                if (it.type2 == "1") {
                    mAlertDialog.textInfo.setText(R.string.contractor_info1)
                } else {
                    mAlertDialog.textInfo.setText(R.string.contractor_info)
                }
            } else if (homeProfile.type == "3") {
                if (it.type2 == "1") {
                    mAlertDialog.textInfo.setText(R.string.non_contractor_info)
                } else {
                    mAlertDialog.textInfo.setText(R.string.non_contractor_info1)
                }
            }

            mAlertDialog.okBttn.setOnClickListener {
                mAlertDialog.dismiss()
            }
        }

    }

    private fun clickListener() {
        dismissText.setOnClickListener {
            val auth = Constant.getPrefs(activity!!).getString(Constant.auth_code, "")
            viewModel.getDeclinedApi(auth)
        }

    }

    private fun setObserver() {

        viewModel.mDataNotification().observe(this, Observer {
            if (it.size > 0) {
                if (it[0].status == "true") {
                    linearViewPager.visibility = View.GONE
                    myCustomPagerAdapter = DashboardSellerPagerAdapter(activity!!, this, it)
                    viewPager.adapter = myCustomPagerAdapter
                    viewPager.setPageTransformer(true, ZoomOutPageTransformer())

                } else {
                    linearViewPager.visibility = View.GONE
                }
            }else{
                linearViewPager.visibility = View.GONE
            }
        })

        viewModel.getmDataDeclined().observe(this, Observer {
            if (it.status == "true") {
                linearViewPager.visibility = View.GONE
            }
        })

    }

    private fun apiInit() {
        val auth = Constant.getPrefs(activity!!).getString(Constant.auth_code, "")

        viewModel.getSellerPagerData(auth)

    }

    override fun onResume() {
//        setHasOptionsMenu(true)
        super.onResume()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menu.clear()
        inflater.inflate(R.menu.dashboard, menu)
        super.onCreateOptionsMenu(menu, inflater)

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.notification) {
            startActivity(Intent(activity, NotificationActivity::class.java))
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onDestroy() {
        super.onDestroy()
        pager.removeAllViews()
        pager.invalidate()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        val metrics = resources.displayMetrics
        val ratio = metrics.heightPixels.toFloat() / metrics.widthPixels.toFloat()
        Log.e("Ratio: ", "" + ratio)
        mAdapter = FragmentAdapter(childFragmentManager, ratio)
    }

    override fun getSellerPaggerData(modelHomeDataSubscriptions: ModelNotificationCustomer) {
        val auth = Constant.getPrefs(activity!!).getString(Constant.auth_code, "")
        viewModel.getDeclinedApi(auth)
        val intent = Intent(activity, ActivityMerchantDetailPage::class.java)
            .putExtra("notificationData", modelHomeDataSubscriptions)
        startActivity(intent)
    }

    override fun getIncreaseLimit(model: ModelNotificationCustomer) {
        val auth = Constant.getPrefs(activity!!).getString(Constant.auth_code, "")
        viewModel.getDeclinedApi(auth)
        val intent = Intent(activity, ActivitySubscriptionSeller::class.java)
            .putExtra("campaignId", model.id)
        startActivity(intent)
    }

    override fun getSellerRelink(model: ModelNotificationCustomer) {
        val auth = Constant.getPrefs(activity!!).getString(Constant.auth_code, "")
        isClicked = true
        viewModel.getDeclinedApi(auth)
        modelNotifications = model
        viewModel1.getBusinessLinkInfo(
            "", model.business_id, "", model.reqtype
        )

    }
}

