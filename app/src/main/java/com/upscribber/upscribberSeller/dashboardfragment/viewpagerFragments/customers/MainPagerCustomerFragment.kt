package com.upscribber.upscribberSeller.dashboardfragment.viewpagerFragments.customers


import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.upscribber.upscribberSeller.dashboardfragment.dashboard.EventHideTitle
import com.squareup.otto.Subscribe
import com.upscribber.commonClasses.BusProvider
import com.upscribber.R
import com.upscribber.databinding.FragmentMainPagerCustomerBinding
import com.upscribber.upscribberSeller.dashboardfragment.viewpagerFragments.products.ProductListener
import com.upscribber.upscribberSeller.reportingDetail.ReportingActivity

class MainPagerCustomerFragment : Fragment() , ProductListener {

    lateinit var mBinding: FragmentMainPagerCustomerBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_main_pager_customer, container, false)
        return mBinding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mBinding.listener = this
    }

    override fun onViewDetail() {
        startActivity(Intent(activity, ReportingActivity::class.java).putExtra("value", 4))
    }


    @Subscribe
    public fun hideTitle(event: EventHideTitle) {
        if (event.position == 3)
            mBinding.title.visibility = View.GONE
        else
            mBinding.title.visibility = View.VISIBLE
    }

    override fun onResume() {
        BusProvider.getInstance().register(this)
        super.onResume()
    }

    override fun onPause() {
        BusProvider.getInstance().unregister(this)
        super.onPause()
    }
}
