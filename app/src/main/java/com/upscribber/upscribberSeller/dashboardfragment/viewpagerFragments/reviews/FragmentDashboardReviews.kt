package com.upscribber.upscribberSeller.dashboardfragment.viewpagerFragments.reviews

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.upscribber.upscribberSeller.dashboardfragment.dashboard.EventHideTitle
import com.upscribber.upscribberSeller.subsriptionSeller.ratingStats.RatingStatsActivity
import com.squareup.otto.Subscribe
import com.upscribber.commonClasses.BusProvider
import com.upscribber.R
import com.upscribber.databinding.FragmentDashboardReviewsBinding

class FragmentDashboardReviews : Fragment() {

    lateinit var title: TextView
    lateinit var mBinding: FragmentDashboardReviewsBinding
    lateinit var mViewModel: ReviewViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container:
        ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_dashboard_reviews, container, false)
        mViewModel = ViewModelProviders.of(this).get(ReviewViewModel::class.java)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val layoutManager = LinearLayoutManager(activity)
        layoutManager.orientation = LinearLayout.HORIZONTAL
        mBinding.recyclerReviews.layoutManager = layoutManager

        mViewModel.getReviewsList().observe(this, Observer {

            mBinding.recyclerReviews.adapter = ReviewAdapterSeller(activity!!, it, 1)

        })
        mBinding.viewALL.setOnClickListener {
            startActivity(Intent(activity, RatingStatsActivity::class.java))
        }

    }

    @Subscribe
    public fun hideTitle(event: EventHideTitle) {
        if (event.position == 2)
            mBinding.title.visibility = View.GONE
        else
            mBinding.title.visibility = View.VISIBLE
    }

    override fun onResume() {
        BusProvider.getInstance().register(this)
        super.onResume()
    }

    override fun onPause() {
        BusProvider.getInstance().unregister(this)
        super.onPause()
    }

}