package com.upscribber.upscribberSeller.customerBottom.profile.activity

import android.os.Parcel
import android.os.Parcelable
import com.upscribber.home.ModelHomeLocation

class ModelActivityData() : Parcelable{

    var locations : ArrayList<ModelHomeLocation> = ArrayList()
    var rtype: String = ""
    var stripe_fee: String = ""
    var merchant_amount: String = ""
    var admin_amount: String = ""
    var typeLinking: String = ""
    var merchant_user_id: String = ""
    var campaign_owner: String = ""
    var subscription_id: String = ""
    var redeem_count: String = ""
    var cancel_reason: String = ""
    var redeem_id: String = ""
    var logo: String = ""
    var business_phone: String = ""
    var order_status: String = ""
    var is_refunded: String = ""
    var description: String = ""
    var card_id: String = ""
    var transaction_type: String = ""
    var transaction_status: String = ""
    var total_amount: String = ""
    var order_id: String = ""
    var transaction_id: String = ""
    var campaign_id: String = ""
    var discount_price: String = ""
    var free_trial: String = ""
    var introductory_price: String = ""
    var frequency_type: String = ""
    var introductory_days: String = ""
    var unit: String = ""
    var url: String = ""
    var status1: String = ""
    var frequency_value: String = ""
    var redeemtion_cycle_qty: String = ""
    var free_trial_qty: String = ""
    var id: String = ""
    var assign_to: String = ""
    var price: String = ""
    var campaign_name: String = ""
    var customer_id: String = ""
    var total: String = ""
    var created_at: String = ""
    var merchant_name: String = ""
    var feature_image: String = ""
    var business_logo: String = ""
    var business_id: String = ""
    var merchant_id: String = ""
    var updated_at: String = ""
    var customer_name: String = ""
    var business_name: String = ""
    var team_member: String = ""
    var location_id: String = ""
    var contact_no: String = ""
    var profile_image: String = ""
    var remaining_visits: String = ""

    constructor(parcel: Parcel) : this() {
        rtype = parcel.readString()
        stripe_fee = parcel.readString()
        merchant_amount = parcel.readString()
        admin_amount = parcel.readString()
        typeLinking = parcel.readString()
        merchant_user_id = parcel.readString()
        campaign_owner = parcel.readString()
        subscription_id = parcel.readString()
        redeem_count = parcel.readString()
        cancel_reason = parcel.readString()
        redeem_id = parcel.readString()
        logo = parcel.readString()
        business_phone = parcel.readString()
        order_status = parcel.readString()
        is_refunded = parcel.readString()
        description = parcel.readString()
        card_id = parcel.readString()
        transaction_type = parcel.readString()
        transaction_status = parcel.readString()
        total_amount = parcel.readString()
        order_id = parcel.readString()
        transaction_id = parcel.readString()
        campaign_id = parcel.readString()
        discount_price = parcel.readString()
        free_trial = parcel.readString()
        introductory_price = parcel.readString()
        frequency_type = parcel.readString()
        introductory_days = parcel.readString()
        unit = parcel.readString()
        url = parcel.readString()
        status1 = parcel.readString()
        frequency_value = parcel.readString()
        redeemtion_cycle_qty = parcel.readString()
        free_trial_qty = parcel.readString()
        id = parcel.readString()
        assign_to = parcel.readString()
        price = parcel.readString()
        campaign_name = parcel.readString()
        customer_id = parcel.readString()
        total = parcel.readString()
        created_at = parcel.readString()
        merchant_name = parcel.readString()
        feature_image = parcel.readString()
        business_logo = parcel.readString()
        business_id = parcel.readString()
        merchant_id = parcel.readString()
        updated_at = parcel.readString()
        customer_name = parcel.readString()
        business_name = parcel.readString()
        team_member = parcel.readString()
        location_id = parcel.readString()
        contact_no = parcel.readString()
        profile_image = parcel.readString()
        remaining_visits = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(rtype)
        parcel.writeString(stripe_fee)
        parcel.writeString(merchant_amount)
        parcel.writeString(admin_amount)
        parcel.writeString(typeLinking)
        parcel.writeString(merchant_user_id)
        parcel.writeString(campaign_owner)
        parcel.writeString(subscription_id)
        parcel.writeString(redeem_count)
        parcel.writeString(cancel_reason)
        parcel.writeString(redeem_id)
        parcel.writeString(logo)
        parcel.writeString(business_phone)
        parcel.writeString(order_status)
        parcel.writeString(is_refunded)
        parcel.writeString(description)
        parcel.writeString(card_id)
        parcel.writeString(transaction_type)
        parcel.writeString(transaction_status)
        parcel.writeString(total_amount)
        parcel.writeString(order_id)
        parcel.writeString(transaction_id)
        parcel.writeString(campaign_id)
        parcel.writeString(discount_price)
        parcel.writeString(free_trial)
        parcel.writeString(introductory_price)
        parcel.writeString(frequency_type)
        parcel.writeString(introductory_days)
        parcel.writeString(unit)
        parcel.writeString(url)
        parcel.writeString(status1)
        parcel.writeString(frequency_value)
        parcel.writeString(redeemtion_cycle_qty)
        parcel.writeString(free_trial_qty)
        parcel.writeString(id)
        parcel.writeString(assign_to)
        parcel.writeString(price)
        parcel.writeString(campaign_name)
        parcel.writeString(customer_id)
        parcel.writeString(total)
        parcel.writeString(created_at)
        parcel.writeString(merchant_name)
        parcel.writeString(feature_image)
        parcel.writeString(business_logo)
        parcel.writeString(business_id)
        parcel.writeString(merchant_id)
        parcel.writeString(updated_at)
        parcel.writeString(customer_name)
        parcel.writeString(business_name)
        parcel.writeString(team_member)
        parcel.writeString(location_id)
        parcel.writeString(contact_no)
        parcel.writeString(profile_image)
        parcel.writeString(remaining_visits)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ModelActivityData> {
        override fun createFromParcel(parcel: Parcel): ModelActivityData {
            return ModelActivityData(parcel)
        }

        override fun newArray(size: Int): Array<ModelActivityData?> {
            return arrayOfNulls(size)
        }
    }


}
