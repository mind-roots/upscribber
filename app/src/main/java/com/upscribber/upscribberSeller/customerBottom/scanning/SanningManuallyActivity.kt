package com.upscribber.upscribberSeller.customerBottom.scanning

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.EditText
import androidx.appcompat.widget.Toolbar
import com.upscribber.R
import android.widget.Toast
import android.os.Handler
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.upscribber.commonClasses.Constant
import com.upscribber.home.ModelHomeLocation
import com.upscribber.subsciptions.manageSubscriptions.redeem.RedeemScreenNew
import kotlinx.android.synthetic.main.activity_sanning_manually.*
import me.dm7.barcodescanner.zxing.ZXingScannerView


class SanningManuallyActivity : AppCompatActivity(), ZXingScannerView.ResultHandler {

    //    lateinit var toolbar: Toolbar
    lateinit var code: EditText
    lateinit var mViewModel: ScanningViewModel
    private lateinit var mScannerView: ZXingScannerView
    lateinit var arrayHomeLocation: ArrayList<ModelHomeLocation>


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sanning_manually)
        mViewModel = ViewModelProviders.of(this)[ScanningViewModel::class.java]
//        setToolbar()
        checkPermission()
        requestPermission()
        clickListeners()
        mScannerView = findViewById(R.id.mScannerView)
        observerInit()
    }

    private fun clickListeners() {
        redeemManual.setOnClickListener {
            startActivity(Intent(this, RedeemScreenNew::class.java))
        }

        imageView66.setOnClickListener {
            finish()
        }

    }

    private fun observerInit() {
        mViewModel.getmDataStatus().observe(this, Observer {
            if (it.msg == "Invalid auth code") {
                Constant.commonAlert(this)
            } else {
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
                finish()
            }
        })

        mViewModel.getmScanBarCode().observe(this, Observer {
            if (it.status == "true") {
                arrayHomeLocation = it.locations
                startActivity(
                    Intent(this, SuccessfullyRedemptionActivity::class.java).putExtra(
                        "modelRedeem",
                        it
                    )
                        .putExtra("arrayHomeLocation", arrayHomeLocation)
                )
                finish()
            }
        })

    }

    private fun checkPermission(): Boolean {
        return ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.CAMERA
        ) == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(Manifest.permission.CAMERA),
            1
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_DENIED) {
            finish()
        }
    }


    public override fun onResume() {
        super.onResume()
        mScannerView.setResultHandler(this)
        mScannerView.startCamera()
        mScannerView.setAspectTolerance(0.5f)
        mScannerView.setAutoFocus(true)
    }

    public override fun onPause() {
        super.onPause()
        mScannerView.stopCamera()
    }

    override fun handleResult(p0: com.google.zxing.Result?) {
        try {
            val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
            val currentString = p0.toString()
            val separated = currentString.split("_".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            separated[0]
            separated[1]
            separated[2]

            mViewModel.getReddeemCode(auth, separated[0], separated[1], separated[2], "1")
        } catch (e: Exception) {
            e.printStackTrace()
            mScannerView.setResultHandler(this)
            mScannerView.startCamera()
            mScannerView.setAspectTolerance(0.5f)
            mScannerView.setAutoFocus(true)
        }

    }

}
