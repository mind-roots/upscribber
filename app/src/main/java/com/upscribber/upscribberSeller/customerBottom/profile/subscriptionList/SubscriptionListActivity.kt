package com.upscribber.upscribberSeller.customerBottom.profile.subscriptionList

import android.content.Context
import android.content.Intent
import android.graphics.Outline
import android.os.Build
import android.os.Bundle
import android.view.*
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.upscribber.commonClasses.Constant
import com.upscribber.R
import com.upscribber.commonClasses.Selectionn
import com.upscribber.databinding.ActivitySubscriptionListBinding
import com.upscribber.commonClasses.RoundedBottomSheetDialogFragment
import com.upscribber.profile.ModelGetProfile
import com.upscribber.upscribberSeller.customerBottom.customerList.ModelCustomerListt
import com.upscribber.upscribberSeller.customerBottom.scanning.ScanningViewModel
import com.upscribber.upscribberSeller.customerBottom.scanning.SuccessfullyRedemptionActivity
import com.upscribber.upscribberSeller.customerBottom.scanning.customerlistselection.ModelScanCustomers
import com.upscribber.upscribberSeller.navigationCustomer.InviteSubscription
import com.upscribber.upscribberSeller.subsriptionSeller.extras.AdapterExtra
import com.upscribber.upscribberSeller.subsriptionSeller.subscriptionMain.SubscriptionSellerViewModel
import kotlinx.android.synthetic.main.activity_subscription_list.*

class SubscriptionListActivity : AppCompatActivity(), SubscriptionListAdapter.SelectionCustomerr,
    Selectionn {

    private lateinit var backgroundChange: ArrayList<SubscriptionListModel>
    lateinit var toolbar: Toolbar
    lateinit var toolbarTitle: TextView
    private lateinit var mAdapterExtrass: AdapterExtra
    private lateinit var mbinding: ActivitySubscriptionListBinding
    //    private lateinit var viewModel: SubscriptionListViewModel
    private lateinit var mAdapter: SubscriptionListAdapter
    lateinit var mViewModel: ScanningViewModel
    var name = ""
    lateinit var mViewModelCreate: SubscriptionSellerViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mbinding = DataBindingUtil.setContentView(this, R.layout.activity_subscription_list)
        mbinding.lifecycleOwner = this
        mViewModelCreate = ViewModelProviders.of(this)[SubscriptionSellerViewModel::class.java]
        mViewModel = ViewModelProviders.of(this)[ScanningViewModel::class.java]
        initz()
        setToolbar()
        setAdapter()
        if (intent.getStringExtra("subscription") == "invite" || intent.getStringExtra("subscription") == "inviteList") {
            apiImplimentation()
            observerInit()
        }

        if (intent.hasExtra("addOption")) {
            name = intent.getIntExtra("addOption", 0).toString()
        }




        clickk()
    }

    private fun observerSubscriptionList() {
        mViewModelCreate.getSubscrpList().observe(this, Observer {
            progressBar.visibility = View.GONE
            backgroundChange = it
            if (it.size > 0) {
                noData.visibility = View.GONE
            } else {
                noData.visibility = View.VISIBLE
            }
            mAdapter.update(it)

        })

    }


    private fun apiImplimentationSubscriptionList() {
        val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
        if (intent.hasExtra("modelCustomerProfile")) {
            val dataCustomer =
                intent.getParcelableExtra("modelCustomerProfile") as ModelCustomerListt
            mViewModelCreate.getSubscriptionList(auth, "0", "1", dataCustomer.customer_id)
        } else if (intent.hasExtra("modelList")) {
            val modelCustomerList = intent.getParcelableExtra<ModelScanCustomers>("modelList")
            mViewModelCreate.getSubscriptionList(auth, "0", "1", modelCustomerList.customer_id)
        } else if (intent.hasExtra("customerId")) {
            val customerId = intent.getStringExtra("customerId")
            mViewModelCreate.getSubscriptionList(auth, "0", "1", customerId)
        }else if(intent.hasExtra("modelCustomerProfiles")){
            val modelCustomerList = intent.getParcelableExtra<ModelScanCustomers>("modelCustomerProfiles")
            mViewModelCreate.getSubscriptionList(auth, "0", "1", modelCustomerList.customer_id)
        }
        progressBar.visibility = View.VISIBLE

    }

    private fun observerInit() {
        mViewModelCreate.getmAllCampaignsData().observe(this, Observer {
            if (it.size > 0) {
                progressBar.visibility = View.GONE
                var campaignsId = ""
                for (model in it) {
                    if (model.is_public == "0" && model.status1 == "1") {
                        campaignsId = if (campaignsId.isEmpty()) {
                            model.campaign_id
                        } else {
                            "$campaignsId, ${model.campaign_id}"
                        }
                    }
                }

                mViewModelCreate.getCampaignSubscriptionList(campaignsId)

            }

        })


        mViewModelCreate.getStatus().observe(this, Observer {
            progressBar.visibility = View.GONE
            if (it.msg == "Invalid auth code") {
                Constant.commonAlert(this)
            } else if (it.msg == "campaign_id is missing or invalid") {
                noData.visibility = View.VISIBLE
            } else {
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
            }
        })


        mViewModelCreate.getSubscrpList().observe(this, Observer {
            if (it.size > 0) {
                noData.visibility = View.GONE
            } else {
                noData.visibility = View.VISIBLE
            }
            progressBar.visibility = View.GONE
            mAdapter.update(it)

        })

    }

    private fun apiImplimentation() {
        val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
        mViewModelCreate.getAllSubscriptionsData(auth, "")
        progressBar.visibility = View.VISIBLE

    }

    private fun setAdapter() {
        mbinding.subscriptionList.layoutManager = LinearLayoutManager(this)
        mAdapter = SubscriptionListAdapter(this, intent.getStringExtra("subscription"))
        mbinding.subscriptionList.adapter = mAdapter

    }

    override fun fixIntent(position: Int, model: SubscriptionListModel) {
        if (intent.getStringExtra("subscription") == "inviteList") {
            startActivity(
                Intent(this, InviteSubscription::class.java).putExtra(
                    "model1",
                    model
                ).putExtra(
                    "modelList", intent.getParcelableExtra<ModelScanCustomers>("modelList")
                )
            )

        } else {
            if (intent.hasExtra("modelCustomerProfile")) {
                val intent1 = intent.getParcelableExtra<ModelCustomerListt>("modelCustomerProfile")
                startActivity(
                    Intent(this, InviteSubscription::class.java).putExtra(
                        "fromCustomers",
                        "fromProfile"
                    ).putExtra(
                        "model",
                        model
                    )
                        .putExtra(
                            "profileData",
                            intent.getParcelableExtra<ModelGetProfile>("profileData")
                        )
                        .putExtra(
                            "modelCustomerProfile", intent1
                        )
                )
            } else {
                val intent2 = intent.getParcelableExtra<ModelScanCustomers>("modelList")
                startActivity(
                    Intent(this, InviteSubscription::class.java).putExtra(
                        "fromCustomers",
                        "fromProfile"
                    ).putExtra(
                        "model",
                        model
                    )
                        .putExtra(
                            "profileData",
                            intent.getParcelableExtra<ModelGetProfile>("profileData")
                        )
                        .putExtra(
                            "modelList", intent2
                        )
                )
            }

        }

    }


    private fun initz() {
        toolbar = findViewById(R.id.toolbar)
        toolbarTitle = findViewById(R.id.title1)

    }

    private fun setToolbar() {
        setSupportActionBar(toolbar)
        title = ""
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)
        if (intent.getStringExtra("subscription") == "list") {
            when {
                intent.hasExtra("modelCustomerProfiles") -> {
                    val modelCustomerProfile = intent.getParcelableExtra<ModelScanCustomers>("modelCustomerProfiles")
                    toolbarTitle.text = modelCustomerProfile.name + "'s Subscription"
                }
                intent.hasExtra("modelCustomerProfile") -> {
                    val dataCustomer = intent.getParcelableExtra("modelCustomerProfile") as ModelCustomerListt
                    toolbarTitle.text = dataCustomer.customer_name + "'s Subscription"
                }
                else -> {
                    toolbarTitle.text = "Subscription List"
                }
            }

        } else {
            toolbarTitle.text = "Subscription List"
        }

    }

    private fun clickk() {

        mbinding.tvProducts.setOnClickListener {
            mbinding.recycleraddOn.visibility = View.GONE
            mbinding.subscriptionList.visibility = View.VISIBLE
            mbinding.tvProducts.setBackgroundResource(R.drawable.bg_seller_blue)
            mbinding.tvExtras.setBackgroundResource(R.drawable.bg_switch_grey)
            mbinding.tvExtras.setTextColor(resources.getColor(R.color.payments))
            mbinding.tvProducts.setTextColor(resources.getColor(R.color.colorWhite))

        }


    }

    class MenuFragmentRedeem3(
        var subscriptionListModel: SubscriptionListModel,
        var mViewModel: ScanningViewModel
    ) :
        RoundedBottomSheetDialogFragment() {

        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val view = inflater.inflate(R.layout.redeem_sheet_quantity, container, false)

            val invite = view.findViewById<TextView>(R.id.invite)
            val etQuantity = view.findViewById<TextView>(R.id.etQuantity)
            val imageViewTop = view.findViewById<ImageView>(R.id.imageViewTop)
            var quantity = ""

            val isSeller = Constant.getSharedPrefs(context!!).getBoolean(Constant.IS_SELLER, false)
            if (isSeller) {
                invite.setBackgroundResource(R.drawable.bg_seller_button_blue)
                imageViewTop.setImageResource(R.mipmap.bg_popup)
            } else {
                invite.setBackgroundResource(R.drawable.favorite_delete_background)
                imageViewTop.setImageResource(R.mipmap.bg_popup_customer)
            }

            roundImage(imageViewTop)
            invite.setOnClickListener {
                val auth = Constant.getPrefs(activity!!).getString(Constant.auth_code, "")
                val customerId = activity!!.intent.getStringExtra("customerId")
                if (quantity == "") {
                    Toast.makeText(
                        activity,
                        "Please select quantity to continue",
                        Toast.LENGTH_SHORT
                    ).show()
                } else {

                    mViewModel.getReddeemCodeCustomers(
                        auth,
                        quantity,
                        subscriptionListModel.order_id,
                        "3",
                        customerId
                    )
                }
            }

            etQuantity.setOnClickListener {
                val choices = getList(subscriptionListModel)
                if (choices.size > 0) {
                    val charSequenceItems =
                        choices.toArray(arrayOfNulls<CharSequence>(choices.size))
                    val mBuilder = AlertDialog.Builder(activity!!)
                    mBuilder.setSingleChoiceItems(charSequenceItems, -1) { dialogInterface, i ->
                        etQuantity.text = choices[i]
                        quantity = choices[i]
                        dialogInterface.dismiss()
                    }
                    val mDialog = mBuilder.create()
                    mDialog.show()
                }else{
                    Toast.makeText(activity,"This Customer does not have enough quantity to redeem.Please Select another Customer.",Toast.LENGTH_SHORT).show()
                    dismiss()
                }
            }

            observerInit()

            return view
        }

        private fun observerInit() {
            mViewModel.getmDataStatus().observe(this, Observer {
                if (it.msg == "Invalid auth code") {
                    Constant.commonAlert(activity!!)
                } else {
                    Toast.makeText(activity, it.msg, Toast.LENGTH_SHORT).show()
                }
            })

            mViewModel.getmScanBarCode().observe(this, Observer {
                if (it.status == "true") {
                    startActivity(Intent(activity, SuccessfullyRedemptionActivity::class.java).putExtra("modelCustomerRedeem", it).putExtra("manual1", "bar"))
                    dismiss()
                    activity!!.finish()
                }
            })

        }

        private fun roundImage(imageViewTop: ImageView) {
            val curveRadius = 50F

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                imageViewTop.outlineProvider = object : ViewOutlineProvider() {

                    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                    override fun getOutline(view: View?, outline: Outline?) {
                        outline?.setRoundRect(
                            0,
                            0,
                            view!!.width,
                            view.height + curveRadius.toInt(),
                            curveRadius

                        )
                    }
                }

                imageViewTop.clipToOutline = true

            }

        }

        private fun getList(subscriptionListModel: SubscriptionListModel): ArrayList<String> {
            val arrayList: ArrayList<String> = ArrayList()
            if (subscriptionListModel.redeemtion_cycle_qty.toInt() > 100) {
                for (i in 0 until 500) {
                    arrayList.add((i + 1).toString())
                }
            } else {
                for (i in 0 until (subscriptionListModel.redeemtion_cycle_qty.toInt() - subscriptionListModel.redeem_count.toInt())) {

                    arrayList.add((i + 1).toString())
                }
            }

            return arrayList
        }

    }

    override fun SelectCustomer(position: Int) {
        val model1 = backgroundChange[position]
        if (model1.status) {
            model1.status = false
            mbinding.buttonRedeem.visibility = View.GONE
        } else {
            for (child in backgroundChange) {
                child.status = false
                mbinding.buttonRedeem.visibility = View.GONE
            }
            model1.status = true
            mbinding.buttonRedeem.visibility = View.VISIBLE
            mbinding.buttonRedeem.setOnClickListener {
                val fragment = MenuFragmentRedeem3(backgroundChange[position],mViewModel)
                fragment.show(supportFragmentManager, fragment.tag)
            }

        }
        backgroundChange[position] = model1

        mAdapter.notifyDataSetChanged()

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onSelectionBackgroundd(position: Int) {


    }


    override fun onResume() {
        super.onResume()
        if (intent.getStringExtra("subscription") == "list" || intent.getStringExtra("subscription") == "selection") {
            apiImplimentationSubscriptionList()
            observerSubscriptionList()
        }

        val sharedPreferences = Constant.getPrefs(this)
        if (!sharedPreferences.getString("list", "").isEmpty()) {
            finish()
        }


//         val sharedPreferences1 = getSharedPreferences("reedeem", Context.MODE_PRIVATE)
//        if (!sharedPreferences1.getString("reedd", "").isEmpty()) {
//            finish()
//        }

    }
}
