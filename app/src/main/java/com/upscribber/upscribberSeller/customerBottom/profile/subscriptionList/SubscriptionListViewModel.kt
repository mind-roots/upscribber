package com.upscribber.upscribberSeller.customerBottom.profile.subscriptionList

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.upscribber.upscribberSeller.subsriptionSeller.extras.ModelExtra

class SubscriptionListViewModel(application: Application) : AndroidViewModel(application) {

    var subscriptionListRepository : SubscriptionListRepository =
        SubscriptionListRepository(application)

}