package com.upscribber.upscribberSeller.customerBottom

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.upscribber.upscribberSeller.customerBottom.customerList.ModelCustomerListt
import com.upscribber.upscribberSeller.customerBottom.recentRedemptions.ModelRecentRedemption
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.commonClasses.ModelStatusMsg
import com.upscribber.commonClasses.WebServicesCustomers
import com.upscribber.profile.ModelGetProfile
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit


class customerBottomRepository(application: Application) {

    //    val  = MutableLiveData<List<ModelRecentRedemption>>()
    val mDataMain = MutableLiveData<ModelMainCustomers>()
    val mDataFailure = MutableLiveData<ModelStatusMsg>()
    val mDataProfile = MutableLiveData<ModelGetProfile>()


    fun getCustomersData(auth: String, type: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.getRedeemHistory(auth, type)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val data = json.optJSONObject("data")
                        val graph_info = data.optJSONObject("graph_info")
                        val customers = data.optJSONArray("customers")
                        val redhistory = data.optJSONArray("redhistory")
                        val campaign_status = data.optString("campaign_status")
                        val model = ModelMainCustomers()
                        val arrayListCustomers = ArrayList<ModelCustomerListt>()
                        val arrayListRedeemCustomers = ArrayList<ModelRecentRedemption>()
                        val modelStatus = ModelStatusMsg()

                        if (status == "true") {
                            model.graph_info = graph_info.optString("graph_info")
                            model.customers = graph_info.optString("customers")
                            model.recurring_customer = graph_info.optString("recurring_customer")
                            model.new_customer = graph_info.optString("new_customer")
                            model.cancelled_customer = graph_info.optString("cancelled_customer")

                            for (i in 0 until customers.length()) {
                                val dataCustomers = customers.optJSONObject(i)
                                val modelCustomers = ModelCustomerListt()
                                modelCustomers.customer_id = dataCustomers.optString("customer_id")
                                modelCustomers.customer_name = dataCustomers.optString("name")
                                modelCustomers.contact_no = dataCustomers.optString("contact_no")
                                modelCustomers.profile_image = dataCustomers.optString("profile_image")
                                modelCustomers.remaining_visits = dataCustomers.optString("remaining_visits")
                                arrayListCustomers.add(modelCustomers)
                            }
                            model.arrayCustomers = arrayListCustomers

                            for (i in 0 until redhistory.length()) {
                                val dataRedeemCustomers = redhistory.optJSONObject(i)
                                val modelRedeemCustomers = ModelRecentRedemption()
                                modelRedeemCustomers.user_id = dataRedeemCustomers.optString("user_id")
                                modelRedeemCustomers.redeem_id = dataRedeemCustomers.optString("redeem_id")
                                modelRedeemCustomers.redeem_date = dataRedeemCustomers.optString("redeem_date")
                                modelRedeemCustomers.customer_name = dataRedeemCustomers.optString("customer_name")
                                modelRedeemCustomers.profile_image = dataRedeemCustomers.optString("profile_image")
                                modelRedeemCustomers.contact_no = dataRedeemCustomers.optString("contact_no")
                                modelRedeemCustomers.team_member = dataRedeemCustomers.optString("team_member")
                                modelRedeemCustomers.campaign_name = dataRedeemCustomers.optString("campaign_name")
                                modelRedeemCustomers.price = dataRedeemCustomers.optString("price")
                                modelRedeemCustomers.created_at = dataRedeemCustomers.optString("created_at")
                                arrayListRedeemCustomers.add(modelRedeemCustomers)

                            }
                            model.arrayListRedeemCustomers = arrayListRedeemCustomers
                            model.status = status
                            model.msg = msg
                            model.campaign_status = campaign_status

                            mDataMain.value = model
                        } else {
                            modelStatus.status = "false"
                            modelStatus.msg = "Network Error!"
                            mDataFailure.value = modelStatus
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus
            }
        })
    }

    fun getmDataCustomers(): LiveData<ModelMainCustomers> {
        return mDataMain
    }

    fun getmDataFailure(): LiveData<ModelStatusMsg> {
        return mDataFailure
    }

    fun getViewCustomerProfile(auth: String, type: String, viewType: String, viewId: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.getProfile(auth, type, viewType, viewId)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        val data = json.optJSONObject("data")
                        val modelGetProfile = ModelGetProfile()
                        if (status == "true") {
                                modelGetProfile.created_campaign = data.optString("created_campaign")
                                modelGetProfile.revenue = data.optString("revenue")
                                modelGetProfile.associated_date = data.optString("associated_date")
                                modelGetProfile.profile_image = data.optString("profile_image")
                                modelGetProfile.name = data.optString("name")
                                modelGetProfile.address = data.optString("address")
                                modelGetProfile.speciality = data.optString("speciality")
                                modelGetProfile.active_campaign = data.optString("active_campaign")
                                modelGetProfile.subscriber_since = data.optString("subscriber_since")
                                modelGetProfile.street = data.optString("street")
                                modelGetProfile.state = data.optString("state")
                                modelGetProfile.city = data.optString("city")
                                modelGetProfile.zip_code = data.optString("zip_code")
                                modelGetProfile.email = data.optString("email")
                                modelGetProfile.associated_campaign = data.optString("associated_campaign")
                                modelGetProfile.gender = data.optString("gender")
                                modelGetProfile.contact_no = data.optString("contact_no")
                            mDataProfile.value = modelGetProfile
                        } else {
                            modelStatus.msg = msg
                            mDataFailure.value = modelStatus
                        }

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus

            }
        })
    }



    fun  getrmDataProfile() : LiveData<ModelGetProfile>{
        return mDataProfile
    }
}