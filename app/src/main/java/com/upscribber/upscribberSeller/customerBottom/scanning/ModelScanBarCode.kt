package com.upscribber.upscribberSeller.customerBottom.scanning

import android.os.Parcel
import android.os.Parcelable
import com.upscribber.home.ModelHomeLocation




class ModelScanBarCode() : Parcelable{

    var unit: String = ""
    var redeemtion_cycle_qty: String = ""
    var price: String = ""
    var description: String = ""
    var business_id: String = ""
    var business_phone: String = ""
    var logo: String = ""
    var merchant_id: String = ""
    var redeem_id: String = ""
    var frequency_value: String = ""
    var frequency_type: String = ""
    var order_id: String = ""
    var status: String = ""
    var msg: String = ""
    var assign_to: String = ""
    var campaign_name: String = ""
    var customer_id: String = ""
    var total: String = ""
    var created_at: String = ""
    var customer_name: String = ""
    var business_name: String = ""
    var team_member: String = ""
    var location_id: String = ""
    var contact_no: String = ""
    var profile_image: String = ""
    var remaining_visits: String = ""
    var locations : ArrayList<ModelHomeLocation> = ArrayList()

    constructor(parcel: Parcel) : this() {
        unit = parcel.readString()
        redeemtion_cycle_qty = parcel.readString()
        price = parcel.readString()
        description = parcel.readString()
        business_id = parcel.readString()
        business_phone = parcel.readString()
        logo = parcel.readString()
        merchant_id = parcel.readString()
        redeem_id = parcel.readString()
        frequency_value = parcel.readString()
        frequency_type = parcel.readString()
        order_id = parcel.readString()
        status = parcel.readString()
        msg = parcel.readString()
        assign_to = parcel.readString()
        campaign_name = parcel.readString()
        customer_id = parcel.readString()
        total = parcel.readString()
        created_at = parcel.readString()
        customer_name = parcel.readString()
        business_name = parcel.readString()
        team_member = parcel.readString()
        location_id = parcel.readString()
        contact_no = parcel.readString()
        profile_image = parcel.readString()
        remaining_visits = parcel.readString()
        locations = parcel.createTypedArrayList(ModelHomeLocation)

    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(unit)
        parcel.writeString(redeemtion_cycle_qty)
        parcel.writeString(price)
        parcel.writeString(description)
        parcel.writeString(business_id)
        parcel.writeString(business_phone)
        parcel.writeString(logo)
        parcel.writeString(merchant_id)
        parcel.writeString(redeem_id)
        parcel.writeString(frequency_value)
        parcel.writeString(frequency_type)
        parcel.writeString(order_id)
        parcel.writeString(status)
        parcel.writeString(msg)
        parcel.writeString(assign_to)
        parcel.writeString(campaign_name)
        parcel.writeString(customer_id)
        parcel.writeString(total)
        parcel.writeString(created_at)
        parcel.writeString(customer_name)
        parcel.writeString(business_name)
        parcel.writeString(team_member)
        parcel.writeString(location_id)
        parcel.writeString(contact_no)
        parcel.writeString(profile_image)
        parcel.writeString(remaining_visits)
        parcel.writeTypedList(locations)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ModelScanBarCode> {
        override fun createFromParcel(parcel: Parcel): ModelScanBarCode {
            return ModelScanBarCode(parcel)
        }

        override fun newArray(size: Int): Array<ModelScanBarCode?> {
            return arrayOfNulls(size)
        }
    }


}
