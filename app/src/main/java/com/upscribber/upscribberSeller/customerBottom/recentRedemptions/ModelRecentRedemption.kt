package com.upscribber.upscribberSeller.customerBottom.recentRedemptions

import android.os.Parcel
import android.os.Parcelable
import com.upscribber.commonClasses.Constant


class ModelRecentRedemption() : Parcelable {

    var created_at: String = ""
    var price: String = ""
    var campaign_name: String = ""
    var team_member: String = ""
    var redeem_id: String = ""
    var user_id: String = ""
    var redeem_date: String = ""
    var customer_name: String = ""
    var profile_image: String = ""
    var contact_no: String = ""

    constructor(parcel: Parcel) : this() {
        created_at = parcel.readString()
        price = parcel.readString()
        campaign_name = parcel.readString()
        team_member = parcel.readString()
        redeem_id = parcel.readString()
        user_id = parcel.readString()
        redeem_date = parcel.readString()
        customer_name = parcel.readString()
        profile_image = parcel.readString()
        contact_no = parcel.readString()
    }


    fun getRedeemDate(): String {
        val date = Constant.getDateMonthYear(redeem_date)
        return date
    }

    fun getRedeemTime(): String {
        val time = Constant.getTime(redeem_date)
        return time
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(created_at)
        parcel.writeString(price)
        parcel.writeString(campaign_name)
        parcel.writeString(team_member)
        parcel.writeString(redeem_id)
        parcel.writeString(user_id)
        parcel.writeString(redeem_date)
        parcel.writeString(customer_name)
        parcel.writeString(profile_image)
        parcel.writeString(contact_no)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ModelRecentRedemption> {
        override fun createFromParcel(parcel: Parcel): ModelRecentRedemption {
            return ModelRecentRedemption(parcel)
        }

        override fun newArray(size: Int): Array<ModelRecentRedemption?> {
            return arrayOfNulls(size)
        }
    }


}
