package com.upscribber.upscribberSeller.customerBottom

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.upscribber.commonClasses.ModelStatusMsg
import com.upscribber.profile.ModelGetProfile
import com.upscribber.upscribberSeller.customerBottom.customerList.ModelCustomerListt
import com.upscribber.upscribberSeller.customerBottom.recentRedemptions.ModelRecentRedemption

class customerBottomViewModel (application: Application) : AndroidViewModel(application){

    var customerBottomRepository : customerBottomRepository = customerBottomRepository(application)

    fun getCustomerData(auth: String, type: String) {
        customerBottomRepository.getCustomersData(auth,type)

    }

    fun getmDataCustomers() : LiveData<ModelMainCustomers>{
       return customerBottomRepository.getmDataCustomers()
    }

     fun getmDataFailure() : LiveData<ModelStatusMsg>{
       return customerBottomRepository.getmDataFailure()
    }


     fun getrmDataProfile() : LiveData<ModelGetProfile>{
       return customerBottomRepository.getrmDataProfile()
    }




    fun getViewCustomerProfile(auth: String, type : String, viewType: String, viewId: String) {
        customerBottomRepository.getViewCustomerProfile(auth,type,viewType,viewId)
    }


}