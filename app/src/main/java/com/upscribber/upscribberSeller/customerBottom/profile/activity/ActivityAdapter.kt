package com.upscribber.upscribberSeller.customerBottom.profile.activity

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import androidx.recyclerview.widget.RecyclerView
import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.View
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import kotlinx.android.synthetic.main.activity_recycler_list.view.*
import kotlinx.android.synthetic.main.alert_already_linked.*
import kotlin.collections.ArrayList

class ActivityAdapter(
    var context: Context,
    var contextt: ProfileActivityListActivity
) :
    RecyclerView.Adapter<ActivityAdapter.MyViewHolder>() {

    private var items = ArrayList<ActivityModel>()
    var listener = contextt as RequestsLinking

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(context).inflate(R.layout.activity_recycler_list, parent, false)
        return MyViewHolder(v)
    }

    override fun getItemCount(): Int {
        return items.size
    }


    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = items[position]

        val campaignDate = Constant.getDateMonthYear(model.created_at)
        val campaignTime = Constant.getTime(model.created_at)
        if (model.model.campaign_name == "") {
            holder.itemView.txtCampaignName.text = model.msg
        } else {
            holder.itemView.txtCampaignName.text = model.model.campaign_name
        }


        holder.itemView.txtCampaignDate.text = campaignDate
        holder.itemView.textCampaignTime.text = campaignTime

        if (model.model.total_amount == "" && model.model.price == "") {
            holder.itemView.txtCampaignPrice.visibility = View.INVISIBLE
        }else if (model.model.total_amount == "") {
            holder.itemView.txtCampaignPrice.visibility = View.VISIBLE
            holder.itemView.txtCampaignPrice.text = "Amount: $" + model.model.price
        } else {
            holder.itemView.txtCampaignPrice.visibility = View.VISIBLE
            holder.itemView.txtCampaignPrice.text = "Amount: $" + model.model.total_amount
        }

        if (model.sub_type == "1") {
            holder.itemView.typeText.text = "Share"
            holder.itemView.typeText.setBackgroundResource(R.drawable.bg_sea_green_rounded)
        } else if (model.sub_type == "2") {
            holder.itemView.typeText.text = "Pause"
            holder.itemView.typeText.setBackgroundResource(R.drawable.bg_yellow_rounded)
        } else if (model.sub_type == "3") {
            holder.itemView.typeText.text = "Redeem"
            holder.itemView.typeText.setBackgroundResource(R.drawable.bg_pink_rounded)

        } else if (model.sub_type == "4") {
            holder.itemView.typeText.text = "Unshare"
            holder.itemView.typeText.setBackgroundResource(R.drawable.bg_pink_rounded)

        } else if (model.sub_type == "5") {
            holder.itemView.typeText.text = "Refund Request"
            holder.itemView.typeText.setBackgroundResource(R.drawable.bg_refund)

        } else if (model.sub_type == "6") {
            holder.itemView.typeText.text = "Unpause"
            holder.itemView.typeText.setBackgroundResource(R.drawable.bg_pause)

        } else if (model.sub_type == "8") {
            holder.itemView.typeText.text = "Cancel"
            holder.itemView.typeText.setBackgroundResource(R.drawable.bg_black_rounded)

        } else if (model.sub_type == "9") {
            holder.itemView.typeText.text = "Renew"
            holder.itemView.typeText.setBackgroundResource(R.drawable.bg_purple_rounded)
        } else if (model.sub_type == "10") {
            holder.itemView.typeText.text = "Refunded"
            holder.itemView.typeText.setBackgroundResource(R.drawable.bg_purple_rounded)
        } else if (model.sub_type == "11") {
            holder.itemView.typeText.text = "Invited"
            holder.itemView.typeText.setBackgroundResource(R.drawable.bg_purple_rounded)
        } else if (model.sub_type == "7") {
            holder.itemView.typeText.text = "Refund Denied"
            holder.itemView.typeText.setBackgroundResource(R.drawable.bg_pink_rounded)
        } else if (model.sub_type == "12") {
            holder.itemView.typeText.text = "Business request"
            holder.itemView.typeText.setBackgroundResource(R.drawable.bg_purple_rounded)
        }



        holder.itemView.setOnClickListener {
            if (model.action_taken == "0") {
                if (model.sub_type == "12") {
                    val homeProfile = Constant.getArrayListProfile(context, Constant.dataProfile)
                    if (homeProfile.type == "1" && homeProfile.steps == "6" || homeProfile.type == "2" && homeProfile.steps == "4" || homeProfile.type == "3") {
                        val mDialogView = LayoutInflater.from(context)
                            .inflate(R.layout.alert_already_linked, null)
                        val mBuilder = AlertDialog.Builder(context).setView(mDialogView)
                        val mAlertDialog = mBuilder.show()
                        mAlertDialog.setCancelable(false)
                        mDialogView.setBackgroundResource(R.drawable.bg_alert_new)
                        mAlertDialog.window.setBackgroundDrawableResource(R.drawable.bg_alert_transparent)
                        if (homeProfile.type == "1" && homeProfile.steps == "6") {
                            if (model.model.typeLinking == "1") {
                                mAlertDialog.textInfo.setText(R.string.merchant_info)
                            } else {
                                mAlertDialog.textInfo.setText(R.string.merchant_info1)
                            }
                        } else if (homeProfile.type == "2" && homeProfile.steps == "4") {
                            if (model.model.typeLinking == "1") {
                                mAlertDialog.textInfo.setText(R.string.contractor_info1)
                            } else {
                                mAlertDialog.textInfo.setText(R.string.contractor_info)
                            }
                        } else if (homeProfile.type == "3") {
                            mAlertDialog.textInfo.setText(R.string.non_contractor_info)
                        }

                        mAlertDialog.okBttn.setOnClickListener {
                            mAlertDialog.dismiss()
                        }
                    } else {
                        listener.linkingCustomers(model)
                    }

                } else {
                    context.startActivity(
                        Intent(
                            context,
                            ActiInfoPageActivity::class.java
                        ).putExtra("activityModel", model)
                    )
                }
            }
        }
    }


    fun update(items: ArrayList<ActivityModel>) {
        this.items = items
        notifyDataSetChanged()
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    interface RequestsLinking {
        fun linkingCustomers(model: ActivityModel)
    }


}