package com.upscribber.upscribberSeller.customerBottom.scanning.customerlistselection

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.upscribber.commonClasses.ModelStatusMsg
import com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.subscription.ModelSubscriptionCard

class ScanCustomerViewModel (application: Application) : AndroidViewModel(application)  {

    var scanCustomerRepository : ScanCustomerRepository =
        ScanCustomerRepository(application)


    fun getCustomersList(auth: String, search: String, view_id: String, view_type: String, type : String) {
        scanCustomerRepository.getCustomersList(auth,search,view_id,view_type,type)
    }

    fun getCustomersList() : LiveData<ArrayList<ModelScanCustomers>>{
        return scanCustomerRepository.getCustomersList()
    }

    fun getStatus() : LiveData<ModelStatusMsg>{
        return scanCustomerRepository.getmDataStatus()
    }




    fun getInviteCustomer(
        auth: String,
        customerId: String,
        subscriptionId: String,
        contactNumber: String
    ) {
        scanCustomerRepository.getInviteCustomers(auth,customerId,subscriptionId,contactNumber)

    }


    fun getmDataInvitedCustomers() : LiveData<ModelSubscriptionCard>{
        return scanCustomerRepository.getmDataInvitedCustomers()
    }



}