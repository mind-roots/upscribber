package com.upscribber.upscribberSeller.customerBottom.scanning.customerlistselection

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.upscribber.commonClasses.Constant
import com.upscribber.upscribberSeller.customerBottom.profile.subscriptionList.SubscriptionListActivity
import com.upscribber.R
import com.upscribber.databinding.ActivityScanCustomersBinding
import kotlinx.android.synthetic.main.activity_scan_customers.*


@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class ScanCustomersActivity : AppCompatActivity(), AdapterScanCustomers.SelectionCustomer,
    AdapterSelectedCustomers.SelectionSelectedCustomer {

    private lateinit var mAdapterSelected: AdapterSelectedCustomers
    var selectedModel: ModelScanCustomers? = null
    private var nonSelectedArrayList: ArrayList<ModelScanCustomers> = ArrayList()
    private var selectedArrayList: ArrayList<ModelScanCustomers> = ArrayList()
    private lateinit var mAdapter: AdapterScanCustomers
    private lateinit var mbinding: ActivityScanCustomersBinding
    private lateinit var viewModel: ScanCustomerViewModel
    var customerId = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mbinding = DataBindingUtil.setContentView(this, R.layout.activity_scan_customers)
        mbinding.lifecycleOwner = this
        viewModel = ViewModelProviders.of(this)[ScanCustomerViewModel::class.java]
        initz()


        apiImplimentation()
        observerInit()

        setToolbar()
        clickListener()
        setAdapter()
    }

    private fun initz() {
        if (intent.hasExtra("list")) {
            selectedArrayList = intent.getParcelableArrayListExtra("selectedlist")
            mbinding.redeem.text = "Done"
            mbinding.linearLayout15.visibility = View.VISIBLE
            mbinding.selectedCustomer.visibility = View.VISIBLE
            mbinding.linearSelected.visibility = View.VISIBLE
            mbinding.redeemNext.visibility = View.GONE
            mbinding.redeem.visibility = View.VISIBLE
            mbinding.redeem.isEnabled = true
        } else {
            mbinding.redeemNext.text = "Next"
            mbinding.redeem.visibility = View.GONE
            mbinding.linearLayout15.visibility = View.GONE
            mbinding.selectedCustomer.visibility = View.GONE
            mbinding.linearSelected.visibility = View.GONE
        }

    }

    @SuppressLint("SetTextI18n")
    private fun observerInit() {
        viewModel.getStatus().observe(this, Observer {
            mbinding.progressBar.visibility = View.GONE
            if (it.msg == "Invalid auth code") {
                Constant.commonAlert(this)
            } else {
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
            }

        })

        viewModel.getCustomersList().observe(this, Observer {
            mbinding.progressBar.visibility = View.GONE
            if (it.size > 0) {
                imgNoSubscription.visibility = View.GONE
                tvNoData.visibility = View.GONE
                tvNoDataDescription.visibility = View.GONE
                if (intent.hasExtra("list")) {
                    mbinding.redeemNext.visibility = View.GONE
                }else{
                    mbinding.redeemNext.visibility = View.VISIBLE
                }
            } else {
                imgNoSubscription.visibility = View.VISIBLE
                tvNoData.visibility = View.VISIBLE
                tvNoDataDescription.visibility = View.VISIBLE
            }
            if (intent.hasExtra("list")) {
                nonSelectedArrayList.clear()
                if (selectedArrayList.size > 0) {
                    nonSelectedArrayList = getArrayListNonSelected(it)
                } else {
                    nonSelectedArrayList.addAll(it)
                }
                mAdapter.update(nonSelectedArrayList)
                mAdapterSelected.update(selectedArrayList)


                if (selectedArrayList.size == it.size) {
                    mbinding.linearLayout15.visibility = View.GONE
                    mbinding.unSelectAll.visibility = View.VISIBLE
                } else {
                    mbinding.unSelectAll.visibility = View.GONE
                    mbinding.linearLayout15.visibility = View.VISIBLE
                }
                mbinding.count.text = "Selected Customers - " + selectedArrayList.size
            } else {
                nonSelectedArrayList.addAll(it)
                mAdapter.update(nonSelectedArrayList)
            }
        })


    }

    private fun getArrayListNonSelected(it: ArrayList<ModelScanCustomers>): ArrayList<ModelScanCustomers> {
        val data = ArrayList<ModelScanCustomers>()

        data.addAll(it)

        for (child in selectedArrayList) {
            inner@ for (newdata in 0 until data.size) {
                if (data[newdata].customer_id == child.customer_id) {
                    data.removeAt(newdata)
                    break@inner
                }
            }
        }
        val hashsetList = HashSet<ModelScanCustomers>(data);
        data.clear()
        data.addAll(hashsetList)

        return data
    }

    private fun apiImplimentation() {
        val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
        val type = Constant.getPrefs(this).getString(Constant.type, "")
        mbinding.progressBar.visibility = View.VISIBLE
        viewModel.getCustomersList(auth, "", "0", "0", type)
    }

    private fun setAdapter() {

        mbinding.customerRecycler.layoutManager = LinearLayoutManager(this)
        mbinding.customerRecycler.isNestedScrollingEnabled = true
        mAdapter = AdapterScanCustomers(this)
        mbinding.customerRecycler.adapter = mAdapter

        mbinding.selectedCustomer.layoutManager = LinearLayoutManager(this)
        mbinding.selectedCustomer.isNestedScrollingEnabled = true
        mAdapterSelected = AdapterSelectedCustomers(this)
        mbinding.selectedCustomer.adapter = mAdapterSelected

        if (selectedArrayList.size > 0) {
            mAdapterSelected.update(selectedArrayList)
            mAdapter.update(nonSelectedArrayList)
        }
        mbinding.selectAll.setOnClickListener {
            selectedArrayList.addAll(nonSelectedArrayList)
            nonSelectedArrayList.clear()
            mAdapter.update(nonSelectedArrayList)
            mAdapterSelected.update(selectedArrayList)
            mbinding.selectedCustomer.visibility = View.VISIBLE
            mbinding.linearSelected.visibility = View.VISIBLE
            mbinding.linearLayout15.visibility = View.GONE
            mbinding.unSelectAll.visibility = View.VISIBLE
            mbinding.count.text = "Selected Customers - " + selectedArrayList.size
            if (selectedArrayList.size > 0) {
                mbinding.redeem.setBackgroundResource(R.drawable.bg_seller_blue_rounded)
            } else {
                mbinding.redeem.setBackgroundResource(R.drawable.seller_button_round_blur)

            }

        }

        mbinding.unSelectAll.setOnClickListener {
            nonSelectedArrayList.addAll(selectedArrayList)
            selectedArrayList.clear()
            mAdapter.update(nonSelectedArrayList)
            mAdapterSelected.update(selectedArrayList)
            mbinding.linearLayout15.visibility = View.VISIBLE
            mbinding.linearSelected.visibility = View.GONE
            mbinding.count.text = "Selected Customers - " + selectedArrayList.size
            if (selectedArrayList.size > 0) {
                mbinding.redeem.setBackgroundResource(R.drawable.bg_seller_blue_rounded)
            } else {
                mbinding.redeem.setBackgroundResource(R.drawable.seller_button_round_blur)
            }

        }
        if (selectedArrayList.size > 0) {
            mbinding.linearSelected.visibility = View.VISIBLE
            mbinding.selectedCustomer.visibility = View.VISIBLE
        } else {
            mbinding.linearSelected.visibility = View.GONE
            mbinding.selectedCustomer.visibility = View.GONE
        }
    }


    private fun setToolbar() {
        setSupportActionBar(mbinding.toolbar)
        title = ""
        mbinding.title.text = "Customer List"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)

    }

    private fun clickListener() {
        if (selectedArrayList.size == 0) {
            mbinding.redeem.setBackgroundResource(R.drawable.seller_button_round_blur)
        } else {
            mbinding.redeem.setBackgroundResource(R.drawable.bg_seller_blue_rounded)
        }

        mbinding.redeem.setOnClickListener {
            if (intent.hasExtra("invite") || intent.hasExtra("list")) {
                if (selectedArrayList.size > 0) {
                    val intent = Intent()
                    intent.putExtra("model1", selectedArrayList)
                    setResult(Activity.RESULT_OK, intent)
                    finish()
                } else {
                    Toast.makeText(this, "Please select customer first", Toast.LENGTH_LONG).show()
                }

            }

        }




        mbinding.redeemNext.setOnClickListener {
            val intent = Intent(this, SubscriptionListActivity::class.java)
            intent.putExtra("subscription", "selection")
            intent.putExtra("customerId",customerId)
            startActivity(intent)
        }

    }

    override fun SelectCustomer(
        position: Int,
        status: Boolean,
        model: ModelScanCustomers
    ) {
        if (intent.hasExtra("list")) {
            selectedArrayList.add(model)
            nonSelectedArrayList.removeAt(position)
            mAdapter.update(nonSelectedArrayList)
            mAdapterSelected.update(selectedArrayList)
            mbinding.selectedCustomer.visibility = View.VISIBLE
            mbinding.unSelectAll.visibility = View.VISIBLE
            mbinding.count.text = "Selected Customers - " + selectedArrayList.size
            if (selectedArrayList.size > 0 || selectedArrayList.size == 0) {
                mbinding.redeem.setBackgroundResource(R.drawable.bg_seller_blue_rounded)
                mbinding.linearSelected.visibility = View.VISIBLE
                mbinding.selectedCustomer.visibility = View.VISIBLE
            } else {
                mbinding.linearSelected.visibility = View.GONE
                mbinding.selectedCustomer.visibility = View.GONE
                mbinding.redeem.setBackgroundResource(R.drawable.seller_button_round_blur)
            }

            if (nonSelectedArrayList.size > 0) {
                mbinding.linearLayout15.visibility = View.VISIBLE
            } else {
                mbinding.linearLayout15.visibility = View.GONE
            }

            mbinding.count.text = "Selected Customers - " + selectedArrayList.size

        } else {
            val model1 = nonSelectedArrayList[position]
            if (model1.status) {
                model1.status = false
            } else {
                for (child in nonSelectedArrayList) {
                    child.status = false
                }
                model1.status = true

            }
            nonSelectedArrayList[position] = model1
            customerId = nonSelectedArrayList[position].customer_id
            mAdapter.notifyDataSetChanged()


        }
    }


    override fun UnselectCustomer(position: Int, status: Boolean, model: ModelScanCustomers) {
        nonSelectedArrayList.add(model)
        selectedArrayList.removeAt(position)
        mAdapter.update(nonSelectedArrayList)
        mAdapterSelected.update(selectedArrayList)
        mbinding.linearSelected.visibility = View.GONE
        mbinding.count.text = "Selected Customers - " + selectedArrayList.size

        if (nonSelectedArrayList.size > 0) {
            mbinding.linearLayout15.visibility = View.VISIBLE
        } else {
            mbinding.linearLayout15.visibility = View.GONE

        }

        if (selectedArrayList.size > 0) {
            mbinding.linearSelected.visibility = View.VISIBLE
            mbinding.selectedCustomer.visibility = View.VISIBLE
            mbinding.redeem.setBackgroundResource(R.drawable.bg_seller_blue_rounded)

        } else {
            mbinding.linearSelected.visibility = View.GONE
            mbinding.selectedCustomer.visibility = View.GONE
            mbinding.redeem.setBackgroundResource(R.drawable.seller_button_round_blur)
        }
        mbinding.count.text = "Selected Customers - " + selectedArrayList.size

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()

        }
        return super.onOptionsItemSelected(item)
    }


    override fun onResume() {
        super.onResume()
//        val sharedPreferences1 = getSharedPreferences("reedeem", Context.MODE_PRIVATE)
//        if (!sharedPreferences1.getString("reedd", "").isEmpty()) {
//            finish()
//        }

    }


}
