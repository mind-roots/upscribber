package com.upscribber.upscribberSeller.customerBottom.customerList

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.upscribberSeller.customerBottom.profile.CustomerProfileInfoActivity
import com.upscribber.R
import com.upscribber.databinding.CustomerRecyclerSecondListBinding
import kotlinx.android.synthetic.main.customer_recycler_second_list.view.*

class AdaptercustomerListt(var context: Context, var i: Int) :
    RecyclerView.Adapter<AdaptercustomerListt.ViewHolder>() {

    private var itemss: List<ModelCustomerListt> = emptyList()
    lateinit var mBinding: CustomerRecyclerSecondListBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        mBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.customer_recycler_second_list,
            parent,
            false
        )
        return ViewHolder(mBinding.root)

    }

    override fun getItemCount(): Int {
        return if (i == 1) {
            itemss.size
        } else {
            if (itemss.size > 4) {
                4
            } else {
                return itemss.size
            }
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = itemss[position]
        mBinding.list = model

        if (model.contact_no != "null") {
            holder.itemView.customerNumber.text =
                com.upscribber.commonClasses.Constant.formatPhoneNumber(
                    model.contact_no
                )
        }



        holder.itemView.setOnClickListener {
            context.startActivity(
                Intent(
                    context,
                    CustomerProfileInfoActivity::class.java
                ).putExtra("modelCustomerProfile", model).putExtra("customers", "custom")
            )

        }
    }


    fun update(items: List<ModelCustomerListt>) {
        this.itemss = items
        notifyDataSetChanged()
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)

}