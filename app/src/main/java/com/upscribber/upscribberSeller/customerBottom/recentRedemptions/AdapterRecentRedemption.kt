package com.upscribber.upscribberSeller.customerBottom.recentRedemptions

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import android.content.Intent
import com.upscribber.upscribberSeller.customerBottom.scanning.SuccessfullyRedemptionActivity
import com.upscribber.R
import com.upscribber.databinding.CustomerRecyclerRecentRedemptionBinding
import kotlinx.android.synthetic.main.customer_recycler_recent_redemption.view.*


class AdapterRecentRedemption(var context: Context) : RecyclerView.Adapter<AdapterRecentRedemption.ViewHolder>() {

    private var itemss: List<ModelRecentRedemption> = emptyList()
    lateinit var mBinding : CustomerRecyclerRecentRedemptionBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.customer_recycler_recent_redemption, parent,false)
        return ViewHolder(
            mBinding.root
        )
    }

    override fun getItemCount(): Int {
        return  itemss.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = itemss[position]
        mBinding.list  = model

        if (model.contact_no != "null"){
            holder.itemView.customerNumber.text = com.upscribber.commonClasses.Constant.formatPhoneNumber(
                model.contact_no
            )
        }

        holder.itemView.setOnClickListener {
            context.startActivity(Intent(context, SuccessfullyRedemptionActivity::class.java).putExtra("modelRedeem",model)
                .putExtra("info","other"))
//            context.startActivity(Intent(Intent.ACTION_DIAL),Uri.parse("+4654654564154"))
//            val intent = Intent(Intent.ACTION_DIAL)
//            intent.data = Uri.parse("tel:0123456789")
//            startActivity(context,intent,null)
        }
    }

    fun update(items: List<ModelRecentRedemption>) {
        this.itemss = items
        notifyDataSetChanged()
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)
}