package com.upscribber.upscribberSeller.customerBottom.profile.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.databinding.DataBindingUtil
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.databinding.ActivityActiInfoPageBinding
import kotlinx.android.synthetic.main.activity_payment_details.*

class ActiInfoPageActivity : AppCompatActivity() {

    lateinit var mBinding : ActivityActiInfoPageBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this,R.layout.activity_acti_info_page)
        setToolbar()
        setData()
    }

    private fun setData() {
        val modelPayments = intent.getParcelableExtra<ActivityModel>("activityModel")
        textBusinessName.text = modelPayments.model.campaign_name
        textDescription.text = modelPayments.model.description
        textFrequency.text = modelPayments.model.frequency_type
//        name.text = modelPayments.model.customer_name
        if (modelPayments.sub_type == "1" || modelPayments.sub_type == "4"){
            merchantName.text = modelPayments.model.customer_name
        }else{
            merchantName.text = modelPayments.model.business_name
        }

        val date = Constant.getDateMonthYear(modelPayments.created_at)
        val time = Constant.getTime(modelPayments.created_at)
        textDate.text = date
        textTime.text = time

        if (modelPayments.model.redeemtion_cycle_qty == "500000") {
            textQuantity.text = "Unlimited"
        } else {
            textQuantity.text = modelPayments.model.redeemtion_cycle_qty
        }


        if (modelPayments.model.total_amount == "") {
            textAmount.text = "$" + modelPayments.model.price
        } else {
            textAmount.text = "$" + modelPayments.model.total_amount
        }



        if (modelPayments.sub_type == "1") {
            textView73.setBackgroundResource(R.drawable.bg_sea_green_rounded)
            textView73.text = "Shared"
            cancelReason.visibility = View.GONE
            cancelReasonspecific.visibility = View.GONE
            linearCustomers.visibility = View.GONE
            view11.visibility = View.GONE
            view12.visibility = View.GONE
            textView7.text = "Customer Name"
        } else if (modelPayments.sub_type == "2") {
            textView73.setBackgroundResource(R.drawable.bg_refund)
            textView73.text = "Paused"
            cancelReason.visibility = View.GONE
            cancelReasonspecific.visibility = View.GONE
            linearCustomers.visibility = View.GONE
            view11.visibility = View.GONE
            view12.visibility = View.GONE
        } else if (modelPayments.sub_type == "3") {
            view11.visibility = View.GONE
            view12.visibility = View.GONE
            textView73.setBackgroundResource(R.drawable.bg_pink_rounded)
            textView73.text = "Redeem"
            cancelReason.visibility = View.GONE
            cancelReasonspecific.visibility = View.GONE
            linearCustomers.visibility = View.GONE
        } else if (modelPayments.sub_type == "5") {
            textView73.setBackgroundResource(R.drawable.bg_refund)
            textView73.text = "Refund Request"
            cancelReason.visibility = View.GONE
            cancelReasonspecific.visibility = View.GONE
            linearCustomers.visibility = View.GONE
            view11.visibility = View.GONE
            view12.visibility = View.GONE

        } else if (modelPayments.sub_type == "8") {
            textView73.setBackgroundResource(R.drawable.bg_black_rounded)
            textView73.text = "Cancel"
            cancelReason.visibility = View.VISIBLE
            cancelReasonspecific.visibility = View.VISIBLE
            if (modelPayments.model.cancel_reason.isEmpty()){
                cancelReasonspecific.text = "Credits Refunded"
                cancelReasonspecific.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_ms_apply_credit, 0, 0, 0)
            }else{
                cancelReasonspecific.text = modelPayments.model.cancel_reason
                cancelReasonspecific.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_bad_service, 0, 0, 0)
            }
            linearCustomers.visibility = View.GONE
            view11.visibility = View.GONE
            view12.visibility = View.GONE

        } else if (modelPayments.sub_type == "9") {
            textView73.setBackgroundResource(R.drawable.bg_purple_rounded)
            textView73.text = "Renew"
            cancelReason.visibility = View.GONE
            cancelReasonspecific.visibility = View.GONE
            linearCustomers.visibility = View.GONE
            view11.visibility = View.GONE
            view12.visibility = View.GONE

        } else if (modelPayments.sub_type == "10") {
            textView73.setBackgroundResource(R.drawable.bg_pink_rounded)
            textView73.text = "Refunded"
            cancelReason.visibility = View.GONE
            cancelReasonspecific.visibility = View.GONE
            linearCustomers.visibility = View.GONE
            view11.visibility = View.GONE
            view12.visibility = View.GONE
        } else if (modelPayments.sub_type == "7") {
            textView73.setBackgroundResource(R.drawable.bg_pink_rounded)
            textView73.text = "Invite"
            cancelReason.visibility = View.GONE
            cancelReasonspecific.visibility = View.GONE
            linearCustomers.visibility = View.GONE
            view11.visibility = View.GONE
            view12.visibility = View.GONE
        } else if (modelPayments.sub_type == "4") {
            textView73.setBackgroundResource(R.drawable.bg_pink_rounded)
            textView73.text = "Unshared"
            cancelReason.visibility = View.GONE
            cancelReasonspecific.visibility = View.GONE
            linearCustomers.visibility = View.GONE
            view11.visibility = View.GONE
            view12.visibility = View.GONE
            textView7.text = "Customer Name"
        } else if (modelPayments.sub_type == "6") {
            textView73.setBackgroundResource(R.drawable.bg_pink_rounded)
            textView73.text = "Unpause"
            cancelReason.visibility = View.GONE
            cancelReasonspecific.visibility = View.GONE
            linearCustomers.visibility = View.GONE
            view11.visibility = View.GONE
            view12.visibility = View.GONE
        } else if (modelPayments.sub_type == "7") {
            textView73.setBackgroundResource(R.drawable.bg_pink_rounded)
            textView73.text = "Refund Denied"
            cancelReason.visibility = View.GONE
            cancelReasonspecific.visibility = View.GONE
            linearCustomers.visibility = View.GONE
            view11.visibility = View.GONE
            view12.visibility = View.GONE
        } else if (modelPayments.sub_type == "11") {
            textView73.setBackgroundResource(R.drawable.bg_pink_rounded)
            textView73.text = "Invited"
            cancelReason.visibility = View.GONE
            cancelReasonspecific.visibility = View.GONE
            linearCustomers.visibility = View.GONE
            view11.visibility = View.GONE
            view12.visibility = View.GONE
        } else if (modelPayments.sub_type == "12") {
            textView73.setBackgroundResource(R.drawable.bg_pink_rounded)
            textView73.text = "Business request"
            cancelReason.visibility = View.GONE
            cancelReasonspecific.visibility = View.GONE
            linearCustomers.visibility = View.GONE
            view11.visibility = View.GONE
            view12.visibility = View.GONE
        }

    }


    private fun setToolbar() {
        val modelPayments = intent.getParcelableExtra<ActivityModel>("activityModel")
        setSupportActionBar(mBinding.toolbar.toolbar)
        title = ""
        mBinding.toolbar.title.text = "Payments"
//        if (modelPayments.sub_type == "1" || modelPayments.sub_type == "4"){
//            mBinding.toolbar.title.text = modelPayments.model.customer_name
//        }else {
            mBinding.toolbar.title.text = modelPayments.model.business_name
//        }
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

}
