package com.upscribber.upscribberSeller.customerBottom.scanning.customerlistselection

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.commonClasses.Constant
import com.upscribber.databinding.ListCustomerSelectedInviteBinding
import kotlinx.android.synthetic.main.list_customer_selected_invite.view.*

class AdapterSelectedCustomers(var context: Context) : RecyclerView.Adapter<AdapterSelectedCustomers.ViewHolder>() {

    private lateinit var binding: ListCustomerSelectedInviteBinding
    private  var items = ArrayList<ModelScanCustomers>()
    var listener = context as SelectionSelectedCustomer

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        binding = ListCustomerSelectedInviteBinding.inflate(LayoutInflater.from(context), parent,false)
        return ViewHolder(binding)
    }



    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = items[position]
        holder.bind(model)

        if (model.contact_no != "null" ||  model.contact_no.isNotEmpty()){
            holder.itemView.customerNumber.text =Constant.formatPhoneNumber(model.contact_no)
        }



        holder.itemView.setOnClickListener {
            listener.UnselectCustomer(position,items[position].status,model)
        }
    }

    class ViewHolder(var  binding: ListCustomerSelectedInviteBinding): RecyclerView.ViewHolder(binding.root)  {
        fun bind(obj: ModelScanCustomers) {
            binding.model1 = obj
            binding.executePendingBindings()
        }
    }


    fun update(items: ArrayList<ModelScanCustomers>) {
        this.items = items
        notifyDataSetChanged()
    }

    interface SelectionSelectedCustomer{
        fun UnselectCustomer(
            position: Int,
            status: Boolean,
            model: ModelScanCustomers
        )
    }


}