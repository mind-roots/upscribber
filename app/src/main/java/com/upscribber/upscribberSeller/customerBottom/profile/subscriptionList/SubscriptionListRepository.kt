package com.upscribber.upscribberSeller.customerBottom.profile.subscriptionList

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.upscribber.R
import com.upscribber.upscribberSeller.subsriptionSeller.extras.ModelExtra

class SubscriptionListRepository(var application: Application)