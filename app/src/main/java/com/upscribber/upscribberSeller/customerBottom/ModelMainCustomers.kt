package com.upscribber.upscribberSeller.customerBottom

import com.upscribber.upscribberSeller.customerBottom.customerList.ModelCustomerListt
import com.upscribber.upscribberSeller.customerBottom.recentRedemptions.ModelRecentRedemption

class ModelMainCustomers {

    var campaign_status: String = ""
    var status: String = ""
    var msg: String = ""
    var arrayListRedeemCustomers: java.util.ArrayList<ModelRecentRedemption> = ArrayList()
    var arrayCustomers: ArrayList<ModelCustomerListt> = ArrayList()
    var cancelled_customer: String = ""
    var new_customer: String = ""
    var recurring_customer: String = ""
    var customers: String = ""
    var graph_info: String = ""
}
