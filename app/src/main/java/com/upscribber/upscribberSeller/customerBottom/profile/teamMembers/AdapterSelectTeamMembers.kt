package com.upscribber.upscribberSeller.customerBottom.profile.teamMembers

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.upscribber.R
import com.upscribber.upscribberSeller.manageTeam.ManageTeamModel
import kotlinx.android.synthetic.main.select_team_recyclerlist.view.*

class AdapterSelectTeamMembers(
    val mContext: Context
) : RecyclerView.Adapter<AdapterSelectTeamMembers.MyViewHolder>() {

    private var itemss: List<ManageTeamModel> = ArrayList()
    var listener = mContext as singleSelection

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(mContext)
            .inflate(
                R.layout.select_team_recyclerlist
                , parent, false
            )
        return MyViewHolder(v)
    }

    override fun getItemCount(): Int {
        return itemss.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = itemss[position]

        holder.itemView.textView198.text = model.namee
        holder.itemView.customerNumber.text = model.speciality
        Glide.with(mContext).load(model.profile_image).placeholder(R.mipmap.circular_placeholder)
            .into(holder.itemView.imageView)

        holder.itemView.setOnClickListener {
            listener.Selection(position)
        }

        if (model.status) {
            holder.itemView.setBackgroundResource(R.drawable.bg_blue_padding)
            holder.itemView.blueTick.visibility = View.VISIBLE
        } else {
            holder.itemView.setBackgroundResource(R.drawable.bg_customer_list)
            holder.itemView.blueTick.visibility = View.GONE
        }

    }


    fun update(items: List<ManageTeamModel>) {
        itemss = items
        notifyDataSetChanged()
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    interface singleSelection {
        fun Selection(position: Int)
    }

}


//
//    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
//        mBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout., parent,false)
//        return ViewHolder(
//            mBinding.root
//        )
//
//    }
//
//    override fun getItemCount(): Int {
//        return  itemss.size
//    }
//
//    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
//        val model = itemss[position]
//        mBinding.model = model
//

//

//
//
//    }
//
//

//
//    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)
//

//}