package com.upscribber.upscribberSeller.customerBottom.scanning

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.upscribber.commonClasses.ModelStatusMsg
import com.upscribber.profile.ModelGetProfile
import com.upscribber.subsciptions.manageSubscriptions.ModelRedeemCode

class ScanningViewModel(application: Application) : AndroidViewModel(application) {

    var scanningRepository: ScanningRepository = ScanningRepository(application)


    fun getReddeemCode(
        auth: String,
        code: String,
        quantity: String,
        order_id: String,
        type: String
    ) {
        scanningRepository.getRedeemCode(auth, code, quantity, order_id, type)
    }

    fun getmDataStatus(): LiveData<ModelStatusMsg> {
        return scanningRepository.getmDataFailure()
    }

    fun getmDataRedeemCodeData(): LiveData<ModelGetProfile> {
        return scanningRepository.getmDataRedeemCodeData()
    }


    fun getmScanBarCode(): LiveData<ModelScanBarCode> {
        return scanningRepository.getmScanBarCode()
    }

    fun getBarcodeQty(auth: String, code: String) {
        scanningRepository.getBarCodeQuantity(auth, code)
    }

    fun getmDataRedeemQuantity(): LiveData<ModelRedeemCode> {
        return scanningRepository.getmDataRedeemQuantity()
    }

    fun getReddeemCodeCustomers(
        auth: String,
        quantity: String,
        orderId: String,
        type: String,
        customerId: String
    ) {
        scanningRepository.getReddeemCodeCustomers(auth, quantity, orderId, type, customerId)

    }

    fun getRedeemCustomer(code: String) {
        scanningRepository.getReddemCustomers(code)

    }



}