package com.upscribber.upscribberSeller.customerBottom.profile

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.media.MediaScannerConnection
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.upscribber.commonClasses.Constant
import com.upscribber.R
import com.upscribber.databinding.ActivityEditProfileStaffBinding
import com.upscribber.commonClasses.RoundedBottomSheetDialogFragment
import com.upscribber.profile.ModelGetProfile
import com.upscribber.profile.ProfileViewModel
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.activity_edit_profile_staff.*
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.lang.Exception
import java.util.*

class EditProfileStaff : AppCompatActivity() {

    lateinit var mBinding: ActivityEditProfileStaffBinding
    lateinit var mViewModel: ProfileViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_edit_profile_staff)
        mViewModel = ViewModelProviders.of(this)[ProfileViewModel::class.java]
        setToolbar()
        clickListeners()
        setData()
    }

    private fun setData() {
        val model = intent.getParcelableExtra<ModelGetProfile>("model")
        val imagePath = Constant.getPrefs(this).getString(Constant.dataImagePath, "")
        if (intent.hasExtra("staff")){
            etName.setText(model.name)
            eTSpeciality.setText(model.speciality)
            Glide.with(this).load(model.profile_image).placeholder(R.mipmap.circular_placeholder).into(imageView1)
        }else if (intent.hasExtra("contractor")){
            etName.setText(model.name)
            eTSpeciality.setText(model.speciality)
            Glide.with(this).load(imagePath + model.profile_image).placeholder(R.mipmap.circular_placeholder).into(imageView1)
        }

    }

    private fun setToolbar() {
        setSupportActionBar(mBinding.toolbar.toolbar)
        title = ""
        mBinding.toolbar.title.text = "Edit Profile"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)

    }

    private fun clickListeners() {
        val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")

        mBinding.linearLayout3.setOnClickListener {
            val fragment = MenuFragmentEdit(mBinding.imageView1)
            fragment.show(supportFragmentManager, fragment.tag)
        }

        mBinding.saveProfile.setOnClickListener {

        }


        mBinding.etName.setOnFocusChangeListener { view, p1 ->
            if (p1) {
                mBinding.etName.setBackgroundResource(R.drawable.bg_seller_blue_outline)
            } else {
                mBinding.etName.setBackgroundResource(R.drawable.login_background_white)
            }
        }


        mBinding.etNumber.setOnFocusChangeListener { view, p1 ->
            if (p1) {
                mBinding.etNumber.setBackgroundResource(R.drawable.bg_seller_blue_outline)
            } else {
                mBinding.etNumber.setBackgroundResource(R.drawable.login_background_white)
            }
        }


        mBinding.etAddress.setOnFocusChangeListener { view, p1 ->
            if (p1) {
                mBinding.etAddress.setBackgroundResource(R.drawable.bg_seller_blue_outline)
            } else {
                mBinding.etAddress.setBackgroundResource(R.drawable.login_background_white)
            }
        }

        mBinding.etEmail.setOnFocusChangeListener { view, p1 ->
            if (p1) {
                mBinding.etEmail.setBackgroundResource(R.drawable.bg_seller_blue_outline)
            } else {
                mBinding.etEmail.setBackgroundResource(R.drawable.login_background_white)
            }
        }

        mBinding.eTSpeciality.setOnFocusChangeListener { view, p1 ->
            if (p1) {
                mBinding.eTSpeciality.setBackgroundResource(R.drawable.bg_seller_blue_outline)
            } else {
                mBinding.eTSpeciality.setBackgroundResource(R.drawable.login_background_white)
            }
        }


        mBinding.eTSpeciality.setOnTouchListener { view, event ->
            view.parent.requestDisallowInterceptTouchEvent(true)
            if ((event.action and MotionEvent.ACTION_MASK) == MotionEvent.ACTION_UP) {
                view.parent.requestDisallowInterceptTouchEvent(false)
            }
            return@setOnTouchListener false
        }


        mBinding.eTSpeciality.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                val result = "${s!!.length}/80"
                mBinding.tvSpecialityLimit.text = result
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {


            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if ( mBinding.eTSpeciality.text.length == 80){
                    Constant.hideKeyboard(this@EditProfileStaff, mBinding.eTSpeciality)
                }
            }

        })



    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    class MenuFragmentEdit(var imageView1: CircleImageView?) : RoundedBottomSheetDialogFragment() {

        private lateinit var imageUri: Uri
        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
            val view = inflater.inflate(R.layout.change_profile_pic_layout, container, false)
            val isSeller = Constant.getSharedPrefs(context!!).getBoolean(Constant.IS_SELLER, false)
            val cancelDialog = view.findViewById<TextView>(R.id.cancelDialog)
            val takePicture = view.findViewById<LinearLayout>(R.id.takePicture)
            val galleryLayout = view.findViewById<LinearLayout>(R.id.galleryLayout)
            val textPicture = view.findViewById<TextView>(R.id.textPicture)
            val imgPicture = view.findViewById<ImageView>(R.id.imgPicture)
            val imgGallery = view.findViewById<ImageView>(R.id.imgGallery)
            val textGallery = view.findViewById<TextView>(R.id.textGallery)

            if (isSeller) {
                cancelDialog.setBackgroundResource(R.drawable.bg_seller_button_blue)
            } else {
                cancelDialog.setBackgroundResource(R.drawable.invite_button_background)
            }


            takePicture.setOnClickListener {
                val checkSelfPermission =
                    ContextCompat.checkSelfPermission(this.activity!!, android.Manifest.permission.CAMERA)
                if (checkSelfPermission != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(this.activity!!, arrayOf(android.Manifest.permission.CAMERA), 2)
                } else {
                    openCamera()
                }

            }

            galleryLayout.setOnClickListener {
                val checkSelfPermission =
                    ContextCompat.checkSelfPermission(
                        this.activity!!,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE
                    )
                if (checkSelfPermission != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(
                        this.activity!!,
                        arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE),
                        1
                    )
                } else {
                    openAlbum()
                    // dismiss()
                }
            }

            cancelDialog.setOnClickListener {
                dismiss()
            }

            return view
        }

        private fun openCamera() {
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)

            startActivityForResult(intent, 29)

        }

        private fun openAlbum() {
            val intent = Intent("android.intent.action.GET_CONTENT")
            intent.type = "image/*"
            startActivityForResult(intent, 1)

        }


        override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

            super.onActivityResult(requestCode, resultCode, data)
            try {
                if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
                    if (data != null) {
                        val contentURI = data.data
                        try {
                            var bitmap = MediaStore.Images.Media.getBitmap(context!!.contentResolver, contentURI)
                            val path = saveImage(bitmap)
                            Toast.makeText(context, "Image Saved!", Toast.LENGTH_SHORT).show()
                            imageView1!!.setImageBitmap(bitmap)

                            dismiss()
                        } catch (e: IOException) {
                            e.printStackTrace()
                            Toast.makeText(context, "Failed!", Toast.LENGTH_SHORT).show()
                        }
                    }
                } else if (requestCode == 29) {
                    try {
                        val extras = data!!.extras
                        val bitmap2 = extras.get("data") as Bitmap

                        imageView1!!.setImageBitmap(bitmap2)

                        //  saveImage(thumbnail)
                        dismiss()
                        Toast.makeText(context, "Image Saved!", Toast.LENGTH_SHORT).show()
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
            dismiss()
        }


        private fun saveImage(myBitmap: Bitmap): String {
            val bytes = ByteArrayOutputStream()
            myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes)
            val wallpaperDirectory = File(
                (Environment.getExternalStorageDirectory()).toString() + IMAGE_DIRECTORY
            )
            // have the object build the directory structure, if needed.
            Log.d("fee", wallpaperDirectory.toString())
            if (!wallpaperDirectory.exists()) {

                wallpaperDirectory.mkdirs()
            }

            try {
                Log.d("heel", wallpaperDirectory.toString())
                val f = File(
                    wallpaperDirectory, ((Calendar.getInstance()
                        .timeInMillis).toString() + ".jpg")
                )
                f.createNewFile()
                val fo = FileOutputStream(f)
                fo.write(bytes.toByteArray())
                MediaScannerConnection.scanFile(
                    context,
                    arrayOf(f.path),
                    arrayOf("image/jpeg"), null
                )
                fo.close()
                Log.d("TAG", "File Saved::--->" + f.absolutePath)

                return f.absolutePath
            } catch (e1: IOException) {
                e1.printStackTrace()
            }

            return ""
        }

        companion object {
            private val IMAGE_DIRECTORY = "/demonuts"
        }


    }
}
