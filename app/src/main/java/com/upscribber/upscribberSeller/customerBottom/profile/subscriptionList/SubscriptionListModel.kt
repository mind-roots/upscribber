package com.upscribber.upscribberSeller.customerBottom.profile.subscriptionList

import android.os.Parcel
import android.os.Parcelable
import com.upscribber.commonClasses.Constant
import java.math.BigDecimal


class SubscriptionListModel() : Parcelable{

    var member_name: String = ""
    var subscriber: String = ""
    var redeem_count: String = ""
    var assign_to: String = ""
    var order_id: String = ""
    var status: Boolean = false
    var type: Int = 0
    var statusMain: String = ""
    var msg: String = ""
    var campaign_id: String = ""
    var price: String = ""
    var unit: String = ""
    var frequency_type: String = ""
    var frequency_value: String = ""
    var campaign_image: String = ""
    var campaign_name: String = ""
    var description: String = ""
    var status1: String = ""
    var redeemtion_cycle_qty: String = ""
    var business_name: String = ""
    var discount_price: String = ""
    var subscription_id: String = ""
    var is_public: String = ""
    var bought: String = ""



    constructor(parcel: Parcel) : this() {
        member_name = parcel.readString()
        subscriber = parcel.readString()
        redeem_count = parcel.readString()
        assign_to = parcel.readString()
        order_id = parcel.readString()
        status = parcel.readByte() != 0.toByte()
        type = parcel.readInt()
        statusMain = parcel.readString()
        msg = parcel.readString()
        campaign_id = parcel.readString()
        price = parcel.readString()
        unit = parcel.readString()
        frequency_type = parcel.readString()
        frequency_value = parcel.readString()
        campaign_image = parcel.readString()
        campaign_name = parcel.readString()
        description = parcel.readString()
        status1 = parcel.readString()
        redeemtion_cycle_qty = parcel.readString()
        business_name = parcel.readString()
        discount_price = parcel.readString()
        subscription_id = parcel.readString()
        is_public = parcel.readString()
        bought = parcel.readString()
    }


    fun frequencyType(): String {
        return frequency_type
    }


    fun subscribers(): String {
        return "$bought subscribers"
    }

    fun discountedPrice(): String {
        return discount_price.toDouble().toInt().toString() + " %off"
    }


    fun pricee(): String {
        var bd = BigDecimal(Constant.getCalculatedPrice(discount_price, price))
        var priceee = ""
        bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP)
        priceee = if (bd.toString().contains(".")){
            val splitPos = bd.toString().split(".")
            if (splitPos[1] == "00" || splitPos[1] == "0") {
                "$" + splitPos[0]
            }else{
                "$$bd"
            }
        }else{
            "$$bd"
        }

        return priceee

    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(member_name)
        parcel.writeString(subscriber)
        parcel.writeString(redeem_count)
        parcel.writeString(assign_to)
        parcel.writeString(order_id)
        parcel.writeByte(if (status) 1 else 0)
        parcel.writeInt(type)
        parcel.writeString(statusMain)
        parcel.writeString(msg)
        parcel.writeString(campaign_id)
        parcel.writeString(price)
        parcel.writeString(unit)
        parcel.writeString(frequency_type)
        parcel.writeString(frequency_value)
        parcel.writeString(campaign_image)
        parcel.writeString(campaign_name)
        parcel.writeString(description)
        parcel.writeString(status1)
        parcel.writeString(redeemtion_cycle_qty)
        parcel.writeString(business_name)
        parcel.writeString(discount_price)
        parcel.writeString(subscription_id)
        parcel.writeString(is_public)
        parcel.writeString(bought)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SubscriptionListModel> {
        override fun createFromParcel(parcel: Parcel): SubscriptionListModel {
            return SubscriptionListModel(parcel)
        }

        override fun newArray(size: Int): Array<SubscriptionListModel?> {
            return arrayOfNulls(size)
        }
    }




}