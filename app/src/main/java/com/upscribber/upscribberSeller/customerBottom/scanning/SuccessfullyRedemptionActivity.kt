package com.upscribber.upscribberSeller.customerBottom.scanning

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.home.ModelHomeLocation
import com.upscribber.notification.ModelNotificationCustomer
import com.upscribber.subsciptions.manageSubscriptions.ModelRedeemCode
import com.upscribber.upscribberSeller.customerBottom.recentRedemptions.ModelRecentRedemption
import kotlinx.android.synthetic.main.activity_successfully_redemption.*

class SuccessfullyRedemptionActivity : AppCompatActivity() {

    lateinit var buttonDone: TextView
    lateinit var textView149: TextView
    lateinit var textView148: TextView
    lateinit var imageView41: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_successfully_redemption)
        initz()
        clickListeners()
        setData()

        when {
            intent.hasExtra("manually") -> {
                imageView41.setImageResource(R.mipmap.bg_reviews)
                textView149.visibility = View.VISIBLE
                textView148.visibility = View.VISIBLE
                textView138.visibility = View.VISIBLE
                textView139.visibility = View.VISIBLE
                textView142.visibility = View.VISIBLE
                textView143.visibility = View.VISIBLE
            }
            intent.hasExtra("manual1") -> {
                imageView41.setImageResource(R.mipmap.bg_reviews)
                textView149.visibility = View.GONE
                textView148.visibility = View.GONE
                textView138.visibility = View.VISIBLE
                textView139.visibility = View.VISIBLE
                textView142.visibility = View.VISIBLE
                textView143.visibility = View.VISIBLE
            }
            intent.hasExtra("info") -> {
                imageView41.setImageResource(R.mipmap.bg_redemption)
                textView149.visibility = View.GONE
                textView148.visibility = View.GONE
                textView138.visibility = View.GONE
                textView139.visibility = View.GONE
                textView142.visibility = View.GONE
                textView143.visibility = View.GONE
            }
            intent.hasExtra("infoNotification") -> {
                imageView41.setImageResource(R.mipmap.bg_redemption)
                textView149.visibility = View.GONE
                textView148.visibility = View.GONE
                textView138.visibility = View.GONE
                textView139.visibility = View.GONE
                textView142.visibility = View.GONE
                textView143.visibility = View.GONE
            }

            else -> {
                imageView41.setImageResource(R.mipmap.bg_redemption)
                textView149.visibility = View.GONE
                textView148.visibility = View.GONE
                textView138.visibility = View.VISIBLE
                textView139.visibility = View.VISIBLE
                textView142.visibility = View.VISIBLE
                textView143.visibility = View.VISIBLE

            }
        }

    }

    private fun setData() {
        val imagePath = Constant.getPrefs(this).getString(Constant.dataImagePath, "")
        val arrayHomeLocation =
            intent.getParcelableArrayListExtra<ModelHomeLocation>("arrayHomeLocation")

        if (intent.hasExtra("info")) {
            val modelRedeem = intent.getParcelableExtra<ModelRecentRedemption>("modelRedeem")

            Glide.with(this).load(imagePath + modelRedeem.profile_image)
                .placeholder(R.mipmap.circular_placeholder).into(circleImageView3)
            if (modelRedeem.team_member.isEmpty()) {
                textView135.text = ""
            } else {
                textView135.text = modelRedeem.team_member
            }

            textSubscription.text = modelRedeem.customer_name
            textView297.text = modelRedeem.customer_name


            textView142.text = modelRedeem.customer_name
            if (modelRedeem.contact_no != "null") {
                textView298.text = Constant.formatPhoneNumber(modelRedeem.contact_no)
            }

            val splitPos = modelRedeem.price.split(".")
            if (splitPos[1] == "00" || splitPos[1] == "0") {
                textView141.text = "$" + splitPos[0]
            } else {
                textView141.text = "$" + modelRedeem.price
            }

            val date = Constant.getDatee(modelRedeem.created_at)
            val time = Constant.getTime(modelRedeem.created_at)
            textView145.text = date
            textView147.text = time
        } else if (intent.hasExtra("infoNotification")) {
            val modelRedeem =
                intent.getParcelableExtra<ModelNotificationCustomer>("modelRedeemNotification")

            Glide.with(this).load(imagePath + modelRedeem.profile_image)
                .placeholder(R.mipmap.circular_placeholder).into(circleImageView3)
            if (modelRedeem.team_member.isEmpty()) {
                textView135.text = ""
            } else {
                textView135.text = modelRedeem.team_member
            }

            textSubscription.text = modelRedeem.customer_name
            textView297.text = modelRedeem.customer_name


            textView142.text = modelRedeem.customer_name
            if (modelRedeem.contact_no != "null") {
                textView298.text = Constant.formatPhoneNumber(modelRedeem.contact_no)
            }

            val splitPos = modelRedeem.price.split(".")
            if (splitPos[1] == "00" || splitPos[1] == "0") {
                textView141.text = "$" + splitPos[0]
            } else {
                textView141.text = "$" + modelRedeem.price
            }

            val date = Constant.getDatee(modelRedeem.created_at)
            val time = Constant.getTime(modelRedeem.created_at)
            textView145.text = date
            textView147.text = time
        } else if (intent.hasExtra("modelRedeem")) {
            val modelRedeemption = intent.getParcelableExtra<ModelScanBarCode>("modelRedeem")
            Glide.with(this).load(imagePath + modelRedeemption.profile_image)
                .placeholder(R.mipmap.circular_placeholder).into(circleImageView3)
            if (modelRedeemption.team_member.isEmpty()) {
                textView135.text = ""
            } else {
                textView135.text = modelRedeemption.team_member
            }

            textSubscription.text = modelRedeemption.business_name
            textView297.text = modelRedeemption.customer_name
            if (modelRedeemption.contact_no != "null") {
                textView298.text = Constant.formatPhoneNumber(
                    modelRedeemption.contact_no
                )
            }

            if (modelRedeemption.remaining_visits > "100") {
                textView143.text = "Unlimited " + modelRedeemption.unit
            } else {
                textView143.text = modelRedeemption.remaining_visits + " " + modelRedeemption.unit
            }

            val splitPos = modelRedeemption.total.split(".")
            if (splitPos[1] == "00" || splitPos[1] == "0") {
                textView141.text = "$" + splitPos[0]
            } else {
                textView141.text = "$" + modelRedeemption.total
            }


            textView139.text = arrayHomeLocation[0].street + "," + arrayHomeLocation[0].city
            val date = Constant.getDatee(modelRedeemption.created_at)
            val time = Constant.getTime(modelRedeemption.created_at)
            textView145.text = date
            textView147.text = time
        } else if (intent.hasExtra("modelCustomerRedeem")) {
            val modelRedeemption =
                intent.getParcelableExtra<ModelScanBarCode>("modelCustomerRedeem")
            Glide.with(this).load(imagePath + modelRedeemption.profile_image)
                .placeholder(R.mipmap.circular_placeholder).into(circleImageView3)
            if (modelRedeemption.team_member.isEmpty()) {
                textView135.text = ""
            } else {
                textView135.text = modelRedeemption.team_member
            }

            textSubscription.text = modelRedeemption.business_name
            textView297.text = modelRedeemption.customer_name
            if (modelRedeemption.contact_no != "null") {
                textView298.text = Constant.formatPhoneNumber(
                    modelRedeemption.contact_no
                )
            }
            val splitPos = modelRedeemption.total.split(".")
            if (splitPos[1] == "00" || splitPos[1] == "0") {
                textView141.text = "$" + splitPos[0]
            } else {
                textView141.text = "$" + modelRedeemption.total
            }
            if (modelRedeemption.remaining_visits > "100") {
                textView143.text = "Unlimited " + modelRedeemption.unit
            } else {
                textView143.text = modelRedeemption.remaining_visits + " " + modelRedeemption.unit
            }
            textView139.text =
                modelRedeemption.locations[0].street + "," + modelRedeemption.locations[0].city
            val date = Constant.getDatee(modelRedeemption.created_at)
            val time = Constant.getTime(modelRedeemption.created_at)
            textView145.text = date
            textView147.text = time
        } else {
            val modelRedeemption = intent.getParcelableExtra<ModelRedeemCode>("modelRedeem")
            Glide.with(this).load(imagePath + modelRedeemption.profile_image)
                .placeholder(R.mipmap.circular_placeholder).into(circleImageView3)
            if (modelRedeemption.team_member.isEmpty()) {
                textView135.text = ""
            } else {
                textView135.text = modelRedeemption.team_member
            }

            textSubscription.text = modelRedeemption.business_name
            textView297.text = modelRedeemption.customer_name
            if (modelRedeemption.contact_no != "null") {
                textView298.text = Constant.formatPhoneNumber(
                    modelRedeemption.contact_no
                )
            }
            val splitPos = modelRedeemption.total_amount.split(".")
            if (splitPos[1] == "00" || splitPos[1] == "0") {
                textView141.text = "$" + splitPos[0]
            } else {
                textView141.text = "$" + modelRedeemption.total_amount
            }
            if (modelRedeemption.remaining_visits > "100") {
                textView143.text = "Unlimited " + modelRedeemption.unit
            } else {
                textView143.text = modelRedeemption.remaining_visits + " " + modelRedeemption.unit
            }
            textView139.text = arrayHomeLocation[0].street + "," + arrayHomeLocation[0].city
            val date = Constant.getDatee(modelRedeemption.created_at)
            val time = Constant.getTime(modelRedeemption.created_at)
            textView145.text = date
            textView147.text = time
        }


    }

    private fun clickListeners() {
        buttonDone.setOnClickListener {
            val sharedPreferences = getSharedPreferences("reedeem", Context.MODE_PRIVATE).edit()
            sharedPreferences.putString("reedd", "back")
            sharedPreferences.apply()
            finish()
        }

    }

    private fun initz() {
        buttonDone = findViewById(R.id.buttonDone1)
        imageView41 = findViewById(R.id.imageView41)
        textView149 = findViewById(R.id.textView149)
        textView148 = findViewById(R.id.textView148)


    }
}
