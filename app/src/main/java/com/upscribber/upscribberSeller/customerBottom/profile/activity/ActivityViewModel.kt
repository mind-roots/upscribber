package com.upscribber.upscribberSeller.customerBottom.profile.activity

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.upscribber.commonClasses.ModelStatusMsg

class ActivityViewModel(application: Application) : AndroidViewModel(application) {

    var activityRepository : ActivityRepository = ActivityRepository(application)

    fun getActivityData(auth: String, type: String, tab: String) {
        activityRepository.getActivityData(auth,type)
    }

    fun getStatus(): LiveData<ModelStatusMsg> {
        return activityRepository.getmAllStatus()
    }

    fun getAllActivities(): LiveData<ArrayList<ActivityModel>> {
        return activityRepository.getAllActivities()
    }


}