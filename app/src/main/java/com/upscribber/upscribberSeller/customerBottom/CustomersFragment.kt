package com.upscribber.upscribberSeller.customerBottom

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.upscribber.upscribberSeller.customerBottom.customerList.AdaptercustomerListt
import com.upscribber.upscribberSeller.customerBottom.recentRedemptions.AdapterRecentRedemption
import com.upscribber.upscribberSeller.navigationCustomer.CustomerSellerActivity
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.databinding.FragmentCustomersBinding
import com.upscribber.home.FavoriteInterface
import kotlinx.android.synthetic.main.fragment_customers.*


class CustomersFragment : Fragment(){

    private lateinit var mAdapterCustomerListt: AdaptercustomerListt
    private lateinit var mAdapterRecentRedemption: AdapterRecentRedemption
    lateinit var mBinding: FragmentCustomersBinding
    private lateinit var viewModel: customerBottomViewModel
    lateinit var update : FavoriteInterface

    override fun onCreateView(
        inflater: LayoutInflater, container:
        ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_customers, container, false)
        viewModel = ViewModelProviders.of(this)[customerBottomViewModel::class.java]
        clickListeners()
        apiInitialization()
        observerInit()
        return mBinding.root
    }

    private fun observerInit() {
        viewModel.getmDataFailure().observe(this, Observer {
            if (it.msg.isNotEmpty()) {
                mBinding.progressBar26.visibility = View.GONE
                Toast.makeText(activity, it.msg, Toast.LENGTH_SHORT).show()
            }
        })


        viewModel.getmDataCustomers().observe(this, Observer {
            mBinding.progressBar26.visibility = View.GONE
            mBinding.textCustomers.text = it.customers
            mBinding.newCustomerValue.text = it.new_customer
            mBinding.returnCustomerValue.text = it.recurring_customer
            mBinding.cancelCustomerValue.text = it.recurring_customer

            if (it.arrayCustomers.size > 0 && it.arrayListRedeemCustomers.size > 0) {
                mAdapterCustomerListt.update(it.arrayCustomers)
                mAdapterRecentRedemption.update(it.arrayListRedeemCustomers)
                recyclerRecentRedemption.visibility = View.VISIBLE
                textView188.visibility = View.VISIBLE
                textView187.visibility = View.VISIBLE
                recyclerCustomerList.visibility = View.VISIBLE
                imgNoSubscription.visibility = View.GONE
                tvNoData.visibility = View.GONE
                tvNoDataDescription.visibility = View.GONE
                cardView8.visibility = View.VISIBLE
                update.addToFav(1)
//                nestedScrollView12.visibility = View.VISIBLE

            } else {
                update.addToFav(1)
                when {
                    it.arrayListRedeemCustomers.size > 0 -> {
                        mAdapterRecentRedemption.update(it.arrayListRedeemCustomers)
                        textView187.visibility = View.VISIBLE
                        recyclerRecentRedemption.visibility = View.VISIBLE
                        imgNoSubscription.visibility = View.GONE
                        recyclerCustomerList.visibility = View.GONE
                        tvNoData.visibility = View.GONE
                        tvNoDataDescription.visibility = View.GONE
                        cardView8.visibility = View.VISIBLE
                    }
                    it.arrayCustomers.size > 0 -> {
                        mAdapterCustomerListt.update(it.arrayCustomers)
                        textView188.visibility = View.VISIBLE
                        recyclerCustomerList.visibility = View.VISIBLE
                        imgNoSubscription.visibility = View.GONE
                        tvNoData.visibility = View.GONE
                        recyclerRecentRedemption.visibility = View.GONE
                        tvNoDataDescription.visibility = View.GONE
                        cardView8.visibility = View.VISIBLE
                        update.addToFav(1)
            //                nestedScrollView12.visibility = View.VISIBLE
                    }
                    else -> {
                        recyclerRecentRedemption.visibility = View.GONE
                        recyclerCustomerList.visibility = View.GONE
                        textView187.visibility = View.GONE
                        imgNoSubscription.visibility = View.VISIBLE
                        tvNoData.visibility = View.VISIBLE
                        tvNoDataDescription.visibility = View.VISIBLE
                        textView188.visibility = View.GONE
                        cardView8.visibility = View.GONE

                    }
                }


//                {
//                    textView187.visibility = View.GONE
//
//                    cardView8.visibility = View.GONE
////                nestedScrollView12.visibility = View.GONE
//                }






//                nestedScrollView12.visibility = View.GONE

            }




            if (it.arrayCustomers.size >= 4) {
                viewAl.visibility = View.VISIBLE
            } else {
                viewAl.visibility = View.GONE
            }

            if (it.arrayListRedeemCustomers.size >= 4) {
                viewAll.visibility = View.VISIBLE
            } else {
                viewAll.visibility = View.GONE
            }


//             else {
//                textView188.visibility = View.GONE
//                recyclerCustomerList.visibility = View.GONE
//                cardView8.visibility = View.GONE
////                nestedScrollView12.visibility = View.GONE
//                update.addToFav(0)
//            }

        })
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        update = context as FavoriteInterface
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        update = activity as FavoriteInterface

    }

    private fun apiInitialization() {
        mBinding.progressBar26.visibility = View.VISIBLE
        val auth = Constant.getPrefs(activity!!).getString(Constant.auth_code, "")
        viewModel.getCustomerData(auth, "1")
    }

    private fun clickListeners() {
        mBinding.viewAl.setOnClickListener {
            activity!!.startActivity(Intent(context, CustomerSellerActivity::class.java))
        }

        mBinding.viewAll.setOnClickListener {
            activity!!.startActivity(Intent(context, CustomerSellerActivity::class.java))
        }

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setAdapter()
    }

    private fun setAdapter() {
        mBinding.recyclerRecentRedemption.layoutManager = LinearLayoutManager(activity!!)
        mBinding.recyclerRecentRedemption.hasFixedSize()
        mBinding.recyclerRecentRedemption.isNestedScrollingEnabled = false
        mAdapterRecentRedemption = AdapterRecentRedemption(activity!!)
        mBinding.recyclerRecentRedemption.adapter = mAdapterRecentRedemption


        mBinding.recyclerCustomerList.layoutManager = LinearLayoutManager(activity!!)
        mBinding.recyclerCustomerList.hasFixedSize()
        mBinding.recyclerCustomerList.isNestedScrollingEnabled = false
        mAdapterCustomerListt = AdaptercustomerListt(activity!!,0)
        mBinding.recyclerCustomerList.adapter = mAdapterCustomerListt

    }


}
