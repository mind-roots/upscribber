package com.upscribber.upscribberSeller.customerBottom.scanning.customerlistselection

import android.os.Parcel
import android.os.Parcelable
import com.upscribber.commonClasses.Constant

class ModelScanCustomers() : Parcelable{

    var profile_image: String = ""
    var profileImage: String = ""
    var contact_no: String = ""
    var name: String = ""
    var customer_id: String = ""
    var customerName : String = ""
    var customerPhone : String = ""
    var customerImage : Int = 0
    var status : Boolean = false

    constructor(parcel: Parcel) : this() {
        profileImage = parcel.readString()
        contact_no = parcel.readString()
        name = parcel.readString()
        customer_id = parcel.readString()
        customerName = parcel.readString()
        customerPhone = parcel.readString()
        customerImage = parcel.readInt()
        status = parcel.readByte() != 0.toByte()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(profileImage)
        parcel.writeString(contact_no)
        parcel.writeString(name)
        parcel.writeString(customer_id)
        parcel.writeString(customerName)
        parcel.writeString(customerPhone)
        parcel.writeInt(customerImage)
        parcel.writeByte(if (status) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ModelScanCustomers> {
        override fun createFromParcel(parcel: Parcel): ModelScanCustomers {
            return ModelScanCustomers(parcel)
        }

        override fun newArray(size: Int): Array<ModelScanCustomers?> {
            return arrayOfNulls(size)
        }
    }

}