package com.upscribber.upscribberSeller.customerBottom.scanning.customerlistselection

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import kotlinx.android.synthetic.main.list_customer_recycler.view.*


class AdapterScanCustomers(var context: Context) : RecyclerView.Adapter<AdapterScanCustomers.ViewHolder>() {

    private var itemss: List<ModelScanCustomers> = ArrayList()
    var listener = context as SelectionCustomer

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(context)
            .inflate(R.layout.list_customer_recycler, parent, false)
        return ViewHolder(v)

    }

    override fun getItemCount(): Int {
        return  itemss.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = itemss[position]

        holder.itemView.setOnClickListener {
            listener.SelectCustomer(position,itemss[position].status,model)
        }
        val imagePath  = Constant.getPrefs(context).getString(Constant.dataImagePath, "")
        holder.itemView.txtName.text = model.name
        if (model.contact_no != "null" || model.contact_no.isNotEmpty()){
            holder.itemView.customerNumber.text = Constant.formatPhoneNumber(model.contact_no)
        }

        Glide.with(context).load(imagePath + model.profileImage).placeholder(R.mipmap.circular_placeholder).into(holder.itemView.imageView)

        if (model.status){
            holder.itemView.blueTick.visibility = View.VISIBLE
            holder.itemView.constraint11.setBackgroundResource(R.drawable.bg_customer_list_selection)
        }else{
            holder.itemView.blueTick.visibility = View.GONE
            holder.itemView.constraint11.setBackgroundResource(R.drawable.bg_customer_list)
        }

    }

    fun updateArray(position: Int) {

        val selectedModel = itemss[position]
        if (selectedModel.status) {
            selectedModel.status = false

        } else {
            for (child in itemss) {
                child.status = false
            }
            itemss[position].status = true
        }

        notifyDataSetChanged()
    }


    interface SelectionCustomer{
        fun SelectCustomer(
            position: Int,
            status: Boolean,
            model: ModelScanCustomers
        )
    }



    fun update(items: List<ModelScanCustomers>) {
        this.itemss = items
        notifyDataSetChanged()
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)

}

