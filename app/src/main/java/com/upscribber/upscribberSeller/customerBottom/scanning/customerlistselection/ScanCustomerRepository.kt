package com.upscribber.upscribberSeller.customerBottom.scanning.customerlistselection

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.upscribber.commonClasses.Constant
import com.upscribber.commonClasses.ModelStatusMsg
import com.upscribber.commonClasses.WebServicesCustomers
import com.upscribber.subsciptions.merchantSubscriptionPages.ModelCampaignInfo
import com.upscribber.upscribberSeller.individualProfile.addIndividual.ModelInvite
import com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.subscription.ModelSubscriptionCard
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class ScanCustomerRepository(application: Application) {


    private val mDataCustomers = MutableLiveData<ArrayList<ModelScanCustomers>>()
    private val mDataFailure = MutableLiveData<ModelStatusMsg>()
    private val mDataInvited = MutableLiveData<ModelSubscriptionCard>()

    fun getCustomersList(
        auth: String,
        search: String,
        viewId: String,
        viewType: String,
        type: String
    ) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.getCustomers(auth, search, viewId, viewType, type)
        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        val arrayCustomers = ArrayList<ModelScanCustomers>()
                        if (status == "true") {
                            val data = json.optJSONObject("data")
                            val customers = data.optJSONArray("customers")
                            for (i in 0 until customers.length()) {
                                val dataCustomers = customers.optJSONObject(i)
                                val modelScanCustomers = ModelScanCustomers()
                                modelScanCustomers.customer_id =
                                    dataCustomers.optString("customer_id")
                                modelScanCustomers.name = dataCustomers.optString("name")
                                modelScanCustomers.contact_no =
                                    dataCustomers.optString("contact_no")
                                modelScanCustomers.profileImage =
                                    dataCustomers.optString("profile_image")
                                arrayCustomers.add(modelScanCustomers)
                            }

                            modelStatus.count = data.optString("count")
                            mDataCustomers.value = arrayCustomers

                        } else {
                            modelStatus.msg = msg
                            modelStatus.status = status
                            mDataFailure.value = modelStatus
                        }

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus

            }
        })

    }


    fun getCustomersList(): LiveData<ArrayList<ModelScanCustomers>> {
        return mDataCustomers
    }

    fun getmDataStatus(): LiveData<ModelStatusMsg> {
        return mDataFailure
    }

    fun getInviteCustomers(
        auth: String,
        customerId: String,
        subscriptionId: String,
        contactNumber: String
    ) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.inviteCustomer(
            auth,
            customerId,
            subscriptionId,
            contactNumber,
            "https://www.upscribbr.com/"
        )
        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        val modelInvited = ModelSubscriptionCard()
                        if (status == "true") {
                            val data = json.optJSONObject("data")
                            modelInvited.invited_customers = data.optString("invited_customers")
                            val subscription = data.optJSONObject("subscription")
                            modelInvited.id = subscription.optString("id")
                            modelInvited.campaign_id = subscription.optString("campaign_id")
                            modelInvited.price = subscription.optString("price")
                            modelInvited.discount_price = subscription.optString("discount_price")
                            modelInvited.redeemtion_cycle_qty =
                                subscription.optString("redeemtion_cycle_qty")
                            modelInvited.frequency_type = subscription.optString("frequency_type")
                            modelInvited.free_trial = subscription.optString("free_trial")
                            modelInvited.free_trial_qty = subscription.optString("free_trial_qty")
                            modelInvited.introductory_price =
                                subscription.optString("introductory_price")
                            modelInvited.introductory_days =
                                subscription.optString("introductory_days")
                            modelInvited.frequency_value = subscription.optString("frequency_value")
                            modelInvited.unit = subscription.optString("unit")
                            modelInvited.status = subscription.optString("status")
                            modelInvited.url = subscription.optString("url")
                            modelInvited.created_at = subscription.optString("created_at")
                            modelInvited.updated_at = subscription.optString("updated_at")
                            modelInvited.merchant_id = subscription.optString("merchant_id")
                            modelInvited.campaign_name = subscription.optString("campaign_name")
                            modelInvited.merchant_name = subscription.optString("merchant_name")
                            modelInvited.business_id = subscription.optString("business_id")
                            modelInvited.feature_image = subscription.optString("feature_image")
                            modelInvited.business_logo = subscription.optString("business_logo")
                            modelInvited.business_name = subscription.optString("business_name")
                            modelInvited.description = subscription.optString("description")
                            modelInvited.status1 = status
                            modelInvited.msg = msg
                            mDataInvited.value = modelInvited
                        } else {
                            modelStatus.msg = msg
                            modelStatus.status = status
                            mDataFailure.value = modelStatus
                        }

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus

            }
        })

    }

    fun getmDataInvitedCustomers(): LiveData<ModelSubscriptionCard> {
        return mDataInvited
    }


}