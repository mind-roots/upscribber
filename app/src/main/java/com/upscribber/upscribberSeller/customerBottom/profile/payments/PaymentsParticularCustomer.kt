package com.upscribber.upscribberSeller.customerBottom.profile.payments

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.payment.customerPayments.PaymentsViewModel
import com.upscribber.upscribberSeller.customerBottom.customerList.ModelCustomerListt
import kotlinx.android.synthetic.main.activity_payments_particular_customer.*

class PaymentsParticularCustomer : AppCompatActivity() {

    lateinit var  toolbar : Toolbar
    lateinit var  toolbartitle : TextView
    lateinit var mViewModel: PaymentsViewModel
    lateinit var mAdapterPayments: PaymentParticularAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payments_particular_customer)
        mViewModel = ViewModelProviders.of(this)[PaymentsViewModel::class.java]
        setToolbar()
        setAdapter()
        apiImplimentation()
        createTabs()
        observerInit()


    }

    private fun observerInit() {
        mViewModel.getmAllTransactions().observe(this, Observer {
            progressBar.visibility = View.GONE
            if (it.arrayPaymentsTransactions.size > 0) {
                noData.visibility = View.GONE
                recyclerView4.visibility = View.VISIBLE
                mAdapterPayments.update(it.arrayPaymentsTransactions)
            }else{
                noData.visibility = View.VISIBLE
                recyclerView4.visibility = View.GONE
            }
        })

        mViewModel.getStatus().observe(this, Observer {
            progressBar.visibility = View.GONE
            if (it.msg == "Invalid auth code") {
                Constant.commonAlert(this)
            } else if(it.msg.isEmpty()){
                noData.visibility = View.VISIBLE
                recyclerView4.visibility = View.GONE
            }else{
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
            }
        })

    }

    private fun setAdapter() {
        recyclerView4.layoutManager = LinearLayoutManager(this)
        mAdapterPayments = PaymentParticularAdapter(this,  "")
        recyclerView4.adapter = mAdapterPayments

    }

    private fun apiImplimentation() {
        val modelCustomerList = intent.getParcelableExtra<ModelCustomerListt>("modelCustomerProfile")
        val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
        mViewModel.getAllTransactions(
            auth,
            "2",
            modelCustomerList.customer_id,
            "1",
            "",
            "",
            "customer"
        )
        progressBar.visibility = View.VISIBLE

    }

    private fun setToolbar() {
        toolbar = findViewById(R.id.toolbar)
        toolbartitle = findViewById(R.id.title)
        setSupportActionBar(toolbar)
        title = ""
        toolbartitle.text = "Payments"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)

    }

    private fun createTabs() {
        val modelCustomerList = intent.getParcelableExtra<ModelCustomerListt>("modelCustomerProfile")
        val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
        payments.setOnClickListener {
            payments.setTextColor(resources.getColor(R.color.colorWhite))
            payments.setBackgroundResource(R.drawable.bg_seller_blue1_padding)
            refunds.setTextColor(resources.getColor(R.color.payments))
            cancelled.setTextColor(resources.getColor(R.color.payments))
            refunds.setBackgroundResource(R.drawable.bg_seller_grey_nonselected)
            cancelled.setBackgroundResource(R.drawable.bg_seller_grey_nonselected)
            mViewModel.getAllTransactions(
                auth,
                "2",
                modelCustomerList.customer_id,
                "1",
                "",
                "",
                "customer"
            )
            progressBar.visibility = View.VISIBLE
        }

        refunds.setOnClickListener {
            payments.setTextColor(resources.getColor(R.color.payments))
            payments.setBackgroundResource(R.drawable.bg_seller_grey_nonselected)
            refunds.setTextColor(resources.getColor(R.color.colorWhite))
            cancelled.setTextColor(resources.getColor(R.color.payments))
            refunds.setBackgroundResource((R.drawable.bg_seller_blue1_padding))
            cancelled.setBackgroundResource(R.drawable.bg_seller_grey_nonselected)
            mViewModel.getAllTransactions(
                auth,
                "2",
                modelCustomerList.customer_id,
                "2",
                "",
                "",
                "customer"
            )
            progressBar.visibility = View.VISIBLE

        }

        cancelled.setOnClickListener {
            payments.setTextColor(resources.getColor(R.color.payments))
            payments.setBackgroundResource(R.drawable.bg_seller_grey_nonselected)
            refunds.setTextColor(resources.getColor(R.color.payments))
            cancelled.setTextColor(resources.getColor(R.color.colorWhite))
            refunds.setBackgroundResource(R.drawable.bg_seller_grey_nonselected)
            cancelled.setBackgroundResource((R.drawable.bg_seller_blue1_padding))
            mViewModel.getAllTransactions(
                auth,
                "2",
                modelCustomerList.customer_id,
                "3",
                "",
                "",
                "customer"
            )
            progressBar.visibility = View.VISIBLE
        }


    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }

        return super.onOptionsItemSelected(item)
    }

}
