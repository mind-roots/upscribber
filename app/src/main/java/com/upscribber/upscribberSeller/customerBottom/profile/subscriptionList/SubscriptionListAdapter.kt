package com.upscribber.upscribberSeller.customerBottom.profile.subscriptionList

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.upscribberSeller.customerBottom.profile.teamMembers.ProfileTeamMembersActivity
import com.upscribber.R
import com.upscribber.databinding.SubscriptionRecyclerListBinding
import kotlinx.android.synthetic.main.subscription_recycler_list.view.*

class SubscriptionListAdapter(var context: Context, var stringExtra: String) :
    RecyclerView.Adapter<SubscriptionListAdapter.ViewHolder>() {

    private lateinit var mBinding: SubscriptionRecyclerListBinding
    private var itemss = ArrayList<SubscriptionListModel>()
    var listener = context as SelectionCustomerr

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        mBinding = SubscriptionRecyclerListBinding.inflate(LayoutInflater.from(context), parent, false)
        return ViewHolder(mBinding)
    }

    override fun getItemCount(): Int {
        return itemss.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = itemss[position]
        holder.bind(model)


        if (model.status) {
            holder.itemView.constraint.setBackgroundResource(R.drawable.bg_seller_blue_outline)
        } else {
            holder.itemView.constraint.setBackgroundResource(0)
        }

        if (stringExtra == "invite" || stringExtra == "inviteList") {
            holder.itemView.publicImage.visibility = View.VISIBLE
            holder.itemView.noOfSubscribers.visibility = View.VISIBLE
            holder.itemView.textView208.visibility = View.GONE
        }

        if (stringExtra == "list" || stringExtra == "selection") {
            holder.itemView.reassignesText.visibility = View.VISIBLE
            holder.itemView.tVAssign.visibility = View.VISIBLE
            holder.itemView.assignName.visibility = View.VISIBLE
            holder.itemView.textView208.visibility = View.VISIBLE
        }

        if (model.discount_price == "0" || model.discount_price == "0.0" ||model.discount_price == "0.00"){
            holder.itemView.tv_off.visibility = View.GONE
        }else{
            holder.itemView.tv_off.visibility = View.GONE
        }



//        if (model.type1 == 1){
//            holder.itemView.textView208.setImageResource(R.drawable.ic_0)
//        }else if (model.type1 == 2){
//            holder.itemView.textView208.setImageResource(R.drawable.ic_1)
//        }else{
//            holder.itemView.textView208.setImageResource(R.drawable.ic_infinity)
//        }


        holder.itemView.setOnClickListener {
            if (stringExtra == "invite") {
               listener.fixIntent(position,model)

            }
//            if(stringExtra == "list"){
//                context.startActivity(Intent(context, ActivitySubscriptionSeller::class.java))
//            }
            if (stringExtra == "inviteList") {
                listener.fixIntent(position,model)

            }
            if (stringExtra == "selection") {
                listener.SelectCustomer(position)

            }

        }


        holder.itemView.reassignesText.setOnClickListener {
            val intent = Intent(context, ProfileTeamMembersActivity::class.java)
            intent.putExtra("notSelected", "SelectionList")
            intent.putExtra("model",model)
            startActivity(context, intent, null)
        }
    }

    class ViewHolder(var binding: SubscriptionRecyclerListBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(obj: SubscriptionListModel) {
            binding.model = obj
            binding.executePendingBindings()
        }
    }

    fun update(items: ArrayList<SubscriptionListModel>) {
        this.itemss = items
        notifyDataSetChanged()
    }

    interface SelectionCustomerr {
        fun SelectCustomer(position: Int)
        fun fixIntent(position: Int,model: SubscriptionListModel)
    }
}