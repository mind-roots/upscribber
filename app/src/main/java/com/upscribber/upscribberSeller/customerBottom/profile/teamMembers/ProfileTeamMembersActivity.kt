package com.upscribber.upscribberSeller.customerBottom.profile.teamMembers

import android.graphics.Outline
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.ViewOutlineProvider
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.databinding.ActivityProfileTeamMembersBinding
import com.upscribber.upscribberSeller.customerBottom.profile.subscriptionList.SubscriptionListModel
import com.upscribber.upscribberSeller.manageTeam.ManageTeamModel
import com.upscribber.upscribberSeller.manageTeam.ManageTeamViewModel
import kotlinx.android.synthetic.main.activity_profile_team_members.*

class ProfileTeamMembersActivity : AppCompatActivity(),
    AdapterSelectTeamMembers.singleSelection {

    private lateinit var mbinding: ActivityProfileTeamMembersBinding
    private lateinit var viewModel: ManageTeamViewModel
    private lateinit var mAdapter: AdapterSelectTeamMembers
    private lateinit var backgroundChange: ArrayList<ManageTeamModel>



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mbinding = DataBindingUtil.setContentView(this, R.layout.activity_profile_team_members)
        mbinding.lifecycleOwner = this
        viewModel = ViewModelProviders.of(this)[ManageTeamViewModel::class.java]
        setToolbar()
        setAdapter()
        apiImplimentation()
        observerInit()

    }

    private fun observerInit() {
        viewModel.getTeamMembersListtt().observe(this, Observer {
            if (it.size > 0) {
                backgroundChange = it
                mAdapter.update(backgroundChange)
            }
        })


        viewModel.getTeamMemberAssign().observe(this, Observer {
            if (it.status == "true"){
                finish()
            }
        })

    }

    private fun apiImplimentation() {
        val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
        viewModel.getTeamMembersList(auth, "1", "", "", "")
    }

    private fun setAdapter() {
        mbinding.recyclerTeam.layoutManager = LinearLayoutManager(this)
        mbinding.recyclerTeam.isNestedScrollingEnabled = false
        mAdapter = AdapterSelectTeamMembers(this)
        mbinding.recyclerTeam.adapter = mAdapter

    }


    fun manageDone(v: View) {
        val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
        val model = intent.getParcelableExtra<SubscriptionListModel>("model")
        var staffId = ""
        for (i in 0 until  backgroundChange.size){
            if (backgroundChange[i].status){
                staffId = backgroundChange[i].staff_id
            }
        }
        viewModel.getTeamMemberAssign(auth,model.order_id,staffId)
    }


    override fun Selection(position: Int) {
        val model = backgroundChange[position]
        if (model.status) {
            model.status = false
        } else {
            for (child in backgroundChange) {
                child.status = false
            }
            model.status = true
        }
        backgroundChange[position] = model
        mAdapter.notifyDataSetChanged()
    }

    private fun setToolbar() {
            mbinding.constraintBackground.visibility = View.GONE
            mbinding.toolbar.visibility = View.VISIBLE
            setSupportActionBar(mbinding.toolbar)
            title = ""
            mbinding.title.text = resources.getString(R.string.Team_Members)
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)

    }

    private fun roundImage() {
        val curveRadius = 50F

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            constraintBackground.outlineProvider = object : ViewOutlineProvider() {

                @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                override fun getOutline(view: View?, outline: Outline?) {
                    outline?.setRoundRect(
                        0,
                        -100,
                        view!!.width,
                        view.height,
                        curveRadius
                    )
                }
            }
            constraintBackground.clipToOutline = true
        }

    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }



}
