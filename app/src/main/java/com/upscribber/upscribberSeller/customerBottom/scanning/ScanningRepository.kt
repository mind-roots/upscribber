package com.upscribber.upscribberSeller.customerBottom.scanning

import android.app.Application
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.upscribber.commonClasses.Constant
import com.upscribber.commonClasses.ModelStatusMsg
import com.upscribber.commonClasses.WebServicesCustomers
import com.upscribber.home.ModelHomeLocation
import com.upscribber.payment.paymentCards.PaymentCardModel
import com.upscribber.profile.ModelGetProfile
import com.upscribber.subsciptions.manageSubscriptions.ModelRedeemCode
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class ScanningRepository(var application: Application) {
    val mDataFailure = MutableLiveData<ModelStatusMsg>()
    val mDataRedeemCodeData = MutableLiveData<ModelGetProfile>()
    val mDataRedeemCode = MutableLiveData<ModelScanBarCode>()
    val mDataRedeemQuantity = MutableLiveData<ModelRedeemCode>()
    val mRetrieveCard = MutableLiveData<ArrayList<PaymentCardModel>>()

    fun getRedeemCode(
        auth: String,
        code: String,
        quantity: String,
        orderId: String,
        type: String
    ) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.redeem(auth, code, quantity, orderId,type)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        val modelScanBarCode = ModelScanBarCode()
                        val arrayLocations = ArrayList<ModelHomeLocation>()
                        if (status == "true") {
                            val data = json.optJSONObject("data")
                            val locations = data.optJSONArray("locations")
                            modelScanBarCode.assign_to = data.optString("assign_to")
                            modelScanBarCode.campaign_name = data.optString("campaign_name")
                            modelScanBarCode.customer_id = data.optString("customer_id")
                            modelScanBarCode.total = data.optString("total_amount")
                            modelScanBarCode.created_at = data.optString("created_at")
                            modelScanBarCode.customer_name = data.optString("customer_name")
                            modelScanBarCode.business_name = data.optString("business_name")
                            modelScanBarCode.team_member = data.optString("team_member")
                            modelScanBarCode.location_id = data.optString("location_id")
                            modelScanBarCode.contact_no = data.optString("contact_no")
                            modelScanBarCode.profile_image = data.optString("profile_image")
                            modelScanBarCode.remaining_visits = data.optString("remaining_visits")

                            for (i in 0 until locations.length()) {
                                val locationData = locations.optJSONObject(i)
                                val modelLocation = ModelHomeLocation()
                                modelLocation.id = locationData.optString("id")
                                modelLocation.business_id = locationData.optString("business_id")
                                modelLocation.user_id = locationData.optString("user_id")
                                modelLocation.street = locationData.optString("street")
                                modelLocation.city = locationData.optString("city")
                                modelLocation.state = locationData.optString("state")
                                modelLocation.zip_code = locationData.optString("zip_code")
                                modelLocation.lat = locationData.optString("lat")
                                modelLocation.long = locationData.optString("long")
                                modelLocation.state_code = locationData.optString("state_code")
                                modelLocation.store_timing = locationData.optString("store_timing")
                                modelLocation.created_at = locationData.optString("created_at")
                                modelLocation.updated_at = locationData.optString("updated_at")
                                arrayLocations.add(modelLocation)
                            }
                            modelScanBarCode.locations = arrayLocations
                            modelScanBarCode.status = status
                            modelScanBarCode.msg = msg

                            mDataRedeemCode.value = modelScanBarCode
                        } else {
                            modelStatus.status = status
                            modelStatus.msg = msg
                            mDataFailure.value = modelStatus
                        }

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus
            }
        })
    }

    fun getmDataFailure(): LiveData<ModelStatusMsg> {
        return mDataFailure
    }


     fun getmDataRedeemCodeData(): LiveData<ModelGetProfile> {
        return mDataRedeemCodeData
    }


    fun getmScanBarCode(): LiveData<ModelScanBarCode> {
        return mDataRedeemCode
    }

    fun getBarCodeQuantity(auth: String, code: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.getBarcodeQty(auth, code)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        val arrayLocation = ArrayList<ModelHomeLocation>()

                        if (status == "true") {
                            val modelRedeemCode = ModelRedeemCode()
                            val data = json.optJSONObject("data")
                            val code = data.optJSONObject("code")
                            val redeemInfo = data.optJSONObject("redeemInfo")
                            val locations = redeemInfo.optJSONArray("locations")
                            modelRedeemCode.id = code.optString("id")
                            modelRedeemCode.order_id = code.optString("order_id")
                            modelRedeemCode.quantity = code.optString("quantity")
                            modelRedeemCode.created_date = code.optString("created_date")
                            modelRedeemCode.code = code.optString("code")
                            modelRedeemCode.user_id = code.optString("user_id")
                            modelRedeemCode.frequency_type = redeemInfo.optString("frequency_type")
                            modelRedeemCode.frequency_value = redeemInfo.optString("frequency_value")
                            modelRedeemCode.unit = redeemInfo.optString("unit")
                            modelRedeemCode.redeemtion_cycle_qty = redeemInfo.optString("redeemtion_cycle_qty")
                            modelRedeemCode.assign_to = redeemInfo.optString("assign_to")
                            modelRedeemCode.price = redeemInfo.optString("price")
                            modelRedeemCode.campaign_name = redeemInfo.optString("campaign_name")
                            modelRedeemCode.customer_id = redeemInfo.optString("customer_id")
                            modelRedeemCode.total_amount = redeemInfo.optString("total_amount")
                            modelRedeemCode.customer_name = redeemInfo.optString("customer_name")
                            modelRedeemCode.business_name = redeemInfo.optString("business_name")
                            modelRedeemCode.team_member = redeemInfo.optString("team_member")
                            modelRedeemCode.location_id = redeemInfo.optString("location_id")
                            modelRedeemCode.contact_no = redeemInfo.optString("contact_no")
                            modelRedeemCode.profile_image = redeemInfo.optString("profile_image")
                            modelRedeemCode.created_at = redeemInfo.optString("created_at")
                            modelRedeemCode.remaining_visits = redeemInfo.optString("remaining_visits")

                            for (i in 0 until locations.length()) {
                                val locationData = locations.getJSONObject(i)
                                val modelLocation = ModelHomeLocation()
                                modelLocation.id = locationData.optString("id")
                                modelLocation.business_id = locationData.optString("business_id")
                                modelLocation.user_id = locationData.optString("user_id")
                                modelLocation.street = locationData.optString("street")
                                modelLocation.city = locationData.optString("city")
                                modelLocation.state = locationData.optString("state")
                                modelLocation.zip_code = locationData.optString("zip_code")
                                modelLocation.lat = locationData.optString("lat")
                                modelLocation.long = locationData.optString("long")
                                modelLocation.store_timing = locationData.optString("store_timing")
                                modelLocation.created_at = locationData.optString("created_at")
                                modelLocation.updated_at = locationData.optString("updated_at")
                                arrayLocation.add(modelLocation)
                            }
                            modelRedeemCode.locations = arrayLocation
                            modelRedeemCode.status = status
                            modelRedeemCode.msg = msg
                            mDataRedeemQuantity.value = modelRedeemCode



                        } else {
                            modelStatus.status = status
                            modelStatus.msg = msg
                            mDataFailure.value = modelStatus
                        }

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus
            }
        })
    }

    fun getmDataRedeemQuantity(): LiveData<ModelRedeemCode> {
        return mDataRedeemQuantity
    }

    fun getReddeemCodeCustomers(
        auth: String,
        quantity: String,
        orderId: String,
        type: String,
        customerId: String
    ) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.redeemCustomer(auth , quantity, orderId,type ,customerId)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        val modelScanBarCode = ModelScanBarCode()
                        val arrayLocations = ArrayList<ModelHomeLocation>()
                        if (status == "true") {
                            val data = json.optJSONObject("data")
                            val locations = data.optJSONArray("locations")
                            modelScanBarCode.assign_to = data.optString("assign_to")
                            modelScanBarCode.campaign_name = data.optString("campaign_name")
                            modelScanBarCode.customer_id = data.optString("customer_id")
                            modelScanBarCode.total = data.optString("total_amount")
                            modelScanBarCode.created_at = data.optString("created_at")
                            modelScanBarCode.customer_name = data.optString("customer_name")
                            modelScanBarCode.business_name = data.optString("business_name")
                            modelScanBarCode.team_member = data.optString("team_member")
                            modelScanBarCode.location_id = data.optString("location_id")
                            modelScanBarCode.contact_no = data.optString("contact_no")
                            modelScanBarCode.profile_image = data.optString("profile_image")
                            modelScanBarCode.remaining_visits = data.optString("remaining_visits")
                            modelScanBarCode.order_id = data.optString("order_id")
                            modelScanBarCode.frequency_type = data.optString("frequency_type")
                            modelScanBarCode.frequency_value = data.optString("frequency_value")
                            modelScanBarCode.unit = data.optString("unit")
                            modelScanBarCode.redeemtion_cycle_qty = data.optString("redeemtion_cycle_qty")
                            modelScanBarCode.price = data.optString("price")
                            modelScanBarCode.description = data.optString("description")
                            modelScanBarCode.business_id = data.optString("business_id")
                            modelScanBarCode.business_phone = data.optString("business_phone")
                            modelScanBarCode.logo = data.optString("logo")
                            modelScanBarCode.merchant_id = data.optString("merchant_id")
                            modelScanBarCode.redeem_id = data.optString("redeem_id")


                            for (i in 0 until locations.length()) {
                                val locationData = locations.optJSONObject(i)
                                val modelLocation = ModelHomeLocation()
                                modelLocation.id = locationData.optString("id")
                                modelLocation.business_id = locationData.optString("business_id")
                                modelLocation.user_id = locationData.optString("user_id")
                                modelLocation.street = locationData.optString("street")
                                modelLocation.city = locationData.optString("city")
                                modelLocation.state = locationData.optString("state")
                                modelLocation.zip_code = locationData.optString("zip_code")
                                modelLocation.lat = locationData.optString("lat")
                                modelLocation.long = locationData.optString("long")
                                modelLocation.store_timing = locationData.optString("store_timing")
                                modelLocation.created_at = locationData.optString("created_at")
                                modelLocation.updated_at = locationData.optString("updated_at")
                                arrayLocations.add(modelLocation)
                            }
                            modelScanBarCode.locations = arrayLocations
                            modelScanBarCode.status = status
                            modelScanBarCode.msg = msg

                            mDataRedeemCode.value = modelScanBarCode
                        } else {
                            modelStatus.status = status
                            modelStatus.msg = msg
                            mDataFailure.value = modelStatus
                        }

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus
            }
        })

    }

    fun getReddemCustomers(code: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.getUserIdByCode(code)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        val modelSignUp = ModelGetProfile()
                        if (status == "true") {
                            val data = json.getJSONObject("data")
                            val user_details = data.getJSONObject("user_details")
                            val primary_address = user_details.optJSONObject("primary_address")
                            val billing_address = user_details.optJSONObject("billing_address")
                            modelSignUp.profile_image = user_details.optString("profile_image")
                            modelSignUp.zip_code = user_details.optString("zip_code")
                            modelSignUp.name = user_details.optString("name")
                            modelSignUp.contact_no = user_details.optString("contact_no")
                            modelSignUp.email = user_details.optString("email")
                            modelSignUp.gender = user_details.optString("gender")
                            modelSignUp.stripe_customer_id = user_details.optString("stripe_customer_id")
                            modelSignUp.birthdate = user_details.optString("birthdate")
                            modelSignUp.last_name = user_details.optString("last_name")
                            modelSignUp.is_merchant = user_details.optString("is_merchant")
                            modelSignUp.steps = user_details.optString("steps")
                            modelSignUp.business_id = user_details.optString("business_id")
                            modelSignUp.tax = user_details.optInt("tax")
                            modelSignUp.merchant_commission = user_details.optInt("merchant_commission")
                            modelSignUp.wallet = user_details.optString("wallet")
                            modelSignUp.type = user_details.optString("type")
                            modelSignUp.social_id = user_details.optString("social_id")
                            modelSignUp.feedback = user_details.optString("feedback")
                            modelSignUp.invite_code = user_details.optString("invite_code")
                            modelSignUp.reg_type = user_details.optString("reg_type")
                            modelSignUp.email_verify = user_details.optString("email_verify")
                            modelSignUp.user_id = user_details.optString("user_id")
                            modelSignUp.primary_address.street = primary_address.optString("street")
                            modelSignUp.primary_address.city = primary_address.optString("city")
                            modelSignUp.primary_address.state = primary_address.optString("state")
                            modelSignUp.primary_address.zip_code = primary_address.optString("zip_code")
                            modelSignUp.billing_address.billing_street = billing_address.optString("billing_street")
                            modelSignUp.billing_address.billing_city = billing_address.optString("billing_city")
                            modelSignUp.billing_address.billing_state = billing_address.optString("billing_state")
                            modelSignUp.billing_address.billing_zipcode = billing_address.optString("billing_zipcode")
                            modelSignUp.msg = msg
                            modelSignUp.status = status
                            modelStatus.status = status
                            modelStatus.msg = msg

                            val editor = Constant.getPrefs(application).edit()
                            editor.putString(Constant.user_id, modelSignUp.user_id)
                            editor.putString(Constant.stripe_new_customer_from_merchant, modelSignUp.stripe_customer_id)
                            editor.apply()

//                            getRetrieveCards(modelSignUp.stripe_customer_id)
                            mDataRedeemCodeData.value = modelSignUp
                        } else {
                            modelStatus.status = status
                            modelStatus.msg = msg
                            mDataFailure.value = modelStatus
                        }

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus
            }
        })

    }

//    private fun getRetrieveCards(stripeCustomerId: String) {
//        val retrofit = Retrofit.Builder()
//            .baseUrl(Constant.stripeBaseUrl)
//            .build()
//
//        val service = retrofit.create(WebServicesCustomers::class.java)
//        val call: Call<ResponseBody> =
//            service.stripeRetrieve(stripeCustomerId, "100")
//        call.enqueue(object : Callback<ResponseBody> {
//
//
//            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
//
//                if (response.isSuccessful) {
//                    try {
//                        val res = response.body()!!.string()
//                        val json = JSONObject(res)
//
//                        val data = json.optJSONArray("data")
//
//                        Constant.cardArrayListDataMerchant.clear()
//
//                        for (i in 0 until data.length()) {
//                            val stripeData = data.getJSONObject(i)
//                            val cardModel = PaymentCardModel()
//                            cardModel.id = stripeData.optString("id")
//                            cardModel.brand = stripeData.optString("brand")
//                            cardModel.country = stripeData.optString("country")
//                            cardModel.last4 = stripeData.optString("last4")
//                            cardModel.account_holder_name = stripeData.optString("account_holder_name")
//                            cardModel.name = stripeData.optString("name")
//                            Constant.cardArrayListDataMerchant.add(cardModel)
//                            val editor = Constant.getPrefs(application).edit()
//                            editor.putString(Constant.stripe_customer_name, cardModel.name)
//                            editor.apply()
//                        }
//
//                        mRetrieveCard.value = Constant.cardArrayListDataMerchant
//
//                    } catch (e: Exception) {
//                        e.printStackTrace()
//                    }
//
//                }
//            }
//
//            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
//
//                Toast.makeText(application, "Error!", Toast.LENGTH_LONG).show()
//            }
//        })
//
//    }



}