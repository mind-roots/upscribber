package com.upscribber.upscribberSeller.customerBottom.customerList

import android.os.Parcel
import android.os.Parcelable

class ModelCustomerListt() : Parcelable {

    var customer_name: String = ""
    var customer_id: String = ""
    var contact_no: String = ""
    var profile_image: String = ""
    var remaining_visits: String = ""


     var customerName : String = ""
    var customerPhone : String = ""
    var customerImage : Int = 0

    constructor(parcel: Parcel) : this() {
        customer_name = parcel.readString()
        customer_id = parcel.readString()
        contact_no = parcel.readString()
        profile_image = parcel.readString()
        remaining_visits = parcel.readString()
        customerName = parcel.readString()
        customerPhone = parcel.readString()
        customerImage = parcel.readInt()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(customer_name)
        parcel.writeString(customer_id)
        parcel.writeString(contact_no)
        parcel.writeString(profile_image)
        parcel.writeString(remaining_visits)
        parcel.writeString(customerName)
        parcel.writeString(customerPhone)
        parcel.writeInt(customerImage)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ModelCustomerListt> {
        override fun createFromParcel(parcel: Parcel): ModelCustomerListt {
            return ModelCustomerListt(parcel)
        }

        override fun newArray(size: Int): Array<ModelCustomerListt?> {
            return arrayOfNulls(size)
        }
    }

}
