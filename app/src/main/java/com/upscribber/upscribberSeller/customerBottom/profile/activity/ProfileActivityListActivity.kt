package com.upscribber.upscribberSeller.customerBottom.profile.activity

import android.content.Intent
import android.graphics.Outline
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.ViewOutlineProvider
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.upscribber.commonClasses.Constant
import com.upscribber.R
import com.upscribber.databinding.ActivityProfileListBinding
import com.upscribber.filters.*
import com.upscribber.filters.Redemption.RedemptionFilterFragment
import com.upscribber.notification.notifications.NotificationViewModel
import com.upscribber.upscribberSeller.accountLinking.LinkingRequestActivity
import com.upscribber.upscribberSeller.manageTeam.AddMemberActivity
import kotlinx.android.synthetic.main.activity_discover.*
import kotlinx.android.synthetic.main.filter_header.*

class ProfileActivityListActivity : AppCompatActivity(), FilterInterface,
    DrawerLayout.DrawerListener, FilterFragment.SetFilterHeaderBackground,
    ActivityAdapter.RequestsLinking {

    private lateinit var mbinding: ActivityProfileListBinding
    private lateinit var viewModel: ActivityViewModel
    private lateinit var mAdapter: ActivityAdapter
    private lateinit var filter_done: TextView
    private lateinit var fragmentManager: FragmentManager
    private lateinit var fragmentTransaction: FragmentTransaction
    private lateinit var mreset: TextView
    lateinit var mViewModel: NotificationViewModel
    private lateinit var mfilters: TextView
    var drawerOpen = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mbinding = DataBindingUtil.setContentView(this, R.layout.activity_profile_list)
        mViewModel = ViewModelProviders.of(this)[NotificationViewModel::class.java]
        mbinding.lifecycleOwner = this
        initz()
        setToolbar()
        setAdapter()
        clickListeners()
        apiImplimentation()
        observerInit()
        mbinding.drawerLayout1.addDrawerListener(this)
    }

    private fun observerInit() {
        viewModel.getStatus().observe(this, Observer {
            mbinding.progressBar.visibility = View.GONE
            if (it.msg == "Invalid auth code") {
                Constant.commonAlert(this)
            } else {
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
            }
        })


        viewModel.getAllActivities().observe(this, Observer {
            mbinding.progressBar.visibility = View.GONE
            if (it.size > 0) {
                mbinding.noDataText.visibility = View.GONE
                mbinding.imgNoActivities.visibility = View.GONE
                mbinding.noDataDescription.visibility = View.GONE
                mAdapter.update(it)
            } else {
                mbinding.noDataText.visibility = View.VISIBLE
                mbinding.imgNoActivities.visibility = View.VISIBLE
                mbinding.noDataDescription.visibility = View.VISIBLE
                if (intent.hasExtra("merchantCustomers")) {
                    mbinding.noDataDescription.text = "Activities will show up once your customer make any actions related to their subscription."
                }else{
                    mbinding.noDataDescription.text = "Activities will show up once you make any actions related to your subscription."
                }


            }
        })

        mViewModel.mDataNotifications().observe(this, Observer {
            if (it.status == "true") {
                startActivity(
                    Intent(this, LinkingRequestActivity::class.java)
                        .putExtra("individualList", "other")
                        .putExtra("fromNotification", "fromNotification")
                        .putExtra("model", it)
                )
            }
        })


    }


    override fun linkingCustomers(model: ActivityModel) {
        if (model.model.typeLinking == "1") {
            mViewModel.getBusinessLinkInfo(
                model.type,
                model.model.business_id,
                model.id,
                ""
            )
        } else if (model.model.typeLinking == "2") {
            startActivity(Intent(this, AddMemberActivity::class.java).putExtra("modelActivity", model))

        }


    }

    private fun apiImplimentation() {
        val auth = Constant.getPrefs(application).getString(Constant.auth_code, "")
        if (intent.hasExtra("merchantCustomers")) {
            viewModel.getActivityData(auth, "2", "")
        } else {
            viewModel.getActivityData(auth, "1", "")
        }

        mbinding.progressBar.visibility = View.VISIBLE

    }

    private fun clickListeners() {
        filter_done.setOnClickListener {
            onBackPressed()
        }

        mbinding.btnFilter.setOnClickListener {
            drawerOpen = 1
            Constant.hideKeyboard(this, mbinding.search)
            mbinding.search.setQuery("", false)
            mbinding.search.clearFocus()
            mbinding.drawerLayout1.openDrawer(GravityCompat.END)
            inflateFragment(FilterFragment(this))
            filterImgBack.visibility = View.GONE
            mfilters.text = "Filters"
            mreset.visibility = View.VISIBLE

        }

    }

    private fun initz() {
        viewModel = ViewModelProviders.of(this)[ActivityViewModel::class.java]
        mreset = findViewById(R.id.reset)
        mfilters = findViewById(R.id.filters)
        filter_done = findViewById(R.id.filter_done)
    }

    private fun setToolbar() {
        setSupportActionBar(mbinding.toolbar.toolbar)
        title = ""
        mbinding.toolbar.title.text = "Activity"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)


    }


    private fun roundImage() {

        val curveRadius = 50F

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            mbinding.constraintBackground.outlineProvider = object : ViewOutlineProvider() {

                @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                override fun getOutline(view: View?, outline: Outline?) {
                    outline?.setRoundRect(
                        0,
                        -100,
                        view!!.width,
                        view.height,
                        curveRadius
                    )
                }
            }
            mbinding.constraintBackground.clipToOutline = true
        }
    }

    private fun setAdapter() {

        mbinding.recyclerActivityList.layoutManager = LinearLayoutManager(this)
        mAdapter = ActivityAdapter(this, this)
        mbinding.recyclerActivityList.adapter = mAdapter

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        if (drawerOpen == 1) {
            drawerOpen = 0
            mbinding.drawerLayout1.closeDrawer(GravityCompat.END)
        } else {
            super.onBackPressed()
        }
    }

    override fun onResume() {
        super.onResume()
        Constant.hideKeyboard(this, mbinding.search)
    }


    override fun nextFragments(i: Int) {
        when (i) {
            1 -> {
                inflateFragment(SortByFragment())
                mreset.visibility = View.GONE
                filterImgBack.visibility = View.VISIBLE
                filters.text = "Sort By"

                filterImgBack.setOnClickListener {
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"
                }

                filter_done.setOnClickListener {
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"
                }
            }


            2 -> {
                inflateFragment(CategoryFilterFragment(this))
                mreset.visibility = View.GONE
                filterImgBack.visibility = View.VISIBLE
                filters.text = "Category"

                filterImgBack.setOnClickListener {
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"

                }
                filter_done.setOnClickListener {
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"
                }
            }


            3 -> {
                inflateFragment(BrandsFilterFragment())
                mreset.visibility = View.GONE
                filterImgBack.visibility = View.VISIBLE
                filters.text = "Brands"

                filterImgBack.setOnClickListener {
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"

                }

                filter_done.setOnClickListener {
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"
                }
            }
            4 -> {
                inflateFragment(RedemptionFilterFragment())
                mreset.visibility = View.GONE
                filterImgBack.visibility = View.VISIBLE
                filters.text = "Redemption"

                filterImgBack.setOnClickListener {
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"

                }
                filter_done.setOnClickListener {
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"
                }
            }
        }

    }

    override fun onDrawerStateChanged(newState: Int) {

    }

    override fun onDrawerSlide(drawerView: View, slideOffset: Float) {

    }

    override fun onDrawerClosed(drawerView: View) {
        drawerOpen = 0
    }

    override fun onDrawerOpened(drawerView: View) {
        drawerOpen = 1
        drawerLayout1.openDrawer(GravityCompat.END)
        inflateFragment(FilterFragment(this))
        filterImgBack.visibility = View.GONE
        mfilters.text = "Filters"
        mreset.visibility = View.VISIBLE

    }

    private fun inflateFragment(fragment: Fragment) {
        fragmentManager = supportFragmentManager
        this.fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.containerSide1, fragment)
        fragmentTransaction.commit()
    }


    override fun filterHeaderBackground(s: String) {
        if (s == "new") {
            filterHeader.setBackgroundDrawable(getDrawable(R.drawable.filter_header_select_category))


        } else {
            filterHeader.setBackgroundDrawable(getDrawable(R.drawable.filter_header))

        }
    }
}
