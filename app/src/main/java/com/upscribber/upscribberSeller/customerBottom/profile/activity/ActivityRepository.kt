package com.upscribber.upscribberSeller.customerBottom.profile.activity

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.upscribber.commonClasses.Constant
import com.upscribber.commonClasses.ModelStatusMsg
import com.upscribber.commonClasses.WebServicesCustomers
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class ActivityRepository(application: Application) {

    private val mDataActivities = MutableLiveData<ArrayList<ActivityModel>>()
    val mDataFailure = MutableLiveData<ModelStatusMsg>()

    fun getActivityData(auth: String, type: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.getActivity(auth, type, "")
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status1 = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        val arrayAllActivity = ArrayList<ActivityModel>()
                        if (status1 == "true") {
                            val data = json.optJSONArray("data")
                            for (i in 0 until data.length()) {
                                val activityData = data.optJSONObject(i)
                                val modelActivity = ActivityModel()
                                modelActivity.id = activityData.optString("id")
                                modelActivity.msg = activityData.optString("msg")
                                modelActivity.recipient_id = activityData.optString("recipient_id")
                                modelActivity.sender_id = activityData.optString("sender_id")
                                modelActivity.activity_type = activityData.optString("activity_type")
                                modelActivity.type = activityData.optString("type")
                                modelActivity.sub_type = activityData.optString("sub_type")
                                modelActivity.reference_id = activityData.optString("reference_id")
                                modelActivity.action_taken = activityData.optString("action_taken")
                                modelActivity.created_at = activityData.optString("created_at")
                                modelActivity.updated_at = activityData.optString("updated_at")
                                val data1 = activityData.optJSONObject("data")
                                val modelActivityData = ModelActivityData()
                                modelActivityData.redeemtion_cycle_qty = data1.optString("redeemtion_cycle_qty")
                                modelActivityData.order_id = data1.optString("order_id")
                                modelActivityData.customer_name = data1.optString("customer_name")
                                modelActivityData.campaign_name = data1.optString("campaign_name")
                                modelActivityData.assign_to = data1.optString("assign_to")
                                modelActivityData.frequency_type = data1.optString("frequency_type")
                                modelActivityData.frequency_value = data1.optString("frequency_value")
                                modelActivityData.order_id = data1.optString("order_id")
                                modelActivityData.unit = data1.optString("unit")
                                modelActivityData.total_amount = data1.optString("total_amount")
                                modelActivityData.merchant_name = data1.optString("merchant_name")
                                modelActivityData.price = data1.optString("price")
                                modelActivityData.free_trial = data1.optString("free_trial")
                                modelActivityData.business_name = data1.optString("business_name")
                                modelActivityData.created_at = data1.optString("created_at")
                                modelActivityData.description = data1.optString("description")
                                modelActivityData.customer_id = data1.optString("customer_id")
                                modelActivityData.created_at = data1.optString("created_at")
                                modelActivityData.contact_no = data1.optString("contact_no")
                                modelActivityData.location_id = data1.optString("location_id")
                                modelActivityData.profile_image = data1.optString("profile_image")
                                modelActivityData.remaining_visits = data1.optString("remaining_visits")
                                modelActivityData.description = data1.optString("description")
                                modelActivityData.business_id = data1.optString("business_id")
                                modelActivityData.business_phone = data1.optString("business_phone")
                                modelActivityData.logo = data1.optString("logo")
                                modelActivityData.typeLinking = data1.optString("type")
                                modelActivityData.merchant_id = data1.optString("merchant_id")
                                modelActivityData.team_member = data1.optString("team_member")
                                modelActivityData.redeem_id = data1.optString("redeem_id")
                                modelActivityData.transaction_id = data1.optString("transaction_id")
                                modelActivityData.cancel_reason = data1.optString("cancel_reason")
                                modelActivity.model = modelActivityData
                                arrayAllActivity.add(modelActivity)


                            }

                            modelStatus.status = status1
                            modelStatus.msg = msg
                            mDataActivities.value = arrayAllActivity
                        }else{
                            modelStatus.status = status1
                            mDataFailure.value = modelStatus
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus

            }
        })
    }

    fun getAllActivities(): LiveData<ArrayList<ActivityModel>> {
        return mDataActivities
    }


    fun getmAllStatus(): LiveData<ModelStatusMsg> {
        return mDataFailure
    }



}