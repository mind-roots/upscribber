package com.upscribber.upscribberSeller.customerBottom.profile

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.upscribber.upscribberSeller.customerBottom.profile.activity.ProfileActivityListActivity
import com.upscribber.upscribberSeller.customerBottom.profile.subscriptionList.SubscriptionListActivity
import com.upscribber.upscribberSeller.navigationCustomer.CustomerSellerActivity
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.profile.ModelGetProfile
import com.upscribber.upscribberSeller.customerBottom.customerBottomViewModel
import com.upscribber.upscribberSeller.customerBottom.customerList.ModelCustomerListt
import com.upscribber.upscribberSeller.customerBottom.profile.payments.PaymentsParticularCustomer
import com.upscribber.upscribberSeller.customerBottom.scanning.customerlistselection.ModelScanCustomers
import com.upscribber.upscribberSeller.manageTeam.ManageTeamModel
import kotlinx.android.synthetic.main.activity_customer_profile_info.*
import kotlinx.android.synthetic.main.activity_customer_profile_info.address
import kotlinx.android.synthetic.main.address_alert.*
import java.math.BigDecimal
import java.math.RoundingMode

@Suppress(
    "NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS",
    "RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS"
)
class CustomerProfileInfoActivity : AppCompatActivity() {

    lateinit var toolbar: Toolbar
    lateinit var toolbarTitle: TextView
    var position = 0
    lateinit var mViewModel: customerBottomViewModel
    lateinit var model: ModelGetProfile
    private var addressShow = ""


    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_customer_profile_info)
        mViewModel = ViewModelProviders.of(this)[customerBottomViewModel::class.java]
        initialization()

        if (intent.hasExtra("notSelected")) {
            constraintStats.visibility = View.VISIBLE
            activityLayout.visibility = View.GONE
            space1.visibility = View.GONE
            view.visibility = View.GONE
            space2.visibility = View.GONE
            customers.visibility = View.VISIBLE
            payTrans.visibility = View.GONE
            manageDone.visibility = View.GONE
            manageDone.text = "Edit"
            tvAccount.text = "Address"
            tvAddress.text = "Merchant Address"
            view1.visibility = View.GONE
            space.visibility = View.GONE
            tvEmail.visibility = View.GONE
            etEmail.visibility = View.GONE
            space3.visibility = View.GONE
            vieww.visibility = View.GONE
            space4.visibility = View.GONE
            tvPhone.visibility = View.GONE
            eTPhone.visibility = View.GONE
            space5.visibility = View.GONE
            space6.visibility = View.GONE
            tVSpecialities.visibility = View.VISIBLE
            tvSpeciality.visibility = View.VISIBLE
        }

        when {
            intent.hasExtra("contractor") -> textView351.text = "Created Subscriptions"
            intent.hasExtra("staff") -> textView351.text = "Subscriptions"
            else -> textView351.text = "Activate Subscriptions"
        }

        setToolbar()
        clicks()
        apiInitialization()
        observerInit()
    }

    private fun apiInitialization() {
        val modelCustomerProfile = intent.getParcelableExtra<ModelCustomerListt>("modelCustomerProfile")
        val modelCustomerList = intent.getParcelableExtra<ModelScanCustomers>("modelList")
        val modelCustomer = intent.getParcelableExtra<ManageTeamModel>("modelCustomer")
        val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
        val type = Constant.getPrefs(this).getString(Constant.type, "")
        progressBar.visibility = View.VISIBLE
        if (intent.hasExtra("staff")) {
            mViewModel.getViewCustomerProfile(
                auth,
                type,
                modelCustomer.type,
                modelCustomer.staff_id
            )
        } else if (intent.hasExtra("contractor")) {
            mViewModel.getViewCustomerProfile(
                auth,
                type,
                modelCustomer.type,
                modelCustomer.staff_id
            )
        } else if (intent.hasExtra("customerList")) {
            mViewModel.getViewCustomerProfile(auth, type, "1", modelCustomerList.customer_id)
        } else if (intent.hasExtra("customers")) {
            mViewModel.getViewCustomerProfile(auth, type, "1", modelCustomerProfile.customer_id)
        }

    }

    @SuppressLint("SetTextI18n")
    private fun observerInit() {
        val imagePath = Constant.getPrefs(this).getString(Constant.dataImagePath, "")
        mViewModel.getrmDataProfile().observe(this, Observer {
            progressBar.visibility = View.GONE
            var editedRevenue =""
            var revenue = it.revenue
            editedRevenue = if (revenue.contains(".")){
                val splitPos = revenue.split(".")
                if (splitPos[1] == "00" || splitPos[1] == "0") {
                    "$" + splitPos[0]
                }else{
                    "$" + BigDecimal(revenue).setScale(2, RoundingMode.HALF_UP).toString()
                }
            }else{
                "$" + BigDecimal(revenue).setScale(2, RoundingMode.HALF_UP).toString()

            }


            model = it
            if (intent.hasExtra("staff")) {
                val date = Constant.getDatee(it.associated_date)
                textView14.text = it.name
                textView353.text = it.associated_campaign
                textView354.text = editedRevenue
                if (it.speciality == ""){
                    specialities.text = "No speciality added"
                    textView15.text = "No speciality added"
                }else{
                    specialities.text = it.speciality
                    textView15.text = it.speciality
                }

                if (it.address == ""){
                    address.text = "No address added"
                    showAddress.visibility = View.GONE
                }else{
                    showAddress.visibility = View.VISIBLE
                    address.text = it.address
                    addressShow = it.address
                }

                datee.text = date

                Glide.with(this).load(imagePath + it.profile_image)
                    .placeholder(R.mipmap.circular_placeholder)
                    .into(image)
            } else if (intent.hasExtra("contractor")) {
                val date = Constant.getDatee(it.associated_date)
                textView14.text = it.name
                datee.text = date
                textView353.text = it.created_campaign
                textView354.text = editedRevenue
                if (it.speciality == ""){
                    specialities.text = "No speciality added"
                    textView15.text = "No speciality added"
                }else{
                    specialities.text = it.speciality
                    textView15.text = it.speciality
                }

                Glide.with(this).load(imagePath + it.profile_image)
                    .placeholder(R.mipmap.circular_placeholder)
                    .into(image)
                if (it.address == ""){
                    address.text = "No address added"
                    showAddress.visibility = View.GONE
                }else{
                    showAddress.visibility = View.VISIBLE
                    address.text = it.address
                    addressShow = it.address
                }
            } else {
                val date = Constant.getDatee(it.subscriber_since)
                textView14.text = it.name
                datee.text = date
                textView353.text = it.active_campaign
                eTPhone.text = Constant.formatPhoneNumber(it.contact_no)
                textView354.text = editedRevenue
                etEmail.text = it.email
                textView15.text = it.street + "," + it.city + "," + it.state
                Glide.with(this).load(imagePath + it.profile_image)
                    .placeholder(R.mipmap.circular_placeholder)
                    .into(image)

                if (it.address == ""){
                    address.text = "No address added"
                    showAddress.visibility = View.GONE
                }else{
                    showAddress.visibility = View.VISIBLE
                    address.text = it.street + "," + it.city + "," + it.state + "," + it.zip_code
                    addressShow = it.street + "," + it.city + "," + it.state + "," + it.zip_code
                }

            }


        })

        mViewModel.getmDataFailure().observe(this, Observer {
            if (it.msg == "Invalid auth code") {
                Constant.commonAlert(this)
            } else {
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
                finish()
            }
        })
    }

    @SuppressLint("InflateParams")
    private fun clicks() {

        customers.setOnClickListener {
            startActivity(Intent(this, CustomerSellerActivity::class.java))
        }


        showAddress.setOnClickListener {
            val mDialogView = LayoutInflater.from(this).inflate(R.layout.address_alert, null)
            val mBuilder = AlertDialog.Builder(this)
                .setView(mDialogView)
            val mAlertDialog = mBuilder.show()
            mAlertDialog.setCancelable(false)
            mAlertDialog.window.setBackgroundDrawableResource(R.drawable.bg_alert)
            mAlertDialog.addresss.text = addressShow
            mAlertDialog.okBtn.setOnClickListener {
                mAlertDialog.dismiss()
            }

        }



        subscriptions.setOnClickListener {
            if (intent.hasExtra("customerList")){
                val modelCustomerProfile = intent.getParcelableExtra<ModelScanCustomers>("modelList")
                val intent = Intent(this, SubscriptionListActivity::class.java)
                intent.putExtra("subscription", "list").putExtra("profileData", model)
                    .putExtra("modelCustomerProfiles", modelCustomerProfile)
                startActivity(intent)

            }else{
                val modelCustomerProfile = intent.getParcelableExtra<ModelCustomerListt>("modelCustomerProfile")
                val intent = Intent(this, SubscriptionListActivity::class.java)
                intent.putExtra("subscription", "list").putExtra("profileData", model)
                    .putExtra("modelCustomerProfile", modelCustomerProfile)
                startActivity(intent)
            }



        }

        manageDone.setOnClickListener {
            if (intent.hasExtra("modelCustomerProfile")) {
                val modelCustomerProfile =
                    intent.getParcelableExtra<ModelCustomerListt>("modelCustomerProfile")
                val intent = Intent(this, SubscriptionListActivity::class.java)
                intent.putExtra("subscription", "invite")
                    .putExtra("profileData", model)
                    .putExtra("modelCustomerProfile", modelCustomerProfile)
                startActivity(intent)
            } else if (intent.hasExtra("modelList")) {
                val modelCustomerList = intent.getParcelableExtra<ModelScanCustomers>("modelList")
                val intent = Intent(this, SubscriptionListActivity::class.java)
                intent.putExtra("subscription", "invite")
                    .putExtra("profileData", model)
                    .putExtra("modelList", modelCustomerList)
                startActivity(intent)
            }

        }

        activityLayout.setOnClickListener {
            startActivity(
                Intent(
                    this,
                    ProfileActivityListActivity::class.java
                ).putExtra("merchantCustomers", "customerProfile")
            )
        }


        payTrans.setOnClickListener {
            if (intent.hasExtra("modelCustomerProfile")) {
                val modelCustomerList = intent.getParcelableExtra<ModelCustomerListt>("modelCustomerProfile")
                startActivity(
                    Intent(this, PaymentsParticularCustomer::class.java).putExtra(
                        "navigation",
                        "deatils"
                    )
                        .putExtra("modelCustomerProfile", modelCustomerList)
                )
            }

        }

    }


    private fun initialization() {
        toolbar = findViewById(R.id.toolbar)
        toolbarTitle = findViewById(R.id.title1)

    }

    @SuppressLint("SetTextI18n")
    private fun setToolbar() {
        setSupportActionBar(toolbar)
        title = ""

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)

        when {
            intent.hasExtra("staff") -> toolbarTitle.text = "Staff"
            intent.hasExtra("contractor") -> toolbarTitle.text = "Contractor"
            else -> toolbarTitle.text = "Profile"
        }

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }


    override fun onResume() {
        super.onResume()
        val sharedPreferences1 = Constant.getPrefs(this)
        if (sharedPreferences1.getString("list", "").isNotEmpty()) {
            val editor = sharedPreferences1.edit()
            editor.remove("list")
            editor.apply()
            finish()
        }


    }

}
