package com.upscribber.upscribberSeller.customerBottom.profile.teamMembers

import android.app.Application
import androidx.lifecycle.AndroidViewModel

class ProfileTeamViewModel (application: Application) : AndroidViewModel(application) {

    var profileTeamRepository : ProfileTeamRepository =
        ProfileTeamRepository(application)

    fun getTeamMembers(): List<ModelSelectTeamMembers> {
        return  profileTeamRepository.getMembers()

    }

}