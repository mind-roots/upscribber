package com.upscribber.upscribberSeller.customerBottom.profile.activity

import android.os.Parcel
import android.os.Parcelable


class ActivityModel() : Parcelable {

    var sender_id: String = ""
    var tab: String = ""
    var action_taken: String = ""
    var sub_type: String = ""
    var updated_at: String = ""
    var created_at: String = ""
    var reference_id: String = ""
    var type: String = ""
    var activity_type: String = ""
    var recipient_id: String = ""
    var msg: String = ""
    var id: String = ""
    var status : Boolean = false
    var model :  ModelActivityData = ModelActivityData()

    constructor(parcel: Parcel) : this() {
        sender_id = parcel.readString()
        tab = parcel.readString()
        action_taken = parcel.readString()
        sub_type = parcel.readString()
        updated_at = parcel.readString()
        created_at = parcel.readString()
        reference_id = parcel.readString()
        type = parcel.readString()
        activity_type = parcel.readString()
        recipient_id = parcel.readString()
        msg = parcel.readString()
        id = parcel.readString()
        status = parcel.readByte() != 0.toByte()
        model = parcel.readParcelable(ModelActivityData::class.java.classLoader)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(sender_id)
        parcel.writeString(tab)
        parcel.writeString(action_taken)
        parcel.writeString(sub_type)
        parcel.writeString(updated_at)
        parcel.writeString(created_at)
        parcel.writeString(reference_id)
        parcel.writeString(type)
        parcel.writeString(activity_type)
        parcel.writeString(recipient_id)
        parcel.writeString(msg)
        parcel.writeString(id)
        parcel.writeByte(if (status) 1 else 0)
        parcel.writeParcelable(model, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ActivityModel> {
        override fun createFromParcel(parcel: Parcel): ActivityModel {
            return ActivityModel(parcel)
        }

        override fun newArray(size: Int): Array<ActivityModel?> {
            return arrayOfNulls(size)
        }
    }


}