package com.upscribber.upscribberSeller.reportingDetail.productPerformance

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.databinding.LayoutReportingProductsBinding

class ProductPerfAdapter(var context: Context) : RecyclerView.Adapter<ProductPerfAdapter.MyViewHolder>() {

    private lateinit var binding: LayoutReportingProductsBinding
    private  var items = ArrayList<ProductPerfModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        binding = LayoutReportingProductsBinding.inflate(LayoutInflater.from(context), parent,false)
        return MyViewHolder(binding)


    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = items[position]
        holder.bind(model)
    }

    class MyViewHolder(var  binding: LayoutReportingProductsBinding): RecyclerView.ViewHolder(binding.root) {

        fun bind(obj: ProductPerfModel) {
            binding.model = obj
            binding.executePendingBindings()
        }
    }

    fun updateArray(items: ArrayList<ProductPerfModel>) {
        this.items = items
        notifyDataSetChanged()
    }

}