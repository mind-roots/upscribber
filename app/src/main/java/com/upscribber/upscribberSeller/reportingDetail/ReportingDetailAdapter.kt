package com.upscribber.upscribberSeller.reportingDetail

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.R
import com.upscribber.databinding.ReportingDetailItemBinding
import kotlinx.android.synthetic.main.reporting_detail_item.view.*


class ReportingDetailAdapter(var context: Context) : RecyclerView.Adapter<ReportingDetailAdapter.ViewHolder>() {

    lateinit var mBinding: ReportingDetailItemBinding
    var listener = context as ReportingClickListener
    var mData = ArrayList<ModelOptions>()
    var selectedData = ArrayList<ModelOptions>()
    private var lastPosition: Int = -1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(context).inflate(R.layout.reporting_detail_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mData.count()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = mData[position]
        holder.itemView.logo.setImageResource(model.optionIcon)
        holder.itemView.title.text = model.optionName
        holder.itemView.value.text = model.optionValue
        holder.itemView.rightLogo.setImageResource(model.optionGraph)

        setAnimation(holder.itemView, position)

        holder.itemView.setOnClickListener {

            val anim = AnimationUtils.loadAnimation(
                context, R.anim.abc_slide_out_top
            )
            anim.duration = 200
            holder.itemView.startAnimation(anim)
            for (i in 0 until mData.size) {
                if (mData[i].optionName == model.optionName) {
                    selectedData.add(mData[i])
                    // mData.removeAt(i)
                    mData[i] = selectedData[0]
                    selectedData.removeAt(0)
                    notifyItemChanged(i)
                    break
                }
            }

            /*anim.setAnimationListener(object : Animation.AnimationListener {

                var positionSelect = -1

                override fun onAnimationRepeat(p0: Animation?) {

                }

                override fun onAnimationEnd(p0: Animation?) {
                     holder.itemView.visibility = View.GONE
                    *//* for (i in 0 until mData.size) {
                         if (mData[i].optionName == model.optionName) {
                           //  selectedData.add(mData[i])
                             mData.removeAt(i)
                             //mData.add(selectedData[0])
                             //selectedData.removeAt(0)
                            // notifyItemChanged(i)
                             break
                         }
                     }
                     notifyDataSetChanged()*//*

                    *//* for (i in 0 until mData.size) {
                         if (mData[i].optionName == model.optionName) {
                             selectedData.add(mData[i])
                             // mData.removeAt(i)
                             mData[i] = selectedData[0]
                             selectedData.removeAt(0)
                             notifyItemChanged(i)
                             break
                         }
                     }*//*
                    notifyItemChanged(positionSelect)
                    holder.itemView.visibility = View.VISIBLE

                }

                override fun onAnimationStart(p0: Animation?) {
                    for (i in 0 until mData.size) {
                        if (mData[i].optionName == model.optionName) {
                            selectedData.add(mData[i])
                            // mData.removeAt(i)
                            mData[i] = selectedData[0]
                            selectedData.removeAt(0)
                            positionSelect = i
                            break
                        }
                    }
                }

            })*/

             listener.onClick(model)
        }
    }

    fun update(data: ArrayList<ModelOptions>) {
        mData = data
        // notifyDataSetChanged()
        val model = ModelOptions()
        model.optionName = "Sales"
        model.optionValue = "$3,000"
        model.optionIcon = R.drawable.ic_sales
        model.optionGraph = R.drawable.ic_graph_sales
        selectedData.add(model)
        // Notify this adaper after animation
    }

    class ViewHolder(item: View) : RecyclerView.ViewHolder(item)


    private fun setAnimation(viewToAnimate: View, position: Int) {
        // If the bound view wasn't previously displayed on screen, it's animated
         if (position > lastPosition) {
        val animation = AnimationUtils.loadAnimation(context, R.anim.abc_slide_in_bottom)
        animation.duration = 500
        viewToAnimate.startAnimation(animation)

        }
        lastPosition = position
    }

    override fun onViewAttachedToWindow(holder: ViewHolder) {
        super.onViewAttachedToWindow(holder)
        setAnimation(holder.itemView, holder.adapterPosition)
    }

    override fun onViewDetachedFromWindow(holder: ViewHolder) {
        super.onViewDetachedFromWindow(holder)
        holder.itemView.clearAnimation()
    }

}
