package com.upscribber.upscribberSeller.reportingDetail.views

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.formatter.IAxisValueFormatter
import com.github.mikephil.charting.formatter.LargeValueFormatter
import com.github.mikephil.charting.utils.ColorTemplate
import com.google.android.material.tabs.TabLayout
import com.upscribber.R
import com.upscribber.databinding.FragmentReportViewsBinding
import com.upscribber.upscribberSeller.dashboardfragment.reporting.ViewsAdapter
import com.upscribber.upscribberSeller.reportingDetail.openFilters
import com.upscribber.upscribberSeller.reportingDetail.teamPerformance.TeamViewModel
import kotlinx.android.synthetic.main.toolbar_reporting.view.*
import kotlin.math.roundToInt


class ReportViewsFragment : Fragment(), TabLayout.OnTabSelectedListener {

    lateinit var mBinding: FragmentReportViewsBinding
    lateinit var update: openFilters
    private lateinit var mAdapter: ViewsAdapter
    lateinit var mViewModel: TeamViewModel
    var option: Int = 0
    var position1: Int = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_report_views, container, false)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setChart1()
        clickListeners()
        mViewModel = ViewModelProviders.of(this).get(TeamViewModel::class.java)
        mBinding.chartReporting.visibility = View.GONE
        mBinding.chartReporting1.visibility = View.GONE
        mBinding.chartReporting3.visibility = View.GONE
        mBinding.chartReporting2.visibility = View.VISIBLE
        mBinding.tabLayout.tabLayout.addOnTabSelectedListener(this)
    }

    private fun clickListeners() {
        mBinding.filter.setOnClickListener {
            update.setFilters()
        }

        mBinding.viewProduct.setOnClickListener {
            mBinding.viewProduct.visibility = View.GONE
            mBinding.widgetView.visibility = View.GONE
            mBinding.recyclerView.visibility = View.VISIBLE
            mBinding.backButton.visibility = View.VISIBLE
            mBinding.filter.visibility = View.VISIBLE
            mBinding.product1.visibility = View.VISIBLE
            option = 1
            setAdapter()
            when (position1) {
                0 -> setChart1()
                1 -> setChart2()
                2 -> setChart3()
                3 -> setChart4()
            }
        }

        mBinding.widgetView.setOnClickListener {
            mBinding.viewProduct.visibility = View.GONE
            mBinding.widgetView.visibility = View.GONE
            mBinding.recyclerView.visibility = View.VISIBLE
            mBinding.backButton.visibility = View.VISIBLE
            mBinding.filter.visibility = View.VISIBLE
            mBinding.product1.visibility = View.VISIBLE
            option = 1
            when (position1) {
                0 -> setChart1()
                1 -> setChart2()
                2 -> setChart3()
                3 -> setChart4()
            }
            setAdapter()
        }

        mBinding.backButton.setOnClickListener {
            option = 0
            when (position1) {
                0 -> setChart1()
                1 -> setChart2()
                2 -> setChart3()
                3 -> setChart4()
            }

            mBinding.viewProduct.visibility = View.VISIBLE
            mBinding.widgetView.visibility = View.VISIBLE
            mBinding.recyclerView.visibility = View.GONE
            mBinding.backButton.visibility = View.GONE
            mBinding.filter.visibility = View.GONE
            mBinding.product1.visibility = View.GONE
            mBinding.imageView59.visibility = View.GONE
        }

    }

    private fun setAdapter() {
        val manager = LinearLayoutManager(activity)
        manager.orientation = LinearLayout.HORIZONTAL
        mBinding.recyclerView.layoutManager = manager
        mAdapter = ViewsAdapter(activity!!)
        mBinding.recyclerView.adapter = mAdapter
        mViewModel.getData().observe(this, Observer {
            mAdapter.updateArray(it)
            if (it.size > 3) {
                mBinding.imageView59.visibility = View.VISIBLE
            } else {
                mBinding.imageView59.visibility = View.GONE
            }
        })

    }

    override fun onTabReselected(p0: TabLayout.Tab?) {

    }

    override fun onTabUnselected(p0: TabLayout.Tab?) {

    }

    override fun onTabSelected(p0: TabLayout.Tab?) {
        when {
            p0!!.position == 0 -> {
                mBinding.chartReporting.visibility = View.GONE
                mBinding.chartReporting2.visibility = View.VISIBLE
                mBinding.chartReporting1.visibility = View.GONE
                mBinding.chartReporting3.visibility = View.GONE
                setChart1()
                position1 = 0
            }
            p0.position == 1 -> {
                mBinding.chartReporting.visibility = View.GONE
                mBinding.chartReporting2.visibility = View.GONE
                mBinding.chartReporting3.visibility = View.GONE
                mBinding.chartReporting1.visibility = View.VISIBLE
                setChart2()
                position1 = 1
            }
            p0.position == 2 -> {
                mBinding.chartReporting2.visibility = View.GONE
                mBinding.chartReporting3.visibility = View.GONE
                mBinding.chartReporting.visibility = View.VISIBLE
                mBinding.chartReporting1.visibility = View.GONE
                setChart3()
                position1 = 2
            }
            else -> {
                mBinding.chartReporting2.visibility = View.GONE
                mBinding.chartReporting.visibility = View.GONE
                mBinding.chartReporting3.visibility = View.VISIBLE
                mBinding.chartReporting1.visibility = View.GONE
                setChart4()
                position1 = 3
            }
        }
    }


    private fun setChart1() {
        mBinding.chartReporting2.description.isEnabled = false
        mBinding.chartReporting2.setTouchEnabled(true)


        val xAxis: XAxis = mBinding.chartReporting2.xAxis
        val yAxis: YAxis = mBinding.chartReporting2.axisLeft

        val listAmount = ArrayList<String>()
        listAmount.add("0")
        listAmount.add("5K")
        listAmount.add("10K")
        listAmount.add("15K")
        listAmount.add("20K")

        val listDays = ArrayList<String>()
        listDays.add("MON")
        listDays.add("TUE")
        listDays.add("WED")
        listDays.add("THUR")
        listDays.add("FRI")
        listDays.add("SAT")
        listDays.add("SUN")

        mBinding.chartReporting2.xAxis.isEnabled = true
        xAxis.position = XAxis.XAxisPosition.BOTTOM
        xAxis.setDrawAxisLine(false)
        xAxis.axisMinimum = 0f
        xAxis.axisMaximum = 6f
        xAxis.textSize = 6f
        xAxis.valueFormatter = IAxisValueFormatter { value, axis -> listDays[value.roundToInt()] }

        // Y axis data
        mBinding.chartReporting2.axisRight.isEnabled = false
        mBinding.chartReporting2.axisLeft.isEnabled = true

        mBinding.chartReporting2.axisLeft.granularity = 1f
        yAxis.axisMaximum = 4f
        yAxis.axisMinimum = 0f
        mBinding.chartReporting2.axisLeft.setDrawGridLines(false)
        mBinding.chartReporting2.xAxis.setDrawGridLines(false)
        mBinding.chartReporting2.axisLeft.setDrawGridLines(true)
        mBinding.chartReporting2.xAxis.setDrawGridLines(false)
        mBinding.chartReporting2.axisLeft.gridColor = resources.getColor(R.color.colorWhite)
        mBinding.chartReporting2.axisLeft.axisLineColor = resources.getColor(R.color.fulltransparentcolor)
        mBinding.chartReporting2.axisRight.axisLineColor = resources.getColor(R.color.colorWhite)
        xAxis.setAvoidFirstLastClipping(true)
        yAxis.setDrawLimitLinesBehindData(true)
        xAxis.setDrawLimitLinesBehindData(true)
        yAxis.textColor = Color.WHITE
        xAxis.textColor = Color.WHITE
        yAxis.textSize = 8f
        yAxis.valueFormatter = IAxisValueFormatter { value, axis ->
            listAmount[value.roundToInt()]
        }

        yAxis.setDrawLimitLinesBehindData(true)
        xAxis.setDrawLimitLinesBehindData(true)

        if (option == 1) {
            setData5()
        } else {
            setData1()
        }

        mBinding.chartReporting2.animateX(500)


        val legend = mBinding.chartReporting2.legend
        legend.form = Legend.LegendForm.NONE

    }


    private fun setChart2() {
        mBinding.chartReporting1.description.isEnabled = false
        val xAxis: XAxis = mBinding.chartReporting1.xAxis
        val yAxis: YAxis = mBinding.chartReporting1.axisLeft

        val listDays1 = ArrayList<String>()
        listDays1.add("1st Week")
        listDays1.add("2nd Week")
        listDays1.add("3rd Week")
        listDays1.add("4th Week")

        val listAmount = ArrayList<String>()
        listAmount.add("0")
        listAmount.add("5K")
        listAmount.add("10K")
        listAmount.add("15K")
        listAmount.add("20K")
        xAxis.axisMaximum = 3f
        xAxis.axisMinimum = 0f
        yAxis.axisMaximum = 4f
        yAxis.axisMinimum = 0f
        xAxis.labelCount = 4
        xAxis.valueFormatter = IAxisValueFormatter { value1, axis -> listDays1[Math.round(value1)] }
        yAxis.valueFormatter = IAxisValueFormatter { value, axis -> listAmount[Math.round(value)] }
        mBinding.chartReporting1.xAxis.isEnabled = true
        xAxis.position = XAxis.XAxisPosition.BOTTOM
        xAxis.setDrawAxisLine(false)
        xAxis.textSize = 7f
        xAxis.textColor = ColorTemplate.rgb("#868795")
        xAxis.granularity = 1f
        // Y axis data
        mBinding.chartReporting1.axisLeft.granularity = 1f
        mBinding.chartReporting1.axisRight.isEnabled = false
        mBinding.chartReporting1.axisLeft.isEnabled = true
        mBinding.chartReporting1.axisLeft.gridColor = resources.getColor(R.color.pic_non_selected)
        mBinding.chartReporting1.axisLeft.axisLineColor = resources.getColor(R.color.fulltransparentcolor)
        mBinding.chartReporting1.axisRight.axisLineColor = resources.getColor(R.color.colorWhite)
        yAxis.textColor = ColorTemplate.rgb("#868795")
        yAxis.textSize = 7f
        mBinding.chartReporting1.axisLeft.setDrawGridLines(true)
        mBinding.chartReporting1.xAxis.setDrawGridLines(false)
        yAxis.setDrawLimitLinesBehindData(true)
        xAxis.setDrawLimitLinesBehindData(true)

        if (option == 1) {
            setData6()
        } else {
            setData2()
        }

        mBinding.chartReporting1.animateX(500)
        val legend = mBinding.chartReporting1.legend
        legend.form = Legend.LegendForm.NONE

    }


    private fun setChart3() {
        mBinding.chartReporting.description.isEnabled = false

        val xAxis: XAxis = mBinding.chartReporting.xAxis
        val yAxis: YAxis = mBinding.chartReporting.axisLeft
        val listAmount = ArrayList<String>()
        listAmount.add("0")
        listAmount.add("5K")
        listAmount.add("10K")
        listAmount.add("15K")
        listAmount.add("20K")

        val listDays2 = ArrayList<String>()
//        listDays2.add("1st Week")
//        listDays2.add("2nd Week")
//        listDays2.add("3rd week")
//        listDays2.add("4th Week")
//        listDays2.add("5th Week")
//        listDays2.add("6th Week")
//        listDays2.add("7th week")
//        listDays2.add("8th Week")
//        listDays2.add("9th Week")
//        listDays2.add("10th Week")
//        listDays2.add("11th week")
//        listDays2.add("12th Week")

        listDays2.add("30 June")
        listDays2.add("7 July")
        listDays2.add("14 July")
        listDays2.add("21 July")
        listDays2.add("28 July")
        listDays2.add("4 Aug")
        listDays2.add("11 Aug")
        listDays2.add("18 Aug")
//        listDays2.add("9th Week")
//        listDays2.add("")
//        listDays2.add("")
//        listDays2.add("")
        xAxis.granularity = 1f
        xAxis.axisMinimum = 0.1f
        xAxis.axisMaximum = 1f
        if (option == 1) {
            setData7()
        } else {
            setData3()
        }

        xAxis.labelCount = 8
//        xAxis.labelRotationAngle = 45F
        xAxis.valueFormatter = IAxisValueFormatter { value, axis -> listDays2[Math.round(value)] }
        mBinding.chartReporting.xAxis.isEnabled = true
        xAxis.position = XAxis.XAxisPosition.BOTTOM
        xAxis.setDrawAxisLine(false)
        mBinding.chartReporting.axisLeft.granularity = 1f
        xAxis.textSize = 8f
        xAxis.textColor = Color.WHITE
        yAxis.textColor = Color.WHITE

        // Y axis data

        mBinding.chartReporting.axisRight.isEnabled = false
        mBinding.chartReporting.axisLeft.isEnabled = true

        yAxis.axisMaximum = 4f
        yAxis.axisMinimum = 0f

        mBinding.chartReporting.axisLeft.setDrawGridLines(true)
        mBinding.chartReporting.xAxis.setDrawGridLines(false)
        mBinding.chartReporting.axisLeft.gridColor = resources.getColor(R.color.colorWhite)
        mBinding.chartReporting.axisLeft.axisLineColor = resources.getColor(R.color.fulltransparentcolor)
        mBinding.chartReporting.axisRight.axisLineColor = resources.getColor(R.color.colorWhite)

        yAxis.setDrawLimitLinesBehindData(true)
        xAxis.setDrawLimitLinesBehindData(true)
        yAxis.textColor = Color.WHITE
        yAxis.textSize = 8f
        yAxis.valueFormatter = IAxisValueFormatter { value, axis ->
            listAmount[value.roundToInt()]
        }
        mBinding.chartReporting.animateX(500)

        val legend = mBinding.chartReporting.legend
        legend.form = Legend.LegendForm.NONE

    }

    private fun setData7() {
        val values1 = ArrayList<BarEntry>()
        val values2 = ArrayList<BarEntry>()
        val values3 = ArrayList<BarEntry>()
        val values4 = ArrayList<BarEntry>()
        for (k in 0 until 14) {
            values1.add(BarEntry(k.toFloat(), (Math.random() * 3).toFloat()))
            values2.add(BarEntry(k.toFloat(), (Math.random() * 4).toFloat()))
            values3.add(BarEntry(k.toFloat(), (Math.random() * 2).toFloat()))
            values4.add(BarEntry(k.toFloat(), (Math.random() * 1).toFloat()))
        }
        val set1 = BarDataSet(values1, "")
        val set2 = BarDataSet(values2, "")
        val set3 = BarDataSet(values3, "")
        val set4 = BarDataSet(values4, "")

        set1.color = ColorTemplate.rgb("#7760f2")
        set2.color = ColorTemplate.rgb("#02cbcd")
        set3.color = ColorTemplate.rgb("#ff3f55")
        set4.color = ColorTemplate.rgb("#fcc117")

        set1.setDrawValues(false)
        set2.setDrawValues(false)
        set3.setDrawValues(false)
        set4.setDrawValues(false)

        set1.formLineWidth = 1f
        set2.formLineWidth = 1f
        set3.formLineWidth = 1f
        set4.formLineWidth = 1f

        set1.formSize = 15f
        set2.formSize = 15f
        set3.formSize = 15f
        set4.formSize = 15f

        set1.valueTextSize = 5f
        set2.valueTextSize = 5f
        set3.valueTextSize = 5f
        set4.valueTextSize = 5f

        set1.isHighlightEnabled = false
        set2.isHighlightEnabled = false
        set3.isHighlightEnabled = false
        set4.isHighlightEnabled = false

        val data = BarData(set1, set2, set3, set4)
        data.setValueFormatter(LargeValueFormatter())
        data.barWidth = .4f

        mBinding.chartReporting.setLayerType(View.LAYER_TYPE_SOFTWARE, null)
        mBinding.chartReporting.setScaleEnabled(false)
        mBinding.chartReporting.invalidate()
        mBinding.chartReporting.xAxis.axisMaximum = 7f + 0.3f
        mBinding.chartReporting.xAxis.axisMinimum = 0f - 0.3f
        mBinding.chartReporting.data = data
        mBinding.chartReporting.invalidate()


    }

    private fun setChart4() {
        mBinding.chartReporting3.description.isEnabled = false

        val xAxis: XAxis = mBinding.chartReporting3.xAxis
        val yAxis: YAxis = mBinding.chartReporting3.axisLeft
        val listAmount = ArrayList<String>()
        listAmount.add("0")
        listAmount.add("5K")
        listAmount.add("10K")
        listAmount.add("15K")
        listAmount.add("20K")

        val listDays3 = ArrayList<String>()
        listDays3.add("Jan")
        listDays3.add("Feb")
        listDays3.add("Mar")
        listDays3.add("April")
        listDays3.add("May")
        listDays3.add("June")
        listDays3.add("July")
        listDays3.add("Aug")
        listDays3.add("Sep")
        listDays3.add("Oct")
        listDays3.add("Nov")
        listDays3.add("Dec")
        xAxis.axisMinimum = 0.1f
        xAxis.axisMaximum = 1f
        if (option == 1) {
            setData8()
        } else {
            setData4()
        }
        xAxis.labelCount = 12
        xAxis.labelRotationAngle = 45F
        xAxis.valueFormatter = IAxisValueFormatter { value, axis -> listDays3[value.roundToInt()] }
        mBinding.chartReporting3.xAxis.isEnabled = true
        mBinding.chartReporting3.axisLeft.granularity = 1f
        xAxis.position = XAxis.XAxisPosition.BOTTOM
        xAxis.setDrawAxisLine(false)

        xAxis.textSize = 7f
        xAxis.textColor = Color.WHITE
        yAxis.textColor = Color.WHITE

        // Y axis data

        mBinding.chartReporting3.axisRight.isEnabled = false
        mBinding.chartReporting3.axisLeft.isEnabled = true

        yAxis.axisMaximum = 4f
        yAxis.axisMinimum = 0f

        mBinding.chartReporting3.axisLeft.setDrawGridLines(true)
        mBinding.chartReporting3.xAxis.setDrawGridLines(false)
        mBinding.chartReporting3.axisLeft.gridColor = resources.getColor(R.color.colorWhite)
        mBinding.chartReporting3.axisLeft.axisLineColor = resources.getColor(R.color.fulltransparentcolor)
        mBinding.chartReporting3.axisRight.axisLineColor = resources.getColor(R.color.colorWhite)

        yAxis.setDrawLimitLinesBehindData(true)
        xAxis.setDrawLimitLinesBehindData(true)
        yAxis.textColor = Color.WHITE
        yAxis.textSize = 7f
        yAxis.valueFormatter = IAxisValueFormatter { value, axis ->
            listAmount[value.roundToInt()]
        }
        mBinding.chartReporting3.animateX(500)

        val legend = mBinding.chartReporting3.legend
        legend.form = Legend.LegendForm.NONE
    }

    private fun setData8() {
        val values1 = ArrayList<BarEntry>()
        val values2 = ArrayList<BarEntry>()
        val values3 = ArrayList<BarEntry>()
        val values4 = ArrayList<BarEntry>()
        for (k in 0 until 14) {
            values1.add(BarEntry(k.toFloat(), (Math.random() * 3).toFloat()))
            values2.add(BarEntry(k.toFloat(), (Math.random() * 4).toFloat()))
            values3.add(BarEntry(k.toFloat(), (Math.random() * 2).toFloat()))
            values4.add(BarEntry(k.toFloat(), (Math.random() * 1).toFloat()))
        }
        val set1 = BarDataSet(values1, "")
        val set2 = BarDataSet(values2, "")
        val set3 = BarDataSet(values3, "")
        val set4 = BarDataSet(values4, "")

        set1.color = ColorTemplate.rgb("#7760f2")
        set2.color = ColorTemplate.rgb("#02cbcd")
        set3.color = ColorTemplate.rgb("#ff3f55")
        set4.color = ColorTemplate.rgb("#fcc117")

        set1.setDrawValues(false)
        set2.setDrawValues(false)
        set3.setDrawValues(false)
        set4.setDrawValues(false)

        set1.formLineWidth = 1f
        set2.formLineWidth = 1f
        set3.formLineWidth = 1f
        set4.formLineWidth = 1f

        set1.formSize = 15f
        set2.formSize = 15f
        set3.formSize = 15f
        set4.formSize = 15f

        set1.valueTextSize = 5f
        set2.valueTextSize = 5f
        set3.valueTextSize = 5f
        set4.valueTextSize = 5f

        set1.isHighlightEnabled = false
        set2.isHighlightEnabled = false
        set3.isHighlightEnabled = false
        set4.isHighlightEnabled = false

        val data = BarData(set1, set2, set3, set4)
        data.setValueFormatter(LargeValueFormatter())
        data.barWidth = .4f

        mBinding.chartReporting3.setLayerType(View.LAYER_TYPE_SOFTWARE, null)
        mBinding.chartReporting3.setScaleEnabled(false)
        mBinding.chartReporting3.invalidate()
        mBinding.chartReporting3.xAxis.axisMaximum = 11f + 0.3f
        mBinding.chartReporting3.xAxis.axisMinimum = 0f - 0.3f
        mBinding.chartReporting3.data = data
        mBinding.chartReporting3.invalidate()


    }


    private fun setData1() {
        var groupSpace = 0.2f
        var barSpace = 0.1f// x4 DataSet

        val values1 = ArrayList<BarEntry>()
        val values2 = ArrayList<BarEntry>()

        values1.add(BarEntry(1f, 1700f / 1000))
        values1.add(BarEntry(2f, 1600f / 1000))
        values1.add(BarEntry(3f, 800f / 1000))
        values1.add(BarEntry(5f, 1000f / 1000))
        values1.add(BarEntry(6f, 900f / 1000))
        values1.add(BarEntry(7f, 1400f / 1000))
        values1.add(BarEntry(8f, 1500f / 1000))

        values2.add(BarEntry(1f, 1000f / 1000))
        values2.add(BarEntry(200f, 45f / 100))
        values2.add(BarEntry(300f, 800f / 1000))
        values2.add(BarEntry(50f, 1000f / 1000))
        values2.add(BarEntry(60f, 900f / 1000))
        values2.add(BarEntry(70f, 1400f / 1000))
        values2.add(BarEntry(80f, 1500f / 1000))

        val set1 = BarDataSet(values1, "")
        set1.color = ColorTemplate.rgb("#7760f2")

        val set2 = BarDataSet(values2, "")
        set2.color = Color.rgb(2, 203, 205)
        set1.setDrawValues(false)
        set2.setDrawValues(false)
        set1.formLineWidth = 1f
        set1.formSize = 15f
        set1.valueTextSize = 5f
        set1.isHighlightEnabled = false

        set2.isHighlightEnabled = false
        set2.formLineWidth = 1f
        set2.formSize = 15f
        set2.valueTextSize = 5f

        val data = BarData(set1, set2)
//        val data = BarData(set1)
        data.setValueFormatter(LargeValueFormatter())
        data.barWidth = .28f
        data.groupBars(-0.2f, groupSpace, barSpace)
        mBinding.chartReporting2.setLayerType(View.LAYER_TYPE_SOFTWARE, null)
        mBinding.chartReporting2.setScaleEnabled(false)
        mBinding.chartReporting2.invalidate()
        mBinding.chartReporting2.xAxis.axisMaximum = 6f + 0.5f
        mBinding.chartReporting2.xAxis.axisMinimum = 0f - 0.1f
        mBinding.chartReporting2.data = data
        mBinding.chartReporting2.invalidate()
    }

    private fun setData6() {
        val values1 = ArrayList<BarEntry>()
        val values2 = ArrayList<BarEntry>()
        val values3 = ArrayList<BarEntry>()
        val values4 = ArrayList<BarEntry>()
        for (k in 0 until 7) {
            values1.add(BarEntry(k.toFloat(), (Math.random() * 3).toFloat()))
            values2.add(BarEntry(k.toFloat(), (Math.random() * 4).toFloat()))
            values3.add(BarEntry(k.toFloat(), (Math.random() * 2).toFloat()))
            values4.add(BarEntry(k.toFloat(), (Math.random() * 1).toFloat()))
        }
        val set1 = BarDataSet(values1, "")
        val set2 = BarDataSet(values2, "")
        val set3 = BarDataSet(values3, "")
        val set4 = BarDataSet(values4, "")

        set1.color = ColorTemplate.rgb("#7760f2")
        set2.color = ColorTemplate.rgb("#02cbcd")
        set3.color = ColorTemplate.rgb("#ff3f55")
        set4.color = ColorTemplate.rgb("#fcc117")

        set1.setDrawValues(false)
        set2.setDrawValues(false)
        set3.setDrawValues(false)
        set4.setDrawValues(false)

        set1.formLineWidth = 1f
        set2.formLineWidth = 1f
        set3.formLineWidth = 1f
        set4.formLineWidth = 1f

        set1.formSize = 15f
        set2.formSize = 15f
        set3.formSize = 15f
        set4.formSize = 15f

        set1.valueTextSize = 5f
        set2.valueTextSize = 5f
        set3.valueTextSize = 5f
        set4.valueTextSize = 5f

        set1.isHighlightEnabled = false
        set2.isHighlightEnabled = false
        set3.isHighlightEnabled = false
        set4.isHighlightEnabled = false

        val data = BarData(set1, set2, set3, set4)
        data.setValueFormatter(LargeValueFormatter())
        data.barWidth = .4f

        mBinding.chartReporting1.setLayerType(View.LAYER_TYPE_SOFTWARE, null)
        mBinding.chartReporting1.setScaleEnabled(false)
        mBinding.chartReporting1.invalidate()
        mBinding.chartReporting1.xAxis.axisMaximum = 3f + 0.45f
        mBinding.chartReporting1.xAxis.axisMinimum = 0f - 0.5f
        mBinding.chartReporting1.data = data
        mBinding.chartReporting1.invalidate()


    }


    private fun setData5() {
        val values1 = ArrayList<BarEntry>()
        val values2 = ArrayList<BarEntry>()
        val values3 = ArrayList<BarEntry>()
        val values4 = ArrayList<BarEntry>()
        for (k in 0 until 7) {
            values1.add(BarEntry(k.toFloat(), (Math.random() * 3).toFloat()))
            values2.add(BarEntry(k.toFloat(), (Math.random() * 4).toFloat()))
            values3.add(BarEntry(k.toFloat(), (Math.random() * 2).toFloat()))
            values4.add(BarEntry(k.toFloat(), (Math.random() * 1).toFloat()))
        }
        val set1 = BarDataSet(values1, "")
        val set2 = BarDataSet(values2, "")
        val set3 = BarDataSet(values3, "")
        val set4 = BarDataSet(values4, "")

        set1.color = ColorTemplate.rgb("#7760f2")
        set2.color = ColorTemplate.rgb("#02cbcd")
        set3.color = ColorTemplate.rgb("#ff3f55")
        set4.color = ColorTemplate.rgb("#fcc117")

        set1.setDrawValues(false)
        set2.setDrawValues(false)
        set3.setDrawValues(false)
        set4.setDrawValues(false)

        set1.formLineWidth = 1f
        set2.formLineWidth = 1f
        set3.formLineWidth = 1f
        set4.formLineWidth = 1f

        set1.formSize = 15f
        set2.formSize = 15f
        set3.formSize = 15f
        set4.formSize = 15f

        set1.valueTextSize = 5f
        set2.valueTextSize = 5f
        set3.valueTextSize = 5f
        set4.valueTextSize = 5f

        set1.isHighlightEnabled = false
        set2.isHighlightEnabled = false
        set3.isHighlightEnabled = false
        set4.isHighlightEnabled = false

        val data = BarData(set1, set2, set3, set4)
        data.setValueFormatter(LargeValueFormatter())
        data.barWidth = .4f

        mBinding.chartReporting2.setLayerType(View.LAYER_TYPE_SOFTWARE, null)
        mBinding.chartReporting2.setScaleEnabled(false)
        mBinding.chartReporting2.invalidate()
        mBinding.chartReporting2.xAxis.axisMaximum = 6f + 0.3f
        mBinding.chartReporting2.xAxis.axisMinimum = 0f - 0.3f
        mBinding.chartReporting2.data = data
        mBinding.chartReporting2.invalidate()

    }


    private fun setData2() {
        val groupSpace = 0.35f
        val barSpace = 0.05f
        val values5 = ArrayList<BarEntry>()
        val values6 = ArrayList<BarEntry>()
        values5.clear()
        values6.clear()
        mBinding.chartReporting1.invalidate()
        values5.add(BarEntry(0f, 70f / 100))
        values5.add(BarEntry(1f, 90f / 100))
        values5.add(BarEntry(2f, 200f / 100))
        values5.add(BarEntry(3f, 300f / 100))
//        values5.add(BarEntry(4f, 200f / 100))

        values6.add(BarEntry(1f, 1000f / 1000))
        values6.add(BarEntry(200f, 45f / 100))
        values6.add(BarEntry(300f, 800f / 1000))
        values6.add(BarEntry(50f, 1000f / 1000))

        val set1 = BarDataSet(values5, "")
        set1.color = ColorTemplate.rgb("#7760f2")

        val set2 = BarDataSet(values6, "")
        set2.color = Color.rgb(2, 203, 205)
        set1.setDrawValues(false)
        set2.setDrawValues(false)
        set1.formLineWidth = 1f
        set1.formSize = 15f
        set1.valueTextSize = 5f
        set1.isHighlightEnabled = false
        set2.isHighlightEnabled = false
        set2.formLineWidth = 1f
        set2.formSize = 15f
        set2.valueTextSize = 5f
        val data = BarData(set1, set2)
//        val data = BarData(set1)
        data.setValueFormatter(LargeValueFormatter())
        data.barWidth = .25f
        data.groupBars(-0.35f, groupSpace, barSpace)
        mBinding.chartReporting1.data = data
        mBinding.chartReporting1.setLayerType(View.LAYER_TYPE_SOFTWARE, null)
        mBinding.chartReporting1.setScaleEnabled(false)
        mBinding.chartReporting1.xAxis.axisMaximum = 3f + 0.45f
        mBinding.chartReporting1.xAxis.axisMinimum = 0f - 0.3f
        mBinding.chartReporting1.invalidate()

    }


    private fun setData3() {
        val groupSpace = 0.25f
        val barSpace = 0.1f
        val values10 = ArrayList<BarEntry>()
        val values20 = ArrayList<BarEntry>()
        values10.clear()
        values20.clear()
        mBinding.chartReporting.invalidate()
        values10.add(BarEntry(0f, 70f / 100));
        values10.add(BarEntry(1f, 90f / 100));
        values10.add(BarEntry(2f, 100f / 100));
        values10.add(BarEntry(3f, 150f / 100));
        // gap of 2f
        values10.add(BarEntry(4f, 170f / 100));
        values10.add(BarEntry(5f, 190f / 100));
        values10.add(BarEntry(6f, 210f / 100));
        values10.add(BarEntry(7f, 250f / 100));
//        values10.add(BarEntry(8f, 130f / 100));
//        values10.add(BarEntry(9f, 300f / 100));
//        values10.add(BarEntry(10f, 450f / 100));
//        values10.add(BarEntry(11f, 600f / 100));
//        values10.add(BarEntry(12f, 900f / 100));

        values20.add(BarEntry(1f, 1000f / 1000))
        values20.add(BarEntry(200f, 45f / 100))
        values20.add(BarEntry(300f, 800f / 1000))
        values20.add(BarEntry(50f, 1000f / 1000))
        values20.add(BarEntry(60f, 900f / 1000))
        values20.add(BarEntry(70f, 1400f / 1000))
        values20.add(BarEntry(80f, 1500f / 1000))
        values20.add(BarEntry(90f, 2000f / 1000))
//        values20.add(BarEntry(100f, 2500f / 1000))
//        values20.add(BarEntry(110f, 3000f / 1000))
//        values20.add(BarEntry(150f, 350f / 1000))
//        values20.add(BarEntry(200f, 400f / 1000))

        val set1 = BarDataSet(values10, "")
        set1.color = ColorTemplate.rgb("#7760f2")

        val set2 = BarDataSet(values20, "")
        set2.color = Color.rgb(2, 203, 205)
        set1.setDrawValues(false)
        set2.setDrawValues(false)
        set1.formLineWidth = 1f
        set1.formSize = 15f
        set1.valueTextSize = 5f
        set1.isHighlightEnabled = false
        set2.isHighlightEnabled = false
        set2.formLineWidth = 1f
        set2.formSize = 15f
        set2.valueTextSize = 5f
//        val data = BarData(set1)
        val data = BarData(set1, set2)
        data.setValueFormatter(LargeValueFormatter())
        data.barWidth = .26f
        data.groupBars(-0.35f, groupSpace, barSpace)
        mBinding.chartReporting.data = data
        mBinding.chartReporting.xAxis.axisMaximum = 7f + 0.2f
        mBinding.chartReporting.xAxis.axisMinimum = 0f - 0.3f
        mBinding.chartReporting.setLayerType(View.LAYER_TYPE_SOFTWARE, null)
        mBinding.chartReporting.setScaleEnabled(false)
        mBinding.chartReporting.invalidate()


    }

    private fun setData4() {
        val groupSpace = 0.28f
        val barSpace = 0.1f
        val values10 = ArrayList<BarEntry>()
        val values20 = ArrayList<BarEntry>()
        values10.clear()
        values20.clear()
        mBinding.chartReporting3.invalidate()
        values10.add(BarEntry(0f, 70f / 100));
        values10.add(BarEntry(1f, 90f / 100));
        values10.add(BarEntry(2f, 100f / 100));
        values10.add(BarEntry(3f, 150f / 100));
        // gap of 2f
        values10.add(BarEntry(4f, 170f / 100));
        values10.add(BarEntry(5f, 190f / 100));
        values10.add(BarEntry(6f, 210f / 100));
        values10.add(BarEntry(7f, 250f / 100));
        values10.add(BarEntry(8f, 130f / 100));
        values10.add(BarEntry(9f, 300f / 100));
        values10.add(BarEntry(10f, 450f / 100));
        values10.add(BarEntry(11f, 600f / 100));
        values10.add(BarEntry(12f, 900f / 100));

        values20.add(BarEntry(1f, 1000f / 1000))
        values20.add(BarEntry(200f, 45f / 100))
        values20.add(BarEntry(300f, 800f / 1000))
        values20.add(BarEntry(50f, 1000f / 1000))
        values20.add(BarEntry(60f, 900f / 1000))
        values20.add(BarEntry(70f, 1400f / 1000))
        values20.add(BarEntry(80f, 1500f / 1000))
        values20.add(BarEntry(90f, 2000f / 1000))
        values20.add(BarEntry(100f, 2500f / 1000))
        values20.add(BarEntry(110f, 3000f / 1000))
        values20.add(BarEntry(150f, 350f / 1000))
        values20.add(BarEntry(200f, 400f / 1000))

        val set1 = BarDataSet(values10, "")
        set1.color = ColorTemplate.rgb("#7760f2")

        val set2 = BarDataSet(values20, "")
        set2.color = Color.rgb(2, 203, 205)
        set1.setDrawValues(false)
        set2.setDrawValues(false)
        set1.formLineWidth = 1f
        set1.formSize = 15f
        set1.valueTextSize = 5f
        set1.isHighlightEnabled = false
        set2.isHighlightEnabled = false
        set2.formLineWidth = 1f
        set2.formSize = 15f
        set2.valueTextSize = 5f
//        val data = BarData(set1)
        val data = BarData(set1, set2)
        data.setValueFormatter(LargeValueFormatter())
        data.barWidth = .23f
        data.groupBars(-0.2f, groupSpace, barSpace)
        mBinding.chartReporting3.data = data
        mBinding.chartReporting3.xAxis.axisMaximum = 11f + 0.2f
        mBinding.chartReporting3.xAxis.axisMinimum = 0f - 0.1f
        mBinding.chartReporting3.setLayerType(View.LAYER_TYPE_SOFTWARE, null)
        mBinding.chartReporting3.setScaleEnabled(false)
        mBinding.chartReporting3.invalidate()

    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        update = context as openFilters
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        update = activity as openFilters
    }

}
