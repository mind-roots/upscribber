package com.upscribber.upscribberSeller.reportingDetail.teamPerformance

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.upscribber.upscribberSeller.reportingDetail.subscription.ModelSubscriptionRecycler

class TeamViewModel(application: Application) : AndroidViewModel(application) {
    private lateinit var teams : LiveData<List<ModelTeamPerformance>>
    private lateinit var items : LiveData<List<ModelSubscriptionRecycler>>
    private lateinit var teams1 : LiveData<List<ModelSubscriptionRecycler>>
    var mRepo: TeamRepository = TeamRepository(application)

    init {
        teams = mRepo.getTeams()
        teams1 = mRepo.getTeams1()
        items = mRepo.getItems()
    }

    fun getTeamsData(): LiveData<List<ModelTeamPerformance>> {
        return teams
    }

    fun getData(): LiveData<List<ModelSubscriptionRecycler>> {
        return teams1
    }

    fun getProductsData(): LiveData<List<ModelSubscriptionRecycler>> {
        return items
    }

}