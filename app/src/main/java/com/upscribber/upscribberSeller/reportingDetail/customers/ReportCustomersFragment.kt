package com.upscribber.upscribberSeller.reportingDetail.customers

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.google.android.material.tabs.TabLayout
import com.upscribber.R
import com.upscribber.databinding.FragmentReportCustomersBinding
import com.upscribber.upscribberSeller.reportingDetail.openFilters
import kotlinx.android.synthetic.main.toolbar_reporting.view.*


class ReportCustomersFragment : Fragment() , TabLayout.OnTabSelectedListener{

    lateinit var mBinding : FragmentReportCustomersBinding
    lateinit var update: openFilters

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_report_customers, container, false)
        return mBinding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        clickListeners()
        mBinding.tabLayout.tabLayout.addOnTabSelectedListener(this)
    }

    private fun clickListeners() {
        mBinding.filter.setOnClickListener {
            update.setFilters()
        }

    }

    override fun onTabReselected(p0: TabLayout.Tab?) {


    }

    override fun onTabUnselected(p0: TabLayout.Tab?) {

    }

    override fun onTabSelected(p0: TabLayout.Tab?) {
        if (p0!!.position == 0){
            mBinding.circularProgressbar.progress = 60
            mBinding.circularProgressbar2.progress = 65
            mBinding.circularProgressbar3.progress = 30
            mBinding.newCustomerValue.setText("2,185")
            mBinding.returnCustomerValue.setText("3,554")
            mBinding.cancelCustomerValue.setText("10")
            mBinding.newPercent.setText("60%")
            mBinding.textView177.setText("65%")
            mBinding.tVCancel.setText("30%")
        }else if (p0.position == 1){
            mBinding.circularProgressbar.progress = 70
            mBinding.circularProgressbar2.progress = 75
            mBinding.circularProgressbar3.progress = 40
            mBinding.newCustomerValue.setText("4,370")
            mBinding.returnCustomerValue.setText("7,108")
            mBinding.cancelCustomerValue.setText("20")
            mBinding.newPercent.setText("70%")
            mBinding.textView177.setText("75%")
            mBinding.tVCancel.setText("40%")
        }else if (p0.position == 2){
            mBinding.circularProgressbar.progress = 80
            mBinding.circularProgressbar2.progress = 85
            mBinding.circularProgressbar3.progress = 50
            mBinding.newCustomerValue.setText("8,740")
            mBinding.returnCustomerValue.setText("14,216")
            mBinding.cancelCustomerValue.setText("30")
            mBinding.newPercent.setText("80%")
            mBinding.textView177.setText("85%")
            mBinding.tVCancel.setText("50%")
        }else{
            mBinding.circularProgressbar.progress = 90
            mBinding.circularProgressbar2.progress = 95
            mBinding.circularProgressbar3.progress = 60
            mBinding.newCustomerValue.setText("10,740")
            mBinding.returnCustomerValue.setText("18,216")
            mBinding.cancelCustomerValue.setText("50")
            mBinding.newPercent.setText("90%")
            mBinding.textView177.setText("95%")
            mBinding.tVCancel.setText("60%")
        }


    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        update = context as openFilters
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        update = activity as openFilters
    }

}
