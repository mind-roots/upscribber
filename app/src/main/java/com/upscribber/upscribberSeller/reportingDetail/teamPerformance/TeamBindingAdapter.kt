package com.upscribber.upscribberSeller.reportingDetail.teamPerformance

import android.view.View
import androidx.databinding.BindingAdapter

@BindingAdapter("back")
fun setBackground(view: View, color: Int) {
    view.setBackgroundResource(color)
}