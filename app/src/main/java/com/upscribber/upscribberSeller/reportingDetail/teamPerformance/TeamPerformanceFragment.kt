package com.upscribber.upscribberSeller.reportingDetail.teamPerformance

import android.app.Activity
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.formatter.LargeValueFormatter
import com.github.mikephil.charting.formatter.IAxisValueFormatter
import com.github.mikephil.charting.utils.ColorTemplate
import com.google.android.material.tabs.TabLayout
import com.upscribber.R
import com.upscribber.databinding.FragmentTeamPerformanceBinding
import com.upscribber.upscribberSeller.reportingDetail.openFilters
import kotlinx.android.synthetic.main.toolbar_reporting.view.*
import kotlin.math.roundToInt

class TeamPerformanceFragment : Fragment(), TeamAdapter.ClickListener, TabLayout.OnTabSelectedListener {

    private lateinit var mAdapter: TeamAdapter
    lateinit var mBinding: FragmentTeamPerformanceBinding
    lateinit var mViewModel: TeamViewModel
    lateinit var update: openFilters

    val TEAM_COLORS = intArrayOf(
        ColorTemplate.rgb("#7760f2"),
        ColorTemplate.rgb("#fcc117"),
        ColorTemplate.rgb("#02cbcd"),
        ColorTemplate.rgb("#ff3f55")
    )

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_team_performance, container, false)
        mViewModel = ViewModelProviders.of(this).get(TeamViewModel::class.java)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        clickListeners()
        setChart1(1, ModelTeamPerformance())
        val manager = LinearLayoutManager(activity)
        manager.orientation = LinearLayout.HORIZONTAL
        mBinding.recyclerTeam.layoutManager = manager
        mAdapter = TeamAdapter(activity, this)
        mBinding.recyclerTeam.adapter = mAdapter
        mViewModel.getTeamsData().observe(this, Observer {
            mAdapter.updateArray(it)
            if (it.size > 3) {
                mBinding.imageView59.visibility = View.VISIBLE
            } else {
                mBinding.imageView59.visibility = View.GONE
            }
        })

        mBinding.click = this
        mBinding.tabLayout.tabLayout.addOnTabSelectedListener(this)
    }

    private fun clickListeners() {
        mBinding.filters.setOnClickListener {
            update.setFilters()
        }

    }

    override fun onItemClick(model: ModelTeamPerformance) {
        mBinding.recyclerTeam.visibility = View.INVISIBLE
        mBinding.backButton.visibility = View.VISIBLE
        mBinding.product1.text = "$7824"
        mBinding.imageView59.visibility = View.GONE
        setChart1(2, model)
    }

    override fun onBackPress() {
        mBinding.recyclerTeam.visibility = View.VISIBLE
        mBinding.imageView59.visibility = View.VISIBLE
        mBinding.backButton.visibility = View.INVISIBLE
        mBinding.product1.text = "782"
        setChart1(1, ModelTeamPerformance())
    }

    override fun onTabReselected(p0: TabLayout.Tab?) {


    }

    override fun onTabUnselected(p0: TabLayout.Tab?) {

    }

    override fun onTabSelected(p0: TabLayout.Tab?) {
        when {
            p0!!.position == 0 -> {
                mBinding.chartReporting.visibility = View.VISIBLE
                mBinding.chartReporting1.visibility = View.GONE
                mBinding.chartReporting2.visibility = View.GONE
                setChart1(1, ModelTeamPerformance())
            }
            p0.position == 1 -> {
                mBinding.chartReporting.visibility = View.GONE
                mBinding.chartReporting1.visibility = View.VISIBLE
                mBinding.chartReporting2.visibility = View.GONE
                setChart2(1, ModelTeamPerformance())
            }
            p0.position == 2 -> {
                mBinding.chartReporting.visibility = View.GONE
                mBinding.chartReporting1.visibility = View.GONE
                mBinding.chartReporting2.visibility = View.VISIBLE
                setChart3(1, ModelTeamPerformance())
            }
            else -> {
                mBinding.chartReporting.visibility = View.GONE
                mBinding.chartReporting1.visibility = View.GONE
                mBinding.chartReporting2.visibility = View.VISIBLE
                setChart4(1, ModelTeamPerformance())
            }
        }
    }

    private fun setChart4(type: Int, model: ModelTeamPerformance) {

        mBinding.chartReporting2.description.isEnabled = false

        val xAxis: XAxis = mBinding.chartReporting2.xAxis
        val yAxis: YAxis = mBinding.chartReporting2.axisLeft

        val listDays3 = ArrayList<String>()
        listDays3.add("Jan")
        listDays3.add("Feb")
        listDays3.add("Mar")
        listDays3.add("April")
        listDays3.add("May")
        listDays3.add("June")
        listDays3.add("July")
        listDays3.add("Aug")
        listDays3.add("Sep")
        listDays3.add("Oct")
        listDays3.add("Nov")
        listDays3.add("Dec")

        val listAmount = ArrayList<String>()
        listAmount.add("0")
        listAmount.add("5K")
        listAmount.add("10K")
        listAmount.add("15K")
        listAmount.add("20K")
        xAxis.axisMaximum = 11f
        xAxis.axisMinimum = 0f
        yAxis.axisMaximum = 4f
        yAxis.axisMinimum = 0f
        setData1()
        xAxis.labelCount = 12
        xAxis.valueFormatter = IAxisValueFormatter { value, axis -> listDays3[value.roundToInt()] }
        yAxis.valueFormatter = IAxisValueFormatter { value, axis -> listAmount[value.roundToInt()] }
        mBinding.chartReporting2.xAxis.isEnabled = true
        xAxis.position = XAxis.XAxisPosition.BOTTOM
        xAxis.setDrawAxisLine(false)
        xAxis.textSize = 7f
        xAxis.textColor = ColorTemplate.rgb("#868795")
        xAxis.granularity = 1f
        // Y axis data
        mBinding.chartReporting2.axisLeft.granularity = 1f
        mBinding.chartReporting2.axisRight.isEnabled = false
        mBinding.chartReporting2.axisLeft.isEnabled = true
        mBinding.chartReporting2.axisLeft.gridColor = resources.getColor(R.color.pic_non_selected)
        mBinding.chartReporting2.axisLeft.axisLineColor = resources.getColor(R.color.fulltransparentcolor)
        mBinding.chartReporting2.axisRight.axisLineColor = resources.getColor(R.color.colorWhite)
        yAxis.textColor = ColorTemplate.rgb("#868795")
        yAxis.textSize = 7f
        mBinding.chartReporting2.axisLeft.setDrawGridLines(true)
        mBinding.chartReporting2.xAxis.setDrawGridLines(false)
        yAxis.setDrawLimitLinesBehindData(true)
        xAxis.setDrawLimitLinesBehindData(true)

        if (type == 1) {
            setData4()
        } else {
            setDataForSingleCompany(model)
        }

        mBinding.chartReporting2.animateX(500)


        val legend = mBinding.chartReporting2.legend
        legend.form = Legend.LegendForm.NONE
    }

    private fun setChart3(type: Int, model: ModelTeamPerformance) {
        mBinding.chartReporting2.description.isEnabled = false

        val xAxis: XAxis = mBinding.chartReporting2.xAxis
        val yAxis: YAxis = mBinding.chartReporting2.axisLeft

        val listDays0 = ArrayList<String>()
        listDays0.add("1st Week")
        listDays0.add("2nd Week")
        listDays0.add("3rd week")
        listDays0.add("4th Week")
        listDays0.add("5th Week")
        listDays0.add("6th Week")
        listDays0.add("7th week")
        listDays0.add("8th Week")
        listDays0.add("9th Week")
        listDays0.add("10th Week")
        listDays0.add("11th week")
        listDays0.add("12th Week")

        val listAmount = ArrayList<String>()
        listAmount.add("0")
        listAmount.add("5K")
        listAmount.add("10K")
        listAmount.add("15K")
        listAmount.add("20K")
        xAxis.axisMaximum = 11f
        xAxis.axisMinimum = 0f
        yAxis.axisMaximum = 4f
        yAxis.axisMinimum = 0f
        setData1()
        xAxis.labelCount = 12
        xAxis.valueFormatter = IAxisValueFormatter { value, axis -> listDays0[value.roundToInt()] }
        yAxis.valueFormatter = IAxisValueFormatter { value, axis -> listAmount[value.roundToInt()] }
        mBinding.chartReporting2.xAxis.isEnabled = true
        xAxis.position = XAxis.XAxisPosition.BOTTOM
        xAxis.setDrawAxisLine(false)
        xAxis.textSize = 7f
        xAxis.textColor = ColorTemplate.rgb("#868795")
        xAxis.granularity = 1f
        // Y axis data
        mBinding.chartReporting2.axisLeft.granularity = 1f
        mBinding.chartReporting2.axisRight.isEnabled = false
        mBinding.chartReporting2.axisLeft.isEnabled = true
        mBinding.chartReporting2.axisLeft.gridColor = resources.getColor(R.color.pic_non_selected)
        mBinding.chartReporting2.axisLeft.axisLineColor = resources.getColor(R.color.fulltransparentcolor)
        mBinding.chartReporting2.axisRight.axisLineColor = resources.getColor(R.color.colorWhite)
        yAxis.textColor = ColorTemplate.rgb("#868795")
        yAxis.textSize = 7f
        mBinding.chartReporting2.axisLeft.setDrawGridLines(true)
        mBinding.chartReporting2.xAxis.setDrawGridLines(false)
        yAxis.setDrawLimitLinesBehindData(true)
        xAxis.setDrawLimitLinesBehindData(true)
        xAxis.labelRotationAngle = 45f
        if (type == 1) {
            setData3()
        } else {
            setDataForSingleCompany(model)
        }

        mBinding.chartReporting2.animateX(500)
        val legend = mBinding.chartReporting2.legend
        legend.form = Legend.LegendForm.NONE
    }

    private fun setChart2(type: Int, model: ModelTeamPerformance) {
        mBinding.chartReporting1.description.isEnabled = false
        val xAxis: XAxis = mBinding.chartReporting1.xAxis
        val yAxis: YAxis = mBinding.chartReporting1.axisLeft

        val listDays1 = ArrayList<String>()
        listDays1.add("1st Week")
        listDays1.add("2nd Week")
        listDays1.add("3rd Week")
        listDays1.add("4th Week")

        val listAmount = ArrayList<String>()
        listAmount.add("0")
        listAmount.add("5K")
        listAmount.add("10K")
        listAmount.add("15K")
        listAmount.add("20K")
        xAxis.axisMaximum = 3f
        xAxis.axisMinimum = 0f
        yAxis.axisMaximum = 4f
        yAxis.axisMinimum = 0f
        xAxis.labelCount = 4
        xAxis.valueFormatter = IAxisValueFormatter { value1, axis -> listDays1[Math.round(value1)] }
        yAxis.valueFormatter = IAxisValueFormatter { value, axis -> listAmount[Math.round(value)] }
        mBinding.chartReporting1.xAxis.isEnabled = true
        xAxis.position = XAxis.XAxisPosition.BOTTOM
        xAxis.setDrawAxisLine(false)
        xAxis.textSize = 7f
        xAxis.textColor = ColorTemplate.rgb("#868795")
        xAxis.granularity = 1f
        // Y axis data
        mBinding.chartReporting1.axisLeft.granularity = 1f
        mBinding.chartReporting1.axisRight.isEnabled = false
        mBinding.chartReporting1.axisLeft.isEnabled = true
        mBinding.chartReporting1.axisLeft.gridColor = resources.getColor(R.color.pic_non_selected)
        mBinding.chartReporting1.axisLeft.axisLineColor = resources.getColor(R.color.fulltransparentcolor)
        mBinding.chartReporting1.axisRight.axisLineColor = resources.getColor(R.color.colorWhite)
        yAxis.textColor = ColorTemplate.rgb("#868795")
        yAxis.textSize = 7f
        mBinding.chartReporting1.axisLeft.setDrawGridLines(true)
        mBinding.chartReporting1.xAxis.setDrawGridLines(false)
        yAxis.setDrawLimitLinesBehindData(true)
        xAxis.setDrawLimitLinesBehindData(true)

        if (type == 1) {
            setData2()
        } else {
            setDataForSingleCompany(model)
        }

        mBinding.chartReporting1.animateX(500)
        val legend = mBinding.chartReporting1.legend
        legend.form = Legend.LegendForm.NONE
    }

    private fun setChart1(type: Int, model: ModelTeamPerformance) {
        mBinding.chartReporting.description.isEnabled = false

        val xAxis: XAxis = mBinding.chartReporting.xAxis
        val yAxis: YAxis = mBinding.chartReporting.axisLeft

        val listDays = ArrayList<String>()
        listDays.add("MON")
        listDays.add("TUE")
        listDays.add("WED")
        listDays.add("THUR")
        listDays.add("FRI")
        listDays.add("SAT")
        listDays.add("SUN")

        val listAmount = ArrayList<String>()
        listAmount.add("0")
        listAmount.add("5K")
        listAmount.add("10K")
        listAmount.add("15K")
        listAmount.add("20K")
        xAxis.axisMaximum = 6f
        xAxis.axisMinimum = 0f
        yAxis.axisMaximum = 4f
        yAxis.axisMinimum = 0f
        setData1()
        xAxis.labelCount = 7
        xAxis.valueFormatter = IAxisValueFormatter { value, axis -> listDays[value.roundToInt()] }
        yAxis.valueFormatter = IAxisValueFormatter { value, axis -> listAmount[value.roundToInt()] }
        mBinding.chartReporting.xAxis.isEnabled = true
        xAxis.position = XAxis.XAxisPosition.BOTTOM
        xAxis.setDrawAxisLine(false)
        xAxis.textSize = 7f
        xAxis.textColor = ColorTemplate.rgb("#868795")
        xAxis.granularity = 1f
        // Y axis data
        mBinding.chartReporting.axisLeft.granularity = 1f
        mBinding.chartReporting.axisRight.isEnabled = false
        mBinding.chartReporting.axisLeft.isEnabled = true
        mBinding.chartReporting.axisLeft.gridColor = resources.getColor(R.color.pic_non_selected)
        mBinding.chartReporting.axisLeft.axisLineColor = resources.getColor(R.color.fulltransparentcolor)
        mBinding.chartReporting.axisRight.axisLineColor = resources.getColor(R.color.colorWhite)
        yAxis.textColor = ColorTemplate.rgb("#868795")
        yAxis.textSize = 7f
        mBinding.chartReporting.axisLeft.setDrawGridLines(true)
        mBinding.chartReporting.xAxis.setDrawGridLines(false)
        yAxis.setDrawLimitLinesBehindData(true)
        xAxis.setDrawLimitLinesBehindData(true)

        if (type == 1) {
            setData1()
        } else {
            setDataForSingleCompany(model)
        }

        mBinding.chartReporting.animateX(500)
        val legend = mBinding.chartReporting.legend
        legend.form = Legend.LegendForm.NONE

    }

    private fun setData4() {

        val values1 = ArrayList<BarEntry>()
        val values2 = ArrayList<BarEntry>()
        val values3 = ArrayList<BarEntry>()
        val values4 = ArrayList<BarEntry>()

        for (i in 0 until 12) {
            values1.add(BarEntry(i.toFloat(), (Math.random() * 2).toFloat()))
            values2.add(BarEntry(i.toFloat(), (Math.random() * 3).toFloat()))
            values3.add(BarEntry(i.toFloat(), (Math.random() * 4).toFloat()))
            values4.add(BarEntry(i.toFloat(), (Math.random() * 5).toFloat()))
        }
        val set1 = BarDataSet(values1, "")
        val set2 = BarDataSet(values2, "")
        val set3 = BarDataSet(values3, "")
        val set4 = BarDataSet(values4, "")

        set1.color = TEAM_COLORS[0]
        set2.color = TEAM_COLORS[1]
        set3.color = TEAM_COLORS[2]
        set4.color = TEAM_COLORS[3]

        set1.setDrawValues(false)
        set2.setDrawValues(false)
        set3.setDrawValues(false)
        set4.setDrawValues(false)

        mBinding.chartReporting2.setLayerType(View.LAYER_TYPE_SOFTWARE, null)
        mBinding.chartReporting2.setScaleEnabled(false)
        set1.formLineWidth = 1f
        set1.formSize = 10f
        set1.valueTextSize = 5f

        set1.isHighlightEnabled = false
        set2.isHighlightEnabled = false
        set3.isHighlightEnabled = false
        set4.isHighlightEnabled = false

        val data = BarData(set1, set2, set3, set4)
        data.setValueFormatter(LargeValueFormatter())
        data.barWidth = .5f

        mBinding.chartReporting2.data = data
        mBinding.chartReporting2.xAxis.axisMaximum = 11f + 0.3f
        mBinding.chartReporting2.xAxis.axisMinimum = 0f - 0.3f
        mBinding.chartReporting2.invalidate()

    }


    private fun setData3() {
        val values1 = ArrayList<BarEntry>()
        val values2 = ArrayList<BarEntry>()
        val values3 = ArrayList<BarEntry>()
        val values4 = ArrayList<BarEntry>()

        for (i in 0 until 12) {
            values1.add(BarEntry(i.toFloat(), (Math.random() * 2).toFloat()))
            values2.add(BarEntry(i.toFloat(), (Math.random() * 3).toFloat()))
            values3.add(BarEntry(i.toFloat(), (Math.random() * 4).toFloat()))
            values4.add(BarEntry(i.toFloat(), (Math.random() * 5).toFloat()))
        }


        val set1 = BarDataSet(values1, "")
        val set2 = BarDataSet(values2, "")
        val set3 = BarDataSet(values3, "")
        val set4 = BarDataSet(values4, "")

        set1.color = TEAM_COLORS[0]
        set2.color = TEAM_COLORS[1]
        set3.color = TEAM_COLORS[2]
        set4.color = TEAM_COLORS[3]

        set1.setDrawValues(false)
        set2.setDrawValues(false)
        set3.setDrawValues(false)
        set4.setDrawValues(false)

        mBinding.chartReporting2.setLayerType(View.LAYER_TYPE_SOFTWARE, null)
        mBinding.chartReporting2.setScaleEnabled(false)

        set1.formLineWidth = 1f
        set1.formSize = 10f
        set1.valueTextSize = 5f

        set1.isHighlightEnabled = false
        set2.isHighlightEnabled = false
        set3.isHighlightEnabled = false
        set4.isHighlightEnabled = false

        val data = BarData(set1, set2, set3, set4)
        data.setValueFormatter(LargeValueFormatter())
        data.barWidth = .5f
        mBinding.chartReporting2.data = data
        mBinding.chartReporting2.xAxis.axisMaximum = 11f + 0.3f
        mBinding.chartReporting2.xAxis.axisMinimum = 0f - 0.3f
        mBinding.chartReporting2.invalidate()

    }

    private fun setData1() {
        val values1 = ArrayList<BarEntry>()
        val values2 = ArrayList<BarEntry>()
        val values3 = ArrayList<BarEntry>()
        val values4 = ArrayList<BarEntry>()

        for (i in 0 until 7) {
            values1.add(BarEntry(i.toFloat(), (Math.random() * 1).toFloat()))
            values2.add(BarEntry(i.toFloat(), (Math.random() * 2).toFloat()))
            values3.add(BarEntry(i.toFloat(), (Math.random() * 3).toFloat()))
            values4.add(BarEntry(i.toFloat(), (Math.random() * 4).toFloat()))
        }
        val set1 = BarDataSet(values1, "")
        val set2 = BarDataSet(values2, "")
        val set3 = BarDataSet(values3, "")
        val set4 = BarDataSet(values4, "")

        set1.color = TEAM_COLORS[0]
        set2.color = TEAM_COLORS[1]
        set3.color = TEAM_COLORS[2]
        set4.color = TEAM_COLORS[3]

        set1.setDrawValues(false)
        set2.setDrawValues(false)
        set3.setDrawValues(false)
        set4.setDrawValues(false)

        mBinding.chartReporting.setLayerType(View.LAYER_TYPE_SOFTWARE, null)
        mBinding.chartReporting.setScaleEnabled(false)

        set1.formLineWidth = 1f
        set1.formSize = 10f
        set1.valueTextSize = 5f

        set1.isHighlightEnabled = false
        set2.isHighlightEnabled = false
        set3.isHighlightEnabled = false
        set4.isHighlightEnabled = false

        val data = BarData(set1, set2, set3, set4)
        data.setValueFormatter(LargeValueFormatter())
        data.barWidth = .5f
        mBinding.chartReporting.data = data
        mBinding.chartReporting.xAxis.axisMaximum = 6f + 0.3f
        mBinding.chartReporting.xAxis.axisMinimum = 0f - 0.3f
        mBinding.chartReporting.invalidate()
    }

    private fun setData2() {
        val values1 = ArrayList<BarEntry>()
        val values2 = ArrayList<BarEntry>()
        val values3 = ArrayList<BarEntry>()
        val values4 = ArrayList<BarEntry>()

        for (i in 0 until 7) {
            values1.add(BarEntry(i.toFloat(), (Math.random() * 3).toFloat()))
            values2.add(BarEntry(i.toFloat(), (Math.random() * 4).toFloat()))
            values3.add(BarEntry(i.toFloat(), (Math.random() * 2).toFloat()))
            values4.add(BarEntry(i.toFloat(), (Math.random() * 1).toFloat()))
        }

        val set1 = BarDataSet(values1, "")
        val set2 = BarDataSet(values2, "")
        val set3 = BarDataSet(values3, "")
        val set4 = BarDataSet(values4, "")

        set1.color = TEAM_COLORS[0]
        set2.color = TEAM_COLORS[1]
        set3.color = TEAM_COLORS[2]
        set4.color = TEAM_COLORS[3]

        set1.setDrawValues(false)
        set2.setDrawValues(false)
        set3.setDrawValues(false)
        set4.setDrawValues(false)

        mBinding.chartReporting1.setLayerType(View.LAYER_TYPE_SOFTWARE, null)
        mBinding.chartReporting1.setScaleEnabled(false)

        set1.formLineWidth = 1f
        set1.formSize = 10f
        set1.valueTextSize = 5f

        set1.isHighlightEnabled = false
        set2.isHighlightEnabled = false
        set3.isHighlightEnabled = false
        set4.isHighlightEnabled = false

        val data = BarData(set1, set2, set3, set4)
        data.setValueFormatter(LargeValueFormatter())
        data.barWidth = .5f
        mBinding.chartReporting1.data = data
        mBinding.chartReporting1.xAxis.axisMaximum = 3f + 0.4f
        mBinding.chartReporting1.xAxis.axisMinimum = 0f - 0.2f
        mBinding.chartReporting1.invalidate()

    }


    private fun setDataForSingleCompany(model: ModelTeamPerformance) {

        val values1 = ArrayList<BarEntry>()


        for (i in 0 until 7) {
            values1.add(BarEntry(i.toFloat(), (Math.random() * 4).toFloat()))
        }

        val set = BarDataSet(values1, "")

        set.color = TEAM_COLORS[0]
        set.color = TEAM_COLORS[1]
        set.color = TEAM_COLORS[2]
        set.color = TEAM_COLORS[3]
        set.setDrawValues(false)

        mBinding.chartReporting.setLayerType(View.LAYER_TYPE_SOFTWARE, null)
        mBinding.chartReporting.setScaleEnabled(false)

        set.formLineWidth = 1f
        set.formSize = 10f
        set.valueTextSize = 5f

        set.isHighlightEnabled = false

        val data = BarData(set)
        data.setValueFormatter(LargeValueFormatter())
        data.barWidth = .5f

        mBinding.chartReporting.data = data
        mBinding.chartReporting.xAxis.axisMaximum = 6f + 0.3f
        mBinding.chartReporting.xAxis.axisMinimum = 0f - 0.3f
        mBinding.chartReporting.invalidate()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        update = context as openFilters
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        update = activity as openFilters
    }
}
