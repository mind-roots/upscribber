package com.upscribber.upscribberSeller.reportingDetail.sales

import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.graphics.DashPathEffect
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.upscribber.util.MyMarkerView
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.IAxisValueFormatter
import com.github.mikephil.charting.formatter.IFillFormatter
import com.github.mikephil.charting.formatter.LargeValueFormatter
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import com.google.android.material.tabs.TabLayout
import com.upscribber.R
import com.upscribber.databinding.FragmentReportSalesBinding
import com.upscribber.upscribberSeller.reportingDetail.openFilters
import kotlinx.android.synthetic.main.toolbar_reporting.view.*
import kotlin.math.roundToInt

class ReportSalesFragment : Fragment(), TabLayout.OnTabSelectedListener {

    lateinit var mBinding: FragmentReportSalesBinding
    val listAmount = ArrayList<String>()
    lateinit var update: openFilters


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_report_sales, container, false)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mBinding.chartReporting1.visibility = View.GONE
        mBinding.chartReporting2.visibility = View.GONE
        mBinding.chartReporting.visibility = View.VISIBLE
        setChart1()
        clickListeners()
        mBinding.tabLayout.tabLayout.addOnTabSelectedListener(this)
    }

    private fun clickListeners() {
        mBinding.filters.setOnClickListener {
            update.setFilters()
        }
    }

    override fun onTabReselected(p0: TabLayout.Tab?) {


    }

    override fun onTabUnselected(p0: TabLayout.Tab?) {

    }

    override fun onTabSelected(p0: TabLayout.Tab?) {

        when {
            p0!!.position == 0 -> {
                mBinding.chartReporting1.visibility = View.GONE
                mBinding.chartReporting2.visibility = View.GONE
                mBinding.chartReporting.visibility = View.VISIBLE
                setChart1()
            }
            p0.position == 1 -> {
                mBinding.chartReporting1.visibility = View.VISIBLE
                mBinding.chartReporting.visibility = View.GONE
                mBinding.chartReporting2.visibility = View.GONE
                setChart2()
            }
            p0.position == 2 -> {
                mBinding.chartReporting1.visibility = View.GONE
                mBinding.chartReporting.visibility = View.GONE
                mBinding.chartReporting2.visibility = View.VISIBLE
                setChart3()
            }
            else -> {
                mBinding.chartReporting1.visibility = View.GONE
                mBinding.chartReporting.visibility = View.GONE
                mBinding.chartReporting2.visibility = View.VISIBLE
                setChart4()
            }
        }

    }

    private fun setChart4() {
        val myMarkerView = MyMarkerView(this.activity!!, R.layout.custom_marker_view)
        myMarkerView.chartView = mBinding.chartReporting2
        mBinding.chartReporting2.marker = myMarkerView
        mBinding.chartReporting2.description.isEnabled = false

        val listAmount = ArrayList<String>()
        listAmount.add("1k")
        listAmount.add("2k")
        listAmount.add("3k")
        listAmount.add("4k")
        listAmount.add("10k")
        listAmount.add("15k")
        listAmount.add("22k")

        val xAxis: XAxis = mBinding.chartReporting2.xAxis
        mBinding.chartReporting2.xAxis.isEnabled = true
        xAxis.position = XAxis.XAxisPosition.BOTTOM
        xAxis.setDrawAxisLine(false)
        xAxis.textSize = 6f

        val listDays3 = ArrayList<String>()
        listDays3.add("Jan")
        listDays3.add("Feb")
        listDays3.add("Mar")
        listDays3.add("April")
        listDays3.add("May")
        listDays3.add("June")
        listDays3.add("July")
        listDays3.add("Aug")
        listDays3.add("Sep")
        listDays3.add("Oct")
        listDays3.add("Nov")
        listDays3.add("Dec")
        xAxis.valueFormatter = IAxisValueFormatter { value, axis -> listDays3[value.roundToInt()] }
        xAxis.axisMaximum = 11f
        xAxis.axisMinimum = 0f
        xAxis.granularity = 1f
        xAxis.textColor= Color.WHITE
        xAxis.textSize = 8f
        xAxis.labelCount = 12
        xAxis.labelRotationAngle = 45F
        xAxis.setDrawLimitLinesBehindData(false)
        val yAxis: YAxis = mBinding.chartReporting2.axisLeft
        mBinding.chartReporting2.axisRight.isEnabled = false
        mBinding.chartReporting2.axisLeft.isEnabled = true


        yAxis.granularity = 1f
        yAxis.axisMinimum = 0f
        yAxis.axisMaximum = 6f
        yAxis.textSize = 8f
        yAxis.textColor = Color.WHITE

        yAxis.valueFormatter = IAxisValueFormatter { value, axis -> listAmount[value.roundToInt()] }

        mBinding.chartReporting2.axisLeft.setDrawGridLines(true)
        mBinding.chartReporting2.axisLeft.axisLineColor = resources.getColor(R.color.fulltransparentcolor)
        mBinding.chartReporting2.axisRight.axisLineColor = resources.getColor(R.color.colorWhite)
        mBinding.chartReporting2.xAxis.setDrawGridLines(false)
        mBinding.chartReporting2.axisLeft.gridColor = resources.getColor(R.color.colorWhite)
        yAxis.setLabelCount(5, true)

        yAxis.setDrawLimitLinesBehindData(false)
        mBinding.chartReporting2.animateX(500)
        setData3()
        val legend = mBinding.chartReporting2.legend
        legend.form = Legend.LegendForm.NONE

    }

    private fun setData3() {
        val valuess = ArrayList<Entry>()
        valuess.add(Entry(0f, 0000f/1000))
        valuess.add(Entry(1f, 1000f/1000))
        valuess.add(Entry(2f, 1200f/1000))
        valuess.add(Entry(3f, 1500f/1000))
        valuess.add(Entry(4f, 2000f/1000))
        valuess.add(Entry(5f, 2500f/1000))
        valuess.add(Entry(6f, 2800f/1000))
        valuess.add(Entry(7f, 3000f/1000))
        valuess.add(Entry(8f, 3500f/1000))
        valuess.add(Entry(9f, 3800f/1000))
        valuess.add(Entry(10f, 4000f/1000))
        valuess.add(Entry(11f, 4500f/1000))
        mBinding.chartReporting2.setLayerType(View.LAYER_TYPE_SOFTWARE, null)
        mBinding.chartReporting2.setScaleEnabled(false)
        mBinding.chartReporting2.xAxis.axisMaximum = 11f + 0.3f
        mBinding.chartReporting2.xAxis.axisMinimum = 0f - 0.3f
        val set1 = LineDataSet(valuess, "")
        set1.setDrawValues(false)


        set1.color = Color.WHITE
        set1.setDrawCircles(false)
        set1.lineWidth = 2f
        set1.setDrawCircleHole(false)
        set1.formLineWidth = 1f
        set1.formLineDashEffect = DashPathEffect(floatArrayOf(10f, 5f), 0f)
        set1.formSize = 15f
        set1.valueTextSize = 10f
        set1.circleRadius = 4.5f
        set1.setCircleColor(Color.parseColor("#7760f2"))
        set1.setDrawHorizontalHighlightIndicator(false)
        set1.setDrawVerticalHighlightIndicator(false)
        set1.setDrawFilled(true)
        set1.fillColor = Color.parseColor("#7760f2")
        set1.fillFormatter = IFillFormatter { _,
                                              _ ->
            mBinding.chartReporting2.axisLeft.axisMinimum
        }

        val dataSets = ArrayList<ILineDataSet>()
        dataSets.add(set1)

        val data = LineData(dataSets)
        data.setValueFormatter(LargeValueFormatter())
//        mBinding.chartReporting.xAxis.axisMaximum = 15f + 0.15f
        mBinding.chartReporting2.data = data
        mBinding.chartReporting2.invalidate()

    }

    private fun setChart3() {
        val myMarkerView = MyMarkerView(this.activity!!, R.layout.custom_marker_view)
        myMarkerView.chartView = mBinding.chartReporting2
        mBinding.chartReporting2.marker = myMarkerView
        mBinding.chartReporting2.description.isEnabled = false

        val listAmount = ArrayList<String>()
        listAmount.add("1k")
        listAmount.add("2k")
        listAmount.add("3k")
        listAmount.add("4k")
        listAmount.add("10k")
        listAmount.add("15k")
        listAmount.add("22k")

        val xAxis: XAxis = mBinding.chartReporting2.xAxis
        mBinding.chartReporting2.xAxis.isEnabled = true
        xAxis.position = XAxis.XAxisPosition.BOTTOM
        xAxis.setDrawAxisLine(false)
        xAxis.textSize = 6f

        val listDays2 = ArrayList<String>()
        listDays2.add("1st Week")
        listDays2.add("2nd Week")
        listDays2.add("3rd week")
        listDays2.add("4th Week")
        listDays2.add("5th Week")
        listDays2.add("6th Week")
        listDays2.add("7th week")
        listDays2.add("8th Week")
        listDays2.add("9th Week")
        listDays2.add("10th Week")
        listDays2.add("11th week")
        listDays2.add("12th Week")
        xAxis.valueFormatter = IAxisValueFormatter { value, axis -> listDays2[value.roundToInt()] }
        xAxis.axisMaximum = 11f
        xAxis.axisMinimum = 0f
        xAxis.granularity = 1f
        xAxis.textColor= Color.WHITE
        xAxis.textSize = 8f
        xAxis.labelCount = 12
        xAxis.labelRotationAngle = 45F
        xAxis.setDrawLimitLinesBehindData(false)

        val yAxis: YAxis = mBinding.chartReporting2.axisLeft
        mBinding.chartReporting2.axisRight.isEnabled = false
        mBinding.chartReporting2.axisLeft.isEnabled = true


        yAxis.granularity = 1f
        yAxis.axisMinimum = 0f
        yAxis.axisMaximum = 6f
        yAxis.textSize = 8f
        yAxis.textColor = Color.WHITE

        yAxis.valueFormatter = IAxisValueFormatter { value, axis -> listAmount[value.roundToInt()] }

        mBinding.chartReporting2.axisLeft.setDrawGridLines(true)
        mBinding.chartReporting2.axisLeft.axisLineColor = resources.getColor(R.color.fulltransparentcolor)
        mBinding.chartReporting2.axisRight.axisLineColor = resources.getColor(R.color.colorWhite)
        mBinding.chartReporting2.xAxis.setDrawGridLines(false)
        mBinding.chartReporting2.axisLeft.gridColor = resources.getColor(R.color.colorWhite)
        yAxis.setLabelCount(5, true)

        yAxis.setDrawLimitLinesBehindData(false)
        mBinding.chartReporting2.animateX(500)
        setData2()
        val legend = mBinding.chartReporting2.legend
        legend.form = Legend.LegendForm.NONE

    }

    private fun setData2() {
        val valuess = ArrayList<Entry>()
        valuess.add(Entry(0f, 0000f/1000))
        valuess.add(Entry(1f, 1000f/1000))
        valuess.add(Entry(2f, 1100f/1000))
        valuess.add(Entry(3f, 1200f/1000))
        valuess.add(Entry(4f, 1500f/1000))
        valuess.add(Entry(5f, 1800f/1000))
        valuess.add(Entry(6f, 2000f/1000))
        valuess.add(Entry(7f, 2200f/1000))
        valuess.add(Entry(8f, 2500f/1000))
        valuess.add(Entry(9f, 3000f/1000))
        valuess.add(Entry(10f, 3500f/1000))
        valuess.add(Entry(11f, 4000f/1000))
        mBinding.chartReporting2.setLayerType(View.LAYER_TYPE_SOFTWARE, null)
        mBinding.chartReporting2.setScaleEnabled(false)
        mBinding.chartReporting2.xAxis.axisMaximum = 11f + 0.2f
        mBinding.chartReporting2.xAxis.axisMinimum = 0f - 0.4f
        val set1 = LineDataSet(valuess, "")
        set1.setDrawValues(false)


        set1.color = Color.WHITE
        set1.setDrawCircles(false)
        set1.lineWidth = 2f
        set1.setDrawCircleHole(false)
        set1.formLineWidth = 1f
        set1.formLineDashEffect = DashPathEffect(floatArrayOf(10f, 5f), 0f)
        set1.formSize = 15f
        set1.valueTextSize = 10f
        set1.circleRadius = 4.5f
        set1.setCircleColor(Color.parseColor("#7760f2"))
        set1.setDrawHorizontalHighlightIndicator(false)
        set1.setDrawVerticalHighlightIndicator(false)
        set1.setDrawFilled(true)
        set1.fillColor = Color.parseColor("#7760f2")
        set1.fillFormatter = IFillFormatter { _,
                                              _ ->
            mBinding.chartReporting2.axisLeft.axisMinimum
        }

        val dataSets = ArrayList<ILineDataSet>()
        dataSets.add(set1)

        val data = LineData(dataSets)
        data.setValueFormatter(LargeValueFormatter())
//        mBinding.chartReporting.xAxis.axisMaximum = 15f + 0.15f
        mBinding.chartReporting2.data = data
        mBinding.chartReporting2.invalidate()

    }

    private fun setChart2() {
        val myMarkerView = MyMarkerView(this.activity!!, R.layout.custom_marker_view)
        myMarkerView.chartView = mBinding.chartReporting1
        mBinding.chartReporting1.marker = myMarkerView
        mBinding.chartReporting1.description.isEnabled = false

        val listAmount = ArrayList<String>()
        listAmount.add("1k")
        listAmount.add("2k")
        listAmount.add("3k")
        listAmount.add("4k")
        listAmount.add("10k")
        listAmount.add("15k")
        listAmount.add("22k")

        val xAxis: XAxis = mBinding.chartReporting1.xAxis
        mBinding.chartReporting1.xAxis.isEnabled = true
        xAxis.position = XAxis.XAxisPosition.BOTTOM
        xAxis.setDrawAxisLine(false)
        xAxis.textSize = 6f

        val listDays = ArrayList<String>()
        listDays.add("1st Week")
        listDays.add("2nd Week")
        listDays.add("3rd Week")
        listDays.add("4th Week")
        xAxis.valueFormatter = IAxisValueFormatter { value, axis -> listDays[value.roundToInt()] }
        xAxis.labelCount = 4
        xAxis.axisMaximum =3f
        xAxis.axisMinimum = 0f
        xAxis.granularity = 1f
        xAxis.textColor= Color.WHITE
        xAxis.textSize = 8f
        xAxis.setDrawLimitLinesBehindData(false)
        val yAxis: YAxis = mBinding.chartReporting1.axisLeft
        mBinding.chartReporting1.axisRight.isEnabled = false
        mBinding.chartReporting1.axisLeft.isEnabled = true


        yAxis.granularity = 1f
        yAxis.axisMinimum = 0f
        yAxis.axisMaximum = 6f
        yAxis.textSize = 8f
        yAxis.textColor = Color.WHITE

        yAxis.valueFormatter = IAxisValueFormatter { value, axis -> listAmount[value.roundToInt()] }

        mBinding.chartReporting1.axisLeft.setDrawGridLines(true)
        mBinding.chartReporting1.axisLeft.axisLineColor = resources.getColor(R.color.fulltransparentcolor)
        mBinding.chartReporting1.axisRight.axisLineColor = resources.getColor(R.color.colorWhite)
        mBinding.chartReporting1.xAxis.setDrawGridLines(false)
        mBinding.chartReporting1.axisLeft.gridColor = resources.getColor(R.color.colorWhite)
        yAxis.setLabelCount(5, true)

        yAxis.setDrawLimitLinesBehindData(false)
        mBinding.chartReporting1.animateX(500)
        setData1()
        val legend = mBinding.chartReporting1.legend
        legend.form = Legend.LegendForm.NONE

    }

    private fun setData1() {
        val valuess = ArrayList<Entry>()
        valuess.add(Entry(0f, 0000f/1000))
        valuess.add(Entry(2f, 4000f/1000))
        valuess.add(Entry(3f, 6000f/1000))
        valuess.add(Entry(4f, 6000f/1000))
        mBinding.chartReporting1.setLayerType(View.LAYER_TYPE_SOFTWARE, null)
        mBinding.chartReporting1.setScaleEnabled(false)
        mBinding.chartReporting1.xAxis.axisMaximum =3f +0.4f
        mBinding.chartReporting1.xAxis.axisMinimum = 0f - 0.4f
        val set1 = LineDataSet(valuess, "")
        set1.setDrawValues(false)


        set1.color = Color.WHITE
        set1.setDrawCircles(false)
        set1.lineWidth = 2f
        set1.setDrawCircleHole(false)
        set1.formLineWidth = 1f
        set1.formLineDashEffect = DashPathEffect(floatArrayOf(10f, 5f), 0f)
        set1.formSize = 15f
        set1.valueTextSize = 10f
        set1.circleRadius = 4.5f
        set1.setCircleColor(Color.parseColor("#7760f2"))
        set1.setDrawHorizontalHighlightIndicator(false)
        set1.setDrawVerticalHighlightIndicator(false)
        set1.setDrawFilled(true)
        set1.fillColor = Color.parseColor("#7760f2")
        set1.fillFormatter = IFillFormatter { _,
                                              _ ->
            mBinding.chartReporting.axisLeft.axisMinimum
        }

        val dataSets = ArrayList<ILineDataSet>()
        dataSets.add(set1)

        val data = LineData(dataSets)
        data.setValueFormatter(LargeValueFormatter())
//        mBinding.chartReporting.xAxis.axisMaximum = 15f + 0.15f
        mBinding.chartReporting1.data = data
        mBinding.chartReporting1.invalidate()

    }

    private fun setChart1() {
        val myMarkerView = MyMarkerView(this.activity!!, R.layout.custom_marker_view)
        myMarkerView.chartView = mBinding.chartReporting
        mBinding.chartReporting.marker = myMarkerView
        mBinding.chartReporting.description.isEnabled = false

        val listAmount = ArrayList<String>()
        listAmount.add("1k")
        listAmount.add("2k")
        listAmount.add("3k")
        listAmount.add("4k")
        listAmount.add("10k")
        listAmount.add("15k")
        listAmount.add("22k")

        val xAxis: XAxis = mBinding.chartReporting.xAxis
        mBinding.chartReporting.xAxis.isEnabled = true
        xAxis.position = XAxis.XAxisPosition.BOTTOM
        xAxis.setDrawAxisLine(false)
        xAxis.textSize = 6f

        val listDays = ArrayList<String>()
        listDays.add("MON")
        listDays.add("TUE")
        listDays.add("WED")
        listDays.add("THUR")
        listDays.add("FRI")
        listDays.add("SAT")
        listDays.add("SUN")
        xAxis.valueFormatter = IAxisValueFormatter { value, axis -> listDays[value.roundToInt()] }
        xAxis.axisMaximum = 6f
        xAxis.axisMinimum = 0f
        xAxis.granularity = 1f
        xAxis.labelCount = 7
        xAxis.textColor= Color.WHITE
        xAxis.textSize = 8f
        xAxis.setDrawLimitLinesBehindData(false)
        val yAxis: YAxis = mBinding.chartReporting.axisLeft
        mBinding.chartReporting.axisRight.isEnabled = false
        mBinding.chartReporting.axisLeft.isEnabled = true


        yAxis.granularity = 1f
        yAxis.axisMinimum = 0f
        yAxis.axisMaximum = 6f
        yAxis.textSize = 8f
        yAxis.textColor = Color.WHITE

        yAxis.valueFormatter = IAxisValueFormatter { value, axis -> listAmount[value.roundToInt()] }

        mBinding.chartReporting.axisLeft.setDrawGridLines(true)
        mBinding.chartReporting.axisLeft.axisLineColor = resources.getColor(R.color.fulltransparentcolor)
        mBinding.chartReporting.axisRight.axisLineColor = resources.getColor(R.color.colorWhite)
        mBinding.chartReporting.xAxis.setDrawGridLines(false)
        mBinding.chartReporting.axisLeft.gridColor = resources.getColor(R.color.colorWhite)
        yAxis.setLabelCount(5, true)

        yAxis.setDrawLimitLinesBehindData(false)
        mBinding.chartReporting.animateX(500)
        setData0()
        val legend = mBinding.chartReporting.legend
        legend.form = Legend.LegendForm.NONE


    }

    private fun setData0() {
        val valuess = ArrayList<Entry>()
        valuess.add(Entry(0f, 0000f/1000))
        valuess.add(Entry(2f, 2000f/1000))
        valuess.add(Entry(3f, 3000f/1000))
        valuess.add(Entry(5f, 4000f/1000))
        valuess.add(Entry(6f, 5000f/1000))
        valuess.add(Entry(7f, 6000f/1000))
        valuess.add(Entry(8f, 7000f/1000))
        valuess.add(Entry(9f, 9000f/1000))
        mBinding.chartReporting.setLayerType(View.LAYER_TYPE_SOFTWARE, null)
        mBinding.chartReporting.setScaleEnabled(false)
        mBinding.chartReporting.xAxis.axisMaximum = 6f + 0.2f
        mBinding.chartReporting.xAxis.axisMinimum = 0f - 0.3f
        val set1 = LineDataSet(valuess, "")
        set1.setDrawValues(false)


        set1.color = Color.WHITE
        set1.setDrawCircles(false)
        set1.lineWidth = 2f
        set1.setDrawCircleHole(false)
        set1.formLineWidth = 1f
        set1.formLineDashEffect = DashPathEffect(floatArrayOf(10f, 5f), 0f)
        set1.formSize = 15f
        set1.valueTextSize = 10f
        set1.circleRadius = 4.5f
        set1.setCircleColor(Color.parseColor("#7760f2"))
        set1.setDrawHorizontalHighlightIndicator(false)
        set1.setDrawVerticalHighlightIndicator(false)
        set1.setDrawFilled(true)
        set1.fillColor = Color.parseColor("#7760f2")
        set1.fillFormatter = IFillFormatter { _,
                                              _ ->
            mBinding.chartReporting.axisLeft.axisMinimum
        }

        val dataSets = ArrayList<ILineDataSet>()
        dataSets.add(set1)

        val data = LineData(dataSets)
        data.setValueFormatter(LargeValueFormatter())
//        mBinding.chartReporting.xAxis.axisMaximum = 15f + 0.15f
        mBinding.chartReporting.data = data
        mBinding.chartReporting.invalidate()

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        update = context as openFilters
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        update = activity as openFilters
    }


}
