package com.upscribber.upscribberSeller.reportingDetail.productPerformance


import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.formatter.IAxisValueFormatter
import com.github.mikephil.charting.formatter.LargeValueFormatter
import com.github.mikephil.charting.utils.ColorTemplate
import com.google.android.material.tabs.TabLayout
import com.upscribber.R
import com.upscribber.databinding.FragmentProductPerformanceBinding
import com.upscribber.upscribberSeller.reportingDetail.openFilters
import kotlinx.android.synthetic.main.toolbar_reporting.view.*
import kotlin.math.roundToInt

class ProductPerformanceFragment : Fragment(), TabLayout.OnTabSelectedListener {

    private lateinit var mViewModel: ProductPerformanceViewModel
    private lateinit var mAdapter: ProductPerfAdapter
    lateinit var update: openFilters

    lateinit var mBinding: FragmentProductPerformanceBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_product_performance, container, false)

        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mViewModel = ViewModelProviders.of(this).get(ProductPerformanceViewModel::class.java)
        setChart1()
        setAdapter()
        clickListeners()
        mBinding.tabLayout.tabLayout.addOnTabSelectedListener(this)
    }

    private fun clickListeners() {
        mBinding.filters.setOnClickListener{
            update.setFilters()
        }

    }

    private fun setAdapter() {
        val manager = LinearLayoutManager(activity)
        manager.orientation = LinearLayout.HORIZONTAL
        mBinding.recyclerProducts.layoutManager = manager
        mAdapter = ProductPerfAdapter(activity!!)
        mBinding.recyclerProducts.adapter = mAdapter
        mViewModel.getTeamsData().observe(this, Observer {
            mAdapter.updateArray(it)
            if (it.size > 3) {
                mBinding.imageView59.visibility = View.VISIBLE
            } else {
                mBinding.imageView59.visibility = View.GONE
            }
        })
    }


    override fun onTabReselected(p0: TabLayout.Tab?) {


    }

    override fun onTabUnselected(p0: TabLayout.Tab?) {

    }

    override fun onTabSelected(p0: TabLayout.Tab?) {
        when {
            p0!!.position == 0 -> {
                mBinding.chartReporting.visibility = View.VISIBLE
                mBinding.chartReporting30.visibility = View.GONE
                setChart1()

            }
            p0.position == 1 -> {
                mBinding.chartReporting.visibility = View.GONE
                mBinding.chartReporting30.visibility = View.VISIBLE
                setChart2()
                setData1()

            }
            p0.position == 2 -> {
                setChart3()
                mBinding.chartReporting.visibility = View.VISIBLE
                mBinding.chartReporting30.visibility = View.GONE

            }
            else -> {
                setChart4()
                mBinding.chartReporting.visibility = View.VISIBLE
                mBinding.chartReporting30.visibility = View.GONE

            }
        }
    }

    private fun setChart4() {
        mBinding.chartReporting.description.isEnabled = false

        val xAxis: XAxis = mBinding.chartReporting.xAxis

        val listDays3 = ArrayList<String>()
        listDays3.add("Jan")
        listDays3.add("Feb")
        listDays3.add("Mar")
        listDays3.add("April")
        listDays3.add("May")
        listDays3.add("June")
        listDays3.add("July")
        listDays3.add("Aug")
        listDays3.add("Sep")
        listDays3.add("Oct")
        listDays3.add("Nov")
        listDays3.add("Dec")

        mBinding.chartReporting.xAxis.isEnabled = false
        xAxis.position = XAxis.XAxisPosition.BOTTOM
        xAxis.setDrawAxisLine(false)
        xAxis.axisMaximum = 68f
        xAxis.axisMinimum = -5f

        // Y axis data
        val yAxis: YAxis = mBinding.chartReporting.axisRight
        mBinding.chartReporting.axisRight.isEnabled = true
        mBinding.chartReporting.axisLeft.isEnabled = false

        yAxis.axisMaximum = 11f
        yAxis.axisMinimum = 0f
        yAxis.textColor = Color.WHITE
        yAxis.textSize = 7f
        yAxis.valueFormatter = IAxisValueFormatter { value, axis -> listDays3[value.roundToInt()] }

        mBinding.chartReporting.axisRight.setDrawGridLines(true)
        mBinding.chartReporting.xAxis.setDrawGridLines(false)

        mBinding.chartReporting.axisLeft.axisLineColor = resources.getColor(R.color.invite_edit_text)
        mBinding.chartReporting.axisRight.axisLineColor = resources.getColor(R.color.fulltransparentcolor)
        mBinding.chartReporting.axisRight.gridColor = resources.getColor(R.color.invite_edit_text)
        yAxis.setDrawLimitLinesBehindData(true)
        xAxis.setDrawLimitLinesBehindData(true)
        yAxis.labelCount =11
        xAxis.labelCount = 8
        setData(3)

        mBinding.chartReporting.animateX(500)


        val legend = mBinding.chartReporting.legend
        legend.form = Legend.LegendForm.NONE

    }

    private fun setChart3() {
        mBinding.chartReporting.description.isEnabled = false

        val xAxis: XAxis = mBinding.chartReporting.xAxis

        val listDays2 = ArrayList<String>()
        listDays2.add("1st Week")
        listDays2.add("2nd Week")
        listDays2.add("3rd week")
        listDays2.add("4th Week")
        listDays2.add("5th Week")
        listDays2.add("6th Week")
        listDays2.add("7th week")
        listDays2.add("8th Week")
        listDays2.add("9th Week")
        listDays2.add("10th Week")
        listDays2.add("11th week")
        listDays2.add("12th Week")

        mBinding.chartReporting.xAxis.isEnabled = false
        xAxis.position = XAxis.XAxisPosition.BOTTOM
        xAxis.setDrawAxisLine(false)
        xAxis.axisMaximum = 68f
        xAxis.axisMinimum = -5f

        // Y axis data
        val yAxis: YAxis = mBinding.chartReporting.axisRight
        mBinding.chartReporting.axisRight.isEnabled = true
        mBinding.chartReporting.axisLeft.isEnabled = false

        yAxis.axisMaximum = 11f
        yAxis.axisMinimum = 0f
        yAxis.textColor = Color.WHITE
        yAxis.textSize = 7f
        yAxis.valueFormatter = IAxisValueFormatter { value, axis -> listDays2[value.roundToInt()] }
        mBinding.chartReporting.axisRight.setDrawGridLines(true)
        mBinding.chartReporting.xAxis.setDrawGridLines(false)

        mBinding.chartReporting.axisLeft.axisLineColor = resources.getColor(R.color.invite_edit_text)
        mBinding.chartReporting.axisRight.axisLineColor = resources.getColor(R.color.fulltransparentcolor)
        mBinding.chartReporting.axisRight.gridColor = resources.getColor(R.color.invite_edit_text)
        yAxis.setDrawLimitLinesBehindData(true)
        xAxis.setDrawLimitLinesBehindData(true)
        yAxis.labelCount =11
        xAxis.labelCount = 8
        setData(2)

        mBinding.chartReporting.animateX(500)


        val legend = mBinding.chartReporting.legend
        legend.form = Legend.LegendForm.NONE


    }

    private fun setChart2() {
        mBinding.chartReporting30.description.isEnabled = false

        val xAxis: XAxis = mBinding.chartReporting30.xAxis

        val listAmount = ArrayList<String>()
        listAmount.add("1st Week")
        listAmount.add("2nd Week")
        listAmount.add("3rd Week")
        listAmount.add("4th Week")

        mBinding.chartReporting30.xAxis.isEnabled = false
        xAxis.position = XAxis.XAxisPosition.BOTTOM
        xAxis.setDrawAxisLine(false)
        xAxis.axisMaximum = 68f
        xAxis.axisMinimum = -5f

        // Y axis data
        val yAxis: YAxis = mBinding.chartReporting30.axisRight
        mBinding.chartReporting30.axisRight.isEnabled = true
        mBinding.chartReporting30.axisLeft.isEnabled = false

        yAxis.axisMaximum = 3f
        yAxis.axisMinimum = 0f
        yAxis.textColor = Color.WHITE
        yAxis.textSize = 7f
        yAxis.valueFormatter = IAxisValueFormatter { value, axis -> listAmount[value.roundToInt()] }

        mBinding.chartReporting30.axisRight.setDrawGridLines(true)
        mBinding.chartReporting30.xAxis.setDrawGridLines(false)

        mBinding.chartReporting30.axisLeft.axisLineColor = resources.getColor(R.color.invite_edit_text)
        mBinding.chartReporting30.axisRight.axisLineColor = resources.getColor(R.color.fulltransparentcolor)
        mBinding.chartReporting30.axisRight.gridColor = resources.getColor(R.color.invite_edit_text)
        yAxis.setDrawLimitLinesBehindData(true)
        xAxis.setDrawLimitLinesBehindData(true)
        yAxis.labelCount = 3
        xAxis.labelCount = 3
        setData1()

        mBinding.chartReporting30.animateX(500)


        val legend = mBinding.chartReporting30.legend
        legend.form = Legend.LegendForm.NONE


    }

    private fun setData1() {
        val values0 = ArrayList<BarEntry>()
        val values50 = ArrayList<BarEntry>()
        val values40 = ArrayList<BarEntry>()
        val values42 = ArrayList<BarEntry>()
        val values56 = ArrayList<BarEntry>()
        val values70 = ArrayList<BarEntry>()
        val values71 = ArrayList<BarEntry>()

        values0.add(BarEntry(0f, 700f / 1))
        values50.add(BarEntry(10f, 900f / 1))
        values40.add(BarEntry(20f, 1000f / 1))
        values42.add(BarEntry(30f, 1500f / 1))
        values56.add(BarEntry(40f, 1700f / 1))
        values70.add(BarEntry(50f, 1900f / 1))
        values71.add(BarEntry(60f, 2000f / 1))

        val set1 = BarDataSet(values0, "")
        val set2 = BarDataSet(values50, "")
        val set3 = BarDataSet(values40, "")
        val set4 = BarDataSet(values42, "")
        val set5 = BarDataSet(values56, "")
        val set6 = BarDataSet(values70, "")
        val set7 = BarDataSet(values71, "")

        set1.color = ColorTemplate.rgb("#6981ec")
        set2.color = ColorTemplate.rgb("#89939a")
        set3.color = ColorTemplate.rgb("#C5CAE9")
        set4.color = ColorTemplate.rgb("#fcc117")
        set5.color = ColorTemplate.rgb("#02CBCD")
        set6.color = ColorTemplate.rgb("#ff3f55")
        set7.color = ColorTemplate.rgb("#F8BBD0")


        set1.setDrawValues(false)
        set2.setDrawValues(false)
        set3.setDrawValues(false)
        set4.setDrawValues(false)
        set5.setDrawValues(false)
        set6.setDrawValues(false)
        set7.setDrawValues(false)
        mBinding.chartReporting30.setLayerType(View.LAYER_TYPE_SOFTWARE, null)
        mBinding.chartReporting30.setScaleEnabled(false)

        set1.formLineWidth = 1f
        set1.formSize = 10f
        set1.valueTextSize = 5f
        set1.isHighlightEnabled = false

        set2.formLineWidth = 1f
        set2.formSize = 10f
        set2.valueTextSize = 5f
        set2.isHighlightEnabled = false

        set3.formLineWidth = 1f
        set3.formSize = 10f
        set3.valueTextSize = 5f
        set3.isHighlightEnabled = false

        set4.formLineWidth = 1f
        set4.formSize = 10f
        set4.valueTextSize = 5f
        set4.isHighlightEnabled = false

        set5.formLineWidth = 1f
        set5.formSize = 10f
        set5.valueTextSize = 5f
        set5.isHighlightEnabled = false

        set6.formLineWidth = 1f
        set6.formSize = 10f
        set6.valueTextSize = 5f
        set6.isHighlightEnabled = false

        set7.formLineWidth = 1f
        set7.formSize = 10f
        set7.valueTextSize = 5f
        set7.isHighlightEnabled = false


        val data = BarData(set1, set2, set3, set4, set5, set6, set7)
        data.setValueFormatter(LargeValueFormatter())
        data.barWidth = 4f
        //            data.groupBars(2f, groupSpace, barSpace)
        mBinding.chartReporting30.data = data
        mBinding.chartReporting30.invalidate()

    }

    @RequiresApi(Build.VERSION_CODES.HONEYCOMB)
    private fun setChart1() {
        mBinding.chartReporting.description.isEnabled = false

        val xAxis: XAxis = mBinding.chartReporting.xAxis

        val listAmount = ArrayList<String>()
        listAmount.add("Mon")
        listAmount.add("Tues")
        listAmount.add("Wed")
        listAmount.add("Thur")
        listAmount.add("Fri")
        listAmount.add("Sat")
        listAmount.add("Sun")



        mBinding.chartReporting.xAxis.isEnabled = false
        xAxis.position = XAxis.XAxisPosition.BOTTOM
        xAxis.setDrawAxisLine(false)
        xAxis.axisMaximum = 68f
        xAxis.axisMinimum = -5f

        // Y axis data
        val yAxis: YAxis = mBinding.chartReporting.axisRight
        mBinding.chartReporting.axisRight.isEnabled = true
        mBinding.chartReporting.axisLeft.isEnabled = false

        yAxis.axisMaximum = 6f
        yAxis.axisMinimum = 0f
        yAxis.textColor = Color.WHITE
        yAxis.textSize = 7f
        yAxis.valueFormatter = IAxisValueFormatter { value, axis -> listAmount[value.roundToInt()] }

        mBinding.chartReporting.axisRight.setDrawGridLines(true)
        mBinding.chartReporting.xAxis.setDrawGridLines(false)

        mBinding.chartReporting.axisLeft.axisLineColor = resources.getColor(R.color.invite_edit_text)
        mBinding.chartReporting.axisRight.axisLineColor = resources.getColor(R.color.fulltransparentcolor)
        mBinding.chartReporting.axisRight.gridColor = resources.getColor(R.color.invite_edit_text)
        yAxis.setDrawLimitLinesBehindData(true)
        xAxis.setDrawLimitLinesBehindData(true)
        yAxis.labelCount = 7
        xAxis.labelCount = 8
        setData(0)

        mBinding.chartReporting.animateX(500)


        val legend = mBinding.chartReporting.legend
        legend.form = Legend.LegendForm.NONE

    }


    private fun setData(position: Int) {
        when (position) {
            0 -> {
                val values0 = ArrayList<BarEntry>()
                val values50 = ArrayList<BarEntry>()
                val values40 = ArrayList<BarEntry>()
                val values42 = ArrayList<BarEntry>()
                val values56 = ArrayList<BarEntry>()
                val values70 = ArrayList<BarEntry>()
                val values71 = ArrayList<BarEntry>()

                values0.add(BarEntry(0f, 700f / 1))
                values50.add(BarEntry(10f, 900f / 1))
                values40.add(BarEntry(20f, 1000f / 1))
                values42.add(BarEntry(30f, 1500f / 1))
                values56.add(BarEntry(40f, 1700f / 1))
                values70.add(BarEntry(50f, 1900f / 1))
                values71.add(BarEntry(60f, 2000f / 1))

                val set1 = BarDataSet(values0, "")
                val set2 = BarDataSet(values50, "")
                val set3 = BarDataSet(values40, "")
                val set4 = BarDataSet(values42, "")
                val set5 = BarDataSet(values56, "")
                val set6 = BarDataSet(values70, "")
                val set7 = BarDataSet(values71, "")

                set1.color = ColorTemplate.rgb("#6981ec")
                set2.color = ColorTemplate.rgb("#89939a")
                set3.color = ColorTemplate.rgb("#C5CAE9")
                set4.color = ColorTemplate.rgb("#fcc117")
                set5.color = ColorTemplate.rgb("#02CBCD")
                set6.color = ColorTemplate.rgb("#ff3f55")
                set7.color = ColorTemplate.rgb("#F8BBD0")


                set1.setDrawValues(false)
                set2.setDrawValues(false)
                set3.setDrawValues(false)
                set4.setDrawValues(false)
                set5.setDrawValues(false)
                set6.setDrawValues(false)
                set7.setDrawValues(false)
                mBinding.chartReporting.setLayerType(View.LAYER_TYPE_SOFTWARE, null)
                mBinding.chartReporting.setScaleEnabled(false)

                set1.formLineWidth = 1f
                set1.formSize = 10f
                set1.valueTextSize = 5f
                set1.isHighlightEnabled = false

                set2.formLineWidth = 1f
                set2.formSize = 10f
                set2.valueTextSize = 5f
                set2.isHighlightEnabled = false

                set3.formLineWidth = 1f
                set3.formSize = 10f
                set3.valueTextSize = 5f
                set3.isHighlightEnabled = false

                set4.formLineWidth = 1f
                set4.formSize = 10f
                set4.valueTextSize = 5f
                set4.isHighlightEnabled = false

                set5.formLineWidth = 1f
                set5.formSize = 10f
                set5.valueTextSize = 5f
                set5.isHighlightEnabled = false

                set6.formLineWidth = 1f
                set6.formSize = 10f
                set6.valueTextSize = 5f
                set6.isHighlightEnabled = false

                set7.formLineWidth = 1f
                set7.formSize = 10f
                set7.valueTextSize = 5f
                set7.isHighlightEnabled = false


                val data = BarData(set1, set2, set3, set4, set5, set6, set7)
                data.setValueFormatter(LargeValueFormatter())
                data.barWidth = 4f
    //            data.groupBars(2f, groupSpace, barSpace)
                mBinding.chartReporting.data = data
                mBinding.chartReporting.invalidate()
            }
            1 -> {
                val values1 = ArrayList<BarEntry>()
                val values2 = ArrayList<BarEntry>()
                val values3 = ArrayList<BarEntry>()
                val values4 = ArrayList<BarEntry>()
                val values5 = ArrayList<BarEntry>()
                val values6 = ArrayList<BarEntry>()
                val values7 = ArrayList<BarEntry>()
                val values8 = ArrayList<BarEntry>()
                val values9 = ArrayList<BarEntry>()
                values1.add(BarEntry(0f, 500f / 1))
                values2.add(BarEntry(10f, 700f / 1))
                values3.add(BarEntry(20f, 900f / 1))
                values4.add(BarEntry(30f, 1100f / 1))
                values5.add(BarEntry(40f, 1300f / 1))
                values6.add(BarEntry(50f, 1400f / 1))
                values7.add(BarEntry(60f, 1600f / 1))
                values8.add(BarEntry(70f, 1900f / 1))
                values9.add(BarEntry(80f, 2000f / 1))
                val set1 = BarDataSet(values1, "")
                val set2 = BarDataSet(values2, "")
                val set3 = BarDataSet(values3, "")
                val set4 = BarDataSet(values4, "")
                val set5 = BarDataSet(values5, "")
                val set6 = BarDataSet(values6, "")
                val set7 = BarDataSet(values7, "")
                val set8 = BarDataSet(values8, "")
                val set9 = BarDataSet(values9, "")
                set1.color = ColorTemplate.rgb("#02CBCD")
                set2.color = ColorTemplate.rgb("#ff3f55")
                set3.color = ColorTemplate.rgb("#F8BBD0")
                set4.color = ColorTemplate.rgb("#b0003a")
                set5.color = ColorTemplate.rgb("#7760f2")
                set6.color = ColorTemplate.rgb("#6981ec")
                set7.color = ColorTemplate.rgb("#89939a")
                set8.color = ColorTemplate.rgb("#C5CAE9")
                set9.color = ColorTemplate.rgb("#fcc117")

                set1.setDrawValues(false)
                set2.setDrawValues(false)
                set3.setDrawValues(false)
                set4.setDrawValues(false)
                set5.setDrawValues(false)
                set6.setDrawValues(false)
                set7.setDrawValues(false)
                set8.setDrawValues(false)
                set9.setDrawValues(false)
                mBinding.chartReporting.setLayerType(View.LAYER_TYPE_SOFTWARE, null)
                mBinding.chartReporting.setScaleEnabled(false)
                set1.formLineWidth = 1f
                set1.formSize = 10f
                set1.valueTextSize = 5f
                set1.isHighlightEnabled = false

                set2.formLineWidth = 1f
                set2.formSize = 10f
                set2.valueTextSize = 5f
                set2.isHighlightEnabled = false

                set3.formLineWidth = 1f
                set3.formSize = 10f
                set3.valueTextSize = 5f
                set3.isHighlightEnabled = false

                set4.formLineWidth = 1f
                set4.formSize = 10f
                set4.valueTextSize = 5f
                set4.isHighlightEnabled = false

                set5.formLineWidth = 1f
                set5.formSize = 10f
                set5.valueTextSize = 5f
                set5.isHighlightEnabled = false

                set6.formLineWidth = 1f
                set6.formSize = 10f
                set6.valueTextSize = 5f
                set6.isHighlightEnabled = false

                set7.formLineWidth = 1f
                set7.formSize = 10f
                set7.valueTextSize = 5f
                set7.isHighlightEnabled = false

                set8.formLineWidth = 1f
                set8.formSize = 10f
                set8.valueTextSize = 5f
                set8.isHighlightEnabled = false

                set9.formLineWidth = 1f
                set9.formSize = 10f
                set9.valueTextSize = 5f
                set9.isHighlightEnabled = false


                val data = BarData(set1, set2, set3, set4, set5, set6, set7, set8, set9)
                data.setValueFormatter(LargeValueFormatter())
                data.barWidth = 4f
    //            data.groupBars(2f, groupSpace, barSpace)
                mBinding.chartReporting.data = data
                mBinding.chartReporting.invalidate()
            }
            2 -> {
                val values1 = ArrayList<BarEntry>()
                val values2 = ArrayList<BarEntry>()
                val values3 = ArrayList<BarEntry>()
                val values4 = ArrayList<BarEntry>()
                val values5 = ArrayList<BarEntry>()
                val values6 = ArrayList<BarEntry>()
                val values7 = ArrayList<BarEntry>()
                val values8 = ArrayList<BarEntry>()
                val values9 = ArrayList<BarEntry>()
                values1.add(BarEntry(0f, 11000f / 1))
                values2.add(BarEntry(10f, 13000f / 1))
                values3.add(BarEntry(20f, 15000f / 1))
                values4.add(BarEntry(30f, 20000f / 1))
                values5.add(BarEntry(40f, 25000f / 1))
                values6.add(BarEntry(50f, 14000f / 1))
                values7.add(BarEntry(60f, 16000f / 1))
                values8.add(BarEntry(70f, 19000f / 1))
                values9.add(BarEntry(80f, 2000f / 1))
                val set1 = BarDataSet(values1, "")
                val set2 = BarDataSet(values2, "")
                val set3 = BarDataSet(values3, "")
                val set4 = BarDataSet(values4, "")
                val set5 = BarDataSet(values5, "")
                val set6 = BarDataSet(values6, "")
                val set7 = BarDataSet(values7, "")
                val set8 = BarDataSet(values8, "")
                val set9 = BarDataSet(values9, "")
                set1.color = ColorTemplate.rgb("#02CBCD")
                set2.color = ColorTemplate.rgb("#ff3f55")
                set3.color = ColorTemplate.rgb("#F8BBD0")
                set4.color = ColorTemplate.rgb("#b0003a")
                set5.color = ColorTemplate.rgb("#7760f2")
                set6.color = ColorTemplate.rgb("#6981ec")
                set7.color = ColorTemplate.rgb("#89939a")
                set8.color = ColorTemplate.rgb("#C5CAE9")
                set9.color = ColorTemplate.rgb("#fcc117")

                set1.setDrawValues(false)
                set2.setDrawValues(false)
                set3.setDrawValues(false)
                set4.setDrawValues(false)
                set5.setDrawValues(false)
                set6.setDrawValues(false)
                set7.setDrawValues(false)
                set8.setDrawValues(false)
                set9.setDrawValues(false)
                mBinding.chartReporting.setLayerType(View.LAYER_TYPE_SOFTWARE, null)
                mBinding.chartReporting.setScaleEnabled(false)
                set1.formLineWidth = 1f
                set1.formSize = 10f
                set1.valueTextSize = 5f
                set1.isHighlightEnabled = false

                set2.formLineWidth = 1f
                set2.formSize = 10f
                set2.valueTextSize = 5f
                set2.isHighlightEnabled = false

                set3.formLineWidth = 1f
                set3.formSize = 10f
                set3.valueTextSize = 5f
                set3.isHighlightEnabled = false

                set4.formLineWidth = 1f
                set4.formSize = 10f
                set4.valueTextSize = 5f
                set4.isHighlightEnabled = false

                set5.formLineWidth = 1f
                set5.formSize = 10f
                set5.valueTextSize = 5f
                set5.isHighlightEnabled = false

                set6.formLineWidth = 1f
                set6.formSize = 10f
                set6.valueTextSize = 5f
                set6.isHighlightEnabled = false

                set2.formLineWidth = 1f
                set2.formSize = 10f
                set2.valueTextSize = 5f
                set2.isHighlightEnabled = false

                set7.formLineWidth = 1f
                set7.formSize = 10f
                set7.valueTextSize = 5f
                set7.isHighlightEnabled = false

                set8.formLineWidth = 1f
                set8.formSize = 10f
                set8.valueTextSize = 5f
                set8.isHighlightEnabled = false

                set9.formLineWidth = 1f
                set9.formSize = 10f
                set9.valueTextSize = 5f
                set9.isHighlightEnabled = false


                val data = BarData(set1, set2, set3, set4, set5, set6, set7, set8, set9)
                data.setValueFormatter(LargeValueFormatter())
                data.barWidth = 4f
    //            data.groupBars(-0.2f, groupSpace, barSpace)
                mBinding.chartReporting.data = data
                mBinding.chartReporting.invalidate()
            }
            else -> {

                val values1 = ArrayList<BarEntry>()
                val values2 = ArrayList<BarEntry>()
                val values3 = ArrayList<BarEntry>()
                val values4 = ArrayList<BarEntry>()
                val values5 = ArrayList<BarEntry>()
                val values6 = ArrayList<BarEntry>()
                val values7 = ArrayList<BarEntry>()
                val values8 = ArrayList<BarEntry>()
                val values9 = ArrayList<BarEntry>()
                values1.add(BarEntry(0f, 1100f / 1))
                values2.add(BarEntry(10f, 1300f / 1))
                values3.add(BarEntry(20f, 1000f / 1))
                values4.add(BarEntry(30f, 2000f / 1))
                values5.add(BarEntry(40f, 2500f / 1))
                values6.add(BarEntry(50f, 1400f / 1))
                values7.add(BarEntry(60f, 1600f / 1))
                values8.add(BarEntry(70f, 3600f / 1))
                values9.add(BarEntry(80f, 2000f / 1))
                val set1 = BarDataSet(values1, "")
                val set2 = BarDataSet(values2, "")
                val set3 = BarDataSet(values3, "")
                val set4 = BarDataSet(values4, "")
                val set5 = BarDataSet(values5, "")
                val set6 = BarDataSet(values6, "")
                val set7 = BarDataSet(values7, "")
                val set8 = BarDataSet(values8, "")
                val set9 = BarDataSet(values9, "")
                set1.color = ColorTemplate.rgb("#02CBCD")
                set2.color = ColorTemplate.rgb("#ff3f55")
                set3.color = ColorTemplate.rgb("#F8BBD0")
                set4.color = ColorTemplate.rgb("#b0003a")
                set5.color = ColorTemplate.rgb("#7760f2")
                set6.color = ColorTemplate.rgb("#6981ec")
                set7.color = ColorTemplate.rgb("#89939a")
                set8.color = ColorTemplate.rgb("#C5CAE9")
                set9.color = ColorTemplate.rgb("#fcc117")

                set1.setDrawValues(false)
                set2.setDrawValues(false)
                set3.setDrawValues(false)
                set4.setDrawValues(false)
                set5.setDrawValues(false)
                set6.setDrawValues(false)
                set7.setDrawValues(false)
                set8.setDrawValues(false)
                set9.setDrawValues(false)
                mBinding.chartReporting.setLayerType(View.LAYER_TYPE_SOFTWARE, null)
                mBinding.chartReporting.setScaleEnabled(false)
                set1.formLineWidth = 1f
                set1.formSize = 10f
                set1.valueTextSize = 5f
                set1.isHighlightEnabled = false

                set2.formLineWidth = 1f
                set2.formSize = 10f
                set2.valueTextSize = 5f
                set2.isHighlightEnabled = false

                set3.formLineWidth = 1f
                set3.formSize = 10f
                set3.valueTextSize = 5f
                set3.isHighlightEnabled = false

                set4.formLineWidth = 1f
                set4.formSize = 10f
                set4.valueTextSize = 5f
                set4.isHighlightEnabled = false

                set5.formLineWidth = 1f
                set5.formSize = 10f
                set5.valueTextSize = 5f
                set5.isHighlightEnabled = false

                set6.formLineWidth = 1f
                set6.formSize = 10f
                set6.valueTextSize = 5f
                set6.isHighlightEnabled = false

                set2.formLineWidth = 1f
                set2.formSize = 10f
                set2.valueTextSize = 5f
                set2.isHighlightEnabled = false

                set7.formLineWidth = 1f
                set7.formSize = 10f
                set7.valueTextSize = 5f
                set7.isHighlightEnabled = false

                set8.formLineWidth = 1f
                set8.formSize = 10f
                set8.valueTextSize = 5f
                set8.isHighlightEnabled = false

                set9.formLineWidth = 1f
                set9.formSize = 10f
                set9.valueTextSize = 5f
                set9.isHighlightEnabled = false


                val data = BarData(set1, set2, set3, set4, set5, set6, set7, set8, set9)
                data.setValueFormatter(LargeValueFormatter())
                data.barWidth = 4f
    //            data.groupBars(-0.2f, groupSpace, barSpace)
                mBinding.chartReporting.data = data
                mBinding.chartReporting.invalidate()


            }
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        update = context as openFilters
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        update = activity as openFilters
    }

}
