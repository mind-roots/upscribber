package com.upscribber.upscribberSeller.reportingDetail

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.transition.Slide
import android.transition.TransitionInflater
import android.util.Log
import android.view.Gravity
import android.view.MenuItem
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.ActionBar
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.upscribber.upscribberSeller.reportingDetail.customers.ReportCustomersFragment
import com.upscribber.upscribberSeller.reportingDetail.productPerformance.ProductPerformanceFragment
import com.upscribber.upscribberSeller.reportingDetail.rating.ReportRatingFragment
import com.upscribber.upscribberSeller.reportingDetail.redemption.ReportRedemptionFragment
import com.upscribber.upscribberSeller.reportingDetail.sales.ReportSalesFragment
import com.upscribber.upscribberSeller.reportingDetail.subscription.ReportSubscriptionFragment
import com.upscribber.upscribberSeller.reportingDetail.teamPerformance.TeamPerformanceFragment
import com.upscribber.upscribberSeller.reportingDetail.views.ReportViewsFragment
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.upscribber.commonClasses.BusProvider
import com.upscribber.R
import com.upscribber.databinding.ActivityReportingBinding
import com.upscribber.filters.*
import com.upscribber.filters.Redemption.RedemptionFilterFragment
import kotlinx.android.synthetic.main.activity_reporting.*
import kotlinx.android.synthetic.main.filter_header.*

@Suppress("DEPRECATION")
class ReportingActivity : AppCompatActivity(), ReportingClickListener, FilterInterface,
    DrawerLayout.DrawerListener, FilterFragment.SetFilterHeaderBackground, openFilters {

    private var bottomSheetBehavior: BottomSheetBehavior<LinearLayout>? = null
    var intentVal: Int = 0
    lateinit var selectedModel: ModelOptions
    lateinit var mBinding: ActivityReportingBinding
    lateinit var mViewModel: ReportingViewModel
    lateinit var adapterReport: ReportingDetailAdapter
    lateinit var preFragment: Fragment
    lateinit var actionbar: ActionBar
    var mData: ArrayList<ModelOptions> = ArrayList()
    var shofull = 0
    private lateinit var btnFilter: TextView
    private lateinit var mfilters: TextView
    var drawerOpen = 0
    private lateinit var filter_done: TextView
    private lateinit var fragmentManager: FragmentManager
    private lateinit var fragmentTransaction: FragmentTransaction
    private lateinit var mreset: TextView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_reporting)
        mViewModel = ViewModelProviders.of(this).get(ReportingViewModel::class.java)
        initz()
        listOptions.layoutManager = LinearLayoutManager(this)
        adapterReport = ReportingDetailAdapter(this)
        listOptions.adapter = adapterReport
        mBinding.drawerLayout1.addDrawerListener(this)
        intentVal = intent.getIntExtra("value", 0)
        mViewModel.getOptions(intentVal).observe(this, Observer {
            adapterReport.update(it)
            mBinding.title.text = "Sales"
            mBinding.title.setTextColor(resources.getColor(R.color.colorWhite))
            inflateInitialFragments(this.intentVal)
        })

        setToolbar()

        bottomSheetBehavior = BottomSheetBehavior.from(mBinding.bottomSheet)
        bottomSheetBehavior!!.peekHeight = resources.getDimension(R.dimen.reporting_peek).toInt()

        mBinding.showHideBottomSheet.setOnClickListener {
            if (shofull == 0) {
                bottomSheetBehavior!!.state = BottomSheetBehavior.STATE_EXPANDED
                mBinding.line.visibility = View.GONE
                mBinding.downImage.visibility = View.VISIBLE
                shofull = 1
            } else {
                shofull = 0
                mBinding.line.visibility = View.VISIBLE
                mBinding.downImage.visibility = View.GONE
                bottomSheetBehavior!!.state = BottomSheetBehavior.STATE_COLLAPSED
            }
        }
        bottomSheetBehavior!!.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            @SuppressLint("SwitchIntDef")
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                // React to state change
                when (newState) {
                    BottomSheetBehavior.STATE_COLLAPSED -> {
                        mBinding.line.visibility = View.VISIBLE
                        mBinding.downImage.visibility = View.GONE
                        shofull = 0
                    }

                    BottomSheetBehavior.STATE_DRAGGING -> {

                    }

                    BottomSheetBehavior.STATE_EXPANDED -> {
                        shofull = 1
                        mBinding.line.visibility = View.GONE
                        mBinding.downImage.visibility = View.VISIBLE
                    }

                    BottomSheetBehavior.STATE_HIDDEN -> {

                    }
                    BottomSheetBehavior.STATE_SETTLING -> {

                    }


                }
            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {
                // React to dragging events
                Log.e("onSlide", "onSlide")
            }
        })

    }

    private fun initz() {

        mreset = findViewById(R.id.reset)
        mfilters = findViewById(R.id.filters)
        filter_done = findViewById(R.id.filter_done)
    }

    override fun setFilters() {

        drawerOpen = 1
        mBinding.drawerLayout1.openDrawer(GravityCompat.END)
        inflateFragment(FilterFragment(this))
        filterImgBack.visibility = View.GONE
        mfilters.text = "Filters"
        mreset.visibility = View.VISIBLE

        filter_done.setOnClickListener {
           onBackPressed()
        }


    }


    private fun inflateFragment(fragment: Fragment) {
        fragmentManager = supportFragmentManager
        this.fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.containerSide1, fragment)
        fragmentTransaction.commit()

    }

    override fun onBackPressed() {
        if (drawerOpen == 1) {
            drawerOpen = 0
            mBinding.drawerLayout1.closeDrawer(GravityCompat.END)
        } else {
            super.onBackPressed()
        }
    }

    override fun onPause() {
        BusProvider.getInstance().unregister(this)
        super.onPause()
    }

    override fun onResume() {
        BusProvider.getInstance().register(this)
        super.onResume()
    }


    // inflate fragments logic
    private fun inflateInitialFragments(intentVal: Int) {
        when (intentVal) {
            1 -> replaceFragment2(ReportSalesFragment(), R.color.colorBlack, actionbar.apply {
                setDisplayHomeAsUpEnabled(true)
                setHomeAsUpIndicator(R.drawable.ic_arrow_back_white)
                mBinding.title.setTextColor(resources.getColor(R.color.colorWhite))
                mBinding.title.text = "Sales"
            })
            2 -> replaceFragment2(ReportSubscriptionFragment(), R.color.colorBlack, actionbar.apply {
                setDisplayHomeAsUpEnabled(true)
                setHomeAsUpIndicator(R.drawable.ic_arrow_back_white)
                mBinding.title.setTextColor(resources.getColor(R.color.colorWhite))
                mBinding.title.text = "Products"
            })
            3 -> replaceFragment2(ReportViewsFragment(), R.color.colorBlack, actionbar.apply {
                setDisplayHomeAsUpEnabled(true)
                setHomeAsUpIndicator(R.drawable.ic_arrow_back_white)
                mBinding.title.setTextColor(resources.getColor(R.color.colorWhite))
                mBinding.title.text = "Views"
            })
            4 -> replaceFragment2(ReportCustomersFragment(), R.color.colorBlack, actionbar.apply {
                setDisplayHomeAsUpEnabled(true)
                setHomeAsUpIndicator(R.drawable.ic_arrow_back_white)
                mBinding.title.setTextColor(resources.getColor(R.color.colorWhite))
                mBinding.title.text = "Customers"
            })
            5 -> replaceFragment2(ProductPerformanceFragment(), R.color.colorBlack, actionbar.apply {
                setDisplayHomeAsUpEnabled(true)
                setHomeAsUpIndicator(R.drawable.ic_arrow_back_white)
                mBinding.title.setTextColor(resources.getColor(R.color.colorWhite))
                mBinding.title.text = "Shares"
            })
            else -> dataInflationAfterClick(selectedModel)
        }
    }

    // Setup toolbar for the screen
    private fun setToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        actionbar = this.supportActionBar!!
        actionbar.apply {
            setDisplayHomeAsUpEnabled(true)
            setHomeAsUpIndicator(R.drawable.ic_arrow_back_white)
        }
        title = ""
        supportActionBar!!.elevation = 0f
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home)
            finish()
        return super.onOptionsItemSelected(item)
    }

    override fun onClick(model: ModelOptions) {
        this.intentVal = 0
        selectedModel = model
        inflateInitialFragments(this.intentVal)
        // mViewModel.updateList(model)
    }

    // inflate fragments on top view
    private fun replaceFragment(fragment: Fragment) {
        listOptions.scrollToPosition(0)
        bottomSheetBehavior!!.state = BottomSheetBehavior.STATE_COLLAPSED
        val slideTransition = Slide(Gravity.BOTTOM)
        slideTransition.duration = 200
        fragment.enterTransition = slideTransition
        val changeBoundsTransition = TransitionInflater.from(this).inflateTransition(R.transition.change_bounds)
        preFragment.sharedElementEnterTransition = changeBoundsTransition


        val transaction = supportFragmentManager.beginTransaction()
        transaction.setCustomAnimations(R.anim.slide_up_dialog, 0)
        transaction.replace(R.id.container, fragment)
        transaction.commit()
        preFragment = fragment

    }

    private fun replaceFragment2(
        fragment: Fragment,
        bg_reporting1: Int,
        apply: ActionBar
    ) {
        mBinding.parentView.setBackgroundColor(resources.getColor(R.color.colorBlack))
        preFragment = fragment
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, preFragment)
        transaction.commit()
    }


    // inflation login after click event
    private fun dataInflationAfterClick(model: ModelOptions) {
        actionbar.apply {
            setDisplayHomeAsUpEnabled(true)
            setHomeAsUpIndicator(R.drawable.ic_arrow_back_white)
        }
//        tabLayout.setSelectedTabIndicatorColor(resources.getColor(R.color.colorWhite))
//        tabLayout.setTabTextColors(resources.getColor(R.color.tab_unselected), resources.getColor(R.color.colorWhite))

        when {
            model.optionName == "Sales" -> {
                replaceFragment(ReportSalesFragment())
                mBinding.title.text = "Sales"
                mBinding.parentView.setBackgroundResource(R.color.colorBlack)
                mBinding.title.setTextColor(resources.getColor(R.color.colorWhite))
                // mainLay.setBackgroundResource(R.drawable.ic_product_performance_bg)
            }
            model.optionName == "Products" -> {
//                tabLayout.setSelectedTabIndicatorColor(resources.getColor(R.color.colorHeading))
//                tabLayout.setTabTextColors(
//                    resources.getColor(R.color.tab_unselected),
//                    resources.getColor(R.color.colorHeading)
//                )
                replaceFragment(ReportSubscriptionFragment())
//                actionbar.apply {
//                    setDisplayHomeAsUpEnabled(true)
//                    setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)
//                }
                mBinding.title.text = "Products"
                mBinding.title.setTextColor(resources.getColor(R.color.colorWhite))
                mBinding.parentView.setBackgroundColor(resources.getColor(R.color.colorBlack))
            }
            model.optionName == "Views" -> {
                replaceFragment(ReportViewsFragment())
                mBinding.parentView.setBackgroundResource(R.color.colorBlack)
                mBinding.title.text = "Views"
                mBinding.title.setTextColor(resources.getColor(R.color.colorWhite))
            }
            model.optionName == "Customers" -> {
                replaceFragment(ReportCustomersFragment())
                // mBinding.parentView.setBackgroundColor(resources.getColor(R.color.favorites_cost))
                mBinding.parentView.setBackgroundColor(resources.getColor(R.color.favorites_cost))
                mBinding.title.text = "Customers"
                mBinding.title.setTextColor(resources.getColor(R.color.colorWhite))
            }
            model.optionName == "Subscription Shares" -> {
                replaceFragment(ProductPerformanceFragment())
//                mBinding.parentView.setBackgroundResource(R.color.colorWhite)
                mBinding.parentView.setBackgroundResource(R.color.colorBlack)
                mBinding.title.text = "Shares"
                mBinding.title.setTextColor(resources.getColor(R.color.colorWhite))
//                actionbar.apply {
//                    setDisplayHomeAsUpEnabled(true)
//                    setHomeAsUpIndicator(R.drawable.ic_arrow_back_white)
//                }
            }
            model.optionName == "Review & Rating" -> {
                replaceFragment(ReportRatingFragment())
//                mBinding.parentView.setBackgroundColor(resources.getColor(R.color.favorites_cost))
                mBinding.parentView.setBackgroundColor(resources.getColor(R.color.colorBlack))
                mBinding.title.text = "Review & Rating"
                mBinding.title.setTextColor(resources.getColor(R.color.colorWhite))
            }
            model.optionName == "Redemption" -> {
                replaceFragment(ReportRedemptionFragment())
//                mBinding.parentView.setBackgroundColor(resources.getColor(R.color.favorites_cost))
                mBinding.parentView.setBackgroundColor(resources.getColor(R.color.colorBlack))
                mBinding.title.text = "Redemption"
                mBinding.title.setTextColor(resources.getColor(R.color.colorWhite))
            }
            model.optionName == "Team Performance" -> {
//                tabLayout.setSelectedTabIndicatorColor(resources.getColor(R.color.colorHeading))
//                tabLayout.setTabTextColors(
//                    resources.getColor(R.color.tab_unselected),
//                    resources.getColor(R.color.colorHeading)
//                )
                replaceFragment(TeamPerformanceFragment())
//                actionbar.apply {
//                    setDisplayHomeAsUpEnabled(true)
//                    setHomeAsUpIndicator(R.drawable.ic_arrow_back_white)
//                }
                mBinding.title.text = "Team Performance"
                mBinding.title.setTextColor(resources.getColor(R.color.colorWhite))
                mBinding.parentView.setBackgroundColor(resources.getColor(R.color.colorBlack))
//                mBinding.parentView.setBackgroundColor(resources.getColor(R.color.bg_color_subscription_reporting))

            }
        }

    }

    override fun nextFragments(i: Int) {
        when (i) {
            1 -> {
                inflateFragment(SortByFragment())
                mreset.visibility = View.GONE
                filterImgBack.visibility = View.VISIBLE
                filters.text = "Sort By"

                filterImgBack.setOnClickListener {
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"
                }

                filter_done.setOnClickListener {
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"
                }
            }


            2 -> {
                inflateFragment(CategoryFilterFragment(this))
                mreset.visibility = View.GONE
                filterImgBack.visibility = View.VISIBLE
                filters.text = "Category"

                filterImgBack.setOnClickListener {
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"

                }
                filter_done.setOnClickListener {
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"
                }
            }


            3 -> {
                inflateFragment(BrandsFilterFragment())
                mreset.visibility = View.GONE
                filterImgBack.visibility = View.VISIBLE
                filters.text = "Brands"

                filterImgBack.setOnClickListener {
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"

                }

                filter_done.setOnClickListener {
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"
                }
            }
            4 -> {
                inflateFragment(RedemptionFilterFragment())
                mreset.visibility = View.GONE
                filterImgBack.visibility = View.VISIBLE
                filters.text = "Redemption"

                filterImgBack.setOnClickListener {
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"

                }
                filter_done.setOnClickListener {
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"
                }
            }
        }

    }

    override fun onDrawerStateChanged(newState: Int) {

    }

    override fun onDrawerSlide(drawerView: View, slideOffset: Float) {

    }

    override fun onDrawerClosed(drawerView: View) {
        drawerOpen = 0
    }

    override fun onDrawerOpened(drawerView: View) {
        drawerOpen = 1
        mBinding.drawerLayout1.openDrawer(GravityCompat.END)
        inflateFragment(FilterFragment(this))
        filterImgBack.visibility = View.GONE
        mfilters.text = "Filters"
        mreset.visibility = View.VISIBLE
    }

    override fun filterHeaderBackground(s: String) {
        if (s == "new") {
            filterHeader.setBackgroundDrawable(getDrawable(R.drawable.filter_header_select_category))


        } else {
            filterHeader.setBackgroundDrawable(getDrawable(R.drawable.filter_header))

        }
    }
}
