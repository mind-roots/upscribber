package com.upscribber.upscribberSeller.reportingDetail.productPerformance

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.upscribber.upscribberSeller.manageTeam.AddMemberModel

class ProductPerformanceViewModel(application: Application) : AndroidViewModel(application) {
    private lateinit var teams : LiveData<ArrayList<ProductPerfModel>>
    private lateinit var teams1 : LiveData<ArrayList<ProductPerfModel>>
    var productPerf : ProductPerfRepository = ProductPerfRepository(application)

    init {
        teams = productPerf.getTeams()
        teams1 = productPerf.getTeams1()
    }

    fun getTeamsData(): LiveData<ArrayList<ProductPerfModel>> {
        return teams
    }

    fun getTeamsData2(): LiveData<ArrayList<ProductPerfModel>> {
        return teams1
    }
}