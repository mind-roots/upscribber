package com.upscribber.upscribberSeller.reportingDetail

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData

class ReportingViewModel(application: Application) : AndroidViewModel(application) {

    var mRepo = ReportingRepository(application)


    fun getOptions(intentVal: Int): LiveData<ArrayList<ModelOptions>>{
            return  mRepo.getOptionsList(intentVal)
    }


    fun updateList(model: ModelOptions) {
        mRepo.updateList(model)
    }


}