package com.upscribber.upscribberSeller.reportingDetail.teamPerformance

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.databinding.TeamPerformanceItemBinding

class TeamAdapter(var context: Context?, listen: ClickListener) : RecyclerView.Adapter<TeamAdapter.ViewHolder>() {
    lateinit var mBinding : TeamPerformanceItemBinding
    var mData: List<ModelTeamPerformance> = ArrayList()
    var listener: ClickListener = listen
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        mBinding = TeamPerformanceItemBinding.inflate(LayoutInflater.from(context), parent, false)
        return  ViewHolder(mBinding)
    }

    override fun getItemCount(): Int {
        return mData.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model =mData[position]
        holder.bind(model)
        holder.itemView.setOnClickListener {
            listener.onItemClick(model)
        }
    }

    fun updateArray(it: List<ModelTeamPerformance>?) {
        mData = it!!
        notifyDataSetChanged()
    }

    class ViewHolder(var binding: TeamPerformanceItemBinding): RecyclerView.ViewHolder(binding.root){
        fun bind(model : ModelTeamPerformance){
            binding.team = model
            binding.executePendingBindings()
        }
    }

    interface ClickListener{
        fun onItemClick(model: ModelTeamPerformance)
        fun onBackPress()
    }
}