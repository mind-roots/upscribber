package com.upscribber.upscribberSeller.reportingDetail.subscription


import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.text.style.RelativeSizeSpan
import android.view.LayoutInflater
import android.view.View
import androidx.fragment.app.Fragment
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.formatter.LargeValueFormatter
import com.github.mikephil.charting.utils.ColorTemplate
import com.google.android.material.tabs.TabLayout
import com.upscribber.R
import com.upscribber.databinding.FragmentReportSubscriptionBinding
import com.upscribber.upscribberSeller.dashboardfragment.reporting.ViewsAdapter
import com.upscribber.upscribberSeller.reportingDetail.openFilters
import com.upscribber.upscribberSeller.reportingDetail.teamPerformance.TeamViewModel
import kotlinx.android.synthetic.main.toolbar_reporting.view.*

class ReportSubscriptionFragment : Fragment() ,TabLayout.OnTabSelectedListener{
    lateinit var mBinding: FragmentReportSubscriptionBinding
    lateinit var update: openFilters
    private lateinit var mAdapter: ViewsAdapter
    lateinit var mViewModel: TeamViewModel

    val SUBSCRIPTION_COLORS = intArrayOf(
       ColorTemplate.rgb("#7760f2"),
       ColorTemplate.rgb("#fcc117"),
       ColorTemplate.rgb("#02cbcd"),
       ColorTemplate.rgb("#ee9ae5"),
       ColorTemplate.rgb("#ff3f55"),
       ColorTemplate.rgb("#1391ff")
    )

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_report_subscription, container, false)
        return mBinding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mViewModel = ViewModelProviders.of(this).get(TeamViewModel::class.java)
        setChart()
        clickListeners()
        mBinding.tabLayout.tabLayout.addOnTabSelectedListener(this)
        setAdapter()

    }

    private fun setAdapter() {
        val manager = LinearLayoutManager(activity)
        manager.orientation = LinearLayout.HORIZONTAL
        mBinding.recyclerItems.layoutManager = manager
        mAdapter = ViewsAdapter(activity!!)
        mBinding.recyclerItems.adapter = mAdapter
        mViewModel.getProductsData().observe(this, Observer {
            mAdapter.updateArray(it)
            if (it.size > 3) {
                mBinding.imageView59.visibility = View.VISIBLE
            } else {
                mBinding.imageView59.visibility = View.GONE
            }
        })

    }



    private fun clickListeners() {
        mBinding.filters.setOnClickListener {
            update.setFilters()
        }

    }

    @RequiresApi(Build.VERSION_CODES.HONEYCOMB)
    private fun setChart() {
        mBinding.chartReporting.description.isEnabled = false

        setData(0)

        mBinding.chartReporting.animateX(500)
        mBinding.chartReporting.centerText = generateCenterSpannableText(0)
        mBinding.chartReporting.holeRadius = 70f
        mBinding.chartReporting.setHoleColor(Color.TRANSPARENT)
        mBinding.chartReporting.centerTextRadiusPercent =90f
        mBinding.chartReporting.isRotationEnabled = false
        val legend = mBinding.chartReporting.legend
        legend.isEnabled = false
    }

    override fun onTabReselected(p0: TabLayout.Tab?) {


    }

    override fun onTabUnselected(p0: TabLayout.Tab?) {
    }

    override fun onTabSelected(p0: TabLayout.Tab?) {
        if (p0!!.position == 0) {

            setData(0)
            mBinding.chartReporting.animateX(300)
            mBinding.chartReporting.centerText = generateCenterSpannableText(0)
        } else if (p0.position == 1) {
            setData(1)
            mBinding.chartReporting.animateX(300)
            mBinding.chartReporting.centerText = generateCenterSpannableText(1)
        } else if (p0.position == 2) {
            setData(2)
            mBinding.chartReporting.animateX(300)
            mBinding.chartReporting.centerText = generateCenterSpannableText(2)
        } else {
            setData(4)
            mBinding.chartReporting.animateX(300)
            mBinding.chartReporting.centerText = generateCenterSpannableText(3)
        }
    }

    private fun setData(position: Int) {
        val values1 = ArrayList<PieEntry>()
        val values2 = ArrayList<PieEntry>()

        when (position) {
            0 -> for (i in 0 until 7) {
                values1.add(PieEntry(i.toFloat(), (Math.random() * 140).toFloat()))
                values2.add(PieEntry(i.toFloat(), (Math.random() * 140).toFloat()))
            }
            1 -> for (i in 0 until 5) {
                values1.add(PieEntry(i.toFloat(), (Math.random() * 130).toFloat()))
                values2.add(PieEntry(i.toFloat(), (Math.random() * 130).toFloat()))
            }
            2 -> for (i in 0 until 10) {
                values1.add(PieEntry(i.toFloat(), (Math.random() * 150).toFloat()))
                values2.add(PieEntry(i.toFloat(), (Math.random() * 150).toFloat()))
            }
            else -> for (i in 0 until 12) {
                values1.add(PieEntry(i.toFloat(), (Math.random() * 160).toFloat()))
                values2.add(PieEntry(i.toFloat(), (Math.random() * 160).toFloat()))
            }
        }


        val set1 = PieDataSet(values1, "Legend 1")
        set1.color = Color.rgb(119, 96, 242)

        val set2 = PieDataSet(values2, "")
        set2.color = Color.rgb(2,203,205)
        set1.setDrawValues(false)
        set2.setDrawValues(false)

        mBinding.chartReporting.setLayerType(View.LAYER_TYPE_SOFTWARE, null)
        //  mBinding.chartReporting.setScaleEnabled(false)

        set1.formLineWidth = 1f
        //  set1.formLineDashEffect = DashPathEffect(floatArrayOf(10f, 5f), 0f)
        set1.formSize = 15f
        set1.valueTextSize = 5f


        set2.formLineWidth = 1f
        //  set2.formLineDashEffect = DashPathEffect(floatArrayOf(10f, 5f), 0f)
        set2.formSize = 15f
        set2.valueTextSize = 5f



        val colors = ArrayList<Int>()

        for (c in SUBSCRIPTION_COLORS)
            colors.add(c)

        set1.colors = colors

        val data = PieData()
        data.dataSet = set1
        //data.dataSet = set2
        data.setValueFormatter(LargeValueFormatter())
        mBinding.chartReporting.data = data

        //   mBinding.chartReporting.groupBars(startYear.toFloat(), groupSpace, barSpace)
        mBinding.chartReporting.invalidate()
    }


    private fun generateCenterSpannableText(position: Int): SpannableString {
        val s : SpannableString
        when (position) {
            0 -> s = SpannableString("400\nTOTAL")
            1 -> s = SpannableString("800\nTOTAL")
            2 -> s = SpannableString("1000\nTOTAL")
            else -> s = SpannableString("1500\nTOTAL")
        }
        s.setSpan(RelativeSizeSpan(2f), 0, s.length-5, 0)
       // s.setSpan(StyleSpan(Typeface.BOLD),   0, s.length - 5, 0)
        s.setSpan(ForegroundColorSpan(Color.WHITE),   0, s.length - 5, 0)
        s.setSpan(ForegroundColorSpan(Color.WHITE), s.length-5, s.length, 0)

//        s.setSpan(ForegroundColorSpan(Color.GRAY), s.length-5, s.length, 0)
        return s
    }



    override fun onAttach(context: Context) {
        super.onAttach(context)
        update = context as openFilters
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        update = activity as openFilters
    }

}
