package com.upscribber.upscribberSeller.reportingDetail.productPerformance

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.upscribber.R

class ProductPerfRepository(application: Application) {

    val mData = MutableLiveData<ArrayList<ProductPerfModel>>()
    val mData1 = MutableLiveData<ArrayList<ProductPerfModel>>()
    val list: ArrayList<ProductPerfModel>
    val list1: ArrayList<ProductPerfModel>

    init {
        list = getDummyData()
        mData.value = list
        list1 = getDummyData1()
        mData1.value = list1
    }

    private fun getDummyData1(): java.util.ArrayList<ProductPerfModel> {
        val data  = ArrayList<ProductPerfModel>()

        var model = ProductPerfModel()
        model.number = "$2156"
        model.color= R.drawable.view_purple
        model.type = "Oxygen Jet Peel"
        data.add(model)

        model = ProductPerfModel()
        model.number = "$3659"
        model.color= R.drawable.view_curv_yellow
        model.type = "Oxygen Jet Peel"
        data.add(model)

        model = ProductPerfModel()
        model.number = "$9874"
        model.color= R.drawable.bg_seller_button_sea_green
        model.type = "Oxygen Jet Peel"
        data.add(model)

        model = ProductPerfModel()
        model.number = "$0236"
        model.color= R.drawable.view_curv_pink
        model.type = "Oxygen Jet Peel"
        data.add(model)

        model = ProductPerfModel()
        model.number = "$3659"
        model.color= R.drawable.view_curv_yellow
        model.type = "Oxygen Jet Peel"
        data.add(model)


        return data

    }

    private fun getDummyData(): ArrayList<ProductPerfModel> {
        val data  = ArrayList<ProductPerfModel>()

        var model = ProductPerfModel()
        model.number = "$2156"
        model.color= R.drawable.view_purple
        model.type = "Pedicure"
        data.add(model)

        model = ProductPerfModel()
        model.number = "$3659"
        model.color= R.drawable.view_curv_yellow
        model.type = "Manicure"
        data.add(model)

        model = ProductPerfModel()
        model.number = "$9874"
        model.color= R.drawable.bg_seller_button_sea_green
        model.type = "Shave"
        data.add(model)

        model = ProductPerfModel()
        model.number = "$0236"
        model.color= R.drawable.view_curv_pink
        model.type = "Facial"
        data.add(model)

        model = ProductPerfModel()
        model.number = "$3659"
        model.color= R.drawable.view_curv_yellow
        model.type = "Manicure"
        data.add(model)


        return data

    }

    fun getTeams(): LiveData<ArrayList<ProductPerfModel>> {
        return mData

    }

    fun getTeams1(): LiveData<java.util.ArrayList<ProductPerfModel>> {
        return mData1

    }
}