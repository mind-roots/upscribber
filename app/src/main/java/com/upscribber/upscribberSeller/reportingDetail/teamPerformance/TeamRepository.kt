package com.upscribber.upscribberSeller.reportingDetail.teamPerformance

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.upscribber.R
import com.upscribber.upscribberSeller.reportingDetail.subscription.ModelSubscriptionRecycler

class TeamRepository(application: Application) {

    private val mData2= MutableLiveData<List<ModelSubscriptionRecycler>>()
    val mData1 = MutableLiveData<List<ModelSubscriptionRecycler>>()
    var mData = MutableLiveData<List<ModelTeamPerformance>>()
    var list : List<ModelTeamPerformance>
    var list1 : List<ModelSubscriptionRecycler>
    var list2 : List<ModelSubscriptionRecycler>

    init {
        list = getDummyData()
        list1 = getDummyData1()
        list2 = getDummyData2()
        mData.value = list
        mData1.value = list1
        mData2.value = list2
    }

    private fun getDummyData2(): List<ModelSubscriptionRecycler> {
        val data  = ArrayList<ModelSubscriptionRecycler>()

        var model = ModelSubscriptionRecycler()
        model.name = "Pedicure"
        model.color= ""
        model.number = "287"
        model.color1 = R.drawable.view_purple
        data.add(model)

        model = ModelSubscriptionRecycler()
        model.name = "Manicure"
        model.color= ""
        model.number = "447"
        model.color1 = R.drawable.view_curv_yellow
        data.add(model)

        model = ModelSubscriptionRecycler()
        model.name = "Shave"
        model.color= ""
        model.number = "187"
        model.color1 = R.drawable.bg_seller_button_sea_green
        data.add(model)

        model = ModelSubscriptionRecycler()
        model.name = "Facial"
        model.color= ""
        model.number = "287"
        model.color1 = R.drawable.view_curv_pink
        data.add(model)

        return data

    }

    private fun getDummyData1(): List<ModelSubscriptionRecycler> {
        val data  = ArrayList<ModelSubscriptionRecycler>()

        var model = ModelSubscriptionRecycler()
        model.name = "Sub 1"
        model.color= ""
        model.number = ""
        model.color1 = R.drawable.view_purple
        data.add(model)

        model = ModelSubscriptionRecycler()
        model.name = "Sub 2"
        model.color= ""
        model.number = ""
        model.color1 = R.drawable.view_curv_yellow
        data.add(model)

        model = ModelSubscriptionRecycler()
        model.name = "Sub 3"
        model.color= ""
        model.number = ""
        model.color1 = R.drawable.bg_seller_button_sea_green
        data.add(model)

        model = ModelSubscriptionRecycler()
        model.name = "Sub 4"
        model.color= ""
        model.number = ""
        model.color1 = R.drawable.view_curv_pink
        data.add(model)

        return data

    }

    private fun getDummyData(): List<ModelTeamPerformance> {
        val data  = ArrayList<ModelTeamPerformance>()

        var model = ModelTeamPerformance()
        model.name = "Carl M. Performance"
        model.color= ""
        model.color1 = R.drawable.view_purple
        data.add(model)

        model = ModelTeamPerformance()
        model.name = "George T."
        model.color= ""
        model.color1 = R.drawable.view_curv_yellow
        data.add(model)

        model = ModelTeamPerformance()
        model.name = "Lissa K."
        model.color= ""
        model.color1 = R.drawable.bg_seller_button_sea_green
        data.add(model)

        model = ModelTeamPerformance()
        model.name = "Mia N."
        model.color= ""
        model.color1 = R.drawable.view_curv_pink
        data.add(model)

        return data
    }

    fun getTeams(): LiveData<List<ModelTeamPerformance>> {
        return mData
    }

    fun getTeams1(): LiveData<List<ModelSubscriptionRecycler>> {
        return mData1


    }

    fun getItems(): LiveData<List<ModelSubscriptionRecycler>> {
        return mData2

    }


}