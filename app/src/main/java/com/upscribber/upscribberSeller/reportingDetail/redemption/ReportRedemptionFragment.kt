package com.upscribber.upscribberSeller.reportingDetail.redemption


import android.app.Activity
import android.content.Context
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.text.style.UpdateAppearance
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.formatter.LargeValueFormatter
import com.google.android.material.tabs.TabLayout
import com.upscribber.R
import com.upscribber.databinding.FragmentReportRedemptionBinding
import com.upscribber.upscribberSeller.reportingDetail.openFilters
import kotlinx.android.synthetic.main.toolbar_reporting.view.*

class ReportRedemptionFragment : Fragment()  , TabLayout.OnTabSelectedListener{
    lateinit var mBinding: FragmentReportRedemptionBinding
    lateinit var update: openFilters

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_report_redemption, container, false)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setChart()
        clickListeners()
        mBinding.tabLayout.tabLayout.addOnTabSelectedListener(this)

    }

    private fun clickListeners() {
        mBinding.filter.setOnClickListener {
            update.setFilters()
        }

    }

    @RequiresApi(Build.VERSION_CODES.HONEYCOMB)
    private fun setChart() {
        mBinding.chartReporting.description.isEnabled = false
        setData(0)
        mBinding.chartReporting.animateX(500)
        mBinding.chartReporting.isDrawHoleEnabled = false
        mBinding.chartReporting.isRotationEnabled = false
        val legend = mBinding.chartReporting.legend
        legend.isEnabled = false
    }

    override fun onTabReselected(p0: TabLayout.Tab?) {


    }

    override fun onTabUnselected(p0: TabLayout.Tab?) {


    }

    override fun onTabSelected(p0: TabLayout.Tab?) {
        if (p0!!.position == 0) {
            setData(0)
            mBinding.chartReporting.animateX(300)
            mBinding.newCustomerValue.setText("89%")
            mBinding.returnCustomerValue.setText("18%")
            mBinding.cancelCustomerValue.setText("10%")
        } else if (p0.position == 1) {
            setData(1)
            mBinding.chartReporting.animateX(300)
            mBinding.newCustomerValue.setText("55%")
            mBinding.returnCustomerValue.setText("30%")
            mBinding.cancelCustomerValue.setText("15%")
        } else if (p0.position == 2) {
            setData(2)
            mBinding.chartReporting.animateX(300)
            mBinding.newCustomerValue.setText("40%")
            mBinding.returnCustomerValue.setText("30%")
            mBinding.cancelCustomerValue.setText("30%")
        } else {
            setData(4)
            mBinding.chartReporting.animateX(300)
            mBinding.newCustomerValue.setText("20%")
            mBinding.returnCustomerValue.setText("45%")
            mBinding.cancelCustomerValue.setText("35%")
        }
    }

    private fun setData(position: Int) {

        val values1 = ArrayList<PieEntry>()
        if (position == 0){
            for (i in 0 until 4) {
                values1.add(PieEntry(i.toFloat(), (Math.random() * 140).toFloat()))
            }
        }else if (position == 1){
            for (i in 1 until 5) {
                values1.add(PieEntry(i.toFloat(), (Math.random() * 140).toFloat()))
            }
        }else if (position ==2){
            for (i in 6 until 10) {
                values1.add(PieEntry(i.toFloat(), (Math.random() * 140).toFloat()))
            }
        }else {
            for (i in 11 until 15) {
                values1.add(PieEntry(i.toFloat(), (Math.random() * 140).toFloat()))
            }
        }


        val set1 = PieDataSet(values1, "Legend 1")
        set1.color = Color.rgb(119, 96, 242)

        set1.setDrawValues(false)

        mBinding.chartReporting.setLayerType(View.LAYER_TYPE_SOFTWARE, null)

        set1.formLineWidth = 1f
        set1.formSize = 15f
        set1.valueTextSize = 5f


        val colors = ArrayList<Int>()

       /* for (c in ColorTemplate.VORDIPLOM_COLORS)
            colors.add(c)

        for (c in ColorTemplate.JOYFUL_COLORS)
            colors.add(c)

        for (c in ColorTemplate.COLORFUL_COLORS)
            colors.add(c)

        for (c in ColorTemplate.LIBERTY_COLORS)
            colors.add(c)

        for (c in ColorTemplate.PASTEL_COLORS)
            colors.add(c)

        colors.add(ColorTemplate.getHoloBlue())
*/

        colors.add( Color.rgb(119, 96, 242))
        colors.add( Color.rgb(190, 198, 205))
        colors.add( Color.rgb(2, 203, 205))
        set1.colors = colors



        val data = PieData()
        data.dataSet = set1
        data.setValueFormatter(LargeValueFormatter())
        mBinding.chartReporting.data = data
        mBinding.chartReporting.invalidate()
    }



    override fun onAttach(context: Context) {
        super.onAttach(context)
        update = context as openFilters
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        update = activity as openFilters
    }
}
