package com.upscribber.upscribberSeller.reportingDetail.rating


import android.app.Activity
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.tabs.TabLayout
import com.upscribber.R
import com.upscribber.databinding.FragmentReportRatingBinding
import com.upscribber.upscribberSeller.dashboardfragment.viewpagerFragments.reviews.ReviewAdapterSeller
import com.upscribber.upscribberSeller.dashboardfragment.viewpagerFragments.reviews.ReviewViewModel
import com.upscribber.upscribberSeller.reportingDetail.openFilters
import kotlinx.android.synthetic.main.toolbar_reporting.view.*



class ReportRatingFragment : Fragment() , TabLayout.OnTabSelectedListener {


    private lateinit var mViewModel: ReviewViewModel
    lateinit var mBinding : FragmentReportRatingBinding
    lateinit var update: openFilters

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_report_rating, container, false)

        return mBinding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mViewModel = ViewModelProviders.of(this).get(ReviewViewModel::class.java)
        setAdapter()
        clickListeners()
        mBinding.tabLayout.tabLayout.addOnTabSelectedListener(this)
    }

    private fun clickListeners() {
        mBinding.filter.setOnClickListener {
            update.setFilters()
        }


    }

    private fun setAdapter() {
        val layoutManager = LinearLayoutManager(activity)
        layoutManager.orientation = LinearLayout.HORIZONTAL
        mBinding.recyclerReviews.layoutManager = layoutManager

        mViewModel.getReviewsList().observe(this, Observer {
            mBinding.recyclerReviews.adapter = ReviewAdapterSellerReporting(activity!!, it)

        })

    }


    override fun onTabReselected(p0: TabLayout.Tab?) {


    }

    override fun onTabUnselected(p0: TabLayout.Tab?) {

    }

    override fun onTabSelected(p0: TabLayout.Tab?) {
        if (p0!!.position == 0){
            mBinding.circularProgressbar.progress = 60
            mBinding.avgVal.text = "65%"
            mBinding.newCustomerValue.text = "350"
            mBinding.returnCustomerValue.text = "300"
            mBinding.cancelCustomerValue.text = "50"
           
        }else if (p0.position == 1){
            mBinding.circularProgressbar.progress = 70
            mBinding.avgVal.text = "75%"
            mBinding.newCustomerValue.text = "300"
            mBinding.returnCustomerValue.text = "450"
            mBinding.cancelCustomerValue.text = "50"
        }else if (p0.position == 2){
            mBinding.circularProgressbar.progress = 80
            mBinding.avgVal.text = "85%"
            mBinding.newCustomerValue.text = "450"
            mBinding.returnCustomerValue.text = "400"
            mBinding.cancelCustomerValue.text = "50"
        }else{
            mBinding.circularProgressbar.progress = 90
            mBinding.newCustomerValue.text = "550"
            mBinding.avgVal.text = "90%"
            mBinding.returnCustomerValue.text = "350"
            mBinding.cancelCustomerValue.text = "50"
        }

    }



    override fun onAttach(context: Context) {
        super.onAttach(context)
        update = context as openFilters
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        update = activity as openFilters
    }
}
