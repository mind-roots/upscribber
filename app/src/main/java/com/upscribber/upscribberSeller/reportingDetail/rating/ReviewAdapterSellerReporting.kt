package com.upscribber.upscribberSeller.reportingDetail.rating

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.R
import com.upscribber.databinding.ReviewListItem1Binding
import com.upscribber.databinding.ReviewListItemBinding
import com.upscribber.upscribberSeller.dashboardfragment.viewpagerFragments.reviews.ModelReviewSeller

class ReviewAdapterSellerReporting(var context: Context, var mData: List<ModelReviewSeller>?) : RecyclerView.Adapter<ReviewAdapterSellerReporting.ViewHolder>() {
    lateinit var mBinding: ReviewListItem1Binding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.review_list_item1, parent, false)
        return ViewHolder(mBinding.root)
    }

    override fun getItemCount(): Int {
        return  mData!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = mData!![position]
        mBinding.review = model

    }

    class ViewHolder(item: View): RecyclerView.ViewHolder(item)
}
