package com.upscribber.upscribberSeller.reportingDetail

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.upscribber.R

class ReportingRepository(var application: Application) {

    var mData = MutableLiveData<ArrayList<ModelOptions>>()

    fun getOptionsList(intentVal: Int): LiveData<ArrayList<ModelOptions>> {
        val list = data()
        list.removeAt(intentVal - 1)
        mData.value = list
        return mData
    }

    private fun data(): ArrayList<ModelOptions> {
        val list = ArrayList<ModelOptions>()

        var model = ModelOptions()
        model.optionName = "Sales"
        model.optionValue = "$3,000"
        model.optionIcon = R.drawable.ic_sales
        model.optionGraph = R.drawable.ic_graph_sales
        list.add(model)

        model = ModelOptions()
        model.optionName = "Products"
        model.optionValue = "1200"
        model.optionIcon = R.mipmap.app_icon
        model.optionGraph = R.drawable.ic_graph_subscription_sold
        list.add(model)

        model = ModelOptions()
        model.optionName = "Views"
        model.optionValue = "500"
        model.optionIcon = R.drawable.ic_views
        model.optionGraph = R.drawable.ic_graph_views
        list.add(model)

        model = ModelOptions()
        model.optionName = "Customers"
        model.optionValue = "1300"
        model.optionIcon = R.drawable.ic_customers_reporting
        model.optionGraph = R.drawable.ic_graph_customers
        list.add(model)

        model = ModelOptions()
        model.optionName = "Subscription Shares"
        model.optionValue = "1350"
        model.optionIcon = R.drawable.ic_product_performance_
        model.optionGraph = R.drawable.ic_graph_product_performance
        list.add(model)

        model = ModelOptions()
        model.optionName = "Review & Rating"
        model.optionValue = "58%"
        model.optionIcon = R.drawable.ic_like
        model.optionGraph = R.drawable.ic_graph_reviews_ratings
        list.add(model)

        model = ModelOptions()
        model.optionName = "Redemption"
        model.optionValue = "5,600"
        model.optionIcon = R.drawable.ic_redemption
        model.optionGraph = R.drawable.ic_graph_redemption
        list.add(model)

        model = ModelOptions()
        model.optionName = "Team Performance"
        model.optionValue = "1300"
        model.optionIcon = R.drawable.ic_team_performance_back
        model.optionGraph = R.drawable.ic_graph_team_performance
        list.add(model)


        return list
    }


    fun updateList(model: ModelOptions) {
        val list = data()
        for (i in 0 until 8) {
            val data = list[i]
            if (model.optionName == data.optionName) {
                list.remove(data)
                mData.value = list
                return
            }
        }

    }
}