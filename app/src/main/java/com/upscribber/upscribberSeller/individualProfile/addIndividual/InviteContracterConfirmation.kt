package com.upscribber.upscribberSeller.individualProfile.addIndividual

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.upscribber.R
import com.upscribber.databinding.ActivityInviteContracterConfirmationBinding
import com.upscribber.home.ModelHomeDataSubscriptions
import com.upscribber.notification.ModelNotificationCustomer

class InviteContracterConfirmation : AppCompatActivity() {

   lateinit var mBinding : ActivityInviteContracterConfirmationBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
       mBinding= DataBindingUtil.setContentView(this,R.layout.activity_invite_contracter_confirmation)
        setData()
        clickListeners()
    }

    @SuppressLint("SetTextI18n")
    private fun setData() {
        if (intent.hasExtra("modelNotification")){
            val modelNotification = intent.getParcelableExtra<ModelNotificationCustomer>("modelNotification")
            mBinding.textView204.text = "Successfully joined!"
            mBinding.textDummy.text = "You have successfully joined " + modelNotification.business_name  + " as staff member"
        }else if (intent.hasExtra("model")){
            val name = intent.getStringExtra("model")
            mBinding.textView204.text = "Invitation Sent!"
            mBinding.textDummy.text = "Your invite has been sent to $name."
        }else if (intent.hasExtra("modelActivity")){
            val modelActivity = intent.getParcelableExtra<ModelNotificationCustomer>("modelActivity")
            mBinding.textView204.text = "Successfully joined!"
            mBinding.textDummy.text = "You have successfully joined " + modelActivity.business_name  + " as staff member"
        }else if (intent.hasExtra("home")){
            val modelActivity = intent.getParcelableExtra<ModelHomeDataSubscriptions>("home")
            mBinding.textView204.text = "Successfully joined!"
            mBinding.textDummy.text = "You have successfully joined " + modelActivity.business_name  + " as staff member"
        }

    }

    private fun clickListeners() {
        mBinding.tvDone.setOnClickListener {
            if (intent.hasExtra("model") || intent.hasExtra("modelActivity") || intent.hasExtra("home")){
                val intent = Intent()
                setResult(1, intent)
                finish()
            }else {
                finish()
            }
        }

    }
}
