package com.upscribber.upscribberSeller.individualProfile

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import com.upscribber.R

class LinkBusinessActivity : AppCompatActivity() {

    lateinit var toolbar: Toolbar
    lateinit var toolbarTitle: TextView
    lateinit var linearAdd: LinearLayout
    lateinit var selectedModel: LinkBusinessModel
    lateinit var nameRequestt: TextView
    lateinit var addressRequestt: TextView
    lateinit var constraintLayout7: ConstraintLayout
    lateinit var imageCross: ImageView
    lateinit var link: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_link_business)
        initz()
        setToolbar()
        click()
    }

    private fun click() {

        linearAdd.setOnClickListener {
            startActivityForResult(Intent(this,SearchLinkBusinessActivity::class.java),10)
        }

        imageCross.setOnClickListener {
            constraintLayout7.visibility = View.GONE
            linearAdd.visibility = View.VISIBLE
            link.visibility = View.GONE
        }

        link.setOnClickListener {
            startActivity(Intent(this,AlmostThereActivity::class.java))
            finish()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK ) {
            selectedModel= LinkBusinessModel()
            selectedModel = data!!.getParcelableExtra("model")
            constraintLayout7.visibility = View.VISIBLE
            linearAdd.visibility = View.GONE
            link.visibility = View.VISIBLE
            nameRequestt.text = selectedModel.nameRequest
            addressRequestt.text = selectedModel.addressRequest
        }
    }


    private fun initz() {
        toolbar = findViewById(R.id.LinkBusinessToolbar)
        toolbarTitle = findViewById(R.id.title)
        nameRequestt = findViewById(R.id.nameRequestt)
        constraintLayout7 = findViewById(R.id.constraintLayout7)
        addressRequestt = findViewById(R.id.addressRequestt)
        imageCross = findViewById(R.id.imageCross)
        linearAdd = findViewById(R.id.linearAdd)
        link = findViewById(R.id.link)

    }

    private fun setToolbar() {
        setSupportActionBar(toolbar)
        title = ""
        toolbarTitle.text = "Link Business"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }

        return super.onOptionsItemSelected(item)
    }

}
