package com.upscribber.upscribberSeller.individualProfile

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.R

class SearchLinkBusinessActivity : AppCompatActivity(),LinkBusinessAdapter.selectionLink {

    private lateinit var toolbarSearch: Toolbar
    private lateinit var toolbartitle: TextView
    private lateinit var addBusineesLinkRecycler: RecyclerView
    private lateinit var SearchDone: TextView
    var selectedModel: LinkBusinessModel? = null
    private lateinit var list: ArrayList<LinkBusinessModel>
    private lateinit var addBusinessLinkAdapter: LinkBusinessAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_link_business)
        initz()
        click()
        setToolbar()
        setAdapter()
    }



    private fun initz() {

        toolbarSearch = findViewById(R.id.toolbarSearch)
        toolbartitle = findViewById(R.id.title)
        SearchDone = findViewById(R.id.SearchDone)
        addBusineesLinkRecycler = findViewById(R.id.addBusineesLinkRecycler)
    }


    private fun click() {
        SearchDone.setOnClickListener {
            if (selectedModel != null) {
                var intent = Intent(this, LinkBusinessActivity::class.java)
                intent.putExtra("model", selectedModel)
                setResult(Activity.RESULT_OK, intent)
                finish()
//                update1.setSelectedBussines(selectedModel!!)
            } else {
                Toast.makeText(this, "Select a business first", Toast.LENGTH_LONG).show()
            }

        }

    }

    private fun setAdapter() {
        addBusineesLinkRecycler.layoutManager = LinearLayoutManager(this)
        list = getList()
        addBusinessLinkAdapter = LinkBusinessAdapter(this, list, this)
        addBusineesLinkRecycler.adapter = addBusinessLinkAdapter

    }

    override fun selectListLink(position: Int, status: Boolean) {
        selectedModel = null
        if (!status) {
            selectedModel = list[position]
        }

        addBusinessLinkAdapter.updateArray(position)

    }

    private fun getList(): ArrayList<LinkBusinessModel> {
        val arrayList: ArrayList<LinkBusinessModel> = ArrayList()

        var addBusiness1 = LinkBusinessModel()
        addBusiness1.imgRequest = R.drawable.ic_lotus
        addBusiness1.nameRequest = "Mars Spa"
        addBusiness1.addressRequest = "1234,Smith St. Minnepolis,MN 0056"
        arrayList.add(addBusiness1)

        addBusiness1 = LinkBusinessModel()
        addBusiness1.imgRequest = R.drawable.ic_lotus
        addBusiness1.addressRequest = "1234,Smith St. Minnepolis,MN 0056"
        addBusiness1.nameRequest = "Mars Spa2"
        arrayList.add(addBusiness1)

        addBusiness1 = LinkBusinessModel()
        addBusiness1.imgRequest = R.drawable.ic_lotus
        addBusiness1.addressRequest = "1234,Smith St. Minnepolis,MN 0056"
        addBusiness1.nameRequest = "Mars Spa3"
        arrayList.add(addBusiness1)


        return arrayList
    }




    private fun setToolbar() {
        setSupportActionBar(toolbarSearch)
        title = ""
        toolbartitle.text ="Search"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }

        return super.onOptionsItemSelected(item)
    }

}
