package com.upscribber.upscribberSeller.individualProfile.addIndividual

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.databinding.ActivityAddIndividualInviteBinding
import com.upscribber.upscribberSeller.manageTeam.ManageTeamViewModel
import kotlinx.android.synthetic.main.activity_add_individual_invite.*

class AddIndividualInvite : AppCompatActivity() {

    lateinit var mBinding: ActivityAddIndividualInviteBinding
    lateinit var mViewModel: ManageTeamViewModel
    var flag = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_add_individual_invite)
        mViewModel = ViewModelProviders.of(this)[ManageTeamViewModel::class.java]
        setData()
        setToolbar()
        clickListeners()
        setButtonVisible()
        observerInit()

    }

    private fun setButtonVisible() {
        editTextName.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(
                s: CharSequence,
                start: Int,
                count: Int,
                after: Int
            ) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (getTextFieldsData()) {
                    tvInvite.setBackgroundResource(R.drawable.bg_seller_button_blue)
                } else {
                    tvInvite.setBackgroundResource(R.drawable.blue_button_background_opacity)
                }
            }

            override fun afterTextChanged(s: Editable) {
            }
        })


        mBinding.editTextPhone.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {


            }

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                val text = editTextPhone.text.toString()
                val textLength = editTextPhone.text.length

                if (text.endsWith("-") || text.endsWith(" ") || text.endsWith(" "))
                    return
                if (textLength == 4) {
                    editTextPhone.setText(
                        StringBuilder(text).insert(text.length - 1, " ").toString()
                    )
                    (editTextPhone as EditText).setSelection(editTextPhone.text.length)
                }
                if (textLength == 8) {
                    editTextPhone.setText(
                        StringBuilder(text).insert(text.length - 1, " ").toString()
                    )
                    (editTextPhone as EditText).setSelection(editTextPhone.text.length)

                }

                if (getTextFieldsData()) {
                    tvInvite.setBackgroundResource(R.drawable.bg_seller_button_blue)
                } else {
                    tvInvite.setBackgroundResource(R.drawable.blue_button_background_opacity)
                }

            }


        })


        editTextEmail.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(
                s: CharSequence,
                start: Int,
                count: Int,
                after: Int
            ) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (getTextFieldsData()) {
                    tvInvite.setBackgroundResource(R.drawable.bg_seller_button_blue)
                } else {
                    tvInvite.setBackgroundResource(R.drawable.blue_button_background_opacity)
                }
            }

            override fun afterTextChanged(s: Editable) {
            }
        })


    }

    private fun getTextFieldsData(): Boolean {
        val name = editTextName.text.toString().trim().length
        val phoneNumber = editTextPhone.text.toString().trim().length


        if (name == 0) {
            return false
        }
        if (phoneNumber <12) {
            return false
        }



        return true

    }

    private fun observerInit() {
        mViewModel.mDataContracters().observe(this, Observer {
            if (it.status == "true") {
                mBinding.progressBar21.visibility = View.GONE
                startActivityForResult(
                    Intent(
                        this,
                        InviteContracterConfirmation::class.java
                    ).putExtra("model", it.name), 1
                )
                finish()
            } else {
                mBinding.progressBar21.visibility = View.GONE
            }
        })

        mViewModel.getStatus().observe(this, Observer {
            if (it.msg.isNotEmpty()) {
                mBinding.progressBar21.visibility = View.GONE
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
            }
        })

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 65537) {
            finish()
        }
    }

    private fun setData() {
        if (intent.hasExtra("nonContractor")) {
            textHeading.text = "Employee Info"
        } else {
            textHeading.text = "Contractor Info"
        }


    }

    private fun clickListeners() {

        val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
        mBinding.tvInvite.setOnClickListener {
            val model = ModelInvite()
            val phoneNumber = mBinding.editTextPhone.text.toString().trim().replace(" ", "")
            model.name = mBinding.editTextName.text.toString()
            model.phone = phoneNumber
            model.email = mBinding.editTextEmail.text.toString()


            if (mBinding.editTextName.text.toString().isEmpty()) {
                Toast.makeText(this, "Please Enter name", Toast.LENGTH_SHORT).show()
            } else if (phoneNumber.isEmpty()) {
                Toast.makeText(this, "Please Enter Phone Number", Toast.LENGTH_SHORT).show()
            }  else if (intent.hasExtra("nonContractor")) {
                mViewModel.getInviteOnPlatform(
                    auth,
                    "2",
                    "+1" + phoneNumber.trim(),
                    mBinding.editTextName.text.toString().trim(),
                    editTextEmail.text.toString().trim()
                )
                mBinding.progressBar21.visibility = View.VISIBLE
            } else {
                mViewModel.getInviteOnPlatform(
                    auth,
                    "1",
                    "+1" + phoneNumber.trim(),
                    mBinding.editTextName.text.toString().trim(),
                    editTextEmail.text.toString().trim()
                )
                mBinding.progressBar21.visibility = View.VISIBLE
            }
        }

    }

    private fun setToolbar() {
        setSupportActionBar(mBinding.include17.toolbar)
        title = ""
        mBinding.include17.title.text = "Invite"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}
