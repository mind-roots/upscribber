package com.upscribber.upscribberSeller.individualProfile

import android.os.Parcel
import android.os.Parcelable

class LinkBusinessModel() : Parcelable {

    var imgRequest : Int = 0
    lateinit var nameRequest : String
    lateinit var addressRequest : String
    var status : Boolean = false

    constructor(parcel: Parcel) : this() {
        imgRequest = parcel.readInt()
        nameRequest = parcel.readString()
        addressRequest = parcel.readString()
        status = parcel.readByte() != 0.toByte()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(imgRequest)
        parcel.writeString(nameRequest)
        parcel.writeString(addressRequest)
        parcel.writeByte(if (status) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<LinkBusinessModel> {
        override fun createFromParcel(parcel: Parcel): LinkBusinessModel {
            return LinkBusinessModel(parcel)
        }

        override fun newArray(size: Int): Array<LinkBusinessModel?> {
            return arrayOfNulls(size)
        }
    }
}