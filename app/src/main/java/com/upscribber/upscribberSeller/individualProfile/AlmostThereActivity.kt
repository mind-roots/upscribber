package com.upscribber.upscribberSeller.individualProfile

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import com.upscribber.R

class AlmostThereActivity : AppCompatActivity() {

    lateinit var doneLinking : TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_almost_there)
        initz()
        click()
    }


    private fun initz() {

        doneLinking = findViewById(R.id.doneLinking)
    }

    private fun click() {
        doneLinking.setOnClickListener {
            finish()
        }

    }

}
