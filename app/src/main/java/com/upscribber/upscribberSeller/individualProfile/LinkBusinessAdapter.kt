package com.upscribber.upscribberSeller.individualProfile

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.R
import kotlinx.android.synthetic.main.link_business_reycler_layout.view.*

class LinkBusinessAdapter(
    private val mContext: Context,
    private val list: ArrayList<LinkBusinessModel>,
    listen: SearchLinkBusinessActivity
) : RecyclerView.Adapter<LinkBusinessAdapter.MyViewHolder>() {

    var listener = listen as selectionLink

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LinkBusinessAdapter.MyViewHolder {
        val v = LayoutInflater.from(mContext)
            .inflate(R.layout.link_business_reycler_layout, parent, false)

        return LinkBusinessAdapter.MyViewHolder(v)

    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: LinkBusinessAdapter.MyViewHolder, position: Int) {
        val model = list[position]
        holder.itemView.imgRequest.setImageResource(model.imgRequest)
        holder.itemView.addressRequestt.text = model.addressRequest
        holder.itemView.nameRequestt.text = model.nameRequest

        holder.itemView.setOnClickListener {
            if (list[position].status){
                listener.selectListLink(position,list[position].status)
            }else{
                listener.selectListLink(position,list[position].status)
            }
//            listener.selectList(position)
        }


        if (model.status){
            holder.itemView.imageCheck.visibility = View.VISIBLE
        }else{
            holder.itemView.imageCheck.visibility = View.GONE
        }

    }

    fun updateArray(position: Int) {

        val selectedModel = list[position]
        if (selectedModel.status) {
            selectedModel.status = false

        } else {
            for (child in list) {
                child.status = false
            }
            list[position].status = true
        }

        notifyDataSetChanged()
    }



    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)


    interface selectionLink{
        fun selectListLink(position: Int, status: Boolean)
    }
}