package com.upscribber.upscribberSeller.subsriptionSeller.subscriptionMain.customerCheckout

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.location.Geocoder
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.upscribber.R
import com.upscribber.becomeseller.seller.ModelAddress
import com.upscribber.businessAddress.ManualBusinessAddress
import com.upscribber.checkout.ModelCheckoutOne
import com.upscribber.commonClasses.Constant
import com.upscribber.payment.paymentCards.AddCardActivity
import com.upscribber.profile.ModelGetProfile
import com.upscribber.upscribberSeller.subsriptionSeller.subscriptionMain.ModelSellerSubscriptions
import kotlinx.android.synthetic.main.activity_checkout_one.*
import kotlinx.android.synthetic.main.toolbar.view.*

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS",
    "RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS"
)
class CheckoutMerchantSignUp : AppCompatActivity() {

    var billingType = "0"
    private var phoneNumberLength = 0
    var latitude = 0.0
    var longitude = 0.0
    val model = ModelCheckoutOne()
    var modelProfile = ModelGetProfile()
    lateinit var mViewModel: MerchantCheckoutViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_checkout_merchant_one)
        mViewModel = ViewModelProviders.of(this)[MerchantCheckoutViewModel::class.java]
        Places.initialize(applicationContext, resources.getString(R.string.google_maps_key))
        setData()
        setColors()
        setToolbar()
        clickListeners()
        observerInit()
    }

    private fun setData() {
       if (intent.hasExtra("modelPro")){
           val modelPro = intent.getParcelableExtra<ModelGetProfile>("modelPro")
           if (modelPro.user_id.isNotEmpty()){
               etFirstName.setText(modelPro.name)
               eTEmailAddress.setText(modelPro.email)
               if (modelPro.primary_address.street.isNotEmpty()) {
                   eTPrimaryAddress.text =
                       modelPro.primary_address.street + " " + modelPro.primary_address.state + " " + modelPro.primary_address.city + " " + modelPro.primary_address.zip_code
               }

               if (modelPro.billing_address.billing_street.isNotEmpty()) {
                   etBillingAddress.text =
                       modelPro.billing_address.billing_street + " " + modelPro.billing_address.billing_state + " " + modelPro.billing_address.billing_city + " " + modelPro.billing_address.billing_zipcode
               }
               etphoneNumber.setText(Constant.getSplitPhoneNumberWithoutCode(modelPro.contact_no,this))
           }
       }


    }

    private fun observerInit() {
        mViewModel.getSignedUpData().observe(this, androidx.lifecycle.Observer {
            if (it.status == "true") {
                modelProfile = it
                val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
                mViewModel.updateAddress(
                    auth,
                    modelProfile.user_id,
                    modelProfile.primary_address.street,
                    modelProfile.primary_address.city,
                    modelProfile.primary_address.state,
                    modelProfile.primary_address.zip_code,
                    modelProfile.primary_address.suite,
                    modelProfile.billing_address.billing_street,
                    modelProfile.billing_address.billing_city,
                    modelProfile.billing_address.billing_state,
                    modelProfile.billing_address.billing_zipcode,
                    modelProfile.billing_address.billing_suite,
                    "",
                    "1",
                    modelProfile.value
                )


            } else {
                Toast.makeText(this, it.msg, Toast.LENGTH_LONG).show()
            }
        })


        mViewModel.getmDataUpdateProfile().observe(this, androidx.lifecycle.Observer {
            if (it.status == "true") {
                modelProfile = it
                val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
                mViewModel.updateAddress(
                    auth,
                    modelProfile.user_id,
                    modelProfile.primary_address.street,
                    modelProfile.primary_address.city,
                    modelProfile.primary_address.state,
                    modelProfile.primary_address.zip_code,
                    modelProfile.primary_address.suite,
                    modelProfile.billing_address.billing_street,
                    modelProfile.billing_address.billing_city,
                    modelProfile.billing_address.billing_state,
                    modelProfile.billing_address.billing_zipcode,
                    modelProfile.billing_address.billing_suite,
                    "",
                    "1",modelProfile.value
                )

            } else {
                Toast.makeText(this, it.msg, Toast.LENGTH_LONG).show()
            }
        })



        mViewModel.getmDataUpdateAddress().observe(this, Observer {
            progressBar.visibility = View.GONE
            when (it.value) {
                "editAddressPrimary" -> {
                    val intent = Intent()
                    intent.putExtra("model",it)
                    setResult(10, intent)
                    finish()
                }
                "editBillingAddress" -> {
                    val intent = Intent()
                    intent.putExtra("model",it)
                    setResult(11, intent)
                    finish()
                }
                else -> {
                    startActivity(
                        Intent(this, AddCardActivity::class.java).putExtra(
                            "merchantCheckOut", "merchantCheckout")
                            .putExtra("modelSubscriptionModel", intent.getParcelableExtra<ModelSellerSubscriptions>("modelSubscriptionModel"))
                            .putExtra("totalPriceText", intent.getStringExtra("totalPriceText"))
                            .putExtra("modelProfile", modelProfile)
                            .putExtra("modelCheckoutMerchant", model)
                    )
                }
            }

            //            progressBar.visibility = View.GONE

        })

        mViewModel.getStatus().observe(this, androidx.lifecycle.Observer {
            progressBar.visibility = View.GONE
            if (it.msg == "Invalid auth code") {
                Constant.commonAlert(this)
            } else {
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
            }
        })

    }

    private fun setToolbar() {
        setSupportActionBar(toolbar.toolbar)
        toolbar.title.text = ""
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)
        Places.initialize(applicationContext, resources.getString(R.string.google_maps_key))

    }

    private fun clickListeners() {
        goToSubscription.setOnClickListener {
            apiImplimentation()
        }

        switchMerchant.setOnCheckedChangeListener { p0, p1 ->
            if (p1) {
                textView.visibility = View.GONE
                updateBilling.visibility = View.GONE
                etBillingAddress.visibility = View.GONE

            } else {
                textView.visibility = View.VISIBLE
                updateBilling.visibility = View.GONE
                etBillingAddress.visibility = View.VISIBLE
            }

        }

        eTPrimaryAddress.setOnClickListener {
            billingType = "0"
            Constant.hideKeyboard(this, etBillingAddress)
            Places.createClient(this)
            val fields =
                listOf(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG)
            val intent = Autocomplete.IntentBuilder(
                AutocompleteActivityMode.FULLSCREEN, fields
            )
                .build(this)
            startActivityForResult(intent, 10)
        }

        etBillingAddress.setOnClickListener {
            billingType = "1"
            Constant.hideKeyboard(this, etBillingAddress)
            Places.createClient(this)
            val fields =
                listOf(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG)
            val intent = Autocomplete.IntentBuilder(
                AutocompleteActivityMode.FULLSCREEN, fields
            )
                .build(this)
            startActivityForResult(intent, 11)
        }


        etphoneNumber.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                val text = etphoneNumber.text.toString()
                val textLength = etphoneNumber.text.length

                if (text.endsWith("-") || text.endsWith(" ") || text.endsWith(" "))
                    return
                if (textLength == 4) {
                    etphoneNumber.setText(
                        StringBuilder(text).insert(
                            text.length - 1, " "
                        ).toString()
                    )
                    (etphoneNumber as EditText).setSelection(etphoneNumber.text.length)
                }
                if (textLength == 8) {
                    etphoneNumber.setText(
                        StringBuilder(text).insert(
                            text.length - 1,
                            " "
                        ).toString()
                    )
                    (etphoneNumber as EditText).setSelection(etphoneNumber.text.length)

                }
                phoneNumberLength = if (textLength == 12) {
                    1
                } else {
                    0
                }

            }

            override fun afterTextChanged(s: Editable) {
            }
        })

    }


    @SuppressLint("SetTextI18n")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 10) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    try {
                        val place = Autocomplete.getPlaceFromIntent(data!!)
                        val latLng = place.latLng
                        latitude = latLng!!.latitude
                        longitude = latLng.longitude
                        val geocoder = Geocoder(this)
                        val addresses =
                            latLng.latitude.let {
                                geocoder.getFromLocation(
                                    it,
                                    latLng.longitude,
                                    1
                                )
                            }
                        val locationModel = ModelAddress()
                        locationModel.streetAddress = addresses[0].thoroughfare
                        locationModel.city = addresses[0].locality
                        locationModel.state = addresses[0].adminArea
                        locationModel.latitude = latitude.toString()
                        locationModel.longitude = longitude.toString()
                        locationModel.zipCode = addresses[0].postalCode
                        startActivityForResult(
                            Intent(
                                this,
                                ManualBusinessAddress::class.java
                            ).putExtra("checkoutMerchant", locationModel), 1
                        )

                    } catch (e: Exception) {
                        Toast.makeText(this, "Could not find the location!", Toast.LENGTH_SHORT)
                            .show()
                        e.printStackTrace()
                    }
                }
            }
        }

        if (requestCode == 11) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    try {
                        val place = Autocomplete.getPlaceFromIntent(data!!)
                        val latLng = place.latLng
                        latitude = latLng!!.latitude
                        longitude = latLng.longitude
                        val geocoder = Geocoder(this)
                        val addresses =
                            latLng.latitude.let {
                                geocoder.getFromLocation(
                                    it,
                                    latLng.longitude,
                                    1
                                )
                            }
                        val locationModel = ModelAddress()
                        locationModel.billingZip = addresses[0].postalCode
                        locationModel.billingCity = addresses[0].locality
                        locationModel.billingState = addresses[0].adminArea
                        locationModel.billingStreet = addresses[0].thoroughfare

                        startActivityForResult(
                            Intent(
                                this,
                                ManualBusinessAddress::class.java
                            ).putExtra("checkoutMerchantBilling", locationModel), 1
                        )

                    } catch (e: Exception) {
                        Toast.makeText(this, "Could not find the location!", Toast.LENGTH_SHORT)
                            .show()
                        e.printStackTrace()
                    }
                }
            }
        }


        if (resultCode == 1 && data != null) {
            val modelGetProfile = data.getParcelableExtra<ModelAddress>("address")
            if (billingType == "0") {
                if (modelGetProfile.suiteNumber == "") {
                    eTPrimaryAddress.text =
                        modelGetProfile.streetAddress + "," + modelGetProfile.city + "," +
                                modelGetProfile.state + "," + modelGetProfile.zipCode
                } else {
                    eTPrimaryAddress.text = modelGetProfile.suiteNumber + "," +
                            modelGetProfile.streetAddress + "," + modelGetProfile.city + "," +
                            modelGetProfile.state + "," + modelGetProfile.zipCode
                }

                model.streetName = modelGetProfile.streetAddress
                model.stateName = modelGetProfile.state
                model.city = modelGetProfile.city
                model.zipCode = modelGetProfile.zipCode
                model.suite = modelGetProfile.suiteNumber

            } else {
                if (modelGetProfile.suiteNumber == "") {
                    etBillingAddress.text =
                        modelGetProfile.billingStreet + "," + modelGetProfile.billingCity + "," +
                                modelGetProfile.billingState + "," + modelGetProfile.billingZip
                } else {
                    etBillingAddress.text = modelGetProfile.suiteNumber + "," +
                            modelGetProfile.billingStreet + "," + modelGetProfile.billingCity + "," +
                            modelGetProfile.billingState + "," + modelGetProfile.billingZip
                }
//                model.streetName = modelGetProfile.streetAddress
//                model.stateName = modelGetProfile.state
//                model.city = modelGetProfile.city
//                model.zipCode = modelGetProfile.zipCode
                model.billingStreet = modelGetProfile.billingStreet
                model.billingState = modelGetProfile.billingState
                model.billingCity = modelGetProfile.billingCity
                model.billingZip = modelGetProfile.billingZip
//                model.suite = modelGetProfile.suiteNumber
                model.billingSuite = modelGetProfile.billingSuite

            }
        }

    }

    private fun apiImplimentation() {
        val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
        val user_id = Constant.getPrefs(this).getString(Constant.user_id, "")
        var value = ""
        if (switchMerchant.isChecked) {
            billingType = "0"
            if (etFirstName.text.toString().isEmpty() || eTEmailAddress.text.toString().isEmpty() || etphoneNumber.text.toString().isEmpty() || eTPrimaryAddress.text.toString().isEmpty()) {
                Toast.makeText(this, "All Fields are mandatory", Toast.LENGTH_LONG).show()
            } else if (etFirstName.text.toString().isEmpty()) {
                Toast.makeText(this, "Please Enter your First Name", Toast.LENGTH_LONG).show()
            } else if (etphoneNumber.text.toString().isEmpty()) {
                Toast.makeText(this, "Please Enter your Street Name", Toast.LENGTH_LONG).show()
            } else if (eTEmailAddress.text.toString().isEmpty()) {
                Toast.makeText(this, "Please Enter your State", Toast.LENGTH_LONG).show()
            } else if (phoneNumberLength == 0 && etphoneNumber.text.toString().isEmpty()) {
                Toast.makeText(this, "Please Enter 10 digit valid phone number!", Toast.LENGTH_LONG)
                    .show()
            } else if (eTPrimaryAddress.text.toString().isEmpty()) {
                Toast.makeText(this, "Please Enter your City", Toast.LENGTH_LONG).show()
            } else {
                model.firstName = etFirstName.text.toString()
                model.emailAddress = eTEmailAddress.text.toString()
                model.phoneNumber = etphoneNumber.text.toString()
                model.billingType = billingType
                Constant.checkoutMerchantData = model
                val phoneNumber = etphoneNumber.text.toString().replace(" ", "")
                progressBar.visibility = View.VISIBLE

                if (user_id == "" || user_id == "0") {
                    mViewModel.getMerchantSignUp(
                        "+1" + phoneNumber,
                        eTEmailAddress.text.toString().trim(),
                        "1",
                        auth,
                        etFirstName.text.toString().trim(),
                        "",
                        "",
                        "",
                        "",
                        "",
                        model.streetName,
                        model.stateName,
                        model.city,
                        model.zipCode,
                        model.suite,
                        "4"
                    )
                } else {
                    if (intent.hasExtra("editAddressPrimary")) {
                        value = intent.getStringExtra("editAddressPrimary")
                        mViewModel.getUpdateProfile(
                            user_id,
                            etFirstName.text.toString().trim(),
                            "+1$phoneNumber",
                            eTEmailAddress.text.toString().trim(),
                            "0",
                            model.billingStreet,
                            model.billingState,
                            model.billingCity,
                            model.billingZip,
                            model.billingSuite,
                            value,model.streetName,model.city,model.stateName,model.zipCode,model.suite
                        )
                    } else if (intent.hasExtra("editBillingAddress")) {
                        value = intent.getStringExtra("editBillingAddress")
                        mViewModel.getUpdateProfile(
                            user_id,
                            etFirstName.text.toString().trim(),
                            "+1" + phoneNumber,
                            eTEmailAddress.text.toString().trim(),
                            "0",
                            model.billingStreet,
                            model.billingState,
                            model.billingCity,
                            model.billingZip,
                            model.billingSuite,
                            value,
                            model.streetName,
                            model.city,
                            model.stateName,
                            model.zipCode,
                            model.suite
                        )
                    } else {
                        mViewModel.getUpdateProfile(
                            user_id,
                            etFirstName.text.toString().trim(),
                            "+1" + phoneNumber,
                            eTEmailAddress.text.toString().trim(),
                            "0",
                            model.billingStreet,
                            model.billingState,
                            model.billingCity,
                            model.billingZip,
                            model.billingSuite,
                            value,
                            model.streetName,
                            model.city,
                            model.stateName,
                            model.zipCode,
                            model.suite
                        )
                    }

                }
            }

        } else {
            billingType = "1"
            if (etFirstName.text.toString().isEmpty() || eTEmailAddress.text.toString().isEmpty() || etphoneNumber.text.toString().isEmpty()
                || eTPrimaryAddress.text.toString().isEmpty() || etBillingAddress.text.toString().isEmpty()
            ) {
                Toast.makeText(this, "All Fields are mandatory", Toast.LENGTH_LONG).show()
            } else if (etFirstName.text.toString().isEmpty()) {
                Toast.makeText(this, "Please Enter your First Name", Toast.LENGTH_LONG)
                    .show()
            } else if (eTEmailAddress.text.toString().isEmpty()) {
                Toast.makeText(this, "Please Enter your Street Name", Toast.LENGTH_LONG)
                    .show()
            } else if (phoneNumberLength == 0 && etphoneNumber.text.toString().isEmpty()) {
                Toast.makeText(this, "Please Enter 10 digit valid phone number!", Toast.LENGTH_LONG)
                    .show()
            }else if (etphoneNumber.text.toString().isEmpty()) {
                Toast.makeText(this, "Please Enter your State", Toast.LENGTH_LONG).show()
            } else if (eTPrimaryAddress.text.toString().isEmpty()) {
                Toast.makeText(this, "Please Enter your City", Toast.LENGTH_LONG).show()
            } else if (etBillingAddress.text.toString().isEmpty()) {
                Toast.makeText(this, "Please Enter your ZipCode", Toast.LENGTH_LONG).show()
            } else {
                model.firstName = etFirstName.text.toString()
                model.phoneNumber = etphoneNumber.text.toString()
                model.emailAddress = eTEmailAddress.text.toString()
                model.billingType = billingType
                Constant.checkoutMerchantData = model
                val phoneNumber = etphoneNumber.text.toString().replace(" ", "")
                progressBar.visibility = View.VISIBLE
                if (user_id == "" || user_id == "0") {
                    mViewModel.getMerchantSignUp(
                        "+1" + phoneNumber,
                        eTEmailAddress.text.toString().trim(),
                        "1",
                        auth,
                        etFirstName.text.toString().trim(),
                        model.billingStreet,
                        model.billingState,
                        model.billingCity,
                        model.billingZip,
                        model.billingSuite,
                        model.streetName,
                        model.stateName,
                        model.city,
                        model.zipCode,
                        model.suite,
                        "4"
                    )

                } else {
                    if (intent.hasExtra("editAddressPrimary")) {
                        value = intent.getStringExtra("editAddressPrimary")
                        mViewModel.getUpdateProfile(
                            user_id,
                            etFirstName.text.toString().trim(),
                            "+1" + phoneNumber,
                            eTEmailAddress.text.toString().trim(),
                            "0",
                            model.billingStreet,
                            model.billingState,
                            model.billingCity,
                            model.billingZip,
                            model.billingSuite,
                            value,
                            model.streetName,
                            model.city,
                            model.stateName,
                            model.zipCode,
                            model.suite
                        )

                    }else if (intent.hasExtra("editBillingAddress")) {
                        value = intent.getStringExtra("editBillingAddress")
                        mViewModel.getUpdateProfile(
                            user_id,
                            etFirstName.text.toString().trim(),
                            "+1" + phoneNumber,
                            eTEmailAddress.text.toString().trim(),
                            "0",
                            model.billingStreet,
                            model.billingState,
                            model.billingCity,
                            model.billingZip,
                            model.billingSuite,
                            value,
                            model.streetName,
                            model.city,
                            model.stateName,
                            model.zipCode,
                            model.suite
                        )

                    } else {
                        mViewModel.getUpdateProfile(
                            user_id,
                            etFirstName.text.toString().trim(),
                            "+1" + phoneNumber,
                            eTEmailAddress.text.toString().trim(),
                            "0",
                            model.billingStreet,
                            model.billingState,
                            model.billingCity,
                            model.billingZip,
                            model.billingSuite,
                            value,
                            model.streetName,
                            model.city,
                            model.stateName,
                            model.zipCode,
                            model.suite
                        )
                    }
                }
            }
        }


    }

    private fun setColors() {
        goToSubscription.setBackgroundResource(R.drawable.bg_seller_button_blue)
        updatePrimaryAddress.setTextColor(resources.getColor(R.color.blue))
        updateBilling.setTextColor(resources.getColor(R.color.blue))
        switch3.visibility = View.GONE
        switchMerchant.visibility = View.VISIBLE

    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}
