package com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.subscription

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.R
import com.upscribber.databinding.CreateSubscriptionCardRecyclerBinding
import java.math.BigDecimal
import java.math.RoundingMode

class AdapterSubscriptionCard(var context: Context) :
    RecyclerView.Adapter<AdapterSubscriptionCard.MyViewHolder>() {

    private lateinit var binding: CreateSubscriptionCardRecyclerBinding
    private var items = ArrayList<ModelSubscriptionCard>()
    var deleteData = context as deleteCard


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        binding = CreateSubscriptionCardRecyclerBinding.inflate(
            LayoutInflater.from(context),
            parent,
            false
        )
        return MyViewHolder(binding)

    }

    override fun getItemCount(): Int {
        return items.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = items[position]
        holder.bind(model)
        holder.binding.imageView38.setOnClickListener {
            val mPrefs = context.getSharedPreferences("data", AppCompatActivity.MODE_PRIVATE)
            var boughtIsMore = mPrefs.getString("bought", "0")
            if (boughtIsMore.isEmpty()) {
                boughtIsMore = "0"
            }
            if (boughtIsMore.toInt() == 0) {
                deleteData.delete(position, model)
            }

        }

        holder.binding.textView266.text = "Subscription Plan ${position + 1}"


        if (model.price == "") {
            holder.binding.textView272.text = "N/A"
        } else {

            holder.binding.textView272.text = "$" + model.price

            if (model.discountPrice.isNotEmpty()) {
                val discountPrice = ((model.discountPrice.toDouble() * model.price.toDouble()) / 100)
                val price = model.price.toDouble() - discountPrice
                if (price.toString().contains(".")){
                    val splitPos = price.toString().split(".")
                    if (splitPos[1] == "00" || splitPos[1] == "0") {
                        holder.binding.textViewIntroPrice.text = "$" + splitPos[0]
                    } else {
                        holder.binding.textViewIntroPrice.text = "$" + BigDecimal(price).setScale(2, RoundingMode.HALF_UP).toString()
                    }
                }else{
                    holder.binding.textViewIntroPrice.text = "$" + BigDecimal(price).setScale(2, RoundingMode.HALF_UP).toString()
                }


            } else {
                holder.binding.textViewIntroPrice.text = "NA"
            }

        }


        if (model.redeemtion_cycle_qty.isNotEmpty()) {
            if (model.redeemtion_cycle_qty.contains("Unlimited")){
                holder.binding.textView270.text = "Unlimited " + model.quantityName
            }else {
                if (model.redeemtion_cycle_qty.toInt() > 100) {
                    holder.binding.textView270.text = "Unlimited " + model.quantityName
                } else {
                    holder.binding.textView270.text =
                        model.redeemtion_cycle_qty + " " + model.quantityName
                }
            }
        }



        if (model.frequencyType == "2" || model.frequencyType == "Year" || model.frequencyType == "Yearly" || model.frequencyType == "Annual") {

            holder.binding.textView271.text = "Yearly"
        }
        if (model.frequencyType == "1" || model.frequencyType == "Month" || model.frequencyType == "Monthly" || model.frequencyType == "months" || model.frequencyType == "month") {

            holder.binding.textView271.text = "Monthly"
        }

        if (model.pricingSubscriber.toInt() > 0) {
            holder.binding.imageView38.visibility = View.GONE
            holder.binding.textIntroDays.text = model.pricingSubscriber
        } else {
            holder.binding.imageView38.visibility = View.VISIBLE
            holder.binding.textIntroDays.text = "0"
        }

        if (model.freeTrail == "No free trial" || model.freeTrail == "") {
            if (model.introPrice != "0" && model.introDays != "0") {

                holder.binding.textView359.text = "Intro Price"
                holder.binding.textView359.visibility = View.VISIBLE
//                holder.binding.textfreeTrail.visibility = View.GONE
//                holder.binding.textFreeTrail.visibility = View.GONE
//                holder.binding.textIntroDays.text = model.introDays
//                holder.binding.textViewIntroPrice.text = "$" + model.introPrice
            } else {
                holder.binding.textView359.visibility = View.GONE
//                holder.binding.textfreeTrail.visibility = View.VISIBLE
//                holder.binding.textFreeTrail.visibility = View.VISIBLE
//                holder.binding.textfreeTrail.text = "Off"
//                holder.binding.textIntroDays.text = "N/A"
//                holder.binding.textViewIntroPrice.text = "Off"
            }

        } else {
            if ((model.introPrice == "0" && model.introDays == "0") || (model.introPrice == "" && model.introDays == "") || (model.introPrice == "0.00" && model.introDays == "0")) {
                holder.binding.textView359.text = "Free Trial"
                holder.binding.textView359.visibility = View.VISIBLE
//                holder.binding.textfreeTrail.text = "On"
//                holder.binding.textfreeTrail.visibility = View.VISIBLE
//                holder.binding.textFreeTrail.visibility = View.VISIBLE
//                holder.binding.textIntroDays.visibility = View.GONE
//                holder.binding.textIntroduction.visibility = View.GONE
//                holder.binding.textViewIntroPrice.visibility = View.GONE
//                holder.binding.textIntroductionDays.visibility = View.GONE
            } else {
                holder.binding.textView359.visibility = View.GONE
//                holder.binding.textfreeTrail.visibility = View.VISIBLE
//                holder.binding.textFreeTrail.visibility = View.VISIBLE
//                holder.binding.textfreeTrail.text = "Off"
//                holder.binding.textIntroDays.text = model.introDays
//                holder.binding.textViewIntroPrice.text = "$" + model.introPrice
            }

        }
//        if (model.freeTrail == "No free trial") {
//            holder.binding.textfreeTrail.text = "Off"
//        } else {
//            holder.binding.textfreeTrail.text = "Off"
//
//        }
        holder.itemView.setOnClickListener {

            try {
                if (model.pricingSubscriber.toInt() > 0) {
                    val mDialogView = LayoutInflater.from(context)
                        .inflate(R.layout.days_left_alert, null)
                    val mBuilder = AlertDialog.Builder(context)
                        .setView(mDialogView)
                    val mAlertDialog = mBuilder.show()
                    val textView249: TextView =
                        mDialogView.findViewById(R.id.textView249)
                    val okBtn: TextView = mDialogView.findViewById(R.id.okBtn)
                    okBtn.setTextColor(context.resources.getColor(R.color.blue))

                    textView249.text =
                        "Cannot edit plan details because this plan has active subscribers. Please create a new plan or subscription."
                    mAlertDialog.setCancelable(false)
                    mAlertDialog.window.setBackgroundDrawableResource(R.drawable.bg_alert)


                    okBtn.setOnClickListener {
                        mAlertDialog.dismiss()
                    }

                } else {
                    deleteData.edit(position, model)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

    }

    class MyViewHolder(var binding: CreateSubscriptionCardRecyclerBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(obj: ModelSubscriptionCard) {
            binding.listing = obj
            binding.executePendingBindings()
        }
    }

    fun update(items: ArrayList<ModelSubscriptionCard>) {
        this.items = items
        notifyDataSetChanged()
    }


    interface deleteCard {
        fun delete(
            position: Int,
            model: ModelSubscriptionCard
        )

        fun edit(
            position: Int,
            model: ModelSubscriptionCard
        )
    }

}