package com.upscribber.upscribberSeller.subsriptionSeller.subscriptionMain.customerCheckout

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.upscribber.becomeseller.seller.ModelAddress
import com.upscribber.commonClasses.Constant
import com.upscribber.commonClasses.ModelStatusMsg
import com.upscribber.commonClasses.WebServicesCustomers
import com.upscribber.profile.ModelGetProfile
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class MerchantCheckOutRepository(var application: Application) {

    val mDataSigningUp = MutableLiveData<ModelGetProfile>()
    val mDataFailure = MutableLiveData<ModelStatusMsg>()
    val mDataUpdateProfile = MutableLiveData<ModelGetProfile>()
    val mDataUpdateAddress = MutableLiveData<ModelAddress>()


    fun getmDataUpdateAddress(): LiveData<ModelAddress> {
        return mDataUpdateAddress
    }

    fun getmDataUpdateProfile(): LiveData<ModelGetProfile> {
        return mDataUpdateProfile
    }

    fun getMerchantSignUp(
        contactNo: String,
        email: String,
        signupType: String,
        authCode: String,
        name: String,
        billingStreet: String,
        billingState: String,
        billingCity: String,
        billingZipcode: String,
        billingSuite: String,
        homeStreet: String,
        homeState: String,
        homeCity: String,
        homeZipcode: String,
        homeSuite: String,
        regType: String
    ) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> =
            service.SignUpMerchant(
                contactNo,
                email,
                signupType,
                authCode,
                name,
                billingStreet,
                billingState,
                billingCity,
                billingZipcode,
                billingSuite,
                homeStreet,
                homeState,
                homeCity,
                homeZipcode,
                homeSuite,
                regType
            )
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelSignUp = ModelGetProfile()
                        val modelStatus = ModelStatusMsg()
                        if (status == "true") {
                            val data = json.getJSONObject("data")
                            val user_details = data.getJSONObject("user_details")
                            val primary_address = user_details.optJSONObject("primary_address")
                            val billing_address = user_details.optJSONObject("billing_address")
                            modelSignUp.profile_image = user_details.optString("profile_image")
                            modelSignUp.zip_code = user_details.optString("zip_code")
                            modelSignUp.name = user_details.optString("name")
                            modelSignUp.contact_no = user_details.optString("contact_no")
                            modelSignUp.email = user_details.optString("email")
                            modelSignUp.gender = user_details.optString("gender")
                            modelSignUp.stripe_customer_id =
                                user_details.optString("stripe_customer_id")
                            modelSignUp.birthdate = user_details.optString("birthdate")
                            modelSignUp.last_name = user_details.optString("last_name")
                            modelSignUp.is_merchant = user_details.optString("is_merchant")
                            modelSignUp.steps = user_details.optString("steps")
                            modelSignUp.business_id = user_details.optString("business_id")
                            modelSignUp.tax = user_details.optInt("tax")
                            modelSignUp.merchant_commission =
                                user_details.optInt("merchant_commission")
                            modelSignUp.wallet = user_details.optString("wallet")
                            modelSignUp.type = user_details.optString("type")
                            modelSignUp.social_id = user_details.optString("social_id")
                            modelSignUp.feedback = user_details.optString("feedback")
                            modelSignUp.invite_code = user_details.optString("invite_code")
                            modelSignUp.reg_type = user_details.optString("reg_type")
                            modelSignUp.email_verify = user_details.optString("email_verify")
                            modelSignUp.user_id = user_details.optString("user_id")
                            modelSignUp.primary_address.street = primary_address.optString("street")
                            modelSignUp.primary_address.city = primary_address.optString("city")
                            modelSignUp.primary_address.state = primary_address.optString("state")
                            modelSignUp.primary_address.zip_code =
                                primary_address.optString("zip_code")
                            modelSignUp.billing_address.billing_street =
                                billing_address.optString("billing_street")
                            modelSignUp.billing_address.billing_city =
                                billing_address.optString("billing_city")
                            modelSignUp.billing_address.billing_state =
                                billing_address.optString("billing_state")
                            modelSignUp.billing_address.billing_zipcode =
                                billing_address.optString("billing_zipcode")

                            val editor = Constant.getPrefs(application).edit()
                            editor.putString(Constant.user_id, modelSignUp.user_id)
                            editor.putString(
                                Constant.stripe_new_customer_from_merchant,
                                modelSignUp.stripe_customer_id
                            )
                            editor.apply()

                            modelSignUp.msg = msg
                            modelSignUp.status = status
                            modelStatus.status = status
                            modelStatus.msg = msg

                            mDataSigningUp.value = modelSignUp
                        } else {
                            val data = json.getJSONArray("data")
                            modelSignUp.msg = msg
                            modelStatus.status = "false"
                            mDataFailure.value = modelStatus
                        }


                    } catch (e: Exception) {
                        val modelStatus = ModelStatusMsg()
                        modelStatus.status = "false"
                        modelStatus.msg = "Error!"
                        mDataFailure.value = modelStatus
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus

            }

        })
    }


    fun getmDataFailure(): LiveData<ModelStatusMsg> {
        return mDataFailure
    }

    fun getmDataSignedUp(): LiveData<ModelGetProfile> {
        return mDataSigningUp
    }

    fun updateAddress(
        auth: String,
        userId: String,
        street: String,
        city: String,
        state: String,
        zipCode: String,
        suite: String,
        billingStreet: String,
        billingCity: String,
        billingState: String,
        billingZipcode: String,
        billingSuite: String,
        type: String,
        uType: String,
        value: String
    ) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> =
            service.updateAddressByMerchant(
                auth,
                userId,
                street,
                state,
                city,
                zipCode,
                suite,
                billingStreet,
                billingState,
                billingCity,
                billingZipcode,
                billingSuite,
                type,
                uType
            )
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        val modelAddress = ModelAddress()
                        if (status == "true") {
                            modelAddress.streetAddress = street
                            modelAddress.suiteNumber = suite
                            modelAddress.city = city
                            modelAddress.state = state
                            modelAddress.zipCode = zipCode
                            modelAddress.billingStreet = billingStreet
                            modelAddress.billingSuite = billingSuite
                            modelAddress.billingCity = billingCity
                            modelAddress.billingState = billingState
                            modelAddress.billingZip = billingZipcode
                            modelAddress.userId = userId
                            modelAddress.value = value

                            modelStatus.status = status
                            modelStatus.msg = msg
                            mDataUpdateAddress.value = modelAddress
                        } else {
                            modelStatus.status = "false"
                            modelStatus.msg = msg
                            mDataFailure.value = modelStatus
                        }


                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus

            }
        })

    }

    fun getUpdateProfile(
        userId: String,
        name: String,
        number: String,
        email: String,
        type: String,
        billingStreet: String,
        billingState: String,
        billingCity: String,
        billingZip: String,
        billingSuite: String,
        value: String,
        streetName: String,
        city: String,
        stateName: String,
        zipCode: String,
        suite: String
    ) {

        try {
            val retrofit = Retrofit.Builder()
                .baseUrl(Constant.BASE_URL)
                .build()

            val service = retrofit.create(WebServicesCustomers::class.java)
            val call: Call<ResponseBody> =
                service.updateProfileMerchant(userId, name, number, email, type)
            call.enqueue(object : Callback<ResponseBody> {

                override fun onResponse(
                    call: Call<ResponseBody>,
                    response: Response<ResponseBody>
                ) {
                    if (response.isSuccessful) {
                        try {
                            val res = response.body()!!.string()
                            val json = JSONObject(res)
                            val status = json.optString("status")
                            val msg = json.optString("msg")
                            val modelUpdateProfile = ModelGetProfile()
                            val modelStatusMsg = ModelStatusMsg()
                            if (status == "true") {
                                val data = json.optJSONObject("data")
                                val primary_address = data.optJSONObject("primary_address")
                                val billing_address = data.optJSONObject("billing_address")
                                modelUpdateProfile.profile_image = data.optString("profile_image")
                                modelUpdateProfile.is_merchant = data.optString("is_merchant")
                                modelUpdateProfile.zip_code = data.optString("zip_code")
                                modelUpdateProfile.name = data.optString("name")
                                modelUpdateProfile.contact_no = data.optString("contact_no")
                                modelUpdateProfile.email = data.optString("email")
                                modelUpdateProfile.gender = data.optString("gender")
                                modelUpdateProfile.birthdate = data.optString("birthdate")
                                modelUpdateProfile.is_merchant = data.optString("is_merchant")
                                modelUpdateProfile.steps = data.optString("steps")
                                modelUpdateProfile.business_id = data.optString("business_id")
                                modelUpdateProfile.tax = data.optInt("tax")
                                modelUpdateProfile.merchant_commission =
                                    data.optInt("merchant_commission")
                                modelUpdateProfile.wallet = data.optString("wallet")
                                modelUpdateProfile.feedback = data.optString("feedback")
                                modelUpdateProfile.social_id = data.optString("social_id")
                                modelUpdateProfile.invite_code = data.optString("invite_code")
                                modelUpdateProfile.reg_type = data.optString("reg_type")
                                modelUpdateProfile.email_verify = data.optString("email_verify")
                                modelUpdateProfile.type = data.optString("type")
                                modelUpdateProfile.value = value
                                modelUpdateProfile.user_id = userId
                                modelUpdateProfile.stripe_customer_id =
                                    data.optString("stripe_customer_id")
                                modelUpdateProfile.tax = data.optInt("tax")



                                if (billingStreet.isEmpty() || billingState.isEmpty() || billingCity.isEmpty() || billingZip.isEmpty()) {
                                    modelUpdateProfile.billing_address.billing_street =
                                        billing_address.optString("billing_street")
                                    modelUpdateProfile.billing_address.billing_city =
                                        billing_address.optString("billing_city")
                                    modelUpdateProfile.billing_address.billing_state =
                                        billing_address.optString("billing_state")
                                    modelUpdateProfile.billing_address.billing_zipcode =
                                        billing_address.optString("billing_zipcode")
                                    modelUpdateProfile.billing_address.billing_suite =
                                        billing_address.optString("billing_suite")
                                } else {
                                    modelUpdateProfile.billing_address.billing_city = billingCity
                                    modelUpdateProfile.billing_address.billing_state = billingState
                                    modelUpdateProfile.billing_address.billing_zipcode = billingZip
                                    modelUpdateProfile.billing_address.billing_suite = billingSuite
                                    modelUpdateProfile.billing_address.billing_street =
                                        billingStreet
                                }
                                if (streetName.isEmpty() || city.isEmpty() || stateName.isEmpty() || zipCode.isEmpty()) {
                                    modelUpdateProfile.primary_address.street =
                                        primary_address.optString("street")
                                    modelUpdateProfile.primary_address.city =
                                        primary_address.optString("city")
                                    modelUpdateProfile.primary_address.state =
                                        primary_address.optString("state")
                                    modelUpdateProfile.primary_address.zip_code =
                                        primary_address.optString("zip_code")
                                    modelUpdateProfile.primary_address.suite =
                                        primary_address.optString("suite")
                                } else {
                                    modelUpdateProfile.primary_address.street = streetName
                                    modelUpdateProfile.primary_address.city = city
                                    modelUpdateProfile.primary_address.state = stateName
                                    modelUpdateProfile.primary_address.zip_code = zipCode
                                    modelUpdateProfile.primary_address.suite = suite
                                }

                                modelUpdateProfile.status = status
                                modelUpdateProfile.msg = msg
                                modelStatusMsg.msg = msg
                                modelStatusMsg.status = status
                                mDataUpdateProfile.value = modelUpdateProfile

                            } else {
                                modelStatusMsg.msg = msg
                                mDataFailure.value = modelStatusMsg
                            }
//

                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                }

                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    val modelStatus = ModelStatusMsg()
                    modelStatus.status = "false"
                    modelStatus.msg = "Network Error!"
                    mDataFailure.value = modelStatus


                }


            })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


}