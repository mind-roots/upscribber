package com.upscribber.upscribberSeller.subsriptionSeller.percentage

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

class PercentageRepository(application: Application) {
    val mData = MutableLiveData<ArrayList<ModelPercentage>>()
    val arrayList = ArrayList<ModelPercentage>()
    fun getList(): LiveData<ArrayList<ModelPercentage>> {


        var modelPercentage = ModelPercentage()

        modelPercentage.header = "The Basics"
        modelPercentage.status =true
        modelPercentage.description =""
        modelPercentage.heading = ""
        arrayList.add(modelPercentage)

       modelPercentage = ModelPercentage()
        modelPercentage.header = ""
        modelPercentage.status =false
       modelPercentage.heading = "Add Product"
        modelPercentage.description =
            "Sed ut perspiciatis unde omnis iste natus visit voluptatem accusantium dolaudantium, totam rem aperiam, eaque."
        arrayList.add(modelPercentage)

        modelPercentage = ModelPercentage()
        modelPercentage.header = ""
        modelPercentage.status =false
        modelPercentage.heading = "Add subscription Price"

        modelPercentage.description =
            "Sed ut perspiciatis unde omnis iste natus visit voluptatem accusantium dolaudantium, totam rem aperiam, eaque."
        arrayList.add(modelPercentage)

        modelPercentage = ModelPercentage()
        modelPercentage.header = ""
        modelPercentage.status =false
        modelPercentage.heading = "Add Photo"

        modelPercentage.description =
            "Sed ut perspiciatis unde omnis iste natus visit voluptatem accusantium dolaudantium, totam rem aperiam, eaque."
        arrayList.add(modelPercentage)

        modelPercentage = ModelPercentage()
        modelPercentage.header = ""
        modelPercentage.status =false
        modelPercentage.heading = "Add subscription name"
        modelPercentage.description =
            "Sed ut perspiciatis unde omnis iste natus visit voluptatem accusantium dolaudantium, totam rem aperiam, eaque."
        arrayList.add(modelPercentage)

        modelPercentage = ModelPercentage()
        modelPercentage.header = ""
        modelPercentage.status =false
        modelPercentage.heading = "Add brief description"
        modelPercentage.description =
            "Sed ut perspiciatis unde omnis iste natus visit voluptatem accusantium dolaudantium, totam rem aperiam, eaque."
        arrayList.add(modelPercentage)

        modelPercentage = ModelPercentage()
        modelPercentage.description =""
        modelPercentage.heading = ""
        modelPercentage.header = "The Details"
        modelPercentage.status =true
        arrayList.add(modelPercentage)

//        modelPercentage = ModelPercentage()
//        modelPercentage.header = ""
//        modelPercentage.status =false
//         modelPercentage.heading = "Add subscription details"
//        modelPercentage.description =
//            "Sed ut perspiciatis unde omnis iste natus visit voluptatem accusantium dolaudantium, totam rem aperiam, eaque."
//        arrayList.add(modelPercentage)

        modelPercentage = ModelPercentage()
        modelPercentage.header = ""
        modelPercentage.status =false
         modelPercentage.heading = "Update privacy"
        modelPercentage.description =
            "Sed ut perspiciatis unde omnis iste natus visit voluptatem accusantium dolaudantium, totam rem aperiam, eaque."
        arrayList.add(modelPercentage)

        modelPercentage = ModelPercentage()
        modelPercentage.header = ""
        modelPercentage.status =false
         modelPercentage.heading = "Update subscriber Limit"
        modelPercentage.description =
            "Sed ut perspiciatis unde omnis iste natus visit voluptatem accusantium dolaudantium, totam rem aperiam, eaque."
        arrayList.add(modelPercentage)

        modelPercentage = ModelPercentage()
        modelPercentage.header = ""
        modelPercentage.status =false
        modelPercentage.heading = "Location Details"
        modelPercentage.description =
            "Sed ut perspiciatis unde omnis iste natus visit voluptatem accusantium dolaudantium, totam rem aperiam, eaque."
        arrayList.add(modelPercentage)

        modelPercentage = ModelPercentage()
        modelPercentage.header = ""
        modelPercentage.status =false
       modelPercentage.heading = "Tags"
        modelPercentage.description =
            "Sed ut perspiciatis unde omnis iste natus visit voluptatem accusantium dolaudantium, totam rem aperiam, eaque."
        arrayList.add(modelPercentage)

        mData.value = arrayList
        return mData

    }

    fun updateList(position: Int): MutableLiveData<ArrayList<ModelPercentage>> {
        arrayList[position].status=true
        mData.value=arrayList
        return mData

    }
}