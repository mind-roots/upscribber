package com.upscribber.upscribberSeller.subsriptionSeller.location.days

import android.app.Application

class SelectDaysRepository(application: Application) {
    fun getList(): ArrayList<ModelSelectDays> {
        val arrayList = ArrayList<ModelSelectDays>()

        var modelSelectDays = ModelSelectDays()
        modelSelectDays.headings = "Sunday"
        arrayList.add(modelSelectDays)


        modelSelectDays = ModelSelectDays()
        modelSelectDays.headings = "Monday"
        arrayList.add(modelSelectDays)


        modelSelectDays = ModelSelectDays()
        modelSelectDays.headings = "Tuesday"
        arrayList.add(modelSelectDays)


        modelSelectDays = ModelSelectDays()
        modelSelectDays.headings = "Wednesday"
        arrayList.add(modelSelectDays)


        modelSelectDays = ModelSelectDays()
        modelSelectDays.headings = "Thursday"
        arrayList.add(modelSelectDays)


        modelSelectDays = ModelSelectDays()
        modelSelectDays.headings = "Friday"
        arrayList.add(modelSelectDays)


        modelSelectDays = ModelSelectDays()
        modelSelectDays.headings = "Saturday"
        arrayList.add(modelSelectDays)

        return arrayList
    }
}