package com.upscribber.upscribberSeller.subsriptionSeller.subscriptionView.subsManage

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import com.upscribber.R
import com.upscribber.commonClasses.ModelStatusMsg
import kotlinx.android.synthetic.main.activity_successfully_delete.*

class SuccessfullyDeleteActivity : AppCompatActivity() {

    lateinit var buttonDone : TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_successfully_delete)
        initz()
        setData()
        clickListeners()
    }

    @SuppressLint("SetTextI18n")
    private fun setData() {
        if (intent.hasExtra("activity")){
            val campaign_name = intent.getStringExtra("activity")
            textView204.text = resources.getString(R.string.successfully_unredeem)
            textView.text = "You have unredeemed the your subscription."
        }else{
            textView204.text = resources.getString(R.string.successfully_deleted)
            textView.text = "Your Product has been\ndeleted"
        }

    }

    private fun initz() {
        buttonDone = findViewById(R.id.buttonDone)

    }

    private fun clickListeners() {
        buttonDone.setOnClickListener {
            if (intent.hasExtra("activity")){
                val intent = Intent()
                setResult(1, intent)
                finish()
            }else {
                val sharedPreferences = getSharedPreferences("customers", Context.MODE_PRIVATE)
                val editor = sharedPreferences.edit()
                editor.putString("customers", "customers")
                editor.apply()
                finish()
            }
        }

    }
}
