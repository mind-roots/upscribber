package com.upscribber.upscribberSeller.subsriptionSeller.percentage;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.upscribber.R;
import com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.createSubs.CreateSubscriptionActivity;
import com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.createSubs.CreateSubscriptionDetailsActivity;
import com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.createSubs.CreateSubscriptionExtraActivity;
import com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.createSubs.CreateSubsriptionBasicActivity;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;

public class HeaderAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    int count=0;
    private ArrayList<ModelPercentage> items = new ArrayList<ModelPercentage>();
    Context context;
    UpdateTick tick;

    public HeaderAdapter(Context context) {
        this.context = context;
        this.tick=(UpdateTick)context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            //inflate your layout and pass it to view holder
            View view= LayoutInflater.from(context).inflate(R.layout.prnew,parent,false);
            return new VHItem(view);
        } else if (viewType == TYPE_HEADER) {
            View view= LayoutInflater.from(context).inflate(R.layout.prhnew,parent,false);

            return new VHHeader(view);
        }

        throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof VHItem) {
            ModelPercentage dataItem = getItem(position);

            VHItem item = (VHItem) holder;

            try {
                item.heading.setText(items.get(position).heading);
                item.description.setText(items.get(position).description);
                if (items.get(position).getStatus()){
                    item.imagePlus.setImageResource(R.drawable.ic_check_circle);
                }else{
                    item.imagePlus.setImageResource(R.drawable.ic_plus_blue);
                }
            }catch (Exception e){
                e.printStackTrace();
            }

            item.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                   //tick.updateTick(position,++count);
                    if (position==1){
                      context.startActivity(new Intent(context, CreateSubsriptionBasicActivity.class));
                    }
                    if (position==2){
                        context.startActivity(new Intent(context, CreateSubscriptionExtraActivity.class));
                    }
                    if (position==3){
                        context.startActivity(new Intent(context, CreateSubsriptionBasicActivity.class));
                    }
                    if (position==4){
                        context.startActivity(new Intent(context, CreateSubsriptionBasicActivity.class));
                    }
                    if (position==5){
                        context.startActivity(new Intent(context, CreateSubscriptionDetailsActivity.class));
                    }
                    if (position==7){
                        context.startActivity(new Intent(context, CreateSubscriptionDetailsActivity.class));
                    }
                    if (position==8){
                        context.startActivity(new Intent(context, CreateSubscriptionDetailsActivity.class));
                    }
                    if (position==9){
                        context.startActivity(new Intent(context, CreateSubscriptionDetailsActivity.class));
                    }
                    if (position==10){
                        context.startActivity(new Intent(context, CreateSubscriptionDetailsActivity.class));
                    }
//                context.startActivity(new Intent(context, CreateSubscriptionActivity.class).putExtra("forCreate","forEdit"));

                }
            });
        } else if (holder instanceof VHHeader) {
            //cast holder to VHHeader and set data for header.
            try {
            VHHeader item = (VHHeader) holder;
            item.header.setText(items.get(position).header);
            }catch (Exception e){
                e.printStackTrace();
            }
        }

    }

    @Override
    public int getItemCount() {
        return items.size() ;
    }

    @Override
    public int getItemViewType(int position) {
        try {
            if (items.get(position).getStatus()&&!items.get(position).header.isEmpty())
                return TYPE_HEADER;

            return TYPE_ITEM;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 0;
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    private ModelPercentage getItem(int position) {
        return items.get(position - 1);
    }

    public void update(@Nullable ArrayList<ModelPercentage> it) {
        items=it;

        notifyDataSetChanged();
    }

    class VHItem extends RecyclerView.ViewHolder {
        TextView heading;
        TextView description;
        ImageView imagePlus;

        public VHItem(View itemView) {
            super(itemView);
            description=itemView.findViewById(R.id.description);
            heading=itemView.findViewById(R.id.heading);
            imagePlus=itemView.findViewById(R.id.imagePlus);
        }
    }

    class VHHeader extends RecyclerView.ViewHolder {
        TextView header;

        public VHHeader(View itemView) {
            super(itemView);
            header=itemView.findViewById(R.id.header);
        }
    }
    interface UpdateTick{
        void updateTick(int position, int i);
    }
}