package com.upscribber.upscribberSeller.subsriptionSeller.subscriptionView.subsManage

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData

class ViewModelManageList (application: Application) : AndroidViewModel(application) {

    var customerListManageRepository : CustomerListManageRepository = CustomerListManageRepository(application)

    fun getCustomers() : LiveData<List<ModelSubscriptionListManage>> {
        return  customerListManageRepository.getCustomersList()

    }

}