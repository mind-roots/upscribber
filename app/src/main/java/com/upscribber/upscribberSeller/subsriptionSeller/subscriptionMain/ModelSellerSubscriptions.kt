package com.upscribber.upscribberSeller.subsriptionSeller.subscriptionMain

import android.os.Parcel
import android.os.Parcelable
import com.upscribber.commonClasses.Constant
import kotlin.math.roundToInt

class ModelSellerSubscriptions() : Parcelable {

    var scount: String = ""
    var introductory_days: String = ""
    var quantity: String = ""
    var subscription_id: String = ""
    var campaign_id: String = ""
    var price: String = ""
    var discount_price: String = ""
    var unit: String = ""
    var created_at: String = ""
    var updated_at: String = ""
    var frequency_type: String = ""
    var free_trial: String = ""
    var frequency_value: String = ""
    var campaign_image: String = ""
    var campaign_name: String = ""
    var subscriber: String = ""
    var location_id: String = ""
    var description: String = ""
    var business_logo: String = ""
    var like: String = ""
    var like_percentage: String = ""
    var views: String = ""
    var status1: String = ""
    var complete_percentage: String = ""
    var introductory_price: String = ""
    var is_public: String = ""
    var status: String = ""
    var msg: String = ""
    var bought: String = ""
    var free_trial_qty: String = ""
    var is_discounted: String = ""

    constructor(parcel: Parcel) : this() {
        scount = parcel.readString()
        introductory_days = parcel.readString()
        quantity = parcel.readString()
        subscription_id = parcel.readString()
        campaign_id = parcel.readString()
        price = parcel.readString()
        discount_price = parcel.readString()
        unit = parcel.readString()
        created_at = parcel.readString()
        updated_at = parcel.readString()
        frequency_type = parcel.readString()
        free_trial = parcel.readString()
        frequency_value = parcel.readString()
        campaign_image = parcel.readString()
        campaign_name = parcel.readString()
        subscriber = parcel.readString()
        location_id = parcel.readString()
        description = parcel.readString()
        business_logo = parcel.readString()
        like = parcel.readString()
        like_percentage = parcel.readString()
        views = parcel.readString()
        status1 = parcel.readString()
        complete_percentage = parcel.readString()
        introductory_price = parcel.readString()
        is_public = parcel.readString()
        status = parcel.readString()
        msg = parcel.readString()
        bought = parcel.readString()
        free_trial_qty = parcel.readString()
        is_discounted = parcel.readString()
    }


    fun percentageComplete(): String {
        return "$complete_percentage% Complete"
    }

    fun like(): String {
        return "$like_percentage%"
    }

    fun offPrice(): String {
        return Constant.getCalculatedPrice(discount_price, price) + "% off"
    }


    fun getDiscountedPrice(): String {
        return "$" + (price.toDouble() - discount_price.toDouble()).toString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(scount)
        parcel.writeString(introductory_days)
        parcel.writeString(quantity)
        parcel.writeString(subscription_id)
        parcel.writeString(campaign_id)
        parcel.writeString(price)
        parcel.writeString(discount_price)
        parcel.writeString(unit)
        parcel.writeString(created_at)
        parcel.writeString(updated_at)
        parcel.writeString(frequency_type)
        parcel.writeString(free_trial)
        parcel.writeString(frequency_value)
        parcel.writeString(campaign_image)
        parcel.writeString(campaign_name)
        parcel.writeString(subscriber)
        parcel.writeString(location_id)
        parcel.writeString(description)
        parcel.writeString(business_logo)
        parcel.writeString(like)
        parcel.writeString(like_percentage)
        parcel.writeString(views)
        parcel.writeString(status1)
        parcel.writeString(complete_percentage)
        parcel.writeString(introductory_price)
        parcel.writeString(is_public)
        parcel.writeString(status)
        parcel.writeString(msg)
        parcel.writeString(bought)
        parcel.writeString(free_trial_qty)
        parcel.writeString(is_discounted)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ModelSellerSubscriptions> {
        override fun createFromParcel(parcel: Parcel): ModelSellerSubscriptions {
            return ModelSellerSubscriptions(parcel)
        }

        override fun newArray(size: Int): Array<ModelSellerSubscriptions?> {
            return arrayOfNulls(size)
        }
    }


}