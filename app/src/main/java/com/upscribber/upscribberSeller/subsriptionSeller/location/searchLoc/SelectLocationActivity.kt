package com.upscribber.upscribberSeller.subsriptionSeller.location.searchLoc

import android.app.Activity
import android.content.Intent
import android.graphics.Outline
import android.location.Address
import android.location.Geocoder
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.*
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.upscribber.commonClasses.Constant
import com.upscribber.R
import com.upscribber.databinding.ActivitySelectLocationBinding
import com.upscribber.databinding.SetPrimaryLocBinding
import com.upscribber.filters.*
import com.upscribber.filters.Redemption.RedemptionFilterFragment
import com.upscribber.commonClasses.RoundedBottomSheetDialogFragment
import com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.createSubs.CreateSubscriptionDetailsActivity
import kotlinx.android.synthetic.main.filter_header.*

class SelectLocationActivity : AppCompatActivity(), AdapterSearchLocation.Selectionn,
    FilterInterface,
    DrawerLayout.DrawerListener, FilterFragment.SetFilterHeaderBackground {

    private lateinit var mbinding: ActivitySelectLocationBinding
    private lateinit var viewModel: SearchLocationViewModel
    private lateinit var mAdapter: AdapterSearchLocation
    private lateinit var backgroundChange: ArrayList<ModelSearchLocation>
    private lateinit var filter_done: TextView
    private lateinit var fragmentManager: FragmentManager
    private lateinit var fragmentTransaction: FragmentTransaction
    private lateinit var mreset: TextView
    private lateinit var linearProgress: LinearLayout
    private lateinit var mfilters: TextView
    var drawerOpen = 0
    val locationModel = LocationModel()
    private var addresses: MutableList<Address> = ArrayList()
    var latitude = 0.0
    var longitude = 0.0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mbinding = DataBindingUtil.setContentView(this, R.layout.activity_select_location)
        mbinding.lifecycleOwner = this
        viewModel = ViewModelProviders.of(this)[SearchLocationViewModel::class.java]
        Places.initialize(applicationContext, resources.getString(R.string.google_maps_key))
        initz()
        if (intent.hasExtra("navigation")) {
            mbinding.textView206.visibility = View.GONE
        } else {
            mbinding.textView206.visibility = View.GONE
        }
        setToolbar()
        apiImplimentation()
        observerInit()
        setClicks()
    }

    private fun observerInit() {
        linearProgress.visibility = View.GONE
        viewModel.getStatus().observe(this, Observer {
            if (it.msg.isNotEmpty()) {
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
            }
        })

        viewModel.getmDataDelete().observe(this, Observer {
            linearProgress.visibility = View.GONE
            if (it.status == "true") {
                apiImplimentation()
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
            }
        })


        viewModel.getmDataDefault().observe(this, Observer {
            linearProgress.visibility = View.GONE
            if (it.status == "true") {
                mAdapter.update(backgroundChange)
                apiImplimentation()
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
            }
        })




        viewModel.getLocationData().observe(this, Observer {
            linearProgress.visibility = View.GONE
            if (it.size > 0) {
                mbinding.dummyText.visibility = View.GONE
                mbinding.card.visibility = View.GONE
                mbinding.nestedScroll.visibility = View.VISIBLE
                setAdapter()
                backgroundChange = it
                if (intent.hasExtra("selectedArray")) {
                    val arraySelected =
                        intent.getParcelableArrayListExtra<ModelSearchLocation>("selectedArray")
                    for (i in 0 until backgroundChange.size) {
                        for (item in arraySelected) {
                            if (item.id == backgroundChange[i].id) {
                                backgroundChange[i].status = true
                                mbinding.textView206.visibility = View.VISIBLE
                            }
                        }

                    }
                }

                mAdapter.update(it)
            } else {
                mbinding.dummyText.visibility = View.VISIBLE
                mbinding.card.visibility = View.VISIBLE
                mbinding.nestedScroll.visibility = View.GONE
            }

        })


    }

    private fun apiImplimentation() {
        val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
        linearProgress.visibility = View.VISIBLE
        viewModel.getAllLocations(auth,"")

    }

    override fun onResume() {
        super.onResume()
        apiImplimentation()
    }


//    override fun onResume() {
//        super.onResume()
//        if (intent.hasExtra("navigation")) {
//
//            viewModel = ViewModelProviders.of(this)[SearchLocationViewModel::class.java]
//            mbinding.recyclerLocations.layoutManager = LinearLayoutManager(this)
//            var value = ""
//            if (intent.hasExtra("navigation")) {
//                value = intent.getStringExtra("navigation")
//
//            } else {
//                value = "other"
//            }
//            mAdapter = AdapterSearchLocation(this, this, value)
//            mbinding.recyclerLocations.adapter = mAdapter
////            backgroundChange = viewModel.getLocations()
//            mAdapter.update(backgroundChange)
//        } else {
//
//        }
//
//        Constant.hideKeyboard(this, mbinding.search)
//    }

    private fun initz() {
        mreset = findViewById(R.id.reset)
        mfilters = findViewById(R.id.filters)
        filter_done = findViewById(R.id.filter_done)
        linearProgress = findViewById(R.id.linearProgress)

    }

    private fun setClicks() {
        mbinding.textView206.setOnClickListener {

            val mmm: ArrayList<ModelSearchLocation> = ArrayList()
            for (i in 0 until backgroundChange.size) {
                if (backgroundChange[i].status) {
                    mmm.add(backgroundChange[i])
                }
            }
            if (mmm.size > 0) {
                var timings = 0
                for (items in mmm) {
                    if (items.store_timing.size > 0) {
                        timings++
                    } else {
                        timings--
                    }
                }
                if (timings == mmm.size) {
                    val intent = Intent(this, CreateSubscriptionDetailsActivity::class.java)
                    intent.putExtra("model", mmm)
                    setResult(Activity.RESULT_OK, intent)
                    finish()
                } else {
                    Toast.makeText(
                        this,
                        "Please select location with store timings.",
                        Toast.LENGTH_LONG
                    ).show()
                }
//                update1.setSelectedBussines(selectedModel!!)
            } else {
                Toast.makeText(
                    this,
                    "Please select location with store timings.",
                    Toast.LENGTH_LONG
                ).show()
            }
        }


        mbinding.search.setOnQueryTextListener(object : androidx.appcompat.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
//                Constant.hideKeyboard(this@SelectLocationActivity,mbinding.search)
//                val auth = Constant.getPrefs(this@SelectLocationActivity).getString(Constant.auth_code, "")
//
//                viewModel.getAllLocations(auth,query)

                return true
            }

            override fun onQueryTextChange(newText: String): Boolean {
                val arrayListSeach = ArrayList<ModelSearchLocation>()
                for (i in backgroundChange){
                    if (i.locationName.contains(newText)){
//                        arrayListSeach = ArrayList()
                        arrayListSeach.add(i)
                    }
                }

                if (arrayListSeach.size > 0){
                    mAdapter.update(arrayListSeach)
                }else{
                    mbinding.dummyText.visibility = View.VISIBLE
                    mbinding.card.visibility = View.VISIBLE
                    mbinding.nestedScroll.visibility = View.GONE
                }


                return true

            }
        })




        mbinding.btnFilter.setOnClickListener {
            drawerOpen = 1
            Constant.hideKeyboard(this, mbinding.search)
            mbinding.search.setQuery("", false)
            mbinding.search.clearFocus();
            mbinding.drawerLayout1.openDrawer(GravityCompat.END)
            inflateFragment(FilterFragment(this))
            filterImgBack.visibility = View.GONE
            mfilters.text = "Filters"
            mreset.visibility = View.VISIBLE

        }

        filter_done.setOnClickListener {
            onBackPressed()
        }
    }

    fun addLoc(v: View) {
        Places.createClient(this)
        val fields =
            listOf(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS)
        // Start the autocomplete intent.
        val intent = Autocomplete.IntentBuilder(
                AutocompleteActivityMode.FULLSCREEN, fields
            )
            .build(this)
        startActivityForResult(intent, 2)
//    }


//        startActivity(Intent(this, AddLocationActivity::class.java))
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 2) {
            when (resultCode) {
                Activity.RESULT_OK -> {

                    try {
                        val place = Autocomplete.getPlaceFromIntent(data!!)
                        val latLng = place.latLng
                        latitude = latLng!!.latitude
                        longitude = latLng.longitude
                        locationModel.address = place.address.toString()
                        val geocoder = Geocoder(this)
                        addresses =
                            latLng.latitude.let { geocoder.getFromLocation(it, latLng.longitude, 1) }
                        if (addresses.isNotEmpty()) {
                            locationModel.address = addresses[0].getAddressLine(0)
                            locationModel.street = addresses[0].thoroughfare
                            locationModel.city = addresses[0].locality
                            locationModel.state = addresses[0].adminArea
                            locationModel.zipcode = addresses[0].postalCode
                            locationModel.latitude = latitude.toString()
                            locationModel.longitude = longitude.toString()
                            startActivity(
                                Intent(
                                    this,
                                    MapsActivity::class.java
                                ).putExtra("location1", locationModel)
                            )
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }
        }
    }


    private fun setAdapter() {
//        if (Constant.getSharedPrefs(this).getBoolean("empty2", true)) {
//            mbinding.dummyText.visibility = View.VISIBLE
//            mbinding.card.visibility = View.VISIBLE
//            mbinding.textView206.visibility = View.GONE
//            mbinding.recyclerLocations.visibility = View.GONE
//        } else {
//            mbinding.dummyText.visibility = View.GONE
//            mbinding.card.visibility = View.GONE
//            mbinding.textView206.visibility = View.VISIBLE
//            mbinding.recyclerLocations.visibility = View.VISIBLE
        var value = ""
        value = if (intent.hasExtra("navigation")) {
            intent.getStringExtra("navigation")

        } else {
            "other"
        }

        mbinding.recyclerLocations.layoutManager = LinearLayoutManager(this)
        mAdapter = AdapterSearchLocation(this, this, value)
        mbinding.recyclerLocations.adapter = mAdapter
//        }

    }

    override fun onSelectionBackgroundd(position: Int) {
        var exits = 0
        val model = backgroundChange[position]
        model.status = !model.status

        backgroundChange[position] = model
        for (item in backgroundChange) {
            if (item.status) {
                exits = 1
                break
            }
        }
        if (exits == 1) {
            mbinding.textView206.visibility = View.VISIBLE
        } else {
            mbinding.textView206.visibility = View.GONE
        }
        mAdapter.notifyDataSetChanged()

    }

    override fun SetSingleSelection(pos: Int) {

        val model = backgroundChange[pos]
        if (model.statusPrimary) {
            model.statusPrimary = false

        } else {
            for (child in backgroundChange) {
                child.statusPrimary = false
            }
            model.statusPrimary = true
        }
        backgroundChange[pos] = model


        mAdapter.notifyDataSetChanged()

    }

    override fun SetPrimary(positionn: Int) {
        val fragment =
            MenuFragmentPrimary(1, mAdapter, backgroundChange, positionn, viewModel, linearProgress)
        fragment.show(supportFragmentManager, fragment.tag)

    }

    override fun delete(pos: Int) {
        val fragment =
            MenuFragmentPrimary(2, mAdapter, backgroundChange, pos, viewModel, linearProgress)
        fragment.show(supportFragmentManager, fragment.tag)
    }

    class MenuFragmentPrimary(
        var i: Int,
        var mAdapter: AdapterSearchLocation,
        var backgroundChange: ArrayList<ModelSearchLocation>,
        var positionn: Int,
        var viewModel: SearchLocationViewModel,
        var linearProgress: LinearLayout
    ) : RoundedBottomSheetDialogFragment() {
        lateinit var binding: SetPrimaryLocBinding

        var status: Boolean = false

        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            binding = DataBindingUtil.inflate(inflater, R.layout.set_primary_loc, container, false)
            return binding.root
        }

        override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
            super.onViewCreated(view, savedInstanceState)
            val auth = Constant.getPrefs(activity!!).getString(Constant.auth_code, "")
            if (i == 1) {
                binding.textHeading.text = "Set As Default"
                binding.textDescription.text =
                    resources.getString(R.string.are_you_sure_you_want_to_set)
            } else {
                binding.textHeading.text = "Delete Location"
                binding.textDescription.text =
                    resources.getString(R.string.are_you_sure_you_want_to_delete)
            }

            binding.no.setOnClickListener {
                dismiss()
            }

            binding.yes.setOnClickListener {
                if (i == 1) {
                    viewModel.getPrimaryLocation(auth, backgroundChange[positionn].id)
                    dismiss()
                } else if (backgroundChange[positionn].id == backgroundChange[0].default) {
                    Toast.makeText(
                        context,
                        "This Location is marked as default location, you can not delete this",
                        Toast.LENGTH_SHORT
                    ).show()
                    dismiss()
                } else {
//                    linearProgress.visibility = View.VISIBLE
                    viewModel.getDeleteLocation(
                        auth,
                        backgroundChange[positionn].id,
                        backgroundChange[0].default
                    )
                    dismiss()
                }

            }
            roundedImage()
        }

        private fun roundedImage() {
            val curveRadius = 50F

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                binding.imageViewTop.outlineProvider = object : ViewOutlineProvider() {

                    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                    override fun getOutline(view: View?, outline: Outline?) {
                        outline?.setRoundRect(
                            0,
                            0,
                            view!!.width,
                            view.height + curveRadius.toInt(),
                            curveRadius
                        )
                    }
                }
                binding.imageViewTop.clipToOutline = true
            }
        }
    }

    private fun setToolbar() {
        if (intent.hasExtra("navigation")) {
            mbinding.constraintBackground.visibility = View.VISIBLE
            mbinding.locationsToolbar.visibility = View.GONE
            setSupportActionBar(mbinding.CustomerListToolbar)
            title = ""
            mbinding.titleToolbar.text = "Store Location"
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)
            roundImage()
        } else {
            mbinding.constraintBackground.visibility = View.GONE
            mbinding.locationsToolbar.visibility = View.VISIBLE
            setSupportActionBar(mbinding.locationsToolbar)
            title = ""
            mbinding.title.text = "Location"
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)
        }
    }

    private fun roundImage() {
        val curveRadius = 50F

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            mbinding.constraintBackground.outlineProvider = object : ViewOutlineProvider() {

                @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                override fun getOutline(view: View?, outline: Outline?) {
                    outline?.setRoundRect(
                        0,
                        -100,
                        view!!.width,
                        view.height,
                        curveRadius
                    )
                }
            }
            mbinding.constraintBackground.clipToOutline = true
        }

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun nextFragments(i: Int) {
        when (i) {
            1 -> {
                inflateFragment(SortByFragment())
                mreset.visibility = View.GONE
                filterImgBack.visibility = View.VISIBLE
                filters.text = "Sort By"

                filterImgBack.setOnClickListener {
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"
                }

                filter_done.setOnClickListener {
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"
                }
            }


            2 -> {
                inflateFragment(CategoryFilterFragment(this))
                mreset.visibility = View.GONE
                filterImgBack.visibility = View.VISIBLE
                filters.text = "Category"

                filterImgBack.setOnClickListener {
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"

                }
                filter_done.setOnClickListener {
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"
                }
            }


            3 -> {
                inflateFragment(BrandsFilterFragment())
                mreset.visibility = View.GONE
                filterImgBack.visibility = View.VISIBLE
                filters.text = "Brands"

                filterImgBack.setOnClickListener {
                    Constant.hideKeyboard(this, mbinding.search)
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"

                }

                filter_done.setOnClickListener {
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"
                }
            }
            4 -> {
                inflateFragment(RedemptionFilterFragment())
                mreset.visibility = View.GONE
                filterImgBack.visibility = View.VISIBLE
                filters.text = "Redemption"

                filterImgBack.setOnClickListener {
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"

                }
                filter_done.setOnClickListener {
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"
                }
            }
        }

    }

    private fun inflateFragment(fragment: Fragment) {
        fragmentManager = supportFragmentManager
        this.fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.containerSide1, fragment)
        fragmentTransaction.commit()
    }

    override fun onBackPressed() {
        if (drawerOpen == 1) {
            drawerOpen = 0
            mbinding.drawerLayout1.closeDrawer(GravityCompat.END)
        } else {
            super.onBackPressed()
        }
    }

    override fun onDrawerStateChanged(newState: Int) {

    }

    override fun onDrawerSlide(drawerView: View, slideOffset: Float) {

    }

    override fun onDrawerClosed(drawerView: View) {
        drawerOpen = 0
    }

    override fun onDrawerOpened(drawerView: View) {
        drawerOpen = 1
        mbinding.drawerLayout1.openDrawer(GravityCompat.END)
        inflateFragment(FilterFragment(this))
        filterImgBack.visibility = View.GONE
        mfilters.text = "Filters"
        mreset.visibility = View.VISIBLE
    }

    override fun filterHeaderBackground(s: String) {
        if (s == "new") {
            filterHeader.setBackgroundDrawable(getDrawable(R.drawable.filter_header_select_category))


        } else {
            filterHeader.setBackgroundDrawable(getDrawable(R.drawable.filter_header))

        }
    }
}


