package com.upscribber.upscribberSeller.subsriptionSeller.extras

import android.graphics.Outline
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.*
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.upscribber.R
import com.upscribber.databinding.ActivityExtraInfoBinding
import com.upscribber.databinding.EditAddOnBinding
import com.upscribber.commonClasses.RoundedBottomSheetDialogFragment
import com.upscribber.upscribberSeller.navigationCustomer.AdapterCustomerList

class ExtraInfoActivity : AppCompatActivity() {

    private lateinit var mbinding: ActivityExtraInfoBinding
    private lateinit var viewModel: ExtraInfoViewModel
    private lateinit var mAdapter: AdapterCustomerList

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mbinding = DataBindingUtil.setContentView(this,R.layout.activity_extra_info)
        mbinding.lifecycleOwner = this
        setToolbar()
        roundImage()
        setAdapter()
    }

    private fun setAdapter() {

        viewModel = ViewModelProviders.of(this)[ExtraInfoViewModel::class.java]

        val layoutManager = LinearLayoutManager(this)
        mbinding.customerListRecycler.layoutManager = layoutManager
        mbinding.customerListRecycler.hasFixedSize()
        mAdapter = AdapterCustomerList(this)
        mbinding.customerListRecycler.adapter = mAdapter

        viewModel.getCustomerList().observe(this, Observer {
//            mAdapter.update(it)
        })
    }

    private fun setToolbar() {
        setSupportActionBar(mbinding.toolbarProducts.toolbar)
        title = ""
        mbinding.toolbarProducts.title.text="Add On's"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)

    }

    private fun roundImage() {
        val curveRadius = 50F

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            mbinding.constraintLayout9.outlineProvider = object : ViewOutlineProvider() {

                @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                override fun getOutline(view: View?, outline: Outline?) {
                    outline?.setRoundRect(
                        0,
                        -100,
                        view!!.width,
                        view.height,
                        curveRadius
                    )
                }
            }
            mbinding.constraintLayout9.clipToOutline = true
        }

    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }

        if(item.itemId == R.id.storeProduct){
            val fragment = ExtraInfoActivity.MenuFragment()
            fragment.show(supportFragmentManager, fragment.tag)
        }

        return super.onOptionsItemSelected(item)
    }

    class MenuFragment: RoundedBottomSheetDialogFragment() {

        lateinit var mBinding : EditAddOnBinding

        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
            mBinding = DataBindingUtil.inflate(inflater, R.layout.edit_add_on, container, false)
            return mBinding.root

        }

        override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
            super.onViewCreated(view, savedInstanceState)

            mBinding.addValue.setOnClickListener {
                dismiss()
            }
            roundedImage()
        }

        private fun roundedImage() {
            val curveRadius = 50F

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                mBinding.imageViewTop1.outlineProvider = object : ViewOutlineProvider() {

                    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                    override fun getOutline(view: View?, outline: Outline?) {
                        outline?.setRoundRect(
                            0,
                            0,
                            view!!.width,
                            view.height + curveRadius.toInt(),
                            curveRadius
                        )
                    }
                }
                mBinding.imageViewTop1.clipToOutline = true
            }

        }

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.store_product, menu)
        return super.onCreateOptionsMenu(menu)
    }

}
