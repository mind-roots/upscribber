package com.upscribber.upscribberSeller.subsriptionSeller.subscriptionMain

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.upscribber.commonClasses.ModelStatusMsg
import com.upscribber.subsciptions.manageSubscriptions.shareSubscription.ModelShareRedeem
import com.upscribber.upscribberSeller.customerBottom.profile.subscriptionList.SubscriptionListModel
import com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.subscription.ModelSubscriptionCard
import com.upscribber.upscribberSeller.subsriptionSeller.subscriptionView.ModelCampaignSubscriber

class SubscriptionSellerViewModel(application: Application) : AndroidViewModel(application) {

    var subscriptionSellerRepository: SubscriptionSellerRepository =
        SubscriptionSellerRepository(application)

    fun getListStatus(): ArrayList<ModelStatus> {
        return subscriptionSellerRepository.getStatusList()
    }

    fun getAllSubscriptionsData(auth: String, query: String) {
        subscriptionSellerRepository.getAllSubscriptionsData(auth,query)
    }

    fun getStatus(): LiveData<ModelStatusMsg> {
        return subscriptionSellerRepository.getmDataFailure()
    }

    fun getmAllCampaignsData(): LiveData<ArrayList<ModelSellerSubscriptions>> {
        return subscriptionSellerRepository.getmAllCampaignsData()
    }


    fun getmDataCampaignsActiveOnly(): LiveData<ArrayList<ModelSellerSubscriptions>> {
        return subscriptionSellerRepository.getmDataCampaignsActiveOnly()
    }


    fun getCampaign(campaignId: String) {
        subscriptionSellerRepository.getCampaigns(campaignId)
    }


    fun getmCampaignData(): LiveData<ModelMain> {
        return subscriptionSellerRepository.getmCampaignData()
    }

    fun getStatusCampaign(): LiveData<ModelStatusMsg> {
        return subscriptionSellerRepository.getmCampaignDataFailure()
    }

    fun cloneCampaign(campaignId: String) {
        subscriptionSellerRepository.cloneCampaign(campaignId)
    }

    fun deleteCampaign(type: String, campaignId: String, subscriptionId: String) {
        subscriptionSellerRepository.deleteCampaign(type, campaignId, subscriptionId)
    }

    fun inactiveCampaign(campaignId: String, type: String) {
        subscriptionSellerRepository.changeCampaignStatus(campaignId, type)
    }

    fun getActiveInactivResponse(): LiveData<ModelStatusMsg> {
        return subscriptionSellerRepository.getmActiveStatus()
    }

     fun getmDataDeleteCampaign(): LiveData<ModelStatusMsg> {
        return subscriptionSellerRepository.getmDataDeleteCampaign()
    }



    fun getmActiveStatus1(): LiveData<ModelSubscriptionCard> {
        return subscriptionSellerRepository.getmActiveStatus1()
    }


    fun getSubsList(): LiveData<ArrayList<ModelShareRedeem>> {
        return subscriptionSellerRepository.getSubsList()
    }

    fun getSubscrpList(): LiveData<ArrayList<SubscriptionListModel>> {
        return subscriptionSellerRepository.getSubscrpList()
    }


    fun getCampaignSubscriptions(campaignsId: String) {
        subscriptionSellerRepository.getCampaignSubs(campaignsId)

    }


    fun getCampaignSubscriptionList(campaignsId: String) {
        subscriptionSellerRepository.getCampaignSubsList(campaignsId)

    }

    fun setUpdateCampDisStatus(campaignId: String, status: String, type: String,limit:String) {
        subscriptionSellerRepository.updateCampDisStatus(campaignId, status, type,limit)
    }

    fun getSubscriptionList(auth: String, view_type: String, page: String, customerId: String) {
        subscriptionSellerRepository.getSubscriptionsList(auth, view_type, page, customerId)

    }

    fun getActiveSubsOnly(auth: String) {
        subscriptionSellerRepository.getActiveSubsOnly(auth)

    }

    fun getCampaignSubscriber(page: Int, campaignId: String) {
        subscriptionSellerRepository.getCampaignSubscriber(page,campaignId)
    }

    fun getSubscriberInfoList(): LiveData<ArrayList<ModelCampaignSubscriber>> {
        return subscriptionSellerRepository.getSubscriberInfoList()
    }

}