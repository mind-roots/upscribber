package com.upscribber.upscribberSeller.subsriptionSeller.addFeatures

class ModelAddFeatures {

    var featureName : String = ""
    var featureDescription : String = ""
    var status : Boolean  = false
    var textUpdate : Boolean  = false
}