package com.upscribber.upscribberSeller.subsriptionSeller.extras

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.upscribber.upscribberSeller.navigationCustomer.ModelCustomerList

class ExtraInfoViewModel(application: Application) : AndroidViewModel(application)  {

    var ExtraInfoRepository : ExtraInfoRepository =
        ExtraInfoRepository(application)

    fun getCustomerList(): LiveData<ArrayList<ModelCustomerList>> {
        return  ExtraInfoRepository.getList()

    }



}