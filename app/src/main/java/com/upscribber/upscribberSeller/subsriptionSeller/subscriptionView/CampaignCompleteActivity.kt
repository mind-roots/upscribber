package com.upscribber.upscribberSeller.subsriptionSeller.subscriptionView

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.upscribberSeller.navigationCustomer.InviteSubscription
import com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.createSubs.BasicModel
import com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.subscription.ModelSubscriptionCard
import kotlinx.android.synthetic.main.activity_campaign_complete.*
import java.math.BigDecimal
import java.math.RoundingMode

@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class CampaignCompleteActivity : AppCompatActivity() {

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_campaign_complete)

        val myPrefs = getSharedPreferences("data", Context.MODE_PRIVATE)
        val gson = Gson()
        val getBasic = myPrefs.getString("basicData", "")
        val getPricing = myPrefs.getString("pricing", "")
        if (getBasic.isNotEmpty() && getPricing.isNotEmpty()) {
            val basicData = gson.fromJson(getBasic, BasicModel::class.java)
            val turnsType = object : TypeToken<ArrayList<ModelSubscriptionCard>>() {}.type
            val pricingData: ArrayList<ModelSubscriptionCard> = gson.fromJson(getPricing, turnsType)
            textView336.text = basicData.name

            if (pricingData.size > 0) {
                if (pricingData[0].frequencyType == "3" || pricingData[0].frequencyType == "Year" || pricingData[0].frequencyType == "Yearly") {

                    storeName.text = "Yearly"
                }
                if (pricingData[0].frequencyType == "2" || pricingData[0].frequencyType == "Month" || pricingData[0].frequencyType == "Monthly") {

                    storeName.text = "Monthly"
                }

                val price = Constant.getCalculatedPrice(pricingData[0].discountPrice,pricingData[0].price)
                if (price.contains(".")){
                    val split = price.split(".")
                    if (split[1] == "00" || split[1] == "0") {
                        amount.text =  "$" + split[0]

                    }else{
                        amount.text =  "$" + BigDecimal(price).setScale(2, RoundingMode.HALF_UP).toString()
                    }
                }else{
                    amount.text =  "$" + BigDecimal(price).setScale(2, RoundingMode.HALF_UP).toString()
                }


                if (pricingData[0].redeemtion_cycle_qty == "500000") {
                    service.text = "Unlimited " + pricingData[0].quantityName
                } else {
                    service.text =
                        pricingData[0].redeemtion_cycle_qty + " " + pricingData[0].quantityName
                }


            }


        }
    }

    fun doLater(v: View) {
        val sharedPreferences = getSharedPreferences("done", Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString("done", "back")
        if (intent.hasExtra("product")) {
            editor.putString("product", "product")
        }
        editor.apply()
        finish()
    }

    fun inviteClick(v: View) {
        val myPrefs = getSharedPreferences("data", Context.MODE_PRIVATE)
        val gson = Gson()
        val getBasic = myPrefs.getString("basicData", "")
        val getPricing = myPrefs.getString("pricing", "")
        startActivity(
            Intent(this, InviteSubscription::class.java).putExtra("subscriptionData", getBasic)
                .putExtra("subsDataPricing", getPricing)
        )
        finish()
    }

    override fun onResume() {
        super.onResume()
        val sharedPreferences1 = getSharedPreferences("customers", Context.MODE_PRIVATE)
        if (!sharedPreferences1.getString("customer", "").isEmpty()) {
            finish()
        }
        val editor = sharedPreferences1.edit()
        editor.remove("customer")
        editor.apply()
    }
}