package com.upscribber.upscribberSeller.subsriptionSeller.subscriptionView.subsManage

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.upscribber.R
import com.upscribber.databinding.ActivityConfirmInActiveBinding

class ConfirmInActiveActivity : AppCompatActivity() {

    lateinit var mBinding : ActivityConfirmInActiveBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this,R.layout.activity_confirm_in_active)
        clickListeners()
    }

    private fun clickListeners() {
        mBinding.done.setOnClickListener {
            val sharedPreferences = getSharedPreferences("customers", Context.MODE_PRIVATE)
            val editor = sharedPreferences.edit()
            editor.putString("customer","back")
            editor.apply()
            finish()
        }

    }
}
