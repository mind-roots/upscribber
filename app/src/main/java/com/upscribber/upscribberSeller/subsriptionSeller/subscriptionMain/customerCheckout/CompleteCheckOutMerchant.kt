package com.upscribber.upscribberSeller.subsriptionSeller.subscriptionMain.customerCheckout

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.R
import com.upscribber.becomeseller.seller.ModelAddress
import com.upscribber.checkout.AdapterCards
import com.upscribber.checkout.CheckOutViewModel
import com.upscribber.checkout.ModelCheckoutOne
import com.upscribber.checkout.cardSelected
import com.upscribber.commonClasses.Constant
import com.upscribber.payment.paymentCards.AddCardActivity
import com.upscribber.payment.paymentCards.PaymentCardModel
import com.upscribber.profile.ModelGetProfile
import com.upscribber.upscribberSeller.subsriptionSeller.subscriptionMain.ModelSellerSubscriptions
import kotlinx.android.synthetic.main.activity_check_out_merchant.*
import kotlinx.android.synthetic.main.checkout_two_total.textView140
import kotlinx.android.synthetic.main.checkout_two_total_merchant.*
import kotlinx.android.synthetic.main.toolbar.view.*
import java.math.BigDecimal
import java.math.RoundingMode

class CompleteCheckOutMerchant : AppCompatActivity(), cardSelected {

    private lateinit var categoriesList: ArrayList<PaymentCardModel>
    private lateinit var adapterCards: AdapterCards
    lateinit var mViewModel: CheckOutViewModel
    var taxx: String = "0"
    var modelProfile = ModelGetProfile()
    var modelAddress = ModelAddress()
    var modelAddSignature = ModelAddSignatureData()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_check_out_merchant)
        mViewModel = ViewModelProviders.of(this)[CheckOutViewModel::class.java]
        setAdapter()
        setToolbar()
        setColors()
        apiImplimentation()
        clickListeners()
        observerInit()
    }

    private fun observerInit() {

        mViewModel.getmDataFailureTax().observe(this, Observer {
            progressBar.visibility = View.GONE
            if (it.msg == "Invalid auth code") {
                Constant.commonAlert(this)
            } else {
                finish()
                Toast.makeText(this, "Please give valid details of location!", Toast.LENGTH_SHORT)
                    .show()
            }
        })


        mViewModel.getmDataTax().observe(this, Observer {
            taxx = it.tax
            tax.text = "$$taxx"
            setData()
            progressBar.visibility = View.GONE
        })

    }

    private fun apiImplimentation() {
        val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
        val modelSubscriptionData =
            intent.getParcelableExtra<ModelSellerSubscriptions>("modelSubscriptionModel")
        val modelCheckOutOne = intent.getParcelableExtra<ModelCheckoutOne>("modelCheckoutMerchant")
        var zip = ""
        var state = ""
        var priceCalculate = ""
        if (modelCheckOutOne.billingType == "0" || modelCheckOutOne.billingType == "") {
            modelCheckOutOne.billingZip = modelCheckOutOne.zipCode
            modelCheckOutOne.billingState = modelCheckOutOne.stateName
            zip = modelCheckOutOne.zipCode
            state = modelCheckOutOne.stateName
        } else {
            zip = modelCheckOutOne.billingZip
            state = modelCheckOutOne.billingState
        }

        if (intent.hasExtra("totalPriceText")) {
            priceCalculate = intent.getStringExtra("totalPriceText").replace("$", "")
        }

        mViewModel.getTax(auth, priceCalculate, modelSubscriptionData.campaign_id, zip, state)
        progressBar.visibility = View.VISIBLE

    }

    private fun setAdapter() {
        recyclerViewCards.layoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
        adapterCards = AdapterCards(this, 2)
        recyclerViewCards.adapter = adapterCards

    }

    @SuppressLint("SetTextI18n")
    private fun setData() {
        val modelSubscriptionData =
            intent.getParcelableExtra<ModelSellerSubscriptions>("modelSubscriptionModel")
        modelProfile = intent.getParcelableExtra<ModelGetProfile>("modelProfile")
        val modelCheckOutOne = intent.getParcelableExtra<ModelCheckoutOne>("modelCheckoutMerchant")
        var is_intro = ""
        var actualPriceParam = ""
        var totalPrice = ""


        val primaryAddress =
            modelProfile.primary_address.street + " " + modelProfile.primary_address.city + " " + modelProfile.primary_address.state + " " + modelProfile.primary_address.zip_code
        val billingAddress =
            modelProfile.billing_address.billing_street + " " + modelProfile.billing_address.billing_city + " " + modelProfile.billing_address.billing_state + " " + modelProfile.billing_address.billing_zipcode


//        if (modelProfile.wallet == "0" ||modelProfile.wallet == "0.00"){
//            credits.visibility = View.GONE
//            creditApplied.visibility = View.GONE
//        }else{
//            credits.visibility = View.VISIBLE
//            creditApplied.visibility = View.VISIBLE
//            creditApplied.text = "$" + modelProfile.wallet
//        }
//

        if (modelCheckOutOne.billingType == "0" || modelCheckOutOne.billingType == "") {
            textView106.visibility = View.VISIBLE
            textView1.visibility = View.GONE
            textView12.visibility = View.GONE
            view1.visibility = View.GONE
            imageEdit.visibility = View.GONE
            textView106.text = primaryAddress
            textView1.text = "Billing address is as same as primary address."
            textView12.text = modelProfile.name
            textView49.text = modelProfile.name
        } else {
            if (primaryAddress == billingAddress) {
                textView106.visibility = View.VISIBLE
                textView49.visibility = View.VISIBLE
                textView12.visibility = View.GONE
                view1.visibility = View.GONE
                imageEdit.visibility = View.GONE
                textView1.visibility = View.GONE
                textView1.text = "Billing address is as same as primary address."
                textView106.text = primaryAddress
                textView49.text = modelProfile.name
            } else if (primaryAddress != billingAddress) {
                textView106.visibility = View.VISIBLE
                textView49.visibility = View.VISIBLE
                imageEdit.visibility = View.VISIBLE
                textView12.visibility = View.GONE
                view1.visibility = View.GONE
                textView1.visibility = View.GONE
                textView106.text = primaryAddress
                textView1.text = billingAddress
                textView12.text = modelProfile.name
                textView49.text = modelProfile.name
            }
        }



        if (modelSubscriptionData.introductory_price == "0" || modelSubscriptionData.introductory_price == "0.00") {
            is_intro = "0"
            introPrice.visibility = View.GONE
            txtIntroPrice.visibility = View.GONE
            textView140.visibility = View.VISIBLE
            subTotal.visibility = View.VISIBLE
            val calculatedDiscount =
                (modelSubscriptionData.discount_price.toDouble() / 100 * modelSubscriptionData.price.toDouble())
            subTotal.text =
                "$" + modelSubscriptionData.price.toDouble().toString().replace(".0", ".00")
            val disPriceFormated = BigDecimal(calculatedDiscount).setScale(2, RoundingMode.HALF_UP)
            if (calculatedDiscount <= 0.0) {
                textView14.visibility = View.GONE
                discountPriceee.visibility = View.GONE
            } else {
                textView14.visibility = View.VISIBLE
                discountPriceee.visibility = View.VISIBLE
                discountPriceee.text = "-$$disPriceFormated"
                textView14.text = "Discount - ${modelSubscriptionData.discount_price.replace(
                    ".00",
                    ""
                ).replace(".0", "")}% off"

            }

            val lastAmount = modelSubscriptionData.price.toDouble() - calculatedDiscount
            subTotalPrice.text = "$$lastAmount"
            val lastAmount1 = lastAmount + taxx.toDouble()
            val amountFormated = BigDecimal(lastAmount1).setScale(2, RoundingMode.HALF_UP)
            totalPricee.text = "$$amountFormated"
            actualPriceParam = modelSubscriptionData.price.toDouble().toString()
            totalPrice = amountFormated.toString()
            if (modelCheckOutOne.credits != "0.00") {
                creditApplied.text = "-$" + modelCheckOutOne.credits
            } else {
                creditApplied.visibility = View.GONE
                credits.visibility = View.GONE
            }
            modelAddSignature.sub_total = lastAmount.toString()

        } else {
            is_intro = "1"
            introPrice.visibility = View.VISIBLE
            txtIntroPrice.visibility = View.VISIBLE
            textView140.visibility = View.GONE
            subTotal.visibility = View.GONE
            val introPrice = BigDecimal(modelSubscriptionData.introductory_price).setScale(
                2,
                RoundingMode.HALF_UP
            ).toString()
            actualPriceParam = modelSubscriptionData.introductory_price
            txtIntroPrice.text = "$$introPrice"
            discountPriceee.text = "$0"
            textView14.visibility = View.GONE
            discountPriceee.visibility = View.GONE
            var amounttttt = 0.00

            amounttttt =
                modelSubscriptionData.introductory_price.toDouble() + taxx.toDouble()

            val amountFormated = BigDecimal(amounttttt).setScale(2, RoundingMode.HALF_UP)
            subTotalPrice.text = "$ $amountFormated"
            totalPricee.text = "$ $amountFormated"
            totalPrice = amountFormated.toString()
            if (modelCheckOutOne.credits != "0.00") {
                creditApplied.text = "-$" + modelCheckOutOne.credits
            } else {
                creditApplied.visibility = View.GONE
                credits.visibility = View.GONE
            }
            modelAddSignature.sub_total = modelSubscriptionData.introductory_price
        }

        if (modelCheckOutOne.credits != "0.00") {
            val total = totalPrice
            totalPrice = "$" + (totalPrice.replace(
                "$",
                ""
            ).toDouble() - modelCheckOutOne.credits.toDouble()).toString()
            modelCheckOutOne.credits = (total.toDouble() - totalPrice.toDouble()).toString()
        }

        modelAddSignature.tax = taxx
        modelAddSignature.subscription_id = modelSubscriptionData.subscription_id
        modelAddSignature.first_name = modelCheckOutOne.firstName
        modelAddSignature.last_name = modelCheckOutOne.firstName
        modelAddSignature.email = modelProfile.email
        modelAddSignature.contact_no = modelProfile.contact_no
        modelAddSignature.street = modelCheckOutOne.streetName
        modelAddSignature.city = modelCheckOutOne.city
        modelAddSignature.state = modelCheckOutOne.stateName
        modelAddSignature.zip = modelCheckOutOne.zipCode
        modelAddSignature.suite = modelCheckOutOne.suite
        modelAddSignature.billingState = modelCheckOutOne.billingState
        modelAddSignature.billingCity = modelCheckOutOne.billingCity
        modelAddSignature.billingStreet = modelCheckOutOne.billingCity
        modelAddSignature.billingZip = modelCheckOutOne.billingCity
        modelAddSignature.billingSuite = modelCheckOutOne.billingCity
        modelAddSignature.is_intro = is_intro
        modelAddSignature.credits = modelCheckOutOne.credits
        modelAddSignature.actual_price = actualPriceParam
        modelAddSignature.total = totalPrice
        modelAddSignature.total_amount = totalPrice
        modelAddSignature.buy_type = "1"
        modelAddSignature.user_id = modelProfile.user_id
        modelAddSignature.freeTrial =
            if (modelSubscriptionData.free_trial == "No free trial" || modelSubscriptionData.free_trial == "0") {
                "0"
            } else {
                "1"
            }

        if (modelCheckOutOne.billingType == "") {
            if (modelAddSignature.street + modelAddSignature.city + modelAddSignature.state + modelAddSignature.zip ==
                modelAddSignature.billingStreet + modelAddSignature.billingCity + modelAddSignature.billingState + modelAddSignature.billingZip
            ) {
                modelAddSignature.billingType = "0"
            } else {
                modelAddSignature.billingType = "1"
            }
        }

    }


    override fun onResume() {
        super.onResume()
        categoriesList = Constant.cardArrayListDataMerchant
        adapterCards.update(Constant.cardArrayListDataMerchant)
    }

    private fun clickListeners() {
        nextCheckout.setOnClickListener {
            var exist = 0
            modelAddSignature.card_id = ""
            for (i in 0 until categoriesList.size) {
                if (categoriesList[i].card) {
                    modelAddSignature.card_id = categoriesList[i].id
                    modelAddSignature.last4 = categoriesList[i].last4
                    modelAddSignature.payment_method_name = categoriesList[i].brand
                    modelAddSignature.payment_type = "2"
                    exist = 1
                    break
                }
            }
            if (exist == 0) {
                Toast.makeText(this, "Please select card.", Toast.LENGTH_SHORT).show()
                return@setOnClickListener

            } else {
                val modelSubscriptionData =
                    intent.getParcelableExtra<ModelSellerSubscriptions>("modelSubscriptionModel")
                startActivity(
                    Intent(
                        this,
                        AddSignatureActivity::class.java
                    ).putExtra("modelAddSignature", modelAddSignature)
                        .putExtra("modelCampaigns", modelSubscriptionData)
                )
            }
        }

        tVAdCard.setOnClickListener {
            startActivity(
                Intent(this, AddCardActivity::class.java).putExtra(
                    "merchantCheckOutt",
                    "merchantCheckOutt"
                ).putExtra("modelProfile", modelProfile)
            )
        }

        imageView61.setOnClickListener {

            startActivityForResult(
                Intent(this, CheckoutMerchantSignUp::class.java).putExtra(
                    "editAddressPrimary",
                    "editAddressPrimary"
                )
                    .putExtra("modelPro", modelProfile), 20
            )
        }

        imageEdit.setOnClickListener {
            startActivityForResult(
                Intent(this, CheckoutMerchantSignUp::class.java).putExtra(
                    "editBillingAddress",
                    "editBillingAddress"
                )
                    .putExtra("modelPro", modelProfile), 21
            )
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == 10) {
            modelAddress = data!!.getParcelableExtra("model")
            val primaryAddress =
                modelAddress.streetAddress + " " + modelAddress.city + " " + modelAddress.state + " " + modelAddress.zipCode
            textView106.text = primaryAddress
            modelAddSignature.street = modelAddress.streetAddress
            modelAddSignature.city = modelAddress.city
            modelAddSignature.state = modelAddress.state
            modelAddSignature.zip = modelAddress.zipCode
            modelAddSignature.suite = modelAddress.suiteNumber
        }

        if (resultCode == 11) {
            modelAddress = data!!.getParcelableExtra("model")
            val billingAddress =
                modelAddress.billingStreet + " " + modelAddress.billingCity + " " + modelAddress.billingState + " " + modelAddress.billingZip
            textView1.text = billingAddress
            modelAddSignature.billingStreet = modelAddress.billingStreet
            modelAddSignature.billingCity = modelAddress.billingCity
            modelAddSignature.billingState = modelAddress.billingState
            modelAddSignature.billingZip = modelAddress.billingZip
            modelAddSignature.billingSuite = modelAddress.billingSuite

        }


    }

    private fun setToolbar() {
        setSupportActionBar(toolbar.toolbar)
        toolbar.title.text = ""
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)

    }

    override fun selectedCard(
        position: Int,
        modell: PaymentCardModel
    ) {
        val model = categoriesList[position]
        if (model.card) {
            model.card = false
            modelAddSignature.lastFour = ""
        } else {
            for (child in categoriesList) {
                child.card = false
            }
            model.card = true
            modelAddSignature.lastFour = model.last4
        }
        categoriesList[position] = model
        adapterCards.notifyDataSetChanged()

    }


    override fun editCard(position: Int, model: PaymentCardModel) {
        startActivity(
            Intent(this, AddCardActivity::class.java).putExtra(
                "updateCard",
                "update"
            ).putExtra("checkoutOne", Constant.checkoutoneData).putExtra(
                "merchantCheckOutt",
                "merchantCheckOutt"
            )

        )

    }

    private fun setColors() {
        nextCheckout.setBackgroundResource(R.drawable.bg_seller_button_blue)
        nextCheckout.text = "Place Order"
        nextCheckout.setTextColor(resources.getColor(R.color.colorWhite))
        tVAdCard.setTextColor(resources.getColor(R.color.blue))
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

}
