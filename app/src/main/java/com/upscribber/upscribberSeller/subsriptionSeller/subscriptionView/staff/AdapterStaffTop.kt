package com.upscribber.upscribberSeller.subsriptionSeller.subscriptionView.staff

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.upscribber.commonClasses.Constant
import com.upscribber.subsciptions.merchantSubscriptionPages.staff.StaffModel
import com.upscribber.R
import com.upscribber.upscribberSeller.manageTeam.ManageTeamModel
import kotlinx.android.synthetic.main.layout_staff_list_top.view.*
import kotlinx.android.synthetic.main.layout_staff_list_top.view.imageView

class AdapterStaffTop(
    private val mContext: Context,
    private val list: ArrayList<StaffModel>,
    private val teamInfo: ArrayList<ManageTeamModel>
) :
    RecyclerView.Adapter<AdapterStaffTop.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val v = LayoutInflater.from(mContext)
            .inflate(
                R.layout.layout_staff_list_top
                , parent, false
            )
        return MyViewHolder(v)
    }

    override fun getItemCount(): Int {
        return teamInfo.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = teamInfo[position]
        val imagePath = Constant.getPrefs(mContext).getString(Constant.dataImagePath, "")
        Glide.with(mContext).load(imagePath + model.profile_image).placeholder(R.mipmap.circular_placeholder)
            .into(holder.itemView.imageView)
        holder.itemView.memberName.text = model.namee

        if(model.speciality == ""){
            holder.itemView.speciality.text = "No speciality added"
        }else{
            holder.itemView.speciality.text = model.speciality
        }

        holder.itemView.setOnClickListener {
            Constant.commonStaffAlert(
                mContext,
                model.profile_image,
                model.namee,
                model.speciality,
                "",
               "",
               ""
            )
        }

    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}