package com.upscribber.upscribberSeller.subsriptionSeller.extras

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData

class ViewModelExtra(application: Application):AndroidViewModel(application) {

    fun getPlanValues(): ArrayList<ModelExtra> {
        return refVar.getValues()
    }

    fun updateArray(model: ModelExtra):  MutableLiveData<ArrayList<ModelExtra>> {
        return refVar.updateValues(model)

    }

    fun edit(model: ModelExtra, i: Int): MutableLiveData<ArrayList<ModelExtra>> {

        return refVar.edit(model,i)
    }

    fun getArray():  MutableLiveData<ArrayList<ModelExtra>> {

        return refVar.getArray()
    }

    var refVar:RepExtra= RepExtra(application)
}