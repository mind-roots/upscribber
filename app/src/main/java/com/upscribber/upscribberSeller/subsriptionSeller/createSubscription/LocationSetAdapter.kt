package com.upscribber.upscribberSeller.subsriptionSeller.createSubscription

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.R
import com.upscribber.databinding.SetLocationLayoutBinding
import com.upscribber.upscribberSeller.subsriptionSeller.location.DaysLogic.DayModel
import com.upscribber.upscribberSeller.subsriptionSeller.location.DaysLogic.ModelDays
import com.upscribber.upscribberSeller.subsriptionSeller.location.DaysLogic.ModelMain
import com.upscribber.upscribberSeller.subsriptionSeller.location.DaysLogic.ResultModel
import com.upscribber.upscribberSeller.subsriptionSeller.location.searchLoc.ModelSearchLocation
import kotlinx.android.synthetic.main.set_location_layout.view.*

class LocationSetAdapter (var context: Context): RecyclerView.Adapter<LocationSetAdapter.MyViewHolder>() {

    private lateinit var mBinding: SetLocationLayoutBinding
    private  var items = ArrayList<ModelSearchLocation>()
    var listener = context as Delete
    var myModel = ArrayList<ModelDays>()
    var daysArray = ArrayList<DayModel>()
    var mainList = ArrayList<ModelMain>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        mBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.set_location_layout, parent, false)
        return MyViewHolder(
            mBinding
        )

    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = items[position]
        holder.bind(model)

        holder.itemView.imageView68.setOnClickListener {
            listener.deleteAddedProduct(position)
        }


        myModel = ArrayList()
        daysArray = ArrayList()
        mainList = ArrayList()
        for(i in 0 until model.store_timing.size) {
            var model22 = ModelDays()
            if (i==0) {
                model22.startTime = model.store_timing[i].open
                model22.closeTime = model.store_timing[i].close
                model22.day = "Sunday"
                myModel.add(model22)
            }
            if (i==1) {
                model22.startTime = model.store_timing[i].open
                model22.closeTime = model.store_timing[i].close
                model22.day = "Monday"
                myModel.add(model22)
            }
            if (i==2) {
                model22 = ModelDays()
                model22.startTime = model.store_timing[i].open
                model22.closeTime = model.store_timing[i].close
                model22.day = "Tuesday"
                myModel.add(model22)
            }
            if (i==3) {
                model22 = ModelDays()
                model22.startTime = model.store_timing[i].open
                model22.closeTime = model.store_timing[i].close
                model22.day = "Wednesday"
                myModel.add(model22)
            }
            if (i==4) {
                model22 = ModelDays()
                model22.startTime = model.store_timing[i].open
                model22.closeTime = model.store_timing[i].close
                model22.day = "Thursday"
                myModel.add(model22)
            }
            if (i==5) {
                model22 = ModelDays()
                model22.startTime = model.store_timing[i].open
                model22.closeTime = model.store_timing[i].close
                model22.day = "Friday"
                myModel.add(model22)
            }
            if (i==6) {
                model22 = ModelDays()
                model22.startTime = model.store_timing[i].open
                model22.closeTime = model.store_timing[i].close
                model22.day = "Saturday"
                myModel.add(model22)
            }



        }
        val array2 = arrayListOf<DayModel>()
        for (i in 0 until myModel.size) {


            var model = DayModel()
            if (i ==0) {
                model.day = "Sun"
            }
            else if (i == 1) {
                model.day = "Mon"
            } else if (i == 2) {
                model.day = "Tue"
            } else if (i == 3) {
                model.day = "Wed"
            } else if (i == 4) {
                model.day = "Thu"
            } else if (i == 5) {
                model.day = "Fri"
            } else if (i == 6) {
                model.day = "Sat"
            }

            val startTime: String = myModel[i].startTime
            val endTime: String = myModel[i].closeTime
            if (startTime == endTime) {
//            if (startTime.isEmpty() &&endTime.isEmpty()) {
                model.time = "closed"
            } else {
                model.time = "$startTime to $endTime"
            }
            array2.add(model)
        }

        for (modelDay in array2) {
            if (!checkIfAlreadyExist(modelDay.day)) {
                val subList = getDayTimeString(modelDay, array2)
                mainList.add(subList)
            }
        }
        val existdays = ArrayList<ResultModel>()
        for (index in mainList.indices) {
            val resultString = mainList[index].resultString
            Log.i("$index : ", resultString.day)
            existdays.add(resultString)

        }
        var daysList=""
        for(i in 0 until existdays.size){
            if (existdays[i].time!="closed") {
                if (daysList.isEmpty()) {
                    daysList = "" + existdays[i].day + " (" + existdays[i].time + ")"
                } else {
                    daysList = daysList + ", " + existdays[i].day + " (" + existdays[i].time + ")"
                }
            }
        }
        holder.itemView.days.text = daysList
    }

    fun checkIfAlreadyExist(day: String): Boolean {
        if (mainList.isNotEmpty()) {
            for (value in mainList) {
                val list = value.list
                for (data in list) {
                    if (data.day == day) {
                        return true
                    }
                }
            }
        }
        return false
    }

    private fun getDayTimeString(timeToFind: DayModel, list: ArrayList<DayModel>): ModelMain {
        val modelMain = ModelMain()
        val subList = ArrayList<DayModel>()
        var timeToReturn = ""
        var lastIndex = -1
        for (i in list.indices) {
            val model = list[i]
            if (timeToFind.time == model.time) {

                subList.add(model)

                if (lastIndex != -1 && i - lastIndex == 1) {

                    if (timeToReturn.contains("-") && !timeToReturn.contains(",")) {
                        timeToReturn = "${timeToFind.day}-${model.day}"
                    } else {
                        timeToReturn = "$timeToReturn-${model.day}"
                    }

                } else {
                    if (timeToReturn.isEmpty()) {
                        timeToReturn = model.day
                    } else {
                        timeToReturn = "$timeToReturn, ${model.day}"
                    }

                }

                lastIndex = i
            }
        }

        modelMain.list = subList
        modelMain.resultString.day = timeToReturn
        modelMain.resultString.time = timeToFind.time

        return modelMain
    }


    class MyViewHolder(var  binding: SetLocationLayoutBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(obj: ModelSearchLocation) {
            binding.model = obj
            binding.executePendingBindings()
        }
    }

    fun update(items: ArrayList<ModelSearchLocation>) {
        this.items = items
        notifyDataSetChanged()
    }

    interface Delete{
        fun deleteAddedProduct(position: Int)

    }

}