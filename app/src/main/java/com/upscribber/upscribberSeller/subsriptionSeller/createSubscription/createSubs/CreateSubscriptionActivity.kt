package com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.createSubs

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.Outline
import android.os.Build
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.databinding.ActivityCreateSubscriptionBinding
import com.upscribber.databinding.AddBottomBinding
import com.upscribber.databinding.CreateConfirmBinding
import com.upscribber.commonClasses.RoundedBottomSheetDialogFragment
import com.upscribber.upscribberSeller.store.storeMain.StoreModel
import com.upscribber.upscribberSeller.subsriptionSeller.addFeatures.AdapterAddFeatures
import com.upscribber.upscribberSeller.subsriptionSeller.addFeatures.AddFeaturesViewModel
import com.upscribber.upscribberSeller.subsriptionSeller.addFeatures.ModelAddFeatures
import com.upscribber.upscribberSeller.subsriptionSeller.subscriptionMain.ModelMain
import com.upscribber.upscribberSeller.subsriptionSeller.subscriptionMain.SubscriptionSellerViewModel
import com.upscribber.upscribberSeller.subsriptionSeller.subscriptionView.CampaignCompleteActivity
import com.upscribber.upscribberSeller.subsriptionSeller.subscriptionView.subsManage.SuccessfullyDeleteActivity


@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class CreateSubscriptionActivity : AppCompatActivity(), AdapterAddFeatures.ChangeImages {

    private var array: ArrayList<ModelAddFeatures> = ArrayList()
    private lateinit var mAdapter: AdapterAddFeatures
    lateinit var mBinding: ActivityCreateSubscriptionBinding
    lateinit var mViewModel: AddFeaturesViewModel
    lateinit var mvModel: CreateSubscriptionViewModel
    var createButtonEnabled = 0
    lateinit var progressBar25: ConstraintLayout


    companion object {
        lateinit var viewModel: SubscriptionSellerViewModel
        var editData = ModelMain()
    }


    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_create_subscription)
        mViewModel = ViewModelProviders.of(this)[AddFeaturesViewModel::class.java]
        viewModel = ViewModelProviders.of(this).get(SubscriptionSellerViewModel::class.java)
        mvModel = ViewModelProviders.of(this).get(CreateSubscriptionViewModel::class.java)
        progressBar25 = findViewById(R.id.progressBar25)
        observerInit()
        mBinding.lifecycleOwner = this
        when {
            intent.hasExtra("edit") -> {
                mBinding.textView236.text = "Edit subscription"
                mBinding.duplicate.visibility = View.VISIBLE
                mBinding.delete.visibility = View.GONE
                mBinding.textUpdation.visibility = View.GONE
                mBinding.create.text = "Save"

            }
            intent.hasExtra("editReactive") -> {
                mBinding.textView236.text = "Delete subscription"
                mBinding.textUpdation.visibility = View.GONE
                mBinding.duplicate.visibility = View.GONE
                mBinding.delete.visibility = View.VISIBLE
                mBinding.create.text = "Save"
            }
            else -> {
                mBinding.textView236.text = "Create subscription"
                mBinding.duplicate.visibility = View.GONE
                mBinding.textUpdation.visibility = View.VISIBLE
                mBinding.delete.visibility = View.GONE
                mBinding.textUpdation.text = "3 left to complete"
            }
        }

        mBinding.create.setOnClickListener {
            if (createButtonEnabled == 1) {
                mvModel.createSubscription()
                if (intent.hasExtra("product")) {
                    startActivity(
                        Intent(
                            this,
                            CampaignCompleteActivity::class.java
                        ).putExtra("product", "product")
                    )
                } else {
                    startActivity(Intent(this, CampaignCompleteActivity::class.java))
                }

            }
        }
        if (intent.hasExtra("campaignId")) {
            createButtonEnabled = 0
            val campaignId = intent.getStringExtra("campaignId")
            viewModel.getCampaign(campaignId)
            progressBar25.visibility = View.VISIBLE
            viewModel.getmCampaignData().observe(this, Observer {
                editData = it
                progressBar25.visibility = View.GONE
                val mPrefs = application.getSharedPreferences("data", MODE_PRIVATE)
                val prefsEditor = mPrefs.edit()
                prefsEditor.putString("campaign_id", it.campaign_id)
                prefsEditor.putString("bought", it.bought)
                prefsEditor.apply()

                if (editData.steps != "[]") {

                    var steps = editData.steps
                    val steps1 = steps.replace("[", "")
                    var steps2 = steps1.replace("]", "")
                    steps2 = steps2.replace(",4", "")
                    steps = steps2
                    val dd = steps.split(",")
                    for (i in dd.indices) {
                        mBinding.textUpdation.text = "" + (3 - dd.size) + " left to complete"
                        mBinding.create.setBackgroundResource(R.drawable.bg_seller_grey)
                        when {
                            dd[i] == "1" -> mAdapter.setTick(dd[i].toInt() - 1, steps)
                            dd[i] == "2" -> mAdapter.setTick(dd[i].toInt() - 1, steps)
                            else -> mAdapter.setTick(dd[i].toInt() - 1, steps)
                        }
                    }

                    if (dd.size == 3) {
                        mBinding.textUpdation.text = "Completed! Please tap Create"
                        mBinding.create.setBackgroundResource(R.drawable.bg_seller_button_blue)
                        createButtonEnabled = 1
                    }
                }

                val detailResModel = DetailResModel()
                detailResModel.location = editData.locationInfo
                detailResModel.tags = editData.tagInfo
                detailResModel.teamMember = editData.teamInfo
                detailResModel.shareable = editData.share_setting
                detailResModel.check = editData.is_public
                detailResModel.subInfo = editData.description
                detailResModel.subLimit = editData.limit
                detailResModel.campaignId = editData.campaign_id
                detailResModel.steps = editData.steps
                detailResModel.subscriber = editData.subs
                detailResModel.parentTags = editData.parentTagInfo


                val mVariable = Gson()
                val json = mVariable.toJson(detailResModel)
                prefsEditor.putString("detailsArray", json)
                prefsEditor.apply()

                val basicModel = BasicModel()
                basicModel.productInfo = editData.productInfo
                basicModel.subscriptionArray = editData.subscription
                basicModel.campaign_start = editData.campaign_start
                basicModel.campaign_end = editData.campaign_end
                basicModel.feature_image = editData.feature_image
                basicModel.logo = editData.logo
                basicModel.name = editData.name
                basicModel.image = editData.image
                basicModel.steps = editData.steps


                val json11 = mVariable.toJson(basicModel)
                prefsEditor.putString("basicData", json11)
                prefsEditor.apply()


                val dataStore = editData.subscription
                val editor1 = Constant.getSharedPrefs(this).edit()
                val json1 = mVariable.toJson(dataStore)
                editor1.putString("pricing", json1)
                editor1.apply()

            })
        }


        setToolbar()
        setAdapter()
        clickListeners()

    }

    private fun observerInit() {
        viewModel.getmDataDeleteCampaign().observe(this, Observer {
            startActivity(
                Intent(
                    this,
                    SuccessfullyDeleteActivity::class.java
                )
            )
        })

    }

    private fun clickListeners() {
        mBinding.duplicate.setOnClickListener {
            val fragment = MenuFragment()
            fragment.show(supportFragmentManager, fragment.tag)
        }

        mBinding.delete.setOnClickListener {
            val fragment = MenuFragmentDelete()
            fragment.show(supportFragmentManager, fragment.tag)
        }

    }

    class MenuFragmentDelete : RoundedBottomSheetDialogFragment() {

        lateinit var mBinding: CreateConfirmBinding


        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            mBinding = DataBindingUtil.inflate(inflater, R.layout.create_confirm, container, false)

            return mBinding.root
        }

        override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
            super.onViewCreated(view, savedInstanceState)
            roundedImage()
            mBinding.yes.setOnClickListener {
                viewModel.deleteCampaign("1", editData.campaign_id, "")
//                startActivity(
//                    Intent(
//                        context!!,
//                        ConfirmDeleteSubsActivity::class.java
//                    ).putExtra("model", editData)
//                )
                dismiss()
            }


            mBinding.no.setOnClickListener {
                dismiss()
            }
        }


        private fun roundedImage() {
            val curveRadius = 50F

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                mBinding.imageViewTop.outlineProvider = object : ViewOutlineProvider() {

                    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                    override fun getOutline(view: View?, outline: Outline?) {
                        outline?.setRoundRect(
                            0,
                            0,
                            view!!.width,
                            view.height + curveRadius.toInt(),
                            curveRadius
                        )
                    }
                }
                mBinding.imageViewTop.clipToOutline = true
            }

        }

    }

    class MenuFragment : RoundedBottomSheetDialogFragment() {

        private lateinit var mBindingBottom: AddBottomBinding

        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            mBindingBottom =
                DataBindingUtil.inflate(inflater, R.layout.add_bottom, container, false)

            return mBindingBottom.root
        }

        override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
            super.onViewCreated(view, savedInstanceState)
            mBindingBottom.invite.setOnClickListener {
                viewModel.cloneCampaign(editData.campaign_id)
//                startActivity(Intent(context!!, CreateSubscriptionActivity::class.java))
                dismiss()
//                activity!!.finish()
            }
            mBindingBottom.delete.setOnClickListener {
                dismiss()
            }
            roundedImage()


        }

        private fun roundedImage() {
            val curveRadius = 50F

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                mBindingBottom.imageViewTop.outlineProvider = object : ViewOutlineProvider() {

                    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                    override fun getOutline(view: View?, outline: Outline?) {
                        outline?.setRoundRect(
                            0,
                            0,
                            view!!.width,
                            view.height + curveRadius.toInt(),
                            curveRadius
                        )
                    }
                }
                mBindingBottom.imageViewTop.clipToOutline = true
            }
        }


    }

    private fun setToolbar() {
        setSupportActionBar(mBinding.toolbarCreate)
        title = ""
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white)

    }

    private fun setAdapter() {
        mBinding.recyclerAdd.layoutManager = LinearLayoutManager(this)
        mBinding.recyclerAdd.isNestedScrollingEnabled = false
        mAdapter = AdapterAddFeatures(this, 0)
        mBinding.recyclerAdd.adapter = mAdapter

        mViewModel.getCreateList().observe(this, Observer {
            array = it
            mAdapter.update(it)
        })

    }

    override fun onResume() {
        super.onResume()
        val sharedPreferences1 = getSharedPreferences("done", Context.MODE_PRIVATE)
        if (sharedPreferences1.getString("done", "").isNotEmpty()) {
            val editor = sharedPreferences1.edit()
            if (sharedPreferences1.getString("done", "") == "back") {
                finish()
            } else {
                editor.remove("done")
                editor.apply()
                finish()
            }

        }

//        val sharedPreferences2 = getSharedPreferences("customers", Context.MODE_PRIVATE)
//        if (sharedPreferences2.getString("customers", "").isNotEmpty()) {
//            finish()
//        }


    }

    override fun callActivity(position: Int) {
        if (position == 0) {
            if (editData.campaign_id != "") {
                val basicModel = BasicModel()
                basicModel.productInfo = editData.productInfo
                basicModel.campaign_start = editData.campaign_start
                basicModel.campaign_end = editData.campaign_end
                basicModel.name = editData.name
                basicModel.image = editData.image
                basicModel.steps = editData.steps
                startActivityForResult(
                    Intent(this, CreateSubsriptionBasicActivity::class.java).putExtra("position", 0)
                        .putExtra("data", editData).putExtra("showDialog", "show"), 99
                )
            } else {
                if (intent.hasExtra("productsData")) {
                    val productData = intent.getParcelableExtra<StoreModel>("productsData")
                    startActivityForResult(
                        Intent(
                            this,
                            CreateSubsriptionBasicActivity::class.java
                        ).putExtra("position", 0).putExtra("products", productData), 99
                    )

                } else {
                    startActivityForResult(
                        Intent(
                            this,
                            CreateSubsriptionBasicActivity::class.java
                        ).putExtra("position", 0), 99
                    )
                }


            }
        } else if (position == 1) {

            if (editData.campaign_id != "") {
                val editor = Constant.getSharedPrefs(this).edit()
                editor.putBoolean("empty1", false)
                editor.apply()
                startActivityForResult(
                    Intent(this, CreateSubscriptionExtraActivity::class.java).putExtra(
                        "position",
                        1
                    ).putExtra("showDialog", "show"),
                    99
                )

            } else {

                val editor = Constant.getSharedPrefs(this).edit()
                editor.putBoolean("empty1", false)
                editor.apply()
                startActivityForResult(
                    Intent(this, CreateSubscriptionExtraActivity::class.java).putExtra(
                        "position",
                        1
                    ),
                    99
                )
            }
        } else {

            if (editData.campaign_id != "") {

                val detailResModel = DetailResModel()
                detailResModel.location = editData.locationInfo
                detailResModel.tags = editData.tagInfo
                detailResModel.teamMember = editData.teamInfo
                detailResModel.shareable = editData.share_setting
                detailResModel.check = editData.is_public
                detailResModel.subInfo = editData.description
                detailResModel.subLimit = editData.limit
                detailResModel.campaignId = editData.campaign_id
                detailResModel.steps = editData.steps
                detailResModel.parentTags = editData.parentTagInfo
                startActivityForResult(
                    Intent(this, CreateSubscriptionDetailsActivity::class.java).putExtra(
                        "position",
                        2
                    ).putExtra("showDialog", "show"),
                    99
                )
            } else {

                startActivityForResult(
                    Intent(this, CreateSubscriptionDetailsActivity::class.java).putExtra(
                        "position",
                        2
                    ),
                    99
                )

            }
        }
        mAdapter.selection(position)


    }

    @SuppressLint("SetTextI18n")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == 99) {

            createButtonEnabled = 0
            try {
                var steps = data!!.getStringExtra("steps")
                val steps1 = steps.replace("[", "")
                val steps2 = steps1.replace("]", "")
                steps = steps2
                val newStep = steps.replace(",4", "")
                val splitPos = newStep.split(",")
                if (steps.isEmpty()) {
                    for (i in 0 until 3) {
                        if (i < 3) {
                            mAdapter.setTick(i, newStep)
                        }
                    }
                } else {
                    for (element in splitPos) {
                        mAdapter.setTick(element.toInt() - 1, newStep)
                    }
                }


                if (steps.contentEquals("1") ||
                    steps.contentEquals("2") ||
                    steps.contentEquals("3")
                ) {
                    mBinding.textUpdation.text = "2 left to complete"
                    mBinding.create.setBackgroundResource(R.drawable.bg_seller_grey)
                }
                if ((steps.contentEquals("1,2")) || steps.contentEquals("2,3") ||
                    (steps.contentEquals("1,3"))
                ) {
                    mBinding.textUpdation.text = "1 left to complete"
                    mBinding.create.setBackgroundResource(R.drawable.bg_seller_grey)
                }
                if (steps.contentEquals("1,2,3")) {
                    mBinding.textUpdation.text = "Completed! Please tap Create"
                    mBinding.create.setBackgroundResource(R.drawable.bg_seller_button_blue)
                    createButtonEnabled = 1
                }
                if (steps.isEmpty()) {

//                    mBinding.textUpdation.text = "Completed! Please tap Create"
                    mBinding.create.setBackgroundResource(R.drawable.bg_seller_grey)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }


        }

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

}
