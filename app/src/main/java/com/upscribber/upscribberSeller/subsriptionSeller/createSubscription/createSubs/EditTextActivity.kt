package com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.createSubs

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.MenuItem
import android.view.MotionEvent
import androidx.databinding.DataBindingUtil
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.databinding.ActivityEditTextBinding

class EditTextActivity : AppCompatActivity() {

    lateinit var binding: ActivityEditTextBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_edit_text)
        setToolbar()
        clickListeners()
        if(intent.hasExtra("preFilled")){
            val preFilled = intent.getStringExtra("preFilled")
            binding.editText.setText(preFilled)
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun clickListeners() {
        binding.editText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                val result = "${s!!.length}/500"
                binding.textView43.text = result

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })

        binding.imageView31.setOnClickListener {
            Constant.CommonIAlert(
                this,
                "Subscription description",
                "Add detail information about your subscription to inform customers what they will get when they purchase your subscription."
            )
        }


        binding.doneText.setOnClickListener {
            val intent = Intent()
            intent.putExtra("text", binding.editText.text.toString())
            setResult(Activity.RESULT_OK, intent)
            finish()

//            val sharedPreferences = Constant.getSharedPrefs(this)
//            val editor = sharedPreferences.edit()
//            editor.putString("newText", "old")
//            editor.apply()
        }

        binding.editText.setOnTouchListener { view, event ->
            view.parent.requestDisallowInterceptTouchEvent(true)
            if ((event.action and MotionEvent.ACTION_MASK) == MotionEvent.ACTION_UP) {
                view.parent.requestDisallowInterceptTouchEvent(false)
            }
            return@setOnTouchListener false
        }

    }

    private fun setToolbar() {
        setSupportActionBar(binding.toolbar)
        title = ""
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)
        binding.title.text = "Description"
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}
