package com.upscribber.upscribberSeller.subsriptionSeller.subscriptionView

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.R
import com.upscribber.subsciptions.merchantSubscriptionPages.plan.PlanModel
import com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.subscription.ModelSubscriptionCard
import kotlinx.android.synthetic.main.subscription_item_plan.view.*
import org.checkerframework.checker.signedness.qual.Constant
import java.math.BigDecimal
import java.math.RoundingMode

class AdapterSubscriptionPlan
    (
    private val mContext: Context,
    private val list: ArrayList<ModelSubscriptionCard>
) :
    RecyclerView.Adapter<AdapterSubscriptionPlan.MyViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val v = LayoutInflater.from(mContext)
            .inflate(
                R.layout.subscription_item_plan
                , parent, false
            )
        return MyViewHolder(v)
    }

    override fun getItemCount(): Int {

        return list.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = list[position]

        var frequency = ""
        frequency = when (model.frequencyType) {
            "Monthly" -> {
                "Month"
            }
            "Yearly" -> {
                "Year"
            }
            "1" -> {
                "Month"
            }
            "2" -> {
                "Year"
            }
            else -> {
                "Week"
            }
        }
        holder.itemView.annual.text = frequency + " Subscription"
        when (model.redeemtion_cycle_qty) {
            "500000" -> {
                holder.itemView.upto.text = "Unlimited " + model.unit + " per " + frequency
            }
            "1" -> {
                holder.itemView.upto.text = "1 " + model.unit + " Build " + frequency
            }
            else -> {
                holder.itemView.upto.text =
                    model.redeemtion_cycle_qty + "X " + model.unit + " per " + frequency
            }
        }

        val discountedPrice = (model.price.toDouble() - (model.discountPrice.toDouble() / 100 * model.price.toDouble())).toString()
        val splitPos = discountedPrice.split(".")
        if (splitPos[1] == "00" || splitPos[1] == "0") {
            holder.itemView.tv_cost.text = "$" + splitPos[0]
        } else {
            holder.itemView.tv_cost.text = "$" + BigDecimal(discountedPrice).setScale(2, RoundingMode.HALF_UP)
        }

        if (model.discountPrice == "0" || model.discountPrice == "0.0" || model.discountPrice == "0.00"){
            holder.itemView.tv_off.visibility = View.INVISIBLE
        }else{
            holder.itemView.tv_off.visibility = View.VISIBLE
            if (model.discountPrice.contains(".")) {
                val splitPerc = model.discountPrice.split(".")
                if (splitPerc[1] == "00" || splitPerc[1] == "0") {
                    holder.itemView.tv_off.text = splitPerc[0] + "% off"
                } else {
                    holder.itemView.tv_off.text =
                        "" + BigDecimal(model.discountPrice).setScale(2, RoundingMode.HALF_UP) + "%"
                }
            }else{
                holder.itemView.tv_off.text = model.discountPrice + "% off"
            }

        }

        holder.itemView.tv_last_text.text = "$" + BigDecimal(model.price).setScale(2, RoundingMode.HALF_UP)
        holder.itemView.tv_last_text.paintFlags = holder.itemView.tv_last_text.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG


        holder.itemView.cl_plan.setBackgroundResource(R.drawable.plan_grey_bg)
        holder.itemView.annual.setTextColor(Color.parseColor("#8f909e"))
        holder.itemView.upto.setTextColor(Color.parseColor("#8f909e"))
        holder.itemView.tv_off.setTextColor(Color.parseColor("#aab5bd"))
        holder.itemView.tv_off.setBackgroundResource(R.drawable.bg_off_non_selected)
        holder.itemView.tv_last_text.setTextColor(Color.parseColor("#aab5bd"))
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)


}
