package com.upscribber.upscribberSeller.subsriptionSeller.subscriptionView.staff

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.upscribberSeller.subsriptionSeller.subscriptionView.ActivitySubscriptionSeller
import com.upscribber.databinding.AddStaffRecyclerLayoutBinding
import kotlinx.android.synthetic.main.add_staff_recycler_layout.view.*

class AddStaffAdapter(var context: Context,listen : ActivitySubscriptionSeller.MenuFragmentStaff) : RecyclerView.Adapter<AddStaffAdapter.MyViewHolder>() {

    private lateinit var binding: AddStaffRecyclerLayoutBinding
    private  var items = ArrayList<AddStaffModel>()
    var listener = listen as SelectionStaffMember

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        binding = AddStaffRecyclerLayoutBinding.inflate(LayoutInflater.from(context), parent,false)
        return MyViewHolder(binding)

    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = items[position]
        holder.bind(model)

        holder.itemView.setOnClickListener {
            listener.SelectMember(position)
        }

        if (model.status){
            holder.itemView.blueTick.visibility = View.VISIBLE
        }else{
            holder.itemView.blueTick.visibility = View.GONE
        }

    }

    class MyViewHolder(var  binding: AddStaffRecyclerLayoutBinding): RecyclerView.ViewHolder(binding.root) {

        fun bind(obj: AddStaffModel) {
            binding.model2 = obj
            binding.executePendingBindings()
        }
    }

    fun update(items: ArrayList<AddStaffModel>) {
        this.items = items
        notifyDataSetChanged()
    }

    interface SelectionStaffMember{
        fun SelectMember(position : Int)
    }
}