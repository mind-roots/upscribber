package com.upscribber.upscribberSeller.subsriptionSeller.subscriptionView

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.upscribber.subsciptions.merchantSubscriptionPages.deal.DealModel
import com.upscribber.subsciptions.merchantSubscriptionPages.plan.PlanModel
import com.upscribber.subsciptions.merchantSubscriptionPages.reviews.ReviewDescriptionModel
import com.upscribber.subsciptions.merchantSubscriptionPages.reviews.ReviewModel
import com.upscribber.subsciptions.merchantSubscriptionPages.staff.StaffFaqModel
import com.upscribber.subsciptions.merchantSubscriptionPages.staff.StaffModel
import com.upscribber.upscribberSeller.dashboardfragment.reporting.ReportingModel
import com.upscribber.R

class RepoActivitySubscriptionSeller(var application: Application) {

    var mData = MutableLiveData<ArrayList<PlanModel>>()
    private var mDataOption = MutableLiveData<ArrayList<ModelOption>>()
    var mDataDeals = MutableLiveData<ArrayList<DealModel>>()
    var mDataStaffTop = MutableLiveData<ArrayList<StaffModel>>()
    var mDataStaffFaq = MutableLiveData<ArrayList<StaffFaqModel>>()
    var mDataReporting = MutableLiveData<ReportingModel>()
    var mReviews = MutableLiveData<ArrayList<ReviewModel>>()
    var mReviewsDesc = MutableLiveData<ArrayList<ReviewDescriptionModel>>()

    fun getPlanData(): MutableLiveData<ArrayList<PlanModel>> {
        val array = planList()
        mData.value = array
        return mData
    }

    private fun planList(): ArrayList<PlanModel> {

        val arrayList: ArrayList<PlanModel> = ArrayList()
//
//        var planModel = PlanModel()
//        planModel.imageRight = R.drawable.ic_check_plan
//        planModel.annual = "Annual Subscription"
//        planModel.upTo = "Up to 10 Manicure Yearly"
//        planModel.cost = "$21"
//        planModel.percentOff = "41% Off"
//        planModel.fiftien = "$15"
//        planModel.isSelected = false
//        arrayList.add(planModel)
//
//        planModel = PlanModel()
//        planModel.imageRight = R.drawable.ic_check_plan
//        planModel.annual = "Annual Subscription"
//        planModel.upTo = "Up to 10 Manicure Yearly"
//        planModel.cost = "$21"
//        planModel.percentOff = "41% Off"
//        planModel.fiftien = "$15"
//        planModel.isSelected = false
//
//        arrayList.add(planModel)
//
//        planModel = PlanModel()
//        planModel.imageRight = R.drawable.ic_check_plan
//        planModel.annual = "Annual Subscription"
//        planModel.upTo = "Up to 10 Manicure Yearly"
//        planModel.cost = "$21"
//        planModel.percentOff = "41% Off"
//        planModel.fiftien = "$15"
//        planModel.isSelected = false
//
//        arrayList.add(planModel)
//        mData.value = arrayList
        return arrayList

    }

    fun getFreeList(): MutableLiveData<ArrayList<ModelOption>> {

        val array = getFreeData()
        mDataOption.value = array
        return mDataOption
    }

    private fun getFreeData(): ArrayList<ModelOption> {

        val arrayList: ArrayList<ModelOption> = ArrayList()

        var modelOption = ModelOption()
        modelOption.name = "Free Trial"
        modelOption.value = "7 Days"
        arrayList.add(modelOption)

        modelOption = ModelOption()
        modelOption.name = "Introductory Price"
        modelOption.value = "$32 for 30 Days"
        arrayList.add(modelOption)

        modelOption = ModelOption()
        modelOption.name = "Subscriber Limit"
        modelOption.value = "50"
        arrayList.add(modelOption)
        mDataOption.value = arrayList
        return arrayList
    }



    fun dealsData(): MutableLiveData<ArrayList<DealModel>> {
        val array = dealList()
        mDataDeals.value = array
        return mDataDeals

    }

    private fun dealList(): ArrayList<DealModel> {
        val arrayList: ArrayList<DealModel> = ArrayList()

        var dealModel = DealModel()
        dealModel.bullet = R.drawable.bullet_bg
        dealModel.includeText = "5 manicures"
        arrayList.add(dealModel)

        dealModel = DealModel()
        dealModel.bullet = R.drawable.bullet_bg
        dealModel.includeText = "5 manicures"
        arrayList.add(dealModel)


        dealModel = DealModel()
        dealModel.bullet = R.drawable.bullet_bg
        dealModel.includeText = "Free Coffee"
        arrayList.add(dealModel)
        arrayList.add(dealModel)



        dealModel = DealModel()
        dealModel.bullet = R.drawable.bullet_bg
        dealModel.includeText = "Food Massage"
        arrayList.add(dealModel)


        dealModel = DealModel()
        dealModel.bullet = R.drawable.bullet_bg
        dealModel.includeText = "5 manicures"
        arrayList.add(dealModel)

        mDataDeals.value = arrayList
        return arrayList

    }

    fun staffTop(): MutableLiveData<ArrayList<StaffModel>> {
        val array = getStaffList()
        mDataStaffTop.value = array
        return mDataStaffTop

    }

    private fun getStaffList(): ArrayList<StaffModel> {

        val arrayList: ArrayList<StaffModel> = ArrayList()

        var modelStaffList = StaffModel()
        modelStaffList.imageStaff = R.mipmap.profile_image
        modelStaffList.speciality = application.getString(R.string.lorum)
        modelStaffList.nameStaffMember = "Robert Upney"
        arrayList.add(modelStaffList)

        modelStaffList = StaffModel()
        modelStaffList.imageStaff = R.mipmap.profile_image
        modelStaffList.speciality = application.getString(R.string.lorum)
        modelStaffList.nameStaffMember = "Mila Kunis"
        arrayList.add(modelStaffList)

        modelStaffList = StaffModel()
        modelStaffList.imageStaff = R.mipmap.profile_image
        modelStaffList.speciality = application.getString(R.string.lorum)
        modelStaffList.nameStaffMember = "George o Vama"
        arrayList.add(modelStaffList)

        modelStaffList = StaffModel()
        modelStaffList.imageStaff = R.mipmap.profile_image
        modelStaffList.speciality = application.getString(R.string.lorum)
        modelStaffList.nameStaffMember = "Robert"
        arrayList.add(modelStaffList)
        mDataStaffTop.value = arrayList
        return arrayList
    }

    fun staffFaq(): MutableLiveData<ArrayList<StaffFaqModel>> {
        val array = staffFaqList()
        mDataStaffFaq.value = array
        return mDataStaffFaq

    }

    private fun staffFaqList(): ArrayList<StaffFaqModel> {

        val arrayList: ArrayList<StaffFaqModel> = ArrayList()

        var modelStaffList = StaffFaqModel()
        modelStaffList.textQuestion = "Can I have both personal and business profile on Upscribbr?"
        modelStaffList.textAnswer =
            "Yes! In fact, you will first register as a customer and then create your business profile. You will then be able to easily switch between you personal profile and your business dashboard."
        arrayList.add(modelStaffList)

        modelStaffList = StaffFaqModel()
        modelStaffList.textQuestion = "How do I know if subscriptions are right for me?"
        modelStaffList.textAnswer =
            "Just about any brick-and-mortar business that offers a product or service can offer subscriptions! From coffee shops and breweries, to gyms and yoga studios, to salons and spas, to auto repair shops - business owners use subscriptions to add a predictable income stream to their business. If you are not sure whether subscriptions will work for you, you can use our Subscription Revenue Calculator to get an estimate of how much \$ you may be able to make with subscriptions."
        arrayList.add(modelStaffList)

        modelStaffList = StaffFaqModel()
        modelStaffList.textQuestion = "How many subscriptions can I have?"
        modelStaffList.textAnswer =
           "You can have as many subscriptions as you want! If it makes sense for your business, you can have dozens or even hundreds of subscriptions. Just make sure you don’t overwhelm your customers. Keep it simple! Most relevant FAQs"
        arrayList.add(modelStaffList)

        modelStaffList = StaffFaqModel()
        modelStaffList.textQuestion = "Can I have subscriptions that are specific to certain locations?"
        modelStaffList.textAnswer =
            "Yes! When creating a subscription, you will be able to select which location (s) will offer the subscription. You can add just one or all of your locations."
        arrayList.add(modelStaffList)
        mDataStaffFaq.value = arrayList
        return arrayList
    }

    fun getGraphData(): MutableLiveData<ReportingModel> {
        val model = setGraphData()
        mDataReporting.value = model
        return mDataReporting

    }

    private fun setGraphData(): ReportingModel {
        val reportingModel = ReportingModel()
        reportingModel.graphPlotPricex = "7"
        reportingModel.graphPlotPricey = "100"
        reportingModel.totalSalePrice = "$18,200"
        mDataReporting.value = reportingModel
        return reportingModel
    }

    fun getReviewsData(): MutableLiveData<ArrayList<ReviewModel>> {
        val array = getReviewsDetailsList()
        mReviews.value = array
        return mReviews

    }

    private fun getReviewsDetailsList(): ArrayList<ReviewModel> {
        val arrayList: ArrayList<ReviewModel> = ArrayList()
        var modelReviews = ReviewModel()
        modelReviews.imageCategory = "http://cloudart.com.au/projects/upscribbr/assets/img/m1.png"
        modelReviews.numberOfReviews = "50"
        modelReviews.reviewCategory = "Great Seller"
        arrayList.add(modelReviews)

        modelReviews = ReviewModel()
        modelReviews.imageCategory = "http://cloudart.com.au/projects/upscribbr/assets/img/m2.png"
        modelReviews.numberOfReviews = "150"
        modelReviews.reviewCategory = "Great Discount"
        arrayList.add(modelReviews)

        modelReviews = ReviewModel()
        modelReviews.imageCategory = "http://cloudart.com.au/projects/upscribbr/assets/img/m3.png"
        modelReviews.numberOfReviews = "350"
        modelReviews.reviewCategory = "Awesome"
        arrayList.add(modelReviews)

        modelReviews = ReviewModel()
        modelReviews.imageCategory = "http://cloudart.com.au/projects/upscribbr/assets/img/m1.png"
        modelReviews.numberOfReviews = "90"
        modelReviews.reviewCategory = "Super Seller"
        arrayList.add(modelReviews)

        modelReviews = ReviewModel()
        modelReviews.imageCategory = "http://cloudart.com.au/projects/upscribbr/assets/img/m2.png"
        modelReviews.numberOfReviews = "500"
        modelReviews.reviewCategory = "Amazing"
        arrayList.add(modelReviews)

        return arrayList
    }

    fun getReviewDescData(): MutableLiveData<ArrayList<ReviewDescriptionModel>> {
        val array = getReviewDescData1()
        mReviewsDesc.value = array
        return mReviewsDesc

    }

    private fun getReviewDescData1(): ArrayList<ReviewDescriptionModel> {
        val arrayList: ArrayList<ReviewDescriptionModel> = ArrayList()

        var modelReviewDescription = ReviewDescriptionModel()

        modelReviewDescription.memberImage = R.mipmap.profile_image
        modelReviewDescription.gestureImage = R.drawable.ic_like
        modelReviewDescription.dateTime = "11/10/2018,08:40 pm"
        modelReviewDescription.review = "Lorem ipsum dolor sit amet, consectetur adipiscing elit.Etiam eget ligula eu lectus lobortis.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam eget ligula eu lectus lobortis condimentum"
        modelReviewDescription.userName = "Robert"
        arrayList.add(modelReviewDescription)

        modelReviewDescription = ReviewDescriptionModel()
        modelReviewDescription.memberImage = R.mipmap.profile_image
        modelReviewDescription.gestureImage = R.drawable.ic_dislike
        modelReviewDescription.dateTime = "11/10/2018,08:40 pm"
        modelReviewDescription.review = "Lorem ipsum dolor sit amet, consectetur adipiscing elit.Etiam eget ligula eu lectus lobortis.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam eget ligula eu lectus lobortis condimentum"
        modelReviewDescription.userName = "Mila Kunis"
        arrayList.add(modelReviewDescription)


        modelReviewDescription = ReviewDescriptionModel()
        modelReviewDescription.memberImage = R.mipmap.profile_image
        modelReviewDescription.gestureImage = R.drawable.ic_dislike
        modelReviewDescription.dateTime = "11/10/2018,08:40 pm"
        modelReviewDescription.review ="Lorem ipsum dolor sit amet, consectetur adipiscing elit.Etiam eget ligula eu lectus lobortis.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam eget ligula eu lectus lobortis condimentum"
        modelReviewDescription.userName = "George o Vama"
        arrayList.add(modelReviewDescription)

        modelReviewDescription =
            ReviewDescriptionModel()
        modelReviewDescription.memberImage = R.mipmap.profile_image
        modelReviewDescription.gestureImage = R.drawable.ic_like
        modelReviewDescription.dateTime = "11/10/2018,08:40 pm"
        modelReviewDescription.review = "Lorem ipsum dolor sit amet, consectetur adipiscing elit.Etiam eget ligula eu lectus lobortis.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam eget ligula eu lectus lobortis condimentum"
        modelReviewDescription.userName = "Robert Upney"
        arrayList.add(modelReviewDescription)

        return arrayList

    }
}