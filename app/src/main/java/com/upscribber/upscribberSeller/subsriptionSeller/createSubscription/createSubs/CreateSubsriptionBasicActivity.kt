package com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.createSubs

import android.annotation.SuppressLint
import android.app.Activity
import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.graphics.Outline
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.commonClasses.RoundedBottomSheetDialogFragment
import com.upscribber.databinding.ActivityCreateSubsriptionBasicBinding
import com.upscribber.upscribberSeller.pictures.PicturesSellerActivity
import com.upscribber.upscribberSeller.pictures.coverImages.ModelCoverImages
import com.upscribber.upscribberSeller.store.storeMain.StoreModel
import com.upscribber.upscribberSeller.subsriptionSeller.StoreCoverAdapter
import com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.products.AdapterCreateProducts
import com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.products.SelectProductActivity
import kotlinx.android.synthetic.main.activity_create_subsription_basic.view.*
import java.util.*
import kotlin.collections.ArrayList


@Suppress(
    "RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS",
    "NAME_SHADOWING",
    "DEPRECATION"
)
class CreateSubsriptionBasicActivity : AppCompatActivity(), AdapterCreateProducts.Delete,
    StoreCoverAdapter.ImageClick {


    private lateinit var mAdapterProducts: AdapterCreateProducts
    private lateinit var binding: ActivityCreateSubsriptionBasicBinding
    private lateinit var viewModel: CreateSubscriptionViewModel
    private var arrayListOfProducts = ArrayList<StoreModel>()
    private var arrayListOfImages = ArrayList<ModelCoverImages>()
    private var checked = "1"
    private var campaignStart = ""
    private var campaignEnd = ""
    var dataStore = BasicModel()
    lateinit var adapter: StoreCoverAdapter
    lateinit var progessBarView: ConstraintLayout
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_create_subsription_basic)
        viewModel = ViewModelProviders.of(this)[CreateSubscriptionViewModel::class.java]
        binding.lifecycleOwner = this
        setToolbar()
        setAdapters()
        clickListeners()
        checkData()
        checkIfBought()
        checkIntentData()
    }

    private fun checkIntentData() {
        if (intent.hasExtra("products")) {
            val productData = intent.getParcelableExtra<StoreModel>("products")
            if (!arrayListOfProducts.contains(productData)) {
                if (arrayListOfProducts.size > 0) {
                    for (items in arrayListOfProducts) {
                        if (items.id == productData.id) {
                            return
                        } else {
                            arrayListOfProducts.add(productData)
                        }
                    }
                } else {
                    arrayListOfProducts.add(productData)
                }
                mAdapterProducts.update(arrayListOfProducts)
            } else {
                return
            }

        }
    }

    @SuppressLint("ResourceAsColor")
    private fun checkIfBought() {
        val mPrefs = application.getSharedPreferences("data", MODE_PRIVATE)
        var boughtIsMore = mPrefs.getString("bought", "0")
        if (boughtIsMore.isEmpty()) {
            boughtIsMore = "0"
        }
        if (boughtIsMore.toInt() > 0) {
            //Disable All Fields Except Description
            if (arrayListOfProducts.size > 0) {
                val modelCoverImages = ModelCoverImages()
                modelCoverImages.updated_at
                modelCoverImages.getImage = "disable"
                arrayListOfImages[0] = modelCoverImages
                adapter.update(arrayListOfImages)
            }

            if (arrayListOfProducts.size > 0) {
                val arrayListImage = ArrayList<ModelCoverImages>()
                arrayListImage.addAll(arrayListOfImages)
                arrayListOfImages.clear()
                for (items in arrayListImage) {
                    items.updated_at = "disable"
                    arrayListOfImages.add(items)
                }
                adapter.update(arrayListOfImages)
            }


            binding.addProduct.isClickable = false
            binding.addProduct.alpha = 0.4F
            binding.editTextDay.isClickable = false
            binding.editTextDay.alpha = 0.4F
            binding.editTextTime.isClickable = false
            binding.editTextTime.alpha = 0.4F
            binding.switchNeverEnds.isClickable = false
            binding.switchNeverEnds.alpha = 0.4F
            binding.recyclerProducts.isClickable = false
            binding.recyclerProducts.alpha = 0.4F
        }
    }

    @SuppressLint("ClickableViewAccessibility", "SetTextI18n")
    private fun clickListeners() {


        binding.editTextSubsDescription.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                val result = "${s!!.length}/90"
                binding.textView43.text = result

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })


        binding.imageView32.setOnClickListener {
            Constant.CommonIAlert(
                this,
                "Product",
                "Select some Products for this subscription"
            )
        }

        binding.imageView31.setOnClickListener {
            Constant.CommonIAlert(
                this,
                "Pictures",
                "Pictures help customers decide if they’ll purchase a subscription. Add a few relevant pictures here that show your product or service. For a service, you can add a picture of a customer enjoying the service or your staff performing the service."
            )
        }



        binding.imageView34.setOnClickListener {
            Constant.CommonIAlert(
                this,
                "Subscription Name",
                "Subscription name is a headline that your customers see on the marketplace. Add key information here to introduce your subscription and make them click! Mention how many redemptions they are getting and how much they’ll save."
            )
        }


        binding.imageView35.setOnClickListener {
            Constant.CommonIAlert(
                this,
                "Choose a Start Date",
                resources.getString(R.string.chooseDate)
            )
        }


        binding.editTextSubsDescription.setOnTouchListener { view, event ->
            view.parent.requestDisallowInterceptTouchEvent(true)
            if ((event.action and MotionEvent.ACTION_MASK) == MotionEvent.ACTION_UP) {
                view.parent.requestDisallowInterceptTouchEvent(false)
            }
            return@setOnTouchListener false
        }

        binding.addProduct.setOnClickListener {
            if (arrayListOfProducts.isNotEmpty()) {
                startActivityForResult(
                    Intent(
                        this,
                        SelectProductActivity::class.java
                    ).putExtra("products", arrayListOfProducts).putExtra("fromBasics","basic"), 1
                )
            } else {
                startActivityForResult(Intent(this, SelectProductActivity::class.java).putExtra("fromBasics","basic"), 1)

            }
        }

        binding.editTextDay.setOnClickListener {
            val mPrefs = application.getSharedPreferences("data", MODE_PRIVATE)
            var boughtIsMore = mPrefs.getString("bought", "0")
            if (boughtIsMore.isEmpty()) {
                boughtIsMore = "0"
            }
            if (boughtIsMore.toInt() > 0) {
                return@setOnClickListener
            }
            val c = Calendar.getInstance()
            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)
            val day = c.get(Calendar.DAY_OF_MONTH)
            val dpd = DatePickerDialog(
                this,
                DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->

                    binding.editTextDay.text = "" + dayOfMonth + "/" + (month + 1) + "/" + year
                    campaignStart =
                        year.toString() + "-" + (month + 1).toString() + "-" + dayOfMonth.toString()
                },
                year,
                month,
                day
            )

            dpd.datePicker.minDate = System.currentTimeMillis() - 1000

            dpd.show()

        }



        binding.editTextTime.setOnClickListener {
            if (binding.editTextDay.text == "") {
                Toast.makeText(this, "Please select start date first!", Toast.LENGTH_SHORT).show()
            } else {
                val mPrefs = application.getSharedPreferences("data", MODE_PRIVATE)
                var boughtIsMore = mPrefs.getString("bought", "0")
                if (boughtIsMore.isEmpty()) {
                    boughtIsMore = "0"
                }
                if (boughtIsMore.toInt() > 0) {
                    return@setOnClickListener
                }
                val c = Calendar.getInstance()
                val year = c.get(Calendar.YEAR)
                val month = c.get(Calendar.MONTH)
                val day = c.get(Calendar.DAY_OF_MONTH)
                val dpd = DatePickerDialog(
                    this,
                    DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->
                        binding.editTextTime.text = "" + dayOfMonth + "/" + (month + 1) + "/" + year
                        campaignEnd =
                            year.toString() + "-" + (month + 1).toString() + "-" + dayOfMonth.toString()

                    },
                    year,
                    month,
                    day
                )


                dpd.datePicker.minDate = System.currentTimeMillis() - 1000
                dpd.show()
            }
        }

        binding.switchNeverEnds.setOnCheckedChangeListener { _, p1 ->
            if (p1) {
                checked = "0"

                binding.editTextTime.isEnabled = false
                binding.editTextTime.setBackgroundResource(R.drawable.login_background_opacity)
                binding.editTextTime.setTextColor(resources.getColor(R.color.colorHeading_opacity))
                binding.editTextTime.setCompoundDrawablesWithIntrinsicBounds(
                    0,
                    0,
                    R.drawable.arrow_opacity,
                    0
                )
            } else {
                checked = "1"
                binding.editTextTime.isEnabled = true
                binding.editTextTime.setBackgroundResource(R.drawable.login_background)
                binding.editTextTime.setTextColor(resources.getColor(R.color.colorHeading))
                binding.editTextTime.setCompoundDrawablesWithIntrinsicBounds(
                    0,
                    0,
                    R.drawable.ic_arrow_right,
                    0
                )
            }

        }


    }

    override fun removeImage(
        position: Int,
        itemss: ArrayList<ModelCoverImages>
    ) {
        if (arrayListOfImages.size > 0) {
            arrayListOfImages.removeAt(position)
            if (arrayListOfImages.size == 1) {
                val modelCoverImages = ModelCoverImages()
                arrayListOfImages.add(modelCoverImages)
                arrayListOfImages.add(modelCoverImages)
                adapter.update(arrayListOfImages)
                return
            }
            adapter.update(arrayListOfImages)
        }

    }

    private fun setToolbar() {
        setSupportActionBar(binding.toolbar.toolbar)
        title = ""
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)
        progessBarView = findViewById(R.id.progressBar)

    }

    @SuppressLint("WrongConstant")
    private fun setAdapters() {
        binding.recyclerProducts.layoutManager = LinearLayoutManager(this)
        mAdapterProducts = AdapterCreateProducts(this)
        binding.recyclerProducts.adapter = mAdapterProducts


        binding.recyclerCreate.layoutManager =
            LinearLayoutManager(this, LinearLayout.HORIZONTAL, false)
        val modelCoverImages = ModelCoverImages()
        arrayListOfImages.add(modelCoverImages)
        arrayListOfImages.add(modelCoverImages)
        arrayListOfImages.add(modelCoverImages)
        adapter = StoreCoverAdapter(this)
        binding.recyclerCreate.adapter = adapter
        adapter.update(arrayListOfImages)
        arrayListOfImages.clear()



        binding.create.setOnClickListener {
            saveData()
//            val name = binding.editTextSubsDescription.text.toString().trim()
//            val campaignEndNull = ""
//            var products = ""
//            var images = ""
//            val list = ArrayList<String>()
//            val listImage = ArrayList<String>()
//
//            if (arrayListOfProducts.isNotEmpty()) {
//                for (i in 0 until arrayListOfProducts.size) {
//                    list.add(arrayListOfProducts[i].id)
//                    Log.e("array List of Products", arrayListOfProducts[i].id)
//                }
//                products = TextUtils.join(",", list)
//                Log.e("array List of Products", products)
//            }
//
//            if (arrayListOfImages.isNotEmpty()) {
//                for (j in 0 until arrayListOfImages.size) {
//                    listImage.add(arrayListOfImages[j].name)
//                }
//                images = TextUtils.join(",", listImage)
//
//            }
//            if (checked == "1" && campaignEnd == "") {
//                campaignStart = ""
//            }
//            if (checked == "1" && campaignEnd == "0000-00-00") {
//                campaignStart = ""
//            }
//
//
////            if (arrayListOfImages.size < 3) {
////                Toast.makeText(this, "Please select minimum of three images.", Toast.LENGTH_SHORT).show()
////
////            } else {
//
//            if (checked == "0") {
//                viewModel.getBasic(images, name, products, campaignStart, campaignEndNull, checked)
//            } else {
//                viewModel.getBasic(images, name, products, campaignStart, campaignEnd, checked)
//            }
//            }


        }

        viewModel.getStatus().observe(this, Observer {
            progessBarView.visibility = View.GONE
            if (it.msg.isNotEmpty()) {
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
            }
        })

        viewModel.getBasicData().observe(this, Observer {
            progessBarView.visibility = View.GONE
            val intent = Intent().putExtra("position", intent.getIntExtra("position", -1))
                .putExtra("steps", it.steps)
            setResult(99, intent)
            finish()
            Toast.makeText(this, "Your changes have been saved successfully", Toast.LENGTH_LONG)
                .show()
        })


    }

    fun saveData() {
        val name = binding.editTextSubsDescription.text.toString().trim()
        val campaignEndNull = ""
        var products = ""
        var images = ""
        val list = ArrayList<String>()
        val listImage = ArrayList<String>()

        if (arrayListOfProducts.isNotEmpty()) {
            for (i in 0 until arrayListOfProducts.size) {
                list.add(arrayListOfProducts[i].id)
                Log.e("array List of Products", arrayListOfProducts[i].id)
            }
            products = TextUtils.join(",", list)
            Log.e("array List of Products", products)
        }

        if (arrayListOfImages.isNotEmpty()) {
            for (j in 0 until arrayListOfImages.size) {
                listImage.add(arrayListOfImages[j].name)
            }
            images = TextUtils.join(",", listImage)

        }

        val mPrefs = application.getSharedPreferences("data", MODE_PRIVATE)
        var boughtIsMore = mPrefs.getString("bought", "0")
        if (boughtIsMore.isEmpty()) {
            boughtIsMore = "0"
        }
        if (boughtIsMore.toInt() <= 0) {
            if (intent.hasExtra(""))
                if (checked == "1" && campaignEnd == "") {
                    campaignStart = ""
                }
            if (checked == "1" && campaignEnd == "0000-00-00") {
                campaignStart = ""
            }
        }


//            if (arrayListOfImages.size < 3) {
//                Toast.makeText(this, "Please select minimum of three images.", Toast.LENGTH_SHORT).show()
//
//            } else {
        progessBarView.visibility = View.VISIBLE
        if (checked == "0") {
            viewModel.getBasic(images, name, products, campaignStart, campaignEndNull, checked)
        } else {
            viewModel.getBasic(images, name, products, campaignStart, campaignEnd, checked)
        }

    }

    @SuppressLint("WrongConstant")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        try {
            if (requestCode == 1) {
                arrayListOfProducts = data!!.getParcelableArrayListExtra<StoreModel>("model1")
                binding.recyclerProducts.visibility = View.VISIBLE
                mAdapterProducts.update(arrayListOfProducts)
            }

            if (requestCode == 1234) {
                arrayListOfImages =
                    data!!.getParcelableArrayListExtra<ModelCoverImages>("SelectedArray")
//                binding.recyclerCreate.layoutManager =
//                    LinearLayoutManager(this, LinearLayout.HORIZONTAL, false)
//                adapter = StoreCoverAdapter(this)
//                binding.recyclerCreate.adapter = adapter
                val modelCoverImages = ModelCoverImages()
                arrayListOfImages.add(0, modelCoverImages)
                adapter.update(arrayListOfImages)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun deleteAddedProduct(position: Int) {
        arrayListOfProducts.removeAt(position)
        mAdapterProducts.notifyDataSetChanged()

    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            if (intent.hasExtra("showDialog")) {
                val fragment = MenuFragmentDelete(this)
                fragment.show(supportFragmentManager, fragment.tag)
            } else {
                finish()
            }

        }
        return super.onOptionsItemSelected(item)
    }

    class MenuFragmentDelete(val mContext: Activity) : RoundedBottomSheetDialogFragment() {

        lateinit var yes: TextView
        lateinit var no: TextView
        lateinit var textHeading: TextView
        lateinit var textDescription: TextView

        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val view = inflater.inflate(R.layout.delete_product_sheet, container, false)

            yes = view.findViewById(R.id.yes)
            no = view.findViewById(R.id.no)
            textHeading = view.findViewById(R.id.textHeading)
            textDescription = view.findViewById(R.id.textDescription)
            val imageViewTop = view.findViewById<ImageView>(R.id.imageViewTop)
            roundImageView(imageViewTop)
            textHeading.text = "Saving"
            textDescription.text = "Do you want to save?"

            no.setOnClickListener {
                dismiss()
                mContext.finish()
            }

            yes.setOnClickListener {
                (mContext as CreateSubsriptionBasicActivity).saveData()
                dismiss()
            }

            return view
        }

        private fun roundImageView(imageViewTop: ImageView) {
            val curveRadius = 50F

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                imageViewTop.outlineProvider = object : ViewOutlineProvider() {

                    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                    override fun getOutline(view: View?, outline: Outline?) {
                        outline?.setRoundRect(
                            0,
                            0,
                            view!!.width,
                            view.height + curveRadius.toInt(),
                            curveRadius
                        )
                    }
                }
                imageViewTop.clipToOutline = true
            }
        }


    }


    @SuppressLint("WrongConstant")
    fun checkData() {
        val myPrefs = getSharedPreferences("data", Context.MODE_PRIVATE)
        val gson = Gson()
        val getBasic = myPrefs.getString("basicData", "")
        if (getBasic.isNotEmpty()) {
            dataStore = gson.fromJson(getBasic, BasicModel::class.java)

            binding.editTextSubsDescription.setText(dataStore.name)
            campaignStart = dataStore.campaign_start

            if (dataStore.campaign_type == "0") {
                binding.switchNeverEnds.isChecked = true
                dataStore.campaign_end = ""
                checked = "0"
                campaignEnd = ""
            } else {
                campaignEnd = dataStore.campaign_end
                checked = "1"
                binding.switchNeverEnds.isChecked =
                    campaignStart.isNotEmpty() && dataStore.campaign_end.isEmpty()
            }
            if (dataStore.productInfo.isNotEmpty()) {

                for (i in 0 until dataStore.productInfo.size) {

                    val storeModel = StoreModel()
                    storeModel.id = dataStore.productInfo[i].id
                    storeModel.price = dataStore.productInfo[i].price
                    storeModel.name = dataStore.productInfo[i].name


                    arrayListOfProducts.add(storeModel)
                }
            }
            binding.recyclerProducts.visibility = View.VISIBLE
            mAdapterProducts.update(arrayListOfProducts)


            binding.editTextDay.text = dataStore.campaign_start
            if (dataStore.campaign_end == "0000-00-00" || dataStore.campaign_end.isEmpty()) {
                binding.editTextTime.text = ""
                dataStore.campaign_end = ""
            } else {
                binding.editTextTime.text = dataStore.campaign_end
            }


            // binding.switchNeverEnds.isChecked = dataStore.campaign_end == ""
            val arrayList = ArrayList<ModelCoverImages>()
            if (dataStore.image.isNotEmpty()) {
                val images = dataStore.image.split(",")
                val imagePath = Constant.getPrefs(this)
                    .getString(
                        Constant.dataImagePath,
                        "http://cloudart.com.au/projects/upscribbr/uploads/"
                    )



                for (i in images.indices) {
                    val model = ModelCoverImages()
                    model.image = 0
                    model.status = false
                    model.getImage = imagePath + images[i]
                    model.name = images[i]
                    arrayList.add(model)
                }
            } else {
                val modelCoverImages = ModelCoverImages()
                arrayList.add(modelCoverImages)
                arrayList.add(modelCoverImages)
//                arrayList.add(modelCoverImages)

            }
            arrayListOfImages = arrayList
//            binding.recyclerCreate.layoutManager =
//                LinearLayoutManager(this, LinearLayout.HORIZONTAL, false)
//            adapter = StoreCoverAdapter(this)
//            binding.recyclerCreate.adapter = adapter
            val modelCoverImages = ModelCoverImages()
            arrayListOfImages.add(0, modelCoverImages)
            adapter.update(arrayListOfImages)
        }


    }

    override fun click() {
        if (arrayListOfImages.size > 0) {
            startActivityForResult(
                Intent(
                    this,
                    PicturesSellerActivity::class.java
                ).putExtra("create", "others").putExtra("images", arrayListOfImages), 1234
            )
        } else {
            startActivityForResult(
                Intent(
                    this,
                    PicturesSellerActivity::class.java
                ).putExtra("create", "others"), 1234
            )
        }
    }
}
