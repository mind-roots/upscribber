package com.upscribber.upscribberSeller.subsriptionSeller.ratingStats

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.upscribber.subsciptions.merchantStore.ModelReviewDescription
import com.upscribber.subsciptions.merchantStore.ModelReviews

class RatingStatsViewModel(application: Application) : AndroidViewModel(application){

    var ratingStatsRepository:RatingStatsRepository= RatingStatsRepository(application)

    fun getReviews(): MutableLiveData<ArrayList<ModelReviews>> {
        return ratingStatsRepository.getReview()
    }

    fun getReviewsDescription(): MutableLiveData<ArrayList<ModelReviewDescription>> {
        return ratingStatsRepository.getReviewsDescription()
    }
}