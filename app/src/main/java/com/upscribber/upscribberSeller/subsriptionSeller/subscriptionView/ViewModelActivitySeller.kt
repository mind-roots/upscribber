package com.upscribber.upscribberSeller.subsriptionSeller.subscriptionView

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.upscribber.subsciptions.merchantSubscriptionPages.deal.DealModel
import com.upscribber.subsciptions.merchantSubscriptionPages.plan.PlanModel
import com.upscribber.subsciptions.merchantSubscriptionPages.reviews.ReviewDescriptionModel
import com.upscribber.subsciptions.merchantSubscriptionPages.reviews.ReviewModel
import com.upscribber.subsciptions.merchantSubscriptionPages.staff.StaffFaqModel
import com.upscribber.subsciptions.merchantSubscriptionPages.staff.StaffModel
import com.upscribber.upscribberSeller.dashboardfragment.reporting.ReportingModel

class ViewModelActivitySeller(application: Application):AndroidViewModel(application) {
    var refVar:RepoActivitySubscriptionSeller= RepoActivitySubscriptionSeller(application)

    fun getPlanValues():MutableLiveData<ArrayList<PlanModel>>{
        return refVar.getPlanData()
    }

    fun getFreeList():MutableLiveData<ArrayList<ModelOption>> {
        return refVar.getFreeList()
    }

    fun deals(): MutableLiveData<ArrayList<DealModel>> {

        return refVar.dealsData()
    }

    fun staffTop(): MutableLiveData<ArrayList<StaffModel>> {
        return refVar.staffTop()
    }

    fun staffFaq(): MutableLiveData<ArrayList<StaffFaqModel>> {
        return refVar.staffFaq()

    }

    fun getGraphData(): MutableLiveData<ReportingModel> {
        return refVar.getGraphData()
    }


    fun getReviewData(): MutableLiveData<ArrayList<ReviewModel>> {
        return refVar.getReviewsData()
    }

    fun getReviewDescData(): MutableLiveData<ArrayList<ReviewDescriptionModel>> {
        return refVar.getReviewDescData()
    }




}