package com.upscribber.upscribberSeller.subsriptionSeller.extras

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.upscribber.R
import com.upscribber.upscribberSeller.navigationCustomer.ModelCustomerList

class ExtraInfoRepository(application: Application) {
    fun getList(): LiveData<ArrayList<ModelCustomerList>> {
        val mData = MutableLiveData<ArrayList<ModelCustomerList>>()
        val arrayList = ArrayList<ModelCustomerList>()

        var modelCustomerList = ModelCustomerList()
        modelCustomerList.customerName = "Mars Spa"
        modelCustomerList.customerPhone = "+578454548545"
        modelCustomerList.customerImage = R.mipmap.profile_image
        arrayList.add(modelCustomerList)

        modelCustomerList = ModelCustomerList()
        modelCustomerList.customerName = "Mars Spa"
        modelCustomerList.customerPhone = "+578454548545"
        modelCustomerList.customerImage = R.mipmap.profile_image
        arrayList.add(modelCustomerList)

        modelCustomerList = ModelCustomerList()
        modelCustomerList.customerName = "Mars Spa"
        modelCustomerList.customerPhone = "+578454548545"
        modelCustomerList.customerImage = R.mipmap.profile_image
        arrayList.add(modelCustomerList)

        modelCustomerList = ModelCustomerList()
        modelCustomerList.customerName = "Mars Spa"
        modelCustomerList.customerPhone = "+578454548545"
        modelCustomerList.customerImage = R.mipmap.profile_image
        arrayList.add(modelCustomerList)

        modelCustomerList = ModelCustomerList()
        modelCustomerList.customerName = "Mars Spa"
        modelCustomerList.customerPhone = "+578454548545"
        modelCustomerList.customerImage = R.mipmap.profile_image
        arrayList.add(modelCustomerList)

        mData.value = arrayList
        return mData

    }
}