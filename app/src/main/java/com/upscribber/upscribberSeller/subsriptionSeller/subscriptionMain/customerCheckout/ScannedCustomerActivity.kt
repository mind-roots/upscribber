package com.upscribber.upscribberSeller.subsriptionSeller.subscriptionMain.customerCheckout

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import com.bumptech.glide.Glide
import com.google.zxing.BarcodeFormat
import com.google.zxing.MultiFormatWriter
import com.google.zxing.WriterException
import com.journeyapps.barcodescanner.BarcodeEncoder
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import kotlinx.android.synthetic.main.activity_scanned_customer.*

class ScannedCustomerActivity : AppCompatActivity() {

    val multiFormatWriter = MultiFormatWriter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scanned_customer)
        setToolbar()
        setData()
    }

    private fun setData() {
        if (intent.hasExtra("generateCode")) {
            val profileData = Constant.getArrayListProfile(this, Constant.dataProfile)
            val imagePath = Constant.getPrefs(this).getString(Constant.dataImagePath, "")
            Glide.with(this).load(imagePath + profileData.profile_image)
                .placeholder(R.drawable.ic_male_dummy).into(circleImageView8)
            textCampaignName.text = profileData.name
            textCode.text = profileData.invite_code
            cancelBtn.visibility = View.GONE

            val generateCode = profileData.invite_code
            try {
                val bitMatrix =
                    multiFormatWriter.encode(generateCode, BarcodeFormat.QR_CODE, 3000, 3000)
                val barcodeEncoder = BarcodeEncoder()
                val bitmap = barcodeEncoder.createBitmap(bitMatrix)
                imageViewQRCode.setImageBitmap(bitmap)
            } catch (e: WriterException) {
                e.printStackTrace()
            }


        } else {
            cancelBtn.visibility = View.VISIBLE
        }
    }


    private fun setToolbar() {
        setSupportActionBar(include10)
        title = ""
        toolbarTitle.text = "Purchase Code"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}
