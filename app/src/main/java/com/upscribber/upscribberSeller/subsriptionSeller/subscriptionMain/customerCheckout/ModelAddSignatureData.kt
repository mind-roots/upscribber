package com.upscribber.upscribberSeller.subsriptionSeller.subscriptionMain.customerCheckout

import android.os.Parcel
import android.os.Parcelable

class ModelAddSignatureData() : Parcelable{

    var lastFour: String = ""
    var new_user: String = ""
    var card_id: String = ""
    var billingType: String = ""
    var sub_total: String = ""
    var subscription_id: String = ""
    var tax: String = ""
    var first_name: String = ""
    var last_name: String = ""
    var email: String = ""
    var contact_no: String = ""
    var street: String = ""
    var city: String = ""
    var state: String = ""
    var zip: String = ""
    var suite: String = ""
    var billingState: String = ""
    var billingCity: String = ""
    var billingStreet: String = ""
    var billingZip: String = ""
    var billingSuite: String = ""
    var is_intro: String = ""
    var actual_price: String = ""
    var total: String = ""
    var total_amount: String = ""
    var buy_type: String = ""
    var user_id: String = ""
    var freeTrial: String = ""
    var credits : String = ""
    var last4 : String =""
    var payment_type : String = ""
    var payment_method_name : String = ""

    constructor(parcel: Parcel) : this() {
        lastFour = parcel.readString()
        new_user = parcel.readString()
        card_id = parcel.readString()
        billingType = parcel.readString()
        sub_total = parcel.readString()
        subscription_id = parcel.readString()
        tax = parcel.readString()
        first_name = parcel.readString()
        last_name = parcel.readString()
        email = parcel.readString()
        contact_no = parcel.readString()
        street = parcel.readString()
        city = parcel.readString()
        state = parcel.readString()
        zip = parcel.readString()
        suite = parcel.readString()
        billingState = parcel.readString()
        billingCity = parcel.readString()
        billingStreet = parcel.readString()
        billingZip = parcel.readString()
        billingSuite = parcel.readString()
        is_intro = parcel.readString()
        actual_price = parcel.readString()
        total = parcel.readString()
        total_amount = parcel.readString()
        buy_type = parcel.readString()
        user_id = parcel.readString()
        freeTrial = parcel.readString()
        credits = parcel.readString()
        last4 = parcel.readString()
        payment_type = parcel.readString()
        payment_method_name = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(lastFour)
        parcel.writeString(new_user)
        parcel.writeString(card_id)
        parcel.writeString(billingType)
        parcel.writeString(sub_total)
        parcel.writeString(subscription_id)
        parcel.writeString(tax)
        parcel.writeString(first_name)
        parcel.writeString(last_name)
        parcel.writeString(email)
        parcel.writeString(contact_no)
        parcel.writeString(street)
        parcel.writeString(city)
        parcel.writeString(state)
        parcel.writeString(zip)
        parcel.writeString(suite)
        parcel.writeString(billingState)
        parcel.writeString(billingCity)
        parcel.writeString(billingStreet)
        parcel.writeString(billingZip)
        parcel.writeString(billingSuite)
        parcel.writeString(is_intro)
        parcel.writeString(actual_price)
        parcel.writeString(total)
        parcel.writeString(total_amount)
        parcel.writeString(buy_type)
        parcel.writeString(user_id)
        parcel.writeString(freeTrial)
        parcel.writeString(credits)
        parcel.writeString(last4)
        parcel.writeString(payment_type)
        parcel.writeString(payment_method_name)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ModelAddSignatureData> {
        override fun createFromParcel(parcel: Parcel): ModelAddSignatureData {
            return ModelAddSignatureData(parcel)
        }

        override fun newArray(size: Int): Array<ModelAddSignatureData?> {
            return arrayOfNulls(size)
        }
    }


}
