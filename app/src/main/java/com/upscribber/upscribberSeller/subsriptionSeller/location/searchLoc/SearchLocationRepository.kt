package com.upscribber.upscribberSeller.subsriptionSeller.location.searchLoc

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.upscribber.commonClasses.Constant
import com.upscribber.commonClasses.ModelStatusMsg
import com.upscribber.commonClasses.WebServicesCustomers
import okhttp3.ResponseBody
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class SearchLocationRepository(application: Application) {

    val mDataFailure = MutableLiveData<ModelStatusMsg>()
    val mDataDelete = MutableLiveData<ModelStatusMsg>()
    val mDataDefault = MutableLiveData<ModelStatusMsg>()
    val mDataLocation = MutableLiveData<ArrayList<ModelSearchLocation>>()
    val mDataAddLocation = MutableLiveData<ModelSearchLocation>()

    fun getlocationList(auth: String, search: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.getLocation(auth)
        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val data = json.optJSONObject("data")
                        val locations = data.optJSONArray("locations")
                        val default = data.optString("default")

                        val arrayLocation = ArrayList<ModelSearchLocation>()
                        val modelStatus = ModelStatusMsg()
                        if (status == "true") {
                            for (i in 0 until locations.length()) {
                                val arrayStoreTimings = ArrayList<ModelStoreLocation>()
                                val locationData = locations.getJSONObject(i)
                                val modelLocation = ModelSearchLocation()
                                modelLocation.id = locationData.optString("id")
                                modelLocation.street = locationData.optString("street")
                                modelLocation.city = locationData.optString("city")
                                modelLocation.state = locationData.optString("state")
                                modelLocation.zip_code = locationData.optString("zip_code")
                                modelLocation.lat = locationData.optString("lat")
                                modelLocation.long = locationData.optString("long")
                                val storeInfo = locationData.optJSONArray("store_timing")
                                for (j in 0 until storeInfo.length()) {
                                    val storetimings = storeInfo.getJSONObject(j)
                                    val storeTimingsModel = ModelStoreLocation()
                                    storeTimingsModel.open = storetimings.optString("open")
                                    storeTimingsModel.close = storetimings.optString("close")
                                    storeTimingsModel.status = storetimings.optString("status")
                                    storeTimingsModel.checked = storetimings.optString("checked")
                                    storeTimingsModel.days.add(storetimings.optInt("day").toString())
                                    arrayStoreTimings.add(storeTimingsModel)
                                }
                                modelLocation.store_timing = arrayStoreTimings
                                modelLocation.status1 = status
                                modelLocation.msg = msg
                                modelLocation.default = default
                                arrayLocation.add(modelLocation)
                            }
                            mDataLocation.value = arrayLocation
                        } else {
                            modelStatus.msg = msg
                            mDataFailure.value = modelStatus
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus

            }
        })
    }


    fun getmDataFailure(): LiveData<ModelStatusMsg> {
        return mDataFailure
    }

    fun getmDataDefault(): LiveData<ModelStatusMsg> {
        return mDataDefault
    }


    fun getmDataDelete(): LiveData<ModelStatusMsg> {
        return mDataDelete
    }


    fun getLocationData(): MutableLiveData<ArrayList<ModelSearchLocation>> {
        return mDataLocation
    }

    fun getAddLocation(
        auth: String,
        id: String,
        address: String,
        city: String,
        state: String,
        zip: String,
        arrayOperationHours: JSONArray,
        latitude: String,
        longitude: String
    ) {

        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.saveLocation(auth, id, address, city, state, zip, arrayOperationHours,latitude,longitude)
        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val data = json.optJSONObject("data")
                        val model = ModelSearchLocation()
                        val modelStatus = ModelStatusMsg()
                        if (status == "true") {
                            model.status1 = status
                            model.msg = msg
                            model.street = data.optString("street")
                            model.city = data.optString("city")
                            model.zip_code = data.optString("zip_code")
                            model.state = data.optString("state")
                            model.lat = data.optString("lat")
                            model.long = data.optString("long")
//                            val arrayStoreTimings = ArrayList<ModelStoreLocation>()
//                            val storeInfo = data.optJSONArray("store_timing")
//                            for (j in 0 until storeInfo.length()) {
//                                val storetimings = storeInfo.getJSONObject(j)
//                                val storeTimingsModel = ModelStoreLocation()
//                                storeTimingsModel.open = storetimings.optString("open")
//                                storeTimingsModel.close = storetimings.optString("close")
//                                storeTimingsModel.status = storetimings.optString("status")
//                                storeTimingsModel.checked = storetimings.optString("checked")
//                                storeTimingsModel.days.add(storetimings.optInt("days").toString())
//                                arrayStoreTimings.add(storeTimingsModel)
//                            }
//                            model.store_timing = arrayStoreTimings
                            val storeTiming = data.optString("store_timing")
                            if (storeTiming == "" || storeTiming == "[]") {
                                val storeModel = ModelStoreLocation()
                                storeModel.day = ""
                                storeModel.open = ""
                                storeModel.close = ""
                                storeModel.checked = ""
                                storeModel.status = ""
                                model.store_timing.add(storeModel)
                            } else {
                                var gson=Gson()
                                val model1: ArrayList<ModelStoreLocation> =
                                    gson.fromJson(
                                        storeTiming,
                                        object : TypeToken<ArrayList<ModelStoreLocation>>() {}.type
                                    )
                                model.store_timing = model1

                                /*if (storeArray.length() > 0) {
                                    for (j in 0 until storeArray.length()) {
                                        val storeData = storeArray.optJSONObject(j)
                                        val storeModel = ModelStoreLocation()
                                        storeModel.day = storeData.optString("days")
                                        storeModel.open = storeData.optString("open")
                                        storeModel.close = storeData.optString("close")
                                        storeModel.checked = storeData.optString("checked")
                                        storeModel.status = storeData.optString("status")
                                        modelSearchLocation.store_timing.add(storeModel)
                                    }
                                }*/
                            }
                            mDataAddLocation.value = model
                        } else {
                            modelStatus.msg = msg
                            mDataFailure.value = modelStatus
                        }


                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus

            }
        })
    }

    fun getAddLocationData(): MutableLiveData<ModelSearchLocation> {
        return mDataAddLocation
    }


    fun getDeleteLocation(auth: String, idddd: String, default: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.deleteLocation(auth, idddd, default)
        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()

                        if (status == "true") {
                            modelStatus.status = status
                            modelStatus.msg = msg

                            mDataDelete.value = modelStatus
                        } else {
                            modelStatus.status = "false"
                            modelStatus.msg = "Network Error!"
                            mDataFailure.value = modelStatus
                        }

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus
            }
        })
    }

    fun getPrimaryLocation(auth: String, id: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.setDefultLocation(auth, id)
        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()

                        if (status == "true") {
                            modelStatus.status = status
                            modelStatus.msg = msg

                            mDataDefault.value = modelStatus
                        } else {
                            modelStatus.status = "false"
                            modelStatus.msg = "Network Error!"
                            mDataFailure.value = modelStatus
                        }

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus
            }
        })

    }
}
