package com.upscribber.upscribberSeller.subsriptionSeller.subscriptionMain.customerCheckout

import android.content.Context
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.R
import com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.subscription.ModelSubscriptionCard
import com.upscribber.upscribberSeller.subsriptionSeller.subscriptionMain.ModelMain
import kotlinx.android.synthetic.main.subscription_plan_recycler.view.*
import java.math.BigDecimal
import java.math.RoundingMode

class AdapterSubscriptionPlanCheckOut(
    var mContext: Context,
    frag: Fragment
) :
    RecyclerView.Adapter<AdapterSubscriptionPlanCheckOut.MyViewHolder>() {


     var list: ArrayList<ModelSubscriptionCard> = ArrayList()
    private var itAdapter: ModelMain = ModelMain()
    var show = frag as GoNextScreenBuyButton

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(mContext).inflate(R.layout.subscription_plan_recycler, parent, false)
        return MyViewHolder(v)

    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(
        holder: MyViewHolder,
        position: Int
    ) {
        try {
            val model = list[position]
            if (model.isSelected) {
                holder.itemView.cl_plan.setBackgroundResource(R.drawable.new_bg_customers_blue_selection)
                holder.itemView.check.visibility = View.VISIBLE
            } else {
                holder.itemView.cl_plan.setBackgroundResource(R.drawable.new_bg_customers)
                holder.itemView.check.visibility = View.GONE
            }

            if (model.frequencyType == "Year"){
                holder.itemView.annual.text =  "Annual" + " Subscription"
            }else{
                holder.itemView.annual.text = model.frequencyType + " Subscription"
            }


            if(model.redeemtion_cycle_qty > "100"){
                holder.itemView.upto.text =   "Unlimited " + model.unit + " per " + model.frequencyType
            } else if (model.redeemtion_cycle_qty == "1") {
                holder.itemView.upto.text = "1 " + model.unit + " Build " + model.frequencyType
            } else {
                holder.itemView.upto.text =
                    model.redeemtion_cycle_qty + "X " + model.unit + " per " + model.frequencyType
            }



//        when (model.redeemtion_cycle_qty) {
//            "500000" -> {
//                holder.itemView.upto.text = "Unlimited " + model.unit + " per " + model.frequencyType
//            }
//            "1" -> {
//                holder.itemView.upto.text = "1 " + model.unit + " Build " + model.frequencyType
//            }
//            else -> {
//                holder.itemView.upto.text =
//                    model.redeemtion_cycle_qty + "X " + model.unit + " per " + model.frequencyType
//            }
//        }

            val discountedPrice =
                (model.price.toDouble() - (model.discountPrice.toDouble() / 100 * model.price.toDouble())).toString()
            val splitPos = discountedPrice.split(".")
            if (splitPos[1] == "00" || splitPos[1] == "0") {
                holder.itemView.tv_cost.text = "$" + splitPos[0]
            } else {
                holder.itemView.tv_cost.text =
                    "$" + BigDecimal(discountedPrice).setScale(2, RoundingMode.HALF_UP)
            }

            if (model.discountPrice == "0" || model.discountPrice == "0.0" || model.discountPrice == "0.00") {
                holder.itemView.tv_off.visibility = View.INVISIBLE
            } else {
                holder.itemView.tv_off.visibility = View.VISIBLE
                if (model.discountPrice.contains(".")) {
                    val splitPerc = model.discountPrice.split(".")
                    if (splitPerc[1] == "00" || splitPerc[1] == "0") {
                        holder.itemView.tv_off.text = splitPerc[0] + "% off"
                    } else {
                        holder.itemView.tv_off.text = "" + BigDecimal(model.discountPrice).setScale(2, RoundingMode.HALF_UP) + "%"
                    }
                } else {
                    holder.itemView.tv_off.text = model.discountPrice + "% off"
                }

            }

            if (model.freeTrail == "No free trial" && (model.introductory_price == "0" || model.introductory_price == "0.00")) {
                holder.itemView.textView362.visibility = View.GONE
                holder.itemView.imageView87.visibility = View.GONE
            } else if (model.freeTrail != "No free trial") {
                holder.itemView.textView362.visibility = View.VISIBLE
                holder.itemView.imageView87.visibility = View.VISIBLE
                holder.itemView.textView362.text = "Free Trail"
                holder.itemView.imageView87.setImageResource(R.drawable.ic_free_trial_merchant)
            } else {
                holder.itemView.textView362.visibility = View.VISIBLE
                holder.itemView.imageView87.visibility = View.VISIBLE
                holder.itemView.textView362.text = "Intro Price"
                holder.itemView.imageView87.setImageResource(R.drawable.ic_intro_price)
            }




            holder.itemView.setOnClickListener {
                show.nextClicks(model, position)
            }



            if (model.price.contains(".")) {
                val splitActualPrice = model.price.split(".")
                if (splitActualPrice[1] == "00" || splitActualPrice[1] == "0") {
                    holder.itemView.tv_last_text.text = "$" +splitActualPrice[0]
                } else {
                    holder.itemView.tv_last_text.text =  "$" + BigDecimal(model.price).setScale(2, RoundingMode.HALF_UP)
                }
            } else {
                holder.itemView.tv_last_text.text = model.price
            }



            holder.itemView.tv_last_text.paintFlags = holder.itemView.tv_last_text.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
        }catch (e : Exception){
            e.printStackTrace()
        }
    }


    fun update(it: ArrayList<ModelSubscriptionCard>) {
        list = it
        notifyDataSetChanged()

    }



    interface GoNextScreenBuyButton {
        fun nextClicks(
            model: ModelSubscriptionCard,
            position: Int
        )
    }

}
