package com.upscribber.upscribberSeller.subsriptionSeller.subscriptionMain

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Outline
import android.graphics.Paint
import android.os.Build
import android.os.Bundle
import android.view.*
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.makeramen.roundedimageview.RoundedImageView
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.commonClasses.RoundedBottomSheetDialogFragment
import com.upscribber.databinding.FragmentSubscriptionsSellerBinding
import com.upscribber.upscribberSeller.navigationCustomer.InviteSubscription
import com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.createSubs.CreateSubscriptionActivity
import com.upscribber.upscribberSeller.subsriptionSeller.subscriptionMain.customerCheckout.CheckCustomer
import java.math.BigDecimal
import java.math.RoundingMode


@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class SubscriptionSellerFragment : Fragment(), AdapterStatus.Selection,
    AdapterSellerSubscriptions.reloadData {

    private var tabPosition: Int = 0
    private lateinit var mAdapterSubsStatus: AdapterStatus
    private lateinit var mAdapterSubsData: AdapterSellerSubscriptions
    lateinit var mBinding: FragmentSubscriptionsSellerBinding
    private lateinit var mViewModel: SubscriptionSellerViewModel
    private lateinit var backgroundChange: ArrayList<ModelStatus>
    lateinit var mAdapterSubscription: AdapterSubscriptionList
    var localData = ArrayList<ModelSellerSubscriptions>()
    var totalPriceText = ""

    override fun onCreateView(
        inflater: LayoutInflater, container:
        ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        mBinding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_subscriptions_seller,
            container,
            false
        )
        mViewModel = ViewModelProviders.of(this)[SubscriptionSellerViewModel::class.java]
        mAdapterSubscription = AdapterSubscriptionList(activity!!)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adiInitialization()
        setAdapter()
        clickListeners()
        observerInit()
    }

    private fun clickListeners() {
        mBinding.getStartedBtn.setOnClickListener {
            val mPrefs = activity!!.getSharedPreferences("data", AppCompatActivity.MODE_PRIVATE)
            val mPrefs1 = activity!!.getSharedPreferences(
                "customers",
                AppCompatActivity.MODE_PRIVATE
            )
            val prefsEditor = mPrefs.edit()
            val prefsEditor1 = mPrefs1.edit()
            prefsEditor1.putString("customers", "")
            prefsEditor.putString("detailsArray", "")
            prefsEditor.putString("pricing", "")
            prefsEditor.putString("basicData", "")
            prefsEditor.putString("campaign_id", "")
            prefsEditor.putString("bought", "")
            prefsEditor.apply()
            prefsEditor1.apply()
            CreateSubscriptionActivity.editData = ModelMain()
            startActivity(Intent(activity, CreateSubscriptionActivity::class.java))
        }

    }

    private fun observerInit() {
        mViewModel.getStatus().observe(this, Observer {
            if (it.msg == "Invalid auth code") {
                Constant.commonAlert(activity!!)
            } else {
                Toast.makeText(activity!!, it.msg, Toast.LENGTH_SHORT).show()
            }
        })
        mViewModel.getmAllCampaignsData().observe(this, Observer {
            if (it.size > 0) {
                mBinding.noDAta.visibility = View.GONE
                mBinding.progressBar23.visibility = View.GONE
                mBinding.recyclerViewStatus.visibility = View.VISIBLE
                mBinding.recyclerCardDAta.visibility = View.VISIBLE
                mBinding.recyclerCardDAta.layoutManager = LinearLayoutManager(activity!!)
                mAdapterSubsData = AdapterSellerSubscriptions(activity!!, this)
                mBinding.recyclerCardDAta.adapter = mAdapterSubsData
                localData = it
                it.reverse()
                val localData1 = ArrayList<ModelSellerSubscriptions>()

                when (tabPosition) {
                    0 -> {
                        for (i in 0 until it.size) {
                            if (it[i].is_public == "0" && it[i].status1 == "1") {
                                localData1.add(it[i])
                            }
                        }
                        if (localData1.size > 0) {
                            mBinding.imgNoSubscription.visibility = View.GONE
                            mBinding.tvNoData.visibility = View.GONE
                            mBinding.tvNoDataDescription.visibility = View.GONE
                            mBinding.getStartedBtn.visibility = View.GONE
                            mBinding.recyclerCardDAta.visibility = View.VISIBLE
                        } else {
                            mBinding.imgNoSubscription.visibility = View.VISIBLE
                            mBinding.tvNoData.visibility = View.VISIBLE
                            mBinding.tvNoDataDescription.visibility = View.VISIBLE
                            mBinding.tvNoDataDescription.text =
                                "You do not have any subscriptions created at the moment. Please create some subscriptions for your customers."
                            val type = Constant.getPrefs(activity!!).getString(Constant.type, "")
                            if (type == "3") {
                                mBinding.getStartedBtn.visibility = View.GONE
                            } else {
                                mBinding.getStartedBtn.visibility = View.VISIBLE
                            }
                            mBinding.recyclerCardDAta.visibility = View.GONE
                        }
                    }
                    1 -> {
                        for (i in 0 until it.size) {
                            if (
                                it[i].status1 == "0" && it[i].is_public == "0") {
                                localData1.add(it[i])
                            }
                        }
                        if (localData1.size > 0) {
                            mBinding.imgNoSubscription.visibility = View.GONE
                            mBinding.tvNoData.visibility = View.GONE
                            mBinding.tvNoDataDescription.visibility = View.GONE
                            mBinding.getStartedBtn.visibility = View.GONE
                            mBinding.recyclerCardDAta.visibility = View.VISIBLE
                        } else {
                            mBinding.imgNoSubscription.visibility = View.VISIBLE
                            mBinding.tvNoData.visibility = View.VISIBLE
                            mBinding.tvNoDataDescription.visibility = View.VISIBLE
                            mBinding.tvNoDataDescription.text =
                                "You do not have any inactive subscriptions at the moment."

                            val type = Constant.getPrefs(activity!!).getString(Constant.type, "")
                            if (type == "3") {
                                mBinding.getStartedBtn.visibility = View.GONE
                            } else {
                                mBinding.getStartedBtn.visibility = View.VISIBLE
                            }
                            mBinding.recyclerCardDAta.visibility = View.GONE
                        }

                    }
                    2 -> {
                        for (i in 0 until it.size) {
                            if (it[i].is_public == "1") {
                                localData1.add(it[i])
                            }
                        }
                        if (localData1.size > 0) {
                            mBinding.imgNoSubscription.visibility = View.GONE
                            mBinding.tvNoData.visibility = View.GONE
                            mBinding.tvNoDataDescription.visibility = View.GONE
                            mBinding.getStartedBtn.visibility = View.GONE
                            mBinding.recyclerCardDAta.visibility = View.VISIBLE
                        } else {
                            mBinding.imgNoSubscription.visibility = View.VISIBLE
                            mBinding.tvNoData.visibility = View.VISIBLE
                            mBinding.tvNoDataDescription.visibility = View.VISIBLE
                            mBinding.tvNoDataDescription.text =
                                "You do not have any private subscriptions at the moment."

                            val type = Constant.getPrefs(activity!!).getString(Constant.type, "")
                            if (type == "3") {
                                mBinding.getStartedBtn.visibility = View.GONE
                            } else {
                                mBinding.getStartedBtn.visibility = View.VISIBLE
                            }
                            mBinding.recyclerCardDAta.visibility = View.GONE
                        }
                    }
                }
                mAdapterSubsData.update(localData1)
            } else {
                mBinding.noDAta.visibility = View.GONE
                mBinding.progressBar23.visibility = View.GONE
                mBinding.recyclerViewStatus.visibility = View.GONE
                mBinding.recyclerCardDAta.visibility = View.GONE
                mBinding.imgNoSubscription.visibility = View.VISIBLE
                mBinding.tvNoData.visibility = View.VISIBLE
                mBinding.tvNoDataDescription.visibility = View.VISIBLE
                val type = Constant.getPrefs(activity!!).getString(Constant.type, "")
                if (type == "3") {
                    mBinding.getStartedBtn.visibility = View.GONE
                } else {
                    mBinding.getStartedBtn.visibility = View.VISIBLE
                }
            }

        })

        mViewModel.getmActiveStatus1().observe(this, Observer {
            adiInitialization()

        })


    }

    private fun adiInitialization() {
        mBinding.progressBar23.visibility = View.VISIBLE
        val auth = Constant.getPrefs(activity!!).getString(Constant.auth_code, "")
        mViewModel.getAllSubscriptionsData(auth, "")

    }


    override fun selectSubscription(position: Int, model: ModelSellerSubscriptions) {
        startActivity(Intent(context, InviteSubscription::class.java).putExtra("model", model))
    }

    override fun checkoutCustomer(position: Int, model: ModelSellerSubscriptions) {
        if (model.introductory_days != "0") {
            val introPrice =
                BigDecimal(model.introductory_price).setScale(2, RoundingMode.HALF_UP).toString()
            if (introPrice.contains(".")) {
                val splitProductPrice = introPrice.split(".")
                if (splitProductPrice[1] == "00" || splitProductPrice[1] == "0") {
                    totalPriceText = "$" + splitProductPrice[0]
                } else {
                    totalPriceText = "$$introPrice"
                }
            } else {
                totalPriceText = "$$introPrice"
            }
        } else {
            val calculatedDiscount =
                (model.discount_price.toDouble() / 100 * model.price.toDouble())

            val priceDis = model.price.toDouble() - calculatedDiscount
            val disPriceFormated = BigDecimal(priceDis).setScale(2, RoundingMode.HALF_UP)
            val price = disPriceFormated.toString().split(".")
            if (price[1] == "0" || price[1] == "00") {
                totalPriceText = "$" + price[0]
            } else {
                totalPriceText = "$$disPriceFormated"
            }
        }

        startActivity(
            Intent(activity, CheckCustomer::class.java).putExtra(
                "modelSubscriptionModel",
                model
            ).putExtra("totalPriceText", totalPriceText)
        )
//        val fragment = MenuFragmentCampaignDetail(model,totalPriceText)
//        fragment.show(fragmentManager!!, fragment.tag)

    }

    class MenuFragmentCampaignDetail(
        var arrayCampaign: ModelSellerSubscriptions,
        var totalPriceText: String
    ) : RoundedBottomSheetDialogFragment() {

        lateinit var tvVisits: TextView
        lateinit var frequencyType: TextView
        lateinit var proceedCheckout: TextView
        lateinit var textFreeTrail: TextView
        lateinit var tvName: TextView
        lateinit var productPrice: TextView
        lateinit var productOffer: TextView
        lateinit var actualPrice: TextView
        lateinit var txtIntroPrice: TextView
        lateinit var subTotal: TextView
        lateinit var subTotalPrice: TextView
        lateinit var roundedImageView: RoundedImageView
        lateinit var discountPrice: TextView
        lateinit var introPrice: TextView
        lateinit var textView140: TextView
        lateinit var credits: TextView
        lateinit var creditApplied: TextView
        lateinit var totalPrice: TextView

        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val view = inflater.inflate(R.layout.activity_checkout_merchant, container, false)
            initz(view)
            setBackground()
            setData()
            clickListeners()

            return view
        }

        private fun clickListeners() {
            proceedCheckout.setOnClickListener {

                startActivity(
                    Intent(
                        activity,
                        CheckCustomer::class.java
                    ).putExtra("modelSubscriptionModel", arrayCampaign).putExtra(
                        "totalPriceText",
                        totalPriceText
                    )
                )
                dismiss()
            }


            tvVisits.setOnClickListener {
                var redemQuantity = ""
                redemQuantity = if (arrayCampaign.quantity == "500000") {
                    "Unlimited"
                } else {
                    arrayCampaign.quantity
                }
                Constant.CommonIAlert(
                    context!!,
                    "Your subscription includes:",
                    redemQuantity + " " + arrayCampaign.unit
                )
            }

            frequencyType.setOnClickListener {
                var frequency = ""
                frequency = when (arrayCampaign.frequency_type) {
                    "1" -> {
                        "Monthly"
                    }
                    "2" -> {
                        "Yearly"
                    }
                    "Year" -> {
                        "Yearly"
                    }
                    "Month" -> {
                        "Monthly"
                    }
                    else -> {
                        arrayCampaign.frequency_type
                    }
                }

                Constant.CommonIAlert(
                    context!!,
                    "Subscription is Billed $frequency from the purchase date",
                    ""
                )
            }


        }

        private fun setBackground() {
            tvVisits.setBackgroundResource(R.drawable.bg_blue_checkout)
            frequencyType.setBackgroundResource(R.drawable.bg_blue_checkout)
            productOffer.setBackgroundResource(R.drawable.bg_blue_off)
            proceedCheckout.setBackgroundResource(R.drawable.bg_seller_button_blue)
            actualPrice.setTextColor(resources.getColor(R.color.blue))
            productOffer.setTextColor(resources.getColor(R.color.blue))
            textFreeTrail.setTextColor(resources.getColor(R.color.blue))
            credits.visibility = View.GONE
            creditApplied.visibility = View.GONE

            if ((arrayCampaign.introductory_price == "0" || arrayCampaign.introductory_price == "0.00") && arrayCampaign.free_trial == "0" ||
                (arrayCampaign.introductory_price == "0" || arrayCampaign.introductory_price == "0.00") && arrayCampaign.free_trial == "No free trial"
            ) {
                textFreeTrail.visibility = View.GONE
            } else if (arrayCampaign.introductory_days == "0") {
                textFreeTrail.text = arrayCampaign.free_trial + " Free Trail"
            } else if (arrayCampaign.introductory_days == "1") {
                textFreeTrail.text = arrayCampaign.introductory_days + " Day Introductory Price"
            } else {
                textFreeTrail.text = arrayCampaign.introductory_days + " Days Introductory Price"
            }

            var frequency = ""
            frequency = when (arrayCampaign.frequency_type) {
                "1" -> {
                    "Monthly"
                }
                "2" -> {
                    "Yearly"
                }
                "Year" -> {
                    "Yearly"
                }
                "Month" -> {
                    "Monthly"
                }
                else -> {
                    arrayCampaign.frequency_type
                }
            }

            frequencyType.text = frequency
            tvName.text = arrayCampaign.campaign_name
            actualPrice.paintFlags = actualPrice.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
            val imagePath = Constant.getPrefs(activity!!).getString(Constant.dataImagePath, "")
            Glide.with(this).load(imagePath + arrayCampaign.campaign_image)
                .placeholder(R.mipmap.placeholder_subscription_square).into(roundedImageView)

        }

        @SuppressLint("SetTextI18n")
        private fun setData() {
            if (arrayCampaign.introductory_days != "0") {
                productOffer.visibility = View.GONE
                actualPrice.visibility = View.GONE
                textView140.visibility = View.GONE
                subTotal.visibility = View.GONE
                introPrice.visibility = View.VISIBLE
                txtIntroPrice.visibility = View.VISIBLE
                val introPrice =
                    BigDecimal(arrayCampaign.introductory_price).setScale(2, RoundingMode.HALF_UP)
                        .toString()
                if (introPrice.contains(".")) {
                    val splitProductPrice = introPrice.split(".")
                    if (splitProductPrice[1] == "00" || splitProductPrice[1] == "0") {
                        txtIntroPrice.text = "$" + splitProductPrice[0]
                        productPrice.text = splitProductPrice[0]
                        subTotalPrice.text = "$" + splitProductPrice[0]
                        totalPrice.text = "$" + splitProductPrice[0]
                        totalPriceText = "$" + splitProductPrice[0]
                    } else {
                        txtIntroPrice.text = "$$introPrice"
                        productPrice.text = introPrice
                        subTotalPrice.text = "$$introPrice"
                        totalPrice.text = "$$introPrice"
                        totalPriceText = "$$introPrice"
                    }
                } else {
                    txtIntroPrice.text = "$$introPrice"
                    productPrice.text = introPrice
                    subTotalPrice.text = "$$introPrice"
                    totalPrice.text = "$$introPrice"
                    totalPriceText = "$$introPrice"
                }
                discountPrice.text = "$0"


            } else {
                productOffer.visibility = View.VISIBLE
                actualPrice.visibility = View.GONE
                introPrice.visibility = View.GONE
                txtIntroPrice.visibility = View.GONE
                textView140.visibility = View.VISIBLE
                subTotal.visibility = View.VISIBLE
                subTotal.text = "$" + arrayCampaign.price.toDouble()
                val productPP =
                    Constant.getCalculatedPrice(arrayCampaign.discount_price, arrayCampaign.price)
                if (productPP.contains(".")) {
                    val splitProductPrice = productPP.split(".")
                    if (splitProductPrice[1] == "00" || splitProductPrice[1] == "0") {
                        productPrice.text = splitProductPrice[0]
                    } else {
                        productPrice.text =
                            BigDecimal(productPP).setScale(2, RoundingMode.HALF_UP).toString()
                    }
                } else {
                    productPrice.text = productPP
                }

                productOffer.text =
                    arrayCampaign.discount_price.toDouble().toInt().toString() + "% off"
                actualPrice.text = "$" + arrayCampaign.price.toDouble().toInt()
                val calculatedDiscount =
                    (arrayCampaign.discount_price.toDouble() / 100 * arrayCampaign.price.toDouble())
                discountPrice.text =
                    arrayCampaign.discount_price.toDouble().toInt().toString() + "%"

                val priceDis = arrayCampaign.price.toDouble() - calculatedDiscount
                val disPriceFormated = BigDecimal(priceDis).setScale(2, RoundingMode.HALF_UP)
                subTotalPrice.text = "$$disPriceFormated"
                val price = disPriceFormated.toString().split(".")
                if (price[1] == "0" || price[1] == "00") {
                    totalPrice.text = "$" + price[0]
                    totalPriceText = "$" + price[0]
                } else {
                    totalPrice.text = "$$disPriceFormated"
                    totalPriceText = "$$disPriceFormated"
                }
            }
        }

        private fun initz(view: View) {
            tvVisits = view.findViewById(R.id.tvVisits)
            introPrice = view.findViewById(R.id.introPrice)
            frequencyType = view.findViewById(R.id.frequencyType)
            proceedCheckout = view.findViewById(R.id.proceedCheckout)
            textFreeTrail = view.findViewById(R.id.textFreeTrail)
            tvName = view.findViewById(R.id.tvName)
            productPrice = view.findViewById(R.id.productPrice)
            productOffer = view.findViewById(R.id.productOffer)
            roundedImageView = view.findViewById(R.id.roundedImageView)
            actualPrice = view.findViewById(R.id.tv_last_text)
            txtIntroPrice = view.findViewById(R.id.txtIntroPrice)
            subTotal = view.findViewById(R.id.subTotal)
            discountPrice = view.findViewById(R.id.discountPrice)
            subTotalPrice = view.findViewById(R.id.subTotalPrice)
            credits = view.findViewById(R.id.credits)
            textView140 = view.findViewById(R.id.textView140)
            creditApplied = view.findViewById(R.id.creditApplied)
            totalPrice = view.findViewById(R.id.totalPrice)

        }

    }

    class MenuFragment(
        var mViewModel: SubscriptionSellerViewModel,
        var model: ModelSellerSubscriptions,
        var mAdapterSubscription: AdapterSubscriptionList
    ) : RoundedBottomSheetDialogFragment() {

        lateinit var imageView: ImageView
        lateinit var noData: TextView
        lateinit var progressBar: LinearLayout
        lateinit var recyclerViewSubscriptions: RecyclerView


        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val view = inflater.inflate(R.layout.select_subscription_sheet, container, false)
            imageView = view.findViewById(R.id.imageView)
            progressBar = view.findViewById(R.id.progressBar)
            noData = view.findViewById(R.id.noData)
            recyclerViewSubscriptions = view.findViewById(R.id.recyclerViewSubscriptions)
            roundedImage(imageView)

            recyclerViewSubscriptions.layoutManager = LinearLayoutManager(activity!!)
            recyclerViewSubscriptions.isNestedScrollingEnabled = false
            mAdapterSubscription = AdapterSubscriptionList(activity!!)
            recyclerViewSubscriptions.adapter = mAdapterSubscription

            mViewModel.getCampaign(model.campaign_id)

            progressBar.visibility = View.VISIBLE
            noData.visibility = View.GONE

            mViewModel.getmCampaignData().observe(this, Observer {
                if (it.subscription.size > 0) {
                    noData.visibility = View.GONE
                } else {
                    noData.visibility = View.VISIBLE
                }
                progressBar.visibility = View.GONE
                mAdapterSubscription.update(it)
            })
            return view
        }

        private fun roundedImage(imageView: ImageView) {
            val curveRadius = 50F

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                imageView.outlineProvider = object : ViewOutlineProvider() {

                    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                    override fun getOutline(view: View?, outline: Outline?) {
                        outline?.setRoundRect(
                            0,
                            0,
                            view!!.width,
                            view.height + curveRadius.toInt(),
                            curveRadius
                        )
                    }
                }
                imageView.clipToOutline = true
            }
        }

    }

    private fun setAdapter() {
        mBinding.recyclerViewStatus.layoutManager =
            LinearLayoutManager(activity!!, LinearLayoutManager.HORIZONTAL, false)
        mBinding.recyclerViewStatus.isNestedScrollingEnabled = false
        mAdapterSubsStatus = AdapterStatus(activity!!, this, 1)
        mBinding.recyclerViewStatus.adapter = mAdapterSubsStatus

        backgroundChange = mViewModel.getListStatus()
        mAdapterSubsStatus.update(backgroundChange)

    }

    override fun onSelectionBackground(position: Int) {
        tabPosition = position
        val auth = Constant.getPrefs(activity!!).getString(Constant.auth_code, "")
        mViewModel.getAllSubscriptionsData(auth, "")
        val model = backgroundChange[position]
        if (model.position) {
            return

        } else {
            for (notSelected in backgroundChange) {
                notSelected.position = false
            }
            model.position = true
        }

        backgroundChange[position] = model
        mAdapterSubsStatus.notifyDataSetChanged()

    }

    override fun onResume() {
        super.onResume()
        adiInitialization()
    }

    override fun list1(position: Int) {
        adiInitialization()

    }

    override fun list2(position: Int) {
        adiInitialization()


    }

    override fun inactiveBooking(
        position: Int,
        model: ModelSellerSubscriptions
    ) {
        try {
            val mPrefs = activity!!.getSharedPreferences("data", AppCompatActivity.MODE_PRIVATE)
            val prefsEditor = mPrefs.edit()
            prefsEditor.putString("detailsArray", "")
            prefsEditor.putString("pricing", "")
            prefsEditor.putString("basicData", "")
            prefsEditor.putString("campaign_id", "")
            prefsEditor.apply()

            activity!!.startActivity(
                Intent(context, CreateSubscriptionActivity::class.java)
                    .putExtra("editReactive", "add")
                    .putExtra("campaignId", model.campaign_id)
            )
        } catch (e: Exception) {
            e.printStackTrace()
        }

//        mViewModel.inactiveCampaign(model.campaign_id, "1")
//        adiInitialization()

    }
}