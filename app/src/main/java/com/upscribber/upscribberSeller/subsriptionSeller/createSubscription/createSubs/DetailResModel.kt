package com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.createSubs

import com.upscribber.filters.CategoryModel
import com.upscribber.filters.ModelSubCategory
import com.upscribber.upscribberSeller.manageTeam.ManageTeamModel
import com.upscribber.upscribberSeller.subsriptionSeller.location.searchLoc.ModelSearchLocation

class DetailResModel {
    var campaignId = ""
    var subInfo = ""
    var subLimit = "0"
    var steps = ""
    var location = ArrayList<ModelSearchLocation>()
    var teamMember = ArrayList<ManageTeamModel>()
    var tags = ArrayList<ModelSubCategory>()
    var shareable = "0"
    var check = "0"
    var subscriber = ""
    var parentTags =  ArrayList<CategoryModel>()

}