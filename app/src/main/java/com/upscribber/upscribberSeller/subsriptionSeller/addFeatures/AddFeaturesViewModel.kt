package com.upscribber.upscribberSeller.subsriptionSeller.addFeatures

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData

class AddFeaturesViewModel (application: Application) : AndroidViewModel(application) {

    var addFeaturesRepository : AddFeaturesRepository = AddFeaturesRepository(application)

    fun getFeatureList(): LiveData<ArrayList<ModelAddFeatures>> {
        return  addFeaturesRepository.getList()
    }

 fun getCreateList(): LiveData<ArrayList<ModelAddFeatures>> {
        return  addFeaturesRepository.getListCreate()
    }

}