package com.upscribber.upscribberSeller.subsriptionSeller.subscriptionMain

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.upscribber.R
import com.upscribber.upscribberSeller.navigationCustomer.InviteSubscription
import kotlinx.android.synthetic.main.activity_subscription_search_result.*

class SubscriptionSearchResultActivity : AppCompatActivity(),AdapterSellerSearchSubscriptions.reloadData{

    lateinit var mAdapter: AdapterSellerSearchSubscriptions

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_subscription_search_result)
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(this, R.color.home_free)
        setToolbar()
        setAdapter()
    }


    private fun setAdapter() {
        val arrayList = intent.getParcelableArrayListExtra<ModelSellerSubscriptions>("arraySubscriptions")
        if (arrayList.size > 0){
            imgNoSubscription.visibility = View.GONE
            tvNoData.visibility = View.GONE
            tvNoDataDescription.visibility = View.GONE
        }else{
            imgNoSubscription.visibility = View.VISIBLE
            tvNoData.visibility = View.VISIBLE
            tvNoDataDescription.visibility = View.VISIBLE
        }

        recyclerViewSubscriptions.layoutManager = LinearLayoutManager(this)
        mAdapter = AdapterSellerSearchSubscriptions(this,this)
        recyclerViewSubscriptions.adapter = mAdapter
        mAdapter.update(arrayList)

    }



    private fun setToolbar() {
        if (intent.hasExtra("subscriptionSearchQuery")){
            val query = intent.getStringExtra("subscriptionSearchQuery")
            setSupportActionBar(toolbar)
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white)
            title = ""
            toolbarTitle.text = query
        }


    }

    override fun checkoutCustomer(position: Int, model: ModelSellerSubscriptions) {
        Toast.makeText(this,"Customer Checkout Possible", Toast.LENGTH_SHORT).show()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun inactiveBooking(
        position: Int,
        model: ModelSellerSubscriptions
    ) {

    }

    override fun selectSubscription(position: Int, model: ModelSellerSubscriptions) {
        startActivity(Intent(this, InviteSubscription::class.java).putExtra("model", model))
    }
}
