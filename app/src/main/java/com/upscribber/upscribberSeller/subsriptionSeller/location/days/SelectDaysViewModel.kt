package com.upscribber.upscribberSeller.subsriptionSeller.location.days

import android.app.Application
import androidx.lifecycle.AndroidViewModel

class SelectDaysViewModel(application: Application) : AndroidViewModel(application) {

    var SelectDaysRepository : SelectDaysRepository =
        SelectDaysRepository(application)

    fun getList(): ArrayList<ModelSelectDays> {
        return  SelectDaysRepository.getList()
    }
}