package com.upscribber.upscribberSeller.subsriptionSeller.location.days

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.R
import com.upscribber.upscribberSeller.subsriptionSeller.location.searchLoc.ModelStoreLocation
import kotlinx.android.synthetic.main.location_days_select.view.*

class AdapterLocationDays(private val mContext: Context) :
    RecyclerView.Adapter<AdapterLocationDays.MyViewHolder>() {

    private var list: ArrayList<ModelStoreLocation> = ArrayList()
    var listener = mContext as adapterClick

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(mContext).inflate(R.layout.location_days_select, parent, false)
        return MyViewHolder(v)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = list[position]

        if (model.status == "true") {
            holder.itemView.editTextTime.text = "Closed"
        } else if (model.open == "1" && model.close == "1") {
            holder.itemView.editTextTime.text = ""
        } else if (model.open != "" && model.close != "") {
            holder.itemView.editTextTime.text = model.open + " - " + model.close
        } else {
            holder.itemView.editTextTime.text = ""
        }
        holder.itemView.editTextDay.text = model.day

        holder.itemView.setOnClickListener {
            listener.click(model, position)
        }


    }

    fun update(it: ArrayList<ModelStoreLocation>) {
        list = it
        notifyDataSetChanged()

    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    interface adapterClick {
        fun click(modelStoreLocation: ModelStoreLocation, position: Int)
    }

}
