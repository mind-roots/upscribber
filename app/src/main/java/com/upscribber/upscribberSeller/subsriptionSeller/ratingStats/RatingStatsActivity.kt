package com.upscribber.upscribberSeller.subsriptionSeller.ratingStats

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.upscribber.subsciptions.merchantStore.AdapterReviews
import com.upscribber.R
import com.upscribber.databinding.ActivityRatingStatsBinding

class RatingStatsActivity : AppCompatActivity(){

    lateinit var mBinding: ActivityRatingStatsBinding
    lateinit var mViewMOdel: RatingStatsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_rating_stats)
        mBinding.lifecycleOwner = this
        mViewMOdel = ViewModelProviders.of(this)[RatingStatsViewModel::class.java]
        setToolBar()
        clickListeners()
        setForRecycler()
    }

    private fun setForRecycler() {
        mViewMOdel.getReviews().observe(this, Observer {
            mBinding.includeStates.recyclerItems.layoutManager =
                LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
            mBinding.includeStates.recyclerItems.isNestedScrollingEnabled = false
            mBinding.includeStates.recyclerItems.adapter = AdapterReviews(this)
        })

        mViewMOdel.getReviewsDescription().observe(this, Observer {
            mBinding.includeStates.reviewDesc.layoutManager = LinearLayoutManager(this)
            mBinding.includeStates.reviewDesc.isNestedScrollingEnabled = false
            mBinding.includeStates.reviewDesc.adapter = AdapterReviewDescSeller(this, it)
        })



    }


    private fun setToolBar() {
        setSupportActionBar(mBinding.ListToolbar)
        title = ""
        mBinding.title.text = "Rating Stats"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white)

    }

    private fun clickListeners() {

        mBinding.includeStates.tvAll.setOnClickListener {
            mBinding.includeStates.tvAll.setBackgroundResource(R.drawable.bg_switch1)
            mBinding.includeStates.tvPositive.setBackgroundResource(R.drawable.bg_switch_fragments)
            mBinding.includeStates.tVNegative.setBackgroundResource(R.drawable.bg_switch_fragments)
            mBinding.includeStates.tvAll.setTextColor(resources.getColor(R.color.colorWhite))
            mBinding.includeStates.tvPositive.setTextColor(resources.getColor(R.color.colorCardDesc))
            mBinding.includeStates.tVNegative.setTextColor(resources.getColor(R.color.colorCardDesc))
        }

        mBinding.includeStates.tvPositive.setOnClickListener {
            mBinding.includeStates.tvPositive.setBackgroundResource(R.drawable.bg_switch1)
            mBinding.includeStates.tvAll.setBackgroundResource(R.drawable.bg_switch_fragments)
            mBinding.includeStates.tVNegative.setBackgroundResource(R.drawable.bg_switch_fragments)
            mBinding.includeStates.tvPositive.setTextColor(resources.getColor(R.color.colorWhite))
            mBinding.includeStates.tvAll.setTextColor(resources.getColor(R.color.colorCardDesc))
            mBinding.includeStates.tVNegative.setTextColor(resources.getColor(R.color.colorCardDesc))
        }

        mBinding.includeStates.tVNegative.setOnClickListener {
            mBinding.includeStates.tVNegative.setBackgroundResource(R.drawable.bg_switch1)
            mBinding.includeStates.tvAll.setBackgroundResource(R.drawable.bg_switch_fragments)
            mBinding.includeStates.tvPositive.setBackgroundResource(R.drawable.bg_switch_fragments)
            mBinding.includeStates.tVNegative.setTextColor(resources.getColor(R.color.colorWhite))
            mBinding.includeStates.tvAll.setTextColor(resources.getColor(R.color.colorCardDesc))
            mBinding.includeStates.tvPositive.setTextColor(resources.getColor(R.color.colorCardDesc))
        }


        mBinding.seven.setOnClickListener {
            mBinding.seven.setTextColor(resources.getColor(R.color.colorWhite))
            mBinding.seven.setBackgroundResource(R.drawable.bg_switch1)
            mBinding.thirty.setTextColor(resources.getColor(R.color.seller_payment_view))
            mBinding.ninety.setTextColor(resources.getColor(R.color.seller_payment_view))
            mBinding.thirty.setBackgroundResource(0)
            mBinding.ninety.setBackgroundResource(0)
            mBinding.totalRatings.text = "350"
            mBinding.positiveRatings.text = "300"
            mBinding.negativeRatings.text = "50"
            mBinding.averageRating.text = "650"
            mBinding.circularProgressbar.progress = 65
        }

        mBinding.thirty.setOnClickListener {
            mBinding.seven.setTextColor(resources.getColor(R.color.seller_payment_view))
            mBinding.seven.setBackgroundResource(0)
            mBinding.thirty.setTextColor(resources.getColor(R.color.colorWhite))
            mBinding.ninety.setTextColor(resources.getColor(R.color.seller_payment_view))
            mBinding.thirty.setBackgroundResource((R.drawable.bg_switch1))
            mBinding.ninety.setBackgroundResource(0)
            mBinding.totalRatings.text = "550"
            mBinding.positiveRatings.text = "450"
            mBinding.negativeRatings.text = "50"
            mBinding.averageRating.text = "750"
            mBinding.circularProgressbar.progress = 75

        }

        mBinding.ninety.setOnClickListener {
            mBinding.seven.setTextColor(resources.getColor(R.color.seller_payment_view))
            mBinding.seven.setBackgroundResource(0)
            mBinding.thirty.setTextColor(resources.getColor(R.color.seller_payment_view))
            mBinding.ninety.setTextColor(resources.getColor(R.color.colorWhite))
            mBinding.thirty.setBackgroundResource(0)
            mBinding.ninety.setBackgroundResource((R.drawable.bg_switch1))
            mBinding.totalRatings.text = "700"
            mBinding.positiveRatings.text = "500"
            mBinding.negativeRatings.text = "200"
            mBinding.averageRating.text = "850"
            mBinding.circularProgressbar.progress = 85
        }


    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}
