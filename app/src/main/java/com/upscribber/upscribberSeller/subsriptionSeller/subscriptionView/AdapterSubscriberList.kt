package com.upscribber.upscribberSeller.subsriptionSeller.subscriptionView

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.upscribber.upscribberSeller.customerBottom.profile.CustomerProfileInfoActivity
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.databinding.CustomerRecyclerSecondListBinding
import com.upscribber.upscribberSeller.customerBottom.customerList.ModelCustomerListt
import kotlinx.android.synthetic.main.customer_recycler_second_list.view.*

class AdapterSubscriberList(var context: Context, list : ArrayList<ModelCampaignSubscriber>) :
    RecyclerView.Adapter<AdapterSubscriberList.ViewHolder>() {

    private var itemss: ArrayList<ModelCampaignSubscriber> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(context)
            .inflate(R.layout.adapter_sub_info_list, parent, false)
        return AdapterSubscriberList.ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return itemss.size

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = itemss[position]
        val imagePath  = Constant.getPrefs(context).getString(Constant.dataImagePath, "")
        holder.itemView.textView198.text = model.name
        holder.itemView.customerNumber.text = model.contact_no
        Glide.with(context).load(imagePath + model.profile_image).placeholder(R.mipmap.circular_placeholder).into(holder.itemView.imageView)

    }


    fun update(items: ArrayList<ModelCampaignSubscriber>) {
        this.itemss = items
        notifyDataSetChanged()
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)

}