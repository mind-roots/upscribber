package com.upscribber.upscribberSeller.subsriptionSeller.subscriptionMain

import android.os.Parcel
import android.os.Parcelable

class ProductInfoModel() : Parcelable {
    var name = ""
    var price = ""
    var id = ""
    var discountPrice =""

    constructor(parcel: Parcel) : this() {
        name = parcel.readString()
        price = parcel.readString()
        id = parcel.readString()
        discountPrice = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeString(price)
        parcel.writeString(id)
        parcel.writeString(discountPrice)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ProductInfoModel> {
        override fun createFromParcel(parcel: Parcel): ProductInfoModel {
            return ProductInfoModel(parcel)
        }

        override fun newArray(size: Int): Array<ProductInfoModel?> {
            return arrayOfNulls(size)
        }
    }


}