package com.upscribber.upscribberSeller.subsriptionSeller.subscriptionMain

import android.os.Parcel
import android.os.Parcelable
import com.upscribber.filters.CategoryModel
import com.upscribber.filters.ModelSubCategory
import com.upscribber.subsciptions.merchantStore.ModelReviewDescription
import com.upscribber.subsciptions.merchantStore.ModelReviews
import com.upscribber.upscribberSeller.manageTeam.ManageTeamModel
import com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.subscription.ModelSubscriptionCard
import com.upscribber.upscribberSeller.subsriptionSeller.location.searchLoc.ModelSearchLocation

class ModelMain() : Parcelable {


    val modelReviews = ArrayList<ModelReviews>()
    val modelLatestreviews = ArrayList<ModelReviewDescription>()
    var campaign_id = ""
    var team_member = ""
    var steps = ""
    var products = ""
    var location_id = ""
    var image = ""
    var name = ""
    var feature_image = ""
    var business_id = ""
    var business_name = ""
    var campaign_info_id = ""
    var logo = ""
    var like_percentage = ""
    var views = ""
    var subscriber = ""
    var website = ""
    var business_phone = ""
    var lat = ""
    var long = ""
    var description = ""
    var tags = ""
    var limit = ""
    var campaign_start = ""
    var campaign_end = ""
    var is_public = ""
    var share_setting = ""
    var complete_percentage = ""
    var productInfo = ArrayList<ProductInfoModel>()
    var locationInfo = ArrayList<ModelSearchLocation>()
    var tagInfo = ArrayList<ModelSubCategory>()
    var parentTags = ArrayList<ModelSubCategory>()
    var teamInfo = ArrayList<ManageTeamModel>()
    var subscription = ArrayList<ModelSubscriptionCard>()
    var bought = ""
    var revenue = ""
    var redeemed = ""
    var shared = ""
    var cancelled = ""
    var status = ""
    var subs = ""
    var parentTagInfo = ArrayList<CategoryModel>()

    constructor(parcel: Parcel) : this() {
        campaign_id = parcel.readString()
        team_member = parcel.readString()
        steps = parcel.readString()
        products = parcel.readString()
        location_id = parcel.readString()
        image = parcel.readString()
        name = parcel.readString()
        feature_image = parcel.readString()
        business_id = parcel.readString()
        business_name = parcel.readString()
        campaign_info_id = parcel.readString()
        logo = parcel.readString()
        like_percentage = parcel.readString()
        views = parcel.readString()
        subscriber = parcel.readString()
        website = parcel.readString()
        business_phone = parcel.readString()
        lat = parcel.readString()
        long = parcel.readString()
        description = parcel.readString()
        tags = parcel.readString()
        limit = parcel.readString()
        campaign_start = parcel.readString()
        campaign_end = parcel.readString()
        is_public = parcel.readString()
        share_setting = parcel.readString()
        complete_percentage = parcel.readString()
        productInfo = parcel.createTypedArrayList(ProductInfoModel.CREATOR)
        locationInfo = parcel.createTypedArrayList(ModelSearchLocation.CREATOR)
        tagInfo = parcel.createTypedArrayList(ModelSubCategory.CREATOR)
        teamInfo = parcel.createTypedArrayList(ManageTeamModel.CREATOR)
        subscription = parcel.createTypedArrayList(ModelSubscriptionCard)
        bought = parcel.readString()
        revenue = parcel.readString()
        redeemed = parcel.readString()
        shared = parcel.readString()
        cancelled = parcel.readString()
        status = parcel.readString()
        subs = parcel.readString()
        parentTagInfo = parcel.createTypedArrayList(CategoryModel)

    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(campaign_id)
        parcel.writeString(team_member)
        parcel.writeString(steps)
        parcel.writeString(products)
        parcel.writeString(location_id)
        parcel.writeString(image)
        parcel.writeString(name)
        parcel.writeString(feature_image)
        parcel.writeString(business_id)
        parcel.writeString(business_name)
        parcel.writeString(campaign_info_id)
        parcel.writeString(logo)
        parcel.writeString(like_percentage)
        parcel.writeString(views)
        parcel.writeString(subscriber)
        parcel.writeString(website)
        parcel.writeString(business_phone)
        parcel.writeString(lat)
        parcel.writeString(long)
        parcel.writeString(description)
        parcel.writeString(tags)
        parcel.writeString(limit)
        parcel.writeString(campaign_start)
        parcel.writeString(campaign_end)
        parcel.writeString(is_public)
        parcel.writeString(share_setting)
        parcel.writeString(complete_percentage)

        parcel.writeTypedList(productInfo)
        parcel.writeTypedList(locationInfo)
        parcel.writeTypedList(tagInfo)
        parcel.writeTypedList(teamInfo)
        parcel.writeTypedList(subscription)
        parcel.writeString(bought)
        parcel.writeString(revenue)
        parcel.writeString(redeemed)
        parcel.writeString(shared)
        parcel.writeString(cancelled)
        parcel.writeString(status)
        parcel.writeString(subs)
        parcel.writeTypedList(parentTagInfo)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ModelMain> {
        override fun createFromParcel(parcel: Parcel): ModelMain {
            return ModelMain(parcel)
        }

        override fun newArray(size: Int): Array<ModelMain?> {
            return arrayOfNulls(size)
        }
    }


} 