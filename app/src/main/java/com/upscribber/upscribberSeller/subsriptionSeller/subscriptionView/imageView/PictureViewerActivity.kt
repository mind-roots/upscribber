package com.upscribber.upscribberSeller.subsriptionSeller.subscriptionView.imageView

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import androidx.viewpager.widget.ViewPager
import com.upscribber.R
import com.upscribber.subsciptions.merchantSubscriptionPages.ModelImagesSubscriptionDetails

class PictureViewerActivity : AppCompatActivity() {

    var viewPager: ViewPager? = null
    lateinit var imageCross: ImageView
    var subsPagerAdapter: SubsPagerAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_picture_viewer)
        initz()
        clickListener()
        subsPagerAdapter = SubsPagerAdapter(intent.getParcelableArrayListExtra<ModelImagesSubscriptionDetails>("mData"),this)
        viewPager!!.adapter = subsPagerAdapter
        viewPager!!.currentItem = intent.getIntExtra("position",0)
    }

    private fun clickListener() {
        imageCross.setOnClickListener {
            finish()
        }

    }

    private fun initz() {
        viewPager = findViewById(R.id.viewPager5)
        imageCross = findViewById(R.id.imageCross)
    }
}
