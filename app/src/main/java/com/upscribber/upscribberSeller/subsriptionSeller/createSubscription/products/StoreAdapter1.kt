package com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.products

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.upscribberSeller.store.storeMain.StoreModel
import com.upscribber.R
import com.upscribber.databinding.StoreRecyclerLayoutBinding
import kotlinx.android.synthetic.main.store_recycler_layout.view.*

class StoreAdapter1(var context: Context) : RecyclerView.Adapter<StoreAdapter1.MyViewHolder>() {

    private lateinit var binding: StoreRecyclerLayoutBinding
    private  var items = ArrayList<StoreModel>()
    var listener = context as SelectSingle

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        binding = StoreRecyclerLayoutBinding.inflate(LayoutInflater.from(context), parent,false)
        return MyViewHolder(binding)

    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = items[position]
        holder.bind(model)

        holder.itemView.setOnClickListener {
                listener.SelectProduct(position)
            }


        if (model.status){
            holder.itemView.constraint.setBackgroundResource(R.drawable.bg_seller_blue_outline)
            holder.itemView.imageTick.visibility= View.VISIBLE
        }else{
            holder.itemView.constraint.setBackgroundResource(0)
            holder.itemView.imageTick.visibility= View.GONE
        }
    }


    class MyViewHolder(var  binding: StoreRecyclerLayoutBinding): RecyclerView.ViewHolder(binding.root) {

        fun bind(obj: StoreModel) {
            binding.model = obj
            binding.executePendingBindings()
        }
    }

    fun update(items: ArrayList<StoreModel>) {
        this.items = items
        notifyDataSetChanged()
    }

    interface SelectSingle {
        fun SelectProduct(position: Int)
    }
}