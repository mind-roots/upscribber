package com.upscribber.upscribberSeller.subsriptionSeller.subscriptionView.staff

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.upscribber.R

class AddStaffRepository(application: Application) {
    fun getStaffList(): LiveData<ArrayList<AddStaffModel>> {

        val mData = MutableLiveData<ArrayList<AddStaffModel>>()
        val arrayList = ArrayList<AddStaffModel>()

        var addStaffModel = AddStaffModel()
        addStaffModel.customerName = "Mars Spa"
        addStaffModel.customerPhone = "+578454548545"
        addStaffModel.status = true
        addStaffModel.customerImage = R.mipmap.profile_image
        arrayList.add(addStaffModel)

        addStaffModel = AddStaffModel()
        addStaffModel.customerName = "Mars Spa"
        addStaffModel.customerPhone = "+578454548545"
        addStaffModel.status = false
        addStaffModel.customerImage = R.mipmap.profile_image
        arrayList.add(addStaffModel)

        addStaffModel = AddStaffModel()
        addStaffModel.customerName = "Mars Spa"
        addStaffModel.customerPhone = "+578454548545"
        addStaffModel.status = false
        addStaffModel.customerImage = R.mipmap.profile_image
        arrayList.add(addStaffModel)

        addStaffModel = AddStaffModel()
        addStaffModel.customerName = "Mars Spa"
        addStaffModel.customerPhone = "+578454548545"
        addStaffModel.status = false
        addStaffModel.customerImage = R.mipmap.profile_image
        arrayList.add(addStaffModel)

        addStaffModel = AddStaffModel()
        addStaffModel.customerName = "Mars Spa"
        addStaffModel.customerPhone = "+578454548545"
        addStaffModel.status = false
        addStaffModel.customerImage = R.mipmap.profile_image
        arrayList.add(addStaffModel)

        addStaffModel = AddStaffModel()
        addStaffModel.customerName = "Mars Spa"
        addStaffModel.customerPhone = "+578454548545"
        addStaffModel.status = false
        addStaffModel.customerImage = R.mipmap.profile_image
        arrayList.add(addStaffModel)

        addStaffModel = AddStaffModel()
        addStaffModel.customerName = "Mars Spa"
        addStaffModel.customerPhone = "+578454548545"
        addStaffModel.status = false
        addStaffModel.customerImage = R.mipmap.profile_image
        arrayList.add(addStaffModel)

        addStaffModel = AddStaffModel()
        addStaffModel.customerName = "Mars Spa"
        addStaffModel.customerPhone = "+578454548545"
        addStaffModel.status = false
        addStaffModel.customerImage = R.mipmap.profile_image
        arrayList.add(addStaffModel)

        addStaffModel = AddStaffModel()
        addStaffModel.customerName = "Mars Spa"
        addStaffModel.customerPhone = "+578454548545"
        addStaffModel.status = false
        addStaffModel.customerImage = R.mipmap.profile_image
        arrayList.add(addStaffModel)

        addStaffModel = AddStaffModel()
        addStaffModel.customerName = "Mars Spa"
        addStaffModel.customerPhone = "+578454548545"
        addStaffModel.status = false
        addStaffModel.customerImage = R.mipmap.profile_image
        arrayList.add(addStaffModel)

        addStaffModel = AddStaffModel()
        addStaffModel.customerName = "Mars Spa"
        addStaffModel.customerPhone = "+578454548545"
        addStaffModel.status = false
        addStaffModel.customerImage = R.mipmap.profile_image
        arrayList.add(addStaffModel)

        mData.value = arrayList
        return mData
    }
}