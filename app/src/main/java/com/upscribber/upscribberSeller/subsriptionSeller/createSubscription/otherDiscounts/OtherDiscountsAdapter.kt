package com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.otherDiscounts

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.databinding.CreteSubscriptionOthersDiscountRecyclerBinding

class OtherDiscountsAdapter (var context: Context) : RecyclerView.Adapter<OtherDiscountsAdapter.MyViewHolder>(){


    private lateinit var binding: CreteSubscriptionOthersDiscountRecyclerBinding
    private  var items = ArrayList<OtherDiscountsModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        binding = CreteSubscriptionOthersDiscountRecyclerBinding.inflate(LayoutInflater.from(context), parent,false)
        return MyViewHolder(binding)

    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = items[position]
        holder.bind(model)
    }

    class MyViewHolder(var  binding: CreteSubscriptionOthersDiscountRecyclerBinding): RecyclerView.ViewHolder(binding.root) {

        fun bind(obj: OtherDiscountsModel) {
            binding.list = obj
            binding.executePendingBindings()
        }
    }

    fun update(items: ArrayList<OtherDiscountsModel>) {
        this.items = items
        notifyDataSetChanged()
    }

}