package com.upscribber.upscribberSeller.subsriptionSeller.location.searchLoc

import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.location.Location
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.upscribber.commonClasses.Constant
import com.upscribber.R
import com.upscribber.filters.MapsActivity
import com.upscribber.upscribberSeller.subsriptionSeller.location.DaysLogic.DayModel
import com.upscribber.upscribberSeller.subsriptionSeller.location.DaysLogic.ModelDays
import com.upscribber.upscribberSeller.subsriptionSeller.location.DaysLogic.ModelMain
import com.upscribber.upscribberSeller.subsriptionSeller.location.DaysLogic.ResultModel
import kotlinx.android.synthetic.main.activity_location_confirmation.*
import kotlinx.android.synthetic.main.search_location_recycler_list.view.*

class LocationConfirmationActivity : AppCompatActivity(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var mMap: GoogleMap
    private lateinit var lastLocation: Location
    private lateinit var buttonDone1: TextView
    var latitude = "0"
    var longitude = "0"
    var mainList = ArrayList<ModelMain>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_location_confirmation)
        initz()
        setData()
        clickListeners()
    }

    private fun setData() {
        try {
            val modelLocation = intent.getParcelableExtra<ModelSearchLocation>("modelLocation")
            timeLogic(modelLocation)
            textView135.text = modelLocation.street + "," + modelLocation.city + "," + modelLocation.zip_code
            textView139.text = modelLocation.state
            textCity.text = modelLocation.city
            latitude = intent.getStringExtra("latitude")
            longitude = intent.getStringExtra("longitude")
            placeMarkerOnMap(LatLng(latitude.toDouble(),longitude.toDouble()))
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(latitude.toDouble(),longitude.toDouble()), 12f))

        } catch (e: Exception) {
        }


    }

    private fun timeLogic(model: ModelSearchLocation) {

       var myModel = ArrayList<ModelDays>()

        for (i in 0 until model.store_timing.size) {
            var model22 = ModelDays()
            if (i==0) {
                model22.startTime = model.store_timing[i].open
                model22.closeTime = model.store_timing[i].close
                model22.day = "Sunday"
                myModel.add(model22)
            }
            if (i==1) {
                model22.startTime = model.store_timing[i].open
                model22.closeTime = model.store_timing[i].close
                model22.day = "Monday"
                myModel.add(model22)
            }
            if (i==2) {
                model22 = ModelDays()
                model22.startTime = model.store_timing[i].open
                model22.closeTime = model.store_timing[i].close
                model22.day = "Tuesday"
                myModel.add(model22)
            }
            if (i==3) {
                model22 = ModelDays()
                model22.startTime = model.store_timing[i].open
                model22.closeTime = model.store_timing[i].close
                model22.day = "Wednesday"
                myModel.add(model22)
            }
            if (i==4) {
                model22 = ModelDays()
                model22.startTime = model.store_timing[i].open
                model22.closeTime = model.store_timing[i].close
                model22.day = "Thursday"
                myModel.add(model22)
            }
            if (i==5) {
                model22 = ModelDays()
                model22.startTime = model.store_timing[i].open
                model22.closeTime = model.store_timing[i].close
                model22.day = "Friday"
                myModel.add(model22)
            }
            if (i==6) {
                model22 = ModelDays()
                model22.startTime = model.store_timing[i].open
                model22.closeTime = model.store_timing[i].close
                model22.day = "Saturday"
                myModel.add(model22)
            }


        }
        val array2 = arrayListOf<DayModel>()
        for (i in 0 until myModel.size) {


            var model = DayModel()
            if (i ==0) {
                model.day = "Sun"
            }
            else if (i == 1) {
                model.day = "Mon"
            } else if (i == 2) {
                model.day = "Tue"
            } else if (i == 3) {
                model.day = "Wed"
            } else if (i == 4) {
                model.day = "Thu"
            } else if (i == 5) {
                model.day = "Fri"
            } else if (i == 6) {
                model.day = "Sat"
            }

            val startTime: String = myModel[i].startTime
            val endTime: String = myModel[i].closeTime
            if (startTime == endTime) {
//            if (startTime.isEmpty() &&endTime.isEmpty()) {
                model.time = "closed"
            } else {
                model.time = "$startTime to $endTime"
            }
            array2.add(model)
        }

        for (modelDay in array2) {
            if (!checkIfAlreadyExist(modelDay.day)) {
                val subList = getDayTimeString(modelDay, array2)
                mainList.add(subList)
            }
        }
        val existdays = ArrayList<ResultModel>()
        for (index in mainList.indices) {
            val resultString = mainList[index].resultString
            Log.i("$index : ", resultString.day)
            existdays.add(resultString)

        }
        var daysList = ""
        for (i in 0 until existdays.size) {
            if (existdays[i].time != "closed") {
                if (daysList.isEmpty()) {
                    daysList = "" + existdays[i].day + " (" + existdays[i].time + ")"
                } else {
                    daysList = daysList + ", " + existdays[i].day + " (" + existdays[i].time + ")"
                }
            }
        }
        textView141.text = daysList

    }

    private fun initz() {
        buttonDone1 = findViewById(R.id.buttonDone1)
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

    }
    fun checkIfAlreadyExist(day: String): Boolean {
        if (mainList.isNotEmpty()) {
            for (value in mainList) {
                val list = value.list
                for (data in list) {
                    if (data.day == day) {
                        return true
                    }
                }
            }
        }

        return false
    }
    private fun getDayTimeString(timeToFind: DayModel, list: ArrayList<DayModel>): ModelMain {
        val modelMain = ModelMain()
        val subList = ArrayList<DayModel>()
        var timeToReturn = ""
        var lastIndex = -1
        for (i in list.indices) {
            val model = list[i]
            if (timeToFind.time == model.time) {

                subList.add(model)

                if (lastIndex != -1 && i - lastIndex == 1) {

                    if (timeToReturn.contains("-") && !timeToReturn.contains(",")) {
                        timeToReturn = "${timeToFind.day}-${model.day}"
                    } else {
                        timeToReturn = "$timeToReturn-${model.day}"
                    }

                } else {
                    if (timeToReturn.isEmpty()) {
                        timeToReturn = model.day
                    } else {
                        timeToReturn = "$timeToReturn, ${model.day}"
                    }

                }

                lastIndex = i
            }
        }

        modelMain.list = subList
        modelMain.resultString.day = timeToReturn
        modelMain.resultString.time = timeToFind.time

        return modelMain
    }
    private fun clickListeners() {
        buttonDone1.setOnClickListener {
            val editor = Constant.getSharedPrefs(this).edit()
            editor.putBoolean("empty2", false)
            editor.apply()
            finish()
        }

    }

    override fun onMapReady(p0: GoogleMap?) {
        mMap = p0!!
        mMap.uiSettings.isCompassEnabled = true
        setUpMap()



    }

    private fun setUpMap() {
        if (ActivityCompat.checkSelfPermission(
                this,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION), MapsActivity.LOCATION_PERMISSION_REQUEST_CODE
            )
            return
        }
        fusedLocationClient.lastLocation.addOnSuccessListener(this) { location ->
            // Got last known location. In some rare situations this can be null.
            if (location != null) {
                lastLocation = location
                val currentLatLng = LatLng(latitude.toDouble(), longitude.toDouble())
                placeMarkerOnMap(currentLatLng)
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 12f))

            }
        }

    }

    private fun placeMarkerOnMap(currentLatLng: LatLng) {
        mMap.addMarker(
            MarkerOptions()
                .position(currentLatLng)
                .icon(bitmapDescriptorFromVector(applicationContext, R.drawable.ic_location_purple))
        )

    }

    private fun bitmapDescriptorFromVector(
        applicationContext: Context,
        ic_location_blue_icon: Int
    ): BitmapDescriptor? {
        val vectorDrawable = ContextCompat.getDrawable(applicationContext, ic_location_blue_icon)
        vectorDrawable!!.setBounds(0, 0, vectorDrawable.intrinsicWidth, vectorDrawable.intrinsicHeight)
        val bitmap =
            Bitmap.createBitmap(vectorDrawable.intrinsicWidth, vectorDrawable.intrinsicHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        vectorDrawable.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)

    }

    override fun onMarkerClick(p0: Marker?): Boolean {
        mMap.uiSettings.isZoomControlsEnabled = true
        mMap.setOnMarkerClickListener(this)
        return true
    }
}
