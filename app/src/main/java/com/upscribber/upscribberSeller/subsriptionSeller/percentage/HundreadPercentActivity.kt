package com.upscribber.upscribberSeller.subsriptionSeller.percentage

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.upscribber.upscribberSeller.subsriptionSeller.addFeatures.AddFeaturesActivity
import com.google.android.material.bottomsheet.BottomSheetBehavior
import android.util.DisplayMetrics
import android.view.WindowManager
import android.content.Context
import android.widget.LinearLayout
import com.google.gson.Gson
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.databinding.ActivityHundreadPercentBinding
import com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.createSubs.BasicModel
import com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.createSubs.CreateSubscriptionActivity
import com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.createSubs.DetailResModel
import com.upscribber.upscribberSeller.subsriptionSeller.subscriptionMain.SubscriptionSellerViewModel
import java.util.ArrayList


class HundreadPercentActivity : AppCompatActivity()
    , HeaderAdapter.UpdateTick {

    private var percentageArray: ArrayList<ModelPercentage> = ArrayList()
    private var bottomSheetBehavior: BottomSheetBehavior<LinearLayout>? = null
    lateinit var toolbar: Toolbar
    lateinit var title: TextView
    private lateinit var mbinding: ActivityHundreadPercentBinding
    private lateinit var viewModel: PercentageViewModel
    private lateinit var mAdapter: HeaderAdapter
    var shofull = 0
    private lateinit var includeLayoutProgress: LinearLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mbinding = DataBindingUtil.setContentView(this, R.layout.activity_hundread_percent)
        mbinding.lifecycleOwner = this
        viewModel = ViewModelProviders.of(this)[PercentageViewModel::class.java]
        initzz()
        setToolbar()
        setAdapter()
        clicksEvents()


    }

    override fun onResume() {
        super.onResume()
        apiInitialise()
    }

    private fun apiInitialise() {
        val campaignId = intent.getStringExtra("campaignId")
        CreateSubscriptionActivity.viewModel =
            ViewModelProviders.of(this).get(SubscriptionSellerViewModel::class.java)
        CreateSubscriptionActivity.viewModel.getCampaign(campaignId)
        includeLayoutProgress.visibility = View.VISIBLE

        CreateSubscriptionActivity.viewModel.getmCampaignData().observe(this, Observer {
            includeLayoutProgress.visibility = View.GONE
            CreateSubscriptionActivity.editData = it
            val mPrefs = application.getSharedPreferences("data", MODE_PRIVATE)
            val prefsEditor = mPrefs.edit()
            prefsEditor.putString("campaign_id", it.campaign_id)
            prefsEditor.putString("bought", it.bought)
            prefsEditor.apply()
            Log.e("it", it.toString())


            if (CreateSubscriptionActivity.editData.steps != "[]") {

                var steps = CreateSubscriptionActivity.editData.steps
                val steps1 = steps.replace("[", "")
                var steps2 = steps1.replace("]", "")
                steps2 = steps2.replace(",4", "")
                steps = steps2
                var dd = steps.split(",")

            }

            val detailResModel = DetailResModel()
            detailResModel.location = CreateSubscriptionActivity.editData.locationInfo
            detailResModel.tags = CreateSubscriptionActivity.editData.tagInfo
            detailResModel.teamMember = CreateSubscriptionActivity.editData.teamInfo
            detailResModel.shareable = CreateSubscriptionActivity.editData.share_setting
            detailResModel.check = CreateSubscriptionActivity.editData.is_public
            detailResModel.subInfo = CreateSubscriptionActivity.editData.description
            detailResModel.subLimit = CreateSubscriptionActivity.editData.limit
            detailResModel.campaignId = CreateSubscriptionActivity.editData.campaign_id
            detailResModel.steps = CreateSubscriptionActivity.editData.steps
            detailResModel.subscriber = CreateSubscriptionActivity.editData.subs


            val gson = Gson()
            val json = gson.toJson(detailResModel)
            prefsEditor.putString("detailsArray", json)
            prefsEditor.apply()

            val basicModel = BasicModel()
            basicModel.productInfo = CreateSubscriptionActivity.editData.productInfo
            basicModel.campaign_start = CreateSubscriptionActivity.editData.campaign_start
            basicModel.campaign_end = CreateSubscriptionActivity.editData.campaign_end
            basicModel.name = CreateSubscriptionActivity.editData.name
            basicModel.image = CreateSubscriptionActivity.editData.image
            basicModel.steps = CreateSubscriptionActivity.editData.steps


            val json11 = gson.toJson(basicModel)
            prefsEditor.putString("basicData", json11)

            prefsEditor.apply()


            val dataStore = CreateSubscriptionActivity.editData.subscription
            val editor1 = Constant.getSharedPrefs(this).edit()
            val gson1 = Gson()
            val json1 = gson1.toJson(dataStore)
            editor1.putString("pricing", json1)
            editor1.apply()
            percentageArray[1].status = it.productInfo.isNotEmpty()
            percentageArray[2].status = it.subscription.isNotEmpty()
            percentageArray[3].status = it.image.isNotEmpty()
            percentageArray[4].status = it.name.isNotEmpty()
            percentageArray[5].status = it.description.isNotEmpty()
            percentageArray[7].status = it.is_public.isNotEmpty()
            percentageArray[8].status = it.limit.isNotEmpty()
            percentageArray[9].status = it.locationInfo.isNotEmpty()
            percentageArray[10].status = it.tagInfo.isNotEmpty()


            mAdapter.update(percentageArray)
            mbinding.circularProgressbar.progress = it.complete_percentage.toInt()
            mbinding.textView287.text = it.complete_percentage + "%"
        })
    }

    private fun clicksEvents() {

        val displayMetrics = DisplayMetrics()
        val windowManager =
            applicationContext.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        val deviceWidth = displayMetrics.widthPixels
        val deviceHeight = displayMetrics.heightPixels

        bottomSheetBehavior = BottomSheetBehavior.from(mbinding.bottomSheet)
        (bottomSheetBehavior as BottomSheetBehavior<LinearLayout>?)!!.peekHeight =
            deviceHeight * 45 / 100

        mbinding.showHideBottomSheet.setOnClickListener {
            if (shofull == 0) {
                (bottomSheetBehavior as BottomSheetBehavior<LinearLayout>?)!!.state =
                    BottomSheetBehavior.STATE_EXPANDED
                mbinding.line.visibility = View.GONE
                mbinding.downImage.visibility = View.VISIBLE
                shofull = 1
            } else {
                shofull = 0
                mbinding.line.visibility = View.VISIBLE
                mbinding.downImage.visibility = View.GONE
                (bottomSheetBehavior as BottomSheetBehavior<LinearLayout>?)!!.state =
                    BottomSheetBehavior.STATE_COLLAPSED
            }
        }
        (bottomSheetBehavior as BottomSheetBehavior<LinearLayout>?)!!.setBottomSheetCallback(object :
            BottomSheetBehavior.BottomSheetCallback() {
            @SuppressLint("SwitchIntDef")
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                // React to state change
                when (newState) {
                    BottomSheetBehavior.STATE_COLLAPSED -> {
                        mbinding.nestedScroll.scrollTo(0, 0)
                        mbinding.line.visibility = View.VISIBLE
                        mbinding.downImage.visibility = View.GONE
                        shofull = 0
                    }

                    BottomSheetBehavior.STATE_DRAGGING -> {

                    }

                    BottomSheetBehavior.STATE_EXPANDED -> {
                        shofull = 1
                        mbinding.line.visibility = View.GONE
                        mbinding.downImage.visibility = View.VISIBLE
                    }

                    BottomSheetBehavior.STATE_HIDDEN -> {

                    }
                    BottomSheetBehavior.STATE_SETTLING -> {

                    }


                }
            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {
                // React to dragging events
                Log.e("onSlide", "onSlide")
            }
        })
        mbinding.linearAdd.setOnClickListener {

            startActivity(
                Intent(this, AddFeaturesActivity::class.java).putExtra(
                    "campaignId",
                    intent.getStringExtra("campaignId")
                )
            )
        }
    }

    private fun setAdapter() {

        mbinding.recyclerItems.layoutManager = LinearLayoutManager(this)
        mAdapter = HeaderAdapter(this)
        mbinding.recyclerItems.adapter = mAdapter
        mbinding.recyclerItems.isNestedScrollingEnabled = false
        viewModel.getProductsList().observe(this, Observer {
            percentageArray = it
        })

    }


    private fun initzz() {

        toolbar = findViewById(R.id.ListToolbar)
        title = findViewById(R.id.title)
        includeLayoutProgress = findViewById(R.id.includeLayoutProgress)

    }

    @SuppressLint("SetTextI18n")
    private fun setToolbar() {
        setSupportActionBar(toolbar)
        title.text = "Complete Your Listing"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white)

    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun updateTick(position: Int, cc: Int) {
//        viewModel.updateList(position).observe(this, Observer {
//            //  mAdapter.update(it)
//            (bottomSheetBehavior as BottomSheetBehavior<LinearLayout>?)!!.state = BottomSheetBehavior.STATE_COLLAPSED
//            if (cc <= 10) {
//                mbinding.circularProgressbar.progress = (cc) * 10
//                mbinding.textView287.text = ((cc) * 10).toString() + "%"
//            }
//        })

    }
}
