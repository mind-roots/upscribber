package com.upscribber.upscribberSeller.subsriptionSeller.subscriptionMain.customerCheckout

import android.app.Activity
import android.content.Intent
import android.graphics.Outline
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.*
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.commonClasses.RoundedBottomSheetDialogFragment
import com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.subscription.ModelSubscriptionCard
import com.upscribber.upscribberSeller.subsriptionSeller.subscriptionMain.ModelSellerSubscriptions
import com.upscribber.upscribberSeller.subsriptionSeller.subscriptionMain.SubscriptionSellerViewModel
import kotlinx.android.synthetic.main.activity_check_customer.*
import kotlinx.android.synthetic.main.toolbar.view.*

class CheckCustomer : AppCompatActivity() {

    lateinit var mViewModel: SubscriptionSellerViewModel


    companion object {
        var planListData: ArrayList<ModelSubscriptionCard> = ArrayList()
        lateinit var fragmentMenu : MenuFragmentSubscriptionChoose
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_check_customer)
        mViewModel = ViewModelProviders.of(this)[SubscriptionSellerViewModel::class.java]
        setToolbar()
        clickListeners()
    }

    private fun clickListeners() {

        val modelSubscription =
            intent.getParcelableExtra<ModelSellerSubscriptions>("modelSubscriptionModel")

        layoutExisting.setOnClickListener {
             fragmentMenu = MenuFragmentSubscriptionChoose("1", mViewModel, modelSubscription,this)
            fragmentMenu.show(supportFragmentManager, fragmentMenu.tag)

        }


        constraintLayout18.setOnClickListener {
            fragmentMenu = MenuFragmentSubscriptionChoose(
                "0",
                mViewModel,
                modelSubscription,
                this
            )
            fragmentMenu.show(supportFragmentManager, fragmentMenu.tag)

        }

    }

    class MenuFragmentSubscriptionChoose(
        var type: String,
        var mViewModel: SubscriptionSellerViewModel,
        var modelSubscription: ModelSellerSubscriptions,
        var activity: Activity
    ) : RoundedBottomSheetDialogFragment(), AdapterSubscriptionPlanCheckOut.GoNextScreenBuyButton {


        lateinit var rvRedeem: RecyclerView
        lateinit var cl_redeem: ConstraintLayout
        lateinit var image: ImageView
        lateinit var progressBar: LinearLayout
        lateinit var mAdapterSubscription: AdapterSubscriptionPlanCheckOut

        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val view = inflater.inflate(R.layout.subscription_list_merchant, container, false)
            rvRedeem = view.findViewById(R.id.rvRedeem)
            image = view.findViewById(R.id.topBg)
            cl_redeem = view.findViewById(R.id.cl_redeem)
            progressBar = view.findViewById(R.id.progressBar)
            rvRedeem.layoutManager = LinearLayoutManager(activity)
            rvRedeem.isNestedScrollingEnabled = false
            mAdapterSubscription = AdapterSubscriptionPlanCheckOut(activity, this)
            rvRedeem.adapter = mAdapterSubscription
            roundedImage()
            apiImplimentation()
            observerInit()
            return view
        }


        private fun observerInit() {
            mViewModel.getmCampaignData().observe(this, Observer {
                progressBar.visibility = View.GONE
                if (it.subscription.size > 0) {
                    planListData = it.subscription
                    mAdapterSubscription.update(planListData)
                }

            })

            mViewModel.getStatus().observe(this, Observer {
                if (it.msg == "Invalid auth code") {
                    Constant.commonAlert(activity)
                } else {
                    Toast.makeText(activity, it.msg, Toast.LENGTH_SHORT).show()
                }
            })

        }


        private fun apiImplimentation() {
            val auth = Constant.getPrefs(activity).getString(Constant.auth_code, "")
            progressBar.visibility = View.VISIBLE
            mViewModel.getCampaign(modelSubscription.campaign_id)
        }


        private fun roundedImage() {
            val curveRadius = 50F
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                image.outlineProvider = object : ViewOutlineProvider() {
                    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                    override fun getOutline(view: View?, outline: Outline?) {
                        outline?.setRoundRect(
                            0,
                            0,
                            view!!.width,
                            view.height + curveRadius.toInt(),
                            curveRadius
                        )
                    }
                }
                image.clipToOutline = true
            }
        }

        override fun nextClicks(model: ModelSubscriptionCard, position: Int) {
            val model1 = planListData[position]
            if (model1.isSelected) {
                model.isSelected = false
            } else {
                for (child in planListData) {
                    child.isSelected = false
                }
                model.isSelected = true
                fragmentMenu.isCancelable = false
            }

            planListData[position] = model
            mAdapterSubscription.update(planListData)

            Handler().postDelayed({
                if (model.isSelected) {
                    fragmentMenu.isCancelable = true
                    if (type == "0") {
                        val new_user1 = "0"
                        Constant.cardArrayListDataMerchant.clear()
                        Constant.cardArrayListData.clear()
                        startActivity(
                            Intent(activity, CheckoutMerchantSignUp::class.java).putExtra("modelSubscriptionModel", modelSubscription)
                                .putExtra("totalPriceText", activity.intent.getStringExtra("totalPriceText")))

                        val editor = Constant.getPrefs(activity).edit()
                        editor.putString(Constant.new_user, new_user1)
                        editor.apply()


                    } else {
                        val new_user = "1"
                        Constant.cardArrayListDataMerchant.clear()
                        Constant.cardArrayListData.clear()
                        startActivity(
                            Intent(activity, ScanningExistingCustomer::class.java).putExtra("modelSubscriptionModel", modelSubscription)
                                .putExtra("totalPriceText", activity.intent.getStringExtra("totalPriceText")))

                        val editor = Constant.getPrefs(activity).edit()
                        editor.putString(Constant.new_user, new_user)
                        editor.apply()
                    }
                    dismiss()
                }

            }, 500)
        }


    }

    private fun setToolbar() {
        setSupportActionBar(toolbar.toolbar)
        title = ""
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }


}
