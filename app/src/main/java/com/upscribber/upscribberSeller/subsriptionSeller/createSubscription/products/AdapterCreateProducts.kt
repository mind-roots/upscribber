package com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.products

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.databinding.CreatesubsProductsRecyclerLayoutBinding
import com.upscribber.upscribberSeller.store.storeMain.StoreModel
import kotlinx.android.synthetic.main.createsubs_products_recycler_layout.view.*

class AdapterCreateProducts(var context: Context) : RecyclerView.Adapter<AdapterCreateProducts.MyViewHolder>() {

    private lateinit var binding: CreatesubsProductsRecyclerLayoutBinding
    private  var items = ArrayList<StoreModel>()
    var listener = context as Delete

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        binding = CreatesubsProductsRecyclerLayoutBinding.inflate(LayoutInflater.from(context), parent,false)
        return MyViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = items[position]
        holder.bind(model)

        holder.itemView.imgCrossDelete.setOnClickListener {
            val mPrefs = context.getSharedPreferences("data", AppCompatActivity.MODE_PRIVATE)
            var boughtIsMore = mPrefs.getString("bought", "0")
            if (boughtIsMore.isEmpty()) {
                boughtIsMore = "0"
            }
            if (boughtIsMore.toInt() == 0) {
                listener.deleteAddedProduct(position)
            }

        }
    }

    class MyViewHolder(var  binding: CreatesubsProductsRecyclerLayoutBinding): RecyclerView.ViewHolder(binding.root) {

        fun bind(obj: StoreModel) {
            binding.model = obj
            binding.executePendingBindings()
        }
    }

    fun update(items: ArrayList<StoreModel>) {
        this.items = items
        notifyDataSetChanged()
    }

    interface Delete{
        fun deleteAddedProduct(position: Int)

    }
}