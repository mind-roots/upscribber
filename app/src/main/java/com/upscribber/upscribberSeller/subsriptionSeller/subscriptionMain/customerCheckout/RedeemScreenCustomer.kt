package com.upscribber.upscribberSeller.subsriptionSeller.subscriptionMain.customerCheckout

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.upscribber.R
import com.upscribber.checkout.ModelCheckoutOne
import com.upscribber.commonClasses.Constant
import com.upscribber.payment.paymentCards.AddCardActivity
import com.upscribber.payment.paymentCards.AddCardViewModel
import com.upscribber.payment.paymentCards.PaymentCardModel
import com.upscribber.profile.ModelGetProfile
import com.upscribber.profile.ProfileFragment.Companion.mViewModel
import com.upscribber.upscribberSeller.customerBottom.scanning.ScanningViewModel
import com.upscribber.upscribberSeller.subsriptionSeller.subscriptionMain.ModelSellerSubscriptions
import kotlinx.android.synthetic.main.activity_redeem_customer_scan.*

class RedeemScreenCustomer : AppCompatActivity() {

    lateinit var manageDone : TextView
    lateinit var mViewModel : ScanningViewModel
    var flag = 0
    var modelProfile = ModelGetProfile()
    var modelCheckout = ModelCheckoutOne()
    lateinit var mViewModel1: AddCardViewModel
    var arrayCard = ArrayList<PaymentCardModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_redeem_customer_scan)
        mViewModel = ViewModelProviders.of(this)[ScanningViewModel::class.java]
        mViewModel1 = ViewModelProviders.of(this)[AddCardViewModel::class.java]
        initz()
        setToolbar()
        clickListener()
        observerInit()

    }


    private fun observerInit() {
        mViewModel.getmDataStatus().observe(this, Observer {
            progressbar.visibility = View.GONE
            if (it.msg == "Invalid auth code") {
                Constant.commonAlert(this)
            } else {
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
            }
        })

        mViewModel1.getmRetrieveCard().observe(this, androidx.lifecycle.Observer {
            arrayCard = it
            if (modelProfile.status == "true") {
                progressbar.visibility = View.GONE
                if (modelProfile.name.isEmpty() || modelProfile.contact_no.isEmpty() || modelProfile.email.isEmpty() || modelProfile.primary_address.street.isEmpty()
                    || modelProfile.primary_address.city.isEmpty() || modelProfile.primary_address.state.isEmpty() ||modelProfile.primary_address.zip_code.isEmpty()){
                    startActivity(Intent(this,CheckoutMerchantSignUp::class.java)
                        .putExtra("totalPriceText",intent.getStringExtra("totalPriceText"))
                        .putExtra("modelPro",modelProfile)
                        .putExtra("modelSubscriptionModel", intent.getParcelableExtra<ModelSellerSubscriptions>("modelSubscriptionModel"))
                    )
                }else if (arrayCard.size > 0){
                    modelCheckout.billingType = ""
                    modelCheckout.streetName = modelProfile.primary_address.street
                    modelCheckout.city = modelProfile.primary_address.city
                    modelCheckout.stateName = modelProfile.primary_address.state
                    modelCheckout.zipCode = modelProfile.primary_address.zip_code
                    modelCheckout.suite = modelProfile.primary_address.suite
                    modelCheckout.billingStreet = modelProfile.billing_address.billing_street
                    modelCheckout.billingCity = modelProfile.billing_address.billing_city
                    modelCheckout.billingState = modelProfile.billing_address.billing_state
                    modelCheckout.billingZip = modelProfile.billing_address.billing_zipcode
                    modelCheckout.billingSuite = modelProfile.billing_address.billing_suite
                    modelCheckout.firstName = modelProfile.name
                    modelCheckout.emailAddress = modelProfile.email
                    modelCheckout.phoneNumber = modelProfile.contact_no
                    modelCheckout.credits = modelProfile.wallet

                    startActivity(
                        Intent(this, CompleteCheckOutMerchant::class.java)
                            .putExtra("modelSubscriptionModel", intent.getParcelableExtra<ModelSellerSubscriptions>("modelSubscriptionModel"))
                            .putExtra("totalPriceText",intent.getStringExtra("totalPriceText"))
                            .putExtra("modelProfile",modelProfile)
                            .putExtra("modelCheckoutMerchant",modelCheckout)
                    )
                }else{
                    modelCheckout.billingType = ""
                    modelCheckout.streetName = modelProfile.primary_address.street
                    modelCheckout.city = modelProfile.primary_address.city
                    modelCheckout.stateName = modelProfile.primary_address.state
                    modelCheckout.zipCode = modelProfile.primary_address.zip_code
                    modelCheckout.suite = modelProfile.primary_address.suite
                    modelCheckout.billingStreet = modelProfile.billing_address.billing_street
                    modelCheckout.billingCity = modelProfile.billing_address.billing_city
                    modelCheckout.billingState = modelProfile.billing_address.billing_state
                    modelCheckout.billingZip = modelProfile.billing_address.billing_zipcode
                    modelCheckout.billingSuite = modelProfile.billing_address.billing_suite
                    modelCheckout.firstName = modelProfile.name
                    modelCheckout.emailAddress = modelProfile.email
                    modelCheckout.phoneNumber = modelProfile.contact_no
                    modelCheckout.credits = modelProfile.wallet

                    startActivity(Intent(this, AddCardActivity::class.java).putExtra("merchantCheckOut", "merchantCheckout")
                        .putExtra("modelSubscriptionModel", intent.getParcelableExtra<ModelSellerSubscriptions>("modelSubscriptionModel"))
                        .putExtra("modelProfile",modelProfile)
                        .putExtra("totalPriceText",intent.getStringExtra("totalPriceText"))
                        .putExtra("modelCheckoutMerchant", modelCheckout))
                }
            }
        })

        mViewModel.getmDataRedeemCodeData().observe(this, Observer {
            modelProfile = it
            mViewModel1.getRetrieveCards(modelProfile.stripe_customer_id)

        })

    }

    private fun initz() {
        manageDone = findViewById(R.id.manageDone)

    }

    private fun clickListener() {
        manageDone.setOnClickListener {
            if (flag == 1) {
                progressbar.visibility = View.VISIBLE
                Constant.hideKeyboard(this,code)
                mViewModel.getRedeemCustomer(code.text.toString().trim())

            } else {
                Toast.makeText(this, "Please enter valid 8 digit invite code", Toast.LENGTH_SHORT).show()
            }
        }

        code.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

                if (s.length == 8) {
                    done.visibility = View.VISIBLE
                    flag = 1
                } else {
                    done.visibility = View.GONE
                    flag = 0
                }
            }

            override fun afterTextChanged(s: Editable) {

            }
        })

    }

    private fun setToolbar() {
        setSupportActionBar(toolbar)
        title = ""
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

}
