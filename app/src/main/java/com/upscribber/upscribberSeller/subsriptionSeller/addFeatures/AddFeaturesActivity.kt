package com.upscribber.upscribberSeller.subsriptionSeller.addFeatures

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.databinding.ActivityAddFeaturesBinding
import com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.createSubs.BasicModel
import com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.createSubs.CreateSubscriptionActivity
import com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.createSubs.DetailResModel
import com.upscribber.upscribberSeller.subsriptionSeller.subscriptionMain.ModelMain
import com.upscribber.upscribberSeller.subsriptionSeller.subscriptionMain.SubscriptionSellerViewModel

class AddFeaturesActivity : AppCompatActivity() , AdapterAddFeatures.ChangeImages{

    lateinit var toolbar: Toolbar
    lateinit var toolbarTitle: TextView
    private lateinit var binding: ActivityAddFeaturesBinding
    private lateinit var viewModel1: AddFeaturesViewModel
    private lateinit var mAdapter: AdapterAddFeatures
   var editData = ModelMain()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_features)
        binding.lifecycleOwner = this
        inittz()
        setToolbar()
        viewModel1 = ViewModelProviders.of(this)[AddFeaturesViewModel::class.java]

    }

    override fun onResume() {
        super.onResume()
        val campaignId = intent.getStringExtra("campaignId")
        CreateSubscriptionActivity.viewModel = ViewModelProviders.of(this).get(SubscriptionSellerViewModel::class.java)
        CreateSubscriptionActivity.viewModel.getCampaign(campaignId)
        CreateSubscriptionActivity.viewModel.getmCampaignData().observe(this, Observer {

            editData = it
            setAdapter()
            val mPrefs = application.getSharedPreferences("data", MODE_PRIVATE)
            val prefsEditor = mPrefs.edit()
            prefsEditor.putString("campaign_id", it.campaign_id)
            prefsEditor.putString("bought", it.bought)
            prefsEditor.apply()
            Log.e("it", it.toString())


            if (editData.steps != "[]") {

                var steps = editData.steps
                val steps1 = steps.replace("[", "")
                var steps2 = steps1.replace("]", "")
                steps2=steps2.replace(",4","")
                steps = steps2
                var dd = steps.split(",")

            }

            val detailResModel = DetailResModel()
            detailResModel.location = editData.locationInfo
            detailResModel.tags = editData.tagInfo
            detailResModel.teamMember = editData.teamInfo
            detailResModel.shareable = editData.share_setting
            detailResModel.check = editData.is_public
            detailResModel.subInfo = editData.description
            detailResModel.subLimit = editData.limit
            detailResModel.campaignId = editData.campaign_id
            detailResModel.steps = editData.steps


            val gson = Gson()
            val json = gson.toJson(detailResModel)
            prefsEditor.putString("detailsArray", json)
            prefsEditor.apply()

            val basicModel = BasicModel()
            basicModel.productInfo = editData.productInfo
            basicModel.campaign_start = editData.campaign_start
            basicModel.campaign_end = editData.campaign_end
            basicModel.name = editData.name
            basicModel.image = editData.image
            basicModel.steps = editData.steps


            val json11 = gson.toJson(basicModel)
            prefsEditor.putString("basicData", json11)

            prefsEditor.apply()


            val dataStore = editData.subscription
            val editor1 = Constant.getSharedPrefs(this).edit()
            val gson1 = Gson()
            val json1 = gson1.toJson(dataStore)
            editor1.putString("pricing", json1)
            editor1.apply()

        })

    }
    private fun setAdapter() {

        binding.recyclerAddFeatures.layoutManager =  LinearLayoutManager(this)
        mAdapter = AdapterAddFeatures(this,1)
        binding.recyclerAddFeatures.adapter = mAdapter

        viewModel1.getFeatureList().observe(this, Observer {
            if (editData.subscription.size>0) {
                if (editData.subscription[0].freeTrail != "No free trial") {
                    it[0].status = editData.subscription[0].freeTrail.isNotEmpty()
                } else {
                    it[0].status = false
                }
                if (editData.subscription[0].introDays != "0" && editData.subscription[0].introPrice != "0") {
                    it[1].status =
                        editData.subscription[0].introDays.isNotEmpty() && editData.subscription[0].introPrice.isNotEmpty()
                } else {
                    it[1].status = false
                }
            }
            mAdapter.update(it)
            if (it[0].status||it[1].status){
                binding.imageTop.setImageDrawable(resources.getDrawable(R.drawable.ic_legendary_top))
                binding.textView244.text = resources.getString(R.string.add_feature4)
                binding.imageView32.setImageDrawable(resources.getDrawable(R.drawable.ic_legendary))
                mAdapter.selection(0)
                mAdapter.selection(1)
            }else{
                binding.imageTop.setImageDrawable(resources.getDrawable(R.drawable.ic_basic_top))
                binding.textView244.text = resources.getString(R.string.add_feature1)
                binding.imageView32.setImageDrawable(resources.getDrawable(R.drawable.ic_basic))

            }
        })
    }

    override fun callActivity(position: Int) {
//        if (position == 0){
//            binding.imageTop.setImageDrawable(resources.getDrawable(R.drawable.ic_basic_top))
//            binding.textView244.text = resources.getString(R.string.add_feature1)
//            binding.imageView32.setImageDrawable(resources.getDrawable(R.drawable.ic_basic))
//
//        }else if (position == 1){
//            binding.imageTop.setImageDrawable(resources.getDrawable(R.drawable.ic_awesome_top))
//            binding.textView244.text = resources.getString(R.string.add_feature3)
//            binding.imageView32.setImageDrawable(resources.getDrawable(R.drawable.ic_awesome))
//        }else{
//            binding.imageTop.setImageDrawable(resources.getDrawable(R.drawable.ic_legendary_top))
//            binding.textView244.text = resources.getString(R.string.add_feature4)
//            binding.imageView32.setImageDrawable(resources.getDrawable(R.drawable.ic_legendary))
//        }



    }

    private fun inittz() {

        toolbar = findViewById(R.id.toolbarFeatures)
        toolbarTitle = findViewById(R.id.title)
    }

    private fun setToolbar() {
        setSupportActionBar(toolbar)
        title = ""
        toolbarTitle.text="Add Extras"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}
