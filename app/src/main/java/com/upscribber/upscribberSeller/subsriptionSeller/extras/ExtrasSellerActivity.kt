package com.upscribber.upscribberSeller.subsriptionSeller.extras

import android.annotation.SuppressLint
import android.graphics.Outline
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.*
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.ActionBar
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.upscribber.commonClasses.Constant
import com.upscribber.commonClasses.RoundedBottomSheetDialogFragment
import com.upscribber.R
import com.upscribber.commonClasses.Selectionn
import com.upscribber.databinding.ActivityExtrasSellerBinding

class ExtrasSellerActivity : AppCompatActivity(), AdapterExtra.EditItems, Selectionn {

    lateinit var mBinding: ActivityExtrasSellerBinding
    lateinit var viewModel: ViewModelExtra

    private lateinit var backgroundChange: ArrayList<ModelExtra>
    lateinit var adapterExtra: AdapterExtra


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_extras_seller)
        viewModel = ViewModelProviders.of(this)[ViewModelExtra::class.java]
        setToolBar()
        setClicks()
        mBinding.extraData.layoutManager = LinearLayoutManager(this)
        mBinding.extraData.itemAnimator = DefaultItemAnimator()
        mBinding.extraData.isNestedScrollingEnabled = false
    }

    override fun onResume() {
        super.onResume()
        if (intent.hasExtra("subscriptionInfoExtras")) {
            mBinding.dummyText.visibility = View.GONE
            mBinding.card.visibility = View.GONE
            mBinding.done.visibility = View.VISIBLE
            mBinding.extraData.visibility = View.VISIBLE
            var vv = ""
            if (intent.hasExtra("subscriptionInfoExtras")) {
                vv = intent.getStringExtra("subscriptionInfoExtras")
            }
            adapterExtra = AdapterExtra(this, vv)
            mBinding.extraData.adapter = adapterExtra
            backgroundChange = viewModel.getPlanValues() as ArrayList<ModelExtra>
            adapterExtra.update(backgroundChange)
            roundedImage()
        }
        setAdapter()


    }

    private fun setAdapter() {
        if (Constant.getSharedPrefs(this).getBoolean("empty", true)) {
            mBinding.dummyText.visibility = View.VISIBLE
            mBinding.card.visibility = View.VISIBLE
            mBinding.done.visibility = View.GONE
            mBinding.extraData.visibility = View.GONE

        }else{
            mBinding.dummyText.visibility = View.GONE
            mBinding.card.visibility = View.GONE
            mBinding.done.visibility = View.VISIBLE
            mBinding.extraData.visibility = View.VISIBLE
            var vv = ""
            if (intent.hasExtra("subscriptionInfoExtras")) {
                vv = intent.getStringExtra("subscriptionInfoExtras")
            }
            adapterExtra = AdapterExtra(this, vv)
            mBinding.extraData.adapter = adapterExtra
            backgroundChange = viewModel.getPlanValues() as ArrayList<ModelExtra>
            adapterExtra.update(backgroundChange)
            roundedImage()


        }
    }

    private fun roundedImage() {
        val curveRadius = 50F

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            mBinding.constraintBackground.outlineProvider = object : ViewOutlineProvider() {

                @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                override fun getOutline(view: View?, outline: Outline?) {
                    outline?.setRoundRect(
                        0,
                        -100,
                        view!!.width,
                        view.height,
                        curveRadius
                    )
                }
            }
            mBinding.constraintBackground.clipToOutline = true
        }
    }


    private fun setClicks() {
        mBinding.addSubscription.setOnClickListener {
            val fragment = MenuFragment(viewModel, this, adapterExtra, -1, "add", ModelExtra())
            fragment.show(supportFragmentManager, fragment.tag)
        }
        mBinding.done.setOnClickListener {
            val fragment = MenuFragment(viewModel, this, adapterExtra, -1, "add", ModelExtra())
            fragment.show(supportFragmentManager, fragment.tag)
        }
//        mBinding.done.setOnClickListener {
//
//            var mmm: ArrayList<ModelExtra> = ArrayList()
//            for(i in 0 until backgroundChange.size){
//                if (backgroundChange[i].isSelected){
//                    mmm.add(backgroundChange[i])
//                }
//            }
//            if (mmm.size>0) {
//                var intent = Intent(this, CreateSubscriptionExtraActivity::class.java)
//                intent.putExtra("model", mmm)
//                setResult(Activity.RESULT_OK, intent)
//                finish()
////                update1.setSelectedBussines(selectedModel!!)
//            } else {
////                Toast.makeText(this, "Select first", Toast.LENGTH_LONG).show()
//            }
//            viewModel.getArray().observe(this, Observer {
//                val intent = Intent(this, ActivitySubscriptionSeller::class.java)
//                intent.putParcelableArrayListExtra("data", it)
//                setResult(20, intent)
//            })
//            finish()
//        }

        }

    private fun setToolBar() {
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(this, R.color.home_free)

        val toolbar = findViewById<Toolbar>(R.id.CustomerListToolbar)
        setSupportActionBar(toolbar)

        val actionbar: ActionBar = this.supportActionBar!!
        actionbar.apply {
            setDisplayHomeAsUpEnabled(true)
            setHomeAsUpIndicator(R.drawable.ic_arrow_back_white)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    class MenuFragment(
        var viewModel: ViewModelExtra,
        var extrasSellerActivity: ExtrasSellerActivity,
        var adapterExtra: AdapterExtra,
        var i: Int,
        var from: String,
        var model: ModelExtra
    ) : RoundedBottomSheetDialogFragment() {

        lateinit var imageViewTop: ImageView
        lateinit var addValue: TextView
        lateinit var heading: TextView
        lateinit var price: EditText
        lateinit var name: EditText
        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
            val view: View = inflater.inflate(R.layout.add_extra_view, container, false)
            return view
        }

        @SuppressLint("SetTextI18n")
        override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
            super.onViewCreated(view, savedInstanceState)
            heading = view.findViewById(R.id.heading)
            name = view.findViewById(R.id.editTextMemberName)
            price = view.findViewById(R.id.editTextPrice)
           imageViewTop = view.findViewById(R.id.imageViewTop1)
            addValue = view.findViewById(R.id.addValue)

            if (from.equals("edit")) {
                addValue.text = "Done"
                heading.text = "Edit Extras"
                name.setText(model.name)
                price.setText(model.price.replace("$", ""))
            } else {
                addValue.text = "Add"
                heading.text = "Add Extras"
            }
            addValue.setOnClickListener {
                val editor = Constant.getSharedPrefs(context!!).edit()
                editor.putBoolean("empty", false)
                editor.apply()
                val model = ModelExtra()
                model.name = name.text.toString()
                model.price = "$" + price.text.toString()
                if (model.name.trim().isEmpty()) {
                    Toast.makeText(extrasSellerActivity, "Enter Name", Toast.LENGTH_LONG).show()
                    return@setOnClickListener
                } else if (model.price.trim().isEmpty()) {
                    Toast.makeText(extrasSellerActivity, "Enter Price", Toast.LENGTH_LONG).show()
                    return@setOnClickListener
                }
                if (from == "edit") {
                    viewModel.edit(model, i).observe(this, Observer {
                        adapterExtra.update(it)
                    })
                } else {

                    viewModel.updateArray(model).observe(this, Observer {
                        adapterExtra.update(it)
                    })
                }
                dismiss()
            }
            roundedImage()
        }

        private fun roundedImage() {
            val curveRadius = 50F

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                imageViewTop.outlineProvider = object : ViewOutlineProvider() {
                    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                    override fun getOutline(view: View?, outline: Outline?) {
                        outline?.setRoundRect(
                            0,
                            0,
                            view!!.width,
                            view.height + curveRadius.toInt(),
                            curveRadius
                        )
                    }
                }
                imageViewTop.clipToOutline = true
            }
        }

    }

    override fun editItems(
        position: Int,
        model: ModelExtra
    ) {

        val fragment = MenuFragment(viewModel, this, adapterExtra, position, "edit", model)
        fragment.show(supportFragmentManager, fragment.tag)
    }

    override fun onSelectionBackgroundd(position: Int) {
        val model = backgroundChange[position]
        model.isSelected = !model.isSelected

        backgroundChange[position] = model
        adapterExtra.notifyDataSetChanged()
    }
}
