package com.upscribber.upscribberSeller.subsriptionSeller.subscriptionMain

import android.app.Application
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.upscribber.commonClasses.Constant
import com.upscribber.commonClasses.ModelStatusMsg
import com.upscribber.commonClasses.WebServicesCustomers
import com.upscribber.commonClasses.WebServicesMerchants
import com.upscribber.filters.CategoryModel
import com.upscribber.filters.ModelSubCategory
import com.upscribber.subsciptions.manageSubscriptions.shareSubscription.ModelShareRedeem
import com.upscribber.subsciptions.merchantStore.ModelReviewDescription
import com.upscribber.subsciptions.merchantStore.ModelReviews
import com.upscribber.upscribberSeller.customerBottom.profile.subscriptionList.SubscriptionListModel
import com.upscribber.upscribberSeller.manageTeam.ManageTeamModel
import com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.subscription.ModelSubscriptionCard
import com.upscribber.upscribberSeller.subsriptionSeller.location.searchLoc.ModelSearchLocation
import com.upscribber.upscribberSeller.subsriptionSeller.location.searchLoc.ModelStoreLocation
import com.upscribber.upscribberSeller.subsriptionSeller.subscriptionView.ModelCampaignSubscriber
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class SubscriptionSellerRepository(var application: Application) {

    val mDataCampaigns = MutableLiveData<ArrayList<ModelSellerSubscriptions>>()
    val mDataCampaignsActiveOnly = MutableLiveData<ArrayList<ModelSellerSubscriptions>>()
    val mDataFailure = MutableLiveData<ModelStatusMsg>()
    val mDataDeleteCampaign = MutableLiveData<ModelStatusMsg>()
    val mDataSuccess = MutableLiveData<ModelStatusMsg>()
    val mDataCampaignInfo = MutableLiveData<ModelMain>()
    val mDataCampaignFailure = MutableLiveData<ModelStatusMsg>()
    val mDataInviteSubList = MutableLiveData<ArrayList<ModelShareRedeem>>()
    val mDataSubscriptionList = MutableLiveData<ArrayList<SubscriptionListModel>>()
    val mDataActiveInactive = MutableLiveData<ModelSubscriptionCard>()
    val mDataCampaignSubscriber = MutableLiveData<ArrayList<ModelCampaignSubscriber>>()


    fun getStatusList(): ArrayList<ModelStatus> {

//        val mData = MutableLiveData<ArrayList<ModelStatus>>()
        val arrayList = ArrayList<ModelStatus>()

        var modelStatus = ModelStatus()
        modelStatus.status = "Marketplace"
        modelStatus.position = true
        arrayList.add(modelStatus)

        modelStatus = ModelStatus()
        modelStatus.status = "Inactive"
        modelStatus.position = false
        arrayList.add(modelStatus)

        modelStatus = ModelStatus()
        modelStatus.status = "Private"
        modelStatus.position = false
        arrayList.add(modelStatus)

//        mData.value = arrayList
        return arrayList
    }

    fun getAllSubscriptionsData(auth: String, query: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.getCampaigns(auth, "1", query)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        val arrayListCampaigns = ArrayList<ModelSellerSubscriptions>()
                        if (status == "true") {
                            val data = json.optJSONArray("data")
                            for (i in 0 until data.length()) {
                                val subscriptionData = data.getJSONObject(i)
                                val modelSellerSubscriptions = ModelSellerSubscriptions()
                                modelSellerSubscriptions.subscription_id =
                                    subscriptionData.optString("subscription_id")
                                modelSellerSubscriptions.campaign_id =
                                    subscriptionData.optString("campaign_id")
                                modelSellerSubscriptions.price = subscriptionData.optString("price")
                                modelSellerSubscriptions.discount_price =
                                    subscriptionData.optString("discount_price")
                                modelSellerSubscriptions.unit = subscriptionData.optString("unit")
                                modelSellerSubscriptions.created_at =
                                    subscriptionData.optString("created_at")
                                modelSellerSubscriptions.updated_at =
                                    subscriptionData.optString("updated_at")
                                modelSellerSubscriptions.frequency_type =
                                    subscriptionData.optString("frequency_type")
                                modelSellerSubscriptions.free_trial =
                                    subscriptionData.optString("free_trial")
                                modelSellerSubscriptions.introductory_price =
                                    subscriptionData.optString("introductory_price")
                                modelSellerSubscriptions.free_trial_qty =
                                    subscriptionData.optString("free_trial_qty")
                                modelSellerSubscriptions.frequency_value =
                                    subscriptionData.optString("frequency_value")
                                modelSellerSubscriptions.campaign_image =
                                    subscriptionData.optString("campaign_image")
                                modelSellerSubscriptions.campaign_name =
                                    subscriptionData.optString("campaign_name")
                                modelSellerSubscriptions.subscriber =
                                    subscriptionData.optString("subscriber")
                                modelSellerSubscriptions.location_id =
                                    subscriptionData.optString("location_id")
                                modelSellerSubscriptions.description =
                                    subscriptionData.optString("description")
                                modelSellerSubscriptions.business_logo =
                                    subscriptionData.optString("business_logo")
                                modelSellerSubscriptions.like = subscriptionData.optString("like")
                                modelSellerSubscriptions.like_percentage =
                                    subscriptionData.optString("like_percentage")
                                modelSellerSubscriptions.views = subscriptionData.optString("views")
                                modelSellerSubscriptions.status1 =
                                    subscriptionData.optString("status")
                                modelSellerSubscriptions.bought =
                                    subscriptionData.optString("bought")
                                modelSellerSubscriptions.quantity =
                                    subscriptionData.optString("quantity")
                                modelSellerSubscriptions.is_discounted =
                                    subscriptionData.optString("is_discounted")
                                modelSellerSubscriptions.is_public =
                                    subscriptionData.optString("is_public")
                                modelSellerSubscriptions.complete_percentage =
                                    subscriptionData.optString("complete_percentage")
                                modelSellerSubscriptions.introductory_days =
                                    subscriptionData.optString("introductory_days")
                                modelSellerSubscriptions.scount =
                                    subscriptionData.optString("scount")
                                modelSellerSubscriptions.status = status
                                modelSellerSubscriptions.msg = msg
                                //   if (modelSellerSubscriptions.is_public == "0" && modelSellerSubscriptions.status1 == "1") {
                                arrayListCampaigns.add(modelSellerSubscriptions)
                                // }
                            }

                            mDataCampaigns.value = arrayListCampaigns

                        } else {
                            modelStatus.msg = msg
                            mDataFailure.value = modelStatus
                        }

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus
            }
        })
    }


    fun getmAllCampaignsData(): LiveData<ArrayList<ModelSellerSubscriptions>> {
        return mDataCampaigns
    }

    fun getmDataCampaignsActiveOnly(): LiveData<ArrayList<ModelSellerSubscriptions>> {
        return mDataCampaignsActiveOnly
    }


    fun getmDataFailure(): LiveData<ModelStatusMsg> {
        return mDataFailure
    }


    fun getCampaigns(campaignId: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val auth = Constant.getPrefs(application).getString(Constant.auth_code, "")


        val service = retrofit.create(WebServicesMerchants::class.java)
        val call: Call<ResponseBody> = service.getCampaignInfo(auth, campaignId)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                val res = response.body()!!.string()
                val json = JSONObject(res)
                val status = json.optString("status")
                val modelStatus = ModelStatusMsg()
                val arrayListCampaign = ArrayList<ModelMain>()
                val msg = json.optString("msg")

                if (status == "true") {
                    try {
                        val data = json.optJSONObject("data")
                        Log.e("data", data.toString())

                        val campaign_info = data.getJSONObject("campaign_info")
                        val modelMain = ModelMain()
                        modelMain.campaign_id = campaign_info.optString("campaign_id")
                        modelMain.team_member = campaign_info.optString("team_member")
                        modelMain.steps = campaign_info.optString("steps")
                        modelMain.products = campaign_info.optString("products")
                        modelMain.location_id = campaign_info.optString("location_id")
                        modelMain.image = campaign_info.optString("image")
                        modelMain.name = campaign_info.optString("name")
                        modelMain.feature_image = campaign_info.optString("feature_image")
                        modelMain.business_id = campaign_info.optString("business_id")
                        modelMain.business_name = campaign_info.optString("business_name")
                        modelMain.campaign_info_id = campaign_info.optString("campaign_info_id")
                        modelMain.logo = campaign_info.optString("logo")
                        modelMain.like_percentage = campaign_info.optString("like_percentage")
                        modelMain.views = campaign_info.optString("views")
                        modelMain.subscriber = campaign_info.optString("subscriber")
                        modelMain.website = campaign_info.optString("website")
                        modelMain.business_phone = campaign_info.optString("business_phone")
                        modelMain.lat = campaign_info.optString("lat")
                        modelMain.long = campaign_info.optString("long")
                        modelMain.revenue = campaign_info.optString("revenue")
                        modelMain.redeemed = campaign_info.optString("redeemed")
                        modelMain.shared = campaign_info.optString("shared")
                        modelMain.status = campaign_info.optString("status")
                        modelMain.cancelled = campaign_info.optString("cancelled")
                        modelMain.description = campaign_info.optString("description")
                        modelMain.tags = campaign_info.optString("tags")
                        modelMain.limit = campaign_info.optString("limit")
                        modelMain.bought = campaign_info.optString("bought")
                        modelMain.campaign_start = campaign_info.optString("campaign_start")
                        modelMain.campaign_end = campaign_info.optString("campaign_end")
                        modelMain.is_public = campaign_info.optString("is_public")
                        modelMain.share_setting = campaign_info.optString("share_setting")
                        modelMain.complete_percentage = campaign_info.optString("complete_percentage")
                        modelMain.subs = campaign_info.optString("max_subscriber")
                        val productInfo = campaign_info.optJSONArray("productInfo")
                        val locationInfo = campaign_info.optJSONArray("locationInfo")
                        val tagInfo = campaign_info.optJSONArray("tagInfo")
                        val parentTags = campaign_info.optJSONArray("parentTags")
                        val teamInfo = campaign_info.optJSONArray("teamInfo")
                        val subscription = data.optJSONArray("subscription")
                        val latest_reviews = data.optJSONArray("latest_reviews")
                        val reviews = data.optJSONArray("reviews")
                        val parent_tags = campaign_info.optJSONArray("parent_tags")

                        for (i in 0 until reviews.length()) {
                            val reviewsmodel = ModelReviews()
                            val reviewsData = reviews.optJSONObject(i)
                            reviewsmodel.name = reviewsData.optString("name")
                            reviewsmodel.count = reviewsData.optString("count")
                            reviewsmodel.id = reviewsData.optString("id")
                            modelMain.modelReviews.add(reviewsmodel)
                        }

                        for (i in 0 until latest_reviews.length()) {
                            val modelLatestreviewsData = ModelReviewDescription()
                            val latestreviewsData = latest_reviews.optJSONObject(i)
                            modelLatestreviewsData.review = latestreviewsData.optString("review")
                            modelLatestreviewsData.type = latestreviewsData.optString("type")
                            modelLatestreviewsData.name = latestreviewsData.optString("name")
                            modelLatestreviewsData.profile_image =
                                latestreviewsData.optString("profile_image")
                            modelLatestreviewsData.updated_at =
                                latestreviewsData.optString("updated_at")
                            modelMain.modelLatestreviews.add(modelLatestreviewsData)
                        }


                        for (i in 0 until productInfo.length()) {
                            val product = productInfo.getJSONObject(i)
                            val productInfoModel = ProductInfoModel()
                            productInfoModel.name = product.optString("name")
                            productInfoModel.price = product.optString("price")
                            productInfoModel.id = product.optString("id")
                            modelMain.productInfo.add(productInfoModel)
                        }




                        for (j in 0 until locationInfo.length()) {
                            val location = locationInfo.getJSONObject(j)
                            val modelSearchLocation = ModelSearchLocation()
                            modelSearchLocation.id = location.optString("id")
                            modelSearchLocation.city = location.optString("city")
                            modelSearchLocation.state = location.optString("state")
                            modelSearchLocation.street = location.optString("street")
                            modelSearchLocation.user_id = location.optString("user_id")
                            modelSearchLocation.business_id = location.optString("business_id")
                            modelSearchLocation.state_code = location.optString("state_code")
                            modelSearchLocation.zip_code = location.optString("zip_code")
                            modelSearchLocation.lat = location.optString("lat")
                            modelSearchLocation.long = location.optString("long")
                            val store_timing = location.getJSONArray("store_timing")
                            for (store in 0 until store_timing.length()) {
                                val storeData = store_timing.optJSONObject(store)
                                val modelStore = ModelStoreLocation()
                                modelStore.close = storeData.optString("close")
                                modelStore.open = storeData.optString("open")
                                modelStore.day = storeData.optString("day")
                                modelStore.status = storeData.optString("status")
                                modelStore.checked = storeData.optString("checked")
                                modelSearchLocation.store_timing.add(modelStore)
                            }
//                            arrayLocation.add(modeLocation)
                            modelMain.locationInfo.add(modelSearchLocation)

                        }


                        for (k in 0 until tagInfo.length()) {
                            val tag = tagInfo.getJSONObject(k)

                            val modelSubCategory = ModelSubCategory()
                            modelSubCategory.id = tag.optString("id")
                            modelSubCategory.name = tag.optString("name")
                            modelMain.tagInfo.add(modelSubCategory)
                        }


                        for (k1 in 0 until parentTags.length()) {
                            val parentTag = parentTags.getJSONObject(k1)
                            val modelSubCategory = ModelSubCategory()
                            modelSubCategory.parent_id = parentTag.optString("id")
                            modelSubCategory.parentName = parentTag.optString("name")
                            modelMain.parentTags.add(modelSubCategory)
                        }


                        for (p in 0 until parent_tags.length()) {
                            val parentTag = parent_tags.getJSONObject(p)
                            val categoryModel = CategoryModel()
                            categoryModel.categoryName = parentTag.optString("parent_name")
                            categoryModel.categoryId = parentTag.optString("parent_id")
                            modelMain.parentTagInfo.add(categoryModel)
                        }


                        for (l in 0 until teamInfo.length()) {
                            val team = teamInfo.getJSONObject(l)
                            val manageTeamModel = ManageTeamModel()
                            manageTeamModel.business_id = team.optString("business_id")
                            manageTeamModel.staff_id = team.optString("staff_id")
                            manageTeamModel.namee = team.optString("name")
                            manageTeamModel.speciality = team.optString("speciality")
                            manageTeamModel.profile_image = team.optString("profile_image")
                            modelMain.teamInfo.add(manageTeamModel)
                        }


                        for (m in 0 until subscription.length()) {
                            val sub = subscription.getJSONObject(m)
                            val subscriptionInfoModel = ModelSubscriptionCard()
                            subscriptionInfoModel.id = sub.optString("id")
                            subscriptionInfoModel.campaignId = sub.optString("campaign_id")
                            subscriptionInfoModel.price = sub.optString("price")
                            subscriptionInfoModel.discountPrice = sub.optString("discount_price")
                            subscriptionInfoModel.redeemtion_cycle_qty =
                                sub.optString("redeemtion_cycle_qty")
                            subscriptionInfoModel.quantityName = sub.optString("unit")
                            subscriptionInfoModel.unit = sub.optString("unit")
                            subscriptionInfoModel.frequencyType = sub.optString("frequency_type")
                            subscriptionInfoModel.freeTrail = sub.optString("free_trial")
                            subscriptionInfoModel.free_trail_qty = sub.optString("free_trial_qty")
                            subscriptionInfoModel.introPrice = sub.optString("introductory_price")
                            subscriptionInfoModel.introDays = sub.optString("introductory_days")
                            subscriptionInfoModel.frequency = sub.optString("frequency_value")
                            subscriptionInfoModel.createdAt = sub.optString("created_at")
                            subscriptionInfoModel.subscription_id = sub.optString("subscription_id")
                            subscriptionInfoModel.introductory_price =
                                sub.optString("introductory_price")
                            subscriptionInfoModel.introductory_days =
                                sub.optString("introductory_days")
                            subscriptionInfoModel.updatedAt = sub.optString("updated_at")
                            subscriptionInfoModel.subscriber = sub.optString("subscriber")
                            subscriptionInfoModel.subscription_id = sub.optString("subscription_id")
                            subscriptionInfoModel.pricingSubscriber = sub.optString("subscriber")
                            modelMain.subscription.add(subscriptionInfoModel)
                        }


                        arrayListCampaign.add(modelMain)
                        mDataCampaignInfo.value = modelMain

                        Log.e("subscription", subscription.toString())

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }


                } else {
                    modelStatus.msg = msg
                    modelStatus.status = status
                    mDataFailure.value = modelStatus
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataCampaignFailure.value = modelStatus

            }


        })

    }


    fun getmCampaignData(): LiveData<ModelMain> {
        return mDataCampaignInfo
    }

    fun getmCampaignDataFailure(): LiveData<ModelStatusMsg> {
        return mDataCampaignFailure
    }

    fun getmActiveStatus(): LiveData<ModelStatusMsg> {
        return mDataSuccess
    }


    //CLONE CAMPAIGN


    fun cloneCampaign(campaignId: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val auth = Constant.getPrefs(application).getString(Constant.auth_code, "")


        val service = retrofit.create(WebServicesMerchants::class.java)
        val call: Call<ResponseBody> = service.cloneCampaign(auth, campaignId)
        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        if (status == "true") {
                            Toast.makeText(application, msg, Toast.LENGTH_SHORT).show()
                        } else {
                            modelStatus.msg = msg
                            modelStatus.status = status
                            Toast.makeText(application, msg, Toast.LENGTH_SHORT).show()
                            mDataFailure.value = modelStatus
                        }

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataCampaignFailure.value = modelStatus

            }
        })

    }

    fun deleteCampaign(type: String, campaignId: String, subscriptionId: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val auth = Constant.getPrefs(application).getString(Constant.auth_code, "")


        val service = retrofit.create(WebServicesMerchants::class.java)
        val call: Call<ResponseBody> =
            service.deleteSubscription(auth, type, campaignId, subscriptionId)
        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        if (status == "true") {
                            mDataDeleteCampaign.value = modelStatus
                            Toast.makeText(application, msg, Toast.LENGTH_SHORT).show()
                        } else {
                            Toast.makeText(application, msg, Toast.LENGTH_SHORT).show()
                            modelStatus.msg = msg
                            modelStatus.status = status
                            mDataFailure.value = modelStatus
                        }

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataCampaignFailure.value = modelStatus

            }
        })


    }

    fun changeCampaignStatus(campaignId: String, type: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val auth = Constant.getPrefs(application).getString(Constant.auth_code, "")


        val service = retrofit.create(WebServicesMerchants::class.java)
        val call: Call<ResponseBody> = service.changeCampaignStatus(auth, campaignId, type)
        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    val res = response.body()!!.string()
                    val json = JSONObject(res)
                    val status = json.optString("status")
                    val msg = json.optString("msg")
                    val modelStatus = ModelStatusMsg()
                    if (status == "true") {
                        val modelSubscriptionCard = ModelSubscriptionCard()
                        modelSubscriptionCard.msg = msg
                        modelSubscriptionCard.status1 = status
                        mDataActiveInactive.value = modelSubscriptionCard
//                        Toast.makeText(application, msg, Toast.LENGTH_SHORT).show()
                    } else {
//                        Toast.makeText(application, msg, Toast.LENGTH_SHORT).show()
                        modelStatus.msg = msg
                        modelStatus.status = status
                        mDataFailure.value = modelStatus
                    }

                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataCampaignFailure.value = modelStatus

            }
        })

    }

    fun getmActiveStatus1(): LiveData<ModelSubscriptionCard> {
        return mDataActiveInactive
    }

    fun getmDataDeleteCampaign(): LiveData<ModelStatusMsg> {
        return mDataDeleteCampaign
    }


    fun getCampaignSubs(campaignsId: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val auth = Constant.getPrefs(application).getString(Constant.auth_code, "")


        val service = retrofit.create(WebServicesMerchants::class.java)
        val call: Call<ResponseBody> = service.getCampaignSubscriptions(auth, campaignsId)
        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        val arrayInviteList = ArrayList<ModelShareRedeem>()
                        if (status == "true") {
                            val data = json.optJSONArray("data")

                            for (i in 0 until data.length()) {
                                val inviteSubs = data.optJSONObject(i)
                                val modelInvite = ModelShareRedeem()
                                modelInvite.campaign_id = inviteSubs.optString("campaign_id")
                                modelInvite.price = inviteSubs.optString("price")
                                modelInvite.unit = inviteSubs.optString("unit")
                                modelInvite.frequency_type = inviteSubs.optString("frequency_type")
                                modelInvite.frequency_value =
                                    inviteSubs.optString("frequency_value")
                                modelInvite.campaign_image = inviteSubs.optString("campaign_image")
                                modelInvite.campaign_name = inviteSubs.optString("campaign_name")
                                modelInvite.description = inviteSubs.optString("description")
                                modelInvite.status1 = inviteSubs.optString("status")
                                modelInvite.redeemtion_cycle_qty =
                                    inviteSubs.optString("redeemtion_cycle_qty")
                                modelInvite.business_name = inviteSubs.optString("business_name")
                                modelInvite.discount_price = inviteSubs.optString("discount_price")
                                modelInvite.subscription_id =
                                    inviteSubs.optString("subscription_id")
                                modelInvite.is_public = inviteSubs.optString("is_public")
                                modelInvite.bought = inviteSubs.optString("bought")
                                modelInvite.msg = msg
                                modelInvite.statusMain = status
                                arrayInviteList.add(modelInvite)
                            }

                            mDataInviteSubList.value = arrayInviteList

                        } else {
                            modelStatus.msg = msg
                            modelStatus.status = status
                            mDataFailure.value = modelStatus
                        }


                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus

            }
        })


    }

    fun getSubsList(): LiveData<ArrayList<ModelShareRedeem>> {
        return mDataInviteSubList
    }

    fun getCampaignSubsList(campaignsId: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val auth = Constant.getPrefs(application).getString(Constant.auth_code, "")


        val service = retrofit.create(WebServicesMerchants::class.java)
        val call: Call<ResponseBody> = service.getCampaignSubscriptions(auth, campaignsId)
        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        val arrayInviteList = ArrayList<SubscriptionListModel>()
                        if (status == "true") {
                            val data = json.optJSONArray("data")

                            for (i in 0 until data.length()) {
                                val inviteSubs = data.optJSONObject(i)
                                val modelInvite = SubscriptionListModel()
                                modelInvite.campaign_id = inviteSubs.optString("campaign_id")
                                modelInvite.price = inviteSubs.optString("price")
                                modelInvite.unit = inviteSubs.optString("unit")
                                modelInvite.frequency_type = inviteSubs.optString("frequency_type")
                                modelInvite.frequency_value =
                                    inviteSubs.optString("frequency_value")
                                modelInvite.campaign_image = inviteSubs.optString("campaign_image")
                                modelInvite.campaign_name = inviteSubs.optString("campaign_name")
                                modelInvite.description = inviteSubs.optString("description")
                                modelInvite.status1 = inviteSubs.optString("status")
                                modelInvite.redeemtion_cycle_qty =
                                    inviteSubs.optString("redeemtion_cycle_qty")
                                modelInvite.business_name = inviteSubs.optString("business_name")
                                modelInvite.discount_price = inviteSubs.optString("discount_price")
                                modelInvite.subscription_id =
                                    inviteSubs.optString("subscription_id")
                                modelInvite.is_public = inviteSubs.optString("is_public")
                                modelInvite.bought = inviteSubs.optString("bought")
                                modelInvite.msg = msg
                                modelInvite.statusMain = status
                                arrayInviteList.add(modelInvite)
                            }

                            mDataSubscriptionList.value = arrayInviteList

                        } else {
                            modelStatus.msg = msg
                            modelStatus.status = status
                            mDataFailure.value = modelStatus
                        }


                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus

            }
        })

    }


    fun getSubscrpList(): LiveData<ArrayList<SubscriptionListModel>> {
        return mDataSubscriptionList
    }


    fun updateCampDisStatus(campaignId: String, statusApi: String, type: String, limit: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val auth = Constant.getPrefs(application).getString(Constant.auth_code, "")


        val service = retrofit.create(WebServicesMerchants::class.java)
        val call: Call<ResponseBody> =
            service.updateCampDisStatus(auth, campaignId, statusApi, type, limit)
        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        if (status == "true") {
                            modelStatus.status = status
                            modelStatus.msg = msg
                            modelStatus.sentStatus = statusApi
                            modelStatus.type = type
                            mDataSuccess.value = modelStatus

                        } else {
                            modelStatus.msg = msg
                            modelStatus.status = status
                            mDataFailure.value = modelStatus
                        }

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataCampaignFailure.value = modelStatus

            }
        })

    }

    fun getSubscriptionsList(auth: String, viewType: String, page: String, customerId: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesMerchants::class.java)
        val call: Call<ResponseBody> = service.getSubscription(auth, viewType, page, customerId)
        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        val arraySubscriptionList = ArrayList<SubscriptionListModel>()
                        if (status == "true") {
                            val data = json.optJSONArray("data")
                            for (i in 0 until data.length()) {
                                val subscriptionData = data.getJSONObject(i)
                                val modelSubscription = SubscriptionListModel()
                                modelSubscription.campaign_id =
                                    subscriptionData.optString("campaign_id")
                                modelSubscription.price = subscriptionData.optString("price")
                                modelSubscription.unit = subscriptionData.optString("unit")
                                modelSubscription.discount_price =
                                    subscriptionData.optString("discount_price")
                                modelSubscription.frequency_type =
                                    subscriptionData.optString("frequency_type")
                                modelSubscription.frequency_value =
                                    subscriptionData.optString("frequency_value")
                                modelSubscription.campaign_image =
                                    subscriptionData.optString("campaign_image")
                                modelSubscription.campaign_name =
                                    subscriptionData.optString("campaign_name")
                                modelSubscription.description =
                                    subscriptionData.optString("description")
                                modelSubscription.status1 = subscriptionData.optString("status")
                                modelSubscription.redeemtion_cycle_qty =
                                    subscriptionData.optString("redeemtion_cycle_qty")
                                modelSubscription.is_public =
                                    subscriptionData.optString("is_public")
                                modelSubscription.subscription_id =
                                    subscriptionData.optString("subscription_id")
                                modelSubscription.order_id = subscriptionData.optString("order_id")
                                modelSubscription.assign_to =
                                    subscriptionData.optString("assign_to")
                                modelSubscription.redeem_count =
                                    subscriptionData.optString("redeem_count")
                                modelSubscription.subscriber =
                                    subscriptionData.optString("subscriber")
                                modelSubscription.member_name =
                                    subscriptionData.optString("member_name")
                                arraySubscriptionList.add(modelSubscription)

                            }

                            modelStatus.status = status
                            modelStatus.msg = msg
                            mDataSubscriptionList.value = arraySubscriptionList

                        } else {
                            modelStatus.msg = msg
                            modelStatus.status = status
                            mDataFailure.value = modelStatus
                        }

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataCampaignFailure.value = modelStatus

            }
        })

    }

    fun getActiveSubsOnly(auth: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.getCampaigns(auth, "1")
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        val arrayListCampaigns = ArrayList<ModelSellerSubscriptions>()
                        if (status == "true") {
                            val data = json.optJSONArray("data")
                            for (i in 0 until data.length()) {
                                val subscriptionData = data.getJSONObject(i)
                                val modelSellerSubscriptions = ModelSellerSubscriptions()
                                modelSellerSubscriptions.subscription_id =
                                    subscriptionData.optString("subscription_id")
                                modelSellerSubscriptions.campaign_id =
                                    subscriptionData.optString("campaign_id")
                                modelSellerSubscriptions.price = subscriptionData.optString("price")
                                modelSellerSubscriptions.discount_price =
                                    subscriptionData.optString("discount_price")
                                modelSellerSubscriptions.unit = subscriptionData.optString("unit")
                                modelSellerSubscriptions.created_at =
                                    subscriptionData.optString("created_at")
                                modelSellerSubscriptions.updated_at =
                                    subscriptionData.optString("updated_at")
                                modelSellerSubscriptions.frequency_type =
                                    subscriptionData.optString("frequency_type")
                                modelSellerSubscriptions.free_trial =
                                    subscriptionData.optString("free_trial")
                                modelSellerSubscriptions.introductory_price =
                                    subscriptionData.optString("introductory_price")
                                modelSellerSubscriptions.free_trial_qty =
                                    subscriptionData.optString("free_trial_qty")
                                modelSellerSubscriptions.frequency_value =
                                    subscriptionData.optString("frequency_value")
                                modelSellerSubscriptions.campaign_image =
                                    subscriptionData.optString("campaign_image")
                                modelSellerSubscriptions.campaign_name =
                                    subscriptionData.optString("campaign_name")
                                modelSellerSubscriptions.subscriber =
                                    subscriptionData.optString("subscriber")
                                modelSellerSubscriptions.location_id =
                                    subscriptionData.optString("location_id")
                                modelSellerSubscriptions.description =
                                    subscriptionData.optString("description")
                                modelSellerSubscriptions.business_logo =
                                    subscriptionData.optString("business_logo")
                                modelSellerSubscriptions.like = subscriptionData.optString("like")
                                modelSellerSubscriptions.like_percentage =
                                    subscriptionData.optString("like_percentage")
                                modelSellerSubscriptions.views = subscriptionData.optString("views")
                                modelSellerSubscriptions.status1 =
                                    subscriptionData.optString("status")
                                modelSellerSubscriptions.bought =
                                    subscriptionData.optString("bought")
                                modelSellerSubscriptions.quantity =
                                    subscriptionData.optString("quantity")
                                modelSellerSubscriptions.is_discounted =
                                    subscriptionData.optString("is_discounted")
                                modelSellerSubscriptions.is_public =
                                    subscriptionData.optString("is_public")
                                modelSellerSubscriptions.complete_percentage =
                                    subscriptionData.optString("complete_percentage")
                                modelSellerSubscriptions.status = status
                                modelSellerSubscriptions.msg = msg
                                if (modelSellerSubscriptions.is_public == "0" && modelSellerSubscriptions.status1 == "1") {
                                    arrayListCampaigns.add(modelSellerSubscriptions)
                                }
                            }

                            mDataCampaignsActiveOnly.value = arrayListCampaigns

                        } else {
                            modelStatus.msg = msg
                            mDataFailure.value = modelStatus
                        }

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus
            }
        })
    }

    fun getCampaignSubscriber(page: Int, campaignId: String) {

        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val auth = Constant.getPrefs(application).getString(Constant.auth_code, "")


        val service = retrofit.create(WebServicesMerchants::class.java)
        val call: Call<ResponseBody> = service.getCampaignSubscriber(auth, page.toString(), campaignId)
        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    val res = response.body()!!.string()
                    val json = JSONObject(res)
                    val status = json.optString("status")
                    val msg = json.optString("msg")
                    val modelStatus = ModelStatusMsg()
                    if (status == "true") {
                       val arrayListSubInfo = ArrayList<ModelCampaignSubscriber>()
                        var data = json.optJSONObject("data")
                        val subscriber = data.optJSONArray("subscriber")

                        for(i in 0 until subscriber.length()){
                            val subInfo = subscriber.optJSONObject(i)
                            val modelCampaignSubscriber = ModelCampaignSubscriber()
                            modelCampaignSubscriber.msg = msg
                            modelCampaignSubscriber.status = status
                            modelCampaignSubscriber.order_id = subInfo.optString("order_id")
                            modelCampaignSubscriber.customer_id = subInfo.optString("customer_id")
                            modelCampaignSubscriber.name = subInfo.optString("name")
                            modelCampaignSubscriber.contact_no = subInfo.optString("contact_no")
                            modelCampaignSubscriber.email = subInfo.optString("email")
                            modelCampaignSubscriber.frequency_type = subInfo.optString("frequency_type")
                            modelCampaignSubscriber.frequency_value = subInfo.optString("frequency_value")
                            modelCampaignSubscriber.profile_image = subInfo.optString("profile_image")
                            modelCampaignSubscriber.count = subInfo.optInt("count")
                            arrayListSubInfo.add(modelCampaignSubscriber)
                        }

                        mDataCampaignSubscriber.value = arrayListSubInfo
                    } else {
//                        Toast.makeText(application, msg, Toast.LENGTH_SHORT).show()
                        modelStatus.msg = msg
                        modelStatus.status = status
                        mDataFailure.value = modelStatus
                    }

                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataCampaignFailure.value = modelStatus

            }
        })
    }

    fun getSubscriberInfoList(): LiveData<ArrayList<ModelCampaignSubscriber>> {
        return mDataCampaignSubscriber
    }


}