package com.upscribber.upscribberSeller.subsriptionSeller.subscriptionView.subsManage

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.upscribber.R
import com.upscribber.databinding.ActivityConfirmDeleteSubsBinding

class ConfirmDeleteSubsActivity : AppCompatActivity() {

    private lateinit var mAdapterCustomerList: AdapterSubscripLIstManage
    lateinit var mBinding : ActivityConfirmDeleteSubsBinding
    lateinit var viewModel : ViewModelManageList

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding= DataBindingUtil.setContentView(this,R.layout.activity_confirm_delete_subs)
        viewModel = ViewModelProviders.of(this)[ViewModelManageList::class.java]
        setToolbar()
        clickListeners()
        setAdapter()
    }

    private fun setAdapter() {
        mBinding.recyclerCustomerList.layoutManager = LinearLayoutManager(this)
        mBinding.recyclerCustomerList.hasFixedSize()
        mBinding.recyclerCustomerList.isNestedScrollingEnabled = false
        mAdapterCustomerList = AdapterSubscripLIstManage(this)
        mBinding.recyclerCustomerList.adapter = mAdapterCustomerList

        viewModel.getCustomers().observe(this, Observer {
            mAdapterCustomerList.update(it)
        })

    }

    private fun setToolbar() {

        setSupportActionBar(mBinding.toolbarProducts)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        title = ""
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)
        mBinding.title.text="Delete Subscription"


    }

    private fun clickListeners() {
        mBinding.continuee.setOnClickListener {
            startActivity(Intent(this,
                SuccessfullyDeleteActivity::class.java))
        }

    }

    override fun onResume() {
        super.onResume()
        val sharedPreferences1 = getSharedPreferences("customers", Context.MODE_PRIVATE)
        if (!sharedPreferences1.getString("customer", "").isEmpty()) {
            finish()
        }
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home){
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}
