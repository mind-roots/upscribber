package com.upscribber.upscribberSeller.subsriptionSeller.subscriptionView

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.location.Location
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.facebook.FacebookSdk.getApplicationContext
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.upscribber.R
import com.upscribber.upscribberSeller.subsriptionSeller.location.searchLoc.ModelSearchLocation


class MapsPagerAdapterMerchant(
    var context: Context,
    var metrics: DisplayMetrics,
    var list : ArrayList<ModelSearchLocation>
) :
    RecyclerView.Adapter<MapsPagerAdapterMerchant.MyViewHolder>(), OnMapReadyCallback {

    private var itemMargin: Int = 0
    private var itemWidth: Int = 0

    internal fun setItemMargin(itemMargin: Int) {
        this.itemMargin = itemMargin
    }

    internal fun updateDisplayMetrics() {
        itemWidth = metrics.widthPixels - itemMargin * 2
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(context).inflate(R.layout.multiple_maps, parent, false)
        return MyViewHolder(v,list)
    }

    class MyViewHolder(
        itemView: View,
        var list: ArrayList<ModelSearchLocation>

    ) : RecyclerView.ViewHolder(itemView), OnMapReadyCallback {
        var mapView: MapView?=null
        private lateinit var gMap: GoogleMap
        private  var fusedLocationClient: FusedLocationProviderClient
        private lateinit var lastLocation: Location
        var tViewLocation : TextView

        init {
            try {
                MapsInitializer.initialize(getApplicationContext())
            } catch (e: Exception) {
                e.printStackTrace()
            }
            mapView=itemView.findViewById(R.id.map)
            tViewLocation=itemView.findViewById(R.id.tViewLocation)
            fusedLocationClient = LocationServices.getFusedLocationProviderClient(getApplicationContext())

            if (mapView != null) {
                mapView!!.onCreate(null)
                mapView!!.getMapAsync(this)
                mapView!!.onResume()
            }
        }

        override fun onMapReady(p0: GoogleMap?) {
            gMap = p0!!
            gMap.mapType = GoogleMap.MAP_TYPE_NORMAL
            setUpMap()
        }

        private fun setUpMap() {
            if (ActivityCompat.checkSelfPermission(
                    getApplicationContext(),
                    android.Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(
                    getApplicationContext() as Activity,
                    arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                    1
                )
                return
            }

            fusedLocationClient.lastLocation.addOnSuccessListener { location ->
                if (location != null) {
                    lastLocation = location
                    var currentLatLng = LatLng(location.latitude, location.longitude)
                    for (itemList in list){
                        if (itemList.lat != "" && itemList.long != "") {
                            currentLatLng = LatLng(itemList.lat.toDouble(), itemList.long.toDouble())
                        }
                        placeMarkerOnMap(currentLatLng)
                        gMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 12f))
                    }

                }
            }

        }

        private fun placeMarkerOnMap(currentLatLng: LatLng) {
            gMap.addMarker(
                MarkerOptions()
                    .position(currentLatLng)
                    .icon(bitmapDescriptorFromVector( getApplicationContext(), R.drawable.ic_location_blue_icon))
            )

        }

        private fun bitmapDescriptorFromVector(
            context: Context,
            vectorResId: Int
        ): BitmapDescriptor? {
            val vectorDrawable = ContextCompat.getDrawable(context, vectorResId)
            vectorDrawable!!.setBounds(
                0,
                0,
                vectorDrawable.intrinsicWidth,
                vectorDrawable.intrinsicHeight
            )
            val bitmap =
                Bitmap.createBitmap(
                    vectorDrawable.intrinsicWidth,
                    vectorDrawable.intrinsicHeight,
                    Bitmap.Config.ARGB_8888
                )
            val canvas = Canvas(bitmap)
            vectorDrawable.draw(canvas)
            return BitmapDescriptorFactory.fromBitmap(bitmap)

        }

    }

    override fun getItemCount(): Int {
        return list.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = list[position]
        var currentItemWidth = itemWidth
        if (position == 0) {
            currentItemWidth += itemMargin
            holder.itemView.setPadding(itemMargin, 0, 0, 0)
        } else if (position == itemCount - 1) {
            currentItemWidth += itemMargin
            holder.itemView.setPadding(0, 0, itemMargin, 0)
        }

        val height = holder.itemView.layoutParams.height
        holder.itemView.layoutParams = ViewGroup.LayoutParams(currentItemWidth, height)
        holder.tViewLocation.text = model.city + "," +  model.state_code

    }


    override fun onMapReady(p0: GoogleMap?) {


    }


}