package com.upscribber.upscribberSeller.subsriptionSeller.subscriptionView

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.Glide
import com.upscribber.upscribberSeller.subsriptionSeller.subscriptionView.imageView.PictureViewerActivity
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.subsciptions.merchantSubscriptionPages.ModelImagesSubscriptionDetails


class CustomerPagerAdapterNew(var context : Context) : PagerAdapter(){

    var mData: ArrayList<ModelImagesSubscriptionDetails> = ArrayList()
    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`

    }

    override fun getCount(): Int {
        return mData.size
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val itemView = LayoutInflater.from(container.context)
            .inflate(R.layout.image_adapter_layout_seller, container, false)

        val img_pager = itemView.findViewById(R.id.img_pager) as ImageView
        val imagePath  = Constant.getPrefs(context).getString(Constant.dataImagePath, "")
       Glide.with(context).load(imagePath + mData[position].image).into(img_pager)
        container.addView(itemView)

        img_pager.setOnClickListener {
            val intent = Intent(container.context, PictureViewerActivity::class.java).putExtra("position",position)
                .putExtra("mData",mData)
            container.context.startActivity(intent)
        }

        return itemView
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View?)
    }

    override fun getPageWidth(position: Int): Float {
        return .50f
    }

    fun update(it: java.util.ArrayList<ModelImagesSubscriptionDetails>?) {
        mData = it!!
        notifyDataSetChanged()

    }

}