package com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.createSubs

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.R
import com.upscribber.databinding.SetTeamLayoutBinding
import com.upscribber.upscribberSeller.manageTeam.ManageTeamModel

class ManageTeamAdapter1 (var context: Context): RecyclerView.Adapter<ManageTeamAdapter1.MyViewHolder>() {

    private lateinit var mBinding: SetTeamLayoutBinding
    private  var items = ArrayList<ManageTeamModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        mBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.set_team_layout, parent, false)
        return MyViewHolder(mBinding)

    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = items[position]
        holder.bind(model)
    }

    class MyViewHolder(var  binding: SetTeamLayoutBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(obj: ManageTeamModel) {
            binding.model = obj
            binding.executePendingBindings()
        }
    }

    fun update(items: ArrayList<ManageTeamModel>) {
        this.items = items
        notifyDataSetChanged()
    }

}