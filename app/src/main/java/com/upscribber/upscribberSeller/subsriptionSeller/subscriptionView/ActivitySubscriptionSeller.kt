package com.upscribber.upscribberSeller.subsriptionSeller.subscriptionView

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.graphics.DashPathEffect
import android.graphics.Outline
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import com.upscribber.commonClasses.RoundedBottomSheetDialogFragment
import com.upscribber.subsciptions.merchantSubscriptionPages.extra.SaveExtraAdapter
import com.upscribber.subsciptions.merchantSubscriptionPages.extra.SaveExtraModel
import com.upscribber.subsciptions.merchantSubscriptionPages.reviews.ReviewAdapter
import com.upscribber.subsciptions.merchantSubscriptionPages.reviews.ReviewDescriptionAdapter
import com.upscribber.upscribberSeller.navigationCustomer.InviteSubscription
import com.upscribber.upscribberSeller.subsriptionSeller.extras.ExtrasSellerActivity
import com.upscribber.upscribberSeller.subsriptionSeller.extras.ModelExtra
import com.upscribber.upscribberSeller.subsriptionSeller.subscriptionView.Info.AdapterSubscriptionDetails
import com.upscribber.upscribberSeller.subsriptionSeller.subscriptionView.staff.*
import com.upscribber.util.MyMarkerView
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.IAxisValueFormatter
import com.github.mikephil.charting.formatter.IFillFormatter
import com.github.mikephil.charting.formatter.LargeValueFormatter
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.chip.Chip
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.databinding.*
import com.upscribber.filters.ModelSubCategory
import com.upscribber.subsciptions.merchantStore.AdapterWorkingDays
import com.upscribber.subsciptions.merchantSubscriptionPages.ModelImagesSubscriptionDetails
import com.upscribber.subsciptions.merchantSubscriptionPages.SubInfoList
import com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.createSubs.CreateSubscriptionActivity
import com.upscribber.upscribberSeller.subsriptionSeller.location.searchLoc.ModelSearchLocation
import com.upscribber.upscribberSeller.subsriptionSeller.ratingStats.RatingStatsActivity
import com.upscribber.upscribberSeller.subsriptionSeller.subscriptionMain.ModelMain
import com.upscribber.upscribberSeller.subsriptionSeller.subscriptionMain.SubscriptionSellerViewModel
import com.upscribber.upscribberSeller.subsriptionSeller.subscriptionView.subsManage.ConfirmInActiveActivity
import kotlinx.android.synthetic.main.boxlay.view.*
import kotlinx.android.synthetic.main.custom_dialog.*
import kotlin.math.roundToInt

@Suppress("DEPRECATION", "RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class ActivitySubscriptionSeller : AppCompatActivity() {

    private val page: Int = 1
    var mAdapterPager: MapsPagerAdapterMerchant? = null
    lateinit var mAdapterOperationHours: AdapterWorkingDays

    val layout = LinearLayoutManager(this)
    var option = 0
    var option2 = 0
    private var arrayName = ArrayList<ModelSubCategory>()
    lateinit var imageEdit: TextView


    var myCustomPagerAdapter: CustomerPagerAdapterNew? = null
    lateinit var mBinding: ActivitySubscriptionSellerBinding
    lateinit var viewModel: ViewModelActivitySeller
    lateinit var mViewModel: SubscriptionSellerViewModel
    var shofull = 0

    var status = ""
    var campaign_id = ""
    var statusEditData = ""
    var subInfo: ArrayList<ModelCampaignSubscriber> = ArrayList()
    lateinit var progressBar : LinearLayout

    companion object {
        var editData = ModelMain()
        var isPublic = ""
        var shareSettings = ""
        var bought = "0"
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_subscription_seller)
        viewModel = ViewModelProviders.of(this)[ViewModelActivitySeller::class.java]
        mViewModel = ViewModelProviders.of(this).get(SubscriptionSellerViewModel::class.java)
        setToolbar()
        setViewPagerForTop()

        mBinding.contentScroll.nestedScrollView2.scrollTo(0, 0)

        progressBar = findViewById(R.id.progressBar)




        mBinding.contentScroll.belowOption.infoViewLay.infoSub.recyclerViewReviewDescription.layoutManager =
            LinearLayoutManager(this)
        mBinding.contentScroll.belowOption.infoViewLay.infoSub.recyclerViewReviewDescription.isNestedScrollingEnabled =
            false
        mBinding.contentScroll.belowOption.infoViewLay.infoSub.recyclerViewReviewDescription.adapter =
            ReviewDescriptionAdapter(this, editData.modelLatestreviews)

        if (intent.hasExtra("campaignId")) {
            campaign_id = intent.getStringExtra("campaignId")
//            mViewModel.getCampaign(campaign_id)
            mBinding.progressBar.visibility = View.VISIBLE

            mViewModel.getmCampaignData().observe(this, Observer {
                mBinding.progressBar.visibility = View.GONE
                editData = it
                optionsAdapter()
                setForInfoClick()
                setForStaffClick()
                setReviewAdapter()
                setReviewDescriptionAdapter()
                isPublic = it.is_public
                status = it.status
                campaign_id = it.campaign_id
                shareSettings = it.share_setting
                bought = it.bought
                mBinding.contentScroll.textViewName.text = editData.name
                mBinding.contentScroll.textView107.text = editData.like_percentage + "%"
                mBinding.contentScroll.tvPeople.text = editData.bought
                mBinding.contentScroll.tvView.text = editData.views
                mBinding.contentScroll.belowOption.saleViewLay.tvSalesPrice.text = editData.revenue
                mBinding.contentScroll.belowOption.saleViewLay.boxLay.redemText.text =
                    editData.redeemed
                mBinding.contentScroll.belowOption.saleViewLay.boxLay.salesText.text =
                    editData.views
                mBinding.contentScroll.belowOption.saleViewLay.boxLay.subText.text =
                    editData.subscriber
                mBinding.contentScroll.belowOption.saleViewLay.boxLay.viewText.text =
                    editData.like_percentage
                mBinding.contentScroll.belowOption.saleViewLay.boxLay.cancelText.text =
                    editData.cancelled
                mBinding.contentScroll.belowOption.saleViewLay.boxLay.sharedText.text =
                    editData.shared
//                mBinding.contentScroll.tvPeople.text = editData. + "%"
                mBinding.contentScroll.rvPlan.layoutManager = LinearLayoutManager(this)
                mBinding.contentScroll.rvPlan.isNestedScrollingEnabled = false
                mBinding.contentScroll.rvPlan.adapter =
                    AdapterSubscriptionPlan(this, editData.subscription)
                setForLocations(it.locationInfo)
                setForOperationHours(it.locationInfo)
                val images = ArrayList<ModelImagesSubscriptionDetails>()
                val newSplitImages = it.image.split(",")
                for (element in newSplitImages) {
                    val model = ModelImagesSubscriptionDetails()
                    model.image = element
                    images.add(model)
                }
                myCustomPagerAdapter!!.update(images)
            })

            mViewModel.getSubscriberInfoList().observe(this, Observer {
                subInfo = it
            })
        }

        if (intent.hasExtra("status1")) {
            statusEditData = intent.getStringExtra("status1")
        }
        setClickListeners()
        setSubscriptionPlanAdapter()

        setOpeningHours()
        setGraphForSales()

        val type = Constant.getPrefs(this).getString(Constant.type, "")
        if (type != "3") {
            imageEdit.isClickable = true
            mBinding.contentScroll.manage.isClickable = true
        } else {
            imageEdit.isClickable = false
            mBinding.contentScroll.manage.isClickable = false
            imageEdit.alpha = 0.6f
            mBinding.contentScroll.manage.alpha = 0.6f
        }

    }

    private fun setForLocations(it: ArrayList<ModelSearchLocation>) {
        mBinding.contentScroll.belowOption.infoViewLay.infoSub.viewPagerMaps.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        mAdapterPager =
            MapsPagerAdapterMerchant(this, Constant.getDisplayMetrics(windowManager), it)
        mAdapterPager!!.setItemMargin(16)
        mAdapterPager!!.updateDisplayMetrics()
        mBinding.contentScroll.belowOption.infoViewLay.infoSub.indicatorMaps.setRecyclerView(
            mBinding.contentScroll.belowOption.infoViewLay.infoSub.viewPagerMaps
        )
        mBinding.contentScroll.belowOption.infoViewLay.infoSub.indicatorMaps.forceUpdateItemCount()
        PagerSnapHelper().attachToRecyclerView(mBinding.contentScroll.belowOption.infoViewLay.infoSub.viewPagerMaps)
        mBinding.contentScroll.belowOption.infoViewLay.infoSub.viewPagerMaps.adapter = mAdapterPager

    }

    private fun setForOperationHours(it: ArrayList<ModelSearchLocation>) {
        mBinding.contentScroll.belowOption.infoViewLay.infoSub.recyclerWorkingDays.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        mAdapterOperationHours = AdapterWorkingDays(this)
        mBinding.contentScroll.belowOption.infoViewLay.infoSub.recyclerWorkingDays.adapter =
            mAdapterOperationHours
        mAdapterOperationHours.update(it)

    }


    private fun setGraphForSales() {
        setChart()
//        viewModel.getGraphData().observe(this, Observer {
//            setChart(it.graphPlotPricex.toInt(), it.graphPlotPricey.toInt())
//        })

    }


    private fun setChart() {
        val myMarkerView = MyMarkerView(this, R.layout.custom_marker_view)
        myMarkerView.chartView = mBinding.contentScroll.belowOption.saleViewLay.chartReporting
        mBinding.contentScroll.belowOption.saleViewLay.chartReporting.marker = myMarkerView
        mBinding.contentScroll.belowOption.saleViewLay.chartReporting.description.isEnabled = false

        val xAxis: XAxis = mBinding.contentScroll.belowOption.saleViewLay.chartReporting.xAxis

        mBinding.contentScroll.belowOption.saleViewLay.chartReporting.xAxis.isEnabled = true
        xAxis.position = XAxis.XAxisPosition.BOTTOM
        xAxis.setDrawAxisLine(true)

        xAxis.textSize = 6f
        xAxis.textColor = Color.parseColor("#94BAF8")

        val listAmount = ArrayList<String>()
        listAmount.add("1k")
        listAmount.add("2k")
        listAmount.add("3k")
        listAmount.add("4k")
        listAmount.add("10k")
        listAmount.add("15k")
        listAmount.add("22k")

        val listDays = ArrayList<String>()
        listDays.add("MON")
        listDays.add("TUE")
        listDays.add("WED")
        listDays.add("THUR")
        listDays.add("FRI")
        listDays.add("SAT")
        listDays.add("SUN")


        val yAxis: YAxis = mBinding.contentScroll.belowOption.saleViewLay.chartReporting.axisLeft
        mBinding.contentScroll.belowOption.saleViewLay.chartReporting.axisRight.isEnabled = false
        mBinding.contentScroll.belowOption.saleViewLay.chartReporting.axisLeft.isEnabled = true

        xAxis.axisMaximum = 6f
        xAxis.axisMinimum = 0f
        yAxis.granularity = 1f
        yAxis.axisMinimum = 0f
        yAxis.axisMaximum = 6f
        yAxis.textSize = 8f
        yAxis.textColor = Color.WHITE
        xAxis.textColor = Color.WHITE
        xAxis.valueFormatter = IAxisValueFormatter { value, axis -> listDays[value.roundToInt()] }
        yAxis.valueFormatter = IAxisValueFormatter { value, axis -> listAmount[value.roundToInt()] }

        mBinding.contentScroll.belowOption.saleViewLay.chartReporting.axisLeft.setDrawGridLines(true)
        mBinding.contentScroll.belowOption.saleViewLay.chartReporting.axisLeft.axisLineColor =
            resources.getColor(R.color.fulltransparentcolor)
        mBinding.contentScroll.belowOption.saleViewLay.chartReporting.axisRight.axisLineColor =
            resources.getColor(R.color.colorWhite)
        mBinding.contentScroll.belowOption.saleViewLay.chartReporting.xAxis.setDrawGridLines(false)
        mBinding.contentScroll.belowOption.saleViewLay.chartReporting.axisLeft.gridColor =
            resources.getColor(R.color.fulltransparentcolor)
        yAxis.setLabelCount(5, true)

        yAxis.setDrawLimitLinesBehindData(false)
        xAxis.setDrawLimitLinesBehindData(false)
        setData()

        mBinding.contentScroll.belowOption.saleViewLay.chartReporting.animateX(500)

        val legend = mBinding.contentScroll.belowOption.saleViewLay.chartReporting.legend
        legend.form = Legend.LegendForm.NONE

    }


//    @RequiresApi(Build.VERSION_CODES.HONEYCOMB)
//    private fun setChart(a: Int, b: Int) {
//
//        val myMarkerView = MyMarkerView(this, R.layout.custom_marker_view)
//        myMarkerView.chartView =  mBinding.contentScroll.belowOption.saleViewLay.chartReporting
//        mBinding.contentScroll.belowOption.saleViewLay.chartReporting.marker = myMarkerView
//        mBinding.contentScroll.belowOption.saleViewLay.chartReporting.description.isEnabled = false
//
//        val xAxis: XAxis = mBinding.contentScroll.belowOption.saleViewLay.chartReporting.xAxis
//
//        mBinding.contentScroll.belowOption.saleViewLay.chartReporting.xAxis.isEnabled = true
//        xAxis.position = XAxis.XAxisPosition.BOTTOM
//        xAxis.setDrawAxisLine(false)
//
//        val listDays = ArrayList<String>()
//        listDays.add("MON")
//        listDays.add("TUE")
//        listDays.add("WED")
//        listDays.add("THUR")
//        listDays.add("FRI")
//        listDays.add("SAT")
//        listDays.add("SUN")
//
//        xAxis.granularity = 1f
//        xAxis.axisMinimum = 0f
//        xAxis.axisMaximum = 6f
//        xAxis.textSize = 6f
//        xAxis.textColor = Color.parseColor("#94BAF8")
//
//        val yAxis: YAxis = mBinding.contentScroll.belowOption.saleViewLay.chartReporting.axisLeft
//        mBinding.contentScroll.belowOption.saleViewLay.chartReporting.axisRight.isEnabled = false
//        mBinding.contentScroll.belowOption.saleViewLay.chartReporting.axisLeft.isEnabled = false
//
//        yAxis.axisMaximum = 150f
//        yAxis.axisMinimum = -70f
//
//        mBinding.contentScroll.belowOption.saleViewLay.chartReporting.axisLeft.setDrawGridLines(false)
//        mBinding.contentScroll.belowOption.saleViewLay.chartReporting.xAxis.setDrawGridLines(false)
//        xAxis.valueFormatter = IAxisValueFormatter { value, _ -> listDays[Math.round(value)] }
//        yAxis.setDrawLimitLinesBehindData(true)
//        xAxis.setDrawLimitLinesBehindData(true)
//
//        setData(a, b)
//
//        mBinding.contentScroll.belowOption.saleViewLay.chartReporting.animateX(500)
//
//
//        val legend = mBinding.contentScroll.belowOption.saleViewLay.chartReporting.legend
//        legend.form = Legend.LegendForm.NONE
//
//    }

    private fun setData() {
        val valuess = ArrayList<Entry>()
        valuess.add(Entry(0f, 0000f / 1000))
        valuess.add(Entry(2f, 2000f / 1000))
        valuess.add(Entry(3f, 3000f / 1000))
        valuess.add(Entry(5f, 4000f / 1000))
        valuess.add(Entry(6f, 5000f / 1000))
        valuess.add(Entry(7f, 6000f / 1000))
        valuess.add(Entry(8f, 7000f / 1000))
        valuess.add(Entry(9f, 9000f / 1000))


        val set1 = LineDataSet(valuess, "")
        set1.setDrawValues(false)

        mBinding.contentScroll.belowOption.saleViewLay.chartReporting.setLayerType(
            View.LAYER_TYPE_SOFTWARE,
            null
        )
        mBinding.contentScroll.belowOption.saleViewLay.chartReporting.setScaleEnabled(false)
        set1.color = Color.WHITE
        set1.setDrawCircles(false)
        set1.lineWidth = 2f
        set1.setDrawCircleHole(false)
        set1.formLineWidth = 1f
        set1.formLineDashEffect = DashPathEffect(floatArrayOf(10f, 5f), 0f)
        set1.formSize = 15f
        set1.valueTextSize = 10f
        set1.circleRadius = 4.5f
        set1.setCircleColor(Color.parseColor("#02CBCD"))
        set1.setDrawHorizontalHighlightIndicator(false)
        set1.setDrawVerticalHighlightIndicator(false)
        set1.setDrawFilled(true)
        set1.fillColor = Color.parseColor("#02CBCD")
        set1.fillFormatter = IFillFormatter { _,
                                              _ ->
            mBinding.contentScroll.belowOption.saleViewLay.chartReporting.axisLeft.axisMinimum
        }

        val dataSets = ArrayList<ILineDataSet>()
        dataSets.add(set1)

        val data = LineData(dataSets)
        data.setValueFormatter(LargeValueFormatter())
//        mBinding.chartReporting.xAxis.axisMaximum = 15f + 0.15f
        mBinding.contentScroll.belowOption.saleViewLay.chartReporting.data = data
        mBinding.contentScroll.belowOption.saleViewLay.chartReporting.invalidate()


    }

    private fun setForStaffClick() {
        viewModel.staffTop().observe(this, Observer {
            mBinding.contentScroll.belowOption.staffViewLay.recyclerView2.layoutManager =
                LinearLayoutManager(this)
            mBinding.contentScroll.belowOption.staffViewLay.recyclerView2.itemAnimator =
                DefaultItemAnimator()
            mBinding.contentScroll.belowOption.staffViewLay.recyclerView2.isNestedScrollingEnabled =
                false
            mBinding.contentScroll.belowOption.staffViewLay.recyclerView2.adapter =
                AdapterStaffTop(this, it, editData.teamInfo)
        })

        viewModel.staffFaq().observe(this, Observer {
            mBinding.contentScroll.belowOption.staffViewLay.staffDescriptionRv.layoutManager =
                LinearLayoutManager(this)
            mBinding.contentScroll.belowOption.staffViewLay.staffDescriptionRv.itemAnimator =
                DefaultItemAnimator()
            mBinding.contentScroll.belowOption.staffViewLay.staffDescriptionRv.isNestedScrollingEnabled =
                false
            mBinding.contentScroll.belowOption.staffViewLay.staffDescriptionRv.adapter =
                AdapterStaffFAQ(this, it)
        })


    }

    private fun setForInfoClick() {
        subscriptionDetails()
        //chipData()

    }

    private fun subscriptionDetails() {

        viewModel.deals().observe(this, Observer {
            mBinding.contentScroll.belowOption.infoViewLay.rvOneInclude.layoutManager =
                LinearLayoutManager(this)
            mBinding.contentScroll.belowOption.infoViewLay.rvOneInclude.itemAnimator =
                DefaultItemAnimator()
            mBinding.contentScroll.belowOption.infoViewLay.rvOneInclude.isNestedScrollingEnabled =
                false
            mBinding.contentScroll.belowOption.infoViewLay.rvOneInclude.adapter =
                AdapterSubscriptionDetails(this, it)
        })

    }

    private fun setOpeningHours() {


    }


    private fun setSubscriptionPlanAdapter() {
        viewModel.getPlanValues().observe(this, Observer {

        })

    }


    private fun setReviewAdapter() {
        viewModel.getReviewData().observe(this, Observer {
            mBinding.contentScroll.belowOption.infoViewLay.infoSub.recyclerViewReviews.layoutManager =
                LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
            mBinding.contentScroll.belowOption.infoViewLay.infoSub.recyclerViewReviews.isNestedScrollingEnabled =
                false
            mBinding.contentScroll.belowOption.infoViewLay.infoSub.recyclerViewReviews.adapter =
                ReviewAdapter(this, it, editData.modelReviews)
        })
    }

    private fun setReviewDescriptionAdapter() {
        viewModel.getReviewDescData().observe(this, Observer {
            mBinding.contentScroll.belowOption.infoViewLay.infoSub.recyclerViewReviewDescription.layoutManager =
                LinearLayoutManager(this)
            mBinding.contentScroll.belowOption.infoViewLay.infoSub.recyclerViewReviewDescription.isNestedScrollingEnabled =
                false
            mBinding.contentScroll.belowOption.infoViewLay.infoSub.recyclerViewReviewDescription.adapter =
                ReviewDescriptionAdapter(this, editData.modelLatestreviews)
        })

    }

    private fun optionsAdapter() {
        viewModel.getFreeList().observe(this, Observer {


            mBinding.contentScroll.belowOption.freeList.layoutManager = LinearLayoutManager(this)
            mBinding.contentScroll.belowOption.freeList.itemAnimator = DefaultItemAnimator()
            mBinding.contentScroll.belowOption.freeList.isNestedScrollingEnabled = false
            mBinding.contentScroll.belowOption.freeList.adapter = AdapterOptions(this, editData, it)

        })

    }

    private fun setClickListeners() {
        mBinding.linearBack.setOnClickListener {
            finish()
        }

        val bottomSheetBehavior = BottomSheetBehavior.from(mBinding.bottomSheet)
        bottomSheetBehavior.peekHeight = 500
        mBinding.showHideBottomSheet.setOnClickListener {
            if (shofull == 0) {
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
                mBinding.line.visibility = View.GONE
                mBinding.downImage.visibility = View.VISIBLE
                shofull = 1
            } else {
                shofull = 0
                mBinding.line.visibility = View.VISIBLE
                mBinding.downImage.visibility = View.GONE
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
            }
        }
        bottomSheetBehavior.setBottomSheetCallback(object :
            BottomSheetBehavior.BottomSheetCallback() {
            @SuppressLint("SwitchIntDef")
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                // React to state change
                when (newState) {
                    BottomSheetBehavior.STATE_COLLAPSED -> {
                        mBinding.contentScroll.nestedScrollView2.scrollTo(0, 0)
                        mBinding.line.visibility = View.VISIBLE
                        mBinding.downImage.visibility = View.GONE
                        shofull = 0
                    }

                    BottomSheetBehavior.STATE_DRAGGING -> {

                    }

                    BottomSheetBehavior.STATE_EXPANDED -> {
                        shofull = 1
                        mBinding.line.visibility = View.GONE
                        mBinding.downImage.visibility = View.VISIBLE
                    }

                    BottomSheetBehavior.STATE_HIDDEN -> {

                    }
                    BottomSheetBehavior.STATE_SETTLING -> {

                    }


                }
            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {
                // React to dragging events
                Log.e("onSlide", "onSlide")
            }
        })
        mBinding.contentScroll.belowOption.infoClick.setTextColor(Color.parseColor("#8f909e"))
        mBinding.contentScroll.belowOption.infoClick.setBackgroundResource(R.drawable.bg_switch_fragments)
        mBinding.contentScroll.belowOption.staffClick.setTextColor(Color.parseColor("#8f909e"))
        mBinding.contentScroll.belowOption.staffClick.setBackgroundResource(R.drawable.bg_switch_fragments)
        mBinding.contentScroll.belowOption.layoutSales.visibility = View.VISIBLE
        mBinding.contentScroll.belowOption.layoutInfo.visibility = View.GONE
        mBinding.contentScroll.belowOption.layoutStaff.visibility = View.GONE

        mBinding.contentScroll.belowOption.saleClick.setOnClickListener {
            mBinding.contentScroll.belowOption.saleClick.setTextColor(resources.getColor(R.color.colorWhite))
            mBinding.contentScroll.belowOption.saleClick.setBackgroundDrawable(
                ContextCompat.getDrawable(
                    this,
                    R.drawable.bg_seller_blue_rounded
                )
            )
            mBinding.contentScroll.belowOption.infoClick.setTextColor(Color.parseColor("#8f909e"))
            mBinding.contentScroll.belowOption.infoClick.setBackgroundResource(R.drawable.bg_switch_fragments)
            mBinding.contentScroll.belowOption.staffClick.setTextColor(Color.parseColor("#8f909e"))
            mBinding.contentScroll.belowOption.staffClick.setBackgroundResource(R.drawable.bg_switch_fragments)
            mBinding.contentScroll.belowOption.layoutSales.visibility = View.VISIBLE
            mBinding.contentScroll.belowOption.layoutInfo.visibility = View.GONE
            mBinding.contentScroll.belowOption.layoutStaff.visibility = View.GONE
        }
        mBinding.contentScroll.belowOption.infoClick.setOnClickListener {
            mBinding.contentScroll.belowOption.infoClick.setTextColor(resources.getColor(R.color.colorWhite))
            mBinding.contentScroll.belowOption.infoClick.setBackgroundDrawable(
                ContextCompat.getDrawable(
                    this,
                    R.drawable.bg_seller_blue_rounded
                )
            )

            mBinding.contentScroll.belowOption.infoViewLay.whatsInclude.text = editData.description
            mBinding.contentScroll.belowOption.saleClick.setTextColor(Color.parseColor("#8f909e"))
            mBinding.contentScroll.belowOption.saleClick.setBackgroundResource(R.drawable.bg_switch_fragments)
            mBinding.contentScroll.belowOption.staffClick.setTextColor(Color.parseColor("#8f909e"))
            mBinding.contentScroll.belowOption.staffClick.setBackgroundResource(R.drawable.bg_switch_fragments)
            mBinding.contentScroll.belowOption.layoutSales.visibility = View.GONE
            mBinding.contentScroll.belowOption.layoutInfo.visibility = View.VISIBLE
            mBinding.contentScroll.belowOption.layoutStaff.visibility = View.GONE

            mBinding.contentScroll.belowOption.infoViewLay.direction.setOnClickListener {
                val latlong = editData.lat + "," + editData.long
                val intent = Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("google.navigation:q=$latlong")
                )
                startActivity(intent);
            }
            mBinding.contentScroll.belowOption.infoViewLay.call.setOnClickListener {
                startActivity(
                    Intent(
                        Intent.ACTION_DIAL,
                        Uri.fromParts("tel", editData.business_phone, null)
                    )
                )

            }
            mBinding.contentScroll.belowOption.infoViewLay.textView114.setOnClickListener {
                try {
                    if (!editData.website.contains("http") || !editData.website.contains("https")) {
                        val browserIntent =
                            Intent(Intent.ACTION_VIEW, Uri.parse("https://$editData.website"))
                        startActivity(browserIntent)
                    } else {
                        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(editData.website))
                        startActivity(browserIntent)
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
//                val emailIntent = Intent(
//                    Intent.ACTION_SENDTO, Uri.fromParts(
//                        "mailto", editData.website, null
//                    )
//                )
//                emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Feedback")
//                emailIntent.putExtra(
//                    Intent.EXTRA_TEXT,
//                    ""
//                )
//                this.startActivity(Intent.createChooser(emailIntent, "Send email"))

            }
            arrayName.clear()
            chipData()

        }




        mBinding.contentScroll.belowOption.staffClick.setOnClickListener {
            mBinding.contentScroll.belowOption.staffClick.setTextColor(resources.getColor(R.color.colorWhite))
            mBinding.contentScroll.belowOption.staffClick.setBackgroundDrawable(
                ContextCompat.getDrawable(
                    this,
                    R.drawable.bg_seller_blue_rounded
                )
            )
            mBinding.contentScroll.belowOption.infoClick.setTextColor(Color.parseColor("#8f909e"))
            mBinding.contentScroll.belowOption.infoClick.setBackgroundResource(R.drawable.bg_switch_fragments)
            mBinding.contentScroll.belowOption.saleClick.setTextColor(Color.parseColor("#8f909e"))
            mBinding.contentScroll.belowOption.saleClick.setBackgroundResource(R.drawable.bg_switch_fragments)
            mBinding.contentScroll.belowOption.layoutSales.visibility = View.GONE
            mBinding.contentScroll.belowOption.layoutInfo.visibility = View.GONE
            mBinding.contentScroll.belowOption.layoutStaff.visibility = View.VISIBLE


        }
        mBinding.contentScroll.options.setOnClickListener {

            val intent = Intent(this, SubInfoList::class.java)
            intent.putParcelableArrayListExtra("list", subInfo)
            startActivity(intent)
//            if (option == 0) {
//                mBinding.contentScroll.belowOption.freeList.visibility = View.GONE
//                option = 1
////                mBinding.contentScroll.rotateImage.rotation = 180f
//            } else {
////                mBinding.contentScroll.rotateImage.rotation = 90f
//                mBinding.contentScroll.belowOption.freeList.visibility = View.GONE
//                option = 0
//            }


        }

        mBinding.contentScroll.optionLay.setOnClickListener {

        }


        mBinding.contentScroll.belowOption.extraListLayout.setOnClickListener {
            if (option2 == 0) {
                mBinding.contentScroll.belowOption.extraList.visibility = View.VISIBLE
                option2 = 1
            } else {
                mBinding.contentScroll.belowOption.extraList.visibility = View.GONE
                option2 = 0
            }
        }

        imageEdit.setOnClickListener {
            val type = Constant.getPrefs(this).getString(Constant.type, "")
            if (type != "3") {
                imageEdit.isClickable = true
                val fragment =
                    MenuFragment(isPublic, status, shareSettings, campaign_id, statusEditData,progressBar)
                fragment.show(supportFragmentManager, fragment.tag)
            } else {
                imageEdit.isClickable = false
            }
//            startActivity(Intent(this, CreateSubscriptionActivity::class.java).putExtra("edit","add"))
        }

//        manage on click

        mBinding.contentScroll.manage.setOnClickListener {
            val type = Constant.getPrefs(this).getString(Constant.type, "")
            if (type != "3") {
                imageEdit.isClickable = true
                val fragment =
                    MenuFragment(
                        isPublic,
                        status,
                        shareSettings,
                        campaign_id,
                        statusEditData,
                        progressBar
                    )
                fragment.show(supportFragmentManager, fragment.tag)
            } else {
                imageEdit.isClickable = false
            }
        }

        mBinding.contentScroll.belowOption.staffViewLay.imageAdd.setOnClickListener {
            val fragment = MenuFragmentStaff()
            fragment.show(supportFragmentManager, fragment.tag)
        }

    }

    fun viewAll(v: View) {
        startActivity(Intent(this, RatingStatsActivity::class.java))
    }

    class MenuFragmentStaff() : RoundedBottomSheetDialogFragment(),
        AddStaffAdapter.SelectionStaffMember {

        private lateinit var backgroundChange: ArrayList<AddStaffModel>
        private lateinit var binding: AddStaffSheetBinding
        private lateinit var viewModel: AddStaffViewModel
        private lateinit var mAdapter: AddStaffAdapter

        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            binding = DataBindingUtil.inflate(inflater, R.layout.add_staff_sheet, container, false)
            viewModel = ViewModelProviders.of(this)[AddStaffViewModel::class.java]

            return binding.root

        }

        override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
            super.onViewCreated(view, savedInstanceState)
            roundedImage()
            binding.reyclerStaff.layoutManager = LinearLayoutManager(activity)
            binding.reyclerStaff.isNestedScrollingEnabled = false
            mAdapter = AddStaffAdapter(activity!!, this)
            binding.reyclerStaff.adapter = mAdapter


            viewModel.getList().observe(this, Observer {
                backgroundChange = it
                mAdapter.update(backgroundChange)
            })


            binding.buttonDone.setOnClickListener {
                dismiss()
            }

        }

        override fun SelectMember(position: Int) {

            val model = backgroundChange[position]
            model.status = !model.status

            backgroundChange[position] = model
            mAdapter.notifyDataSetChanged()

        }

        private fun roundedImage() {
            val curveRadius = 50F

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                binding.imageViewTop.outlineProvider = object : ViewOutlineProvider() {

                    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                    override fun getOutline(view: View?, outline: Outline?) {
                        outline?.setRoundRect(
                            0,
                            0,
                            view!!.width,
                            view.height + curveRadius.toInt(),
                            curveRadius
                        )
                    }
                }
                binding.imageViewTop.clipToOutline = true
            }
        }
    }


    @SuppressLint("SetTextI18n", "ResourceType")
    private fun chipData() {
        arrayName = editData.parentTags


        for (index in arrayName.indices) {
            val chip = Chip(mBinding.contentScroll.belowOption.infoViewLay.infoSub.rvTags.context)
            chip.text = arrayName[index].parentName

            // necessary to get single selection working
            chip.isClickable = true
            chip.isCheckable = false
            chip.setTextAppearanceResource(R.style.ChipTextStyle_Selected)
            chip.setChipBackgroundColorResource(R.color.blue)
            chip.chipEndPadding = 20.0f
            chip.gravity = Gravity.CENTER
            mBinding.contentScroll.belowOption.infoViewLay.infoSub.rvTags.addView(chip)
        }
        mBinding.contentScroll.belowOption.infoViewLay.infoSub.rvTags.isSingleSelection = true


    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == 20) {
            try {
                val callLog = data?.getParcelableArrayListExtra<ModelExtra>("data")
                val newArray: ArrayList<SaveExtraModel> = ArrayList()
                for (i in 0 until callLog!!.size) {
                    mBinding.contentScroll.belowOption.extraLayout.visibility = View.VISIBLE
                    val modelStaffList =
                        SaveExtraModel()
                    modelStaffList.textViewExtra = callLog[i].name
                    modelStaffList.costExtra = callLog[i].price
                    newArray.add(modelStaffList)
                }


                mBinding.contentScroll.belowOption.extraList.layoutManager =
                    LinearLayoutManager(this)
                mBinding.contentScroll.belowOption.extraList.itemAnimator = DefaultItemAnimator()
                mBinding.contentScroll.belowOption.extraList.isNestedScrollingEnabled = false
                mBinding.contentScroll.belowOption.extraList.adapter =
                    SaveExtraAdapter(this, newArray)
            } catch (e: Exception) {

            }
        }
    }


    private fun setViewPagerForTop() {
        myCustomPagerAdapter = CustomerPagerAdapterNew(this)
        mBinding.viewpager.adapter = myCustomPagerAdapter
        //   settingIndicators()

    }

    private fun setToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        imageEdit = toolbar.findViewById(R.id.imageEdit)
        setSupportActionBar(toolbar)
//        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        title = ""
//        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white)

    }

//    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
//        if (item!!.itemId == android.R.id.home) {
//            finish()
//        }
//
//        return super.onOptionsItemSelected(item)
//    }


    fun getExtraActivity(v: View) {

        val intent = Intent(this, ExtrasSellerActivity::class.java)
        startActivityForResult(intent, 20)

    }


    class MenuFragment(
        var public: String,
        var status1: String,
        var shareSetting: String,
        var campaignId: String,
        var statusEditData: String,
        var progressBar: LinearLayout
    ) :
        RoundedBottomSheetDialogFragment() {

        private lateinit var mBindingBottom: ManageSubscriptionBottomBinding
        lateinit var mViewModel: SubscriptionSellerViewModel

        @SuppressLint("SetTextI18n")
        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {

            mBindingBottom = DataBindingUtil.inflate(
                inflater,
                R.layout.manage_subscription_bottom,
                container,
                false
            )
            mViewModel = ViewModelProviders.of(this).get(SubscriptionSellerViewModel::class.java)

            mBindingBottom.btnEdit.setOnClickListener {
                if (statusEditData == "0") {
                    val mPrefs =
                        activity!!.getSharedPreferences("data", MODE_PRIVATE)
                    val prefsEditor = mPrefs.edit()
                    prefsEditor.putString("detailsArray", "")
                    prefsEditor.putString("pricing", "")
                    prefsEditor.putString("basicData", "")
                    prefsEditor.putString("campaign_id", "")
                    prefsEditor.apply()
                    activity!!.startActivity(
                        Intent(activity, CreateSubscriptionActivity::class.java).putExtra(
                            "editReactive",
                            "add"
                        )
                            .putExtra("campaignId", campaignId)
                            .putExtra("showDialog", "show")
                    )
                } else {
                    val mPrefs =
                        activity!!.getSharedPreferences("data", MODE_PRIVATE)
                    val prefsEditor = mPrefs.edit()
                    prefsEditor.putString("detailsArray", "")
                    prefsEditor.putString("pricing", "")
                    prefsEditor.putString("basicData", "")
                    prefsEditor.putString("campaign_id", "")
                    prefsEditor.apply()

                    activity!!.startActivity(
                        Intent(activity, CreateSubscriptionActivity::class.java)
                            .putExtra("edit", "add")
                            .putExtra("campaignId", campaignId)
                            .putExtra("showDialog", "show")
                    )
                }
                dismiss()

            }



            mViewModel.getActiveInactivResponse().observe(this, Observer {
                if (it.status == "true") {
                    mBindingBottom.progressBarBottom.visibility = View.GONE
                    if (it.type == "2") {

                        isPublic = it.sentStatus

                        if (isPublic == "0") {
                            showDialog("Subscription is now viewable in the marketplace", 0)
                        } else {
                            showDialog(
                                "Subscription is no longer viewable in the marketplace, subscription is set to private", 0)
                        }
                    }else if (it.type == "3") {
                        shareSettings = it.sentStatus
                    }else{
                        showDialog("Subscriber limiter updated",1)

                    }

                    //activity!!.finish()
                }
            })
            mBindingBottom.etLimiter.setText(editData.subs)
            var activeStatus = ""
            if (public == "0" && status1 == "1") {
                activeStatus = "1"
                mBindingBottom.titleFour.text = "Active"
                mBindingBottom.switchFour.isChecked = true
                mBindingBottom.subTitleFour.text = "Subscription is active"
            }

            if ((status1 == "0" && public == "1") || (status1 == "1" && public == "1" || status1 == "0" && public == "0")) {
                mBindingBottom.titleFour.text = "Inactive"
                mBindingBottom.switchFour.isChecked = false
                mBindingBottom.subTitleFour.text = "Subscription is deactive"
            }
            if (public == "1") {
                mBindingBottom.titleFour.text = "Inactive"
                mBindingBottom.switchFour.isChecked = false
                mBindingBottom.subTitleFour.text = "Subscription is deactive"
            }

            mBindingBottom.switchThree.isChecked = public != "0"
            mBindingBottom.switchTwo.isChecked = shareSetting != "0"


            if (public == "1") {
                mBindingBottom.subTitleThree.text = "Subscription is not viewable in marketplace"
            } else {
                mBindingBottom.subTitleThree.text = "Subscription is viewable in marketplace"
            }

            mBindingBottom.invite.setOnClickListener {
                val intent = Intent(activity, InviteSubscription::class.java)
                startActivity(intent)
                dismiss()
            }

            mBindingBottom.switchUsers.setOnCheckedChangeListener { p0, p1 ->
                if (p1) {
                    mBindingBottom.textView345.visibility = View.VISIBLE

                } else {
                    mBindingBottom.textView345.visibility = View.GONE
                }

            }

            mBindingBottom.switchThree.setOnCheckedChangeListener { p0, p1 ->
                if (p1) {
                    mBindingBottom.progressBarBottom.visibility = View.VISIBLE
                    mViewModel.setUpdateCampDisStatus(campaignId, "1", "2", "")
                    mBindingBottom.subTitleThree.text =
                        "Subscription is not viewable in marketplace"
                } else {
                    mBindingBottom.progressBarBottom.visibility = View.VISIBLE
                    mViewModel.setUpdateCampDisStatus(campaignId, "0", "2", "")
                    mBindingBottom.subTitleThree.text = "Subscription is viewable in marketplace"
                }

            }


            mBindingBottom.switchFour.setOnClickListener {
                if (bought.isNotEmpty()) {
                    if (bought.toInt() <= 0) {
                        if (activeStatus == "1") {
                            mBindingBottom.progressBarBottom.visibility = View.VISIBLE
                            activeStatus = ""
                            mViewModel.inactiveCampaign(editData.campaign_id, "0")
                        } else {
                            mBindingBottom.progressBarBottom.visibility = View.VISIBLE
                            activeStatus = "1"
                            mViewModel.inactiveCampaign(editData.campaign_id, "1")
                        }
                    } else {
                        mBindingBottom.switchFour.isChecked = true
                        val mDialogView =
                            LayoutInflater.from(activity).inflate(R.layout.custom_dialog, null)
                        val mBuilder = AlertDialog.Builder(activity).setView(mDialogView)
                        val mAlertDialog = mBuilder.show()
                        mAlertDialog.setCancelable(false)
                        mAlertDialog.window.setBackgroundDrawableResource(R.drawable.bg_alert)
                        mAlertDialog.addressText.visibility = View.GONE
                        mAlertDialog.addresssText.text =
                            "Subscription has active users hence cannot be deactivated. You will need to reach out to the subscribers and ask them to manually cancel the subscription in order to deactivate."
                        mAlertDialog.okBtnClick.setOnClickListener {
                            mAlertDialog.dismiss()
                        }
                    }
                } else {
                    if (activeStatus == "1") {
                        mBindingBottom.progressBarBottom.visibility = View.VISIBLE
                        activeStatus = ""
                        mViewModel.inactiveCampaign(editData.campaign_id, "0")
                    } else {
                        mBindingBottom.progressBarBottom.visibility = View.VISIBLE
                        activeStatus = "1"
                        mViewModel.inactiveCampaign(editData.campaign_id, "1")
                    }
                }

            }


//            mBindingBottom.switchFour.setOnCheckedChangeListener { p0, p1 ->
//                if (p1) {
//                    activeStatus = "1"
//                    mViewModel.inactiveCampaign(editData.campaign_id, "1")
//                } else {
//                    activeStatus = ""
//                    mViewModel.inactiveCampaign(editData.campaign_id, "0")
//                }
//
//            }
            mBindingBottom.switchTwo.setOnCheckedChangeListener { p0, p1 ->
                if (p1) {
                    mBindingBottom.progressBarBottom.visibility = View.VISIBLE
                    mViewModel.setUpdateCampDisStatus(campaignId, "1", "3", "")
                    mBindingBottom.subTitleTwo.text = "Customers can share subscription with others"
                } else {
                    mBindingBottom.progressBarBottom.visibility = View.VISIBLE
                    mViewModel.setUpdateCampDisStatus(campaignId, "0", "3", "")
                    mBindingBottom.subTitleTwo.text = "Customers can share subscription with others"
                }

            }

            mBindingBottom.delete.setOnClickListener {
                mBindingBottom.progressBarBottom.visibility = View.VISIBLE
                mViewModel.setUpdateCampDisStatus(
                    editData.campaign_id,
                    "",
                    "4",
                    mBindingBottom.etLimiter.text.toString()
                )
//                dismiss()
//                activity!!.finish()
//                if (public == "0" && status1 == "1") {
//                    val fragment = MenuFragmentInactive("0", public, status1)
//                    val bundle = Bundle()
//                    bundle.putString("campaignId", editData.campaign_id)
//                    bundle.putString("is_public", editData.is_public)
//                    fragment.arguments = bundle
//                    fragment.show(fragmentManager!!, fragment.tag)
//                    dismiss()
//                }
//
//                if ((status1 == "0" && public == "1") ||
//                    (status1 == "1" && public == "1" ||
//                            status1 == "0" && public == "0")
//                ) {
//                    val fragment = MenuFragmentInactive("1", public, status1)
//                    val bundle = Bundle()
//                    bundle.putString("campaignId", editData.campaign_id)
//                    bundle.putString("is_public", editData.is_public)
//                    fragment.arguments = bundle
//                    fragment.show(fragmentManager!!, fragment.tag)
//                    dismiss()
//                }
//                if (public == "1") {
//                    val fragment = MenuFragmentInactive("1", public, status1)
//                    val bundle = Bundle()
//                    bundle.putString("campaignId", editData.campaign_id)
//                    bundle.putString("is_public", editData.is_public)
//                    fragment.arguments = bundle
//                    fragment.show(fragmentManager!!, fragment.tag)
//                    dismiss()
//                }

            }

            mBindingBottom.shareText.setOnClickListener {
                val message =
                    "Come abroad Upscribbr and experience this amazing subscription at a very attractive price.\nhttps://www.Upscribbr.com/sharablelink.php"
                val share = Intent(Intent.ACTION_SEND)
                share.type = "text/plain"
                share.putExtra(Intent.EXTRA_TEXT, message)
                startActivity(Intent.createChooser(share, "Share!"))
            }

            mBindingBottom.copyText.setOnClickListener {
                Toast.makeText(context, "Copied", Toast.LENGTH_SHORT).show()
            }

            mViewModel.getmActiveStatus1().observe(this, Observer {
                if (it.status1 == "true") {
                    mBindingBottom.progressBarBottom.visibility = View.GONE
                    if (activeStatus == "1") {
                        mBindingBottom.switchFour.isChecked = true
                        mBindingBottom.subTitleFour.text = "Subscription is active"
                        mBindingBottom.titleFour.text = "Activate"
                        showDialog("Subscription changed to active", 0)
                    } else {
                        mBindingBottom.progressBarBottom.visibility = View.GONE
                        mBindingBottom.switchFour.isChecked = false
                        mBindingBottom.subTitleFour.text = "Subscription is deactive"
                        mBindingBottom.titleFour.text = "Activate"
                        showDialog("Subscription changed to inactive", 0)
                    }
                }
                //startActivity(Intent(context!!, ConfirmInActiveActivity::class.java))

//                activity!!.finish()
            })

            mViewModel.getStatusCampaign().observe(this, Observer {
                if (it.msg == "Invalid auth code") {
                    mBindingBottom.progressBarBottom.visibility = View.GONE
                    Constant.commonAlert(activity!!)
                }
            })

            mViewModel.getStatus().observe(this, Observer {
                if (it.msg == "Invalid auth code") {
                    mBindingBottom.progressBarBottom.visibility = View.GONE
                    Constant.commonAlert(activity!!)
                } else {
                    mBindingBottom.progressBarBottom.visibility = View.GONE
                    Toast.makeText(activity!!, it.msg, Toast.LENGTH_SHORT).show()
                    mBindingBottom.switchFour.isChecked = activeStatus != "1"
                }
            })

            return mBindingBottom.root
        }

        fun showDialog(body: String, i: Int) {
            val mDialogView =
                LayoutInflater.from(activity).inflate(R.layout.custom_dialog, null)
            val mBuilder = AlertDialog.Builder(activity).setView(mDialogView)
            val mAlertDialog = mBuilder.show()
            mAlertDialog.setCancelable(false)
            mAlertDialog.window.setBackgroundDrawableResource(R.drawable.bg_alert)
            mAlertDialog.addressText.visibility = View.VISIBLE
            mAlertDialog.addressText.text = "Settings Saved"
            mAlertDialog.addresssText.text = body
            mAlertDialog.okBtnClick.setOnClickListener {
                if(i==0){
                    mAlertDialog.dismiss()
                }else{
                    mAlertDialog.dismiss()
                    dismiss()
                    activity!!.finish()
                }

            }
        }

        override fun onDismiss(dialog: DialogInterface) {
            super.onDismiss(dialog)
            mViewModel.getCampaign(campaignId)
        }

        override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
            super.onViewCreated(view, savedInstanceState)
            roundedImage()
        }

        private fun roundedImage() {
            val curveRadius = 50F

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                mBindingBottom.imageViewTop.outlineProvider = object : ViewOutlineProvider() {

                    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                    override fun getOutline(view: View?, outline: Outline?) {
                        outline?.setRoundRect(
                            0,
                            0,
                            view!!.width,
                            view.height + curveRadius.toInt(),
                            curveRadius
                        )
                    }
                }
                mBindingBottom.imageViewTop.clipToOutline = true
            }
        }


    }

    class MenuFragmentInactive(
        var publicId: String,
        var public: String,
        var status1: String
    ) : RoundedBottomSheetDialogFragment() {

        lateinit var mBinding: CreateConfirmInactiveBinding
        lateinit var mViewModel: SubscriptionSellerViewModel


        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            mBinding = DataBindingUtil.inflate(
                inflater,
                R.layout.create_confirm_inactive,
                container,
                false
            )
            mViewModel = ViewModelProviders.of(this).get(SubscriptionSellerViewModel::class.java)

            return mBinding.root
        }

        override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
            super.onViewCreated(view, savedInstanceState)
            roundedImage()

            if (public == "0" && status1 == "1") {
                mBinding.saving.text = "Inactive Subscription"
                mBinding.textView288.text = "Do you want to inactive this subscription?"
            }

            if ((status1 == "0" && public == "1") || (status1 == "1" && public == "1" || status1 == "0" && public == "0")) {
                mBinding.saving.text = "Active Subscription"
                mBinding.textView288.text = "Do you want to active this subscription?"
            }
            if (public == "1") {
                mBinding.saving.text = "Active Subscription"
                mBinding.textView288.text = "Do you want to active this subscription?"
            }


            mBinding.yes.setOnClickListener {
                apiImplimentation()
            }

            observerInit()

            mBinding.no.setOnClickListener {
                dismiss()
            }
        }

        private fun observerInit() {
            mViewModel.getmActiveStatus1().observe(this, Observer {
                startActivity(Intent(context!!, ConfirmInActiveActivity::class.java))
                activity!!.finish()
            })

            mViewModel.getStatusCampaign().observe(this, Observer {
                if (it.msg == "Invalid auth code") {
                    Constant.commonAlert(activity!!)
                } else {
                    Toast.makeText(activity, it.msg, Toast.LENGTH_SHORT).show()

                }
            })

        }

        private fun apiImplimentation() {
            var dd = 0
            dd = publicId.toInt()
            if (dd == 0) {
                mViewModel.inactiveCampaign(editData.campaign_id, "0")
            } else {
                mViewModel.inactiveCampaign(editData.campaign_id, "1")
            }

            dismiss()

        }


        private fun roundedImage() {
            val curveRadius = 50F

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                mBinding.imageViewTop.outlineProvider = object : ViewOutlineProvider() {

                    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                    override fun getOutline(view: View?, outline: Outline?) {
                        outline?.setRoundRect(
                            0,
                            0,
                            view!!.width,
                            view.height + curveRadius.toInt(),
                            curveRadius
                        )
                    }
                }
                mBinding.imageViewTop.clipToOutline = true
            }

        }
    }


    override fun onResume() {
        super.onResume()
        val sharedPreferences2 = getSharedPreferences("customers", Context.MODE_PRIVATE)
        if (sharedPreferences2.getString("customers", "").isNotEmpty()) {
            val editor = sharedPreferences2.edit()
            editor.remove("customers")
            editor.apply()
            finish()
        }

        val sharedPreferences1 = getSharedPreferences("done", Context.MODE_PRIVATE)
        if (sharedPreferences1.getString("done", "").isNotEmpty()) {
            val editor = sharedPreferences1.edit()
            editor.remove("done")
            editor.apply()
            finish()
        }

        mViewModel.getCampaign(campaign_id)
        mViewModel.getCampaignSubscriber(page, campaign_id)


    }


}
