package com.upscribber.upscribberSeller.subsriptionSeller.subscriptionMain

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.createSubs.CreateSubscriptionActivity
import com.upscribber.upscribberSeller.subsriptionSeller.percentage.HundreadPercentActivity
import com.upscribber.upscribberSeller.subsriptionSeller.ratingStats.RatingStatsActivity
import com.upscribber.upscribberSeller.subsriptionSeller.subscriptionView.ActivitySubscriptionSeller
import kotlinx.android.synthetic.main.seller_subscription_card_recycler.view.*
import kotlinx.android.synthetic.main.seller_subscription_card_recycler.view.tv_brazillian
import kotlinx.android.synthetic.main.seller_subscription_card_recycler.view.tv_cost
import kotlinx.android.synthetic.main.seller_subscription_card_recycler.view.tv_last_text
import kotlinx.android.synthetic.main.seller_subscription_card_recycler.view.tv_long_text
import kotlinx.android.synthetic.main.seller_subscription_card_recycler.view.tv_off
import java.math.BigDecimal
import java.math.RoundingMode

class AdapterSellerSearchSubscriptions(
    var context: FragmentActivity,
    listen: SubscriptionSearchResultActivity
) :
    RecyclerView.Adapter<AdapterSellerSearchSubscriptions.MyViewHolder>(), ClickSubs {

    private var items = ArrayList<ModelSellerSubscriptions>()
    var interfaceReload = listen as reloadData
    lateinit var mViewModel: SubscriptionSellerViewModel


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(context)
            .inflate(R.layout.seller_subscription_card_recycler, parent, false)
        mViewModel = ViewModelProviders.of(context)[SubscriptionSellerViewModel::class.java]

        return MyViewHolder(view)

    }

    override fun getItemCount(): Int {
        return items.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = items[position]

        holder.itemView.tv_brazillian.text = model.campaign_name
        val cost = Constant.getCalculatedPrice(model.discount_price, model.price)
        val splitPos = cost.split(".")
        if (splitPos[1] == "00" || splitPos[1] == "0") {
            holder.itemView.tv_cost.text = "$" +  splitPos[0]
        } else {
            holder.itemView.tv_cost.text = "$" + BigDecimal(cost).setScale(2, RoundingMode.HALF_UP)
        }

        if (model.discount_price == "0" || model.discount_price == "0.0" || model.discount_price == "0.00"){
            holder.itemView.tv_off.visibility = View.INVISIBLE
        }else{
            holder.itemView.tv_off.visibility = View.VISIBLE
            holder.itemView.tv_off.text = model.discount_price.toDouble().toInt().toString() + "% off"

        }

        holder.itemView.tv_last_text.text = "$" + model.price.toDouble().toInt()
        holder.itemView.tv_long_text.text = model.description
        holder.itemView.textView105.text = model.bought
        holder.itemView.textView107.text = model.like_percentage + "%"
        holder.itemView.textView111.text = model.views
        val imagePath = Constant.getPrefs(context).getString(Constant.dataImagePath, "")
        holder.itemView.tv_last_text.paintFlags = holder.itemView.tv_last_text.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG


        Glide.with(context).load(imagePath + model.campaign_image)
            .placeholder(R.mipmap.placeholder_subscription)
            .into(holder.itemView.roundedImageView2)
        holder.itemView.textView234.text = model.complete_percentage + " % Complete"
        holder.itemView.setOnClickListener {
            context.startActivity(
                Intent(context, ActivitySubscriptionSeller::class.java)
                    .putExtra("campaignId", model.campaign_id).putExtra("status1", model.status1)
            )
        }

        if (model.is_discounted == "0") {
            holder.itemView.discounted.setTextColor(context.resources.getColor(R.color.colorAccent))
            holder.itemView.discounted.setCompoundDrawablesWithIntrinsicBounds(
                R.drawable.ic_discount_percentage,
                0,
                R.drawable.ic_arrow_red,
                0
            )


        } else {
            holder.itemView.discounted.setTextColor(context.resources.getColor(R.color.line_color))
            holder.itemView.discounted.setCompoundDrawablesWithIntrinsicBounds(
                R.drawable.ic_discount_percentage_green,
                0,
                R.drawable.ic_arrow_seagreen,
                0
            )

        }

        if (model.free_trial == "No free trial" && model.introductory_price == "0") {
            holder.itemView.freeTrail.visibility = View.GONE
        } else if (model.free_trial != "No free trial") {
            holder.itemView.freeTrail.visibility = View.VISIBLE
            holder.itemView.freeTrail.text = "Free"
            holder.itemView.freeTrail.setBackgroundResource(R.drawable.intro_price_background)
        } else {
            holder.itemView.freeTrail.visibility = View.VISIBLE
            holder.itemView.freeTrail.text = "Intro"
            holder.itemView.freeTrail.setBackgroundResource(R.drawable.home_free_background)
        }


        holder.itemView.circularProgressbar.progress = model.complete_percentage.toInt()

        holder.itemView.discounted.setOnClickListener {
            context.startActivity(
                Intent(context, DiscountOnOffActivity::class.java).putExtra(
                    "campaignId",
                    model.campaign_id
                ).putExtra("discountStatus", model.is_discounted)
            )
        }


        if (model.status1 == "0") {
            holder.itemView.textInvite.text = "Edit Subscription"
            if (model.campaign_image.isEmpty()){
                holder.itemView.incompleteImage.setImageResource(R.drawable.ic_incomplete_blue)
            }else{
                holder.itemView.incompleteImage.setImageResource(R.drawable.ic_incomplete)
            }
            holder.itemView.incompleteImage.visibility = View.VISIBLE
            holder.itemView.imageInvite.setImageResource(R.drawable.ic_edit_blue)
        } else {
            holder.itemView.textInvite.text = "Invite Customers"
            holder.itemView.incompleteImage.visibility = View.GONE
            holder.itemView.imageInvite.setImageResource(R.drawable.ic_share_blue_sub)
        }


        holder.itemView.linearAdd.setOnClickListener {
            if (model.status1 == "0") {
                interfaceReload.inactiveBooking(position, model)
            } else {
                interfaceReload.selectSubscription(position, model)
            }
        }


        holder.itemView.linearCustomerCheckOut.setOnClickListener {
            if (model.status1 != "0") {
                interfaceReload.checkoutCustomer(position, model)
            }
        }




        holder.itemView.imageEdit.setOnClickListener {
            if (model.status1 == "0") {
                val mPrefs = context.getSharedPreferences("data", AppCompatActivity.MODE_PRIVATE)
                val prefsEditor = mPrefs.edit()
                prefsEditor.putString("detailsArray", "")
                prefsEditor.putString("pricing", "")
                prefsEditor.putString("basicData", "")
                prefsEditor.putString("campaign_id", "")
                prefsEditor.apply()
                context.startActivity(
                    Intent(context, CreateSubscriptionActivity::class.java).putExtra(
                        "editReactive",
                        "add"
                    ).putExtra("campaignId", model.campaign_id)
                )
            } else {
                val mPrefs = context.getSharedPreferences("data", AppCompatActivity.MODE_PRIVATE)
                val prefsEditor = mPrefs.edit()
                prefsEditor.putString("detailsArray", "")
                prefsEditor.putString("pricing", "")
                prefsEditor.putString("basicData", "")
                prefsEditor.putString("campaign_id", "")
                prefsEditor.apply()

                context.startActivity(
                    Intent(context, CreateSubscriptionActivity::class.java)
                        .putExtra("edit", "add")
                        .putExtra("campaignId", model.campaign_id)
                )
            }

        }


        holder.itemView.textView107.setOnClickListener {
            context.startActivity(Intent(context, RatingStatsActivity::class.java))
        }
        holder.itemView.hunderdClick.setOnClickListener {
            context.startActivity(
                Intent(context, HundreadPercentActivity::class.java).putExtra(
                    "campaignId",
                    model.campaign_id
                )
            )
        }
    }


    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view)

    fun update(items: ArrayList<ModelSellerSubscriptions>) {
        this.items = items
        notifyDataSetChanged()
    }

    override fun onLikeClick() {
        context.startActivity(Intent(context, RatingStatsActivity::class.java))
    }

    override fun hundreadClick() {
        context.startActivity(Intent(context, HundreadPercentActivity::class.java))

    }

    interface reloadData {
        fun inactiveBooking(
            position: Int,
            model: ModelSellerSubscriptions
        )

        fun selectSubscription(position: Int, model: ModelSellerSubscriptions)
        fun checkoutCustomer(position: Int, model: ModelSellerSubscriptions)
    }


}