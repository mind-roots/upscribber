package com.upscribber.upscribberSeller.subsriptionSeller.subscriptionMain

import android.content.Context
import android.graphics.Color
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.R
import com.upscribber.databinding.SubscriptionStatusRecyclerListBinding
import android.widget.LinearLayout



class AdapterStatus(var context: Context, listen: SubscriptionSellerFragment, var type: Int) :
    RecyclerView.Adapter<AdapterStatus.MyViewHolder>() {

    var listener = listen as Selection
    private lateinit var binding: SubscriptionStatusRecyclerListBinding
    private var items = ArrayList<ModelStatus>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        binding = SubscriptionStatusRecyclerListBinding.inflate(LayoutInflater.from(context), parent, false)
        return MyViewHolder(binding)

    }

    override fun getItemCount(): Int {
        return if (type == 1) {
            3
        } else
            items.size

    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = items[position]
        holder.bind(model)

        holder.itemView.setOnClickListener {
            if (position == 0){
                listener.list1(position)
            }else{
                listener.list2(position)
            }
            listener.onSelectionBackground(position)
        }


        val displayMetrics = DisplayMetrics()
        val windowmanager = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        windowmanager.defaultDisplay.getMetrics(displayMetrics)
        val deviceWidth = displayMetrics.widthPixels

        val param = LinearLayout.LayoutParams(
            /*width*/(deviceWidth/3)-30,
            /*height*/ ViewGroup.LayoutParams.WRAP_CONTENT
        )
        binding.linear.layoutParams = param

        if (model.position) {
            holder.linear.setBackgroundResource(R.drawable.bg_seller_blue_padding)
            holder.tvHeading.setTextColor(Color.WHITE)
        } else {
            holder.linear.setBackgroundResource(R.drawable.bg_seller_white)
            holder.tvHeading.setTextColor(Color.parseColor("#8f909e"))
        }
    }

    class MyViewHolder(var binding: SubscriptionStatusRecyclerListBinding) : RecyclerView.ViewHolder(binding.root) {
        var linear: ConstraintLayout = itemView.findViewById(R.id.linear)
        var tvHeading: TextView = itemView.findViewById(R.id.tvHeading)
        fun bind(obj: ModelStatus) {
            binding.model = obj
            binding.executePendingBindings()
        }
    }

    fun update(items: ArrayList<ModelStatus>) {
        this.items = items
        notifyDataSetChanged()
    }

    interface Selection {
        fun onSelectionBackground(position: Int)
        fun list1(position: Int)
        fun list2(position: Int)
    }


}