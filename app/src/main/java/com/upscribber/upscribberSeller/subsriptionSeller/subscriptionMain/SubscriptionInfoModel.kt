package com.upscribber.upscribberSeller.subsriptionSeller.subscriptionMain

import android.os.Parcel
import android.os.Parcelable

class SubscriptionInfoModel() : Parcelable {

    var id = ""
    var campaign_id = ""
    var price = ""
    var discount_price = ""
    var redeemtion_cycle_qty = ""
    var frequency_type = ""
    var free_trial = ""
    var introductory_price = ""
    var introductory_days = ""
    var frequency_value = ""
    var created_at = ""
    var updated_at = ""
    var unit = ""

    constructor(parcel: Parcel) : this() {
        id = parcel.readString()
        campaign_id = parcel.readString()
        price = parcel.readString()
        discount_price = parcel.readString()
        redeemtion_cycle_qty = parcel.readString()
        frequency_type = parcel.readString()
        free_trial = parcel.readString()
        introductory_price = parcel.readString()
        introductory_days = parcel.readString()
        frequency_value = parcel.readString()
        created_at = parcel.readString()
        updated_at = parcel.readString()
        unit = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(campaign_id)
        parcel.writeString(price)
        parcel.writeString(discount_price)
        parcel.writeString(redeemtion_cycle_qty)
        parcel.writeString(frequency_type)
        parcel.writeString(free_trial)
        parcel.writeString(introductory_price)
        parcel.writeString(introductory_days)
        parcel.writeString(frequency_value)
        parcel.writeString(created_at)
        parcel.writeString(updated_at)
        parcel.writeString(unit)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SubscriptionInfoModel> {
        override fun createFromParcel(parcel: Parcel): SubscriptionInfoModel {
            return SubscriptionInfoModel(parcel)
        }

        override fun newArray(size: Int): Array<SubscriptionInfoModel?> {
            return arrayOfNulls(size)
        }
    }
}