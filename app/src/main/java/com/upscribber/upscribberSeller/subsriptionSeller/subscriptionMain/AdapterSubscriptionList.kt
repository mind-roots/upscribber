package com.upscribber.upscribberSeller.subsriptionSeller.subscriptionMain

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.upscribberSeller.navigationCustomer.InviteSubscription
import com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.subscription.ModelSubscriptionCard
import kotlinx.android.synthetic.main.subscription_recycler_layout_invite.view.*

class AdapterSubscriptionList(private val mContext: Context) :
    RecyclerView.Adapter<AdapterSubscriptionList.MyViewHolder>() {

    private var list: ArrayList<ModelSubscriptionCard> = ArrayList()
    private var itAdapter: ModelMain = ModelMain()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(mContext).inflate(R.layout.subscription_recycler_layout_invite, parent, false)
        return MyViewHolder(v)

    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = list[position]


        val imagePath  = Constant.getPrefs(mContext).getString(Constant.dataImagePath, "")
        Glide.with(mContext).load(imagePath + itAdapter.feature_image).into(holder.itemView.ivLogo)
        holder.itemView.tvLogoNum.text = model.redeemtion_cycle_qty
        holder.itemView.tvFab.text = itAdapter.name
        holder.itemView.tvRedeemCost.text = itAdapter.business_name
        holder.itemView.tvRedeem.text = model.frequencyType + "ly"
        holder.itemView.remainingQuantity.text =  "Up to " + model.redeemtion_cycle_qty + " " + model.unit + " Billed " + model.frequency


        holder.itemView.setOnClickListener {
            mContext.startActivity(Intent(mContext, InviteSubscription::class.java).putExtra("model",itAdapter))
        }

    }

    fun update(it: ModelMain) {
        itAdapter=it
        list = it.subscription
        notifyDataSetChanged()

    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

}