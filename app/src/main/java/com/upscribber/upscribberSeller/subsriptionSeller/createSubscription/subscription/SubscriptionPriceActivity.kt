package com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.subscription

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Outline
import android.graphics.Paint
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import android.view.KeyEvent.KEYCODE_ENTER
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.upscribber.commonClasses.Constant
import com.upscribber.R
import com.upscribber.databinding.ActivitySubscriptionPriceBinding
import com.upscribber.databinding.BillingSheetBinding
import com.upscribber.databinding.QuantitySheetBinding
import com.upscribber.commonClasses.RoundedBottomSheetDialogFragment
import com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.createSubs.CreateSubscriptionViewModel
import kotlinx.android.synthetic.main.custom_dialog.*
import java.math.BigDecimal
import java.math.RoundingMode


@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class SubscriptionPriceActivity : AppCompatActivity() {

    private lateinit var mBinding: ActivitySubscriptionPriceBinding
    lateinit var mvModel: CreateSubscriptionViewModel
    private var modelSubscriptionCard = ModelSubscriptionCard()

    companion object {
        var getFrequencyValue = ""
        var getFrequencyTimePeriod = ""
        var getQuantityAmount = ""
        var getQuantityUnitName = ""
    }


    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_subscription_price)
        mvModel = ViewModelProviders.of(this)[CreateSubscriptionViewModel::class.java]

        mBinding.lifecycleOwner = this

        try {
            clickListener()

        } catch (e: Exception) {
            e.printStackTrace()
        }

        if (intent.hasExtra("priceToDisplay")) {
            val priceToDisplay = intent.getStringExtra("priceToDisplay")
            val splitPrice = priceToDisplay.toString().split(".")
            if (splitPrice[1] == "00" || splitPrice[1] == "0") {
                mBinding.eTPrice1.setText("$" + splitPrice[0])
            } else {
                mBinding.eTPrice1.setText(priceToDisplay.toString())
            }

        }

        if (intent.hasExtra("discountToDisplay")) {
            val discountToDisplay = intent.getStringExtra("discountToDisplay")
            mBinding.eTDiscountPrice.text = "$discountToDisplay%"
            try {
                Constant.hideKeyboard(this@SubscriptionPriceActivity, mBinding.eTPrice1)
                val price = mBinding.eTPrice1.text.toString().replace("$", "")
                var retailPrice = price.toDouble()
                val check = mBinding.eTDiscountPrice.text.toString()
                val removePercent = check.replace("%", "")
                val discount = removePercent.toInt()
                val discountPrice = ((discount * retailPrice) / 100)
                retailPrice -= discountPrice


                val splitPos = retailPrice.toString().split(".")
                if (splitPos[1] == "00" || splitPos[1] == "0") {
                    mBinding.subscPrice.text = "$" + splitPos[0]
                    mBinding.textView329.text = "$" + splitPos[0]
                } else {
                    mBinding.subscPrice.text = "$" + BigDecimal(retailPrice).setScale(
                        2,
                        RoundingMode.HALF_UP
                    ).toString()
                    mBinding.textView329.text = "$" + BigDecimal(retailPrice).setScale(
                        2,
                        RoundingMode.HALF_UP
                    ).toString()
                }

                mBinding.tvLastText.paintFlags =
                    mBinding.tvLastText.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
                val merchantDiscount = ((20 * retailPrice) / 100)
                val splitDiscount = merchantDiscount.toString().split(".")
                if (splitDiscount[1] == "00" || splitDiscount[1] == "0") {
                    mBinding.textView330.text = "$" + splitDiscount[0]
                } else {
                    mBinding.textView330.text = "$" + BigDecimal(merchantDiscount).setScale(
                        2,
                        RoundingMode.HALF_UP
                    ).toString()

                }

                val merchantRetail = retailPrice - merchantDiscount
                val splitWhatYouGet = merchantRetail.toString().split(".")
                if (splitWhatYouGet[1] == "00" || splitWhatYouGet[1] == "0") {
                    mBinding.whatYouGet.text = "$" + splitWhatYouGet[0]
                } else {
                    mBinding.whatYouGet.text = "$" + BigDecimal(merchantRetail).setScale(
                        2,
                        RoundingMode.HALF_UP
                    ).toString()

                }

                mBinding.tvLastText.text =
                    "$" + mBinding.eTPrice1.text.toString().replace("$", "").toDouble().toInt()
                mBinding.tvOff.text = check

            } catch (e: Exception) {
                e.printStackTrace()

            }

        }
        mBinding.tvLastText.paintFlags =
            mBinding.tvLastText.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG

        if (intent.hasExtra("model")) {
            modelSubscriptionCard = intent.getParcelableExtra("model")
            if (modelSubscriptionCard.redeemtion_cycle_qty == "500000") {
                mBinding.eTQuantity.text = "Unlimited " + modelSubscriptionCard.quantityName
            } else {
                mBinding.eTQuantity.text =
                    modelSubscriptionCard.redeemtion_cycle_qty + " " + modelSubscriptionCard.quantityName
            }

            getQuantityAmount = modelSubscriptionCard.redeemtion_cycle_qty
            getQuantityUnitName = modelSubscriptionCard.quantityName


//            mBinding.eTFrequency.text =
//                modelSubscriptionCard.frequency + " " + modelSubscriptionCard.frequencyType


            getFrequencyValue = modelSubscriptionCard.frequency
            getFrequencyTimePeriod = modelSubscriptionCard.frequencyType

            if (getFrequencyTimePeriod == "Yearly" || getFrequencyTimePeriod == "Year" || getFrequencyTimePeriod == "Annual") {
//                mBinding.textBilled.text = "Annual"
                mBinding.eTFrequency.text = "Annual"
            } else {
//                mBinding.textBilled.text = "Every $getFrequencyValue $getFrequencyTimePeriod"

                if (getFrequencyValue.isNotEmpty()) {
                    if (getFrequencyValue.toInt() > 1) {
                        mBinding.eTFrequency.text = "Every $getFrequencyValue months"
                    } else if (getFrequencyValue.toInt() == 1) {
                        mBinding.eTFrequency.text = "Once per month"
                    } else {
                        mBinding.eTFrequency.text = "Every $getFrequencyValue month"
                    }
                }

            }

//            val splitPrice = modelSubscriptionCard.price.split(".")
//            if (splitPrice[1] == "00" || splitPrice[1] == "0") {
//                mBinding.eTPrice1.setText("$" + splitPrice[0])
//            } else {
            mBinding.eTPrice1.setText(modelSubscriptionCard.price)
//            }


            mBinding.eTDiscountPrice.text = modelSubscriptionCard.discountPrice
            mBinding.tvOff.text = modelSubscriptionCard.discountPrice
            try {
                var retailPrice = modelSubscriptionCard.price.toDouble()
                val discount = modelSubscriptionCard.discountPrice.toDouble()
                val discountPrice = ((discount * retailPrice) / 100)
                retailPrice -= discountPrice
                mBinding.tvLastText.text = discountPrice.toString()
                val splitPos = retailPrice.toString().split(".")
                if (splitPos[1] == "00" || splitPos[1] == "0") {
                    mBinding.subscPrice.text = "$" + splitPos[0]
                    mBinding.textView329.text = "$" + splitPos[0]
                } else {
                    mBinding.subscPrice.text =
                        "$" + BigDecimal(retailPrice).setScale(2, RoundingMode.HALF_UP).toString()
                    mBinding.textView329.text =
                        "$" + BigDecimal(retailPrice).setScale(2, RoundingMode.HALF_UP).toString()
                }

                mBinding.tvLastText.paintFlags =
                    mBinding.tvLastText.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
                val merchantDiscount = ((20 * retailPrice) / 100)
                val splitDiscount = merchantDiscount.toString().split(".")
                if (splitDiscount[1] == "00" || splitDiscount[1] == "0") {
                    mBinding.textView330.text = "$" + splitDiscount[0]
                } else {
                    mBinding.textView330.text = "$" + BigDecimal(merchantDiscount).setScale(
                        2,
                        RoundingMode.HALF_UP
                    ).toString()

                }

                val merchantRetail = retailPrice - merchantDiscount
                val splitWhatYouGet = merchantRetail.toString().split(".")
                if (splitWhatYouGet[1] == "00" || splitWhatYouGet[1] == "0") {
                    mBinding.whatYouGet.text = "$" + splitWhatYouGet[0]
                } else {
                    mBinding.whatYouGet.text = "$" + BigDecimal(merchantRetail).setScale(
                        2,
                        RoundingMode.HALF_UP
                    ).toString()

                }

            } catch (e: Exception) {
                e.printStackTrace()
            }

            if (modelSubscriptionCard.freeTrail == "No free trial" || modelSubscriptionCard.freeTrail == "") {
                if (modelSubscriptionCard.introPrice != "0" && modelSubscriptionCard.introDays != "0") {
                    mBinding.switch5.isChecked = true
                    mBinding.switch4.isChecked = false
                    mBinding.tvTrail.visibility = View.GONE
                    mBinding.tvTrailValue.visibility = View.GONE
                    mBinding.textView2533.visibility = View.GONE
                    modelSubscriptionCard.freeTrail = "No free trial"
                    mBinding.etPrice.visibility = View.VISIBLE
                    mBinding.etDays.visibility = View.VISIBLE
                    if (modelSubscriptionCard.introDays.isNotEmpty()) {
                        if (modelSubscriptionCard.introDays == "1" || modelSubscriptionCard.introDays == "0") {
                            mBinding.etDays.text = modelSubscriptionCard.introDays + " cycle"
                        } else {
                            mBinding.etDays.text = modelSubscriptionCard.introDays + " cycles"

                        }
                    }
                    mBinding.etPrice.setText(modelSubscriptionCard.introPrice)


                } else {
                    mBinding.switch5.isChecked = false
                    mBinding.switch4.isChecked = false
                    mBinding.tvTrail.visibility = View.GONE
                    mBinding.tvTrailValue.visibility = View.GONE
                    mBinding.textView2533.visibility = View.GONE
                    modelSubscriptionCard.freeTrail = "No free trial"
                    mBinding.etPrice.visibility = View.GONE
                    mBinding.etDays.visibility = View.GONE
                }


            } else {
                mBinding.switch4.isChecked = true
                mBinding.switch5.isChecked = false
                mBinding.tvTrail.visibility = View.VISIBLE
                mBinding.tvTrail.text = modelSubscriptionCard.freeTrail
                mBinding.textView2533.visibility = View.VISIBLE
                mBinding.tvTrailValue.visibility = View.VISIBLE
                mBinding.tvTrailValue.text = modelSubscriptionCard.free_trail_qty
                mBinding.textView254.visibility = View.GONE
                mBinding.etPrice.visibility = View.GONE
                mBinding.etDays.visibility = View.GONE
            }


        }

        setToolbar()
        if (intent.hasExtra("new")) {
            return
        }
//        else {
//            checkIfBought()
//        }

    }

    @SuppressLint("ResourceAsColor")
    private fun checkIfBought() {
        val mPrefs = application.getSharedPreferences("data", MODE_PRIVATE)
        var boughtIsMore = mPrefs.getString("bought", "0")
        if (boughtIsMore.isEmpty()) {
            boughtIsMore = "0"
        }
        if (boughtIsMore.toInt() > 0) {
            //Disable All Fields Except Description
            mBinding.eTQuantity.isClickable = false
            mBinding.eTQuantity.alpha = 0.4F
            mBinding.eTFrequency.isClickable = false
            mBinding.eTFrequency.alpha = 0.4F
            mBinding.eTQuantity.isClickable = false
            mBinding.eTQuantity.alpha = 0.4F
            mBinding.eTDiscountPrice.isClickable = false
            mBinding.eTDiscountPrice.alpha = 0.4F
            mBinding.switch5.isClickable = false
            mBinding.switch5.alpha = 0.4F
            mBinding.switch4.isClickable = false
            mBinding.switch4.alpha = 0.4F
            mBinding.etDays.isClickable = false
            mBinding.etDays.alpha = 0.4F
            mBinding.tvTrailValue.isClickable = false
            mBinding.tvTrailValue.alpha = 0.4F
            mBinding.tvTrail.isClickable = false
            mBinding.tvTrail.alpha = 0.4F
            mBinding.etPrice.isClickable = false
            mBinding.etPrice.isFocusableInTouchMode = false
            mBinding.etPrice.alpha = 0.4F
            mBinding.eTPrice1.isClickable = false
            mBinding.eTPrice1.isFocusableInTouchMode = false
            mBinding.eTPrice1.alpha = 0.4F


        }


    }

    @SuppressLint("SetTextI18n")
    private fun clickListener() {
        mBinding.eTFrequency.setOnClickListener {
            Constant.hideKeyboard(this, mBinding.eTFrequency)
            val fragment = MenuFragmentBilling(mBinding.eTFrequency, mBinding.etDays)
            fragment.show(supportFragmentManager, fragment.tag)
        }

        mBinding.imageView1.setOnClickListener {
            Constant.CommonIAlert(
                this,
                "Quantity",
                "Specify how many redemption your customer gets with this subscription and enter the unit name for this product or service. For example, 1 haircut, 5 coffees, 2 manicures, etc."
            )
        }

        mBinding.imageView2.setOnClickListener {
            Constant.CommonIAlert(
                this,
                "Billing Cycle",
                "Specify how frequently your customer will be charged for a subscription."
            )
        }

        mBinding.imageView3.setOnClickListener {
            Constant.CommonIAlert(
                this,
                "Retail Price",
                "What is the price of your product? Typically this will be the price a customer pays for your product without a discount."
            )
        }


        mBinding.imageView4.setOnClickListener {
            Constant.CommonIAlert(
                this,
                "Discount Price",
                "Adding a discount to your subscription will keep customers a higher chance of purchasing your plan."
            )
        }

        mBinding.imageView33.setOnClickListener {
            Constant.CommonIAlert(
                this,
                "Introductory Price",
                "Specify the introductory price and the length of the introductory period"
            )
        }


        mBinding.imageView32.setOnClickListener {
            Constant.CommonIAlert(
                this,
                "Free Trial",
                "Let your customers try your product or service for free. Don’t worry – you can limit what they get."
            )
        }


        mBinding.eTQuantity.setOnClickListener {
            Constant.hideKeyboard(this, mBinding.eTFrequency)
            val fragment = MenuFragmentQuantity(mBinding.eTQuantity)
            fragment.show(supportFragmentManager, fragment.tag)
        }

        mBinding.etPrice.setOnClickListener{
            if (mBinding.eTPrice1.text.toString().trim().isEmpty() && mBinding.eTDiscountPrice.text.toString().trim().isEmpty()){
                mBinding.etPrice.isFocusable = false
                Constant.hideKeyboard(this@SubscriptionPriceActivity, mBinding.etPrice)
                val mDialogView = LayoutInflater.from(this@SubscriptionPriceActivity).inflate(R.layout.custom_dialog, null)
                val mBuilder = android.app.AlertDialog.Builder(this@SubscriptionPriceActivity).setView(mDialogView)
                val mAlertDialog = mBuilder.show()
                mAlertDialog.setCancelable(false)
                mAlertDialog.window.setBackgroundDrawableResource(R.drawable.bg_alert)
                mAlertDialog.addressText.visibility = View.GONE
                mAlertDialog.addresssText.text =
                    "Please add retail price and discount first."
                mAlertDialog.okBtnClick.setOnClickListener {
                    mAlertDialog.dismiss()
                }
            }
        }



        mBinding.etPrice.setOnEditorActionListener { v, actionId, event ->
            if (event != null && event.keyCode === KEYCODE_ENTER || actionId == EditorInfo.IME_ACTION_DONE) {
                if (mBinding.etPrice.text.toString().trim() >= mBinding.textView329.text.toString().trim()) {
                    mBinding.etPrice.setText("")
                    Constant.hideKeyboard(this@SubscriptionPriceActivity, mBinding.etPrice)
                    val mDialogView = LayoutInflater.from(this@SubscriptionPriceActivity).inflate(R.layout.custom_dialog, null)
                    val mBuilder = android.app.AlertDialog.Builder(this@SubscriptionPriceActivity).setView(mDialogView)
                    val mAlertDialog = mBuilder.show()
                    mAlertDialog.setCancelable(false)
                    mAlertDialog.window.setBackgroundDrawableResource(R.drawable.bg_alert)
                    mAlertDialog.addressText.visibility = View.GONE
                    mAlertDialog.addresssText.text =
                        "Introductory Price can't be higher than subscription price."
                    mAlertDialog.okBtnClick.setOnClickListener {
                        mAlertDialog.dismiss()
                    }
                }
            }
            false
        }


        mBinding.eTDiscountPrice.setOnClickListener {
            Constant.hideKeyboard(this, mBinding.eTDiscountPrice)
            val choices = getList()
            val charSequenceItems = choices.toArray(arrayOfNulls<CharSequence>(choices.size))
            val mBuilder = AlertDialog.Builder(this)
            mBuilder.setSingleChoiceItems(charSequenceItems, -1) { dialogInterface, i ->
                mBinding.eTDiscountPrice.text = choices[i]
                if (mBinding.eTPrice1.text.isNotEmpty()) {
                    var retailPrice = mBinding.eTPrice1.text.toString().replace("$", "").toDouble()
                    val check = choices[i].replace("%", "")
                    val discount = check.toInt()
                    val discountPrice = ((discount * retailPrice) / 100)
                    retailPrice -= discountPrice

                    val splitPos = retailPrice.toString().split(".")
                    if (splitPos[1] == "00" || splitPos[1] == "0") {
                        mBinding.subscPrice.text = "$" + splitPos[0]
                        mBinding.textView329.text = "$" + splitPos[0]
                    } else {
                        mBinding.subscPrice.text = "$" + BigDecimal(retailPrice).setScale(
                            2,
                            RoundingMode.HALF_UP
                        ).toString()
                        mBinding.textView329.text = "$" + BigDecimal(retailPrice).setScale(
                            2,
                            RoundingMode.HALF_UP
                        ).toString()
                    }

                    mBinding.tvLastText.paintFlags =
                        mBinding.tvLastText.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
                    val merchantDiscount = ((20 * retailPrice) / 100)
                    val splitDiscount = merchantDiscount.toString().split(".")
                    if (splitDiscount[1] == "00" || splitDiscount[1] == "0") {
                        mBinding.textView330.text = "$" + splitDiscount[0]
                    } else {
                        mBinding.textView330.text = "$" + BigDecimal(merchantDiscount).setScale(
                            2,
                            RoundingMode.HALF_UP
                        ).toString()

                    }

                    val merchantRetail = retailPrice - merchantDiscount
                    val splitWhatYouGet = merchantRetail.toString().split(".")
                    if (splitWhatYouGet[1] == "00" || splitWhatYouGet[1] == "0") {
                        mBinding.whatYouGet.text = "$" + splitWhatYouGet[0]
                    } else {
                        mBinding.whatYouGet.text = "$" + BigDecimal(merchantRetail).setScale(
                            2,
                            RoundingMode.HALF_UP
                        ).toString()

                    }
                    mBinding.tvLastText.text =
                        "$" + mBinding.eTPrice1.text.toString().replace("$", "").toDouble().toInt()
                    mBinding.tvOff.text = choices[i]

                }

                dialogInterface.dismiss()

            }
            val mDialog = mBuilder.create()
            mDialog.show()
        }

        mBinding.eTPrice1.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if (mBinding.eTDiscountPrice.text.isNotEmpty()) {
                    try {
                        val price = p0.toString()
                        var retailPrice = price.toDouble()
                        val check = mBinding.eTDiscountPrice.text.toString()
                        val removePercent = check.replace("%", "")
                        val discount = removePercent.toDouble()
                        val discountPrice = ((discount * retailPrice) / 100)
                        retailPrice -= discountPrice


                        val splitPos = retailPrice.toString().split(".")
                        if (splitPos[1] == "00" || splitPos[1] == "0") {
                            mBinding.subscPrice.text = "$" + splitPos[0]
                            mBinding.textView329.text = "$" + splitPos[0]
                        } else {
                            mBinding.subscPrice.text = "$" + BigDecimal(retailPrice).setScale(
                                2,
                                RoundingMode.HALF_UP
                            ).toString()
                            mBinding.textView329.text = "$" + BigDecimal(retailPrice).setScale(
                                2,
                                RoundingMode.HALF_UP
                            ).toString()
                        }

                        mBinding.tvLastText.paintFlags =
                            mBinding.tvLastText.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
                        val merchantDiscount = ((20 * retailPrice) / 100)
                        val splitDiscount = merchantDiscount.toString().split(".")
                        if (splitDiscount[1] == "00" || splitDiscount[1] == "0") {
                            mBinding.textView330.text = "$" + splitDiscount[0]
                        } else {
                            mBinding.textView330.text = "$" + BigDecimal(merchantDiscount).setScale(
                                2,
                                RoundingMode.HALF_UP
                            ).toString()

                        }

                        val merchantRetail = retailPrice - merchantDiscount
                        val splitWhatYouGet = merchantRetail.toString().split(".")
                        if (splitWhatYouGet[1] == "00" || splitWhatYouGet[1] == "0") {
                            mBinding.whatYouGet.text = "$" + splitWhatYouGet[0]
                        } else {
                            mBinding.whatYouGet.text = "$" + BigDecimal(merchantRetail).setScale(
                                2,
                                RoundingMode.HALF_UP
                            ).toString()

                        }

                        mBinding.tvLastText.text =
                            "$" + mBinding.eTPrice1.text.toString().toDouble().toInt()
                        mBinding.tvOff.text = check

                    } catch (e: Exception) {
                        e.printStackTrace()

                    }
                }

                if (p0!!.isEmpty()) {
                    var check = mBinding.eTDiscountPrice.text.toString()
                    if (check.isEmpty()) {
                        check = "0"
                    }
                    mBinding.tvLastText.paintFlags =
                        mBinding.tvLastText.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
                    mBinding.tvLastText.text = "$0"
                    mBinding.whatYouGet.text = "$0"
                    mBinding.textView330.text = "$0"
                    mBinding.textView329.text = "$0"
                    mBinding.subscPrice.text = "$0"
                    mBinding.tvOff.text = check
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

        })

        mBinding.eTPrice1.setOnEditorActionListener { v, actionId, event ->
            var handled = false

            if ((event != null && event.keyCode == KeyEvent.KEYCODE_ENTER) ||
                (actionId == EditorInfo.IME_ACTION_DONE)
            ) {
                handled = true
                if (mBinding.eTPrice1.text.isNotEmpty() &&
                    mBinding.eTDiscountPrice.text.isNotEmpty()
                ) {

                    try {
                        Constant.hideKeyboard(this@SubscriptionPriceActivity, mBinding.eTPrice1)
                        val price = mBinding.eTPrice1.text.toString().replace("$", "")
                        var retailPrice = price.toDouble()
                        val check = mBinding.eTDiscountPrice.text.toString()
                        val removePercent = check.replace("%", "")
                        val discount = removePercent.toInt()
                        val discountPrice = ((discount * retailPrice) / 100)
                        retailPrice -= discountPrice


                        val splitPos = retailPrice.toString().split(".")
                        if (splitPos[1] == "00" || splitPos[1] == "0") {
                            mBinding.subscPrice.text = "$" + splitPos[0]
                            mBinding.textView329.text = "$" + splitPos[0]
                        } else {
                            mBinding.subscPrice.text = "$" + BigDecimal(retailPrice).setScale(
                                2,
                                RoundingMode.HALF_UP
                            ).toString()
                            mBinding.textView329.text = "$" + BigDecimal(retailPrice).setScale(
                                2,
                                RoundingMode.HALF_UP
                            ).toString()
                        }

                        mBinding.tvLastText.paintFlags =
                            mBinding.tvLastText.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
                        val merchantDiscount = ((20 * retailPrice) / 100)
                        val splitDiscount = merchantDiscount.toString().split(".")
                        if (splitDiscount[1] == "00" || splitDiscount[1] == "0") {
                            mBinding.textView330.text = "$" + splitDiscount[0]
                        } else {
                            mBinding.textView330.text = "$" + BigDecimal(merchantDiscount).setScale(
                                2,
                                RoundingMode.HALF_UP
                            ).toString()

                        }

                        val merchantRetail = retailPrice - merchantDiscount
                        val splitWhatYouGet = merchantRetail.toString().split(".")
                        if (splitWhatYouGet[1] == "00" || splitWhatYouGet[1] == "0") {
                            mBinding.whatYouGet.text = "$" + splitWhatYouGet[0]
                        } else {
                            mBinding.whatYouGet.text = "$" + BigDecimal(merchantRetail).setScale(
                                2,
                                RoundingMode.HALF_UP
                            ).toString()

                        }

                        mBinding.tvLastText.text =
                            "$" + mBinding.eTPrice1.text.toString().toDouble().toInt()
                        mBinding.tvOff.text = check

                    } catch (e: Exception) {
                        e.printStackTrace()

                    }
                } else {
                    Constant.hideKeyboard(this@SubscriptionPriceActivity, mBinding.eTPrice1)
                }

            }


            handled
        }


        mBinding.tvTrail.setOnClickListener {
            if (mBinding.switch4.isChecked) {
                Constant.hideKeyboard(this, mBinding.eTDiscountPrice)
                val choices = getTrailList()
                val charSequenceItems = choices.toArray(arrayOfNulls<CharSequence>(choices.size))
                val mBuilder = AlertDialog.Builder(this)
                mBuilder.setSingleChoiceItems(charSequenceItems, -1) { dialogInterface, i ->
                    mBinding.tvTrail.text = choices[i]
                    dialogInterface.dismiss()
                }
                val mDialog = mBuilder.create()
                mDialog.show()
            }
        }
        mBinding.tvTrailValue.setOnClickListener {
            if (mBinding.eTQuantity.text.toString().isEmpty()) {
                Toast.makeText(this, "You haven't select any quantity above.", Toast.LENGTH_LONG)
                    .show()
                return@setOnClickListener
            } else {
                Constant.hideKeyboard(this, mBinding.tvTrailValue)
                val choices = getListForQuantity(getQuantityAmount)
                val charSequenceItems = choices.toArray(arrayOfNulls<CharSequence>(choices.size))
                val mBuilder = AlertDialog.Builder(this)
                mBuilder.setSingleChoiceItems(charSequenceItems, -1) { dialogInterface, i ->
                    mBinding.tvTrailValue.text = choices[i]
                    dialogInterface.dismiss()

                }
                val mDialog = mBuilder.create()
                mDialog.show()

            }

        }
        mBinding.etDays.setOnClickListener {
            if (mBinding.eTFrequency.text.toString().isEmpty()) {
                Toast.makeText(
                        this,
                        "You haven't select any billing cycle above.",
                        Toast.LENGTH_LONG
                    )
                    .show()
                return@setOnClickListener
            } else {
                Constant.hideKeyboard(this, mBinding.etDays)
                val choices = getListDays(getFrequencyValue)
                val charSequenceItems = choices.toArray(arrayOfNulls<CharSequence>(choices.size))
                val mBuilder = AlertDialog.Builder(this)
                mBuilder.setSingleChoiceItems(charSequenceItems, -1) { dialogInterface, i ->
                    if (choices[i] == "1") {
                        mBinding.etDays.text = choices[i] + " cycle"
                    } else {
                        mBinding.etDays.text = choices[i] + " cycles"
                    }
                    dialogInterface.dismiss()
                }
                val mDialog = mBuilder.create()
                mDialog.show()

            }

        }

        mBinding.textView252.setOnClickListener {

            if (checkBeforeSaving() == 1) {
                return@setOnClickListener
            }
            val retailPrice = mBinding.eTPrice1.text.toString().replace("$", "")
            val getDiscount = mBinding.eTDiscountPrice.text.toString()

            val discount = getDiscount.replace("%", "")
            var setFrequencyType = ""
            val arrayList = ArrayList<ModelSubscriptionCard>()
            when (getFrequencyTimePeriod.trim()) {
                "month" -> setFrequencyType = "1"
                "Year" -> setFrequencyType = "2"
                "months" -> setFrequencyType = "1"
                "Monthly" -> setFrequencyType = "1"
                "Yearly" -> setFrequencyType = "2"
                "Annual" -> setFrequencyType = "2"
            }

            if (mBinding.switch4.isChecked) {
                modelSubscriptionCard.freeTrail = mBinding.tvTrail.text.toString()
                modelSubscriptionCard.free_trail_qty = mBinding.tvTrailValue.text.toString()
            } else {
                modelSubscriptionCard.freeTrail = "No free trial"
                modelSubscriptionCard.free_trail_qty = "0"
            }
            /*if (modelSubscriptionCard.free_trail_qty.trim().isNotEmpty()) {

            }*/
            if (mBinding.switch5.isChecked) {

                var introductoryDay = mBinding.etDays.text.toString()
                //remove cycle from sending to api
                if (introductoryDay.contains("cycles")) {
                    introductoryDay = introductoryDay.replace(" cycles", "")
                } else if (introductoryDay.contains("cycle")) {
                    introductoryDay = introductoryDay.replace(" cycle", "")
                }
                val introductoryPrice = mBinding.etPrice.text.toString()
                modelSubscriptionCard.introPrice = introductoryPrice
                modelSubscriptionCard.introDays = introductoryDay
                modelSubscriptionCard.freeTrail = "No free trial"
            } else {
                modelSubscriptionCard.introPrice = "0"
                modelSubscriptionCard.introDays = "0"
            }


            modelSubscriptionCard.pricingNumber = "Weekly Pricing"
            modelSubscriptionCard.redeemtion_cycle_qty = getQuantityAmount
            modelSubscriptionCard.quantityName = getQuantityUnitName
            modelSubscriptionCard.frequency = getFrequencyValue
            modelSubscriptionCard.frequencyType = setFrequencyType
            modelSubscriptionCard.price = retailPrice
            modelSubscriptionCard.discountPrice = discount

            arrayList.add(modelSubscriptionCard)
            val editor = Constant.getSharedPrefs(this).edit()
            editor.putBoolean("empty1", false)
            editor.apply()
            if (intent.hasExtra("model")) {
                val intent = Intent()
                intent.putExtra("data", arrayList)
                setResult(201, intent)
            } else {
                val intent = Intent()
                intent.putExtra("data", arrayList)
                setResult(101, intent)
            }
            getFrequencyValue = ""
            getFrequencyTimePeriod = ""
            getQuantityAmount = ""
            getQuantityUnitName = ""
            finish()
        }



        mBinding.switch5.setOnCheckedChangeListener { p0, p1 ->
            if (p1) {
                mBinding.switch5.isChecked = p1
                mBinding.switch4.isChecked = false
                mBinding.textView254.visibility = View.VISIBLE
                mBinding.etPrice.visibility = View.VISIBLE
                mBinding.textView255.visibility = View.VISIBLE
                mBinding.etDays.visibility = View.VISIBLE

            } else {
                mBinding.textView254.visibility = View.GONE
                mBinding.etPrice.visibility = View.GONE
                mBinding.textView255.visibility = View.GONE
                mBinding.etDays.visibility = View.GONE
                mBinding.etDays.text = ""
                mBinding.etPrice.setText("")
            }
        }

        mBinding.switch4.setOnCheckedChangeListener { p0, p1 ->
            if (p1) {
                mBinding.switch4.isChecked = p1
                mBinding.switch5.isChecked = false
                mBinding.textView250.visibility = View.VISIBLE
                mBinding.tvTrail.visibility = View.VISIBLE
                mBinding.textView2533.visibility = View.VISIBLE
                mBinding.tvTrailValue.visibility = View.VISIBLE
            } else {
                mBinding.tvTrail.text = ""
                mBinding.tvTrailValue.text = ""
                mBinding.textView250.visibility = View.GONE
                mBinding.tvTrail.visibility = View.GONE
                mBinding.tvTrailValue.visibility = View.GONE
                mBinding.textView2533.visibility = View.GONE
            }
        }


    }

    private fun getListForQuantity(limit: String): ArrayList<String> {
        val arrayList: ArrayList<String> = ArrayList()

        if (limit == "Unlimited") {
            for (i in 0 until 101) {
                if (i == 0) {
                    arrayList.add("Unlimited")
                } else {
                    if (i < 101) {

                        arrayList.add((i).toString())

                    }
                }
            }
            return arrayList
        } else {
            for (i in 0 until limit.toInt()) {

                if (i <= limit.toInt()) {

                    arrayList.add((i + 1).toString())

                }

            }
        }

        return arrayList
    }

    //Check before saving that same subscription is already added or not
    private fun checkBeforeSaving(): Int {
        val subArray = intent.getParcelableArrayListExtra<ModelSubscriptionCard>("subArray")
        var value = ""
        if (subArray.size > 0) {
            val eTQuantity = mBinding.eTQuantity.text.toString().trim()
            var eTFrequency = mBinding.eTFrequency.text.toString().trim()
            if ("months" in eTFrequency || "month" in eTFrequency) {
                value = "$getFrequencyValue Monthly"
            } else if ("Annual" in eTFrequency) {
                value = "$getFrequencyValue Yearly"
            }
            var doone = 0
            if ("month" in eTFrequency || "months" in eTFrequency || "Month" in eTFrequency || "Months" in eTFrequency) {
                doone = 1
            }
            var doone2 = 0
            if ("Yearly" in eTFrequency || "Annual" in eTFrequency) {
                doone2 = 1
            }
            if (doone == 0) {
                if ("month" in eTFrequency || "months" in eTFrequency || "Month" in eTFrequency || "Months" in eTFrequency) {
                    eTFrequency = eTFrequency.replace("Month", "Monthly")
                    //tem.frequencyType="Yearly"
                }
            }
            if (doone2 == 0) {
                if ("Year" in eTFrequency || "Yearly" in eTFrequency || "Annual" in eTFrequency) {
                    eTFrequency = eTFrequency.replace("Year", "Yearly")
                    //tem.frequencyType="Yearly"
                }
            }
            var exist = 0
            if (intent.hasExtra("model")) {
                val model = intent.getParcelableExtra<ModelSubscriptionCard>("model")
                for (item in subArray) {
                    if (item.frequencyType == "2" || item.frequencyType == "Year" || item.frequencyType == "Annual" || item.frequencyType == "Yearly") {
                        item.frequencyType = "Yearly"
                    }
                    if (item.frequencyType == "1" || item.frequencyType == "month" || item.frequencyType == "months" || item.frequencyType == "Monthly") {
                        item.frequencyType = "Monthly"
                    }
                    if (model.id != item.id) {
                        if (eTQuantity.toLowerCase() == item.redeemtion_cycle_qty.toLowerCase() + " " + item.quantityName.toLowerCase() &&
                            value.toLowerCase() == item.frequency.toLowerCase() + " " + item.frequencyType.toLowerCase()
                        ) {
                            exist = 1
                            break
                        }
                    }
                }
                if (exist == 1) {
                    Toast.makeText(
                        this,
                        "You already have pricing setup with the same unit name and unit number.",
                        Toast.LENGTH_LONG
                    ).show()
                    return 1
                }
            } else {
                for (item in subArray) {
                    if (item.frequencyType == "2" || item.frequencyType == "Year" || item.frequencyType == "Annual" || item.frequencyType == "Yearly") {
                        item.frequencyType = "Yearly"
                    }
                    if (item.frequencyType == "1" || item.frequencyType == "month" || item.frequencyType == "months" || item.frequencyType == "Monthly") {
                        item.frequencyType = "Monthly"
                    }

                    if (eTQuantity.toLowerCase() == item.redeemtion_cycle_qty.toLowerCase() + " " + item.quantityName.toLowerCase() &&
                        value.toLowerCase() == item.frequency.toLowerCase() + " " + item.frequencyType.toLowerCase()
                    ) {
                        exist = 1
                        break
                    }

                }
                if (exist == 1) {
                    Toast.makeText(
                        this,
                        "You already have pricing setup with the same unit name and unit number.",
                        Toast.LENGTH_LONG
                    ).show()
                    return 1
                }


            }
        }
        return 0
    }

    private fun getTrailList(): ArrayList<String> {
        val arrayList: ArrayList<String> = ArrayList()

        for (i in 0 until 5) {
            when (i) {
                0 -> arrayList.add("1 Week")
                1 -> arrayList.add("2 Weeks")
                2 -> arrayList.add("1 Month")
                3 -> arrayList.add("3 Months")
                else -> arrayList.add("6 Months")
            }
        }

        return arrayList


    }

    private fun getListDays(limit: String): ArrayList<String> {
        val arrayList: ArrayList<String> = ArrayList()


        for (i in 0 until limit.toInt()) {

            if (i <= limit.toInt()) {
                arrayList.add((i + 1).toString())
            }

        }

//        for (i in 0 until 5) {
//            when (i) {
//                0 -> arrayList.add("7 Days")
//                1 -> arrayList.add("14 Days")
//                2 -> arrayList.add("30 Days")
//                3 -> arrayList.add("60 Days")
//                else -> arrayList.add("90 Days")
//            }
//        }

        return arrayList


    }

    private fun getList(): ArrayList<String> {
        val arrayList: ArrayList<String> = ArrayList()

        for (i in 1..80) {
            if (i % 5 == 0)
                arrayList.add((i).toString() + "%")
        }


        return arrayList

    }

    class MenuFragmentBilling(
        var eTFrequency: TextView,
        var etDays: TextView
    ) : RoundedBottomSheetDialogFragment() {

        lateinit var mBinding: BillingSheetBinding
        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            mBinding = DataBindingUtil.inflate(inflater, R.layout.billing_sheet, container, false)

            return mBinding.root
        }


        @SuppressLint("SetTextI18n")
        override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
            super.onViewCreated(view, savedInstanceState)
            roundedImage()
            var valueDuration = getFrequencyTimePeriod
            var duration = getFrequencyValue


            if (getFrequencyTimePeriod == "Yearly" || getFrequencyTimePeriod == "Year" || getFrequencyTimePeriod == "Annual") {
//                mBinding.textBilled.text = "Annual"
                mBinding.editText5.text = "Annual"
            } else {
//                mBinding.textBilled.text = "Every $getFrequencyValue $getFrequencyTimePeriod"

                if (getFrequencyValue.isNotEmpty()) {
                    if (getFrequencyValue.toInt() > 1) {
                        mBinding.editText5.text = "Every $getFrequencyValue months"
                    } else if (getFrequencyValue.toInt() == 1) {
                        mBinding.editText5.text = "Once every month"
                    } else {
                        mBinding.editText5.text = "Every $getFrequencyValue month"
                    }
                }

            }


//            when {
//                getFrequencyValue == "1" -> {
//                    mBinding.textBilled.text = "$getFrequencyValue $getFrequencyTimePeriod"
//                }
//                getFrequencyValue != "1" && getFrequencyTimePeriod != "" -> {
//                    mBinding.textBilled.text = "$getFrequencyValue $getFrequencyTimePeriod" + "s"
//                }
//                else -> {
//                    mBinding.textBilled.text = "$getFrequencyValue $getFrequencyTimePeriod"
//                }
//            }

            mBinding.imageView.setOnClickListener {
                Constant.CommonIAlert(
                    activity!!,
                    "Billing Frequency",
                    "Specify the period that the customer get billed."
                )
            }


            mBinding.imageView1.setOnClickListener {
                Constant.CommonIAlert(
                    activity!!,
                    "Duration",
                    "Specify how long will the subscription be offered to the subscription."
                )
            }


            mBinding.editText5.setOnClickListener {
                Constant.hideKeyboard(activity!!, mBinding.editText5)
                val choices = getNewArrayList()
                val charSequenceItems = choices.toArray(arrayOfNulls<CharSequence>(choices.size))
                val mBuilder = AlertDialog.Builder(activity!!)
                mBuilder.setSingleChoiceItems(charSequenceItems, -1) { dialogInterface, i ->
                    mBinding.editText5.text = choices[i]


                    when {
                        choices[i].contains("months") -> {
                            duration = (i + 1).toString()
                            valueDuration = "months"
                        }
                        choices[i].contains("month") -> {
                            duration = (i + 1).toString()
                            valueDuration = "month"
                        }
                        choices[i].contains("Annual") -> {
                            duration = "1"
                            valueDuration = "Annual"
                        }


                        //                    val value = mBinding.editText6.text.toString()
                        //                    when {
                        //                        value == "1" -> {
                        //                            mBinding.textBilled.text =
                        //                                value + " " + mBinding.editText5.text
                        //                        }
                        //                        value != "1" && mBinding.editText5.text.toString() != "" -> {
                        //                            mBinding.textBilled.text =
                        //                                value + " " + mBinding.editText5.text + "s"
                        //                        }
                        //                        else -> {
                        //                            mBinding.textBilled.text =
                        //                                value + " " + mBinding.editText5.text
                        //                        }
                        //                    }
                    }


//                    val value = mBinding.editText6.text.toString()
//                    when {
//                        value == "1" -> {
//                            mBinding.textBilled.text =
//                                value + " " + mBinding.editText5.text
//                        }
//                        value != "1" && mBinding.editText5.text.toString() != "" -> {
//                            mBinding.textBilled.text =
//                                value + " " + mBinding.editText5.text + "s"
//                        }
//                        else -> {
//                            mBinding.textBilled.text =
//                                value + " " + mBinding.editText5.text
//                        }
//                    }

                    dialogInterface.dismiss()

                }
                val mDialog = mBuilder.create()
                mDialog.show()
            }


            mBinding.editText6.setOnClickListener {
                Constant.hideKeyboard(activity!!, mBinding.editText6)
                val choices = getListDuration()
                val charSequenceItems = choices.toArray(arrayOfNulls<CharSequence>(choices.size))
                val mBuilder = AlertDialog.Builder(activity!!)
                mBuilder.setSingleChoiceItems(charSequenceItems, -1) { dialogInterface, i ->
                    mBinding.editText6.text = choices[i]
                    when {
                        i == 0 -> {
                            mBinding.textBilled.text =
                                mBinding.editText6.text.toString() + " " + mBinding.editText5.text
                        }
                        i != 0 && mBinding.editText5.text.toString() != "" -> {
                            mBinding.textBilled.text =
                                mBinding.editText6.text.toString() + " " + mBinding.editText5.text + "s"
                        }
                        else -> {
                            mBinding.editText6.text.toString() + " " + mBinding.editText5.text
                        }
                    }

                    dialogInterface.dismiss()

                }
                val mDialog = mBuilder.create()
                mDialog.show()
            }

            if (eTFrequency.text.toString().isNotEmpty()) {
                val separated = eTFrequency.text.split(" ")
                mBinding.editText6.text = separated[0]
                var str = ""
                for (i in separated.indices) {
                    if (i > 0) {
                        str = str + " " + separated[i]
                    }
                }
                //valueDuration = str
//                mBinding.editText5.text = str

            }
            mBinding.textView252.setOnClickListener {
                etDays.text = ""
                eTFrequency.text = mBinding.editText5.text.toString()
                getFrequencyValue = duration
                getFrequencyTimePeriod = valueDuration

                dismiss()
            }


        }

//        private fun getList(): ArrayList<String> {
//            val arrayList: ArrayList<String> = ArrayList()
//
//            for (i in 0 until 2) {
//                when (i) {
//                    0 -> arrayList.add("Month")
//                    else -> arrayList.add("Year")
//                }
//            }
//
//            return arrayList
//
//        }

        private fun getListDuration(): ArrayList<String> {
            val arrayList: ArrayList<String> = ArrayList()

            for (i in 1 until 12) {
                arrayList.add(i.toString())
            }

            return arrayList

        }

        private fun getNewArrayList(): ArrayList<String> {
            val arrayList: ArrayList<String> = ArrayList()

            for (i in 1 until 13) {
                when (i) {
                    1 -> arrayList.add("Once per month")
                    12 -> arrayList.add("Annual")
                    else -> arrayList.add("Every $i months")
                }
            }
            return arrayList
        }

        private fun roundedImage() {
            val curveRadius = 50F

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                mBinding.imageViewTop.outlineProvider = object : ViewOutlineProvider() {

                    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                    override fun getOutline(view: View?, outline: Outline?) {
                        outline?.setRoundRect(
                            0,
                            0,
                            view!!.width,
                            view.height + curveRadius.toInt(),
                            curveRadius
                        )
                    }
                }
                mBinding.imageViewTop.clipToOutline = true
            }

        }

    }


    class MenuFragmentQuantity(var eTQuantity: TextView) : RoundedBottomSheetDialogFragment() {
        lateinit var mBinding: QuantitySheetBinding


        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            mBinding = DataBindingUtil.inflate(inflater, R.layout.quantity_sheet, container, false)

            return mBinding.root
        }

        @SuppressLint("SetTextI18n")
        override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
            super.onViewCreated(view, savedInstanceState)
            roundedImage()

            mBinding.editText6.setOnClickListener {
                Constant.hideKeyboard(activity!!, mBinding.editText6)
                val choices = getList()
                val charSequenceItems = choices.toArray(arrayOfNulls<CharSequence>(choices.size))
                val mBuilder = AlertDialog.Builder(activity!!)
                mBuilder.setSingleChoiceItems(charSequenceItems, -1) { dialogInterface, i ->
                    mBinding.editText6.text = choices[i]
                    mBinding.textBilled.text =
                        mBinding.editText6.text.toString() + " " + mBinding.editText5.text
                    dialogInterface.dismiss()
                }
                val mDialog = mBuilder.create()
                mDialog.show()
            }

            mBinding.imageView.setOnClickListener {
                Constant.CommonIAlert(
                    activity!!,
                    "Amount",
                    "Specify the price that you want to charge as per the units being offered."
                )
            }


            mBinding.imageView1.setOnClickListener {
                Constant.CommonIAlert(
                    activity!!,
                    "Unit Name",
                    "Enter the name of the service being offered."
                )
            }




            mBinding.editText5.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {
                    mBinding.textBilled.text =
                        mBinding.editText6.text.toString() + " " + s
                }

                override fun beforeTextChanged(
                    s: CharSequence?,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                }

            })
            val separated = eTQuantity.text.trim().split(" ")
            if (separated.isNotEmpty()) {
                mBinding.editText6.text = separated[0]
            }
            if (separated.size > 1) {
                mBinding.editText5.setText(separated[1])
            }

            mBinding.textView252.setOnClickListener {
                eTQuantity.text =
                    mBinding.editText6.text.toString() + " " + mBinding.editText5.text.toString()
                getQuantityAmount = mBinding.editText6.text.toString()
                getQuantityUnitName = mBinding.editText5.text.toString()

                dismiss()
            }

        }

        private fun getList(): ArrayList<String> {
            val arrayList: ArrayList<String> = ArrayList()

            for (i in 0 until 101) {
                if (i == 0) {
                    arrayList.add("Unlimited")
                } else {
                    if (i < 101) {

                        arrayList.add((i).toString())

                    }
                }
            }

            return arrayList
        }


        private fun roundedImage() {
            val curveRadius = 50F

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                mBinding.imageViewTop.outlineProvider = object : ViewOutlineProvider() {

                    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                    override fun getOutline(view: View?, outline: Outline?) {
                        outline?.setRoundRect(
                            0,
                            0,
                            view!!.width,
                            view.height + curveRadius.toInt(),
                            curveRadius
                        )
                    }
                }
                mBinding.imageViewTop.clipToOutline = true
            }

        }

    }

    @SuppressLint("SetTextI18n")
    private fun setToolbar() {
        setSupportActionBar(mBinding.toolbarPrice.toolbar)
        title = ""
        mBinding.toolbarPrice.title.text = "Subscription Price"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)

    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            getFrequencyValue = ""
            getFrequencyTimePeriod = ""
            getQuantityAmount = ""
            getQuantityUnitName = ""
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        getFrequencyValue = ""
        getFrequencyTimePeriod = ""
        getQuantityAmount = ""
        getQuantityUnitName = ""
    }


}
