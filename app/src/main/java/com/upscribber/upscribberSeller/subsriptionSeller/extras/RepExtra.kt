package com.upscribber.upscribberSeller.subsriptionSeller.extras

import android.app.Application
import androidx.lifecycle.MutableLiveData

class RepExtra(application: Application) {
    var mData = MutableLiveData<ArrayList<ModelExtra>>()
    var array = ArrayList<ModelExtra>()
    fun getValues(): ArrayList<ModelExtra> {
        var model = ModelExtra()
        model.name = "Facial"
        model.price = "$45"
        array.add(model)

        model = ModelExtra()
        model.name = "French Tips"
        model.price = "$45"
        array.add(model)

        model = ModelExtra()
        model.name = "Gel Nails"
        model.price = "$45"
        array.add(model)

        model = ModelExtra()
        model.name = "Shave"
        model.price = "$35"
        array.add(model)

        mData.value = array
        return array
    }

    fun updateValues(model: ModelExtra): MutableLiveData<ArrayList<ModelExtra>> {

        array.add(model)
        mData.value = array
        return mData
    }

    fun edit(model: ModelExtra, i: Int): MutableLiveData<ArrayList<ModelExtra>> {

        array[i] = model
        mData.value = array
        return mData
    }

    fun getArray(): MutableLiveData<ArrayList<ModelExtra>> {

        return mData
    }
}