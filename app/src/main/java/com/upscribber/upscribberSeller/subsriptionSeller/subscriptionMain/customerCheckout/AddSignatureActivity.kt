package com.upscribber.upscribberSeller.subsriptionSeller.subscriptionMain.customerCheckout

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.text.Spannable
import android.text.SpannableString
import android.text.Spanned
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.text.style.UnderlineSpan
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.github.gcacace.signaturepad.views.SignaturePad
import com.upscribber.R
import com.upscribber.checkout.CheckOutViewModel
import com.upscribber.commonClasses.Constant
import com.upscribber.dashboard.DashboardActivity
import com.upscribber.payment.paymentCards.WebPagesOpenActivity
import com.upscribber.upscribberSeller.subsriptionSeller.subscriptionMain.ModelSellerSubscriptions
import kotlinx.android.synthetic.main.activity_add_signature.*
import java.io.*
import java.math.BigDecimal
import java.math.RoundingMode
import java.text.SimpleDateFormat
import java.util.*


@Suppress("DEPRECATED_IDENTITY_EQUALS", "NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class AddSignatureActivity : AppCompatActivity() {

    lateinit var signaturePad: SignaturePad
    lateinit var mViewModel: CheckOutViewModel
    lateinit var toolbartitle: TextView
    private val REQUEST_EXTERNAL_STORAGE = 1
    var showHide = 0
    var signed = false
    private val PERMISSIONS_STORAGE = arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE)
    var file: File? = null

    @SuppressLint("SourceLockedOrientationActivity")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_signature)
        signaturePad = findViewById(R.id.signaturePad)
        mViewModel = ViewModelProviders.of(this)[CheckOutViewModel::class.java]
        toolbartitle = findViewById(R.id.title)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        verifyStoragePermissions(this)
        cancelBtn.isEnabled = false
        spannableString()
        setData()
        setSignature()
        setToolbar()
        clickListener()
        observerInit()
    }

    private fun spannableString() {
        val spannableString = SpannableString("I agree to the Upscribbr LLC's Terms of Service and Privacy Policy")


        spannableString.setSpan(
            clickableSpanforSigleText, 31, 47, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        //2nd click
        spannableString.setSpan(
            clickableSpanforPrivacyPolicy, 52, 66, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        )

        spannedText.movementMethod = LinkMovementMethod.getInstance()


        spannableString.setSpan(
            ForegroundColorSpan(resources.getColor(R.color.blue)),
            31, 47,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )

        spannableString.setSpan(
            ForegroundColorSpan(resources.getColor(R.color.blue)),
            52, 66,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )

        spannableString.setSpan(UnderlineSpan(), 31, 47, 0)
        spannedText.text = spannableString

        spannableString.setSpan(UnderlineSpan(), 52, 66, 0)
        spannedText.text = spannableString

    }


    val clickableSpanforSigleText = object : ClickableSpan() {
        @SuppressLint("SetJavaScriptEnabled")
        override fun onClick(textView: View) {
            startActivity(
                Intent(
                    this@AddSignatureActivity,
                    WebPagesOpenActivity::class.java
                ).putExtra("terms", "terms")
            )

        }

        override fun updateDrawState(ds: TextPaint) {
            super.updateDrawState(ds)
            ds.isUnderlineText = false
        }
    }

    //set the click on the spannable
    val clickableSpanforPrivacyPolicy = object : ClickableSpan() {
        @SuppressLint("SetJavaScriptEnabled")
        override fun onClick(textView: View) {
            startActivity(
                Intent(
                    this@AddSignatureActivity,
                    WebPagesOpenActivity::class.java
                ).putExtra("privacy", "privacy")
            )

        }
    }

    private fun setData() {
        if (intent.hasExtra("modelCampaigns")) {
            val model = intent.getParcelableExtra<ModelSellerSubscriptions>("modelCampaigns")
            var frequency = ""
            frequency = when (model.frequency_type) {
                "1" -> {
                    "Monthly"
                }
                "2" -> {
                    "Yearly"
                }
                "Year" -> {
                    "Yearly"
                }
                "Month" -> {
                    "Monthly"
                }
                else -> {
                    model.frequency_type
                }
            }
//            textView110.text = model.campaign_name
            if (model.quantity >"100"){
                textView290.text = "Unlimited " + model.unit
            }else{
                textView290.text = model.quantity + " " + model.unit
            }


            val c = Calendar.getInstance()
            val year = c.get(Calendar.YEAR)
            val date = c.get(Calendar.DAY_OF_MONTH)
            val hour = c.get(Calendar.HOUR_OF_DAY)
            val minut = c.get(Calendar.MINUTE)
            val second = c.get(Calendar.SECOND)
            val month = c.get(Calendar.MONTH) + 1

            val sortedDate = "$year-$month-$date $hour:$minut:$second"

            var ourDate = sortedDate
            val formatter = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
            val dateFormatter =
                SimpleDateFormat("dd MMM yyyy", Locale.getDefault()) //this format changeable

            try {
                formatter.timeZone = TimeZone.getTimeZone("UTC")
                val value = formatter.parse(ourDate)

                dateFormatter.timeZone = TimeZone.getDefault()
                ourDate = dateFormatter.format(value)
                textView328.text = "Your subscription will renewal $frequency starting $ourDate"
                textViewCalender.text = "Your subscription will renewal $frequency starting $ourDate"
            } catch (e: Exception) {
                ourDate = "00-00-0000 00:00"
            }


            var price1 = ""
            if (model.price.contains(".")){
                val price = model.price.toDouble().toString().split(".")
                if (price[1] == "00" || price[1] == "0") {
                    price1 =  price[0]
                }else{
                    price1 = BigDecimal(model.price).setScale(2, RoundingMode.HALF_UP).toString()
                }
            }else{
                price1 = "" + BigDecimal(model.price).setScale(2, RoundingMode.HALF_UP)
            }

            val imagePath = Constant.getPrefs(this).getString(Constant.dataImagePath, "")
            Glide.with(this).load(imagePath + model.business_logo).placeholder(R.mipmap.placeholder_subscription_square)
                .into(imageSubscription)

            campaignName.text = model.campaign_name


            if (model.free_trial != "No free trial"){
                freeIntro.visibility = View.VISIBLE
                freeIntro.setBackgroundResource(R.drawable.bg_cash_earned)
                freeIntro.text = "Free Trail"
            }else if (model.introductory_days != "0"){
                freeIntro.setBackgroundResource(R.drawable.bg_intro_price)
                freeIntro.visibility = View.VISIBLE
                freeIntro.text = "Intro Price"
            }else{
                freeIntro.visibility = View.GONE
            }
        }


        if (showHide == 0){
            include10.setBackgroundColor(resources.getColor(R.color.colorWhite))
            val slideInLeft = AnimationUtils.loadAnimation(this, R.anim.slide_up)
            contraintLayout.startAnimation(slideInLeft)
            contraintLayout.visibility = View.GONE
            textView328.visibility = View.VISIBLE
            mainConstraint.visibility = View.GONE
        }else{
            contraintLayout.visibility = View.VISIBLE
            textView328.visibility = View.GONE
            mainConstraint.visibility = View.VISIBLE
            include10.setBackgroundColor(resources.getColor(R.color.grey_transparent))
            val slideInLeft = AnimationUtils.loadAnimation(this, R.anim.slide_down)
            contraintLayout.startAnimation(slideInLeft)
        }

        if (intent.hasExtra("modelAddSignature")) {
            val modelSignature =
                intent.getParcelableExtra<ModelAddSignatureData>("modelAddSignature")
            textView358.text =
                "I agree to pay the amount above as per the  cardholder and/or merchant agreement. Card ending in " + modelSignature.lastFour
        }



    }

    private fun observerInit() {
        mViewModel.getmDataBuySubscription().observe(this, Observer {
            progressBar.visibility = View.GONE
            if (it.status == "true") {
                progressBar.visibility = View.GONE
//                Toast.makeText(this, "subscribbed", Toast.LENGTH_SHORT).show()
                val sharedPreferences =
                    getSharedPreferences("dashLayoutMerchant", Context.MODE_PRIVATE)
                val editor = sharedPreferences.edit()
                editor.putString("dashLayoutMerchantt", "2")
                editor.apply()
                val intent = Intent(this, DashboardActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                intent.putExtra("showDialog", "0")
                intent.putExtra("model", it)
                startActivity(intent)
                finish()
            }
        })


        mViewModel.getStatus().observe(this, Observer {
            progressBar.visibility = View.GONE
            if (it.msg == "Invalid auth code") {
                Constant.commonAlert(this)
            } else {
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
                progressBar.visibility = View.GONE
//                Toast.makeText(this, "subscribbed", Toast.LENGTH_SHORT).show()
                val sharedPreferences =
                    getSharedPreferences("dashLayoutMerchant", Context.MODE_PRIVATE)
                val editor = sharedPreferences.edit()
                editor.putString("dashLayoutMerchantt", "2")
                editor.apply()
                val intent = Intent(this, DashboardActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                intent.putExtra("showDialog", "0")
                intent.putExtra("failure", "failure")
                startActivity(intent)
                finish()
            }
        })


    }

    private fun verifyStoragePermissions(activity: AddSignatureActivity) {

        val permission = ActivityCompat.checkSelfPermission(
            activity,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        )

        if (permission != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                activity,
                PERMISSIONS_STORAGE,
                REQUEST_EXTERNAL_STORAGE
            )

        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            REQUEST_EXTERNAL_STORAGE -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.isEmpty()
                    || grantResults[0] !== PackageManager.PERMISSION_GRANTED
                ) {
                    Toast.makeText(
                        this,
                        "Cannot write images to external storage",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
    }

    private fun clickListener() {
        nextCheckout.setOnClickListener {
            val signatureBitmap = signaturePad.signatureBitmap
            file = addJpgSignatureToGallery(signatureBitmap)
            val new_user = Constant.getPrefs(this).getString(Constant.new_user, "")
            if (intent.hasExtra("modelAddSignature")) {
                val modelSignature = intent.getParcelableExtra<ModelAddSignatureData>("modelAddSignature")
                if (!checkBox3.isChecked){
                    Toast.makeText(this,"Please agree to terms & conditions to continue",Toast.LENGTH_SHORT).show()
                }else if (!signed){
                    Toast.makeText(this,"Please signature",Toast.LENGTH_SHORT).show()
                }else{
                    progressBar.visibility = View.VISIBLE
                    mViewModel.getBuySubscriptionMercahnt(
                        modelSignature.user_id,
                        modelSignature.sub_total.replace("$","").replace("-",""),
                        modelSignature.total.replace("$","").replace("-",""),
                        modelSignature.subscription_id,
                        modelSignature.tax,
                        modelSignature.first_name,
                        modelSignature.email,
                        modelSignature.street,
                        modelSignature.state,
                        modelSignature.city,
                        modelSignature.zip,
                        modelSignature.card_id,
                        modelSignature.billingType,
                        modelSignature.billingStreet,
                        modelSignature.billingState,
                        modelSignature.billingCity,
                        modelSignature.billingZip,
                        modelSignature.freeTrial,
                        modelSignature.total_amount.replace("$","").replace("-",""),
                        modelSignature.is_intro,
                        modelSignature.contact_no,
                        modelSignature.actual_price.replace("$","").replace("-",""),
                        modelSignature.buy_type,
                        file!!,new_user,
                        modelSignature.last4,
                        modelSignature.payment_type,
                        modelSignature.payment_method_name,
                        modelSignature.credits
                    )
                }
            }
        }


        textView328.setOnClickListener {
            showHide = 1
            contraintLayout.visibility = View.VISIBLE
            textView328.visibility = View.GONE
            mainConstraint.visibility = View.VISIBLE
            val slideInLeft = AnimationUtils.loadAnimation(this, R.anim.slide_down)
            contraintLayout.startAnimation(slideInLeft)
        }

        textViewCalender.setOnClickListener {
            showHide = 0
            contraintLayout.visibility = View.GONE
            textView328.visibility = View.VISIBLE
            mainConstraint.visibility = View.GONE
            val slideInLeft = AnimationUtils.loadAnimation(this, R.anim.slide_up)
            contraintLayout.startAnimation(slideInLeft)
        }


//        cancelBtn.setOnClickListener {
//            val mDialogView =
//                LayoutInflater.from(this).inflate(R.layout.cancel_merchant_checkout, null)
//            val mBuilder = AlertDialog.Builder(this)
//                .setView(mDialogView)
//            val mAlertDialog = mBuilder.show()
//            val isSeller =
//                Constant.getSharedPrefs(this).getBoolean(Constant.IS_SELLER, false)
//            if (isSeller) {
//                mAlertDialog.no.setTextColor(resources.getColor(R.color.blue))
//                mAlertDialog.yes.setTextColor(resources.getColor(R.color.blue))
//            } else {
//                mAlertDialog.no.setTextColor(resources.getColor(R.color.accent))
//                mAlertDialog.yes.setTextColor(resources.getColor(R.color.accent))
//            }
//
//            mAlertDialog.no.setOnClickListener {
//                mAlertDialog.dismiss()
//            }
//            mAlertDialog.setCancelable(false)
//            mAlertDialog.window.setBackgroundDrawableResource(R.drawable.bg_alert_new)
//            mAlertDialog.yes.setOnClickListener {
//                mAlertDialog.dismiss()
//                val sharedPreferences =
//                    getSharedPreferences("dashLayoutCancelMerchant", Context.MODE_PRIVATE)
//                val editor = sharedPreferences.edit()
//                editor.putString("dashLayoutCancelMerchantt", "3")
//                editor.apply()
//                val intent = Intent(this, DashboardActivity::class.java)
//                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
//                startActivity(intent)
//                finish()
//
//            }
//
//
//        }

    }


    private fun addJpgSignatureToGallery(signature: Bitmap): File {
        var result: File? = null
        try {
            val photo = File(
                getAlbumStorageDir("SignaturePad"),
                //String.format("Signature_%d.jpg", System.currentTimeMillis())
                String.format("Signature_photo.jpg")
            )
            saveBitmapToJPG(signature, photo)
            scanMediaFile(photo)
            result = photo
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return result!!

    }

    private fun saveBitmapToJPG(signature: Bitmap, photo: File) {
        val newBitmap = Bitmap.createBitmap(
            signature.width,
            signature.height,
            Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(newBitmap)
        canvas.drawColor(Color.WHITE)
        canvas.drawBitmap(signature, 0.0f, 0.0f, null)
        val stream: OutputStream = FileOutputStream(photo)
        newBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream)
        stream.close()

    }

    private fun scanMediaFile(photo: File) {
        val mediaScanIntent = Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE)
        val contentUri: Uri = Uri.fromFile(photo)
        mediaScanIntent.data = contentUri
        this.sendBroadcast(mediaScanIntent)

    }

    private fun getAlbumStorageDir(s: String): File? {

        val file = File(
            Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES
            ), s
        )
        if (!file.mkdirs()) {
            Log.e("SignaturePad", "Directory not created")
        }
        return file
    }


    private fun setSignature() {
        signaturePad.setOnSignedListener(object : SignaturePad.OnSignedListener {
            override fun onStartSigning() {
                signed = true
                textView363.visibility = View.GONE
                imageView88.visibility = View.GONE

            }

            override fun onSigned() {

                cancelBtn.isEnabled = true
            }

            override fun onClear() {
                signed = false
                textView363.visibility = View.VISIBLE
                imageView88.visibility = View.VISIBLE
                cancelBtn.isEnabled = false
            }
        })


        cancelBtn.setOnClickListener {
            textView363.visibility = View.VISIBLE
            imageView88.visibility = View.VISIBLE
            signaturePad.clear()
        }

    }

    private fun setToolbar() {
        if (intent.hasExtra("modelAddSignature")) {
            val modelSignature = intent.getParcelableExtra<ModelAddSignatureData>("modelAddSignature")
            if (modelSignature.total.contains(".")){
                val splitProductPrice =modelSignature.total.split(".")
                if (splitProductPrice[1] == "00" || splitProductPrice[1] == "0") {
                    toolbartitle.text = "$" + splitProductPrice[0]
                } else {
                    toolbartitle.text = "$" +BigDecimal(modelSignature.total).setScale(2, RoundingMode.HALF_UP).toString()
                }
            }else{
                toolbartitle.text = "$" + modelSignature.total
            }
        }

        setSupportActionBar(include10)
        title = ""
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

}

