package com.upscribber.upscribberSeller.subsriptionSeller.location.days

import android.os.Parcel
import android.os.Parcelable

class ModelSelectDays() : Parcelable{

    var checked: String = ""
    var status: String = ""
    var day: String = ""
    var checkBox : Boolean = false
    var headings : String = ""
    var closeTime : String = ""
    var openTime : String = ""

    constructor(parcel: Parcel) : this() {
        checked = parcel.readString()
        status = parcel.readString()
        day = parcel.readString()
        checkBox = parcel.readByte() != 0.toByte()
        headings = parcel.readString()
        closeTime = parcel.readString()
        openTime = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(checked)
        parcel.writeString(status)
        parcel.writeString(day)
        parcel.writeByte(if (checkBox) 1 else 0)
        parcel.writeString(headings)
        parcel.writeString(closeTime)
        parcel.writeString(openTime)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ModelSelectDays> {
        override fun createFromParcel(parcel: Parcel): ModelSelectDays {
            return ModelSelectDays(parcel)
        }

        override fun newArray(size: Int): Array<ModelSelectDays?> {
            return arrayOfNulls(size)
        }
    }

}