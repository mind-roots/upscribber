package com.upscribber.upscribberSeller.subsriptionSeller.addFeatures

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

class AddFeaturesRepository(application: Application) {
    fun getList(): LiveData<ArrayList<ModelAddFeatures>> {

        val mData = MutableLiveData<ArrayList<ModelAddFeatures>>()
        val arrayList = ArrayList<ModelAddFeatures>()

        var modelAddFeatures = ModelAddFeatures()
//        modelAddFeatures.featureName = "Add Add On's"
//        arrayList.add(modelAddFeatures)

       // modelAddFeatures = ModelAddFeatures()
        modelAddFeatures.featureName = "Add free trial"
        arrayList.add(modelAddFeatures)

        modelAddFeatures = ModelAddFeatures()
        modelAddFeatures.featureName = "Add intro price"
        arrayList.add(modelAddFeatures)

        mData.value = arrayList
        return mData
    }

    fun   getListCreate(): LiveData<ArrayList<ModelAddFeatures>> {
        val mData = MutableLiveData<ArrayList<ModelAddFeatures>>()
        val arrayList = ArrayList<ModelAddFeatures>()

        var modelCreate = ModelAddFeatures()
        modelCreate.featureName = "Add the Basic"
        modelCreate.featureDescription = "Add the name of your subscription, select the products to include and add relevant pictures. "
        arrayList.add(modelCreate)

        modelCreate = ModelAddFeatures()
        modelCreate.featureName = "Add the Pricing"
        modelCreate.featureDescription = "Add the quantity of redemptions, the billing cycle, frequency, the retail price of the service, and the discounted price your customers get with a subscription. Plus, specify promotional pricing options, like free trials and introductory prices."
        arrayList.add(modelCreate)

        modelCreate = ModelAddFeatures()
        modelCreate.featureName = "Add the Details"
        modelCreate.featureDescription = "Add subscription description and applicable locations, assign team members, add relevant tags and explore special options – like subscriber limiter, privacy and sharing."
        arrayList.add(modelCreate)

        mData.value = arrayList
        return mData

    }

}