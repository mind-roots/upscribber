package com.upscribber.upscribberSeller.subsriptionSeller.location.DaysLogic

class DayModel {
     var day:String=""
     var time:String=""

    override fun toString(): String {
        return "$day     $time"
    }
}