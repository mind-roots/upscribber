package com.upscribber.upscribberSeller.subsriptionSeller.subscriptionMain.customerCheckout

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.upscribber.R
import kotlinx.android.synthetic.main.activity_confirm_customer_checkout.*

class ConfirmCustomerCheckoutActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_confirm_customer_checkout)
        setData()
        clickListener()
    }

    private fun clickListener() {
        deniedLinkDone.setOnClickListener {
            val sharedPreferences = getSharedPreferences("dashLayoutMerchantt", Context.MODE_PRIVATE)
            val editor = sharedPreferences.edit()
            editor.putString("dashLayoutMerchantt","subscriptionMerchant")
            editor.apply()
            finish()
        }

    }

    private fun setData() {
        if (intent.hasExtra("failure")){
            description.text = "Please update your payment details to continue."
            textView204.text = "Payment Declined"
            imageView26.setImageResource(R.drawable.ic_link_denied)

        }else{
            description.text = "Subscription purchased for the customer.Please ask customer to check the subscription in their \"My Subscription\" Tab"
            textView204.text = "Customer Upscribbed"
            imageView26.setImageResource(R.drawable.ic_successful_redeem)

        }


    }
}
