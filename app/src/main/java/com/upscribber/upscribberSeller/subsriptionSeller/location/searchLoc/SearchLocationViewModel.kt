package com.upscribber.upscribberSeller.subsriptionSeller.location.searchLoc

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.upscribber.commonClasses.ModelStatusMsg
import org.json.JSONArray

class SearchLocationViewModel (application: Application) : AndroidViewModel(application) {

    var searchLocationRepository : SearchLocationRepository = SearchLocationRepository(application)

    fun getAllLocations(auth: String, search: String) {
        searchLocationRepository.getlocationList(auth,search)
    }

    fun getStatus(): LiveData<ModelStatusMsg> {
        return searchLocationRepository.getmDataFailure()
    }


     fun getmDataDelete(): LiveData<ModelStatusMsg> {
        return searchLocationRepository.getmDataDelete()
    }

  fun getmDataDefault(): LiveData<ModelStatusMsg> {
        return searchLocationRepository.getmDataDefault()
    }




    fun getLocationData(): MutableLiveData<ArrayList<ModelSearchLocation>> {
        return searchLocationRepository.getLocationData()
    }

      fun getAddLocationData(): MutableLiveData<ModelSearchLocation>{
        return searchLocationRepository.getAddLocationData()
    }



    fun getAddLocation(
        auth: String,
        id: String,
        address: String,
        city: String,
        state: String,
        zip: String,
        arrayOperationHours: JSONArray,
        latitude: String,
        longitude: String
    ) {
        searchLocationRepository.getAddLocation(auth,id,address,city,state,zip,arrayOperationHours,latitude,longitude)

    }

    fun getDeleteLocation(auth: String, idddd: String, default: String) {
        searchLocationRepository.getDeleteLocation(auth,idddd,default)

    }

    fun getPrimaryLocation(auth: String, id: String) {
        searchLocationRepository.getPrimaryLocation(auth,id)
    }

}