package com.upscribber.upscribberSeller.subsriptionSeller.ratingStats

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.upscribber.subsciptions.merchantStore.ModelReviewDescription
import com.upscribber.R
import kotlinx.android.synthetic.main.layout_reviews_description.view.*

class AdapterReviewDescSeller(
    private val mContext: Context,
    private val list: ArrayList<ModelReviewDescription>
) : RecyclerView.Adapter<AdapterReviewDescSeller.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(mContext)
            .inflate(
                R.layout.layout_reviews_description
                , parent, false
            )
        return MyViewHolder(v)

    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = list[position]
        Glide.with(mContext).load(model.profile_image).into(holder.itemView.imageView)
        Glide.with(mContext).load(model.type).into(holder.itemView.imageViewGesture)

        holder.itemView.memberNameReview.text= model.name
        holder.itemView.tVDateTime.text= model.updated_at
        holder.itemView.reviewText.text= model.review
    }

   class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}