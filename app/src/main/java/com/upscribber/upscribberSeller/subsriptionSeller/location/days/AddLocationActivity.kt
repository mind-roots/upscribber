package com.upscribber.upscribberSeller.subsriptionSeller.location.days

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.app.TimePickerDialog
import android.app.TimePickerDialog.OnTimeSetListener
import android.content.Intent
import android.graphics.Outline
import android.location.Address
import android.location.Geocoder
import android.os.Build
import android.os.Bundle
import android.view.*
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.tools.build.jetifier.core.utils.Log
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.google.gson.Gson
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.commonClasses.RoundedBottomSheetDialogFragment
import com.upscribber.databinding.SelectDaysLayoutBinding
import com.upscribber.databinding.SelectDaysListViewBinding
import com.upscribber.databinding.SelectTimeLayoutBinding
import com.upscribber.databinding.SetPrimaryLocBinding
import com.upscribber.filters.LocationModel
import com.upscribber.filters.MapsActivity
import com.upscribber.upscribberSeller.subsriptionSeller.location.searchLoc.*
import kotlinx.android.synthetic.main.activity_add_location.*
import kotlinx.android.synthetic.main.address_alert.*
import kotlinx.android.synthetic.main.address_alert.address
import kotlinx.android.synthetic.main.address_alert.addresss
import kotlinx.android.synthetic.main.custom_dialog.*
import kotlinx.android.synthetic.main.social_alert.*
import org.json.JSONArray
import org.json.JSONObject
import java.util.*
import kotlin.collections.ArrayList


@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class AddLocationActivity : AppCompatActivity(), AdapterSelectDays.SelectDays,
    AdapterLocationDays.adapterClick {


    private lateinit var viewModel1: SelectDaysViewModel
    private var arrayOperationHours: JSONArray = JSONArray()
    lateinit var addHours: ImageView
    lateinit var addLoc: TextView
    lateinit var searchAddress: EditText
    lateinit var toolbar: Toolbar
    lateinit var title: TextView
    var openTime: String = ""
    var closeTime: String = ""
    var status: String = "false"
    var checked: String = "false"
    private lateinit var viewModel: SearchLocationViewModel
    private lateinit var linearProgress: ConstraintLayout
    var modellocation: ModelStoreLocation = ModelStoreLocation()
    lateinit var adapterLocationDays: AdapterLocationDays
    var day = ArrayList<String>()
    var dayOf =
        arrayListOf("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday")
    var jsonArray = JSONArray()
    var editData = ModelSearchLocation()
    var newArray: ArrayList<ModelStoreLocation> = ArrayList()

    var id = ""
    var latitude = ""
    var longitude = ""
    var latitude1 = 0.0
    var longitude1 = 0.0
    val locationModel = LocationModel()
    private var addresses: MutableList<Address> = ArrayList()
    var newListAdapter = ArrayList<ModelStoreLocation>()


    companion object {
        var openTimeSlot = ""
        var closeTimeSlot = ""
        var check = "0"
        var newArrayWithTime = ArrayList<ModelStoreLocation>()
        var selectedValue = -1

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_location)
        viewModel = ViewModelProviders.of(this)[SearchLocationViewModel::class.java]
        viewModel1 = ViewModelProviders.of(this)[SelectDaysViewModel::class.java]
        Places.initialize(applicationContext, resources.getString(R.string.google_maps_key))
        initzz()
        initialize()
        setData()
        clicks()
        setToolbar()
        setAdapter()
        observerInit()
        try {
            if (newArrayWithTime.size > 0) {
                newArrayWithTime.clear()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        if (intent.hasExtra("model")) {
            try {
                editData = intent.getParcelableExtra("model")
                editTextAddress.setText(editData.street)
                editTextCity.setText(editData.city)
                editTextState.setText(editData.state)
                editTextZip.setText(editData.zip_code)
                latitude = editData.lat
                longitude = editData.long
                id = editData.id
                newArray = ArrayList()
                newArrayWithTime = ArrayList()
                delete.visibility = View.VISIBLE
                textView284.visibility = View.GONE
                textView285.visibility = View.GONE
                if (editData.store_timing.size > 0) {
                    for (i in 0 until editData.store_timing.size) {
                        if (editData.store_timing.isNotEmpty()) {
                            editTextDay.visibility = View.GONE
                            editTextTime.visibility = View.GONE
                            reyclerDayTime.visibility = View.VISIBLE
                            if (editData.store_timing[i].checked == "true") {
                                val model = ModelStoreLocation()
                                model.open = editData.store_timing[i].open
                                model.close = editData.store_timing[i].close
                                model.checked = editData.store_timing[i].checked
                                model.checkedFirst = editData.store_timing[i].checked
                                model.status = editData.store_timing[i].status
                                model.day = dayOf[i]
                                newArray.add(model)
                                newArrayWithTime.add(model)
                            } else {
                                val model = ModelStoreLocation()
                                if (editData.store_timing[i].status == "true") {
                                    model.open = ""
                                    model.close = ""
                                } else {
                                    model.open = "1"
                                    model.close = "1"
                                }
                                model.checked = editData.store_timing[i].checked
                                model.status = editData.store_timing[i].status
                                model.day = dayOf[i]
                                newArray.add(model)
                                newArrayWithTime.add(model)
                            }
                        } else {
                            editTextDay.visibility = View.VISIBLE
                            editTextTime.visibility = View.VISIBLE
                            reyclerDayTime.visibility = View.VISIBLE
                        }
                    }
                } else {
                    newArrayWithTime.addAll(getList())
                }

                for (items in newArrayWithTime) {
                    if ((items.status == "true") || (items.open != "1" && items.close != "1" && items.open.isNotEmpty() && items.close.isNotEmpty())) {
                        newListAdapter.add(items)
                    }
                }
                //adapterLocationDays.update(newListAdapter)
            } catch (e: Exception) {
                e.printStackTrace()
            }

        } else {
            newArrayWithTime.addAll(getList())
        }
        adapterLocationDays.update(newArrayWithTime)

    }

    private fun setData() {
        if (intent.hasExtra("locationModel")) {
            val data1 = intent.getParcelableExtra<LocationModel>("locationModel")
            editTextAddress.setText(data1.street)
            editTextCity.setText(data1.city)
            editTextState.setText(data1.state)
            editTextZip.setText(data1.zipcode)
            latitude = data1.latitude
            longitude = data1.longitude
        }

    }

    private fun setAdapter() {
        reyclerDayTime.layoutManager = LinearLayoutManager(this)
        adapterLocationDays = AdapterLocationDays(this)
        reyclerDayTime.adapter = adapterLocationDays

    }

    private fun initialize() {
        for (i in 0 until 7) {
            day.add("")
        }
    }

    private fun observerInit() {
        viewModel.getAddLocationData().observe(this, androidx.lifecycle.Observer {
            linearProgress.visibility = View.GONE
            if (it.status1 == "true") {
                startActivity(
                    Intent(this, LocationConfirmationActivity::class.java).putExtra(
                        "modelLocation", it
                    ).putExtra("longitude", longitude).putExtra("latitude", latitude)
                )
                finish()
            }
        })

        viewModel.getmDataDelete().observe(this, androidx.lifecycle.Observer {
            linearProgress.visibility = View.GONE
            if (it.status == "true") {
                finish()
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
            }
        })

        viewModel.getStatus().observe(this, androidx.lifecycle.Observer {
            if (it.msg.isNotEmpty()) {
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
            }
        })

        viewModel.getmDataDefault().observe(this, androidx.lifecycle.Observer {
            linearProgress.visibility = View.GONE
            if (it.status == "true") {
                val mDialogView = LayoutInflater.from(this).inflate(R.layout.custom_dialog, null)
                val mBuilder = AlertDialog.Builder(this).setView(mDialogView)
                val mAlertDialog = mBuilder.show()
                mAlertDialog.setCancelable(false)
                mAlertDialog.window.setBackgroundDrawableResource(R.drawable.bg_alert)
                mAlertDialog.addressText.visibility = View.GONE
                mAlertDialog.addresssText.text = it.msg
                mAlertDialog.okBtnClick.setOnClickListener {
                    mAlertDialog.dismiss()
                    finish()
                }

            }
        })

    }


    private fun initzz() {
        toolbar = findViewById(R.id.toolbarAdd)
        title = findViewById(R.id.title)
        addHours = findViewById(R.id.addHours)
        linearProgress = findViewById(R.id.linearProgress)
        searchAddress = findViewById(R.id.searchAddress)
        addLoc = findViewById(R.id.addLoc)
        modellocation.open = openTime
        modellocation.checked = checked
        modellocation.close = closeTime
        modellocation.days = day
        modellocation.status = status
        if (intent.hasExtra("model")) {
            addLoc.text = "Update"
        } else {
            addLoc.text = "Save"
        }

        val gson = Gson()
        val json = gson.toJson(modellocation)
        val JsonObject = JSONObject(json)
        val jsonArray = JSONArray()
        jsonArray.put(JsonObject)
        Log.i("JSONARRAY ", jsonArray.toString())
    }

    @SuppressLint("SetTextI18n")
    private fun setToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)
        if (intent.hasExtra("model")) {
            title.text = "Update Location"
        } else {
            title.text = "Add Location"
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 102) {
            val data1 = data?.getParcelableExtra<LocationModel>("locationModel")
            editTextAddress.setText(data1?.street)
            editTextCity.setText(data1?.city)
            editTextState.setText(data1?.state)
            editTextZip.setText(data1?.zipcode)
            latitude = data?.getStringExtra("latitude").toString()
            longitude = data?.getStringExtra("longitude").toString()
        }


        if (requestCode == 2) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    val place = Autocomplete.getPlaceFromIntent(data!!)
                    val latLng = place.latLng
                    latitude1 = latLng!!.latitude
                    longitude1 = latLng.longitude
                    locationModel.address = place.address.toString()
                    val geocoder = Geocoder(this)
                    addresses =
                        latLng.latitude.let { geocoder.getFromLocation(it, latLng.longitude, 1) }
                    try {
                        if (addresses.isNotEmpty()) {
                            locationModel.address = addresses[0].getAddressLine(0)
                            locationModel.street = addresses[0].thoroughfare
                            locationModel.city = addresses[0].locality
                            locationModel.state = addresses[0].adminArea
                            locationModel.zipcode = addresses[0].postalCode
                            locationModel.latitude = latitude1.toString()
                            locationModel.longitude = longitude1.toString()
                            startActivity(
                                Intent(
                                    this,
                                    MapsActivity::class.java
                                ).putExtra("location1", locationModel)
                            )
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }
        }


    }

    private fun clicks() {

        setPrimary.setOnClickListener {
            val fragment =
                MenuFragmentPrimary(editData, viewModel, linearProgress, 1)
            fragment.show(supportFragmentManager, fragment.tag)
        }
        addHours.setOnClickListener {

            for (i in 0 until newArrayWithTime.size) {

                newArrayWithTime[i].checked = "false"
            }
            val fragment = MenuFragment(openTime, closeTime, status, this, newListAdapter)
            fragment.show(supportFragmentManager, fragment.tag)
        }

        searchAddress.setOnClickListener {
            Places.createClient(this)
            val fields =
                listOf(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS)
            // Start the autocomplete intent.
            val intent = Autocomplete.IntentBuilder(
                AutocompleteActivityMode.FULLSCREEN, fields
            )
                .build(this)
            startActivityForResult(intent, 2)


//            startActivityForResult(Intent(this, MapsActivity::class.java), 102)
        }

        addLoc.setOnClickListener {
            arrayOperationHours = JSONArray()
            var exist = 0

            for (i in 0 until newArrayWithTime.size) {
                val operationHours = JSONObject()
                if (newArrayWithTime[i].open == "1") {
                    newArrayWithTime[i].open = ""
                }
                if (newArrayWithTime[i].close == "1") {
                    newArrayWithTime[i].close = ""
                }
                if ((newArrayWithTime[i].open.isNotEmpty() && newArrayWithTime[i].close.isNotEmpty())) {
                    exist = 1
                } else {
                    newArrayWithTime[i].checked = "true"
                    newArrayWithTime[i].status = "true"
                }
                operationHours.put("open", newArrayWithTime[i].open)
                operationHours.put("close", newArrayWithTime[i].close)
                operationHours.put("status", newArrayWithTime[i].status)
                operationHours.put("checked", newArrayWithTime[i].checked)
                operationHours.put("day", (i + 1))
                arrayOperationHours.put(operationHours)

            }

            if (editTextAddress.text.toString().isEmpty()) {
                Toast.makeText(this, "Please enter data in all fields", Toast.LENGTH_SHORT).show()
            } else if (editTextCity.text.toString().isEmpty()) {
                Toast.makeText(this, "Please enter data in all fields", Toast.LENGTH_SHORT).show()
            } else if (editTextState.text.toString().isEmpty()) {
                Toast.makeText(this, "Please enter data in all fields", Toast.LENGTH_SHORT).show()
            } else if (editTextZip.text.toString().isEmpty()) {
                Toast.makeText(this, "Please enter data in all fields", Toast.LENGTH_SHORT).show()
            }
//            else if (exist == 0) {
//                Toast.makeText(
//                    this,
//                    "Please add your location hours to continue",
//                    Toast.LENGTH_SHORT
//                ).show()
//            }
            else {
                linearProgress.visibility = View.VISIBLE
                val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
                viewModel.getAddLocation(
                    auth,
                    id,
                    editTextAddress.text.toString().trim(),
                    editTextCity.text.toString().trim(),
                    editTextState.text.toString().trim(),
                    editTextZip.text.toString().trim(),
                    arrayOperationHours, latitude, longitude
                )
            }
        }

        delete.setOnClickListener {
            val fragment =
                MenuFragmentPrimary(editData, viewModel, linearProgress, 2)
            fragment.show(supportFragmentManager, fragment.tag)
        }
    }

    override fun daysData(model: ModelSelectDays, position: Int) {
        if (newArrayWithTime.size >= 7) {
            newArrayWithTime[position].checked = model.checkBox.toString()
        } else {
            return
        }
        modellocation.days = day

        Log.e("checkDays", day.toString())

    }

    override fun onResume() {
        super.onResume()

        if (openTimeSlot != "" && closeTimeSlot != "") {
            Log.e("", "$openTimeSlot  $closeTimeSlot  $check")
        }
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    class MenuFragment(
        var openTime: String,
        var closeTime: String,
        var status: String,
        var context: AddLocationActivity,
        var newListAdapter: ArrayList<ModelStoreLocation>
    ) : RoundedBottomSheetDialogFragment() {

        private lateinit var binding: SelectDaysListViewBinding
        private lateinit var viewModel: SelectDaysViewModel
        private lateinit var mAdapter: AdapterSelectDays
        private var value = -1
        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            binding =
                DataBindingUtil.inflate(inflater, R.layout.select_days_list_view, container, false)
            viewModel = ViewModelProviders.of(this)[SelectDaysViewModel::class.java]

            return binding.root
        }

        override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
            super.onViewCreated(view, savedInstanceState)
//            binding.recyclerView3.layoutManager = LinearLayoutManager(activity)
//            mAdapter = AdapterSelectDays(activity!!)
//            binding.recyclerView3.adapter = mAdapter
            binding.save.setOnClickListener {
                openTimeSlot = binding.etOpen.text.toString()
                closeTimeSlot = binding.etClose.text.toString()
                Log.e("", "$openTimeSlot  $closeTimeSlot  $check")
                context.updateAdapter(openTimeSlot, closeTimeSlot, status, 1, -1)
                selectedValue = value

                dismiss()
            }
            fun getListDuration(): ArrayList<String> {
                val arrayList: ArrayList<String> = ArrayList()

                for (i in 1 until 11) {
                    when (i) {
                        1 -> arrayList.add("Everyday")
                        2 -> arrayList.add("Mon-Fri")
                        3 -> arrayList.add("Sat-Sun")
                        4 -> arrayList.add("Sunday")
                        5 -> arrayList.add("Monday")
                        6 -> arrayList.add("Tuesday")
                        7 -> arrayList.add("Wednesday")
                        8 -> arrayList.add("Thursday")
                        9 -> arrayList.add("Friday")
                        10 -> arrayList.add("Saturday")
                    }
                }

                return arrayList

            }

            binding.selectDay.setOnClickListener {
                Constant.hideKeyboard(activity!!, binding.selectDay)
                val choices = getListDuration()
                val charSequenceItems = choices.toArray(arrayOfNulls<CharSequence>(choices.size))
                val mBuilder = androidx.appcompat.app.AlertDialog.Builder(activity!!)
                mBuilder.setSingleChoiceItems(charSequenceItems, -1) { dialogInterface, i ->
                    if (selectedValue == 0) {
                        Toast.makeText(
                            context,
                            "You have already selected all days",
                            Toast.LENGTH_SHORT
                        ).show()
                    } else if (selectedValue == 1) {
                        if (i == 8 || i == 9 || i == 2) {
                            binding.selectDay.text = choices[i]
                            value = i
                            dialogInterface.dismiss()
                            setData(i)
                        } else {
                            Toast.makeText(
                                context,
                                "You have already selected monday to friday",
                                Toast.LENGTH_SHORT
                            ).show()
                        }

                    } else if (selectedValue == 2) {
                        if (i != 8 || i != 9) {
                            binding.selectDay.text = choices[i]
                            value = i
                            dialogInterface.dismiss()
                            setData(i)
                        } else {
                            Toast.makeText(
                                context,
                                "You have already selected monday to friday",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    } else {
                        binding.selectDay.text = choices[i]
                        value = i
                        dialogInterface.dismiss()
                        setData(i)
                    }
                }
                val mDialog = mBuilder.create()
                mDialog.show()
            }

            binding.etOpen.setOnClickListener {
                Constant.hideKeyboard(activity!!, binding.etOpen)
                val mcurrentTime = Calendar.getInstance()
                val hour = mcurrentTime.get(Calendar.HOUR_OF_DAY)
                val minute = mcurrentTime.get(Calendar.MINUTE)
                val mTimePicker: TimePickerDialog
                mTimePicker = TimePickerDialog(
                    activity, OnTimeSetListener { timePicker, selectedHour, selectedMinute ->

                        var format = ""
                        var selection = selectedHour
                        val minutes = selectedMinute

                        when {
                            selection == 0 -> {
                                selection += 12

                                format = "AM"
                            }
//                            selection < 10 ->{
//
//                            }

                            selection == 12 -> {

                                format = "PM"

                            }
                            selection > 12 -> {

                                selection -= 12

                                format = "PM"

                            }
                            else -> {
                                format = "AM"
                            }
                        }

                        var newMinutes = ""
                        var newHours = ""
                        newMinutes = if (minutes < 10) {
                            "0$minutes"
                        } else {
                            minutes.toString()
                        }

                        newHours = if (selection < 10) {
                            "0$selection"
                        } else {
                            selection.toString()
                        }


                        binding.etOpen.text = "$newHours:$newMinutes $format"


                    }, hour, minute, false
                )
                mTimePicker.setTitle("Select Time")
                mTimePicker.show()

            }

            openTime = binding.etOpen.text.toString()

            binding.etClose.setOnClickListener {
                Constant.hideKeyboard(activity!!, binding.etClose)
                val mcurrentTime = Calendar.getInstance()
                val hour = mcurrentTime.get(Calendar.HOUR_OF_DAY)
                val minute = mcurrentTime.get(Calendar.MINUTE)
                val mTimePicker: TimePickerDialog
                mTimePicker = TimePickerDialog(
                    activity, OnTimeSetListener { timePicker, selectedHour, selectedMinute ->

                        var format = ""
                        var selection = selectedHour
                        val minutes = selectedMinute

                        when {
                            selection == 0 -> {
                                selection += 12

                                format = "AM"
                            }
                            selection == 12 -> {

                                format = "PM"

                            }
                            selection > 12 -> {

                                selection -= 12

                                format = "PM"

                            }
                            else -> {
                                format = "AM"
                            }
                        }

                        var newMinutes = ""
                        var newHours = ""
                        newMinutes = if (minutes < 10) {
                            "0$minutes"
                        } else {
                            minutes.toString()
                        }

                        newHours = if (selection < 10) {
                            "0$selection"
                        } else {
                            selection.toString()
                        }

                        binding.etClose.text = "$newHours:$newMinutes $format"


                    }, hour, minute, false
                )
                mTimePicker.setTitle("Select Time")
                mTimePicker.show()

            }
            closeTime = binding.etClose.text.toString()


//            val list = viewModel.getList()
//            for (item in newListAdapter) {
//                for (i in 0 until list.size) {
//                    if (item.day.toLowerCase() == list[i].headings.toLowerCase()) {
//                        list[i].checkBox = true
//                        newArrayWithTime[i].checked = "true"
//
//                    }
//                }
//            }

//            mAdapter.update(list)
            roundedImage()
        }

        fun setData(i: Int) {
            if (newArrayWithTime.size >= 7) {
                when (i) {
                    0 -> {
                        for (items in newArrayWithTime) {
                            items.checked = "true"
                            items.checkedFirst = "true"
                        }
                    }
                    1 -> {
                        for (j in 0 until newArrayWithTime.size) {
                            if (j == 1 || j == 2 || j == 3 || j == 4 || j == 5) {
                                newArrayWithTime[j].checked = "true"
                                newArrayWithTime[j].checkedFirst = "true"
                            }
                        }
                    }
                    2 -> {
                        for (j in 0 until newArrayWithTime.size) {
                            if (j == 0 || j == 6) {
                                newArrayWithTime[j].checked = "true"
                                newArrayWithTime[j].checkedFirst = "true"
                            }
                        }
                    }
                    3 -> {
                        for (j in 0 until newArrayWithTime.size) {
                            if (j == 0) {
                                newArrayWithTime[j].checked = "true"
                                newArrayWithTime[j].checkedFirst = "true"
                            }
                        }
                    }
                    4 -> {
                        for (j in 0 until newArrayWithTime.size) {
                            if (j == 1) {
                                newArrayWithTime[j].checked = "true"
                                newArrayWithTime[j].checkedFirst = "true"
                            }
                        }
                    }
                    5 -> {
                        for (j in 0 until newArrayWithTime.size) {
                            if (j == 2) {
                                newArrayWithTime[j].checked = "true"
                                newArrayWithTime[j].checkedFirst = "true"
                            }
                        }
                    }
                    6 -> {
                        for (j in 0 until newArrayWithTime.size) {
                            if (j == 3) {
                                newArrayWithTime[j].checked = "true"
                                newArrayWithTime[j].checkedFirst = "true"
                            }
                        }
                    }
                    7 -> {
                        for (j in 0 until newArrayWithTime.size) {
                            if (j == 4) {
                                newArrayWithTime[j].checked = "true"
                                newArrayWithTime[j].checkedFirst = "true"
                            }
                        }
                    }
                    8 -> {
                        for (j in 0 until newArrayWithTime.size) {
                            if (j == 5) {
                                newArrayWithTime[j].checked = "true"
                                newArrayWithTime[j].checkedFirst = "true"
                            }
                        }
                    }
                    9 -> {
                        for (j in 0 until newArrayWithTime.size) {
                            if (j == 6) {
                                newArrayWithTime[j].checked = "true"
                                newArrayWithTime[j].checkedFirst = "true"
                            }
                        }
                    }
                }
            }
        }

        private fun roundedImage() {
            val curveRadius = 50F

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                binding.imageViewTop.outlineProvider = object : ViewOutlineProvider() {

                    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                    override fun getOutline(view: View?, outline: Outline?) {
                        outline?.setRoundRect(
                            0,
                            0,
                            view!!.width,
                            view.height + curveRadius.toInt(),
                            curveRadius
                        )
                    }
                }
                binding.imageViewTop.clipToOutline = true
            }
        }
    }

    class MenuFragmentTwo(
        var openTime: String,
        var closeTime: String,
        var statuss: String,
        var context: AddLocationActivity,
        var i: Int,
        var position: Int
    ) : RoundedBottomSheetDialogFragment() {

        private lateinit var binding: SelectTimeLayoutBinding
        private var status: Boolean = false

        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            binding =
                DataBindingUtil.inflate(inflater, R.layout.select_time_layout, container, false)

            return binding.root
        }

        @SuppressLint("SetTextI18n")
        override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
            super.onViewCreated(view, savedInstanceState)

            if (position != -1) {
                if (newArrayWithTime[position].open.isNotEmpty() && newArrayWithTime[position].open != "1") {
                    binding.etOpen.text = newArrayWithTime[position].open
                }

                if (newArrayWithTime[position].close.isNotEmpty() && newArrayWithTime[position].close != "1") {
                    binding.etClose.text = newArrayWithTime[position].close
                }

                if (newArrayWithTime[position].status == "true") {
                    check = "1"
                    status = true
                    binding.etOpen.isEnabled = false
                    binding.etClose.isEnabled = false
                    statuss = "true"
                    binding.notOpened.setBackgroundResource(R.drawable.select_time_bg_fill)
                    binding.notOpened.setTextColor(resources.getColor(R.color.colorWhite))
                    openTimeSlot = ""
                    closeTimeSlot = ""
                } else {
                    check = "0"
                    status = false
                    binding.etOpen.isEnabled = true
                    binding.etClose.isEnabled = true
                    statuss = "false"
                    binding.notOpened.setBackgroundResource(R.drawable.select_time_bg)
                    binding.notOpened.setTextColor(resources.getColor(R.color.colorSkip))
                }
            }

            binding.notOpened.setOnClickListener {
                if (status) {
                    check = "0"
                    status = false
                    binding.etOpen.isEnabled = true
                    binding.etClose.isEnabled = true
                    statuss = "false"
                    binding.notOpened.setBackgroundResource(R.drawable.select_time_bg)
                    binding.notOpened.setTextColor(resources.getColor(R.color.colorSkip))

                } else {
                    check = "1"
                    status = true
                    binding.etOpen.isEnabled = false
                    binding.etOpen.text = ""
                    binding.etClose.isEnabled = false
                    binding.etClose.text = ""
                    statuss = "true"
                    binding.notOpened.setBackgroundResource(R.drawable.select_time_bg_fill)
                    binding.notOpened.setTextColor(resources.getColor(R.color.colorWhite))
                    openTimeSlot = ""
                    closeTimeSlot = ""
                }
            }

            binding.etOpen.setOnClickListener {
                Constant.hideKeyboard(activity!!, binding.etOpen)
                val mcurrentTime = Calendar.getInstance()
                val hour = mcurrentTime.get(Calendar.HOUR_OF_DAY)
                val minute = mcurrentTime.get(Calendar.MINUTE)
                val mTimePicker: TimePickerDialog
                mTimePicker = TimePickerDialog(
                    activity, OnTimeSetListener { timePicker, selectedHour, selectedMinute ->

                        var format = ""
                        var selection = selectedHour
                        val minutes = selectedMinute

                        when {
                            selection == 0 -> {
                                selection += 12

                                format = "AM"
                            }
//                            selection < 10 ->{
//
//                            }

                            selection == 12 -> {

                                format = "PM"

                            }
                            selection > 12 -> {

                                selection -= 12

                                format = "PM"

                            }
                            else -> {
                                format = "AM"
                            }
                        }

                        var newMinutes = ""
                        var newHours = ""
                        newMinutes = if (minutes < 10) {
                            "0$minutes"
                        } else {
                            minutes.toString()
                        }

                        newHours = if (selection < 10) {
                            "0$selection"
                        } else {
                            selection.toString()
                        }


                        binding.etOpen.text = "$newHours:$newMinutes $format"


                    }, hour, minute, false
                )
                mTimePicker.setTitle("Select Time")
                mTimePicker.show()

            }

            openTime = binding.etOpen.text.toString()

            binding.etClose.setOnClickListener {
                Constant.hideKeyboard(activity!!, binding.etClose)
                val mcurrentTime = Calendar.getInstance()
                val hour = mcurrentTime.get(Calendar.HOUR_OF_DAY)
                val minute = mcurrentTime.get(Calendar.MINUTE)
                val mTimePicker: TimePickerDialog
                mTimePicker = TimePickerDialog(
                    activity, OnTimeSetListener { timePicker, selectedHour, selectedMinute ->

                        var format = ""
                        var selection = selectedHour
                        val minutes = selectedMinute

                        when {
                            selection == 0 -> {
                                selection += 12

                                format = "AM"
                            }
                            selection == 12 -> {

                                format = "PM"

                            }
                            selection > 12 -> {

                                selection -= 12

                                format = "PM"

                            }
                            else -> {
                                format = "AM"
                            }
                        }

                        var newMinutes = ""
                        var newHours = ""
                        newMinutes = if (minutes < 10) {
                            "0$minutes"
                        } else {
                            minutes.toString()
                        }

                        newHours = if (selection < 10) {
                            "0$selection"
                        } else {
                            selection.toString()
                        }

                        binding.etClose.text = "$newHours:$newMinutes $format"


                    }, hour, minute, false
                )
                mTimePicker.setTitle("Select Time")
                mTimePicker.show()

            }
            closeTime = binding.etClose.text.toString()

            roundedImage()

            binding.save.setOnClickListener {
                openTimeSlot = binding.etOpen.text.toString()
                closeTimeSlot = binding.etClose.text.toString()
                if ((openTimeSlot == closeTimeSlot)&& (openTimeSlot.isNotEmpty() && closeTimeSlot.isNotEmpty())) {
                    Toast.makeText(
                        context,
                        "Open and close time cannot be same",
                        Toast.LENGTH_SHORT
                    ).show()
                } else {
                    Log.e("", "$openTimeSlot  $closeTimeSlot  $check")
                    context.updateAdapter(openTimeSlot, closeTimeSlot, statuss, i, position)

                    dismiss()
                }


            }
        }

        private fun roundedImage() {
            val curveRadius = 50F

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                binding.imageViewTop.outlineProvider = object : ViewOutlineProvider() {

                    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                    override fun getOutline(view: View?, outline: Outline?) {
                        outline?.setRoundRect(
                            0,
                            0,
                            view!!.width,
                            view.height + curveRadius.toInt(),
                            curveRadius
                        )
                    }
                }
                binding.imageViewTop.clipToOutline = true
            }
        }
    }

    private fun updateAdapter(
        openTimeSlot: String,
        closeTimeSlot: String,
        statuss: String,
        i: Int,
        position: Int
    ) {

        if (i == 1) {

            for (i in 0 until newArrayWithTime.size) {
                if (newArrayWithTime[i].checked == "true") {

                    newArrayWithTime[i].open = openTimeSlot
                    newArrayWithTime[i].close = closeTimeSlot
                    newArrayWithTime[i].status = statuss
                }
                if (newArrayWithTime[i].open != "1" && newArrayWithTime[i].close != "1" && newArrayWithTime[i].open.isNotEmpty() && newArrayWithTime[i].close.isNotEmpty()
                ) {
                    if (newArrayWithTime[i].checked == "true") {
                        newArrayWithTime[i].checked = "true"
                    } else {
                        if (newArrayWithTime[i].checkedFirst == "true") {
                            newArrayWithTime[i].checked = "true"
                        } else {
                            newArrayWithTime[i].checked = "false"
                        }

                    }
                } else {
                    if (newArrayWithTime[i].checked == "true") {
                        newArrayWithTime[i].checked = "true"
                    } else {
                        if (newArrayWithTime[i].checkedFirst == "true") {
                            newArrayWithTime[i].checked = "true"
                        } else {
                            newArrayWithTime[i].checked = "false"
                        }

                    }
                }
            }
        } else {
            newArrayWithTime[position].open = openTimeSlot
            newArrayWithTime[position].close = closeTimeSlot
            newArrayWithTime[position].status = statuss
            newArrayWithTime[position].checked = "true"


            /* if (openTimeSlot != "" && closeTimeSlot != "") {
                 newArrayWithTime[position].checked = "true"
             } else {
                 newArrayWithTime[position].checked = "false"
             }*/

            if (newArrayWithTime[i].open != "1" && newArrayWithTime[i].close != "1" && newArrayWithTime[i].open.isNotEmpty() && newArrayWithTime[i].close.isNotEmpty()
            ) {
                if (newArrayWithTime[i].checked == "true") {
                    newArrayWithTime[i].checked = "true"
                } else {
                    if (newArrayWithTime[i].checkedFirst == "true") {
                        newArrayWithTime[i].checked = "true"
                    } else {
                        newArrayWithTime[i].checked = "false"
                    }

                }
            } else {
                if (newArrayWithTime[i].checked == "true") {
                    newArrayWithTime[i].checked = "true"
                } else {
                    if (newArrayWithTime[i].checkedFirst == "true") {
                        newArrayWithTime[i].checked = "true"
                    } else {
                        newArrayWithTime[i].checked = "false"
                    }

                }
            }
        }


//        if (newArrayWithTime.size > 0) {
//            reyclerDayTime.visibility = View.VISIBLE
//            editTextDay.visibility = View.GONE
//            editTextTime.visibility = View.GONE
//            textView284.visibility = View.GONE
//            textView285.visibility = View.GONE
//        } else {
//            editTextDay.visibility = View.VISIBLE
//            editTextTime.visibility = View.VISIBLE
//            reyclerDayTime.visibility = View.GONE
//        }

        newListAdapter = ArrayList<ModelStoreLocation>()

        for (items in newArrayWithTime) {
            if ((items.status == "true") || (items.open != "1" && items.close != "1" && items.open.isNotEmpty() && items.close.isNotEmpty())) {
                if (items.checked == "true") {
                    newListAdapter.add(items)
                }
            }
        }
        adapterLocationDays.update(newArrayWithTime)

    }

    fun getList(): ArrayList<ModelStoreLocation> {
        val arrayList = ArrayList<ModelStoreLocation>()

        var modelSelectDays = ModelStoreLocation()
        modelSelectDays.day = "Sunday"
        modelSelectDays.open = "1"
        modelSelectDays.close = "1"
        modelSelectDays.status = "false"
        modelSelectDays.checked = "false"
        arrayList.add(modelSelectDays)


        modelSelectDays = ModelStoreLocation()
        modelSelectDays.day = "Monday"
        modelSelectDays.open = "1"
        modelSelectDays.close = "1"
        modelSelectDays.status = "false"
        modelSelectDays.checked = "false"
        arrayList.add(modelSelectDays)


        modelSelectDays = ModelStoreLocation()
        modelSelectDays.day = "Tuesday"
        modelSelectDays.open = "1"
        modelSelectDays.close = "1"
        modelSelectDays.status = "false"
        modelSelectDays.checked = "false"
        arrayList.add(modelSelectDays)


        modelSelectDays = ModelStoreLocation()
        modelSelectDays.day = "Wednesday"
        modelSelectDays.open = "1"
        modelSelectDays.close = "1"
        modelSelectDays.status = "false"
        modelSelectDays.checked = "false"
        arrayList.add(modelSelectDays)


        modelSelectDays = ModelStoreLocation()
        modelSelectDays.day = "Thursday"
        modelSelectDays.open = "1"
        modelSelectDays.close = "1"
        modelSelectDays.status = "false"
        modelSelectDays.checked = "false"
        arrayList.add(modelSelectDays)


        modelSelectDays = ModelStoreLocation()
        modelSelectDays.day = "Friday"
        modelSelectDays.open = "1"
        modelSelectDays.close = "1"
        modelSelectDays.status = "false"
        modelSelectDays.checked = "false"
        arrayList.add(modelSelectDays)


        modelSelectDays = ModelStoreLocation()
        modelSelectDays.day = "Saturday"
        modelSelectDays.open = "1"
        modelSelectDays.close = "1"
        modelSelectDays.status = "false"
        modelSelectDays.checked = "false"
        arrayList.add(modelSelectDays)

        return arrayList
    }

    class MenuFragmentPrimary(
        var backgroundChange: ModelSearchLocation,
        var viewModel: SearchLocationViewModel,
        var linearProgress: ConstraintLayout,
        var i: Int
    ) : RoundedBottomSheetDialogFragment() {
        lateinit var binding: SetPrimaryLocBinding

        var status: Boolean = false

        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            binding = DataBindingUtil.inflate(inflater, R.layout.set_primary_loc, container, false)
            return binding.root
        }

        override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
            super.onViewCreated(view, savedInstanceState)
            val auth = Constant.getPrefs(activity!!).getString(Constant.auth_code, "")

            if (i == 1) {
                binding.textHeading.text = "Set As Default"
                binding.textDescription.text =
                    resources.getString(R.string.are_you_sure_you_want_to_set)
            } else {
                binding.textHeading.text = "Delete Location"
                binding.textDescription.text =
                    resources.getString(R.string.are_you_sure_you_want_to_delete)
            }


            binding.no.setOnClickListener {
                dismiss()
            }

            binding.yes.setOnClickListener {

                linearProgress.visibility = View.VISIBLE
                if (i == 1) {
                    linearProgress.visibility = View.VISIBLE
                    viewModel.getPrimaryLocation(auth, backgroundChange.id)
                    dismiss()
                } else if (backgroundChange.id == backgroundChange.default) {
                    Toast.makeText(
                        context,
                        "This Location is marked as default location, you can not delete this",
                        Toast.LENGTH_SHORT
                    ).show()
                    dismiss()
                } else {
                    linearProgress.visibility = View.VISIBLE
                    viewModel.getDeleteLocation(
                        auth,
                        backgroundChange.id,
                        backgroundChange.default
                    )
                    dismiss()
                }
                dismiss()
            }
            roundedImage()
        }

        private fun roundedImage() {
            val curveRadius = 50F

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                binding.imageViewTop.outlineProvider = object : ViewOutlineProvider() {

                    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                    override fun getOutline(view: View?, outline: Outline?) {
                        outline?.setRoundRect(
                            0,
                            0,
                            view!!.width,
                            view.height + curveRadius.toInt(),
                            curveRadius
                        )
                    }
                }
                binding.imageViewTop.clipToOutline = true
            }
        }
    }

    override fun click(modelStoreLocation: ModelStoreLocation, position: Int) {
        val fragment = MenuFragmentTwo(openTime, closeTime, status, this, 2, position)
        fragment.show(supportFragmentManager, fragment.tag)
    }
}



