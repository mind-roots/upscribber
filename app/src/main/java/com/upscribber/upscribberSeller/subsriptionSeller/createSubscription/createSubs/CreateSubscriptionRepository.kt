package com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.createSubs

import android.app.Application
import android.text.TextUtils
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.otherDiscounts.OtherDiscountsModel
import com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.products.ModelCreateProducts
import com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.subscription.ModelSubscriptionCard
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.commonClasses.ModelStatusMsg
import com.upscribber.commonClasses.WebServicesCustomers
import com.upscribber.commonClasses.WebServicesMerchants
import com.upscribber.filters.CategoryModel
import com.upscribber.filters.ModelSubCategory
import com.upscribber.upscribberSeller.manageTeam.ManageTeamModel
import com.upscribber.upscribberSeller.subsriptionSeller.location.searchLoc.ModelSearchLocation
import com.upscribber.upscribberSeller.subsriptionSeller.location.searchLoc.ModelStoreLocation
import com.upscribber.upscribberSeller.subsriptionSeller.subscriptionMain.ProductInfoModel
import okhttp3.ResponseBody
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit


@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS", "NAME_SHADOWING")
class CreateSubscriptionRepository(var application: Application) {
    var mDataCategories = MutableLiveData<java.util.ArrayList<CategoryModel>>()
    val mDataFailure = MutableLiveData<ModelStatusMsg>()

    var mDataDetails = MutableLiveData<DetailResModel>()
    var mDataBasic = MutableLiveData<BasicModel>()

    val mDataSubs = MutableLiveData<ArrayList<ModelSubscriptionCard>>()


    fun getProducts(): LiveData<ArrayList<ModelCreateProducts>> {
        val mData = MutableLiveData<ArrayList<ModelCreateProducts>>()
        val arrayList = ArrayList<ModelCreateProducts>()

        var modelCreateProducts = ModelCreateProducts()
        modelCreateProducts.productName = "Diamond Peel Mask"
        modelCreateProducts.productPrice = "$45"
        arrayList.add(modelCreateProducts)

        modelCreateProducts = ModelCreateProducts()
        modelCreateProducts.productName = "Facial"
        modelCreateProducts.productPrice = "$45"
        arrayList.add(modelCreateProducts)

        modelCreateProducts = ModelCreateProducts()
        modelCreateProducts.productName = "Diamond Peel Mask"
        modelCreateProducts.productPrice = "$45"
        arrayList.add(modelCreateProducts)

        mData.value = arrayList
        return mData

    }


//--------------------------------------PRICING---------------------------//

    fun getSubsCard(
        subscription_data: JSONArray
    ) {


        var dataStored = DetailResModel()
        var dataStored1 = BasicModel()


        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()
        val auth = Constant.getPrefs(application).getString(Constant.auth_code, "")


        val mPrefs = application.getSharedPreferences("data", AppCompatActivity.MODE_PRIVATE)
        val gson = Gson()
        val json = mPrefs.getString("detailsArray", "")
        val json1 = mPrefs.getString("basicData", "")
        var mLocationData = ""
        var mTeamData = ""
        var mTagData = ""
        var products = ""
        val listLocation = ArrayList<String>()
        val listTeam = ArrayList<String>()
        val listTags = ArrayList<String>()
        val list = ArrayList<String>()
        var campaignId = ""
        if (mPrefs.getString("campaign_id", "") != "") {
            campaignId = mPrefs.getString("campaign_id", "")
        }

        if (json != "") {
            dataStored = gson.fromJson(json, DetailResModel::class.java)
            Log.e("", dataStored.check)
            if (dataStored.teamMember.isNotEmpty()) {


                for (j in 0 until dataStored.teamMember.size) {
                    listTeam.add(dataStored.teamMember[j].staff_id)
                }
                mTeamData = TextUtils.join(",", listTeam)
            }


            if (dataStored.location.isNotEmpty()) {

                for (i in 0 until dataStored.location.size) {
                    listLocation.add(dataStored.location[i].id)
                }
                mLocationData = TextUtils.join(",", listLocation)
            }


            if (dataStored.tags.isNotEmpty()) {
                for (k in 0 until dataStored.tags.size) {
                    listTags.add(dataStored.tags[k].id)
                }
                mTagData = TextUtils.join(",", listTags)
            }


        }

        if (json1 != "") {
            dataStored1 = gson.fromJson(json1, BasicModel::class.java)
            if (dataStored1.productInfo.isNotEmpty()) {
                for (i in 0 until dataStored1.productInfo.size) {
                    list.add(dataStored1.productInfo[i].id)
                    Log.e("array List of Products", dataStored1.productInfo[i].id)
                }
                products = TextUtils.join(",", list)
            }


        }


        val service = retrofit.create(WebServicesMerchants::class.java)
        val call: Call<ResponseBody> = service.saveSubscription(
            campaignId,
            auth,
            dataStored1.image,
            dataStored1.name,
            products,
            dataStored1.campaign_start,
            dataStored1.campaign_end,
            dataStored1.campaign_type,
            subscription_data,
            dataStored.subInfo,
            mTeamData,
            mTagData,
            dataStored.shareable,
            dataStored.check,
            mLocationData,
            dataStored.subscriber, "0"
        )
        call.enqueue(object : Callback<ResponseBody> {


            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                try {
                    val res = response.body()!!.string()
                    val json = JSONObject(res)
                    val status = json.optString("status")
                    val msg = json.optString("msg")
                    val modelStatus = ModelStatusMsg()
                    val data = json.optJSONObject("data")
                    val arrayList = ArrayList<ModelSubscriptionCard>()
                    if (status == "true") {

                        try {

                            val subscriptionData = data.optJSONArray("subscription")
                            val campaignInfo = data.getJSONObject("campaign_info")
                            val campaignId = campaignInfo.optString("campaign_id")
                            for (i in 0 until subscriptionData.length()) {
                                val subData = subscriptionData.getJSONObject(i)
                                val modelSubscriptionCard = ModelSubscriptionCard()
                                val campaignInfo = data.getJSONObject("campaign_info")
                                modelSubscriptionCard.campaignId =
                                    campaignInfo.optString("campaign_id")
                                modelSubscriptionCard.steps =
                                    campaignInfo.optJSONArray("steps").toString()
                                modelSubscriptionCard.id = subData.optString("id")
                                modelSubscriptionCard.campaignId = subData.optString("campaign_id")
                                modelSubscriptionCard.price = subData.optString("price")
                                modelSubscriptionCard.discountPrice =
                                    subData.optString("discount_price")
                                modelSubscriptionCard.redeemtion_cycle_qty =
                                    subData.optString("redeemtion_cycle_qty")
                                modelSubscriptionCard.quantityName = subData.optString("unit")
                                modelSubscriptionCard.unit = subData.optString("unit")
                                modelSubscriptionCard.frequencyType =
                                    subData.optString("frequency_type")
                                modelSubscriptionCard.introPrice =
                                    subData.optString("introductory_price")
                                modelSubscriptionCard.introDays =
                                    subData.optString("introductory_days")
                                modelSubscriptionCard.frequency =
                                    subData.optString("frequency_value")
                                modelSubscriptionCard.createdAt = subData.optString("created_at")
                                modelSubscriptionCard.updatedAt = subData.optString("updated_at")
                                modelSubscriptionCard.freeTrail = subData.optString("free_trial")
                                modelSubscriptionCard.free_trail_qty = subData.optString("free_trial_qty")
                                arrayList.add(modelSubscriptionCard)

                            }


                            val mPrefs = application.getSharedPreferences(
                                "data",
                                AppCompatActivity.MODE_PRIVATE
                            )
                            val prefsEditor = mPrefs.edit()
                            prefsEditor.putString("campaign_id", campaignId)
                            prefsEditor.apply()
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                        mDataSubs.value = arrayList
                    } else {
                        modelStatus.msg = msg
                        modelStatus.status = status
                        mDataFailure.value = modelStatus
                    }

                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus


            }


        })


    }


    fun mSuccessful(): MutableLiveData<ArrayList<ModelSubscriptionCard>> {
        return mDataSubs
    }

    fun getSubsOtherDiscount(): LiveData<ArrayList<OtherDiscountsModel>> {
        val mData = MutableLiveData<ArrayList<OtherDiscountsModel>>()
        val arrayList = ArrayList<OtherDiscountsModel>()

        var otherDiscountsModel = OtherDiscountsModel()
        otherDiscountsModel.image = R.drawable.ic_discount_percentage
        otherDiscountsModel.name = "Discount Percentage"
        otherDiscountsModel.amount = "25%"
        arrayList.add(otherDiscountsModel)

        otherDiscountsModel = OtherDiscountsModel()
        otherDiscountsModel.image = R.drawable.ic_fee
        otherDiscountsModel.name = "Fee"
        otherDiscountsModel.amount = "$7.00"
        arrayList.add(otherDiscountsModel)

        otherDiscountsModel = OtherDiscountsModel()
        otherDiscountsModel.image = R.drawable.ic_discount_percentage
        otherDiscountsModel.name = "What You Get?"
        otherDiscountsModel.amount = "$20.00"
        arrayList.add(otherDiscountsModel)

        mData.value = arrayList
        return mData
    }

    fun getProductCategories(auth: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.productCategory(auth)
        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {

                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val arrayCategories = ArrayList<CategoryModel>()
                        if (status == "true") {
                            val data = json.optJSONObject("data")
                            val iter = data.keys()
                            while (iter.hasNext()) {
                                val key = iter.next()
                                try {
                                    val categoryModel = CategoryModel()
                                    categoryModel.categoryName = key
                                    val catName = data.optJSONObject(key)
                                    categoryModel.image = catName.optString("image")
                                    categoryModel.categoryId = catName.optString("parent_id")
                                    val value = catName.optJSONArray("sub_category")
                                    for (i in 0 until value.length()) {
                                        val objMainCategory = value.getJSONObject(i)
                                        val model = ModelSubCategory()
                                        model.name = objMainCategory.optString("name")
                                        model.id = objMainCategory.optString("id")
                                        categoryModel.arrayList.add(model)
                                    }

                                    arrayCategories.add(categoryModel)

                                } catch (e: JSONException) {
                                    e.printStackTrace()
                                }

                            }
                            mDataCategories.value = arrayCategories

                        } else {
                            val modelStatus = ModelStatusMsg()
                            modelStatus.msg = msg
                            mDataFailure.value = modelStatus
                        }

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus

            }
        })
    }


    fun getmDataFailure(): LiveData<ModelStatusMsg> {
        return mDataFailure
    }

    fun getProductCategory(): MutableLiveData<java.util.ArrayList<CategoryModel>> {
        return mDataCategories
    }

    //-----------------------------------------BASICS------------------------------------------//
    fun saveBasic(
        image: String,
        name: String,
        product: String,
        campaign_start: String,
        campaign_end: String,
        campaign_type: String
    ) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()


        val auth = Constant.getPrefs(application).getString(Constant.auth_code, "")
        var dataStored = DetailResModel()
        var mLocationData = ""
        var mTeamData = ""
        var mTagData = ""
        val listLocation = ArrayList<String>()
        val listTeam = ArrayList<String>()
        val listTags = ArrayList<String>()


        val mPrefs = application.getSharedPreferences("data", AppCompatActivity.MODE_PRIVATE)
        val gson = Gson()
        val json = mPrefs.getString("detailsArray", "")
        val json1 = mPrefs.getString("pricing", "")
        var jsArray = JSONArray()

        if (json != "") {
            dataStored = gson.fromJson(json, DetailResModel::class.java)
            Log.e("", dataStored.check)

            if (dataStored.teamMember.isNotEmpty()) {


                for (j in 0 until dataStored.teamMember.size) {
                    listTeam.add(dataStored.teamMember[j].staff_id)
                }
                mTeamData = TextUtils.join(",", listTeam)
            }


            if (dataStored.location.isNotEmpty()) {

                for (i in 0 until dataStored.location.size) {
                    listLocation.add(dataStored.location[i].id)
                }
                mLocationData = TextUtils.join(",", listLocation)
            }


            if (dataStored.tags.isNotEmpty()) {
                for (k in 0 until dataStored.tags.size) {
                    listTags.add(dataStored.tags[k].id)
                }
                mTagData = TextUtils.join(",", listTags)
            }

        }


        if (json1 != "") {

            jsArray = JSONArray(json1)
            // val turnsType = object : TypeToken<JSONArray>() {}.type
            // dataStored1 = gson.fromJson(json1, turnsType)
            //Log.e("", pricingData.discountPrice)
        }

        var campaignId = ""
        if (mPrefs.getString("campaign_id", "") != "") {
            campaignId = mPrefs.getString("campaign_id", "")
        }
        val service = retrofit.create(WebServicesMerchants::class.java)
        val call: Call<ResponseBody> =
            service.saveSubscription(
                campaignId,
                auth,
                image,
                name,
                product,
                campaign_start,
                campaign_end,
                campaign_type, jsArray,
                dataStored.subInfo,
                mTeamData,
                mTagData,
                dataStored.shareable,
                dataStored.check,
                mLocationData,
                dataStored.subscriber, "0"
            )
        call.enqueue(object : Callback<ResponseBody> {


            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {

                val res = response.body()!!.string()
                val json = JSONObject(res)
                val status = json.optString("status")
                val msg = json.optString("msg")
                val data = json.optJSONObject("data")
                val modelStatus = ModelStatusMsg()
                if (status == "true") {

                    try {
                        val basicModel = BasicModel()
                        val campaignInfo = data.getJSONObject("campaign_info")
                        val subscription = data.getJSONArray("subscription")

                        basicModel.steps = campaignInfo.optJSONArray("steps").toString()
                        val campaignId = campaignInfo.optString("campaign_id").toString()
                        basicModel.campaign_id = campaignInfo.optString("campaign_id")
                        val productInfo = campaignInfo.optJSONArray("productInfo")
                        if (product != "") {
                            val tagInfo = campaignInfo.optJSONArray("protagInfo")
                            for (i in 0 until tagInfo.length()) {
                                val products = tagInfo.getJSONObject(i)
                                val modelSubCategory = ModelSubCategory()
                                modelSubCategory.name = products.optString("name")
                                modelSubCategory.id = products.optString("id")
                                modelSubCategory.parentId = products.optString("parent_id").toInt()
                                basicModel.tags.add(modelSubCategory)
                            }
                            val parentInfo =  campaignInfo.optJSONArray("parent_tags")
                            for(p in 0 until parentInfo.length()){
                                val parentTag = parentInfo.getJSONObject(p)
                                val categoryModel = CategoryModel()
                                categoryModel.categoryName = parentTag.optString("parent_name")
                                categoryModel.categoryId = parentTag.optString("parent_id")
                                basicModel.parentTags.add(categoryModel)
                            }
                        }

                        basicModel.product = campaignInfo.optString("products")
                        basicModel.image = campaignInfo.optString("image")
                        basicModel.name = campaignInfo.optString("name")
                        basicModel.campaign_end = campaignInfo.optString("campaign_end")
                        basicModel.campaign_start = campaignInfo.optString("campaign_start")
                        basicModel.feature_image = campaignInfo.optString("feature_image")
                        basicModel.logo = campaignInfo.optString("logo")
                        basicModel.campaign_type = campaign_type

                        for (i in 0 until productInfo.length()) {
                            val products = productInfo.getJSONObject(i)
                            val productInfoModel = ProductInfoModel()
                            productInfoModel.name = products.optString("name")
                            productInfoModel.price = products.optString("price")
                            productInfoModel.id = products.optString("id")
                            productInfoModel.discountPrice = products.optString("discount_price")
                            basicModel.productInfo.add(productInfoModel)
                        }


                        for (i in 0 until subscription.length()) {
                            val products = subscription.getJSONObject(i)
                            val subscriptionDeatils = ModelSubscriptionCard()
                            subscriptionDeatils.campaign_id = products.optString("campaign_id")
                            subscriptionDeatils.price = products.optString("price")
                            subscriptionDeatils.discount_price = products.optString("discount_price")
                            subscriptionDeatils.redeemtion_cycle_qty = products.optString("redeemtion_cycle_qty")
                            subscriptionDeatils.frequency_type = products.optString("frequency_type")
                            subscriptionDeatils.frequency_value = products.optString("frequency_value")
                            subscriptionDeatils.id = products.optString("id")
                            subscriptionDeatils.subscription_id = products.optString("subscription_id")
                            subscriptionDeatils.unit = products.optString("unit")
                            basicModel.subscriptionArray.add(subscriptionDeatils)

                        }


                        val mPrefs =
                            application.getSharedPreferences("data", AppCompatActivity.MODE_PRIVATE)
                        val prefsEditor = mPrefs.edit()
                        val gson = Gson()
                        val json = gson.toJson(basicModel)
                        prefsEditor.putString("campaign_id", campaignId)
                        prefsEditor.putString("basicData", json)

                        prefsEditor.apply()

                        mDataBasic.value = basicModel
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }


                } else {
                    modelStatus.msg = msg
                    modelStatus.status = status
                    mDataFailure.value = modelStatus
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {

                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus

            }
        })

    }


    fun basicData(): MutableLiveData<BasicModel> {
        return mDataBasic
    }


//---------------------------------------------DETAILS----------------------------------//

    fun details(
        subscription_info: String,
        limit: String,
        team_member: String,
        tags: String,
        sharable: String,
        privacy: String,
        location: String,
        max_subscriber: String
    ) {

        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()


        val auth = Constant.getPrefs(application).getString(Constant.auth_code, "")
        val service = retrofit.create(WebServicesMerchants::class.java)
        var dataStored = BasicModel()
        val mPrefs = application.getSharedPreferences("data", AppCompatActivity.MODE_PRIVATE)
        val gson = Gson()
        val json1 = mPrefs.getString("pricing", "")
        val json = mPrefs.getString("basicData", "")
        var jsArray = JSONArray()

        var products = ""
        val list = ArrayList<String>()

        if (json1 != "") {
            jsArray = JSONArray(json1)
        }

        if (json != "") {
            dataStored = gson.fromJson(json, BasicModel::class.java)
            if (dataStored.productInfo.isNotEmpty()) {
                for (i in 0 until dataStored.productInfo.size) {
                    list.add(dataStored.productInfo[i].id)
                    Log.e("array List of Products", dataStored.productInfo[i].id)
                }
                products = TextUtils.join(",", list)
            }


        }
        var campaignId = ""
        if (mPrefs.getString("campaign_id", "") != "") {
            campaignId = mPrefs.getString("campaign_id", "")
        }

        val call: Call<ResponseBody> = service.saveSubscription(
            campaignId,
            auth,
            dataStored.image,
            dataStored.name,
            products,
            dataStored.campaign_start,
            dataStored.campaign_end,
            dataStored.campaign_type,
            jsArray,
            subscription_info,
            team_member,
            tags,
            sharable,
            privacy,
            location,
            max_subscriber, "0"
        )
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                val res = response.body()!!.string()
                val json = JSONObject(res)
                val status = json.optString("status")
                val msg = json.optString("msg")
                val data2 = json.optJSONObject("data")
                val data = data2.optJSONObject("campaign_info")
                val modelStatus = ModelStatusMsg()


                if (status == "true") {

                    try {
                        val detailResModel = DetailResModel()
                        detailResModel.steps = data.optJSONArray("steps").toString()
                        detailResModel.campaignId = data.optString("campaign_id")
                        detailResModel.subInfo = data.optString("description")
                        detailResModel.subscriber = data.optString("max_subscriber")
                        detailResModel.shareable = data.optString("share_setting")
                        detailResModel.check = data.optString("is_public")
                        detailResModel.subLimit = data.optString("limit")

                        val locationData = data.optJSONArray("locationInfo")
                        val tagsData = data.optJSONArray("tagInfo")
                        val teamInfoData = data.optJSONArray("teamInfo")
                        val parent_tags = data.optJSONArray("parent_tags")


                        for (i in 0 until locationData.length()) {
                            val location = locationData.getJSONObject(i)
                            val modellocation = ModelSearchLocation()
                            modellocation.id = location.optString("id")
                            modellocation.city = location.optString("city")
                            modellocation.street = location.optString("street")
                            modellocation.state = location.optString("state")
                            modellocation.zip_code = location.optString("zip_code")
                            modellocation.lat = location.optString("lat")
                            modellocation.long = location.optString("long")
                            val store_timing = location.getJSONArray("store_timing")
                            for (store in 0 until store_timing.length()){
                                val storeData = store_timing.optJSONObject(store)
                                val modelStore = ModelStoreLocation()
                                modelStore.close = storeData.optString("close")
                                modelStore.open = storeData.optString("open")
                                modelStore.day = storeData.optString("day")
                                modelStore.status = storeData.optString("status")
                                modelStore.checked = storeData.optString("checked")
                                modellocation.store_timing.add(modelStore)
                            }
                            detailResModel.location.add(modellocation)
                        }

                        for (p in 0 until parent_tags.length()) {
                            val parentTag = parent_tags.getJSONObject(p)
                            val categoryModel = CategoryModel()
                            categoryModel.categoryName = parentTag.optString("parent_name")
                            categoryModel.categoryId = parentTag.optString("parent_id")
                            detailResModel.parentTags.add(categoryModel)
                        }



                        for (j in 0 until tagsData.length()) {

                            val tags = tagsData.getJSONObject(j)
                            val modelSubCategory = ModelSubCategory()


                            modelSubCategory.id = tags.optString("id")
                            modelSubCategory.name = tags.optString("name")
                            detailResModel.tags.add(modelSubCategory)
                        }


                        for (k in 0 until teamInfoData.length()) {

                            val teamInfo = teamInfoData.getJSONObject(k)

                            val manageTeamModel = ManageTeamModel()

                            manageTeamModel.business_id = teamInfo.optString("business_id")
                            manageTeamModel.staff_id = teamInfo.optString("staff_id")
                            manageTeamModel.namee = teamInfo.optString("name")
                            manageTeamModel.speciality = teamInfo.optString("speciality")
                            manageTeamModel.profile_image = teamInfo.optString("profile_image")
                            manageTeamModel.type = teamInfo.optString("type")
                            detailResModel.teamMember.add(manageTeamModel)
                        }
                        val mPrefs =
                            application.getSharedPreferences("data", AppCompatActivity.MODE_PRIVATE)
                        val prefsEditor = mPrefs.edit()
                        val gson = Gson()
                        val json = gson.toJson(detailResModel)
                        prefsEditor.putString("detailsArray", json)
                        prefsEditor.putString("campaign_id", detailResModel.campaignId)
                        prefsEditor.apply()
                        mDataDetails.value = detailResModel


                    } catch (e: Exception) {
                        e.printStackTrace()
                    }


                } else {
                    modelStatus.msg = msg
                    modelStatus.status = status
                    mDataFailure.value = modelStatus
                }


            }


            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus

            }


        })


    }


    fun getDetailsData(): MutableLiveData<DetailResModel> {
        return mDataDetails
    }


    fun createSubscription() {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()


        val auth = Constant.getPrefs(application).getString(Constant.auth_code, "")
        val mPrefs = application.getSharedPreferences("data", AppCompatActivity.MODE_PRIVATE)
        var campaignId = ""
        if (mPrefs.getString("campaign_id", "") != "") {
            campaignId = mPrefs.getString("campaign_id", "")
        }

        val service = retrofit.create(WebServicesMerchants::class.java)

        val call: Call<ResponseBody> =
            service.createSub(auth, campaignId, "1")
        call.enqueue(object : Callback<ResponseBody> {


            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {

                try {
                    val res = response.body()!!.string()
                    val json = JSONObject(res)
                    val status = json.optString("status")
                    val msg = json.optString("msg")
                    val modelStatus = ModelStatusMsg()


                    if (status != "true") {


                        modelStatus.msg = msg
                        modelStatus.status = status
                        mDataFailure.value = modelStatus

                    }
                } catch (e: Exception) {

                }


            }


            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus


            }


        })


    }


}