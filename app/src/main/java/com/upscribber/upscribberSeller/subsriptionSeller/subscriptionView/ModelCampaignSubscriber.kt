package com.upscribber.upscribberSeller.subsriptionSeller.subscriptionView

import android.os.Parcel
import android.os.Parcelable

class ModelCampaignSubscriber() : Parcelable {
    var msg : String = ""
    var status : String = ""
    var order_id : String = ""
    var customer_id : String = ""
    var name : String = ""
    var contact_no : String = ""
    var email : String = ""
    var frequency_type : String = ""
    var frequency_value : String = ""
    var profile_image : String = ""
    var count : Int = 0

    constructor(parcel: Parcel) : this() {
        msg = parcel.readString()
        status = parcel.readString()
        order_id = parcel.readString()
        customer_id = parcel.readString()
        name = parcel.readString()
        contact_no = parcel.readString()
        email = parcel.readString()
        frequency_type = parcel.readString()
        frequency_value = parcel.readString()
        profile_image = parcel.readString()
        count = parcel.readInt()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(msg)
        parcel.writeString(status)
        parcel.writeString(order_id)
        parcel.writeString(customer_id)
        parcel.writeString(name)
        parcel.writeString(contact_no)
        parcel.writeString(email)
        parcel.writeString(frequency_type)
        parcel.writeString(frequency_value)
        parcel.writeString(profile_image)
        parcel.writeInt(count)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ModelCampaignSubscriber> {
        override fun createFromParcel(parcel: Parcel): ModelCampaignSubscriber {
            return ModelCampaignSubscriber(parcel)
        }

        override fun newArray(size: Int): Array<ModelCampaignSubscriber?> {
            return arrayOfNulls(size)
        }
    }

}