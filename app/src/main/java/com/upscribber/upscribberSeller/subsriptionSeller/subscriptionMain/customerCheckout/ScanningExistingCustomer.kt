package com.upscribber.upscribberSeller.subsriptionSeller.subscriptionMain.customerCheckout

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.zxing.Result
import com.upscribber.R
import com.upscribber.checkout.ModelCheckoutOne
import com.upscribber.commonClasses.Constant
import com.upscribber.payment.paymentCards.AddCardActivity
import com.upscribber.payment.paymentCards.AddCardViewModel
import com.upscribber.payment.paymentCards.PaymentCardModel
import com.upscribber.profile.ModelGetProfile
import com.upscribber.upscribberSeller.customerBottom.scanning.ScanningViewModel
import com.upscribber.upscribberSeller.subsriptionSeller.subscriptionMain.ModelSellerSubscriptions
import kotlinx.android.synthetic.main.activity_sanning_manually.*
import me.dm7.barcodescanner.zxing.ZXingScannerView

class ScanningExistingCustomer : AppCompatActivity(), ZXingScannerView.ResultHandler {

    lateinit var mViewModel: ScanningViewModel
    lateinit var mViewModel1: AddCardViewModel
    private lateinit var mScannerView: ZXingScannerView
    var modelProfile = ModelGetProfile()
    var modelCheckout = ModelCheckoutOne()
    var arrayCard = ArrayList<PaymentCardModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sanning_manually)
        mViewModel = ViewModelProviders.of(this)[ScanningViewModel::class.java]
        mViewModel1 = ViewModelProviders.of(this)[AddCardViewModel::class.java]
        checkPermission()
        requestPermission()
        initz()
        clickListeners()

        observerInit()
    }


    private fun observerInit() {
        mViewModel.getmDataStatus().observe(this, Observer {
            if (it.msg == "Invalid auth code") {
                Constant.commonAlert(this)
            } else {
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
                finish()
            }
        })

        mViewModel1.getmRetrieveCard().observe(this, androidx.lifecycle.Observer {
            arrayCard = it
            if (modelProfile.status == "true") {
                if (modelProfile.name.isEmpty() || modelProfile.contact_no.isEmpty() || modelProfile.email.isEmpty() || modelProfile.primary_address.street.isEmpty()
                    || modelProfile.primary_address.city.isEmpty() || modelProfile.primary_address.state.isEmpty() || modelProfile.primary_address.zip_code.isEmpty()
                ) {
                    startActivity(Intent(this, CheckoutMerchantSignUp::class.java)
                        .putExtra("totalPriceText", intent.getStringExtra("totalPriceText")).putExtra("modelPro",modelProfile)
                        .putExtra("modelSubscriptionModel", intent.getParcelableExtra<ModelSellerSubscriptions>("modelSubscriptionModel")))

                } else if (arrayCard.size > 0){
                    modelCheckout.billingType = ""
                    modelCheckout.streetName = modelProfile.primary_address.street
                    modelCheckout.city = modelProfile.primary_address.city
                    modelCheckout.stateName = modelProfile.primary_address.state
                    modelCheckout.zipCode = modelProfile.primary_address.zip_code
                    modelCheckout.suite = modelProfile.primary_address.suite
                    modelCheckout.billingStreet = modelProfile.billing_address.billing_street
                    modelCheckout.billingCity = modelProfile.billing_address.billing_city
                    modelCheckout.billingState = modelProfile.billing_address.billing_state
                    modelCheckout.billingZip = modelProfile.billing_address.billing_zipcode
                    modelCheckout.billingSuite = modelProfile.billing_address.billing_suite
                    modelCheckout.firstName = modelProfile.name
                    modelCheckout.emailAddress = modelProfile.email
                    modelCheckout.phoneNumber = modelProfile.contact_no
                    startActivity(
                        Intent(this, CompleteCheckOutMerchant::class.java)
                            .putExtra("modelSubscriptionModel", intent.getParcelableExtra<ModelSellerSubscriptions>("modelSubscriptionModel"))
                            .putExtra("totalPriceText",intent.getStringExtra("totalPriceText"))
                            .putExtra("modelProfile",modelProfile)
                            .putExtra("modelCheckoutMerchant",modelCheckout)
                    )
                } else{
                    modelCheckout.billingType = ""
                    modelCheckout.streetName = modelProfile.primary_address.street
                    modelCheckout.city = modelProfile.primary_address.city
                    modelCheckout.stateName = modelProfile.primary_address.state
                    modelCheckout.zipCode = modelProfile.primary_address.zip_code
                    modelCheckout.suite = modelProfile.primary_address.suite
                    modelCheckout.billingStreet = modelProfile.billing_address.billing_street
                    modelCheckout.billingCity = modelProfile.billing_address.billing_city
                    modelCheckout.billingState = modelProfile.billing_address.billing_state
                    modelCheckout.billingZip = modelProfile.billing_address.billing_zipcode
                    modelCheckout.billingSuite = modelProfile.billing_address.billing_suite
                    modelCheckout.firstName = modelProfile.name
                    modelCheckout.emailAddress = modelProfile.email
                    modelCheckout.phoneNumber = modelProfile.contact_no
                    startActivity(
                        Intent(this, AddCardActivity::class.java)
                            .putExtra("merchantCheckOut", "merchantCheckout")
                            .putExtra("modelSubscriptionModel", intent.getParcelableExtra<ModelSellerSubscriptions>("modelSubscriptionModel"))
                            .putExtra("modelProfile", modelProfile)
                            .putExtra("totalPriceText", intent.getStringExtra("totalPriceText"))
                            .putExtra("modelCheckoutMerchant", modelCheckout)
                    )
                }
            }

        })

        mViewModel.getmDataRedeemCodeData().observe(this, Observer {
            modelProfile = it
            mViewModel1.getRetrieveCards(modelProfile.stripe_customer_id)
        })

    }

    private fun initz() {
        mScannerView = findViewById(R.id.mScannerView)
        textView133.text = "Scan Customer's Checkout Code"
        redeemManual.text = "Enter Code"
    }

    private fun checkPermission(): Boolean {
        return ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.CAMERA
        ) == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(Manifest.permission.CAMERA),
            1
        )
    }

    private fun clickListeners() {
        redeemManual.setOnClickListener {
            startActivity(
                Intent(this, RedeemScreenCustomer::class.java).putExtra(
                    "totalPriceText",
                    intent.getStringExtra("totalPriceText")
                )
                    .putExtra("modelProfile", modelProfile)
                    .putExtra(
                        "modelSubscriptionModel",
                        intent.getParcelableExtra<ModelSellerSubscriptions>("modelSubscriptionModel")
                    )
                    .putExtra("merchantCheckOut", "merchantCheckout")
            )
        }

        imageView66.setOnClickListener {
            finish()
        }

    }


    public override fun onResume() {
        super.onResume()
        mScannerView.setResultHandler(this)
        mScannerView.startCamera()
        mScannerView.setAspectTolerance(0.5f)
        mScannerView.setAutoFocus(true)
    }

    public override fun onPause() {
        super.onPause()
        mScannerView.stopCamera()
    }

    override fun handleResult(p0: Result?) {
        try {
            val currentString = p0.toString()
            mViewModel.getRedeemCustomer(currentString)
        } catch (e: Exception) {
            e.printStackTrace()
            mScannerView.setResultHandler(this)
            mScannerView.startCamera()
            mScannerView.setAspectTolerance(0.5f)
            mScannerView.setAutoFocus(true)
        }

    }

}
