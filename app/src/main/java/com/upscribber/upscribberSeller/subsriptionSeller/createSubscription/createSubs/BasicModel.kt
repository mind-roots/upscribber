package com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.createSubs

import android.os.Parcel
import android.os.Parcelable
import com.upscribber.filters.CategoryModel
import com.upscribber.filters.ModelSubCategory
import com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.subscription.ModelSubscriptionCard
import com.upscribber.upscribberSeller.subsriptionSeller.subscriptionMain.ProductInfoModel

class BasicModel() : Parcelable {

    var logo: String = ""
    var feature_image: String = ""
    var image: String = ""
    var name: String = ""
    var product: String = ""
    var campaign_start: String = ""
    var campaign_end: String = ""
    var campaign_type: String = ""
    var steps: String = ""
    var campaign_id: String = ""
    var productInfo = ArrayList<ProductInfoModel>()
    var tags = ArrayList<ModelSubCategory>()
    var subscriptionArray = ArrayList<ModelSubscriptionCard>()
    var parentTags = ArrayList<CategoryModel>()

    constructor(parcel: Parcel) : this() {
        logo = parcel.readString()
        feature_image = parcel.readString()
        image = parcel.readString()
        name = parcel.readString()
        product = parcel.readString()
        campaign_start = parcel.readString()
        campaign_end = parcel.readString()
        campaign_type = parcel.readString()
        steps = parcel.readString()
        campaign_id = parcel.readString()
        productInfo = parcel.createTypedArrayList(ProductInfoModel)
        tags = parcel.createTypedArrayList(ModelSubCategory)
        subscriptionArray = parcel.createTypedArrayList(ModelSubscriptionCard)
        parentTags = parcel.createTypedArrayList(CategoryModel)
    }


    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(logo)
        parcel.writeString(feature_image)
        parcel.writeString(image)
        parcel.writeString(name)
        parcel.writeString(product)
        parcel.writeString(campaign_start)
        parcel.writeString(campaign_end)
        parcel.writeString(campaign_type)
        parcel.writeString(steps)
        parcel.writeString(campaign_id)
            parcel.writeTypedList(productInfo)
        parcel.writeTypedList(tags)
        parcel.writeTypedList(subscriptionArray)
        parcel.writeTypedList(parentTags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<BasicModel> {
        override fun createFromParcel(parcel: Parcel): BasicModel {
            return BasicModel(parcel)
        }

        override fun newArray(size: Int): Array<BasicModel?> {
            return arrayOfNulls(size)
        }
    }

}