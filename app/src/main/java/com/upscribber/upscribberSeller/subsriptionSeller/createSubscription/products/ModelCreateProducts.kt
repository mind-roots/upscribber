package com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.products

import android.os.Parcel
import android.os.Parcelable

class ModelCreateProducts() : Parcelable{

    lateinit var productName : String
    lateinit var productPrice : String

    constructor(parcel: Parcel) : this() {
        productName = parcel.readString()
        productPrice = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(productName)
        parcel.writeString(productPrice)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ModelCreateProducts> {
        override fun createFromParcel(parcel: Parcel): ModelCreateProducts {
            return ModelCreateProducts(parcel)
        }

        override fun newArray(size: Int): Array<ModelCreateProducts?> {
            return arrayOfNulls(size)
        }
    }
}