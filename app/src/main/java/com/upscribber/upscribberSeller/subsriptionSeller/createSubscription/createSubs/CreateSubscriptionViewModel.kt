package com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.createSubs

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.upscribber.commonClasses.ModelStatusMsg
import com.upscribber.filters.CategoryModel
import com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.otherDiscounts.OtherDiscountsModel
import com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.products.ModelCreateProducts
import com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.subscription.ModelSubscriptionCard
import org.json.JSONArray
import java.io.File

class CreateSubscriptionViewModel(application: Application) : AndroidViewModel(application) {
    var createSubscriptionRepository: CreateSubscriptionRepository =
        CreateSubscriptionRepository(
            application
        )

    fun getProductsList(): LiveData<ArrayList<ModelCreateProducts>> {
        return createSubscriptionRepository.getProducts()

    }


    fun getSubscriptionCardList(

        subscription_data: JSONArray

    ) {
        createSubscriptionRepository.getSubsCard(
            subscription_data
        )

    }


    fun mSubsSuccessful(): LiveData<ArrayList<ModelSubscriptionCard>> {
        return createSubscriptionRepository.mSuccessful()
    }

    fun getSubscriptionOtherDiscounts(): LiveData<ArrayList<OtherDiscountsModel>> {
        return createSubscriptionRepository.getSubsOtherDiscount()

    }

    fun getproductCategory(auth: String) {
        createSubscriptionRepository.getProductCategories(auth)
    }

    fun getStatus(): LiveData<ModelStatusMsg> {
        return createSubscriptionRepository.getmDataFailure()
    }


    fun getProductCategoryAll(): MutableLiveData<ArrayList<CategoryModel>> {
        return createSubscriptionRepository.getProductCategory()
    }

    fun getBasic(
        uri: String,
        name: String,
        product: String,
        campaign_start: String,
        campaign_end: String,
        campaign_type: String
    ) {
        createSubscriptionRepository.saveBasic(uri, name, product, campaign_start, campaign_end, campaign_type)
    }

    fun getBasicData() : MutableLiveData<BasicModel>{
        return createSubscriptionRepository.basicData()
    }




    fun getDetails(
        subscription_info: String,
        limit: String,
        team_member: String,
        tags: String,
        sharable: String,
        privacy: String,
        location: String,
        max_subscriber : String
    ) {
        createSubscriptionRepository.details(
            subscription_info,
            limit,
            team_member,
            tags,
            sharable,
            privacy,
            location,max_subscriber
        )
    }

    fun getDetails(): MutableLiveData<DetailResModel> {
        return createSubscriptionRepository.getDetailsData()
    }


    fun createSubscription() {
        createSubscriptionRepository.createSubscription()
    }


}