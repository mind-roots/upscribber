package com.upscribber.upscribberSeller.subsriptionSeller.location.days

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.commonClasses.Constant
import com.upscribber.R
import com.upscribber.databinding.DaysSelectedRecyclerBinding
import kotlinx.android.synthetic.main.days_selected_recycler.view.*
import kotlinx.android.synthetic.main.layout_recycler.view.*

class AdapterSelectDays(private val mContext: Context) :
    RecyclerView.Adapter<AdapterSelectDays.MyViewHolder>() {

    private var list: ArrayList<ModelSelectDays> = ArrayList()
    var listener = mContext as SelectDays

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val v = LayoutInflater.from(mContext)
            .inflate(R.layout.days_selected_recycler, parent, false)
        return MyViewHolder(v)

    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = list[position]
        holder.itemView.checkBox.text = model.headings
        holder.itemView.checkBox.isChecked = model.checkBox


        holder.itemView.checkBox.setOnCheckedChangeListener { buttonView, isChecked ->


            list[position].checkBox = !list[position].checkBox
            listener.daysData(list[position], position)
        }

    }

    fun update(it: ArrayList<ModelSelectDays>) {
        list = it
        notifyDataSetChanged()

    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    interface SelectDays {
        fun daysData(model: ModelSelectDays, position: Int)
    }
}
