package com.upscribber.upscribberSeller.subsriptionSeller.subscriptionMain.customerCheckout

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.upscribber.becomeseller.seller.ModelAddress
import com.upscribber.commonClasses.ModelStatusMsg
import com.upscribber.profile.ModelGetProfile

class MerchantCheckoutViewModel(application: Application) : AndroidViewModel(application) {

    var merchantCheckOutRepository: MerchantCheckOutRepository =
        MerchantCheckOutRepository(application)


    fun getMerchantSignUp(
        contact_no: String,
        email: String,
        signup_type: String,
        auth_code: String,
        name: String,
        billing_street: String,
        billing_state: String,
        billing_city: String,
        billing_zipcode: String,
        billing_suite: String,
        home_street: String,
        home_state: String,
        home_city: String,
        home_zipcode: String,
        home_suite: String,
        reg_type: String

    ) {
        merchantCheckOutRepository.getMerchantSignUp(
            contact_no,
            email,
            signup_type,
            auth_code,
            name,
            billing_street,
            billing_state,
            billing_city,
            billing_zipcode,
            billing_suite,
            home_street,
            home_state,
            home_city,
            home_zipcode,
            home_suite,
            reg_type

        )

    }


    fun getStatus(): LiveData<ModelStatusMsg> {
        return merchantCheckOutRepository.getmDataFailure()
    }


    fun getSignedUpData(): LiveData<ModelGetProfile> {
        return merchantCheckOutRepository.getmDataSignedUp()
    }

    fun updateAddress(
        auth: String,
        userId: String,
        street: String,
        city: String,
        state: String,
        zipCode: String,
        suite: String,
        billingStreet: String,
        billingCity: String,
        billingState: String,
        billingZipcode: String,
        billingSuite: String,
        type: String,
        u_type: String,
        value: String
    ) {
        merchantCheckOutRepository.updateAddress(
            auth,
            userId,
            street,
            city,
            state,
            zipCode,
            suite,
            billingStreet,
            billingCity,
            billingState,
            billingZipcode,
            billingSuite,
            type,
            u_type,
            value
        )

    }

    fun getmDataUpdateAddress(): LiveData<ModelAddress> {
        return merchantCheckOutRepository.getmDataUpdateAddress()
    }


    fun getmDataUpdateProfile(): LiveData<ModelGetProfile> {
        return merchantCheckOutRepository.getmDataUpdateProfile()
    }

    fun getUpdateProfile(
        userId: String,
        name: String,
        number: String,
        email: String,
        type: String,
        billingStreet: String,
        billingState: String,
        billingCity: String,
        billingZip: String,
        billingSuite: String,
        value: String,
        streetName: String,
        city: String,
        stateName: String,
        zipCode: String,
        suite: String
    ) {
        merchantCheckOutRepository.getUpdateProfile(userId, name, number, email, type,billingStreet,billingState,billingCity,billingZip,billingSuite,value,streetName,city,stateName,zipCode,suite)

    }


}