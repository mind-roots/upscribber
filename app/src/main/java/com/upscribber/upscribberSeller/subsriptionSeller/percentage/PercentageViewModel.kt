package com.upscribber.upscribberSeller.subsriptionSeller.percentage

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

class PercentageViewModel(application: Application) : AndroidViewModel(application) {

    var percentageRepository : PercentageRepository = PercentageRepository(application)

    fun getProductsList(): LiveData<ArrayList<ModelPercentage>> {
        return  percentageRepository.getList()

    }

    fun updateList(position: Int): MutableLiveData<ArrayList<ModelPercentage>> {

        return  percentageRepository.updateList(position)
    }
}