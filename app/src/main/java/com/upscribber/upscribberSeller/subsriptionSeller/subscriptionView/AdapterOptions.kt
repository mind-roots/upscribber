package com.upscribber.upscribberSeller.subsriptionSeller.subscriptionView

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.R
import com.upscribber.upscribberSeller.subsriptionSeller.subscriptionMain.ModelMain
import kotlinx.android.synthetic.main.option_item.view.*

class AdapterOptions(
    private val mContext: Context,
    private val dealList: ModelMain,
    private val it: ArrayList<ModelOption>
) :
    RecyclerView.Adapter<AdapterOptions.MyViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(mContext)
            .inflate(
                R.layout.option_item
                , parent, false
            )
        return AdapterOptions.MyViewHolder(v)
    }

    override fun getItemCount(): Int {
        return it.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = it[position]
        holder.itemView.name.text = model.name

        if (dealList.subscription.size > 0) {

            var introPrice = ""

            introPrice = if (dealList.subscription[0].introPrice == "0" || dealList.subscription[0].introPrice == "0") {
                "No introductory price"
            } else {
                dealList.subscription[0].introPrice
            }
            when (position) {

                0 -> holder.itemView.value.text = dealList.subscription[0].freeTrail
                1 -> holder.itemView.value.text = introPrice
                2 -> holder.itemView.value.text = dealList.subs
            }
        } else {
            when (position) {
                0 -> holder.itemView.value.text = "No free Trial"
                1 -> holder.itemView.value.text = "No introductory price"
                2 -> holder.itemView.value.text = dealList.subs
            }
        }


        //holder.itemView.value.text = dealList[0].freeTrail


    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

}
