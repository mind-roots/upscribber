package com.upscribber.upscribberSeller.subsriptionSeller.location.searchLoc

import android.os.Parcel
import android.os.Parcelable
import com.upscribber.upscribberSeller.store.storeMain.StoreModel
import org.json.JSONArray

class ModelSearchLocation() : Parcelable {

    var default: String = ""
    var locationName: String = ""
    var locationTimings: String = ""
    var status: Boolean = false
    var statusPrimary: Boolean = false
    var msg: String = ""
    var status1: String = ""
    var street: String = ""
    var id: String = ""
    var city: String = ""
    var state: String = ""
    var state_code: String = ""
    var zip_code: String = ""
    var lat: String = ""
    var long: String = ""
    var business_id = ""
    var user_id = ""
    var suite = ""
    var created_at = ""
    var updated_at = ""
    var store_timing: ArrayList<ModelStoreLocation> = ArrayList()

    constructor(parcel: Parcel) : this() {
        default = parcel.readString()
        locationName = parcel.readString()
        locationTimings = parcel.readString()
        status = parcel.readByte() != 0.toByte()
        statusPrimary = parcel.readByte() != 0.toByte()
        msg = parcel.readString()
        status1 = parcel.readString()
        street = parcel.readString()
        id = parcel.readString()
        city = parcel.readString()
        state = parcel.readString()
        zip_code = parcel.readString()
        lat = parcel.readString()
        long = parcel.readString()
        business_id = parcel.readString()
        user_id = parcel.readString()
        suite = parcel.readString()
        created_at = parcel.readString()
        updated_at = parcel.readString()
        store_timing = parcel.createTypedArrayList(ModelStoreLocation)
    }


    fun address(): String {
        return "$street,$city,$state,$zip_code"
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(default)
        parcel.writeString(locationName)
        parcel.writeString(locationTimings)
        parcel.writeByte(if (status) 1 else 0)
        parcel.writeByte(if (statusPrimary) 1 else 0)
        parcel.writeString(msg)
        parcel.writeString(status1)
        parcel.writeString(street)
        parcel.writeString(id)
        parcel.writeString(city)
        parcel.writeString(state)
        parcel.writeString(zip_code)
        parcel.writeString(lat)
        parcel.writeString(long)
        parcel.writeString(business_id)
        parcel.writeString(user_id)
        parcel.writeString(suite)
        parcel.writeString(created_at)
        parcel.writeString(updated_at)
        parcel.writeTypedList(store_timing)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ModelSearchLocation> {
        override fun createFromParcel(parcel: Parcel): ModelSearchLocation {
            return ModelSearchLocation(parcel)
        }

        override fun newArray(size: Int): Array<ModelSearchLocation?> {
            return arrayOfNulls(size)
        }
    }
}

