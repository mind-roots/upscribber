package com.upscribber.upscribberSeller.subsriptionSeller.location.searchLoc

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.daimajia.swipe.SwipeLayout
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter
import com.upscribber.R
import com.upscribber.databinding.SearchLocationRecyclerListBinding
import com.upscribber.upscribberSeller.subsriptionSeller.location.DaysLogic.DayModel
import com.upscribber.upscribberSeller.subsriptionSeller.location.DaysLogic.ModelDays
import com.upscribber.upscribberSeller.subsriptionSeller.location.DaysLogic.ModelMain
import com.upscribber.upscribberSeller.subsriptionSeller.location.DaysLogic.ResultModel
import com.upscribber.upscribberSeller.subsriptionSeller.location.days.AddLocationActivity
import kotlinx.android.synthetic.main.search_location_recycler_list.view.*

class AdapterSearchLocation(
    var context: Context,
    listen: SelectLocationActivity,
    var stringExtra: String
) :
    RecyclerSwipeAdapter<AdapterSearchLocation.MyViewHolder>() {

    var listener = listen as Selectionn
    var listener1 = listen as Selectionn
    var listener2 = listen as Selectionn
    var listener3 = listen as Selectionn
    var dayOf = arrayListOf("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday")

    private lateinit var binding: SearchLocationRecyclerListBinding
    private var items = ArrayList<ModelSearchLocation>()
    var myModel = ArrayList<ModelDays>()
    var daysArray = ArrayList<DayModel>()
    var mainList = ArrayList<ModelMain>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        binding = SearchLocationRecyclerListBinding.inflate(LayoutInflater.from(context), parent, false)
        return MyViewHolder(binding)

    }

    override fun getSwipeLayoutResourceId(position: Int): Int {

        return R.id.swipe
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = items[position]
        holder.bind(model)


        holder.itemView.card1.setOnClickListener {

            if (stringExtra == "others") {
                val intent = Intent(context, AddLocationActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                intent.putExtra("model", model)
                context.startActivity(intent)

            } else {
                listener.onSelectionBackgroundd(position)
            }

        }
/*
        if (model.store_timing.size > 0) {

            val storeTime = model.store_timing
            val setStoreTime = ArrayList<ModelStoreLocation>()

            for (j in 0 until model.store_timing.size) {
                for (i in 0 until storeTime.size) {
                    if (model.store_timing[j].open == storeTime[i].open
                        && model.store_timing[j].close == storeTime[i].close
                        && dayOf[j] != dayOf[i]
                    ) {
                        setStoreTime.add(model.store_timing[j])
                        break
                    }
                }
            }
        }*/


        if (stringExtra == "others") {
            if (model.default == model.id) {
                holder.itemView.imageView39.setImageResource(R.drawable.ic_star_simple)
                holder.itemView.imageDone.visibility = View.GONE
            } else {
                holder.itemView.imageDone.visibility = View.GONE
                holder.itemView.imageView39.setImageResource(R.drawable.ic_location_purple)
            }
        } else {
            holder.itemView.imageViewSlide.setImageResource(R.drawable.ic_edit_greyback)
            holder.itemView.textViewSlide.text = "Edit"
            if (model.status) {
                holder.itemView.imageDone.visibility = View.VISIBLE
                holder.itemView.card1.setBackgroundResource(R.drawable.bg_seller_blue_outline)
            } else {
                holder.itemView.imageDone.visibility = View.GONE
                holder.itemView.card1.setBackgroundResource(0)
            }
        }

        holder.itemView.swipe.showMode = SwipeLayout.ShowMode.PullOut
//        holder.itemView.swipe.toggle(true)
        holder.itemView.swipe.addDrag(
            SwipeLayout.DragEdge.Right,
            holder.itemView.swipe
                .findViewById(R.id.bottom_wrapper)
        )

        holder.itemView.editLoc.setOnClickListener {
//            context.startActivity(Intent(context, AddLocationActivity::class.java).putExtra("model", model))
            if (stringExtra == "others") {
                listener1.SetPrimary(position)

            } else {
                val intent = Intent(context, AddLocationActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                intent.putExtra("model", model)
                context.startActivity(intent)
            }
            holder.itemView.swipe.toggle(true)
        }

        holder.itemView.delete.setOnClickListener {
            listener3.delete(position)
            holder.itemView.swipe.toggle(true)
        }
////////////////////////////////////////////////////////////////////////////////////
       myModel = ArrayList()
         daysArray = ArrayList()
         mainList = ArrayList()
        for(i in 0 until model.store_timing.size) {
            var model22 = ModelDays()
            if (i==0) {
                model22.startTime = model.store_timing[i].open
                model22.closeTime = model.store_timing[i].close
                model22.day = "Sunday"
                myModel.add(model22)
            }
           if (i==1) {
                model22.startTime = model.store_timing[i].open
                model22.closeTime = model.store_timing[i].close
                model22.day = "Monday"
                myModel.add(model22)
            }
            if (i==2) {
                model22 = ModelDays()
                model22.startTime = model.store_timing[i].open
                model22.closeTime = model.store_timing[i].close
                model22.day = "Tuesday"
                myModel.add(model22)
            }
            if (i==3) {
                model22 = ModelDays()
                model22.startTime = model.store_timing[i].open
                model22.closeTime = model.store_timing[i].close
                model22.day = "Wednesday"
                myModel.add(model22)
            }
            if (i==4) {
                model22 = ModelDays()
                model22.startTime = model.store_timing[i].open
                model22.closeTime = model.store_timing[i].close
                model22.day = "Thursday"
                myModel.add(model22)
            }
            if (i==5) {
                model22 = ModelDays()
                model22.startTime = model.store_timing[i].open
                model22.closeTime = model.store_timing[i].close
                model22.day = "Friday"
                myModel.add(model22)
            }
            if (i==6) {
                model22 = ModelDays()
                model22.startTime = model.store_timing[i].open
                model22.closeTime = model.store_timing[i].close
                model22.day = "Saturday"
                myModel.add(model22)
            }



        }
        val array2 = arrayListOf<DayModel>()
        for (i in 0 until myModel.size) {


            var model = DayModel()
            if (i ==0) {
                model.day = "Sun"
            }
            else if (i == 1) {
                model.day = "Mon"
            } else if (i == 2) {
                model.day = "Tue"
            } else if (i == 3) {
                model.day = "Wed"
            } else if (i == 4) {
                model.day = "Thu"
            } else if (i == 5) {
                model.day = "Fri"
            } else if (i == 6) {
                model.day = "Sat"
            }

            val startTime: String = myModel[i].startTime
            val endTime: String = myModel[i].closeTime
            if (startTime == endTime) {
//            if (startTime.isEmpty() &&endTime.isEmpty()) {
                model.time = "closed"
            } else {
                model.time = "$startTime to $endTime"
            }
            array2.add(model)
        }

    for (modelDay in array2) {
        if (!checkIfAlreadyExist(modelDay.day)) {
            val subList = getDayTimeString(modelDay, array2)
            mainList.add(subList)
        }
    }
    val existdays = ArrayList<ResultModel>()
    for (index in mainList.indices) {
        val resultString = mainList[index].resultString
        Log.i("$index : ", resultString.day)
        existdays.add(resultString)

    }
        var daysList=""
        for(i in 0 until existdays.size){
           if (existdays[i].time!="closed") {
                if (daysList.isEmpty()) {
                    daysList = "" + existdays[i].day + " (" + existdays[i].time + ")"
                } else {
                    daysList = daysList + ", " + existdays[i].day + " (" + existdays[i].time + ")"
                }
            }
        }
        holder.itemView.days.text = daysList
////////////////////////////////////////////////////////////////////////////////////


        holder.itemView.swipe.addSwipeListener(object : SwipeLayout.SwipeListener {
            override fun onOpen(layout: SwipeLayout?) {
                mItemManger.closeAllExcept(layout)

            }

            override fun onUpdate(layout: SwipeLayout?, leftOffset: Int, topOffset: Int) {
            }

            override fun onStartOpen(layout: SwipeLayout?) {

            }

            override fun onStartClose(layout: SwipeLayout?) {

            }

            override fun onHandRelease(layout: SwipeLayout?, xvel: Float, yvel: Float) {
                layout?.close()

            }

            override fun onClose(layout: SwipeLayout?) {

            }
        })

        mItemManger.bindView(holder.itemView, position)
    }

    class MyViewHolder(var binding: SearchLocationRecyclerListBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(obj: ModelSearchLocation) {
            binding.model = obj
            binding.executePendingBindings()
        }
    }

    fun update(items: ArrayList<ModelSearchLocation>) {
        this.items = items
        notifyDataSetChanged()
    }

    fun update1(backgroundChange: ArrayList<ModelSearchLocation>, position: Int) {
        listener2.SetSingleSelection(position)

    }

    interface Selectionn {
        fun onSelectionBackgroundd(position: Int)
        fun SetPrimary(positionn: Int)
        fun SetSingleSelection(pos: Int)
        fun delete(pos: Int)
    }


    fun checkIfAlreadyExist(day: String): Boolean {
        if (mainList.isNotEmpty()) {
            for (value in mainList) {
                val list = value.list
                for (data in list) {
                    if (data.day == day) {
                        return true
                    }
                }
            }
        }

        return false
    }
    private fun getDayTimeString(timeToFind: DayModel, list: ArrayList<DayModel>): ModelMain {
        val modelMain = ModelMain()
        val subList = ArrayList<DayModel>()
        var timeToReturn = ""
        var lastIndex = -1
        for (i in list.indices) {
            val model = list[i]
            if (timeToFind.time == model.time) {

                subList.add(model)

                if (lastIndex != -1 && i - lastIndex == 1) {

                    if (timeToReturn.contains("-") && !timeToReturn.contains(",")) {
                        timeToReturn = "${timeToFind.day}-${model.day}"
                    } else {
                        timeToReturn = "$timeToReturn-${model.day}"
                    }

                } else {
                    if (timeToReturn.isEmpty()) {
                        timeToReturn = model.day
                    } else {
                        timeToReturn = "$timeToReturn, ${model.day}"
                    }

                }

                lastIndex = i
            }
        }

        modelMain.list = subList
        modelMain.resultString.day = timeToReturn
        modelMain.resultString.time = timeToFind.time

        return modelMain
    }

}