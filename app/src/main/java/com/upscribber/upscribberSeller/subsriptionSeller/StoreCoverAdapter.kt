package com.upscribber.upscribberSeller.subsriptionSeller

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.upscribber.R

import com.upscribber.upscribberSeller.pictures.coverImages.ModelCoverImages
import kotlinx.android.synthetic.main.cover_images_layout_store.view.*


class StoreCoverAdapter(
    var context: Context
) :
    RecyclerView.Adapter<StoreCoverAdapter.ViewHolder>() {

    private var itemss: ArrayList<ModelCoverImages> = ArrayList()
    var listen = context as ImageClick


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(context).inflate(R.layout.cover_images_layout_store, parent, false)
        return ViewHolder(view)

    }

    override fun getItemCount(): Int {
        return itemss.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = itemss[position]
        if (model.name == "") {
            if (position == 0 || position == 1 || position == 2) {
//                if (model.getImage == "disable") {
//                    holder.itemView.imgCross.visibility = View.GONE
//                    holder.itemView.circleImageView.visibility = View.VISIBLE
//                    holder.itemView.circleImageView.isClickable = false
//                    holder.itemView.layoutClick.isClickable = false
//                    holder.itemView.layoutClick.isEnabled = false
//                    holder.itemView.circleImageView.alpha = 0.4F
//                    holder.itemView.imageCamera.visibility = View.VISIBLE
//                    holder.itemView.imageCamera.isClickable = false
//                    holder.itemView.imageCamera.alpha = 0.4F
//                    holder.itemView.imageView.visibility = View.GONE
//                } else {
                holder.itemView.circleImageView.visibility = View.VISIBLE
                holder.itemView.imageCamera.visibility = View.VISIBLE
                holder.itemView.imageView.visibility = View.GONE
//                }
                holder.itemView.imgCross.visibility = View.GONE
                holder.itemView.circleImageView.visibility = View.VISIBLE
                holder.itemView.imageCamera.visibility = View.VISIBLE
                holder.itemView.imageView.visibility = View.GONE
            } else {
                holder.itemView.circleImageView.visibility = View.GONE
                holder.itemView.imageCamera.visibility = View.GONE
                holder.itemView.imageView.visibility = View.VISIBLE
                Glide.with(context).load(model.getImage)
                    .placeholder(R.mipmap.placeholder_subscription_square)
                    .into(holder.itemView.imageView)

            }
            holder.itemView.layoutClick.setOnClickListener {

                if (position == 0 || position == 1 || position == 2) {
                    listen.click()
                }

            }
        } else {
            if (position == 0) {
//                if (model.getImage == "disable") {
//                    holder.itemView.circleImageView.visibility = View.VISIBLE
//                    holder.itemView.circleImageView.isClickable = false
//                    holder.itemView.layoutClick.isClickable = false
//                    holder.itemView.circleImageView.alpha = 0.4F
//                    holder.itemView.imageCamera.visibility = View.VISIBLE
//                    holder.itemView.imageCamera.isClickable = false
//                    holder.itemView.imageCamera.alpha = 0.4F
//                    holder.itemView.imageView.visibility = View.GONE
//                } else {
                holder.itemView.circleImageView.visibility = View.VISIBLE
                holder.itemView.imageCamera.visibility = View.VISIBLE
                holder.itemView.imageView.visibility = View.GONE
//                }
                holder.itemView.circleImageView.visibility = View.VISIBLE
                holder.itemView.imageCamera.visibility = View.VISIBLE
                holder.itemView.imageView.visibility = View.GONE
            } else {
//                if (model.updated_at == "disable") {
//                    holder.itemView.imgCross.isClickable = false
//                    holder.itemView.imgCross.isEnabled = false
//                    holder.itemView.imgCross.alpha = 0.4F
//                }
                holder.itemView.circleImageView.visibility = View.GONE
                holder.itemView.imgCross.visibility = View.VISIBLE
                holder.itemView.imageCamera.visibility = View.GONE
                holder.itemView.imageView.visibility = View.VISIBLE
                Glide.with(context).load(model.getImage)
                    .placeholder(R.mipmap.placeholder_subscription_square)
                    .into(holder.itemView.imageView)

            }
            holder.itemView.layoutClick.setOnClickListener {
                if (position == 0) {
                    if (model.getImage != "disable") {
                        listen.click()
                    }

                }

            }

            holder.itemView.imgCross.setOnClickListener {
                listen.removeImage(position, itemss)
            }
        }


    }

    fun update(items: List<ModelCoverImages>) {
        itemss.clear()
        itemss.addAll(items)
        notifyDataSetChanged()

    }


    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)

    interface ImageClick {
        fun click()
        fun removeImage(
            position: Int,
            itemss: ArrayList<ModelCoverImages>
        )
    }
}