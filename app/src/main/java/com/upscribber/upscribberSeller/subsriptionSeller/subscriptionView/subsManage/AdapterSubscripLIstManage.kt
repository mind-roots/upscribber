package com.upscribber.upscribberSeller.subsriptionSeller.subscriptionView.subsManage

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.R
import com.upscribber.databinding.CustomerRecyclerManageListBinding

class AdapterSubscripLIstManage(var context: Context) : RecyclerView.Adapter<AdapterSubscripLIstManage.ViewHolder>() {

    private var itemss: List<ModelSubscriptionListManage> = emptyList()
    lateinit var mBinding : CustomerRecyclerManageListBinding


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.customer_recycler_manage_list, parent,false)
        return ViewHolder(mBinding.root)

    }

    override fun getItemCount(): Int {
        return  itemss.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = itemss[position]
        mBinding.list = model

    }

    fun update(items: List<ModelSubscriptionListManage>) {
        this.itemss = items
        notifyDataSetChanged()
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)
}