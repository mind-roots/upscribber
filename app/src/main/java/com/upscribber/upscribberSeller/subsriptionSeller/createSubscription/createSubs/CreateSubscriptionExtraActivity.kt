package com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.createSubs

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Outline
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.*
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.upscribber.commonClasses.Constant
import com.upscribber.R
import com.upscribber.commonClasses.RoundedBottomSheetDialogFragment
import com.upscribber.databinding.ActivityCreateSubscriptionExtraBinding
import com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.subscription.AdapterSubscriptionCard
import com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.subscription.ModelSubscriptionCard
import com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.subscription.SubscriptionPriceActivity
import com.upscribber.upscribberSeller.subsriptionSeller.subscriptionMain.ModelMain
import com.upscribber.upscribberSeller.subsriptionSeller.subscriptionMain.SubscriptionSellerViewModel
import org.json.JSONArray
import org.json.JSONObject
import java.lang.Exception

@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class CreateSubscriptionExtraActivity : AppCompatActivity(), AdapterSubscriptionCard.deleteCard {

    private var adapterPostion: Int = 0
    private var adapterPostionDelete: Int = 0
    private lateinit var mAdapterSubsCard: AdapterSubscriptionCard
    private lateinit var mBinding: ActivityCreateSubscriptionExtraBinding
    private lateinit var viewModel: CreateSubscriptionViewModel
    private lateinit var mvModel: SubscriptionSellerViewModel
    private var showData = ArrayList<ModelSubscriptionCard>()
    var dataStore = ArrayList<ModelSubscriptionCard>()
    lateinit var progessBarView: ConstraintLayout


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_create_subscription_extra)
        viewModel = ViewModelProviders.of(this)[CreateSubscriptionViewModel::class.java]
        mvModel = ViewModelProviders.of(this)[SubscriptionSellerViewModel::class.java]
        progessBarView = findViewById(R.id.progressBar)
        setToolbar()
        clickListeners()

        checkIfBought()
    }


    private fun setToolbar() {
        setSupportActionBar(mBinding.toolbar)
        title = ""
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)
    }

    override fun onResume() {
        super.onResume()
        setAdapter()
    }

    @SuppressLint("ResourceAsColor")
    private fun checkIfBought() {
        val mPrefs = application.getSharedPreferences("data", MODE_PRIVATE)
        var boughtIsMore = mPrefs.getString("bought", "0")
        if (boughtIsMore.isEmpty()) {
            boughtIsMore = "0"
        }
        if (boughtIsMore.toInt() > 0) {
            //Disable All Fields Except Description
            mBinding.addProduct.isClickable = false
            mBinding.addProduct.alpha = 0.4F
        }


    }

    private fun setAdapter() {

        if (intent.hasExtra("data")) {
            mBinding.dummyText.visibility = View.GONE
            mBinding.card.visibility = View.GONE
            mBinding.addSubPrice.visibility = View.VISIBLE
            mBinding.recyclerSubPrice.visibility = View.VISIBLE
            mBinding.nestedScrollView5.visibility = View.VISIBLE
            mBinding.create.visibility = View.VISIBLE
            mBinding.textView28.visibility = View.VISIBLE
            mBinding.addProduct.visibility = View.GONE
            mBinding.recyclerSubPrice.layoutManager = LinearLayoutManager(this)
            mAdapterSubsCard = AdapterSubscriptionCard(this)
            mBinding.recyclerSubPrice.adapter = mAdapterSubsCard
            val getData = intent.getParcelableExtra<ModelMain>("data")
            val data = ArrayList<ModelSubscriptionCard>()

            if (getData.subscription.size > 0) {
                for (i in 0 until getData.subscription.size) {
                    val subData = getData.subscription[i]
                    val model = ModelSubscriptionCard()
                    model.introDays = subData.introDays
                    model.introPrice = subData.introPrice
                    model.freeTrail = subData.freeTrail
                    model.frequency = subData.frequencyType + " " + subData.freeTrailValue
                    model.redeemtion_cycle_qty = subData.redeemtion_cycle_qty
                    model.frequencyType = subData.frequencyType
                    model.discountPrice = subData.discountPrice
                    model.bought = subData.bought
                    model.price = subData.price
                    model.pricingNumber = subData.redeemtion_cycle_qty
                    data.add(model)
                    dataStore.clear()
                    dataStore.addAll(data)
                    mAdapterSubsCard.update(dataStore)
                }
            } else {

                val myPrefs = getSharedPreferences("data", Context.MODE_PRIVATE)
                val mVariable = Gson()
                val getPricing = myPrefs.getString("pricing", "")
                if (getPricing.isEmpty()) {
                    if (dataStore.isNotEmpty()) {
                        mAdapterSubsCard.update(dataStore)
                    } else {
                        mBinding.dummyText.visibility = View.GONE
                        mBinding.nestedScrollView5.visibility = View.GONE
                        mBinding.card.visibility = View.VISIBLE
                        mBinding.addSubPrice.visibility = View.VISIBLE
                        mBinding.create.visibility = View.GONE
                        mBinding.textView28.visibility = View.GONE
                        mBinding.addProduct.visibility = View.GONE
                        mBinding.recyclerSubPrice.visibility = View.GONE
                    }

                    return
                } else {
                    mBinding.dummyText.visibility = View.GONE
                    mBinding.card.visibility = View.GONE
                    mBinding.addSubPrice.visibility = View.VISIBLE
                    mBinding.recyclerSubPrice.visibility = View.VISIBLE
                    mBinding.nestedScrollView5.visibility = View.VISIBLE
                    mBinding.create.visibility = View.VISIBLE
                    mBinding.textView28.visibility = View.VISIBLE
                    mBinding.addProduct.visibility = View.GONE
                    mBinding.recyclerSubPrice.layoutManager = LinearLayoutManager(this)
                    mAdapterSubsCard = AdapterSubscriptionCard(this)
                    mBinding.recyclerSubPrice.adapter = mAdapterSubsCard
                    dataStore.clear()
                    val turnsType = object : TypeToken<ArrayList<ModelSubscriptionCard>>() {}.type
                    dataStore = mVariable.fromJson(getPricing, turnsType)

                    mAdapterSubsCard.update(dataStore)

                }
            }

        } else {

            if (showData.isNotEmpty()) {
                mAdapterSubsCard.update(showData)
            } else if (dataStore.isNotEmpty()) {
                mBinding.dummyText.visibility = View.GONE
                mBinding.card.visibility = View.GONE
                mBinding.addSubPrice.visibility = View.VISIBLE
                mBinding.recyclerSubPrice.visibility = View.VISIBLE
                mBinding.nestedScrollView5.visibility = View.VISIBLE
                mBinding.create.visibility = View.VISIBLE
                mBinding.textView28.visibility = View.VISIBLE
                mBinding.addProduct.visibility = View.GONE
                mBinding.recyclerSubPrice.layoutManager = LinearLayoutManager(this)
                mAdapterSubsCard = AdapterSubscriptionCard(this)
                mBinding.recyclerSubPrice.adapter = mAdapterSubsCard
                mAdapterSubsCard.update(dataStore)
            } else {

                val myPrefs = getSharedPreferences("data", Context.MODE_PRIVATE)
                val mVariable = Gson()
                val getPricing = myPrefs.getString("pricing", "")
                if (getPricing == "[]" || getPricing.isEmpty()) {
                    mBinding.dummyText.visibility = View.GONE
                    mBinding.nestedScrollView5.visibility = View.GONE
                    mBinding.card.visibility = View.VISIBLE
                    mBinding.addSubPrice.visibility = View.VISIBLE
                    mBinding.create.visibility = View.GONE
                    mBinding.textView28.visibility = View.GONE
                    mBinding.addProduct.visibility = View.GONE
                    mBinding.recyclerSubPrice.visibility = View.GONE
                    return
                } else {
                    mBinding.dummyText.visibility = View.GONE
                    mBinding.card.visibility = View.GONE
                    mBinding.addSubPrice.visibility = View.VISIBLE
                    mBinding.recyclerSubPrice.visibility = View.VISIBLE
                    mBinding.nestedScrollView5.visibility = View.VISIBLE
                    mBinding.create.visibility = View.VISIBLE
                    mBinding.textView28.visibility = View.VISIBLE
                    mBinding.addProduct.visibility = View.GONE
                    mBinding.recyclerSubPrice.layoutManager = LinearLayoutManager(this)
                    mAdapterSubsCard = AdapterSubscriptionCard(this)
                    mBinding.recyclerSubPrice.adapter = mAdapterSubsCard
                    dataStore.clear()
                    val turnsType = object : TypeToken<ArrayList<ModelSubscriptionCard>>() {}.type
                    dataStore = mVariable.fromJson(getPricing, turnsType)
                    mAdapterSubsCard.update(dataStore)

                }
            }

        }

    }

    private fun clickListeners() {
        mBinding.addSubPrice.setOnClickListener {
            var priceToDisplay = 0.0
            var discountToDisplay = 0
            try {
                val mVariable = Gson()
                var dataStoredBasic = BasicModel()

                val mPrefsBasic = application.getSharedPreferences("data", MODE_PRIVATE)
                val jsonBasicData = mPrefsBasic.getString("basicData", "")
                if (jsonBasicData != "") {
                    dataStoredBasic = mVariable.fromJson(jsonBasicData, BasicModel::class.java)
                    if (dataStoredBasic.productInfo.size > 0) {
                        for (items in dataStoredBasic.productInfo) {
                            if (dataStoredBasic.productInfo.size == 1) {
                                priceToDisplay = items.price.toDouble()
                                discountToDisplay = items.discountPrice.toInt()
                            }
                        }
//                        mBinding.eTPrice1.setText(priceToDisplay.toString())
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
            if (priceToDisplay != 0.0) {
                if (discountToDisplay != 0) {
                    startActivityForResult(
                        Intent(this, SubscriptionPriceActivity::class.java).putExtra(
                            "subArray",
                            dataStore
                        ).putExtra("priceToDisplay", priceToDisplay.toString()).putExtra(
                            "new",
                            "new"
                        ).putExtra("discountToDisplay", discountToDisplay.toString()),
                        101
                    )
                } else {
                    startActivityForResult(
                        Intent(this, SubscriptionPriceActivity::class.java).putExtra(
                            "subArray",
                            dataStore
                        ).putExtra("priceToDisplay", priceToDisplay.toString()).putExtra(
                            "new",
                            "new"
                        ),
                        101
                    )
                }
            } else {
                startActivityForResult(
                    Intent(this, SubscriptionPriceActivity::class.java).putExtra(
                        "subArray",
                        dataStore
                    ).putExtra("new", "new"),
                    101
                )
            }

        }

        mBinding.addProduct.setOnClickListener {
            var priceToDisplay = 0.0
            try {
                val mVariable = Gson()
                var dataStoredBasic = BasicModel()

                val mPrefsBasic = application.getSharedPreferences("data", MODE_PRIVATE)
                val jsonBasicData = mPrefsBasic.getString("basicData", "")
                if (jsonBasicData != "") {
                    dataStoredBasic = mVariable.fromJson(jsonBasicData, BasicModel::class.java)
                    if (dataStoredBasic.productInfo.size > 0) {
                        for (items in dataStoredBasic.productInfo) {
                            priceToDisplay += items.price.toDouble()
                        }
//                        mBinding.eTPrice1.setText(priceToDisplay.toString())
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
            if (priceToDisplay != 0.0) {
                startActivityForResult(
                    Intent(this, SubscriptionPriceActivity::class.java).putExtra(
                        "subArray",
                        dataStore
                    ).putExtra("priceToDisplay", priceToDisplay.toString()),
                    101
                )
            } else {
                startActivityForResult(
                    Intent(this, SubscriptionPriceActivity::class.java).putExtra(
                        "subArray",
                        dataStore
                    ),
                    101
                )
            }
        }



        mBinding.create.setOnClickListener {
            saveData()
        }



        viewModel.getStatus().observe(this, Observer {
            progessBarView.visibility = View.GONE
            if (it.msg.isNotEmpty()) {
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
            }
        })

        viewModel.mSubsSuccessful().observe(this, Observer {
            progessBarView.visibility = View.GONE
            var steps = ""
            for (i in 0 until it.size) {
                steps = it[i].steps
            }
            val editor1 = Constant.getSharedPrefs(this).edit()
            val gson1 = Gson()
            val json1 = gson1.toJson(it)
            editor1.putString("pricing", json1)
            editor1.apply()

            val intent = Intent().putExtra("position", intent.getIntExtra("position", -1))
                .putExtra("steps", steps)
            setResult(99, intent)
            finish()
            Toast.makeText(this, "Your changes have been saved successfully", Toast.LENGTH_LONG)
                .show()
        })

    }


    fun saveData() {
        val jsonArray = JSONArray()
        val myPrefs = getSharedPreferences("data", Context.MODE_PRIVATE)
        val editor1 = myPrefs.edit()
        val gson1 = Gson()
        val json1 = gson1.toJson(dataStore)
        editor1.putString("pricing", json1)
        editor1.apply()
//        mAdapterSubsCard.update(dataStore)


        if (dataStore.isNotEmpty()) {
            for (i in 0 until dataStore.size) {
                val jsonObject = JSONObject()
                jsonObject.put("id", dataStore[i].id)
                if (dataStore[i].redeemtion_cycle_qty == "Unlimited") {
                    jsonObject.put("redeemtion_cycle_qty", "500000")
                } else {
                    jsonObject.put("redeemtion_cycle_qty", dataStore[i].redeemtion_cycle_qty)
                }
                jsonObject.put("unit", dataStore[i].quantityName)
                if ("Month" in dataStore[i].frequencyType || dataStore[i].frequencyType == "1") {
                    jsonObject.put("frequency_type", "1")
                } else {
                    jsonObject.put("frequency_type", "2")
                }

                jsonObject.put("free_trial_qty", dataStore[i].free_trail_qty)
                jsonObject.put("frequency_value", dataStore[i].frequency)
                jsonObject.put("price", dataStore[i].price)
                jsonObject.put("discount_price", dataStore[i].discountPrice)
                jsonObject.put("introductory_price", dataStore[i].introPrice)
                jsonObject.put("introductory_days", dataStore[i].introDays)
                if (dataStore[i].freeTrail == "No free trial") {
                    jsonObject.put("free_trial", "0")
                } else {
                    jsonObject.put("free_trial", dataStore[i].freeTrail)
                }
                jsonArray.put(jsonObject)
            }
            progessBarView.visibility = View.VISIBLE
            viewModel.getSubscriptionCardList(jsonArray)
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        try {
            if (requestCode == 101 && data != null) {
                val list = data.getParcelableArrayListExtra<ModelSubscriptionCard>("data")
//                val mVariable = Gson()
//                val turnsType = object : TypeToken<ArrayList<ModelSubscriptionCard>>() {}.type
//                val myPrefs = getSharedPreferences("data", Context.MODE_PRIVATE)
//                val getPricing = myPrefs.getString("pricing", "")
//                if (getPricing.isNotEmpty()) {
//                    dataStore = mVariable.fromJson(getPricing, turnsType)
//                }
                dataStore.add(list[0])
//                val editor1 = Constant.getSharedPrefs(this).edit()

//                val json1 = mVariable.toJson(dataStore)
//                editor1.putString("pricing", json1)
//                editor1.apply()
                //dataStore=list
                //showData = list
                setAdapter()
            }
            if (requestCode == 201 && data != null) {
                val list = data.getParcelableArrayListExtra<ModelSubscriptionCard>("data")
//                val mVariable = Gson()
//                val turnsType = object : TypeToken<ArrayList<ModelSubscriptionCard>>() {}.type
//                val myPrefs = getSharedPreferences("data", Context.MODE_PRIVATE)
//                val getPricing = myPrefs.getString("pricing", "")
//                if (getPricing.isNotEmpty()) {
//                    dataStore = mVariable.fromJson(getPricing, turnsType)
//                }
                dataStore.set(adapterPostion, list[0])
//                val editor1 = Constant.getSharedPrefs(this).edit()
//
//                val json1 = mVariable.toJson(dataStore)
//                editor1.putString("pricing", json1)
//                editor1.apply()
                //dataStore=list
                //showData = list
                setAdapter()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            if (intent.hasExtra("showDialog") && dataStore.size > 0) {
                val fragment = MenuFragmentDelete(this)
                fragment.show(supportFragmentManager, fragment.tag)
            } else {
                finish()
            }

        }
        return super.onOptionsItemSelected(item)
    }


    class MenuFragmentDelete(val mContext: Activity) : RoundedBottomSheetDialogFragment() {

        lateinit var yes: TextView
        lateinit var no: TextView
        lateinit var textHeading: TextView
        lateinit var textDescription: TextView

        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val view = inflater.inflate(R.layout.delete_product_sheet, container, false)

            yes = view.findViewById(R.id.yes)
            no = view.findViewById(R.id.no)
            textHeading = view.findViewById(R.id.textHeading)
            textDescription = view.findViewById(R.id.textDescription)
            val imageViewTop = view.findViewById<ImageView>(R.id.imageViewTop)
            roundImageView(imageViewTop)
            textHeading.text = "Saving"
            textDescription.text = "Do you want to save?"

            no.setOnClickListener {
                dismiss()
                mContext.finish()
            }

            yes.setOnClickListener {
                dismiss()
                (mContext as CreateSubscriptionExtraActivity).saveData()
            }

            return view
        }

        private fun roundImageView(imageViewTop: ImageView) {
            val curveRadius = 50F

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                imageViewTop.outlineProvider = object : ViewOutlineProvider() {

                    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                    override fun getOutline(view: View?, outline: Outline?) {
                        outline?.setRoundRect(
                            0,
                            0,
                            view!!.width,
                            view.height + curveRadius.toInt(),
                            curveRadius
                        )
                    }
                }
                imageViewTop.clipToOutline = true
            }
        }


    }

    override fun delete(
        position: Int,
        model: ModelSubscriptionCard
    ) {
        try {
            adapterPostionDelete = position
            val myPrefs = getSharedPreferences("data", Context.MODE_PRIVATE)
            val mVariable = Gson()
            val getPricing = myPrefs.getString("pricing", "")
            if (getPricing.isNotEmpty()) {
                val turnsType = object : TypeToken<ArrayList<ModelSubscriptionCard>>() {}.type
                val backup = dataStore
                dataStore = mVariable.fromJson(getPricing, turnsType)
                var exist = 0
                for (items in dataStore) {
                    if (items.id == model.id) {
                        exist = 1
                    }
                }
                if (exist == 1) {
                    mvModel.deleteCampaign("2", "", dataStore[position].id)
                    dataStore.removeAt(position)
                    val editor1 = Constant.getSharedPrefs(this).edit()
                    val json1 = mVariable.toJson(dataStore)
                    editor1.putString("pricing", json1)
                    editor1.apply()
                } else {

                    backup.removeAt(position)
                    dataStore.clear()
                    dataStore.addAll(backup)
                }


//                if (dataStore.size == 0) {
                setAdapter()
//                }
            } else {
                if (dataStore.size > 0) {
                    dataStore.removeAt(position)
                    setAdapter()
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }


    }

    override fun edit(position: Int, model: ModelSubscriptionCard) {
        adapterPostion = position
        startActivityForResult(
            Intent(this, SubscriptionPriceActivity::class.java).putExtra(
                "model",
                model
            ).putExtra("subArray", dataStore), 201
        )

    }
}
