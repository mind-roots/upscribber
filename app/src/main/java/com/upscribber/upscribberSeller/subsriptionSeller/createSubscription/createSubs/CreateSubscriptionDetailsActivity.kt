package com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.createSubs

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.graphics.Outline
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.*
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.chip.Chip
import com.google.gson.Gson
import com.upscribber.commonClasses.Constant
import com.upscribber.R
import com.upscribber.commonClasses.RoundedBottomSheetDialogFragment
import com.upscribber.databinding.ActivityCreateSubscriptionDetailsBinding
import com.upscribber.filters.CategoryModel
import com.upscribber.filters.ModelSubCategory
import com.upscribber.upscribberSeller.manageTeam.ManageTeamActivity
import com.upscribber.upscribberSeller.manageTeam.ManageTeamModel
import com.upscribber.upscribberSeller.store.storeInfo.CategoryProductsFragment
import com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.LocationSetAdapter
import com.upscribber.upscribberSeller.subsriptionSeller.location.searchLoc.ModelSearchLocation
import com.upscribber.upscribberSeller.subsriptionSeller.location.searchLoc.SelectLocationActivity
import com.upscribber.upscribberSeller.subsriptionSeller.subscriptionMain.SubscriptionSellerViewModel
import kotlinx.android.synthetic.main.activity_create_subscription_details.*
import kotlinx.android.synthetic.main.activity_discover.drawerLayout1

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS", "DEPRECATION")
class CreateSubscriptionDetailsActivity : AppCompatActivity(), DrawerLayout.DrawerListener,
    LocationSetAdapter.Delete {

    private lateinit var locationSetAdapter: LocationSetAdapter
    private lateinit var teamSetAdapter: ManageTeamAdapter1
    private lateinit var binding: ActivityCreateSubscriptionDetailsBinding
    private lateinit var viewModel: CreateSubscriptionViewModel
    private lateinit var mViewModel: SubscriptionSellerViewModel
    var drawerOpen = 0
    var subLimit = "0"
    var shareable = "0"
    var check = "0"
    private var maxSub = "0"
    private lateinit var fragmentManager: FragmentManager
    private lateinit var fragmentTransaction: FragmentTransaction
    private lateinit var mfilters: TextView
    private lateinit var filterImgBack: ImageView
    private lateinit var filterDone: TextView
    private var arrayListOfLocation = ArrayList<ModelSearchLocation>()
    private var arrayListOfTeam = ArrayList<ManageTeamModel>()
    private var fragmentMain = CategoryProductsFragment()
    var arrayCategoriesMain = ArrayList<CategoryModel>()
    var arrayTags = ArrayList<ModelSubCategory>()
    lateinit var progessBarView: ConstraintLayout
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding =
            DataBindingUtil.setContentView(this, R.layout.activity_create_subscription_details)
        viewModel = ViewModelProviders.of(this)[CreateSubscriptionViewModel::class.java]
        mViewModel = ViewModelProviders.of(this)[SubscriptionSellerViewModel::class.java]
        drawerLayout1.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, GravityCompat.END)
        inIt()
        setToolbar()
        setAdapters()
        clickListeners()
        apiImplementation()
        observerInit()
        checkIfBought()
    }

    @SuppressLint("ResourceAsColor")
    private fun checkIfBought() {
        val mPrefs = application.getSharedPreferences("data", MODE_PRIVATE)
        var boughtIsMore = mPrefs.getString("bought", "0")
        if (boughtIsMore.isEmpty()) {
            boughtIsMore = "0"
        }
        if (boughtIsMore.toInt() > 0) {
            //Disable All Fields Except Description
//            binding.switch6.isClickable = false
//            binding.switch8.isClickable = false
//            binding.etMax.isClickable = false
//            binding.etLocation.isClickable = false
//            binding.addTeamMember.isClickable = false
            binding.addTags.isClickable = false
            binding.addTags.alpha = 0.4F
//            binding.etMax.alpha = 0.4F
//            binding.etLocation.alpha = 0.4F
//            binding.addTeamMember.alpha = 0.4F
//            binding.switch6.alpha = 0.4F
//            binding.switch8.alpha = 0.4F

        }


    }

    private fun observerInit() {
        viewModel.getStatus().observe(this, Observer {
            if (it.msg.isNotEmpty()) {
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
            }
        })

        viewModel.getProductCategoryAll().observe(this, Observer {
            if (it.size > 0) {
                val bundle = Bundle()
                bundle.putParcelableArrayList("tags", it)
                fragmentMain = CategoryProductsFragment()
                inflateFragment(fragmentMain)
                fragmentMain.arguments = bundle
                arrayCategoriesMain = it
                checkPreValidData()
            }
        })


    }

    private fun apiImplementation() {
        val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
        viewModel.getproductCategory(auth)
        progessBarView.visibility = View.VISIBLE
    }

    private fun inIt() {
        mfilters = findViewById(R.id.filters)
        filterImgBack = findViewById(R.id.filterImgBack)
        filterDone = findViewById(R.id.filter_done)
        progessBarView = findViewById(R.id.progressBar)
    }


    override fun onBackPressed() {
        if (drawerOpen == 1) {
            drawerOpen = 0
            drawerLayout1.closeDrawer(GravityCompat.END)
        } else {
            super.onBackPressed()
        }
    }

    @SuppressLint("ClickableViewAccessibility", "SetTextI18n")
    private fun clickListeners() {

        binding.etLocation.setOnClickListener {
            startActivityForResult(
                Intent(
                    this,
                    SelectLocationActivity::class.java
                ).putExtra("selectedArray", arrayListOfLocation), 77
            )
        }

        binding.addTags.setOnClickListener {
            drawerOpen = 1
            drawerLayout1.openDrawer(GravityCompat.END)
            val bundle = Bundle()
            bundle.putParcelableArrayList("tags", arrayCategoriesMain)
            fragmentMain.arguments = bundle
            inflateFragment(fragmentMain)
            mfilters.text = "Category"
        }

        filterImgBack.setOnClickListener {
            onBackPressed()
        }

        filterDone.setOnClickListener {
            onDonePressed()
        }

        binding.create.setOnClickListener {
            saveData()
        }


        viewModel.getDetails().observe(this, Observer {
            progessBarView.visibility = View.GONE
            val intent = Intent().putExtra("position", intent.getIntExtra("position", -1))
                .putExtra("steps", it.steps)
            setResult(99, intent)
            finish()
            Toast.makeText(this, "Your changes have been saved successfully", Toast.LENGTH_LONG)
                .show()

        })




        binding.switch8.setOnCheckedChangeListener { p0, p1 ->
            check = if (p1) {
                "1"
            } else {
                "0"
            }

        }

        binding.switch6.setOnCheckedChangeListener { p0, p1 ->

            if (p1) {
                binding.etMax.isEnabled = true
                subLimit = "1"


            } else {
                subLimit = "0"
                binding.etMax.isEnabled = false
            }

        }


        binding.switch7.setOnCheckedChangeListener { p0, p1 ->
            shareable = if (p1) {
                "1"
            } else {
                "0"

            }

        }

        binding.addTeamMember.setOnClickListener {
            startActivityForResult(
                Intent(this, ManageTeamActivity::class.java).putExtra(
                    "chooseTeam",
                    "others1"
                ).putExtra("selectedArray", arrayListOfTeam), 71
            )
        }

        binding.imageView31.setOnClickListener {
            Constant.CommonIAlert(
                this,
                "Subscription description",
                "Add detail information about your subscription to inform customers what they will get when they purchase your subscription."
            )
        }

        binding.imageView34.setOnClickListener {
            Constant.CommonIAlert(
                this,
                "Limit Subscribers",
                "Specify how many customers can subscribe to this subscription. You may want to do this if you are running a limited promotion, giving away a subscription or testing a new product or product bundle."
            )
        }

        binding.imageView35.setOnClickListener {
            Constant.CommonIAlert(
                this,
                resources.getString(R.string.location),
                "Select location for this subscription"
            )
        }

        binding.imageView335.setOnClickListener {
            Constant.CommonIAlert(
                this,
                resources.getString(R.string.team_member),
                "You can assign a team member (or members) to a subscription. This is a great option if you have a contractor who specializes in a particular service. It’s also a creative way to give your clients access to a specific staff member they have a relationship with. Team members can also invite their clients to this subscription."
            )
        }

        binding.imageView57.setOnClickListener {
            Constant.CommonIAlert(
                this,
                "Add Tags",
                "Tags are keywords your customers may use to find a specific subscription in the marketplace. Customers search and filter by tags, so be sure to add all that apply."
            )
        }


        binding.imagePrivacy.setOnClickListener {
            Constant.CommonIAlert(
                this,
                "Subscription Privacy",
                "You can set your subscriptions to be public or private. Public subscriptions will be visible to all platform users while Private ones will only be visible to the ones whom you personally invite."
            )
        }

        binding.imageCustomers.setOnClickListener {
            Constant.CommonIAlert(
                this,
                "Customers can share",
                "Recommended! Customers can share unused subscription redemptions with friends and family. This is a great way to get introduced to new customers!"
            )
        }




        binding.linear.setOnClickListener {
            val intent = Intent(this, EditTextActivity::class.java)
            val preFilled = binding.editTextSubsDetails.text.toString()
            intent.putExtra("preFilled", preFilled)
            startActivityForResult(intent, 1)
        }
        binding.editTextSubsDetails.setOnClickListener {
            val intent = Intent(this, EditTextActivity::class.java)
            val preFilled = binding.editTextSubsDetails.text.toString()
            intent.putExtra("preFilled", preFilled)
            startActivityForResult(intent, 1)
        }


    }

    override fun deleteAddedProduct(position: Int) {
        arrayListOfLocation.removeAt(position)
        locationSetAdapter.notifyDataSetChanged()

    }

    fun saveData() {
        val listLocation = ArrayList<String>()
        val listTeam = ArrayList<String>()
        val listTags = ArrayList<String>()
        var mLocationData = ""
        var mTeamData = ""
        var mTagData = ""
        val subInfo = binding.editTextSubsDetails.text.toString()
        binding.textView257.text

        if (arrayListOfLocation.isNotEmpty()) {

            for (i in 0 until arrayListOfLocation.size) {
                listLocation.add(arrayListOfLocation[i].id)
            }
            mLocationData = TextUtils.join(",", listLocation)
        }

        if (arrayListOfTeam.isNotEmpty()) {
            for (j in 0 until arrayListOfTeam.size) {
                listTeam.add(arrayListOfTeam[j].staff_id)
            }
            mTeamData = TextUtils.join(",", listTeam)
        }

        if (arrayTags.isNotEmpty()) {
            for (k in 0 until arrayTags.size) {
                listTags.add(arrayTags[k].id)
            }
            mTagData = TextUtils.join(",", listTags)
        }

        if (subLimit == "1") {
            maxSub = etMax.text.toString()
        } else {
            maxSub = "1000"
        }

        Log.e("mLocationData", mLocationData)
        Log.e("mTeamData", mTeamData)
        Log.e("mTagData", mTagData)
        Log.e("subLimit", subLimit)
        Log.e("shareable", shareable)
        Log.e("check", check)
        progessBarView.visibility = View.VISIBLE
        viewModel.getDetails(
            subInfo,
            subLimit,
            mTeamData,
            mTagData,
            shareable,
            check,
            mLocationData,
            maxSub
        )


    }

    private fun getTrailList(): ArrayList<String> {

        val arrayList: ArrayList<String> = ArrayList()
        var k = 50

        for (i in 0 until 3) {

            when (i) {
                0 -> arrayList.add("10")
                1 -> arrayList.add("50")
                else -> for (j in 0 until 19) {
                    k += 50
                    arrayList.add(k.toString())
                }

            }
        }

        return arrayList


    }

    private fun onDonePressed() {
//        arrayTags = ArrayList()
//        binding.rvTags.removeAllViews()
        chipData(arrayCategoriesMain)
        onBackPressed()
    }


    private fun setAdapters() {
        binding.locationRecycler.layoutManager = LinearLayoutManager(this)
        binding.locationRecycler.isNestedScrollingEnabled = false
        locationSetAdapter = LocationSetAdapter(this)
        binding.locationRecycler.adapter = locationSetAdapter


        binding.recyclerProducts.layoutManager = LinearLayoutManager(this)
        binding.recyclerProducts.isNestedScrollingEnabled = false
        teamSetAdapter = ManageTeamAdapter1(this)
        binding.recyclerProducts.adapter = teamSetAdapter

    }

    private fun setToolbar() {
        setSupportActionBar(binding.toolbar.toolbar)
        title = ""
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)

    }

    private fun chipData(it: ArrayList<CategoryModel>) {

        binding.rvTags.removeAllViews()
        arrayTags.clear()
        for (category in it) {
            var categoryName = ""
            for (subCat in category.arrayList)
                if (subCat.check) {
                    categoryName = category.categoryName
                    arrayTags.add(subCat)
                }
            if (categoryName != "") {
                val chip = Chip(binding.rvTags.context)
                chip.text = categoryName
                chip.isClickable = true
                chip.isCheckable = false
                chip.setTextAppearanceResource(R.style.ChipTextStyle_Selected)
                chip.setChipBackgroundColorResource(R.color.home_free)
                chip.isCheckedIconEnabled = true
                chip.setOnCloseIconClickListener {
                    binding.rvTags.removeView(chip)
                }
                chip.chipEndPadding = 20.0f
                chip.gravity = Gravity.CENTER

                binding.rvTags.addView(chip)
            }
        }
        binding.rvTags.isSingleSelection = true

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {

            if (intent.hasExtra("showDialog")) {
                val fragment = MenuFragmentDelete(this)
                fragment.show(supportFragmentManager, fragment.tag)
            } else {
                finish()
            }

        }
        return super.onOptionsItemSelected(item)
    }


    class MenuFragmentDelete(val mContext: Activity) :
        RoundedBottomSheetDialogFragment() {

        lateinit var yes: TextView
        lateinit var no: TextView
        lateinit var textHeading: TextView
        lateinit var textDescription: TextView

        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val view = inflater.inflate(R.layout.delete_product_sheet, container, false)

            yes = view.findViewById(R.id.yes)
            no = view.findViewById(R.id.no)
            textHeading = view.findViewById(R.id.textHeading)
            textDescription = view.findViewById(R.id.textDescription)
            val imageViewTop = view.findViewById<ImageView>(R.id.imageViewTop)
            roundImageView(imageViewTop)
            textHeading.text = "Saving"
            textDescription.text = "Do you want to save?"

            no.setOnClickListener {
                dismiss()
                mContext.finish()
            }

            yes.setOnClickListener {
                (mContext as CreateSubscriptionDetailsActivity).saveData()
                dismiss()

            }

            return view
        }

        private fun roundImageView(imageViewTop: ImageView) {
            val curveRadius = 50F

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                imageViewTop.outlineProvider = object : ViewOutlineProvider() {

                    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                    override fun getOutline(view: View?, outline: Outline?) {
                        outline?.setRoundRect(
                            0,
                            0,
                            view!!.width,
                            view.height + curveRadius.toInt(),
                            curveRadius
                        )
                    }
                }
                imageViewTop.clipToOutline = true
            }
        }


    }


    override fun onDrawerStateChanged(newState: Int) {


    }

    override fun onDrawerSlide(drawerView: View, slideOffset: Float) {

    }

    override fun onDrawerClosed(drawerView: View) {
        drawerOpen = 0
    }

    @SuppressLint("SetTextI18n")
    override fun onDrawerOpened(drawerView: View) {
        drawerOpen = 1
        drawerLayout1.openDrawer(GravityCompat.END)
        inflateFragment(fragmentMain)
        mfilters.text = "Category"
    }

    private fun inflateFragment(fragment: Fragment) {
        fragmentManager = supportFragmentManager
        this.fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.containerSide1, fragment)
        fragmentTransaction.commit()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        try {

            val mPrefs = application.getSharedPreferences("data", MODE_PRIVATE)
            val mVariable = Gson()
            val json = mPrefs.getString("detailsArray", "")
            var dataStored = DetailResModel()
            if (json != "") {
                dataStored = mVariable.fromJson(json, DetailResModel::class.java)
            }

            if (requestCode == 77 && resultCode == Activity.RESULT_OK) {

                if (dataStored.location.isNotEmpty()) {
                    arrayListOfLocation =
                        data!!.getParcelableArrayListExtra<ModelSearchLocation>("model")
                    binding.locationRecycler.visibility = View.VISIBLE
                    locationSetAdapter.update(arrayListOfLocation)
                } else {
                    arrayListOfLocation =
                        data!!.getParcelableArrayListExtra<ModelSearchLocation>("model")
                    binding.locationRecycler.visibility = View.VISIBLE
                    locationSetAdapter.update(arrayListOfLocation)
                }

            }

            if (requestCode == 71) {
                val arrayList = data!!.getParcelableArrayListExtra<ManageTeamModel>("model1")
                arrayListOfTeam.clear()
                if (dataStored.teamMember.isNotEmpty()) {
                    for (i in 0 until arrayList.size) {
                        arrayListOfTeam.add(arrayList[i])
                    }
                    binding.recyclerProducts.visibility = View.VISIBLE
                    teamSetAdapter.update(arrayListOfTeam)


                } else {
                    arrayListOfTeam = data.getParcelableArrayListExtra<ManageTeamModel>("model1")
                    binding.recyclerProducts.visibility = View.VISIBLE
                    teamSetAdapter.update(arrayListOfTeam)
                }

            }


            if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
                binding.editTextSubsDetails.text = data!!.getStringExtra("text")

            }


        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    @SuppressLint("SetTextI18n")
    private fun checkPreValidData() {
        try {
            progessBarView.visibility = View.VISIBLE
            arrayTags.clear()
            var dataStored = DetailResModel()
            var dataStoredBasic = BasicModel()
            val mPrefs = application.getSharedPreferences("data", MODE_PRIVATE)
            val mVariable = Gson()
            val json = mPrefs.getString("detailsArray", "")
            if (json != "") {
                dataStored = mVariable.fromJson(json, DetailResModel::class.java)
                if (dataStored.shareable == "1") {
                    shareable = "1"
                    binding.switch7.isChecked = true
                } else {
                    shareable = "0"
                }

                if (dataStored.subscriber == "1000") {
                    subLimit = "0"
                    binding.etMax.isEnabled = false
                } else {
                    subLimit = "1"
                    binding.etMax.isEnabled = true
                    binding.switch6.isChecked = true
                }

                if (dataStored.subInfo.isNotEmpty()) {
                    binding.editTextSubsDetails.text = dataStored.subInfo
                }

                if (dataStored.check == "1") {
                    binding.switch8.isChecked = true
                    check = "1"
                    binding.imagePublic.setImageResource(R.drawable.ic_private)
//                    binding.tVPublic.text = "Private"
                } else {
                    binding.switch8.isChecked = false
                    check = "0"
                    binding.imagePublic.setImageResource(R.drawable.ic_public)
//                    binding.tVPublic.text = "Public"

                }
                if (dataStored.location.isNotEmpty()) {
                    binding.locationRecycler.visibility = View.VISIBLE
                    for (i in 0 until dataStored.location.size) {
                        arrayListOfLocation.add(dataStored.location[i])
                    }
                    locationSetAdapter.update(arrayListOfLocation)
                }
                if (dataStored.tags.isNotEmpty() && dataStored.parentTags.isNotEmpty()) {

                    for (i in 0 until dataStored.tags.size) {
                        dataStored.tags[i].check = true
                        arrayTags.add(dataStored.tags[i])
                    }


                    if (arrayCategoriesMain.size > 0) {
                        for (items in arrayCategoriesMain) {
                            for (j in 0 until dataStored.parentTags.size) {
                                if (items.categoryId == dataStored.parentTags[j].categoryId) {
                                    items.check = true
                                    val chip = Chip(binding.rvTags.context)
                                    chip.text = dataStored.parentTags[j].categoryName
                                    chip.isClickable = true
                                    chip.isCheckable = false
                                    chip.setTextAppearanceResource(R.style.ChipTextStyle_Selected)
                                    chip.setChipBackgroundColorResource(R.color.home_free)
                                    chip.isCheckedIconEnabled = true
                                    chip.setOnCloseIconClickListener {
                                        binding.rvTags.removeView(chip)
                                    }
                                    chip.chipEndPadding = 20.0f
                                    chip.gravity = Gravity.CENTER

                                    binding.rvTags.addView(chip)

                                    binding.rvTags.isSingleSelection = true
                                    for (subItems in items.arrayList) {
                                        subItems.check = true
                                    }
                                }
                            }
                        }
                    }
                }

                if (dataStored.teamMember.isNotEmpty()) {

                    binding.recyclerProducts.visibility = View.VISIBLE
                    teamSetAdapter.update(dataStored.teamMember)
                    for (j in 0 until dataStored.teamMember.size) {
                        arrayListOfTeam.add(dataStored.teamMember[j])
                    }
                }
                binding.etMax.setText(dataStored.subscriber)
            } else {
                val mPrefsBasic = application.getSharedPreferences("data", MODE_PRIVATE)
                val jsonBasicData = mPrefsBasic.getString("basicData", "")
                if (jsonBasicData != "") {
                    dataStoredBasic = mVariable.fromJson(jsonBasicData, BasicModel::class.java)
                    if (dataStoredBasic.tags.isNotEmpty() && dataStoredBasic.parentTags.isNotEmpty()) {

                        for (i in 0 until dataStoredBasic.tags.size) {
                            dataStoredBasic.tags[i].check = true
                            arrayTags.add(dataStoredBasic.tags[i])
                        }

                        if (arrayCategoriesMain.size > 0) {
                            for (items in arrayCategoriesMain) {
                                for (j in 0 until dataStoredBasic.parentTags.size) {
                                    if (items.categoryId == dataStoredBasic.parentTags[j].categoryId) {
                                        items.check = true
                                        val chip = Chip(binding.rvTags.context)
                                        chip.text = dataStoredBasic.parentTags[j].categoryName
                                        chip.isClickable = true
                                        chip.isCheckable = false
                                        chip.setTextAppearanceResource(R.style.ChipTextStyle_Selected)
                                        chip.setChipBackgroundColorResource(R.color.home_free)
                                        chip.isCheckedIconEnabled = true
                                        chip.setOnCloseIconClickListener {
                                            binding.rvTags.removeView(chip)
                                        }
                                        chip.chipEndPadding = 20.0f
                                        chip.gravity = Gravity.CENTER

                                        binding.rvTags.addView(chip)

                                        binding.rvTags.isSingleSelection = true
                                        for (subItems in items.arrayList) {
                                            subItems.check = true
                                        }
                                    }
                                }
                            }
                        }

//                        if (arrayTags.size > 0) {
//                            for (prefilledItems in arrayTags)
//                                for (j in 0 until arrayCategoriesMain.size) {
//                                    for (items in arrayCategoriesMain[j].arrayList)
//                                        if (items.id == prefilledItems.id) {
//                                            arrayCategoriesMain[j].check = true
//                                            items.check = true
//
//                                        }
//                                }
//                        }

                    }
                }
            }
            progessBarView.visibility = View.GONE

        } catch (e: Exception) {
            progessBarView.visibility = View.GONE
            e.printStackTrace()
        }


    }


}
