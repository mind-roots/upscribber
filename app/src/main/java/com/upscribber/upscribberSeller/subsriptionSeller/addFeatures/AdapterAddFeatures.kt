package com.upscribber.upscribberSeller.subsriptionSeller.addFeatures

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.databinding.AddFeaturesRecyclerListBinding
import com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.createSubs.CreateSubscriptionExtraActivity
import kotlinx.android.synthetic.main.add_features_recycler_list.view.*

class AdapterAddFeatures(var context: Context, var from: Int) :
    RecyclerView.Adapter<AdapterAddFeatures.MyViewHolder>() {

    private lateinit var binding: AddFeaturesRecyclerListBinding
    private var items = ArrayList<ModelAddFeatures>()
    var listener = context as ChangeImages

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        binding = AddFeaturesRecyclerListBinding.inflate(LayoutInflater.from(context), parent, false)
        return MyViewHolder(binding)

    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = items[position]
        holder.bind(model)

        holder.itemView.setOnClickListener {
            if (from==1){
                context.startActivity(Intent(context,CreateSubscriptionExtraActivity::class.java))
            }else{
                listener.callActivity(position)
            }
            //


        }
        if (model.status) {
            holder.itemView.imagePlus.visibility = View.GONE
            holder.itemView.imageCheck.visibility = View.VISIBLE
        } else {
            holder.itemView.imagePlus.visibility = View.VISIBLE
            holder.itemView.imageCheck.visibility = View.GONE
        }
    }

    class MyViewHolder(var binding: AddFeaturesRecyclerListBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(obj: ModelAddFeatures) {
            binding.model = obj
            binding.executePendingBindings()
        }
    }

    fun update(items: ArrayList<ModelAddFeatures>) {
        if (items.size > 3) {
            items.removeAt(3)
        }
        this.items = items
        notifyDataSetChanged()
    }

    fun selection(position: Int) {

    }

    fun setTick(position: Int, step: String) {
        val steps = step
        if (steps.contains("4")) {
            steps.replace(",4", "")
        }
        val newSteps = steps.split(",")
        if (steps.isNotEmpty()) {
            for (element in newSteps) {
                items[element.toInt() - 1].status = true
            }
        }
        if (!steps.contains("1")) {
            items[0].status = false
        }
        if (!steps.contains("2")) {
            items[1].status = false
        }
        if (!steps.contains("3")) {
            items[2].status = false
        }
//        if (steps.contentEquals("1") ||
//            steps.contentEquals("1,2") ||
//            steps.contentEquals("1,3") ||
//            steps.contentEquals("1,2,3")
//        ) {
//
//            items[position].status = true
//
//        } else if (steps.contentEquals("1,2") ||
//            steps.contentEquals("2,3") ||
//            steps.contentEquals("1,2,3") ||
//            steps.contentEquals("2")
//        ) {
//            items[position].status = true
//        } else if (steps.isEmpty()) {
//            for (i in 0 until items.size) {
//                items[i].status = false
//
//            }
//        } else {
//            items[position].status = steps.contentEquals("3") ||
//                    steps.contentEquals("1,2,3") || steps.contentEquals("1,3") || steps.contentEquals("2,3")
//        }

        notifyDataSetChanged()
    }

    interface ChangeImages {
        fun callActivity(position: Int)
    }
}