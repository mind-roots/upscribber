package com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.products

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Gravity
import androidx.databinding.DataBindingUtil
import com.google.android.material.chip.Chip
import com.upscribber.commonClasses.Constant
import com.upscribber.R
import com.upscribber.databinding.ActivityProductSuccessfullyBinding
import com.upscribber.filters.ModelSubCategory
import com.upscribber.upscribberSeller.store.storeMain.ModelAddProduct
import com.upscribber.upscribberSeller.store.storeMain.StoreModel
import kotlinx.android.synthetic.main.activity_product_successfully.*

class ProductSuccessfullyActivity : AppCompatActivity() {

    lateinit var mBinding: ActivityProductSuccessfullyBinding
    private var arrayName : ArrayList<ModelSubCategory> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this,R.layout.activity_product_successfully)

        clickListeners()
        setData()
    }

    private fun setData() {
        val data = intent.getParcelableExtra<ModelAddProduct>("modelData")
        val arrayTags = intent.getParcelableArrayListExtra<ModelSubCategory>("arrayTags")
        txtProductName.text = data.productName
        txtProductPrice.text = "$" +  data.productPrice

        chipData(arrayTags)
    }

    private fun clickListeners() {
        mBinding.buttonDone1.setOnClickListener {
            val editor = Constant.getPrefs(this).edit()
            editor.putString("product","back")
            editor.apply()
            finish()
        }

    }

    private fun chipData(it: ArrayList<ModelSubCategory>) {
        arrayName = it

        for (index in arrayName.indices) {
            val chip = Chip(mBinding.rvTags.context)
            chip.text ="${arrayName[index].name}"

            // necessary to get single selection working
            chip.isClickable = true
            chip.isCheckable = false
            chip.setTextAppearanceResource(R.style.ChipTextStyle_Selected)
            chip.setChipBackgroundColorResource(R.color.home_free)
            chip.chipEndPadding = 20.0f
            chip.gravity = Gravity.CENTER
            mBinding.rvTags.addView(chip)
        }

        mBinding.rvTags.isSingleSelection = true


    }

}
