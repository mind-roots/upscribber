package com.upscribber.upscribberSeller.subsriptionSeller.subscriptionView.imageView

import android.content.Context
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.Glide
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.subsciptions.merchantSubscriptionPages.ModelImagesSubscriptionDetails

class SubsPagerAdapter(
    var mData: ArrayList<ModelImagesSubscriptionDetails>,
    var context: Context
) : PagerAdapter() {



    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`

    }

    override fun getCount(): Int {
        return mData.size
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val itemView = LayoutInflater.from(container.context)
            .inflate(R.layout.subs_image_pager_adpter, container, false)
        val imagePath  = Constant.getPrefs(context).getString(Constant.dataImagePath, "")
        val img_pager1 = itemView.findViewById(R.id.imageSmall) as ImageView
        Glide.with(context).load(imagePath + mData[position].image).into(img_pager1)
        container.addView(itemView)


        return itemView
    }



    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View?)
    }
}