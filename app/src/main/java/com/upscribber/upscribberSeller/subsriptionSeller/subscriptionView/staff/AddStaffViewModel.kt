package com.upscribber.upscribberSeller.subsriptionSeller.subscriptionView.staff

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData

class AddStaffViewModel(application: Application) : AndroidViewModel(application) {

    var addStaffRepository : AddStaffRepository = AddStaffRepository(application)

    fun getList(): LiveData<ArrayList<AddStaffModel>> {
        return  addStaffRepository.getStaffList()

    }
}
