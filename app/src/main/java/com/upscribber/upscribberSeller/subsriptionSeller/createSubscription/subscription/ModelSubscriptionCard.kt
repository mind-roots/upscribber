package com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.subscription

import android.os.Parcel
import android.os.Parcelable

class ModelSubscriptionCard() : Parcelable {

    var subscriber: String = ""
    var isSelected: Boolean = false
    var free_trial: String = ""
    var free_trial_qty: String = ""
    var introductory_price: String = ""
    var introductory_days: String = ""
    var invited_customers: String = ""
    var unit: String = ""
    var id = ""
    var pricingNumber = ""
    var campaignId = ""
    var redeemtion_cycle_qty = ""
    var quantityName = ""
    var frequency = ""
    var frequencyType = ""
    var price = ""
    var discountPrice = ""
    var intro = ""
    var introPrice = ""
    var introDays = ""
    var freeTrail = ""
    var freeTrailValue = ""
    var steps = ""
    var createdAt = ""
    var updatedAt = ""
    var statusMain: String = ""
    var msg: String = ""
    var campaign_id: String = ""
    var frequency_type: String = ""
    var frequency_value: String = ""
    var status: String = ""
    var url: String = ""
    var created_at: String = ""
    var updated_at: String = ""
    var merchant_id: String = ""
    var merchant_name: String = ""
    var business_id: String = ""
    var feature_image: String = ""
    var business_logo: String = ""
    var campaign_image: String = ""
    var campaign_name: String = ""
    var description: String = ""
    var status1: String = ""
    var business_name: String = ""
    var discount_price: String = ""
    var subscription_id: String = ""
    var is_public: String = ""
    var bought: String = ""
    var free_trail_qty: String = ""
    var pricingSubscriber: String = "0"

    constructor(parcel: Parcel) : this() {
        subscriber = parcel.readString()
        isSelected = parcel.readByte() != 0.toByte()
        free_trial = parcel.readString()
        free_trial_qty = parcel.readString()
        introductory_price = parcel.readString()
        introductory_days = parcel.readString()
        invited_customers = parcel.readString()
        unit = parcel.readString()
        id = parcel.readString()
        pricingNumber = parcel.readString()
        campaignId = parcel.readString()
        redeemtion_cycle_qty = parcel.readString()
        quantityName = parcel.readString()
        frequency = parcel.readString()
        frequencyType = parcel.readString()
        price = parcel.readString()
        discountPrice = parcel.readString()
        intro = parcel.readString()
        introPrice = parcel.readString()
        introDays = parcel.readString()
        freeTrail = parcel.readString()
        freeTrailValue = parcel.readString()
        steps = parcel.readString()
        createdAt = parcel.readString()
        updatedAt = parcel.readString()
        statusMain = parcel.readString()
        msg = parcel.readString()
        campaign_id = parcel.readString()
        frequency_type = parcel.readString()
        frequency_value = parcel.readString()
        status = parcel.readString()
        url = parcel.readString()
        created_at = parcel.readString()
        updated_at = parcel.readString()
        merchant_id = parcel.readString()
        merchant_name = parcel.readString()
        business_id = parcel.readString()
        feature_image = parcel.readString()
        business_logo = parcel.readString()
        campaign_image = parcel.readString()
        campaign_name = parcel.readString()
        description = parcel.readString()
        status1 = parcel.readString()
        business_name = parcel.readString()
        discount_price = parcel.readString()
        subscription_id = parcel.readString()
        is_public = parcel.readString()
        bought = parcel.readString()
        free_trail_qty = parcel.readString()
        pricingSubscriber = parcel.readString()
    }

    fun frequencyType(): String {
        if (frequencyType == "2" || frequencyType == "Year") {
            frequencyType = "Yearly"
        }
        if (frequencyType == "1" || frequencyType == "Month") {
            frequencyType = "Monthly"
        }

        return "$frequencyType Pricing"
    }

    fun disPrice(): String {
        var disPrice = ""
        disPrice = if (discountPrice == "") {
            "0.00"
        } else {
            discountPrice
        }

        return "$disPrice%"
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(subscriber)
        parcel.writeByte(if (isSelected) 1 else 0)
        parcel.writeString(free_trial)
        parcel.writeString(free_trial_qty)
        parcel.writeString(introductory_price)
        parcel.writeString(introductory_days)
        parcel.writeString(invited_customers)
        parcel.writeString(unit)
        parcel.writeString(id)
        parcel.writeString(pricingNumber)
        parcel.writeString(campaignId)
        parcel.writeString(redeemtion_cycle_qty)
        parcel.writeString(quantityName)
        parcel.writeString(frequency)
        parcel.writeString(frequencyType)
        parcel.writeString(price)
        parcel.writeString(discountPrice)
        parcel.writeString(intro)
        parcel.writeString(introPrice)
        parcel.writeString(introDays)
        parcel.writeString(freeTrail)
        parcel.writeString(freeTrailValue)
        parcel.writeString(steps)
        parcel.writeString(createdAt)
        parcel.writeString(updatedAt)
        parcel.writeString(statusMain)
        parcel.writeString(msg)
        parcel.writeString(campaign_id)
        parcel.writeString(frequency_type)
        parcel.writeString(frequency_value)
        parcel.writeString(status)
        parcel.writeString(url)
        parcel.writeString(created_at)
        parcel.writeString(updated_at)
        parcel.writeString(merchant_id)
        parcel.writeString(merchant_name)
        parcel.writeString(business_id)
        parcel.writeString(feature_image)
        parcel.writeString(business_logo)
        parcel.writeString(campaign_image)
        parcel.writeString(campaign_name)
        parcel.writeString(description)
        parcel.writeString(status1)
        parcel.writeString(business_name)
        parcel.writeString(discount_price)
        parcel.writeString(subscription_id)
        parcel.writeString(is_public)
        parcel.writeString(bought)
        parcel.writeString(free_trail_qty)
        parcel.writeString(pricingSubscriber)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ModelSubscriptionCard> {
        override fun createFromParcel(parcel: Parcel): ModelSubscriptionCard {
            return ModelSubscriptionCard(parcel)
        }

        override fun newArray(size: Int): Array<ModelSubscriptionCard?> {
            return arrayOfNulls(size)
        }
    }


}