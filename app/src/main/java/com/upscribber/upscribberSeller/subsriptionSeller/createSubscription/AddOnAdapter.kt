package com.upscribber.upscribberSeller.subsriptionSeller.createSubscription

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.R
import com.upscribber.databinding.AddOnLayoutBinding
import com.upscribber.upscribberSeller.subsriptionSeller.extras.ModelExtra

class AddOnAdapter(var context: Context ): RecyclerView.Adapter<AddOnAdapter.MyViewHolder>() {

    private lateinit var mBinding: AddOnLayoutBinding
      var items = ArrayList<ModelExtra>()



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
//        binding = AddsOnLayoutBinding.inflate(LayoutInflater.from(context), parent,false)
//        return AddOnAdapter.MyViewHolder(binding)


        mBinding = DataBindingUtil.inflate(LayoutInflater.from(context),
            R.layout.add_on_layout, parent, false)
        return MyViewHolder(mBinding)

    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model:ModelExtra= items[position]

//        holder.binding.textView259.text = model.name
//        holder.binding.textView263.text = model.price


        holder.bind(model)
    }

    class MyViewHolder(var  binding: AddOnLayoutBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(obj: ModelExtra) {
         binding.list = obj
            binding.executePendingBindings()
        }
    }


    fun update(items: ArrayList<ModelExtra>) {
        this.items = items
        notifyDataSetChanged()
    }
}