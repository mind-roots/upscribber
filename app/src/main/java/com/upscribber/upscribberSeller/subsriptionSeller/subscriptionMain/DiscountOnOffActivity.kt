package com.upscribber.upscribberSeller.subsriptionSeller.subscriptionMain

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.LinearLayout
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.databinding.ActivityDiscounOnOffBinding

class DiscountOnOffActivity : AppCompatActivity() {

    lateinit var mBinding: ActivityDiscounOnOffBinding
    private lateinit var mViewModel: SubscriptionSellerViewModel
    private lateinit var includeLayoutProgress: ConstraintLayout
    var discountStatus = ""


    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_discoun_on_off)
        mViewModel = ViewModelProviders.of(this)[SubscriptionSellerViewModel::class.java]
        setToolbar()
        includeLayoutProgress = findViewById(R.id.includeLayoutProgress)
        clickListener()
        observerInit()
        if (intent.hasExtra("discountStatus")) {
            discountStatus = intent.getStringExtra("discountStatus")
        }

        if (discountStatus == "0") {
            mBinding.imageView62.setImageResource(R.drawable.ic_referral_discount_red)
            mBinding.textView85.setCompoundDrawablesWithIntrinsicBounds(
                R.drawable.ic_check_circle_red,
                0,
                0,
                0
            )
            mBinding.textView85.text = "Discounting Off"
            mBinding.buttonTurn.text = "Turn On Discounting"


        } else {
            mBinding.imageView62.setImageResource(R.drawable.ic_referral_discount_green)
            mBinding.textView85.setCompoundDrawablesWithIntrinsicBounds(
                R.drawable.ic_check_circle_green,
                0,
                0,
                0
            )
            mBinding.textView85.text = "20% Discount On"
            mBinding.buttonTurn.text = "Turn Off Discounting"

        }
    }

    private fun observerInit() {
        mViewModel.getActiveInactivResponse().observe(this, Observer {
            if (it.status == "true") {
                finish()
            }

        })

        mViewModel.getStatus().observe(this, Observer {
            if (it.msg == "Invalid auth code"){
                Constant.commonAlert(this)
            }else{
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun clickListener() {
        mBinding.buttonTurn.setOnClickListener {
            includeLayoutProgress.visibility = View.VISIBLE
            var campignId = ""
            if (intent.hasExtra("campaignId")) {
                campignId = intent.getStringExtra("campaignId")
            }
            if (discountStatus == "0") {
                mViewModel.setUpdateCampDisStatus(campignId, "1", "1","")

            } else {
                mViewModel.setUpdateCampDisStatus(campignId, "0", "1","")
            }


        }
    }

    private fun setToolbar() {
        setSupportActionBar(mBinding.toolbar)
        title = ""
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

}
