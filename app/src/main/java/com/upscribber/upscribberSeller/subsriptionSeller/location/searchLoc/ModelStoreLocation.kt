package com.upscribber.upscribberSeller.subsriptionSeller.location.searchLoc

import android.os.Parcel
import android.os.Parcelable

class ModelStoreLocation() : Parcelable {

    var open: String = ""
    var close: String = ""
    var status: String = ""
    var checked: String = ""
    var day: String = ""
    var checkedFirst = ""
    var days = ArrayList<String>()

    constructor(parcel: Parcel) : this() {
        open = parcel.readString()
        close = parcel.readString()
        status = parcel.readString()
        checked = parcel.readString()
        day = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(open)
        parcel.writeString(close)
        parcel.writeString(status)
        parcel.writeString(checked)
        parcel.writeString(day)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ModelStoreLocation> {
        override fun createFromParcel(parcel: Parcel): ModelStoreLocation {
            return ModelStoreLocation(parcel)
        }

        override fun newArray(size: Int): Array<ModelStoreLocation?> {
            return arrayOfNulls(size)
        }
    }
}
