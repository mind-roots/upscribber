package com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.products

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.upscribber.upscribberSeller.store.storeMain.StoreModel
import com.upscribber.upscribberSeller.store.storeMain.StoreViewModel
import com.upscribber.R
import com.upscribber.databinding.ActivitySelectProductBinding
import com.upscribber.upscribberSeller.store.storeInfo.AddProductActivity
import com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.createSubs.CreateSubsriptionBasicActivity
import kotlinx.android.synthetic.main.activity_select_product.*

class SelectProductActivity : AppCompatActivity(), StoreAdapter1.SelectSingle {

    private lateinit var mAdapter: StoreAdapter1
    private lateinit var mBinding: ActivitySelectProductBinding
    private lateinit var viewModel: StoreViewModel
    private lateinit var arrayBackgroundChange: ArrayList<StoreModel>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_select_product)
        mBinding.lifecycleOwner = this
        viewModel = ViewModelProviders.of(this)[StoreViewModel::class.java]
        apiInit()
        setToolbar()
        clickListener()
        setAdapter()
        observerInit()

    }

    private fun observerInit() {
        viewModel.getAllProducts().observe(this, Observer {
            if (it.size > 0) {
                hideLoader()
                mBinding.tvNoDataDescription.visibility = View.GONE
                mBinding.tvNoData.visibility = View.GONE
                mBinding.getStartedBtn.visibility = View.GONE
                mBinding.imageView84.visibility = View.GONE
                mBinding.recyclerProducts.visibility = View.VISIBLE
                mBinding.nestedScrollView10.visibility = View.VISIBLE
                mBinding.buttonDone.visibility = View.VISIBLE
                mBinding.recyclerProducts.adapter = mAdapter
                arrayBackgroundChange = it
                if (intent.hasExtra("products")) {
                    val arrayListOfProducts =
                        intent.getParcelableArrayListExtra<StoreModel>("products")
                    for (items in it) {
                        for (prefilled in arrayListOfProducts) {
                            if (items.id == prefilled.id) {
                                items.status = true
                            }
                        }
                    }
                }
                mAdapter.update(it)
            } else {
                hideLoader()
                mBinding.tvNoDataDescription.visibility = View.VISIBLE
                mBinding.tvNoData.visibility = View.VISIBLE
                mBinding.getStartedBtn.visibility = View.VISIBLE
                mBinding.imageView84.visibility = View.VISIBLE
                mBinding.buttonDone.visibility = View.GONE
                mBinding.recyclerProducts.visibility = View.GONE
                mBinding.nestedScrollView10.visibility = View.GONE
            }
        })


    }

    private fun apiInit() {
        showLoader()
        viewModel.getProducts("")
    }

    override fun onResume() {
        super.onResume()
        showLoader()
        viewModel.getProducts("")

    }

    private fun setAdapter() {
        mBinding.recyclerProducts.layoutManager = LinearLayoutManager(this)
        mAdapter = StoreAdapter1(this)
        mBinding.recyclerProducts.adapter = mAdapter

//            viewModel.getStoreList().observe(this, Observer {
//                arrayBackgroundChange= it
//                mAdapter.update(it)
//            })
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setToolbar() {
        setSupportActionBar(mBinding.toolbar)
        title = ""
        mBinding.title.text = resources.getString(R.string.select_product)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)

    }

    override fun SelectProduct(position: Int) {

        val model = arrayBackgroundChange[position]
        model.status = !model.status

        arrayBackgroundChange[position] = model
        mAdapter.notifyDataSetChanged()

    }

    private fun clickListener() {
        mBinding.addProduct.setOnClickListener {
            if(intent.hasExtra("fromBasics")){
                startActivity(Intent(this, AddProductActivity::class.java).putExtra("fromBasics","basic"))
            }else{
                startActivity(Intent(this, AddProductActivity::class.java))
            }

        }


        mBinding.getStartedBtn.setOnClickListener {
            startActivity(Intent(this, AddProductActivity::class.java))
        }


        mBinding.buttonDone.setOnClickListener {
            showLoader()
            val arraySelected: ArrayList<StoreModel> = ArrayList()
            for (i in 0 until arrayBackgroundChange.size) {
                if (arrayBackgroundChange[i].status) {
                    arraySelected.add(arrayBackgroundChange[i])
                }
            }
            if (arraySelected.size > 0) {
                if (arraySelected.size > 2) {
                    hideLoader()
                    Toast.makeText(
                        this,
                        "You cannot select more than 2 products!",
                        Toast.LENGTH_LONG
                    ).show()
                } else {
                    val intent = Intent(this, CreateSubsriptionBasicActivity::class.java)
                    intent.putExtra("model1", arraySelected)
                    setResult(Activity.RESULT_OK, intent)
                    finish()
                }
            } else {
                hideLoader()
                Toast.makeText(this, "Please select first", Toast.LENGTH_LONG).show()
            }

        }
    }

    fun showLoader() {
        progressBarProducts.visibility = View.VISIBLE
    }

    fun hideLoader() {
        progressBarProducts.visibility = View.GONE
    }


}
