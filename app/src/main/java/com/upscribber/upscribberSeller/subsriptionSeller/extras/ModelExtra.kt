package com.upscribber.upscribberSeller.subsriptionSeller.extras

import android.os.Parcel
import android.os.Parcelable

class ModelExtra() : Parcelable{
    lateinit var name:String
    lateinit var price:String
    var isSelected: Boolean = false

    constructor(parcel: Parcel) : this() {
        name = parcel.readString()
        price = parcel.readString()
        isSelected = parcel.readByte() != 0.toByte()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        parcel.writeString(price)
        parcel.writeByte(if (isSelected) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ModelExtra> {
        override fun createFromParcel(parcel: Parcel): ModelExtra {
            return ModelExtra(parcel)
        }

        override fun newArray(size: Int): Array<ModelExtra?> {
            return arrayOfNulls(size)
        }
    }


}