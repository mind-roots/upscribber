package com.upscribber.upscribberSeller.navigationCustomer

import android.content.Context
import android.view.View
import androidx.viewpager.widget.PagerAdapter
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import com.upscribber.R
import com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.subscription.ModelSubscriptionCard

class InviteSendConfirmationAdapter(
    var context: Context,
    var mData: ModelSubscriptionCard
) :
    PagerAdapter() {

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun getCount(): Int {
        return 0
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val itemView = LayoutInflater.from(container.context)
            .inflate(R.layout.fragment_share2, container, false)

//        val data = mData[position]
//        val billingCycle = itemView.findViewById(R.id.storeName) as TextView
//        val quantity = itemView.findViewById(R.id.service) as TextView
//        val amount = itemView.findViewById(R.id.amount) as TextView
//        val camapaignName = itemView.findViewById(R.id.textView336) as TextView
//
//
//        billingCycle.text = data.frequency_type
//        amount.text = data.price
//        camapaignName.text = data.campaign_name
//
//        if (data.redeemtion_cycle_qty == "500000"){
//            quantity.text = context.resources.getString(R.string.infinity) + " " + data.unit
//        }else{
//            quantity.text = data.redeemtion_cycle_qty  + " " + data.unit
//        }


        container.addView(itemView)
        return itemView
    }


    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View?)
    }


}