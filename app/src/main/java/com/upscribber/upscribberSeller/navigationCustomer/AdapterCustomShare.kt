package com.upscribber.upscribberSeller.navigationCustomer

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.R
import kotlinx.android.synthetic.main.custom_sharekit_layout.view.*

class AdapterCustomShare(val mContext: Context, private val customShareList: ArrayList<CustomShareKitModel>) : RecyclerView.Adapter<AdapterCustomShare.MyViewHolder>() {


    var customShareKit = mContext as CustomShareKit


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(mContext)
            .inflate(R.layout.custom_sharekit_layout, parent, false)

        return MyViewHolder(v)
    }

    override fun getItemCount(): Int {
        return customShareList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = customShareList[position]

        holder.itemView.ivShareImage.setImageResource(model.shareImage)
        holder.itemView.tvName.text = model.shareImageName

        holder.itemView.setOnClickListener {

            customShareKit.getCustomShareClick(position)

        }

    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    interface CustomShareKit{
        fun getCustomShareClick(position: Int)
    }

}
