package com.upscribber.upscribberSeller.navigationCustomer

import android.content.Intent
import android.graphics.Outline
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.ViewOutlineProvider
import android.view.WindowManager
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.widget.SearchView
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.upscribber.commonClasses.Constant
import com.upscribber.filters.*
import com.upscribber.filters.Redemption.RedemptionFilterFragment
import com.upscribber.upscribberSeller.customerBottom.profile.subscriptionList.SubscriptionListActivity
import com.upscribber.R
import com.upscribber.databinding.ActivityCustomerSellerBinding
import com.upscribber.upscribberSeller.customerBottom.scanning.customerlistselection.ModelScanCustomers
import com.upscribber.upscribberSeller.customerBottom.scanning.customerlistselection.ScanCustomerViewModel
import kotlinx.android.synthetic.main.activity_customer_seller.*
import kotlinx.android.synthetic.main.activity_discover.drawerLayout1
import kotlinx.android.synthetic.main.filter_header.*

class CustomerSellerActivity : AppCompatActivity()
   , DrawerLayout.DrawerListener , FilterInterface, FilterFragment.SetFilterHeaderBackground
{

    lateinit var toolbar: Toolbar
    lateinit var toolbarTitle: TextView
    private lateinit var binding: ActivityCustomerSellerBinding
    private lateinit var mAdapter: AdapterCustomerList
    private lateinit var constraintBackground: ConstraintLayout
    private lateinit var fragmentManager: FragmentManager
    private lateinit var fragmentTransaction: FragmentTransaction
    var drawerOpen = 0
    private lateinit var btnFilter: TextView
    private lateinit var brand_search: SearchView
    private lateinit var mfilters: TextView
    private lateinit var mreset: TextView
    private lateinit var filterHeader: LinearLayout
    private lateinit var filter_done: TextView
    private lateinit var viewModelCustomers: ScanCustomerViewModel
    var modelCustomer : ArrayList<ModelScanCustomers> = ArrayList()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(this, R.color.home_free)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_customer_seller)
        viewModelCustomers = ViewModelProviders.of(this)[ScanCustomerViewModel::class.java]
        binding.lifecycleOwner = this
        initz()
        setToolbar()
        clickks()
        setFilters()
        roundedImage()
        setAdapter()
        apiInitialization()
        observerInit()



    }

    private fun observerInit() {
        viewModelCustomers.getStatus().observe(this, Observer {
            progressBar.visibility = View.GONE
            if (it.msg == "Invalid auth code") {
                Constant.commonAlert(this)
            } else {
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
            }

        })

        viewModelCustomers.getCustomersList().observe(this, Observer {
            modelCustomer = it
            progressBar.visibility = View.GONE
            mAdapter.update(modelCustomer)
        })

    }

    private fun setAdapter() {
        val layoutManager = LinearLayoutManager(this)
        binding.customerRecycler.layoutManager = layoutManager
        mAdapter = AdapterCustomerList(this)
        binding.customerRecycler.adapter = mAdapter

    }

    private fun apiInitialization() {
        val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
        val type = Constant.getPrefs(this).getString(Constant.type, "")
        viewModelCustomers.getCustomersList(auth,"","0","0",type)
        progressBar.visibility = View.VISIBLE

    }

    private fun setFilters() {

        btnFilter.setOnClickListener {
            drawerOpen = 1
            drawerLayout1.openDrawer(GravityCompat.END)
            inflateFragment(FilterFragment(this))
            filterImgBack.visibility = View.GONE
            mfilters.text = "Filters"
            if (brand_search != null) {
                brand_search.setQuery("", false);
                brand_search.clearFocus();
            }
            Constant.hideKeyboard(this,brand_search)
            mreset.visibility = View.VISIBLE
        }
    }

    override fun onResume() {
        super.onResume()
        Constant.hideKeyboard(this,brand_search)

    }


    private fun inflateFragment(fragment: Fragment) {
        fragmentManager = supportFragmentManager
        this.fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.containerSide1, fragment)
        fragmentTransaction.commit()
    }

    override fun onBackPressed() {
        if (drawerOpen == 1) {
            drawerOpen = 0
            drawerLayout1.closeDrawer(GravityCompat.END)
            Constant.hideKeyboard(this,brand_search)
        } else {
            super.onBackPressed()
        }
    }


    private fun clickks() {

        binding.InviteDone.setOnClickListener {
            val intent = Intent(this, SubscriptionListActivity::class.java)
            intent.putExtra("subscription","inviteList")
            startActivity(intent)

        }

        filter_done.setOnClickListener {
            onBackPressed()
        }
    }

    private fun roundedImage() {
        val curveRadius = 50F

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            constraintBackground.outlineProvider = object : ViewOutlineProvider() {

                @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                override fun getOutline(view: View?, outline: Outline?) {
                    outline?.setRoundRect(
                        0,
                        -100,
                        view!!.width,
                        view.height,
                        curveRadius
                    )
                }
            }
            constraintBackground.clipToOutline = true
        }

    }


    private fun initz() {
        filterHeader = findViewById(R.id.filterHeader)
        toolbar = findViewById(R.id.CustomerListToolbar)
        toolbarTitle = findViewById(R.id.titleCustomerList)
        constraintBackground = findViewById(R.id.constraintBackground)
        btnFilter = findViewById(R.id.btnFilter)
        mreset = findViewById(R.id.reset)
        mfilters = findViewById(R.id.filters)
        filter_done = findViewById(R.id.filter_done)
        brand_search = findViewById(R.id.brand_search)
        if (intent.hasExtra("profileTeam")){
            binding.InviteDone.visibility = View.GONE
        }else{
            binding.InviteDone.visibility = View.VISIBLE
        }

    }

    private fun setToolbar() {
        setSupportActionBar(toolbar)
        title = ""
        toolbarTitle.text = "Customer"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white)

    }

    override fun filterHeaderBackground(s: String) {

        if (s=="new") {
            filterHeader.setBackgroundDrawable(getDrawable(R.drawable.filter_header_select_category))


        }else{
            filterHeader.setBackgroundDrawable(getDrawable(R.drawable.filter_header))

        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onDrawerStateChanged(newState: Int) {

        var hh = newState
    }

    override fun onDrawerSlide(drawerView: View, slideOffset: Float) {
        var f = slideOffset

    }

    override fun onDrawerClosed(drawerView: View) {
        drawerOpen = 0
        Constant.hideKeyboard(this,brand_search)
    }

    override fun onDrawerOpened(drawerView: View) {
        drawerOpen = 1
        drawerLayout1.openDrawer(GravityCompat.END)
        inflateFragment(FilterFragment(this))
        filterImgBack.visibility = View.GONE
        mfilters.text = "Filters"
        mreset.visibility = View.VISIBLE
    }


    override fun nextFragments(i: Int) {
        when (i) {
            1 -> {
                inflateFragment(SortByFragment())
                mreset.visibility = View.GONE
                filterImgBack.visibility = View.VISIBLE
                filters.text = "Sort By"

                filterImgBack.setOnClickListener {
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"
                }

                filter_done.setOnClickListener {
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"
                }
            }


            2 -> {
                inflateFragment(CategoryFilterFragment(this))
                mreset.visibility = View.GONE
                filterImgBack.visibility = View.VISIBLE
                filters.text = "Category"

                filterImgBack.setOnClickListener {
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"

                }
                filter_done.setOnClickListener {
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"
                }
            }


            3 -> {
                inflateFragment(BrandsFilterFragment())
                mreset.visibility = View.GONE
                filterImgBack.visibility = View.VISIBLE
                filters.text = "Brands"

                filterImgBack.setOnClickListener {
                    Constant.hideKeyboard(this,brand_search)
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"

                }

                filter_done.setOnClickListener {
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"
                }
            }
            4 -> {
                inflateFragment(RedemptionFilterFragment())
                mreset.visibility = View.GONE
                filterImgBack.visibility = View.VISIBLE
                filters.text = "Redemption"

                filterImgBack.setOnClickListener {
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"

                }
                filter_done.setOnClickListener {
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"
                }
            }
        }

    }
}
