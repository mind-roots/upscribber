package com.upscribber.upscribberSeller.navigationCustomer

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.wafflecopter.multicontactpicker.ContactResult
import kotlinx.android.synthetic.main.set_contact_list.view.*
import java.lang.Exception
import java.util.ArrayList

class ContactListAdapter(var contacts: ArrayList<ContactResult>, val mContext: Context) :
    RecyclerView.Adapter<ContactListAdapter.MyViewHolder>() {

    val listener = mContext as updataData

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(mContext)
            .inflate(R.layout.set_contact_list, parent, false)
        return MyViewHolder(v)
    }

    override fun getItemCount(): Int {
        return contacts.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
//        val model = contacts[position]
//        val separated = contacts[position].split(";".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
//        val number = separated[1].replace("-", "").replace("(", "").replace(")", "")
//                        .replace(" ", "").replace("+91", "")

        try {
            holder.itemView.customerNumber.text =Constant.formatPhoneNumber(
                contacts[position].phoneNumbers[0].number
            )

        } catch (e: Exception) {
            e.printStackTrace()
        }
        try {
            holder.itemView.txtName.text = contacts[position].displayName

        } catch (e: Exception) {
            e.printStackTrace()
        }



        holder.itemView.imgCross.setOnClickListener {
            contacts.removeAt(position)
            notifyDataSetChanged()
            //listener.removeAtPosition(contacts, position)
        }

    }

    fun update(contactsUpdate: ArrayList<ContactResult>) {
        contacts = contactsUpdate
        notifyDataSetChanged()
    }


    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    interface updataData {
        fun removeAtPosition(contacts: ArrayList<ContactResult>, position: Int)
    }
}
