package com.upscribber.upscribberSeller.navigationCustomer

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.pm.ResolveInfo
import android.graphics.Color
import android.graphics.Outline
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.appsflyer.AppsFlyerConversionListener
import com.appsflyer.AppsFlyerLib
import com.appsflyer.AppsFlyerProperties
import com.appsflyer.CreateOneLinkHttpTask
import com.appsflyer.share.ShareInviteHelper
import com.bumptech.glide.Glide
import com.google.gson.Gson
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.commonClasses.RoundedBottomSheetDialogFragment
import com.upscribber.databinding.ActivityInviteSubscriptionBinding
import com.upscribber.profile.ModelGetProfile
import com.upscribber.subsciptions.manageSubscriptions.shareSubscription.AdapterShareRedeem
import com.upscribber.subsciptions.manageSubscriptions.shareSubscription.ModelShareRedeem
import com.upscribber.upscribberSeller.customerBottom.customerList.ModelCustomerListt
import com.upscribber.upscribberSeller.customerBottom.profile.subscriptionList.SubscriptionListModel
import com.upscribber.upscribberSeller.customerBottom.scanning.customerlistselection.ModelScanCustomers
import com.upscribber.upscribberSeller.customerBottom.scanning.customerlistselection.ScanCustomerViewModel
import com.upscribber.upscribberSeller.customerBottom.scanning.customerlistselection.ScanCustomersActivity
import com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.createSubs.BasicModel
import com.upscribber.upscribberSeller.subsriptionSeller.subscriptionMain.ModelSellerSubscriptions
import com.upscribber.upscribberSeller.subsriptionSeller.subscriptionMain.SubscriptionSellerViewModel
import com.wafflecopter.multicontactpicker.ContactResult
import com.wafflecopter.multicontactpicker.LimitColumn
import com.wafflecopter.multicontactpicker.MultiContactPicker
import org.json.JSONArray
import java.math.BigDecimal
import java.math.RoundingMode


@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class InviteSubscription : AppCompatActivity(), AdapterShareRedeem.HideLayout,
    ScanCustomerAdapterForList.DeleteList, ContactListAdapter.updataData {

    private val devKey: String = "9ZoQoVJhU3FRyk8oC5X9kA"
    private var results = ArrayList<ContactResult>()
    lateinit var fragmentMenu: MenuFragmentShare
    private lateinit var mBinding: ActivityInviteSubscriptionBinding
    private var arrayList: ArrayList<ModelScanCustomers> = ArrayList()
    private lateinit var teamSetAdapter: ScanCustomerAdapterForList
    private lateinit var mContactAdapter: ContactListAdapter
    private val PICK_CONTACT = 1000
    private lateinit var mViewModel: ScanCustomerViewModel
    private lateinit var mViewModelCreate: SubscriptionSellerViewModel
    lateinit var mAdapterRedeem: AdapterShareRedeem
    lateinit var progressBar: ConstraintLayout
    var subscriptionListId = ""
    var subscriptionProfileId = ""
    var customerId = ""
    var flag = 0
    var isVisiblee: Boolean = false

    private val AF_DEV_KEY = "9ZoQoVJhU3FRyk8oC5X9kA"

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_invite_subscription)
        mViewModel = ViewModelProviders.of(this)[ScanCustomerViewModel::class.java]
        mViewModelCreate = ViewModelProviders.of(this)[SubscriptionSellerViewModel::class.java]
        mAdapterRedeem = AdapterShareRedeem(this)
        progressBar = findViewById(R.id.progressBar)
        setAdapter()


        when {
            intent.hasExtra("toolbar") -> {
                mBinding.subView.visibility = View.VISIBLE
                mBinding.etSubsription.visibility = View.GONE
                clickListeners()
            }
            intent.hasExtra("fromCustomers") -> {
                mBinding.subView.visibility = View.GONE
                mBinding.etSubsription.visibility = View.VISIBLE
                mBinding.constraint.visibility = View.GONE
                setDataSubscription()
            }
            else -> {
                setSubscriptionData()
            }
        }

        setToolbar()
        onClick()
        observerInit()

//        AppsFlyerLib.getInstance().sendDeepLinkData(this)
        AppsFlyerLib.getInstance().setAppInviteOneLink("GtCg")
//        AppsFlyerLib.getInstance().setCustomerUserId("myId")
        setConversationalData()

//        AppsFlyerProperties.getInstance().getString(AppsFlyerProperties.APP_USER_ID)
//        AppsFlyerLib.getInstance().setCustomerIdAndTrack("customer_id", this)

//        userIdentifier()

    }



    private fun setDataSubscription() {
        val data = intent.getParcelableExtra<SubscriptionListModel>("model")


        if (intent.hasExtra("modelCustomerProfile")) {
            val dataCustomer =
                intent.getParcelableExtra("modelCustomerProfile") as ModelCustomerListt
            customerId = dataCustomer.customer_id
        } else {
            val dataCustomer1 = intent.getParcelableExtra("modelList") as ModelScanCustomers
            customerId = dataCustomer1.customer_id
        }
        val imagePath = Constant.getPrefs(this).getString(Constant.dataImagePath, "")
        mBinding.tvLogoNum.text = data.redeemtion_cycle_qty
        mBinding.name.text = data.campaign_name
        val priceDiscounted = Constant.getCalculatedPrice(data.discount_price, data.price)
        if (priceDiscounted.contains(".")){
            val splitPos = priceDiscounted.split(".")
            if (splitPos[1] == "00" || splitPos[1] == "0") {
                mBinding.cost.text  = data.unit + " for $" + splitPos[0]
            }else{
                mBinding.cost.text = data.unit + " for $" + BigDecimal(priceDiscounted).setScale(2, RoundingMode.HALF_UP).toString()
            }
        }else{
            mBinding.cost.text =  data.unit + " for $" + BigDecimal(priceDiscounted).setScale(2, RoundingMode.HALF_UP).toString()

        }

        subscriptionProfileId = data.subscription_id
        Glide.with(this).load(imagePath + data.campaign_image)
            .placeholder(R.mipmap.placeholder_subscription_square)
            .into(mBinding.logo)

        if (intent.hasExtra("profileData")) {
            isVisiblee = true

//            val imagePath = Constant.getPrefs(this).getString(Constant.dataImagePath, "")
//
//            Glide.with(this).load(imagePath + dataProfile.profile_image)
//                .placeholder(R.mipmap.placeholder_subscription_square)
//                .into(mBinding.imageView)
//
//            mBinding.textView198.text = dataProfile.name
//            mBinding.customerNumber.text = Constant.formatPhoneNumber(dataProfile.contact_no)
            val dataProfile = intent.getParcelableExtra("profileData") as ModelGetProfile
            val modelScanCustomers = ModelScanCustomers()
            modelScanCustomers.name = dataProfile.name
            modelScanCustomers.contact_no = dataProfile.contact_no
            modelScanCustomers.profileImage = dataProfile.profile_image
            modelScanCustomers.customer_id = customerId
            arrayList.add(modelScanCustomers)
            teamSetAdapter.update(arrayList)
            customerId = ""

        }

    }

    private fun clickListeners() {
        mBinding.etSubsription.setOnClickListener {
            fragmentMenu = MenuFragmentShare(mViewModelCreate, mAdapterRedeem)
            fragmentMenu.show(supportFragmentManager, fragmentMenu.tag)
        }

    }

    private fun setSubscriptionData() {
        if (intent.hasExtra("model")) {
            val model = intent.getParcelableExtra<ModelSellerSubscriptions>("model")
            mBinding.subView.visibility = View.GONE
            mBinding.etSubsription.visibility = View.VISIBLE
            mBinding.name.text = model.campaign_name
            val priceDiscounted = Constant.getCalculatedPrice(model.discount_price, model.price)
            if (priceDiscounted.contains(".")){
                val splitPos = priceDiscounted.split(".")
                if (splitPos[1] == "00" || splitPos[1] == "0") {
                    mBinding.cost.text  = model.unit + " for $" + splitPos[0]
                }else{
                    mBinding.cost.text = model.unit + " for $" + BigDecimal(priceDiscounted).setScale(2, RoundingMode.HALF_UP).toString()
                }
            }else{
                mBinding.cost.text =  model.unit + " for $" + BigDecimal(priceDiscounted).setScale(2, RoundingMode.HALF_UP).toString()

            }

            mBinding.tvLogoNum.text = model.quantity
            val imagePath = Constant.getPrefs(this).getString(Constant.dataImagePath, "")
            Glide.with(this).load(imagePath + model.campaign_image).into(mBinding.logo)
        } else if (intent.hasExtra("subscriptionData") && intent.hasExtra("subsDataPricing")) {
            val myPrefs = getSharedPreferences("data", Context.MODE_PRIVATE)
            val gson = Gson()
            val getBasic = myPrefs.getString("basicData", "")
            val basicData = gson.fromJson(getBasic, BasicModel::class.java)
            mBinding.subView.visibility = View.GONE
            mBinding.etSubsription.visibility = View.VISIBLE
            mBinding.name.text = basicData.name
            val priceDiscounted = Constant.getCalculatedPrice(basicData.subscriptionArray[0].discountPrice, basicData.subscriptionArray[0].price)
            if (priceDiscounted.contains(".")){
                val splitPos = priceDiscounted.split(".")
                if (splitPos[1] == "00" || splitPos[1] == "0") {
                    mBinding.cost.text  = basicData.productInfo[0].name + " for $" + splitPos[0]
                }else{
                    mBinding.cost.text =basicData.productInfo[0].name + " for $" + BigDecimal(priceDiscounted).setScale(2, RoundingMode.HALF_UP).toString()
                }
            }else{
                mBinding.cost.text = basicData.productInfo[0].name + " for $" + BigDecimal(priceDiscounted).setScale(2, RoundingMode.HALF_UP).toString()

            }
            mBinding.tvLogoNum.text = basicData.subscriptionArray[0].redeemtion_cycle_qty
            val imagePath = Constant.getPrefs(this).getString(Constant.dataImagePath, "")
            Glide.with(this).load(imagePath + basicData.feature_image).into(mBinding.logo)
        } else {
            val model = intent.getParcelableExtra<SubscriptionListModel>("model1")
            mBinding.subView.visibility = View.GONE
            mBinding.etSubsription.visibility = View.VISIBLE
            mBinding.name.text = model.campaign_name
            val priceDiscounted = Constant.getCalculatedPrice(model.discount_price, model.price)
            if (priceDiscounted.contains(".")){
                val splitPos = priceDiscounted.split(".")
                if (splitPos[1] == "00" || splitPos[1] == "0") {
                    mBinding.cost.text  = model.unit  + " for $" + splitPos[0]
                }else{
                    mBinding.cost.text =model.unit  + " for $" + BigDecimal(priceDiscounted).setScale(2, RoundingMode.HALF_UP).toString()
                }
            }else{
                mBinding.cost.text =model.unit  + " for $" + BigDecimal(priceDiscounted).setScale(2, RoundingMode.HALF_UP).toString()

            }
            mBinding.tvLogoNum.text = model.redeemtion_cycle_qty
            val imagePath = Constant.getPrefs(this).getString(Constant.dataImagePath, "")
            Glide.with(this).load(imagePath + model.campaign_image).into(mBinding.logo)
        }

    }

    private fun observerInit() {

        mViewModel.getStatus().observe(this, Observer {
            progressBar.visibility = View.GONE
            if (it.msg == "Invalid auth code") {
                Constant.commonAlert(this)
            } else {
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
            }

        })


        mViewModel.getmDataInvitedCustomers().observe(this, Observer {
            progressBar.visibility = View.GONE
            if (it.status1 == "true") {
                startActivity(
                    Intent(this, InviteSendSuccessfully::class.java)
                        .putExtra("swipe", "other")
                        .putExtra("subscriptionList", it)
                        .putExtra("arrayList", arrayList)
                )
                finish()
            }
        })
    }

    override fun deleteAt(position: Int) {
        arrayList.removeAt(position)
        teamSetAdapter.notifyDataSetChanged()
        if (arrayList.size > 3) {
            mBinding.viewAll.visibility = View.VISIBLE
        } else {
            mBinding.viewAll.visibility = View.GONE
        }

    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        setIntent(intent)
    }


    private fun setAdapter() {
        mBinding.recyclerCustomers.visibility = View.VISIBLE
        mBinding.recyclerCustomers.layoutManager = LinearLayoutManager(this)
        mBinding.recyclerCustomers.isNestedScrollingEnabled = false
        teamSetAdapter = ScanCustomerAdapterForList(this)
        mBinding.recyclerCustomers.adapter = teamSetAdapter


    }

    fun name(v: View) {

//        val phonebookIntent = Intent("intent.action.INTERACTION_TOPMENU")
//        phonebookIntent.putExtra("additional", "phone-multi")
//        phonebookIntent.putExtra("maxRecipientCount", 1000)
//        phonebookIntent.putExtra("FromMMS", true)
//        startActivityForResult(phonebookIntent, PICK_CONTACT)

        if (ActivityCompat.checkSelfPermission(
                this,
                android.Manifest.permission.READ_CONTACTS
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(android.Manifest.permission.READ_CONTACTS), 120
            )
            return
        }

        //pickcontact()
        MultiContactPicker.Builder(this) //Activity/fragment context
            .theme(R.style.MyCustomPickerTheme) //Optional - default: MultiContactPicker.Azure
            .hideScrollbar(false) //Optional - default: false
            .showTrack(true) //Optional - default: true
            .searchIconColor(Color.WHITE) //Option - default: White
            .setChoiceMode(MultiContactPicker.CHOICE_MODE_MULTIPLE) //Optional - default: CHOICE_MODE_MULTIPLE
            .handleColor(
                ContextCompat.getColor(
                    this,
                    R.color.colorPrimary
                )
            ) //Optional - default: Azure Blue
            .bubbleColor(
                ContextCompat.getColor(
                    this,
                    R.color.colorPrimary
                )
            ) //Optional - default: Azure Blue
            .bubbleTextColor(Color.WHITE) //Optional - default: White
            .setTitleText("Select Contacts") //Optional - default: Select Contacts
            //.setSelectedContacts("10", "5" / myList) //Optional - will pre-select contacts of your choice. String... or List<ContactResult>
            .setLoadingType(MultiContactPicker.LOAD_ASYNC) //Optional - default LOAD_ASYNC (wait till all loaded vs stream results)
            .limitToColumn(LimitColumn.NONE) //Optional - default NONE (Include phone + email, limiting to one can improve loading time)
            .setActivityAnimations(
                android.R.anim.fade_in, android.R.anim.fade_out,
                android.R.anim.fade_in,
                android.R.anim.fade_out
            ) //Optional - default: No animation overrides
            .showPickerForResult(PICK_CONTACT);
    }

    fun pickcontact() {
        try {
            val phonebookintent = Intent("intent.action.INTERACTION_TOPMENU")
            phonebookintent.putExtra("additional", "phone-multi")
            startActivityForResult(phonebookintent, PICK_CONTACT)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun onClick() {
        mBinding.PhoneNumber.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                flag = if (s.length == 10) {
                    1
                } else {
                    0
                }
            }

            override fun afterTextChanged(s: Editable) {
            }
        })


        var subscriptionId = ""

        mBinding.invite.setOnClickListener {
            if (intent.hasExtra("toolbar")) {
                subscriptionId = subscriptionListId
            } else if (intent.hasExtra("fromCustomers")) {
                subscriptionId = subscriptionProfileId
            } else if (intent.hasExtra("model")) {
                val model = intent.getParcelableExtra<ModelSellerSubscriptions>("model")
                subscriptionId = model.subscription_id
            } else if (intent.hasExtra("subscriptionData") && intent.hasExtra("subsDataPricing")) {
                val myPrefs = getSharedPreferences("data", Context.MODE_PRIVATE)
                val gson = Gson()
                val getBasic = myPrefs.getString("basicData", "")
                val basicData = gson.fromJson(getBasic, BasicModel::class.java)
                subscriptionId = basicData.subscriptionArray[0].subscription_id

            } else {
                val model1 = intent.getParcelableExtra<SubscriptionListModel>("model1")
                subscriptionId = model1.subscription_id
            }

            val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
            for (model in arrayList) {
                customerId = if (customerId.isEmpty()) {
                    model.customer_id
                } else {
                    "$customerId,${model.customer_id}"
                }
            }


            if (intent.hasExtra("toolbar")) {
                if (arrayList.size == 0 && mBinding.etPhone.text.isEmpty()) {
                    Toast.makeText(
                        this,
                        "Please select customer first to invite customer",
                        Toast.LENGTH_SHORT
                    ).show()
//                    if (results.size == 0) {
//                        Toast.makeText(
//                            this,
//                            "Please select customer first to invite customer",
//                            Toast.LENGTH_SHORT
//                        ).show()
//                    } else {
//                        val jsonArray = JSONArray()
//                        for (items in results) {
//                            val jsonObject = JSONObject()
//                            jsonObject.put("name", items.displayName)
//                            jsonObject.put("contact_no", items.phoneNumbers[0].number)
//                            jsonArray.put(jsonObject)
//                        }
//                        mViewModel.getInviteCustomer(
//                            auth,
//                            customerId,
//                            subscriptionId,
//                            jsonArray.toString()
//                        )
//                    }

                } else if (subscriptionId.isNullOrEmpty()) {
                    Toast.makeText(
                            this,
                            "Please select subscription first to invite customer",
                            Toast.LENGTH_SHORT
                        )
                        .show()
                }

//                else if (mBinding.etPhone.text.isNotEmpty()) {
//                    if (flag == 0) {
//                        Toast.makeText(
//                            this,
//                            "Please enter 10 digit phone number",
//                            Toast.LENGTH_LONG
//                        ).show()
//                    } else {
//
//                        val jsonArray = JSONArray()
//                        val jsonObject1 = JSONObject()
//                        jsonObject1.put("contact_no", mBinding.etPhone.text.toString().trim())
//                        jsonObject1.put("name", mBinding.PhoneNumber.text.toString().trim())
//                        jsonArray.put(jsonObject1)
////                        for (items in results) {
////                            val jsonObject = JSONObject()
////                            jsonObject.put("name", items.displayName)
////                            jsonObject.put("contact_no", items.phoneNumbers[0].number)
////                            jsonArray.put(jsonObject)
////                        }
//
//                        mViewModel.getInviteCustomer(
//                            auth,
//                            customerId,
//                            subscriptionId,
//                            jsonArray.toString()
//
//                        )
//                    }
//                }

                else {
                    progressBar.visibility = View.VISIBLE
                    val jsonArray = JSONArray()
//                    for (items in results) {
//                        val jsonObject = JSONObject()
//                        jsonObject.put("name", items.displayName)
//                        jsonObject.put("contact_no", items.phoneNumbers[0].number)
//                        jsonArray.put(jsonObject)
//                    }
                    mViewModel.getInviteCustomer(
                        auth,
                        customerId,
                        subscriptionId,
                        jsonArray.toString()
                    )
                }

            } else if (intent.hasExtra("fromCustomers")) {
                if ((arrayList.size == 0 && mBinding.textView198.text.isEmpty()) || customerId.isEmpty()) {

                    Toast.makeText(
                        this,
                        "Please select customer first to invite customer",
                        Toast.LENGTH_SHORT
                    ).show()
//                    if (results.size == 0) {
//                        Toast.makeText(
//                            this,
//                            "Please select customer first to invite customer",
//                            Toast.LENGTH_SHORT
//                        ).show()
//                    } else {
//                        val jsonArray = JSONArray()
//                        for (items in results) {
//                            val jsonObject = JSONObject()
//                            jsonObject.put("name", items.displayName)
//                            jsonObject.put("contact_no", items.phoneNumbers[0].number)
//                            jsonArray.put(jsonObject)
//                        }
//                        mViewModel.getInviteCustomer(
//                            auth,
//                            customerId,
//                            subscriptionId,
//                            jsonArray.toString()
//                        )
//                    }
                } else if (subscriptionId.isNullOrEmpty()) {
                    Toast.makeText(
                            this,
                            "Please select subscription first to invite customer",
                            Toast.LENGTH_SHORT
                        )
                        .show()
                }

//                else if (mBinding.etPhone.text.isNotEmpty()) {
//                    if (flag == 0) {
//                        Toast.makeText(
//                            this,
//                            "Please enter 10 digit phone number",
//                            Toast.LENGTH_LONG
//                        ).show()
//                    } else {
//                        val jsonArray = JSONArray()
//                        val jsonObject1 = JSONObject()
//                        jsonObject1.put("contact_no", mBinding.etPhone.text.toString().trim())
//                        jsonObject1.put("name", mBinding.PhoneNumber.text.toString().trim())
//                        jsonArray.put(jsonObject1)
////                        for (items in results) {
////                            val jsonObject = JSONObject()
////                            jsonObject.put("name", items.displayName)
////                            jsonObject.put("contact_no", items.phoneNumbers[0].number)
////                            jsonArray.put(jsonObject)
////                        }
//
//                        mViewModel.getInviteCustomer(
//                            auth,
//                            customerId,
//                            subscriptionId,
//                            jsonArray.toString()
//
//                        )
//                    }
//
//                }

                else {
                    progressBar.visibility = View.VISIBLE
                    val jsonArray = JSONArray()
//                    for (items in results) {
//                        val jsonObject = JSONObject()
//                        jsonObject.put("name", items.displayName)
//                        jsonObject.put("contact_no", items.phoneNumbers[0].number)
//                        jsonArray.put(jsonObject)
//                    }
                    mViewModel.getInviteCustomer(
                        auth,
                        customerId,
                        subscriptionId,
                        jsonArray.toString()
                    )
                }
            } else {
                if (arrayList.size == 0 && mBinding.etPhone.text.isEmpty()) {
                    Toast.makeText(
                        this,
                        "Please select customer first to invite customer",
                        Toast.LENGTH_SHORT
                    ).show()
//                    if (results.size == 0) {
//                        Toast.makeText(
//                            this,
//                            "Please select customer first to invite customer",
//                            Toast.LENGTH_SHORT
//                        ).show()
//                    } else {
//                        val jsonArray = JSONArray()
//                        for (items in results) {
//                            val jsonObject = JSONObject()
//                            jsonObject.put("name", items.displayName)
//                            jsonObject.put("contact_no", items.phoneNumbers[0].number)
//                            jsonArray.put(jsonObject)
//                        }
//                        mViewModel.getInviteCustomer(
//                            auth,
//                            customerId,
//                            subscriptionId,
//                            jsonArray.toString()
//                        )
//                    }
                } else if (subscriptionId.isNullOrEmpty()) {
                    Toast.makeText(
                            this,
                            "Please select subscription first to invite customer",
                            Toast.LENGTH_SHORT
                        )
                        .show()
                }

//                else if (mBinding.etPhone.text.isNotEmpty()) {
//                    if (flag == 0) {
//                        Toast.makeText(
//                            this,
//                            "Please enter 10 digit phone number",
//                            Toast.LENGTH_LONG
//                        ).show()
//                    } else {
//
//                        val jsonArray = JSONArray()
//                        val jsonObject1 = JSONObject()
//                        jsonObject1.put("contact_no", mBinding.etPhone.text.toString().trim())
//                        jsonObject1.put("name", mBinding.PhoneNumber.text.toString().trim())
//                        jsonArray.put(jsonObject1)
////                        for (items in results) {
////                            val jsonObject = JSONObject()
////                            jsonObject.put("name", items.displayName)
////                            jsonObject.put("contact_no", items.phoneNumbers[0].number)
////                            jsonArray.put(jsonObject)
////                        }
//
//                        mViewModel.getInviteCustomer(
//                            auth,
//                            customerId,
//                            subscriptionId,
//                            jsonArray.toString()
//
//                        )
//                    }
//                }

                else {
                    progressBar.visibility = View.VISIBLE
                    val jsonArray = JSONArray()
//                    for (items in results) {
//                        val jsonObject = JSONObject()
//                        jsonObject.put("name", items.displayName)
//                        jsonObject.put("contact_no", items.phoneNumbers[0].number)
//                        jsonArray.put(jsonObject)
//                    }
                    mViewModel.getInviteCustomer(
                        auth,
                        customerId,
                        subscriptionId,
                        jsonArray.toString()
                    )
                }
            }

        }



        mBinding.blueTick.setOnClickListener {
            mBinding.constraint.visibility = View.GONE
            customerId = ""
            isVisiblee = false
        }



        mBinding.viewAll.setOnClickListener {
            Constant.hideKeyboard(this, mBinding.etChooseCustomer)
            startActivityForResult(
                Intent(this, ScanCustomersActivity::class.java).putExtra(
                        "invite",
                        "other"
                    ).putExtra("list", "other")
                    .putExtra("selectedlist", arrayList), 10
            )
            /*startActivity(
                Intent(this, ScanCustomersActivity::class.java).putExtra(
                    "selectedlist",
                    arrayList
                ).putExtra("list", "other")
            )*/
        }

        mBinding.etChooseCustomer.setOnClickListener {
            Constant.hideKeyboard(this, mBinding.etChooseCustomer)
            startActivityForResult(
                Intent(this, ScanCustomersActivity::class.java).putExtra(
                        "invite",
                        "other"
                    ).putExtra("list", "other")
                    .putExtra("selectedlist", arrayList), 10
            )

        }

//        mBinding.switchUsers.setOnCheckedChangeListener { p0, p1 ->
//            if (p1) {
//                mBinding.textView345.visibility = View.VISIBLE
//                mBinding.etPhone.isEnabled = false
//                mBinding.etPhone.setBackgroundResource(R.drawable.login_background_opacity1)
//                mBinding.PhoneNumber.setBackgroundResource(R.drawable.login_background_opacity1)
//                mBinding.PhoneNumber.setTextColor(resources.getColor(R.color.colorHeading_opacity))
//                mBinding.etPhone.setTextColor(resources.getColor(R.color.colorHeading_opacity))
//                mBinding.etPhone.setCompoundDrawablesWithIntrinsicBounds(
//                    0,
//                    0,
//                    R.drawable.arrow_opacity,
//                    0
//                )
//                mBinding.PhoneNumber.setCompoundDrawablesWithIntrinsicBounds(
//                    0,
//                    0,
//                    R.drawable.arrow_opacity,
//                    0
//                )
//                mBinding.PhoneNumber.isEnabled = false
//                mBinding
//
//            } else {
//                mBinding.textView345.visibility = View.INVISIBLE
//                mBinding.etPhone.setBackgroundResource(R.drawable.login_background)
//                mBinding.PhoneNumber.setBackgroundResource(R.drawable.login_background)
//                mBinding.etPhone.setCompoundDrawablesWithIntrinsicBounds(
//                    0,
//                    0,
//                    R.drawable.ic_arrow_right,
//                    0
//                )
//                mBinding.PhoneNumber.setCompoundDrawablesWithIntrinsicBounds(
//                    0,
//                    0,
//                    R.drawable.ic_arrow_right,
//                    0
//                )
//                mBinding.etPhone.isEnabled = true
//                mBinding.PhoneNumber.isEnabled = true
//            }
//
//        }

        mBinding.shareText.setOnClickListener {

            generateLink()

        }

        mBinding.copyText.setOnClickListener {
            Toast.makeText(this, "Copied", Toast.LENGTH_SHORT).show()
        }
    }


    fun itemsSubs(v: View) {
        fragmentMenu = MenuFragmentShare(mViewModelCreate, mAdapterRedeem)
        fragmentMenu.show(supportFragmentManager, fragmentMenu.tag)
    }

    class MenuFragmentShare(
        var mViewModelCreate: SubscriptionSellerViewModel,
        var mAdapterRedeem: AdapterShareRedeem
    ) : RoundedBottomSheetDialogFragment() {

        lateinit var rvRedeem: RecyclerView
        lateinit var cl_redeem: ConstraintLayout
        lateinit var image: ImageView
        lateinit var noData: TextView
        lateinit var imageView82: ImageView
        lateinit var txtHeading: TextView
        lateinit var txtHeading1: TextView
        lateinit var inviteCode: TextView
        lateinit var tvNoDataDescription: TextView
        lateinit var progressBar: LinearLayout

        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val view = inflater.inflate(R.layout.redeem_sheet, container, false)
            rvRedeem = view.findViewById(R.id.rvRedeem)
            image = view.findViewById(R.id.topBg)
            cl_redeem = view.findViewById(R.id.cl_redeem)
            progressBar = view.findViewById(R.id.progressBar)
            txtHeading = view.findViewById(R.id.cancel)
            txtHeading1 = view.findViewById(R.id.cancel1)
            tvNoDataDescription = view.findViewById(R.id.cancel)
            noData = view.findViewById(R.id.tvNoData)
            inviteCode = view.findViewById(R.id.inviteCode)
            imageView82 = view.findViewById(R.id.imageView82)
            txtHeading.visibility = View.GONE
            inviteCode.visibility = View.GONE
            txtHeading1.visibility = View.VISIBLE
            image.setImageResource(R.mipmap.bg_popup)
            setAdapter()
            apiImplimentation()
            observerInit()
            roundedImage()
            return view
        }

        private fun observerInit() {
            mViewModelCreate.getmDataCampaignsActiveOnly().observe(this, Observer {
                if (it.size > 0) {
                    progressBar.visibility = View.VISIBLE
                    var campaignsId = ""
                    for (model in it) {
                        if (model.is_public == "0" && model.status1 == "1") {
                            campaignsId = if (campaignsId.isEmpty()) {
                                model.campaign_id
                            } else {
                                "$campaignsId, ${model.campaign_id}"
                            }
                        }
                    }

                    mViewModelCreate.getCampaignSubscriptions(campaignsId)

                }

            })


            mViewModelCreate.getStatus().observe(this, Observer {
                progressBar.visibility = View.GONE
                when (it.msg) {
                    "Invalid auth code" -> Constant.commonAlert(activity!!)
                    "campaign_id is missing or invalid" -> noData.visibility =
                        View.VISIBLE
                    else -> Toast.makeText(activity, it.msg, Toast.LENGTH_SHORT).show()
                }
            })


            mViewModelCreate.getSubsList().observe(this, Observer {
                if (it.size > 0) {
                    noData.visibility = View.GONE
                    tvNoDataDescription.visibility = View.GONE
                    imageView82.visibility = View.GONE
                } else {
                    noData.visibility = View.VISIBLE
                    tvNoDataDescription.visibility = View.VISIBLE
                    imageView82.visibility = View.VISIBLE
                }
                progressBar.visibility = View.GONE
                mAdapterRedeem.update(it)

            })

        }

        private fun apiImplimentation() {
            val auth = Constant.getPrefs(activity!!).getString(Constant.auth_code, "")
            mViewModelCreate.getActiveSubsOnly(auth)
            progressBar.visibility = View.VISIBLE

        }


        private fun setAdapter() {
            rvRedeem.layoutManager = LinearLayoutManager(activity)
            mAdapterRedeem = AdapterShareRedeem(this.activity!!)
            rvRedeem.adapter = mAdapterRedeem
        }


        private fun roundedImage() {
            val curveRadius = 50F

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                image.outlineProvider = object : ViewOutlineProvider() {

                    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                    override fun getOutline(view: View?, outline: Outline?) {
                        outline?.setRoundRect(
                            0,
                            0,
                            view!!.width,
                            view.height + curveRadius.toInt(),
                            curveRadius
                        )
                    }
                }
                image.clipToOutline = true
            }
        }
    }

    override fun dismissSheet() {
        if (fragmentMenu != null) {
            fragmentMenu.dismiss()
        }

    }

    override fun setData(position: Int, model: ModelShareRedeem) {
        val imagePath = Constant.getPrefs(this).getString(Constant.dataImagePath, "")
        Glide.with(this).load(imagePath + model.campaign_image)
            .placeholder(R.mipmap.placeholder_subscription_square)
            .into(mBinding.logo)
        mBinding.subView.visibility = View.GONE
        mBinding.etSubsription.visibility = View.VISIBLE
        mBinding.tvLogoNum.text = model.redeemtion_cycle_qty
        mBinding.name.text = model.campaign_name
        val priceDiscounted = Constant.getCalculatedPrice(model.discount_price, model.price)
        if (priceDiscounted.contains(".")){
            val splitPos = priceDiscounted.split(".")
            if (splitPos[1] == "00" || splitPos[1] == "0") {
                mBinding.cost.text  =  model.unit + " for $" + splitPos[0]
            }else{
                mBinding.cost.text = model.unit + " for $" + BigDecimal(priceDiscounted).setScale(2, RoundingMode.HALF_UP).toString()
            }
        }else{
            mBinding.cost.text = model.unit + " for $" + BigDecimal(priceDiscounted).setScale(2, RoundingMode.HALF_UP).toString()

        }
        subscriptionListId = model.subscription_id

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        try {
            if (requestCode == 10 && resultCode == Activity.RESULT_OK) {
                arrayList = ArrayList()
                arrayList = data!!.getParcelableArrayListExtra("model1")
                teamSetAdapter.update(arrayList)
                if (arrayList.size > 3) {
                    mBinding.viewAll.visibility = View.VISIBLE
                } else {
                    mBinding.viewAll.visibility = View.GONE
                }

            }

            if (requestCode == PICK_CONTACT || resultCode == Activity.RESULT_OK) {
                results = MultiContactPicker.obtainResult(data)
                var allPhones = ArrayList<String>()
                mBinding.recyclerContacts.visibility = View.VISIBLE
                mBinding.recyclerContacts.layoutManager = LinearLayoutManager(this)
                mBinding.recyclerContacts.isNestedScrollingEnabled = false
                mContactAdapter = ContactListAdapter(results, this)
                mBinding.recyclerContacts.adapter = mContactAdapter


            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun setToolbar() {
        setSupportActionBar(mBinding.toolbarInvite)
        title = ""
//        mBinding.title.text = "Invite"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun generateLink() {

        val linkGenerator = ShareInviteHelper.generateInviteUrl(this)
        linkGenerator.channel = "Skype"
        linkGenerator.addParameter("af_cost_value", "2.5")
        linkGenerator.addParameter("af_cost_currency", "USD")

        val listener = object : CreateOneLinkHttpTask.ResponseListener {
            override fun onResponse(s: String?) {
                val share = Intent(Intent.ACTION_SEND)
                share.type = "text/plain"
                share.putExtra(
                    Intent.EXTRA_TEXT,
                    "Come abroad Upscribbr and experience this amazing subscription at a very attractive price. $s"
                )
                startActivity(Intent.createChooser(share, "Share!"))
                Log.d("Invite Link", s)
            }

            override fun onResponseError(s: String?) {

                Log.d("Invite Link", s)

            }
        }
        linkGenerator.generateLink(this, listener)
        ShareInviteHelper.trackInvite(this, linkGenerator.channel, emptyMap());
    }

    private fun setConversationalData() {

        val conversionDataListener = object : AppsFlyerConversionListener {


            override fun onAppOpenAttribution(data: MutableMap<String, String>?) {
                data?.map {
                    Log.d("LOG_TAG", "onAppOpen_attribute: ${it.key} = ${it.value}")
                }
            }

            override fun onAttributionFailure(error: String?) {
                Log.e("LOG_TAG", "error onAttributionFailure :  $error")
            }

            override fun onInstallConversionDataLoaded(p0: MutableMap<String, String>?) {
                Log.e("LOG_TAG", "error onAttributionFailure :  $p0")


            }

            override fun onInstallConversionFailure(p0: String?) {
                Log.e("LOG_TAG", "error onAttributionFailure :  $p0")

            }
        }

        AppsFlyerLib.getInstance().init(devKey, conversionDataListener, applicationContext)
        AppsFlyerLib.getInstance().startTracking(application)

    }

    private fun getNameFromApp(
        packageManager: PackageManager?,
        shareIntent: Intent
    ): String {


        val resolveInfos: List<ResolveInfo> = packageManager!!.queryIntentActivities(shareIntent, 0)

        var lastDot = 0

        var name = ""

        for (element in resolveInfos) {

            // Extract the label, append it, and repackage it in a LabeledIntent
            val resolveInfo: ResolveInfo = element
            val packageName: String = resolveInfo.activityInfo.packageName
            lastDot = packageName.lastIndexOf(".")
            name = packageName.substring(lastDot + 1)
        }

        return name


    }

    fun setCustomerUserId(id: String?) {

    }

    override fun removeAtPosition(contacts: ArrayList<ContactResult>, position: Int) {

    }

//    class MenuFragmentCustomShare(var mAdapterCustomShare: AdapterCustomShare) : RoundedBottomSheetDialogFragment() {
//
//        override fun onCreateView(
//            inflater: LayoutInflater,
//            container: ViewGroup?,
//            savedInstanceState: Bundle?
//        ): View? {
//            val view = inflater.inflate(R.layout.custom_sharekit_sheet, container, false)
//
//            setShareAdapter()
//            return view
//        }
//
//
//        private fun setShareAdapter() {
//            rvCustomShare.layoutManager = LinearLayoutManager(activity)
//            mAdapterCustomShare = AdapterCustomShare(this.activity!!, customShareList())
//            rvCustomShare.adapter = mAdapterCustomShare
//        }
//
//        private fun customShareList() : ArrayList<CustomShareKitModel>{
//
//            val customShareArrayList = ArrayList<CustomShareKitModel>()
//
//            val shareModel = CustomShareKitModel()
//
//            shareModel.shareImageName = "Facebook"
//            shareModel.shareImage = R.drawable.ic_fb
//            customShareArrayList.add(shareModel)
//
//            shareModel.shareImageName = "WhatsApp"
//            shareModel.shareImage = R.drawable.ic_fb
//            customShareArrayList.add(shareModel)
//
//            shareModel.shareImageName = "Skype"
//            shareModel.shareImage = R.drawable.ic_fb
//            customShareArrayList.add(shareModel)
//
//            shareModel.shareImageName = "SMS"
//            shareModel.shareImage = R.drawable.ic_fb
//            customShareArrayList.add(shareModel)
//
//            shareModel.shareImageName = "Gmail"
//            shareModel.shareImage = R.drawable.ic_fb
//            customShareArrayList.add(shareModel)
//
//            return customShareArrayList
//
//
//        }
//
//    }
//
//    override fun getCustomShareClick(position: Int) {
//
//        when (position) {
//            0 -> {
//
//            }
//            1 -> {
//
//            }
//            2 -> {
//
//            }
//            else -> {
//
//            }
//        }
//
//    }
}


//                var projection2 = arrayOf(
//                    Contacts.People.NAME,
//                    Contacts.People.NUMBER
//                )
//
//                val bundle = data!!.extras
//                var result = bundle.getString("additional")
//                var result2 = bundle.getString("additional1")
//                val contacts = bundle.getStringArrayList("result")
//                val separated = contacts[0].split(";".toRegex())
//                var personUri = ContentUris.withAppendedId(
//                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
//                    separated[0].toLong()
//                );
//                var phonesUri = Uri.withAppendedPath(
//                    personUri,
//                    ContactsContract.CommonDataKinds.Phone.IN_DEFAULT_DIRECTORY
//                );
//                var proj = arrayOf(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)
//                var cursor2 = contentResolver.query(
//                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
//                    proj,
//                    ContactsContract.CommonDataKinds.Phone.NUMBER + " = " + separated[1],
//                    null, null
//                );
//                var contacts3 = ArrayList<String>();
//                var name2 =
//                    cursor2.getString(cursor2.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
//                if (cursor2.moveToFirst()) {
//                    // Iterate through the cursor
//                    do {
//                        // Get the contacts name
//                        var name =
//                            cursor2.getString(cursor2.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
//                        contacts3.add(name);
//                    } while (cursor2.moveToNext());
//                }
//                // Close the curosor
//                cursor2.close()
//
//                var allPhones = ArrayList<String>();
//// The Phone class should be imported from CommonDataKinds.Phone
//                var cursor1 = getContentResolver().query(
//                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
//                    arrayOf(
//                        ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
//                        ContactsContract.CommonDataKinds.Phone.NUMBER
//                    ),
//                    ContactsContract.CommonDataKinds.Phone.IN_VISIBLE_GROUP + "=1",
//                    null,
//                    ContactsContract.CommonDataKinds.Phone.TIMES_CONTACTED + " DESC"
//                );
//                while (cursor1 != null && cursor1.moveToNext()) {
//                    var name = cursor1.getString(0);
//                    var number = cursor1.getString(1);
//                    allPhones.add(name + " - " + number);
//                }
//
//                val contactData = data.data
//                val projection = arrayOf(
//                    ContactsContract.CommonDataKinds.Phone.NUMBER,
//                    ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME
//                )
//                val cursor = contentResolver.query(contactData, projection, null, null, null)
//                cursor!!.moveToFirst()
//                val numberColumn =
//                    cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)
//                val nameColumn =
//                    cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME)
////                val number = cursor.getString(numberColumn)
////                val name = cursor.getString(nameColumn)
//
//
//                val names = ArrayList<String>()
//                while (!cursor.isAfterLast) {
////                    names.add(cursor.getString(numberColumn))
//                    names.add(cursor.getString(nameColumn))
//                    cursor.moveToNext()
//                }
//                cursor.close()
