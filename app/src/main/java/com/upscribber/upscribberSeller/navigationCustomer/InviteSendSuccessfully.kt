package com.upscribber.upscribberSeller.navigationCustomer

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.TypedValue
import android.view.View
import androidx.databinding.DataBindingUtil
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.databinding.ActivityInviteSendSuccessfullyBinding
import com.upscribber.upscribberSeller.customerBottom.scanning.customerlistselection.ModelScanCustomers
import com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.subscription.ModelSubscriptionCard
import me.relex.circleindicator.Config

class InviteSendSuccessfully : AppCompatActivity() {

    lateinit var mBinding: ActivityInviteSendSuccessfullyBinding
    private lateinit var mAdapter: SuccessPagerAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_invite_send_successfully)

        if (intent.hasExtra("swipe")){
            mBinding.circleIndicator.visibility = View.GONE
        }

        mBinding.doneSharing.setOnClickListener {
            if (intent.hasExtra("fromCustomers") && intent.hasExtra("toolbar")) {
                finish()

            } else {
                val sharedPreferences = Constant.getPrefs(this)
                val editor = sharedPreferences.edit()
                editor.putString("list", "back")
                editor.apply()
                finish()
            }
        }
        setAdapter()
        setData()
    }

    private fun setData() {
        val subscriptionListData = intent.getParcelableExtra<ModelSubscriptionCard>("subscriptionList")
        val arrayList = intent.getParcelableArrayListExtra<ModelScanCustomers>("arrayList")
        mAdapter.update(subscriptionListData,arrayList)

    }

    private fun setAdapter() {
        mAdapter = SuccessPagerAdapter(supportFragmentManager)
        mBinding.pager.adapter = mAdapter
        mBinding.pager.offscreenPageLimit = 2

        val indicatorWidth = (TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, 9.5f,
            resources.displayMetrics
        ) + 0.5f).toInt()
        val indicatorHeight = (TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, 2f,
            resources.displayMetrics
        ) + 0.5f).toInt()
        val indicatorMargin = (TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, 6f,
            resources.displayMetrics
        ) + 0.5f).toInt()

        val config = Config.Builder().width(indicatorWidth)
            .height(indicatorHeight)
            .margin(indicatorMargin)
            .drawable(R.drawable.bg_seller_button_blue)
            .build()

        mBinding.circleIndicator.initialize(config)
        mBinding.pager.adapter = mAdapter
        mBinding.circleIndicator.setViewPager(mBinding.pager)

    }
}
