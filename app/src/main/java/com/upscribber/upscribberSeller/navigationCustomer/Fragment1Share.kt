package com.upscribber.upscribberSeller.navigationCustomer

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.upscribber.R
import com.upscribber.databinding.FragmentShare1Binding

class Fragment1Share : Fragment() {

    lateinit var mBinding: FragmentShare1Binding

    override fun onCreateView(
        inflater: LayoutInflater, container:
        ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_share1, container, false)
        return mBinding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        clickListeners()
    }

    private fun clickListeners() {
        mBinding.copyText.setOnClickListener {
            Toast.makeText(context, "Copied", Toast.LENGTH_SHORT).show()
        }

        mBinding.textView313.setOnClickListener {
            val message =
                "Come abroad Upscribbr and experience this amazing subscription at a very attractive price.\nhttps://www.Upscribbr.com/sharablelink.php"
            val share = Intent(Intent.ACTION_SEND)
            share.type = "text/plain"
            share.putExtra(Intent.EXTRA_TEXT, message)
            startActivity(Intent.createChooser(share, "Share!"))
        }

        mBinding.textView314.setOnClickListener {
            val message =
                "Come abroad Upscribbr and experience this amazing subscription at a very attractive price.\nhttps://www.Upscribbr.com/sharablelink.php"
            val share = Intent(Intent.ACTION_SEND)
            share.type = "text/plain"
            share.putExtra(Intent.EXTRA_TEXT, message)
            startActivity(Intent.createChooser(share, "Share!"))
        }

        mBinding.textView317.setOnClickListener {
            val message =
                "Come abroad Upscribbr and experience this amazing subscription at a very attractive price.\nhttps://www.Upscribbr.com/sharablelink.php"
            val share = Intent(Intent.ACTION_SEND)
            share.type = "text/plain"
            share.putExtra(Intent.EXTRA_TEXT, message)
            startActivity(Intent.createChooser(share, "Share!"))
        }

        mBinding.textView316.setOnClickListener {
            val message =
                "Come abroad Upscribbr and experience this amazing subscription at a very attractive price.\nhttps://www.Upscribbr.com/sharablelink.php"
            val share = Intent(Intent.ACTION_SEND)
            share.type = "text/plain"
            share.putExtra(Intent.EXTRA_TEXT, message)
            startActivity(Intent.createChooser(share, "Share!"))
        }

    }

}
