package com.upscribber.upscribberSeller.navigationCustomer

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.upscribber.upscribberSeller.customerBottom.scanning.customerlistselection.ModelScanCustomers
import com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.subscription.ModelSubscriptionCard
import java.util.ArrayList

@Suppress("UNREACHABLE_CODE")
class SuccessPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    var fr1 = Fragment1Share()
    var fr2 = Fragment2Share()
    var data = ModelSubscriptionCard()
    var dataArray =ArrayList<ModelScanCustomers>()

    override fun getItem(position: Int): Fragment {
        return if (position == 0){
            val bundle = Bundle()
            bundle.putParcelable("model",data)
            bundle.putParcelableArrayList("dataArray",dataArray)
            fr2.arguments=bundle
            fr2
        }else {
            fr1
        }
        return null!!
    }



    override fun getCount(): Int {
        return if (data.invited_customers <= "1"){
            1
        }else {
            2
        }
    }

    fun update(
        subscriptionListData: ModelSubscriptionCard,
        arrayList: ArrayList<ModelScanCustomers>
    ) {
        data = subscriptionListData
        dataArray = arrayList
        notifyDataSetChanged()

    }
}