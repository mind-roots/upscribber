package com.upscribber.upscribberSeller.navigationCustomer

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.databinding.SetCustomerListBinding
import com.upscribber.upscribberSeller.customerBottom.scanning.customerlistselection.ModelScanCustomers
import kotlinx.android.synthetic.main.set_customer_list.view.*

class ScanCustomerAdapterForList (var context: Context): RecyclerView.Adapter<ScanCustomerAdapterForList.MyViewHolder>() {

    private lateinit var mBinding: SetCustomerListBinding
    private  var items = ArrayList<ModelScanCustomers>()
    var listener = context as DeleteList

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        mBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.set_customer_list, parent, false)
        return MyViewHolder(mBinding)

    }

    override fun getItemCount(): Int {
        if (items.size > 3){
            return 3
        }
       return items.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = items[position]
        holder.bind(model)

        holder.itemView.customerNumber.text = Constant.formatPhoneNumber(
            model.contact_no
        )

        holder.itemView.blueTick.setOnClickListener {
            listener.deleteAt(position)
        }
    }

    class MyViewHolder(var  binding: SetCustomerListBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(obj: ModelScanCustomers) {
            binding.model1 = obj
            binding.executePendingBindings()
        }
    }

    fun update(items: ArrayList<ModelScanCustomers>) {
        this.items = items
        notifyDataSetChanged()
    }

    interface DeleteList{
        fun deleteAt(position: Int)
    }

}