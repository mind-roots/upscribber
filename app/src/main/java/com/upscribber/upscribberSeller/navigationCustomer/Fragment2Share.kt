package com.upscribber.upscribberSeller.navigationCustomer

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.databinding.FragmentShare2Binding
import com.upscribber.upscribberSeller.customerBottom.scanning.customerlistselection.ModelScanCustomers
import com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.subscription.ModelSubscriptionCard

@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class Fragment2Share : Fragment() {

    lateinit var mBinding: FragmentShare2Binding

    override fun onCreateView(
        inflater: LayoutInflater, container:
        ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_share2, container, false)
        return mBinding.root
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val data = arguments!!.getParcelable<ModelSubscriptionCard>("model")
        val dataArray = arguments!!.getParcelableArrayList<ModelScanCustomers>("dataArray")
        mBinding.storeName.text = data.frequency_type

        val splitPos = data.price.split(".")
        if (splitPos[1] == "00" || splitPos[1] == "0") {
            mBinding.amount.text = "$" + splitPos[0]
        } else {
            mBinding.amount.text = "$" + data.price
        }

        mBinding.textView336.text = data.campaign_name

        if (data.redeemtion_cycle_qty == "500000") {
            mBinding.service.text = "Unlimited " + data.unit
        } else {
            mBinding.service.text = data.redeemtion_cycle_qty + " " + data.unit
        }

        if (dataArray.size <= 1) {
            mBinding.textView.visibility = View.GONE
            mBinding.customerName.visibility = View.VISIBLE
            mBinding.customerNameField.text = dataArray[0].name
            mBinding.phoneNumberTxt.text = Constant.formatPhoneNumber(dataArray[0].contact_no)
            mBinding.customerNameField.visibility = View.VISIBLE
            mBinding.phoneNumber.visibility = View.VISIBLE
            mBinding.phoneNumberTxt.visibility = View.VISIBLE

        } else {
            mBinding.textView.visibility = View.VISIBLE
            mBinding.textView.text = data.invited_customers + " Customers Invited"
            mBinding.customerName.visibility = View.GONE
            mBinding.customerNameField.visibility = View.GONE
            mBinding.phoneNumber.visibility = View.GONE
            mBinding.phoneNumberTxt.visibility = View.GONE
        }


    }
}
