package com.upscribber.upscribberSeller.navigationWallet

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.Outline
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.*
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.upscribber.upscribberSeller.dashboardfragment.viewpagerFragments.wallet.PaymentHistoryAdapter
import com.google.android.material.appbar.CollapsingToolbarLayout
import androidx.core.content.ContextCompat
import android.view.WindowManager
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.upscribber.commonClasses.Constant
import com.upscribber.payment.paymentCards.AddCardActivity
import com.upscribber.commonClasses.RoundedBottomSheetDialogFragment
import com.upscribber.upscribberSeller.payments.bankAccount.AddBankAccountActivity
import com.upscribber.R
import com.upscribber.commonClasses.CloseListPagination
import com.upscribber.payment.PaymentTransModel
import com.upscribber.payment.customerPayments.ModelPayouts
import com.upscribber.payment.customerPayments.PaymentsViewModel
import com.upscribber.profile.ModelGetProfile
import com.upscribber.profile.ProfileViewModel
import com.upscribber.upscribberSeller.dashboardfragment.viewpagerFragments.wallet.PayoutsAdapter
import kotlinx.android.synthetic.main.activity_wallet_seller.*
import kotlinx.android.synthetic.main.layout_wallet_header.*
import java.text.SimpleDateFormat
import java.util.*


class WalletSellerActivity : AppCompatActivity() {

    private var screen: Int = 1
    lateinit var toolbar: Toolbar
    lateinit var toolbarTitle: TextView
    lateinit var progressBarSmall: ConstraintLayout

    //private lateinit var mBinding: ActivityWalletSellerBinding
    lateinit var mViewModel: PaymentsViewModel
    private lateinit var mAdapter: PaymentHistoryAdapter
    private lateinit var mAdapterPayouts: PayoutsAdapter
    var drawerOpen = 0
    private lateinit var fragmentManager: FragmentManager
    private lateinit var fragmentTransaction: FragmentTransaction
    private lateinit var filter_done: TextView
    lateinit var modelProfile: ModelGetProfile
    lateinit var mViewModelProfile: ProfileViewModel
    var modelPay = ModelPayouts()
    var typee = 1
    var pageTrans = 1
    var pagePay = 1
    var isLoadingMoreTrans = false
    var isLoadingMorePay = false
    var isLastPageTrans = false
    var isLastPagePay = false
    var layoutpag = LinearLayoutManager(this)
    var layoutpag1 = LinearLayoutManager(this)
    var arrayPaymentsTransactions: ArrayList<PaymentTransModel> = ArrayList()
    var arrayPayoutsTransactions: ArrayList<PaymentTransModel> = ArrayList()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setFullScreen()
        setContentView(R.layout.activity_wallet_seller)
        mViewModel = ViewModelProviders.of(this).get(PaymentsViewModel::class.java)
        mViewModelProfile = ViewModelProviders.of(this).get(ProfileViewModel::class.java)
        modelProfile = intent.getParcelableExtra("modelProfile")
        init()
        setToolbar()
        roundedImage()
        clickListeners()
        apiImplimentation()
        observerInit()
        setAdapter()
        scrollListener(this)


    }

    fun scrollListener(context: Context) {

        recyclerViewPayments.addOnScrollListener(object : CloseListPagination(layoutpag) {
            override fun isLastPage(): Boolean {
                return isLastPagePay
            }

            override fun loadMoreItems() {
                val auth = Constant.getPrefs(context).getString(Constant.auth_code, "")
                pagePay++
                mViewModel.getAllTransactions(
                    auth,
                    "2",
                    "",
                    "",
                    pageTrans.toString(),
                    pagePay.toString(),
                    "merchant"
                )
                progressBar_pagination.visibility = View.VISIBLE
            }

            override fun isLoading(): Boolean {
                return isLoadingMorePay
            }

        })
        recyclerViewPayouts.addOnScrollListener(object : CloseListPagination(layoutpag1) {
            override fun isLastPage(): Boolean {
                return isLastPageTrans
            }

            override fun loadMoreItems() {
                val auth = Constant.getPrefs(context).getString(Constant.auth_code, "")
                pageTrans++
                mViewModel.getAllTransactions(
                    auth,
                    "2",
                    "",
                    "",
                    pageTrans.toString(),
                    pagePay.toString(),
                    "merchant"
                )
                progressBar_pagination.visibility = View.VISIBLE
            }

            override fun isLoading(): Boolean {
                return isLoadingMoreTrans
            }

        })


    }

    private fun setFullScreen() {
        val window = window
        val background = resources.getDrawable(R.drawable.status_wallet)
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(this, R.color.fulltransparentcolor)
        window.setBackgroundDrawable(background)
    }

    @SuppressLint("SimpleDateFormat")
    private fun observerInit() {
        mViewModel.getmAllTransactions().observe(this, Observer {
            progressBar.visibility = View.GONE
            modelPay = it
            if (modelPay.arrayPaymentsTransactions.size > 0 || modelPay.arrayPayoutsTransactions.size > 0) {
                mAdapter.update(modelPay.arrayPaymentsTransactions)
                mAdapterPayouts.update(modelPay.arrayPayoutsTransactions)
                arrayPaymentsTransactions = modelPay.arrayPaymentsTransactions
                arrayPayoutsTransactions = modelPay.arrayPayoutsTransactions
                isLoadingMorePay = false
                isLoadingMoreTrans = false
                if (arrayPaymentsTransactions.size >= modelPay.pcount) {
                    isLastPagePay = true
                }
                if (arrayPayoutsTransactions.size >= modelPay.tcount) {
                    isLastPageTrans = true
                }

                noData.visibility = View.GONE
            } else {
                noData.visibility = View.VISIBLE
            }


        })

        mViewModelProfile.getmDataProfile().observe(this, androidx.lifecycle.Observer {
            if (it.status == "true") {
                progressBarSmall.visibility = View.GONE
                modelProfile.selected_payout_method =
                    it.modelGetProfileCustomer.selected_payout_method
                modelProfile.payment_method_available =
                    it.modelGetProfileCustomer.payment_method_available
                modelProfile.payout_date = it.modelGetProfileCustomer.payout_date
                modelProfile.available_payout = it.modelGetProfileCustomer.available_payout
                payoutAmount.text = "$" + modelProfile.available_payout
                calculatePayoutDateTime(modelProfile)


                imageView21.setImageResource(R.drawable.ic_wallet_dollar1)
                tvGetPaid.text = "Paid"
                tvGetPaid.isClickable = false
                textDays.visibility = View.GONE
                days.visibility = View.GONE
                textHours.visibility = View.GONE
                hours.visibility = View.GONE
                textMinuts.visibility = View.GONE
                minutes.visibility = View.GONE


            }
        })

        mViewModelProfile.getStatus().observe(this, Observer {
            progressBarSmall.visibility = View.GONE
            if (it.msg == "Invalid auth code") {
                Constant.commonAlert(this)
            } else if (it.msg.isNotEmpty()) {
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
            }
        })

        mViewModel.getStatus().observe(this, Observer {
            progressBar.visibility = View.GONE
            if (it.msg == "Invalid auth code") {
                Constant.commonAlert(this)
            } else if (it.msg.isNotEmpty()) {
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
            }
        })

    }

    private fun calculatePayoutDateTime(modelProfile: ModelGetProfile) {
        var sortedDate = ""
        var sortedDate1 = ""
        var dateToBeWrite = ""
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val date = c.get(Calendar.DAY_OF_MONTH)
        val hour = c.get(Calendar.HOUR_OF_DAY)
        val minut = c.get(Calendar.MINUTE)
        val second = c.get(Calendar.SECOND)
        val month = c.get(Calendar.MONTH) + 1

        sortedDate = if (date.toString().toInt() > modelProfile.payout_date.toInt()) {
            modelProfile.payout_date + " " + (month + 1) + " " + year
        } else {
            modelProfile.payout_date + " " + month + " " + year
        }

        sortedDate1 = if (date.toString().toInt() > modelProfile.payout_date.toInt()) {
            year.toString() + "-" + (month + 1) + "-" + modelProfile.payout_date + " " + hour + ":" + minut + ":" + second
        } else {
            year.toString() + "-" + month + "-" + modelProfile.payout_date + " " + hour + ":" + minut + ":" + second
        }


        val formatter = SimpleDateFormat("dd M yyyy", Locale.getDefault())
        val formatter1 = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
        val dateFormatter = SimpleDateFormat("dd MMM YYYY", Locale.getDefault())
        val dateFormatter1 = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
        try {
            formatter.timeZone = TimeZone.getTimeZone("IST")
            formatter1.timeZone = TimeZone.getTimeZone("IST")
            val value = formatter.parse(sortedDate)
            val value1 = formatter1.parse(sortedDate1)

            dateFormatter.timeZone = TimeZone.getDefault()
            dateFormatter1.timeZone = TimeZone.getDefault()
            payoutDate.text = dateFormatter.format(value)
            dateToBeWrite = dateFormatter1.format(value1)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        val currentDateandTime = Constant.getDate(dateToBeWrite)
        daysCalculation(currentDateandTime)

    }


    private fun daysCalculation(currentDateandTime: Date) {
        val serverMillis = currentDateandTime.time
        val currentMillis = System.currentTimeMillis()
        val millis = serverMillis - currentMillis
        val secondMillis = 1000
        val minuteMillis = secondMillis * 60
        val hoursMillis = minuteMillis * 60
        val daysMillis = hoursMillis * 24

        val daysMil = millis / daysMillis
        val afterDays = millis - (daysMillis * daysMil)
        val hours = afterDays / hoursMillis
        val minutes = (afterDays - (hoursMillis * hours)) / minuteMillis

        textDays.text = daysMil.toString()
        textHours.text = hours.toString()
        textMinuts.text = minutes.toString()
    }

    private fun apiImplimentation() {
        val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
        mViewModel.getAllTransactions(
            auth,
            "2",
            "",
            "",
            pageTrans.toString(),
            pagePay.toString(),
            "merchant"
        )
        progressBar.visibility = View.VISIBLE

    }

    private fun setAdapter() {
        recyclerViewPayments.layoutManager = layoutpag
        //mBinding.recyclerViewPayments.isNestedScrollingEnabled = false
        mAdapter = PaymentHistoryAdapter(this, 2)
        recyclerViewPayments.adapter = mAdapter


        recyclerViewPayouts.layoutManager = layoutpag1
        // mBinding.recyclerViewPayouts.isNestedScrollingEnabled = false
        mAdapterPayouts = PayoutsAdapter(this, 2)
        recyclerViewPayouts.adapter = mAdapterPayouts


    }

    private fun clickListeners() {
//        mBinding.sort.setOnClickListener {
//            drawerOpen = 1
//            Constant.hideKeyboard(this, mBinding.searchQuery)
//            if (searchView != null) {
//                mBinding.searchQuery.setQuery("", false)
//                mBinding.searchQuery.clearFocus()
//            }
//            mBinding.drawerLayout1.openDrawer(GravityCompat.END)
//            inflateFragment(WalletFilters())
//        }

//        filter_done.setOnClickListener {
//            onBackPressed()
//        }
//
        backButton.setOnClickListener {
            if (screen == 2) {
                titleWallet.text = "Payouts"
                product3.text = "Today's Payouts"
                tvGetPaid.isClickable = true
                dollar.setImageResource(R.drawable.ic_wallet_dollar1)
                textDays.visibility = View.GONE
                days.visibility = View.GONE
                textHours.visibility = View.GONE
                hours.visibility = View.GONE
                textMinuts.visibility = View.GONE
                minutes.visibility = View.GONE
                screen = 1
                if (modelProfile.available_payout == "0") {
                    tvGetPaid.text = "Paid"
                    tvGetPaid.isClickable = false
                } else {
                    tvGetPaid.text = "Get Paid Now"
                    tvGetPaid.isClickable = true
                }
            } else {
                finish()
            }
        }


        tvGetPaid.setOnClickListener {
            if (modelProfile.stripe_status == "0" || modelProfile.stripe_status == "1") {
                Constant.commonErrorAlert(this, "Payouts not enabled on your Stripe account.")
            } else {
                if (modelProfile.available_payout != "0") {
                    val fragment = MenuFragmentManual(
                        mViewModel,
                        progressBarSmall,
                        mViewModelProfile,
                        modelProfile
                    )
                    fragment.show(supportFragmentManager, fragment.tag)
                }
            }
//            else {
//                startActivityForResult(Intent(this, InstantPayoutsActivity::class.java), 1)
//            }

        }


        dollar.setOnClickListener {
            if (screen == 1) {
                screen = 2
                titleWallet.text = "Auto Payment"
                product3.text = "Payment Amount"
                tvGetPaid.text = "Next Payment"
                tvGetPaid.isClickable = false
                dollar.setImageResource(R.drawable.info_white)
                textDays.visibility = View.VISIBLE
                days.visibility = View.VISIBLE
                textHours.visibility = View.VISIBLE
                hours.visibility = View.VISIBLE
                textMinuts.visibility = View.VISIBLE
                minutes.visibility = View.VISIBLE
            } else {
                val mDialogView =
                    LayoutInflater.from(this).inflate(R.layout.get_paid_activate_alert, null)
                val mBuilder = AlertDialog.Builder(this)
                    .setView(mDialogView)
                val mAlertDialog = mBuilder.show()
                mAlertDialog.window.setBackgroundDrawableResource(R.drawable.bg_alert1)

            }


        }

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == 1) {
            val isSeller = Constant.getSharedPrefs(this).getBoolean(Constant.IS_SELLER, false)
            val type = Constant.getPrefs(this).getString(Constant.type, "")
            val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
            if (isSeller) {
                when (type) {
                    "1" -> {
                        mViewModelProfile.getProfileData(auth, "1", "0", "0")
                    }
                    "2" -> {
                        mViewModelProfile.getProfileData(auth, "2", "0", "0")
                    }
                }
            }
        }
    }

    class MenuFragmentManual(
        var mViewModel: PaymentsViewModel,
        var progressBarSmall: ConstraintLayout,
        var mViewModelProfile: ProfileViewModel,
        var modelProfile: ModelGetProfile
    ) : RoundedBottomSheetDialogFragment() {

        lateinit var no: TextView
        lateinit var yes: TextView
        lateinit var imageViewTop: ImageView

        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val view = inflater.inflate(R.layout.manual_payout_method, container, false)
            no = view.findViewById(R.id.no)
            yes = view.findViewById(R.id.yes)
            imageViewTop = view.findViewById(R.id.imageViewTop)
            roundImageView(imageViewTop)
            observerInit()
            no.setOnClickListener {
                dismiss()
            }

            yes.setOnClickListener {
                if (modelProfile.payment_method == "1") {
                    dismiss()
                    val mDialogView = LayoutInflater.from(activity)
                        .inflate(R.layout.days_left_alert, null)
                    val mBuilder = android.app.AlertDialog.Builder(activity)
                        .setView(mDialogView)
                    val mAlertDialog = mBuilder.show()
                    val textView249: TextView = mDialogView.findViewById(R.id.textView249)
                    val okBtn: TextView = mDialogView.findViewById(R.id.okBtn)

                    val title: TextView = mDialogView.findViewById(R.id.textViewTitle)

                    title.visibility = View.GONE
                    textView249.text =
                        "Your selected payment method is Bank, so you cannot avail this payment."
                    okBtn.setTextColor(resources.getColor(R.color.blue))

                    mAlertDialog.setCancelable(false)
                    mAlertDialog.window.setBackgroundDrawableResource(R.drawable.bg_alert_white)

                    okBtn.setOnClickListener {
                        mAlertDialog.dismiss()
                    }
                } else {
                    val auth = Constant.getPrefs(activity!!).getString(Constant.auth_code, "")
                    progressBarSmall.visibility = View.VISIBLE
                    mViewModel.changePayoutMethod(auth, "1")
                    dismiss()
                }
            }



            return view
        }

        private fun roundImageView(imageViewTop: ImageView) {
            val curveRadius = 50F

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                imageViewTop.outlineProvider = object : ViewOutlineProvider() {

                    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                    override fun getOutline(view: View?, outline: Outline?) {
                        outline?.setRoundRect(
                            0,
                            0,
                            view!!.width,
                            view.height + curveRadius.toInt(),
                            curveRadius
                        )
                    }
                }
                imageViewTop.clipToOutline = true
            }
        }

        private fun observerInit() {
            val isSeller = Constant.getSharedPrefs(context!!).getBoolean(Constant.IS_SELLER, false)
            val type = Constant.getPrefs(context!!).getString(Constant.type, "")

            mViewModel.getmDataInstantPaymentMethod().observe(this, Observer {
                dismiss()
                val auth = Constant.getPrefs(context!!).getString(Constant.auth_code, "")
                if (isSeller) {
                    when (type) {
                        "1" -> {
                            mViewModelProfile.getProfileData(auth, "1", "0", "0")
                        }
                        "2" -> {
                            mViewModelProfile.getProfileData(auth, "2", "0", "0")
                        }
                    }
                }

            })


            mViewModel.getStatus().observe(this, Observer {
                progressBarSmall.visibility = View.GONE
                if (it.msg == "Invalid auth code") {
                    Constant.commonAlert(activity!!)
                } else if (it.msg.isNotEmpty()) {
                    Toast.makeText(activity, it.msg, Toast.LENGTH_SHORT).show()
                }
            })

        }

    }

    private fun inflateFragment(fragment: Fragment) {
        fragmentManager = supportFragmentManager
        this.fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.containerSide1, fragment)
        fragmentTransaction.commit()

    }

    private fun roundedImage() {

        val image = findViewById<CollapsingToolbarLayout>(R.id.ll_one)
        val curveRadius = 70F

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            image.outlineProvider = object : ViewOutlineProvider() {

                @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                override fun getOutline(view: View?, outline: Outline?) {
                    outline?.setRoundRect(
                        0,
                        -100,
                        view!!.width,
                        view.height,
                        curveRadius
                    )
                }
            }

            image.clipToOutline = true

        }
    }


    private fun init() {
        toolbarTitle = findViewById(R.id.titleWallet)
        progressBarSmall = findViewById(R.id.progressBarSmall)
        payoutAmount.text = "$" + modelProfile.available_payout
        recyclerViewPayments.visibility = View.VISIBLE
        recyclerViewPayouts.visibility = View.GONE



        calculatePayoutDateTime(modelProfile)
        if (modelProfile.payment_method_available == "1") {
            tvGetPaid.visibility = View.VISIBLE
            if (modelProfile.available_payout == "0") {
                tvGetPaid.text = "Paid"
                tvGetPaid.isClickable = false
                tvGetPaid.isEnabled = false
                textDays.visibility = View.GONE
                days.visibility = View.GONE
                textHours.visibility = View.GONE
                hours.visibility = View.GONE
                textMinuts.visibility = View.GONE
                minutes.visibility = View.GONE
            } else {
                tvGetPaid.text = "Get Paid Now"
                tvGetPaid.isClickable = true
                tvGetPaid.isEnabled = true
                textDays.visibility = View.GONE
                days.visibility = View.GONE
                textHours.visibility = View.GONE
                hours.visibility = View.GONE
                textMinuts.visibility = View.GONE
                minutes.visibility = View.GONE
            }
        } else {
            tvGetPaid.visibility = View.GONE
        }





        transactions.setOnClickListener {
            transactions.setBackgroundResource(R.drawable.bg_seller_blue_padding)
            transactions.setTextColor(Color.WHITE)
            payouts.setBackgroundResource(R.drawable.bg_seller_white)
            payouts.setTextColor(Color.parseColor("#8f909e"))
            recyclerViewPayments.visibility = View.VISIBLE
            recyclerViewPayouts.visibility = View.GONE
            mAdapter.update(modelPay.arrayPaymentsTransactions)
            typee = 1
            noData.text = "No Transactions made!"

        }


        payouts.setOnClickListener {
            payouts.setBackgroundResource(R.drawable.bg_seller_blue_padding)
            payouts.setTextColor(Color.WHITE)
            transactions.setBackgroundResource(R.drawable.bg_seller_white)
            transactions.setTextColor(Color.parseColor("#8f909e"))
            recyclerViewPayments.visibility = View.GONE
            recyclerViewPayouts.visibility = View.VISIBLE
            mAdapterPayouts.update(modelPay.arrayPayoutsTransactions)
            typee = 2
            noData.text = "No Payouts made!"
        }

        if (screen == 1) {
            titleWallet.text = "Payouts"
            product3.text = "Today's Payouts"
            tvGetPaid.isClickable = true
            dollar.setImageResource(R.drawable.ic_wallet_dollar1)
            textDays.visibility = View.GONE
            days.visibility = View.GONE
            textHours.visibility = View.GONE
            hours.visibility = View.GONE
            textMinuts.visibility = View.GONE
            minutes.visibility = View.GONE
            if (modelProfile.available_payout == "0") {
                tvGetPaid.text = "Paid"
                tvGetPaid.isClickable = false
            } else {
                tvGetPaid.text = "Get Paid Now"
                tvGetPaid.isClickable = true
            }

        } else {
            titleWallet.text = "Auto Payment"
            product3.text = "Payment Amount"
            tvGetPaid.text = "Next Payment"
            tvGetPaid.isClickable = false
            dollar.setImageResource(R.drawable.info_white)
            textDays.visibility = View.VISIBLE
            days.visibility = View.VISIBLE
            textHours.visibility = View.VISIBLE
            hours.visibility = View.VISIBLE
            textMinuts.visibility = View.VISIBLE
            minutes.visibility = View.VISIBLE

        }


    }


    private fun setToolbar() {

//        setSupportActionBar(toolbar)
//        title = ""
//        toolbarTitle.text = "Payouts"
//        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
//        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white)
    }


//    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
//        if (item!!.itemId == android.R.id.home) {
//            finish()
//        }

//        if (item.itemId == R.id.wallet){
//            startActivity(Intent(this,PaymentsMerchantActivity::class.java))
//            val fragment = MenuFragment()
//            fragment.show(supportFragmentManager, fragment.tag)
//        }

//        return super.onOptionsItemSelected(item)
//    }

    class MenuFragment : RoundedBottomSheetDialogFragment() {

        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val view = inflater.inflate(R.layout.add_cards_option_sheet, container, false)

            val cancelDialog = view.findViewById<TextView>(R.id.cancelDialog)
            val addCreditCard = view.findViewById<LinearLayout>(R.id.addCreditCard)
            val addBankAccount = view.findViewById<LinearLayout>(R.id.addBankAccount)
            val textAddCredit = view.findViewById<TextView>(R.id.textAddCredit)
            val imgAddCredit = view.findViewById<ImageView>(R.id.imgAddCredit)
            val imgBAnkAccount = view.findViewById<ImageView>(R.id.imgBAnkAccount)
            val textBankAccount = view.findViewById<TextView>(R.id.textBankAccount)

            cancelDialog.setOnClickListener {
                dismiss()
            }

            addCreditCard.setOnClickListener {
                startActivity(
                    Intent(activity, AddCardActivity::class.java)
                )
                dismiss()
                addCreditCard.setBackgroundResource(R.drawable.bg_change_pic)
                textAddCredit.setTextColor(resources.getColor(R.color.colorWhite))
                imgAddCredit.setImageResource(R.drawable.ic_visa_card)
                imgBAnkAccount.setImageResource(R.drawable.ic_bank_account_grey)
                textBankAccount.setTextColor(resources.getColor(R.color.pic_non_selected))
                addBankAccount.setBackgroundResource(R.drawable.bg_non_selected_pic)


            }

            addBankAccount.setOnClickListener {
                startActivity(Intent(activity, AddBankAccountActivity::class.java))
                dismiss()
                addBankAccount.setBackgroundResource(R.drawable.bg_change_pic)
                addCreditCard.setBackgroundResource(R.drawable.bg_non_selected_pic)
                textAddCredit.setTextColor(resources.getColor(R.color.pic_non_selected))
                imgAddCredit.setImageResource(R.drawable.ic_visa_card_grey)
                imgBAnkAccount.setImageResource(R.drawable.ic_bank_account)
                textBankAccount.setTextColor(resources.getColor(R.color.colorWhite))
            }

            return view
        }

    }


//    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
//        menuInflater.inflate(R.menu.wallet, menu)
//        return super.onCreateOptionsMenu(menu)
//    }

    fun onGetPaid(view: View) {

    }


}
