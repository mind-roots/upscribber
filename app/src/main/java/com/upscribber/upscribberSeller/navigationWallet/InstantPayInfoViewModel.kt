package com.upscribber.upscribberSeller.navigationWallet

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData


class InstantPayInfoViewModel(application: Application) : AndroidViewModel(application)  {

    var instantPayInfoRepository : InstantPayInfoRepository = InstantPayInfoRepository(application)

    fun getPaymentsList(): LiveData<List<InstantPayInfoModel>> {
        return  instantPayInfoRepository.getItems()
    }



}