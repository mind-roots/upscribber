package com.upscribber.upscribberSeller.navigationWallet

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.upscribber.R

class InstantPayInfoRepository(application: Application) {

    fun getItems(): LiveData<List<InstantPayInfoModel>> {

        val mData = MutableLiveData<List<InstantPayInfoModel>>()
        val arrayList = ArrayList<InstantPayInfoModel>()


        var instantPayInfoModel = InstantPayInfoModel()
        instantPayInfoModel.personName = "Craig Martin"
        instantPayInfoModel.personTransAmount = "$31.5"
        instantPayInfoModel.personImage = R.mipmap.profile_image
        instantPayInfoModel.transDateTime = "19 March, circularprogress_black:20 pm"
        arrayList.add(instantPayInfoModel)

        instantPayInfoModel = InstantPayInfoModel()
        instantPayInfoModel.personName = "Craig Martin"
        instantPayInfoModel.personTransAmount = "$45.3"
        instantPayInfoModel.personImage = R.mipmap.profile_image
        instantPayInfoModel.transDateTime = "19 March, circularprogress_black:20 pm"
        arrayList.add(instantPayInfoModel)

        instantPayInfoModel = InstantPayInfoModel()
        instantPayInfoModel.personName = "Craig Martin"
        instantPayInfoModel.personTransAmount = "$31"
        instantPayInfoModel.personImage = R.mipmap.profile_image
        instantPayInfoModel.transDateTime = "19 March, circularprogress_black:20 pm"
        arrayList.add(instantPayInfoModel)

        instantPayInfoModel = InstantPayInfoModel()
        instantPayInfoModel.personName = "Craig Martin"
        instantPayInfoModel.personTransAmount = "$36.3"
        instantPayInfoModel.personImage = R.mipmap.profile_image
        instantPayInfoModel.transDateTime = "19 March, circularprogress_black:20 pm"
        arrayList.add(instantPayInfoModel)

        instantPayInfoModel = InstantPayInfoModel()
        instantPayInfoModel.personName = "Craig Martin"
        instantPayInfoModel.personTransAmount = "$100.5"
        instantPayInfoModel.personImage = R.mipmap.profile_image
        instantPayInfoModel.transDateTime = "19 March, circularprogress_black:20 pm"
        arrayList.add(instantPayInfoModel)

        instantPayInfoModel = InstantPayInfoModel()
        instantPayInfoModel.personName = "Craig Martin"
        instantPayInfoModel.personTransAmount = "$500.6"
        instantPayInfoModel.personImage = R.mipmap.profile_image
        instantPayInfoModel.transDateTime = "19 March, circularprogress_black:20 pm"
        arrayList.add(instantPayInfoModel)

        mData.value = arrayList
        return mData
    }
}