package com.upscribber.upscribberSeller.navigationWallet

import android.content.Intent
import android.os.Bundle
import android.view.*
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.upscribber.payment.paymentCards.AddCardActivity
import com.upscribber.commonClasses.RoundedBottomSheetDialogFragment
import com.upscribber.upscribberSeller.payments.bankAccount.AddBankAccountActivity
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.payment.customerPayments.PaymentsViewModel
import com.upscribber.upscribberSeller.payments.PaymentsMerchantActivity
import kotlinx.android.synthetic.main.get_paid_activate_alert.*
import kotlinx.android.synthetic.main.get_paid_activate_alert.view.*

class InstantPayoutsActivity : AppCompatActivity() {

    lateinit var toolbar: Toolbar
    lateinit var toolbarTitle: TextView
    lateinit var tvActivate: TextView
    lateinit var progressBar: ConstraintLayout
    lateinit var mViewModel: PaymentsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_instant_payouts)
        mViewModel = ViewModelProviders.of(this).get(PaymentsViewModel::class.java)
        initz()
        setToolbar()
        click()
        observerInit()
    }

    private fun observerInit() {
        mViewModel.getmDataInstantPaymentMethod().observe(this, Observer {
            progressBar.visibility = View.GONE
            val intent = Intent()
            setResult(1, intent)
            finish()
        })

        mViewModel.getStatus().observe(this, Observer {
            progressBar.visibility = View.GONE
            if (it.msg == "Invalid auth code") {
                Constant.commonAlert(this)
            } else {
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
            }
        })

    }


    private fun initz() {
        toolbar = findViewById(R.id.InstantsPayToolbar)
        toolbarTitle = findViewById(R.id.title)
        tvActivate = findViewById(R.id.tvActivate)
        progressBar = findViewById(R.id.progressBar)
    }

    private fun setToolbar() {
        setSupportActionBar(toolbar)
        title = ""
        toolbarTitle.text = "Wallet"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white)

    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }

        return super.onOptionsItemSelected(item)
    }


    private fun click() {
        tvActivate.setOnClickListener {
            val mDialogView =
                LayoutInflater.from(this).inflate(R.layout.get_paid_activate_alert, null)
            val mBuilder = AlertDialog.Builder(this)
                .setView(mDialogView)
            val mAlertDialog = mBuilder.show()
            mAlertDialog.setCancelable(false)
            mAlertDialog.window.setBackgroundDrawableResource(R.drawable.bg_alert1)
            mDialogView.no.setOnClickListener {
                mAlertDialog.dismiss()
            }

            mAlertDialog.yes.setOnClickListener {
                val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
                progressBar.visibility = View.VISIBLE
                mViewModel.changePayoutMethod(auth, "1")
                mAlertDialog.dismiss()
            }
        }

    }


}
