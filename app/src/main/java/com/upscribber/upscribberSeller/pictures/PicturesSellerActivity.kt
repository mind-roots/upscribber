package com.upscribber.upscribberSeller.pictures

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.Outline
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.view.*
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.upscribber.commonClasses.Constant
import com.upscribber.commonClasses.RoundedBottomSheetDialogFragment
import com.upscribber.upscribberSeller.pictures.coverImages.AdapterCoverImages
import com.upscribber.upscribberSeller.pictures.coverImages.ModelCoverImages
import com.upscribber.upscribberSeller.pictures.logoImages.AdapterLogoImages
import com.upscribber.upscribberSeller.pictures.logoImages.ModelLogoImages
import com.upscribber.R
import com.upscribber.databinding.ActivityPicturesSellerBinding
import com.upscribber.databinding.CreateConfirmImageBinding
import kotlinx.android.synthetic.main.activity_pictures_seller.*
import java.io.*
import kotlin.collections.ArrayList


class PicturesSellerActivity : AppCompatActivity()
    , AdapterCoverImages.SelectionCover, AdapterLogoImages.selection {

    private var getImages = ArrayList<ModelCoverImages>()
    lateinit var toolbar: Toolbar
    lateinit var toolbarTitle: TextView
    lateinit var done: TextView
    private lateinit var binding: ActivityPicturesSellerBinding
    private lateinit var mAdapterCoverImages: AdapterCoverImages
    private lateinit var backgroundChange: ArrayList<ModelCoverImages>
    private lateinit var backgroundLogoChange: ArrayList<ModelLogoImages>
    private lateinit var mAdapterLogoImages: AdapterLogoImages
    private lateinit var imageUri: Uri
    private var pos: Int = 0
    private var whatToDo = "1"
    var deleteOrNot = 0

    companion object {
        lateinit var viewModel: PicturesViewModel

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_pictures_seller)
        binding.lifecycleOwner = this
        initz()
        click()
        setToolbar()
        setAdapter()
        viewModel = ViewModelProviders.of(this)[PicturesViewModel::class.java]
        if (intent.hasExtra("create")) {
            whatToDo = "1"
            binding.textView182.visibility = View.GONE
            binding.recyclerLogoImages.visibility = View.GONE
            binding.textView203.visibility = View.VISIBLE
            done.isEnabled = false
        } else {
            whatToDo = "1"
            binding.textView203.visibility = View.GONE
            binding.textView182.visibility = View.VISIBLE

        }

        observerInit()
    }

    override fun onResume() {
        super.onResume()
        viewModel.getCoverImages()
        progressBar.visibility = View.VISIBLE
    }

    private fun observerInit() {
        viewModel.getmDataCoverImages().observe(this, Observer {
            if (it.isNotEmpty()) {
                whatToDo = "2"
                binding.noData.visibility = View.GONE
                binding.noDataDescription.visibility = View.GONE
                binding.getStartedBtn.visibility = View.GONE
                binding.imageView83.visibility = View.GONE
                if (intent.hasExtra("create")) {
                    binding.textView185.visibility = View.VISIBLE
                    binding.textView203.visibility = View.VISIBLE
                    binding.recyclerCoverImages.visibility = View.VISIBLE
                    binding.recyclerLogoImages.visibility = View.GONE
                    binding.textView182.visibility = View.GONE
                } else {
                    binding.textView185.visibility = View.VISIBLE
                    binding.recyclerCoverImages.visibility = View.VISIBLE
                    binding.recyclerLogoImages.visibility = View.VISIBLE
                    binding.textView182.visibility = View.VISIBLE
                }
                getImages = it
                if (intent.hasExtra("images")) {
                    val preFilledList =
                        intent.getParcelableArrayListExtra<ModelCoverImages>("images")
                    val duplicateData = ArrayList<ModelCoverImages>()

                    for (items in it) {
                        for (selectedItems in preFilledList) {
                            if (items.name == selectedItems.name) {
                                items.status = true
                                duplicateData.add(selectedItems)
                            }
                        }
                    }

                    if (duplicateData.size >= 3) {
                        done.background = resources.getDrawable(R.drawable.bg_seller_button_blue)
                        done.isEnabled = true
                        done.visibility = View.VISIBLE
                    } else {
                        done.visibility = View.VISIBLE
                        done.background =
                            resources.getDrawable(R.drawable.blue_button_background_opacity)
                        done.isEnabled = false
                    }

                }
                if (intent.hasExtra("create")) {
                    val arraylist = it
                    for (items in arraylist) {
                        items.status = true
                    }
                    if (arraylist.size >= 3) {
                        done.visibility = View.VISIBLE
                        done.background = resources.getDrawable(R.drawable.bg_seller_button_blue)
                        done.isEnabled = true
                    }
                    mAdapterCoverImages.update(arraylist)
                } else {
                    mAdapterCoverImages.update(it)
                }

            } else {
                whatToDo = "1"
                mAdapterCoverImages.update(it)
            }

        })

        viewModel.getmDataLogoImages().observe(this, Observer {
            progressBar.visibility = View.GONE
            if (it.size > 0) {
                whatToDo = ""
                val modelLogoImages = ModelLogoImages()
                it.add(modelLogoImages)
                mAdapterLogoImages.update(it)
                if(intent.hasExtra("create")){
                    binding.recyclerLogoImages.visibility = View.GONE
                }else{
                    binding.recyclerLogoImages.visibility = View.VISIBLE
                }

            } else {
                if (whatToDo == "1") {
                    if (intent.hasExtra("create")) {
                        binding.noData.visibility = View.VISIBLE
                        binding.noDataDescription.visibility = View.VISIBLE
                        binding.getStartedBtn.visibility = View.VISIBLE
                        binding.imageView83.visibility = View.VISIBLE
                        binding.recyclerCoverImages.visibility = View.GONE
                        binding.recyclerLogoImages.visibility = View.GONE
                        binding.textView182.visibility = View.GONE
                        binding.textView185.visibility = View.GONE
                        binding.textView203.visibility = View.GONE

                    } else {
                        binding.noData.visibility = View.VISIBLE
                        binding.noDataDescription.visibility = View.VISIBLE
                        binding.getStartedBtn.visibility = View.VISIBLE
                        binding.imageView83.visibility = View.VISIBLE
                        binding.recyclerCoverImages.visibility = View.GONE
                        binding.recyclerLogoImages.visibility = View.GONE
                        binding.textView182.visibility = View.GONE
                        binding.textView185.visibility = View.GONE
                    }
                } else {
                    if (!intent.hasExtra("create")) {
                        val modelLogoImages = ModelLogoImages()
                        it.add(modelLogoImages)
                        mAdapterLogoImages.update(it)
                        binding.recyclerLogoImages.visibility = View.VISIBLE
                    }

                }
            }

        })


        viewModel.getIfImageUploaded().observe(this, Observer {
            if (it.status == "true") {
                viewModel.getCoverImages()
                progressBarr.visibility = View.GONE
            }
        })

        viewModel.getmDataFailure().observe(this, Observer {
            if (it.msg == "Invalid auth code") {
                Constant.commonAlert(this)
            } else {
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
            }

        })

    }

    private fun setAdapter() {
        var value = ""
        if (intent.hasExtra("create")) {
            value = intent.getStringExtra("create")
        }
        binding.recyclerCoverImages.layoutManager =
            GridLayoutManager(this, 3)
        mAdapterCoverImages = AdapterCoverImages(this, value)
        binding.recyclerCoverImages.adapter = mAdapterCoverImages
        mAdapterCoverImages.showDeleteButton(deleteOrNot)

        val layoutManager1 = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        binding.recyclerLogoImages.layoutManager = layoutManager1
        binding.recyclerLogoImages.isNestedScrollingEnabled = false
        mAdapterLogoImages = AdapterLogoImages(this)
        binding.recyclerLogoImages.adapter = mAdapterLogoImages
    }

    private fun click() {
        binding.addPictures.setOnClickListener {
            val fragment = MenuFragment(pos, "bottom", binding)
            fragment.show(supportFragmentManager, fragment.tag)
        }

        binding.getStartedBtn.setOnClickListener {
            val fragment = MenuFragment(pos, "bottom", binding)
            fragment.show(supportFragmentManager, fragment.tag)
        }

        binding.deleteImage.setOnClickListener {
            if (intent.hasExtra("create")) {
                done.text = "Save"
                done.background = resources.getDrawable(R.drawable.bg_seller_button_blue)
                done.isEnabled = true
            }else{
                done.visibility = View.VISIBLE
                done.text = "Save"
                done.background = resources.getDrawable(R.drawable.bg_seller_button_blue)
            }
            deleteOrNot = 1
            mAdapterCoverImages.showDeleteButton(deleteOrNot)
        }



        done.setOnClickListener {
            if (intent.hasExtra("create")) {
                if (deleteOrNot == 0) {
                    val selectedItems: ArrayList<ModelCoverImages> =
                        mAdapterCoverImages.getSelectedItems()
                    val intent = Intent()
                    intent.putExtra("SelectedArray", selectedItems)
                    setResult(1234, intent)
                    finish()
                } else {
                    done.text = "Done"
                    val selectedItems: ArrayList<ModelCoverImages> =
                        mAdapterCoverImages.getSelectedItems()
                    if (selectedItems.size < 3) {
                        done.background =
                            resources.getDrawable(R.drawable.blue_button_background_opacity)
                        done.isEnabled = false
                    }
                    deleteOrNot = 0
                    mAdapterCoverImages.showDeleteButton(deleteOrNot)
                }
            } else {
                deleteOrNot = 0
                mAdapterCoverImages.showDeleteButton(deleteOrNot)
                done.visibility = View.GONE
            }
        }

    }

    @Suppress(
        "NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS",
        "RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS"
    )
    class MenuFragment(
        var pos: Int,
        var from: String,
        var binding: ActivityPicturesSellerBinding


    ) : RoundedBottomSheetDialogFragment() {
        private lateinit var imageUri2: Uri
        private lateinit var cursor: Cursor

        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val view = inflater.inflate(R.layout.change_profile_pic_layout, container, false)
            val isSeller = Constant.getSharedPrefs(context!!).getBoolean(Constant.IS_SELLER, false)
            val cancelDialog = view.findViewById<TextView>(R.id.cancelDialog)
            val takePicture = view.findViewById<LinearLayout>(R.id.takePicture)
            val galleryLayout = view.findViewById<LinearLayout>(R.id.galleryLayout)

            if (isSeller) {
                cancelDialog.setBackgroundResource(R.drawable.bg_seller_button_blue)
            } else {
                cancelDialog.setBackgroundResource(R.drawable.invite_button_background)
            }


            takePicture.setOnClickListener {

                val checkSelfPermission =
                    ContextCompat.checkSelfPermission(
                        this.activity!!,
                        android.Manifest.permission.CAMERA
                    )
                if (checkSelfPermission != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(
                        this.activity!!,
                        arrayOf(android.Manifest.permission.CAMERA),
                        2
                    )
                } else {
                    openCamera()
                }

            }

            galleryLayout.setOnClickListener {
                val checkSelfPermission =
                    ContextCompat.checkSelfPermission(
                        this.activity!!,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE
                    )
                if (checkSelfPermission != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(
                        this.activity!!,
                        arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE),
                        1
                    )
                } else {
                    openAlbum()
                    // dismiss()
                }
            }

            cancelDialog.setOnClickListener {
                dismiss()
            }

            return view
        }

        private fun openCamera() {
            try {
                val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                if (from == "bottom") {
                    startActivityForResult(intent, 39)
                } else {
                    startActivityForResult(intent, 29)
                }
            } catch (e: Exception) {
                e.printStackTrace()

            }

        }

        private fun openAlbum() {
            val intent = Intent("android.intent.action.GET_CONTENT")
            intent.type = "image/*"
            if (from == "bottom") {
                startActivityForResult(intent, 133)
            } else {
                startActivityForResult(intent, 143)
            }
        }

        fun getImageUri(inContext: Context, inImage: Bitmap): Uri {
            val bytes = ByteArrayOutputStream()
            inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
            val path = MediaStore.Images.Media.insertImage(
                inContext.contentResolver,
                inImage,
                "Title",
                null
            )
            return Uri.parse(path)
        }

        private fun getImageUri22(inImage: Bitmap): File {
            val root: String = Environment.getExternalStorageDirectory().toString()
            val myDir = File("$root/req_images")
            myDir.mkdirs()
            val fname = "Image_profile.jpg"
            val file = File(myDir, fname)
            if (file.exists()) {
                file.delete()
            }

            try {
                val out = FileOutputStream(file)
                inImage.compress(Bitmap.CompressFormat.JPEG, 70, out)
                out.flush()
                out.close()
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return file
        }

        private fun getPathFromURI(contentUri: Uri): String {
            try {
                val proj: Array<String> = arrayOf(MediaStore.Images.Media.DATA)
                cursor = context!!.contentResolver.query(contentUri, proj, null, null, null)
                val columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
                cursor.moveToFirst()
                return cursor.getString(columnIndex)
            } finally {
                cursor.close()
            }
        }

        override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
            super.onActivityResult(requestCode, resultCode, data)
            try {
                if (requestCode == 29) {
                    try {
                        if (data != null) {
                            val bitmap = data.extras.get("data") as Bitmap
                            val file = getImageUri22(bitmap)
                            viewModel.setImage(file, "2")
                            progressBarr.visibility = View.VISIBLE
                        }
                        val model = ModelLogoImages()
                        model.image = 0
                        model.status = false
                        model.uriImage = data!!.data
                        dismiss()
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
                if (requestCode == 39) {
                    try {
                        if (data != null) {
                            val bitmap = data.extras.get("data") as Bitmap
                            val file = getImageUri22(bitmap)
                            viewModel.setImage(file, "1")
                            progressBarr.visibility = View.VISIBLE
                        }
                        val model = ModelCoverImages()
                        model.image = 0
                        model.status = false
                        model.uriImage = data!!.data
                        dismiss()
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }

                if (requestCode == 143) {
                    try {

                        val selectedImage: Uri = data!!.data
                        // val fileee = File(getPath(selectedImage))

//                        val model = ModelLogoImages()
//                        model.image = 0
//                        model.status = false
//                        model.uriImage = selectedImage
                        try {
                            val bitmap2: Uri = data.data
//                    val bitmap =
//                        MediaStore.Images.Media.getBitmap(this.contentResolver, bitmap2)
                            val uri = Constant.uploadImage(bitmap2, context!!)
                            val file: File = Constant.uploadImage(bitmap2, context!!)
                            //..................................Api FOr Update Image from gallery....................................................................//
                            viewModel.setImage(file, "2")
                            progressBarr.visibility = View.VISIBLE
                            dismiss()
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }

                if (requestCode == 133) {
                    try {

                        val selectedImage: Uri = data!!.data
                        //val fileee = File(getPath(selectedImage))

                        val model = ModelCoverImages()
                        model.image = 0
                        model.status = false
                        model.uriImage = selectedImage
                        try {
                            val bitmap2: Uri = data.data
//                    val bitmap =
//                        MediaStore.Images.Media.getBitmap(this.contentResolver, bitmap2)
                            val uri = Constant.uploadImage(bitmap2, context!!)
                            val file: File = Constant.uploadImage(bitmap2, context!!)
                            //..................................Api FOr Update Image from gallery....................................................................//
                            viewModel.setImage(file, "1")
                            progressBarr.visibility = View.VISIBLE
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }

                        dismiss()
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
                dismiss()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }


        fun getPath(uri: Uri): String {
            val projection = arrayOf(MediaStore.Images.Media.DATA)
            val cursor =
                activity!!.contentResolver.query(uri, projection, null, null, null) ?: return ""
            val column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
            cursor.moveToFirst()
            val s = cursor.getString(column_index)
            cursor.close()
            return s
        }
    }


    private fun initz() {
//        circleImageView = findViewById(R.id.circleImageView)
        toolbar = findViewById(R.id.toolbarPictures)
        toolbarTitle = findViewById(R.id.title)
        done = findViewById(R.id.done)
    }

    override fun openMenuFragment() {
        val fragment = MenuFragment(
            pos,
            "top",
            binding
        )
        fragment.show(supportFragmentManager, fragment.tag)
    }

    override fun backgroundSelected(
        selected: Boolean,
        itemss: List<ModelCoverImages>
    ) {
        var checked = 0
        if (itemss.isNotEmpty()) {
            for (items in itemss) {
                if (items.status) {
                    checked++
                }
            }
            if (checked >= 3) {
                if (intent.hasExtra("create")) {
                    done.visibility = View.VISIBLE
                    done.background = resources.getDrawable(R.drawable.bg_seller_button_blue)
                    done.isEnabled = true
                }
            } else {
                done.visibility = View.VISIBLE
//                done.visibility = View.GONE
                done.background = resources.getDrawable(R.drawable.blue_button_background_opacity)
                done.isEnabled = false
            }

        } else {
            if (intent.hasExtra("create")) {
                done.visibility = View.VISIBLE
//                binding.done.visibility = View.GONE
                done.background = resources.getDrawable(R.drawable.blue_button_background_opacity)
                done.isEnabled = false
            }

        }

    }


    override fun deleteImage(position: Int, model: ModelCoverImages) {
        val fragment = MenuFragmentDeleteImage(model,progressBarr)
        fragment.show(supportFragmentManager, fragment.tag)

    }

    class MenuFragmentDeleteImage(

        var modelCover: ModelCoverImages,
       var  progressBarr: View
    ) : RoundedBottomSheetDialogFragment() {

        lateinit var mBinding: CreateConfirmImageBinding
        lateinit var viewModel: PicturesViewModel

        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            mBinding = DataBindingUtil.inflate(inflater, R.layout.create_confirm_image, container, false)
            viewModel = ViewModelProviders.of(this)[PicturesViewModel::class.java]



            return mBinding.root
        }

        override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
            super.onViewCreated(view, savedInstanceState)
            roundedImage()
            mBinding.yes.setOnClickListener {
                val auth = Constant.getPrefs(activity!!).getString(Constant.auth_code, "")
                viewModel.getDeleteImage(auth, modelCover.name)
                progressBarr.visibility = View.VISIBLE
                dismiss()
            }

            observerInit()
            mBinding.no.setOnClickListener {
                dismiss()
            }
        }

        private fun observerInit() {
            viewModel.getmDataFailure().observe(this, Observer {
                progressBarr.visibility = View.GONE
                if (it.msg == "Invalid auth code") {
                    Constant.commonAlert(activity!!)
                } else {
                    Toast.makeText(activity, it.msg, Toast.LENGTH_SHORT).show()
                }

            })

            viewModel.getmDataSuccess().observe(this, Observer {
                if (it.status == "true") {
                    dismiss()
                    progressBarr.visibility = View.GONE
                    startActivity(Intent(activity, ConfirmDeleteImageActivity::class.java))

//                    activity!!.finish()
                }

            })

        }


        private fun roundedImage() {
            val curveRadius = 50F

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                mBinding.imageViewTop.outlineProvider = object : ViewOutlineProvider() {

                    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                    override fun getOutline(view: View?, outline: Outline?) {
                        outline?.setRoundRect(
                            0,
                            0,
                            view!!.width,
                            view.height + curveRadius.toInt(),
                            curveRadius
                        )
                    }
                }
                mBinding.imageViewTop.clipToOutline = true
            }

        }

    }

    override fun backgroundSelectedLogo(
        position: Int,
        modelLogoImages: ModelLogoImages
    ) {
        pos = position
        val intent = Intent(this, ImageViewerActivity::class.java)
            .putExtra("model1", modelLogoImages)
        startActivityForResult(intent, 1)

    }


    private fun setToolbar() {
        setSupportActionBar(toolbar)
        title = ""
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)

        if (intent.hasExtra("profile")) {
            toolbarTitle.text = "Manage Pictures"
        } else {
            toolbarTitle.text = "Pictures"
        }

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }


    fun getImageUri(inContext: Context, inImage: Bitmap): Uri {
        val bytes = ByteArrayOutputStream()
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
        val path =
            MediaStore.Images.Media.insertImage(inContext.contentResolver, inImage, "Title", null)
        return Uri.parse(path)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        try {
            if (requestCode == 29) {
                val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, imageUri)
                //viewModel.updateModel(getImageUri(this, bitmap))
                val model = ModelLogoImages()
//                model.image = getImageUri(this, bitmap)
                val file = File(model.image.toString())
                viewModel.setImage(file, "1")
                progressBarr.visibility = View.VISIBLE
                backgroundLogoChange.add(0, model)
                mAdapterLogoImages.update2(backgroundLogoChange)
            }

            if (resultCode == 1) {
                val image = data!!.getParcelableExtra<ModelCoverImages>("model")
                if (image.status) {
                    for (i in 0 until backgroundChange.size) {
                        backgroundChange[i].status = false
                    }

                    backgroundChange.removeAt(pos)
                    backgroundChange.add(0, image)
                }
                mAdapterCoverImages.update(backgroundChange)
                binding.recyclerCoverImages.scrollToPosition(0)
            }

            if (resultCode == 2) {
                val image = data!!.getParcelableExtra<ModelLogoImages>("model1")
                if (image.status) {
                    for (i in 0 until backgroundLogoChange.size) {
                        backgroundLogoChange[i].status = false
                    }

                    backgroundLogoChange.removeAt(pos)
                    backgroundLogoChange.add(0, image)
                }
                mAdapterLogoImages.update2(backgroundLogoChange)
                binding.recyclerLogoImages.scrollToPosition(0)

            }
            if (requestCode == 39) {
                try {
                    val model = ModelCoverImages()
                    model.image = 0
                    model.status = false
                    model.uriImage = imageUri

                    backgroundChange.add(0, model)
                    mAdapterCoverImages.update(backgroundChange)
                    Toast.makeText(this, "Image Saved!", Toast.LENGTH_SHORT).show()
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}
