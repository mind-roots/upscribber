package com.upscribber.upscribberSeller.pictures

import android.app.Application
import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.upscribber.upscribberSeller.pictures.coverImages.ModelCoverImages
import com.upscribber.upscribberSeller.pictures.logoImages.ModelLogoImages
import com.upscribber.commonClasses.Constant
import com.upscribber.commonClasses.ModelStatusMsg
import com.upscribber.commonClasses.WebServicesMerchants
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import java.io.File


class PicturesRepository(var application: Application) {
    val mData = MutableLiveData<List<ModelLogoImages>>()
    val arrayList = ArrayList<ModelLogoImages>()
    val mDataFailure = MutableLiveData<ModelStatusMsg>()
    val mDataSuccess = MutableLiveData<ModelStatusMsg>()
    val mData2 = MutableLiveData<ArrayList<ModelCoverImages>>()
    val mDataLogo = MutableLiveData<ArrayList<ModelLogoImages>>()
    val mStatus = MutableLiveData<ModelStatusMsg>()


    fun getImagesCover() {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()
        val auth = Constant.getPrefs(application).getString(Constant.auth_code, "")

        val service = retrofit.create(WebServicesMerchants::class.java)
        val call: Call<ResponseBody> = service.getPhotos(auth)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                try {
                    val res = response.body()!!.string()
                    val json = JSONObject(res)
                    val status = json.optString("status")
                    val msg = json.optString("msg")
                    val data = json.optJSONObject("data")

                    if (status == "true") {
                        val arrayList2 = ArrayList<ModelCoverImages>()
                        val arrayListLogo = ArrayList<ModelLogoImages>()
                        val images = data.optJSONArray("images")
                        val logo = data.optJSONArray("logo")
                        for (j in 0 until images.length()) {
                            val modelCoverImages = ModelCoverImages()
                            val getImages = images.optJSONObject(j)
                            modelCoverImages.getImage = getImages.optString("image_url")
                            modelCoverImages.id = getImages.optString("id")
                            modelCoverImages.user_id = getImages.optString("user_id")
                            modelCoverImages.business_id = getImages.optString("business_id")
                            modelCoverImages.type = getImages.optString("type")
                            modelCoverImages.created_at = getImages.optString("created_at")
                            modelCoverImages.updated_at = getImages.optString("updated_at")
                            modelCoverImages.name = getImages.optString("image_name")
                            arrayList2.add(modelCoverImages)
                        }
                        mData2.value = arrayList2

                        for (j in 0 until logo.length()) {
                            val modelCoverImages = ModelLogoImages()
                            val getLogoImages = logo.optJSONObject(j)
                            modelCoverImages.getImage = getLogoImages.optString("image_url")
                            modelCoverImages.id = getLogoImages.optString("id")
                            modelCoverImages.user_id = getLogoImages.optString("user_id")
                            modelCoverImages.business_id = getLogoImages.optString("business_id")
                            modelCoverImages.type = getLogoImages.optString("type")
                            modelCoverImages.created_at = getLogoImages.optString("created_at")
                            modelCoverImages.updated_at = getLogoImages.optString("updated_at")
                            modelCoverImages.name = getLogoImages.optString("image_name")
                            arrayListLogo.add(modelCoverImages)
                        }

                        mDataLogo.value = arrayListLogo

                    } else {
                        val modelStatus = ModelStatusMsg()
                        modelStatus.status = "false"
                        modelStatus.msg = "Network Error!"
                        mDataFailure.value = modelStatus
                    }


                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus
            }

        })


    }


    fun setImagesCover(uri: File, type: String) {

        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()
        val auth = Constant.getPrefs(application).getString(Constant.auth_code, "")
        val service = retrofit.create(WebServicesMerchants::class.java)
        //..........................technique to storing the image in the web server......................//

        //pass it like this
        val file = uri
        val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file)
        // MultipartBody.Part is used to send also the actual file name
        val body = MultipartBody.Part.createFormData("image", file.name, requestFile)

// add another part within the multipart request
        val fullName = RequestBody.create(MediaType.parse("multipart/form-data"), auth)
        val type = RequestBody.create(MediaType.parse("multipart/form-data"), type)// type

        val call: Call<ResponseBody> = service.addPhoto(body, fullName, type)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {

                if (response.isSuccessful) {
                    val res = response.body()!!.string()
                    val json = JSONObject(res)
                    val status = json.optString("status")
                    val msg = json.optString("msg")

                    val model = ModelStatusMsg()
                    model.status = status
                    model.msg = msg
                    mStatus.value = model
                } else {
                    val modelStatus = ModelStatusMsg()
                    modelStatus.status = "false"
                    modelStatus.msg = "Network Error!"
                    mDataFailure.value = modelStatus
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus
            }
        })
    }


    fun mDataCoverImages(): MutableLiveData<ArrayList<ModelCoverImages>> {
        return mData2
    }

    fun mDataLogoImages(): MutableLiveData<ArrayList<ModelLogoImages>> {
        return mDataLogo
    }

    fun getmDataSuccess(): MutableLiveData<ModelStatusMsg> {
        return mDataSuccess
    }


    fun mStatusData(): MutableLiveData<ModelStatusMsg> {
        return mStatus
    }

    fun getmDataFailure(): LiveData<ModelStatusMsg> {
        return mDataFailure
    }

    fun getDeleteImage(auth: String, name: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesMerchants::class.java)
        val call: Call<ResponseBody> = service.deleteImage(auth, name)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                try {
                    val res = response.body()!!.string()
                    val json = JSONObject(res)
                    val status = json.optString("status")
                    val msg = json.optString("msg")
                    val data = json.optJSONObject("data")
                    val modelStatusMsg = ModelStatusMsg()
                    if (status == "true") {
                        modelStatusMsg.msg = msg
                        modelStatusMsg.status = status
                        mDataSuccess.value = modelStatusMsg
                    } else {
                        modelStatusMsg.msg = msg
                        modelStatusMsg.status = status
                        mDataFailure.value = modelStatusMsg
                    }


                } catch (e: Exception) {
                    e.printStackTrace()

                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus
            }
        })
    }

}