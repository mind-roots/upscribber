package com.upscribber.upscribberSeller.pictures.logoImages

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.upscribber.R
import com.upscribber.databinding.LogoImagesLayoutBinding
import kotlinx.android.synthetic.main.logo_images_layout.view.*

class AdapterLogoImages(var context: Context) :
    RecyclerView.Adapter<AdapterLogoImages.ViewHolder>() {

    private var itemss: ArrayList<ModelLogoImages> = ArrayList()
    var listener = context as selection

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.logo_images_layout, parent, false)
        return ViewHolder(view)
//        mBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.logo_images_layout, parent, false)
//        return AdapterLogoImages.ViewHolder(mBinding.root)

    }

    override fun getItemCount(): Int {
        return itemss.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        try {
            val model = itemss[position]
            if (position == itemss.size - 1) {
                holder.itemView.firstView.visibility = View.VISIBLE
                holder.itemView.constraintLayout28.visibility = View.GONE
            } else {
                holder.itemView.firstView.visibility = View.GONE
                holder.itemView.constraintLayout28.visibility = View.VISIBLE
                Glide.with(context).load(model.getImage).placeholder(R.mipmap.circular_placeholder)
                    .into(holder.itemView.imageView)


                if (model.status) {
                    holder.itemView.imageGradient.visibility = View.VISIBLE
                } else {
                    holder.itemView.imageGradient.visibility = View.INVISIBLE
                }
            }


            holder.itemView.setOnClickListener {
                if (position == itemss.size - 1) {
                    listener.openMenuFragment()
                } else {
                    listener.backgroundSelectedLogo(position, itemss[position])
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }


    fun update(items: ArrayList<ModelLogoImages>) {
        itemss.clear()
        itemss.addAll(items)
        notifyDataSetChanged()
    }

    fun update2(items: ArrayList<ModelLogoImages>) {
        itemss = items
        notifyDataSetChanged()
    }


    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)

    interface selection {
        fun backgroundSelectedLogo(
            position: Int,
            modelLogoImages: ModelLogoImages
        )

        fun openMenuFragment()
    }

}