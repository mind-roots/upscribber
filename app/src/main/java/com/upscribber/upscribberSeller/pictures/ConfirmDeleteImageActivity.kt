package com.upscribber.upscribberSeller.pictures

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import com.upscribber.R

class ConfirmDeleteImageActivity : AppCompatActivity() {

    lateinit var doneLinking: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_confirm_delete_image)
        doneLinking = findViewById(R.id.doneLinking)
        doneLinking.setOnClickListener {
            finish()
        }
    }
}
