package com.upscribber.upscribberSeller.pictures

import android.app.Application
import android.net.Uri
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.upscribber.commonClasses.ModelStatusMsg
import com.upscribber.upscribberSeller.pictures.coverImages.ModelCoverImages
import com.upscribber.upscribberSeller.pictures.logoImages.ModelLogoImages
import java.io.File

/**
 * Created by amrit on 17/4/19.
 */
class PicturesViewModel(application: Application) : AndroidViewModel(application) {

    var picturesViewModel: PicturesRepository = PicturesRepository(application)

    fun getCoverImages() {
        return picturesViewModel.getImagesCover()
    }

    fun setImage(uri : File,type:String){
        picturesViewModel.setImagesCover(uri,type)
    }



    fun getmDataCoverImages(): MutableLiveData<ArrayList<ModelCoverImages>> {
        return  picturesViewModel.mDataCoverImages()
    }

    fun getmDataLogoImages(): MutableLiveData<ArrayList<ModelLogoImages>> {
        return  picturesViewModel.mDataLogoImages()
    }


    fun getIfImageUploaded(): MutableLiveData<ModelStatusMsg> {
        return picturesViewModel.mStatusData()
    }

     fun getmDataFailure(): LiveData<ModelStatusMsg> {
        return picturesViewModel.getmDataFailure()
    }

     fun getmDataSuccess(): LiveData<ModelStatusMsg> {
        return picturesViewModel.getmDataSuccess()
    }



    fun getDeleteImage(auth: String, name: String) {
        picturesViewModel.getDeleteImage(auth,name)
    }

}