package com.upscribber.upscribberSeller.pictures.coverImages

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.upscribber.R
import com.upscribber.databinding.CoverImagesLayoutBinding
import com.upscribber.upscribberSeller.pictures.ImageViewerActivity
import kotlinx.android.synthetic.main.cover_images_layout.view.*

class AdapterCoverImages(
    var context: Context,
    var stringExtra: String
) :
    RecyclerView.Adapter<AdapterCoverImages.ViewHolder>() {

    private var itemss: List<ModelCoverImages> = emptyList()
    lateinit var mBinding: CoverImagesLayoutBinding
    var listener = context as SelectionCover
    var deleteOrNot: Int = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.cover_images_layout, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return itemss.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = itemss[position]

//        if (stringExtra == "others") {
////            if (itemss.size <= 3){
//                model.status = true
////            }
//        }

        Glide.with(context).load(model.getImage)
            .placeholder(R.mipmap.placeholder_subscription_square).into(holder.itemView.imageView)
        if (deleteOrNot == 1) {
            holder.itemView.imgCross.visibility = View.VISIBLE
            if (stringExtra == "others") {
                holder.itemView.isEnabled = false
            }
        } else {
            holder.itemView.isEnabled = true
            holder.itemView.imgCross.visibility = View.GONE
        }


        holder.itemView.setOnClickListener {
            if (stringExtra == "others") {
                itemss[position].status = !itemss[position].status
                var selected = false
                for (element in itemss) {
                    if (element.status) {
                        selected = true
                    }
                }
                listener.backgroundSelected(selected, itemss)
                notifyDataSetChanged()
            } else {
                context.startActivity(
                    Intent(
                        context,
                        ImageViewerActivity::class.java
                    ).putExtra("model", model)
                )
            }
        }

        if (model.status) {
            holder.itemView.imageGradient.visibility = View.INVISIBLE
            holder.itemView.imgTick.visibility = View.VISIBLE
        } else {
            holder.itemView.imageGradient.visibility = View.INVISIBLE
            holder.itemView.imgTick.visibility = View.GONE
        }

        holder.itemView.imgCross.setOnClickListener {
            listener.deleteImage(position, model)
        }

    }


    fun update(items: List<ModelCoverImages>) {
        this.itemss = items
        notifyDataSetChanged()
    }

    fun showDeleteButton(i: Int) {
        deleteOrNot = i
        notifyDataSetChanged()
    }

    fun getSelectedItems(): java.util.ArrayList<ModelCoverImages> {
        val selectedItems: ArrayList<ModelCoverImages> = ArrayList()
        for (i in itemss.indices) {
            if (itemss[i].status) {
                selectedItems.add(itemss[i])
            }
        }
        return selectedItems
    }


    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)

    interface SelectionCover {
        fun backgroundSelected(
            selected: Boolean,
            itemss: List<ModelCoverImages>
        )

        fun deleteImage(
            position: Int,
            model: ModelCoverImages
        )
    }


}