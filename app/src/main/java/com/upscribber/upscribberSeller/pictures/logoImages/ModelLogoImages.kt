package com.upscribber.upscribberSeller.pictures.logoImages

import android.net.Uri
import android.os.Parcel
import android.os.Parcelable

/**
 * Created by amrit on 17/4/19.
 */
class ModelLogoImages() : Parcelable {

    var updated_at: String = ""
    var created_at: String = ""
    var type: String = ""
    var business_id: String = ""
    var user_id: String = ""
    var id: String = ""
    var image: Int = 0
    var getImage: String = ""
    var status: Boolean = false
    var uriImage: Uri? = null
    var name: String = ""

    constructor(parcel: Parcel) : this() {
        updated_at = parcel.readString()
        created_at = parcel.readString()
        type = parcel.readString()
        business_id = parcel.readString()
        user_id = parcel.readString()
        id = parcel.readString()
        image = parcel.readInt()
        getImage = parcel.readString()
        status = parcel.readByte() != 0.toByte()
        uriImage = parcel.readParcelable(Uri::class.java.classLoader)
        name = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(updated_at)
        parcel.writeString(created_at)
        parcel.writeString(type)
        parcel.writeString(business_id)
        parcel.writeString(user_id)
        parcel.writeString(id)
        parcel.writeInt(image)
        parcel.writeString(getImage)
        parcel.writeByte(if (status) 1 else 0)
        parcel.writeParcelable(uriImage, flags)
        parcel.writeString(name)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ModelLogoImages> {
        override fun createFromParcel(parcel: Parcel): ModelLogoImages {
            return ModelLogoImages(parcel)
        }

        override fun newArray(size: Int): Array<ModelLogoImages?> {
            return arrayOfNulls(size)
        }
    }

}