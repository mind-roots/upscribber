package com.upscribber.upscribberSeller.pictures

import android.content.Intent
import android.graphics.Outline
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.widget.Toolbar
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.upscribber.upscribberSeller.pictures.coverImages.ModelCoverImages
import com.upscribber.upscribberSeller.pictures.logoImages.ModelLogoImages
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.databinding.CreateConfirmImageBinding
import com.upscribber.commonClasses.RoundedBottomSheetDialogFragment
import kotlinx.android.synthetic.main.activity_image_viewer.*

class ImageViewerActivity : AppCompatActivity() {

    private lateinit var toolbar: Toolbar
    lateinit var viewModel: PicturesViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image_viewer)
        viewModel = ViewModelProviders.of(this)[PicturesViewModel::class.java]
        initz()
        setToolbar()


        if (intent.hasExtra("model")) {
            val image = intent.getParcelableExtra<ModelCoverImages>("model")
            Glide.with(this).load(image.getImage).into(imageViwer)
//            if (intent.getParcelableExtra<ModelCoverImages>("model").getImage == "0") {
//                imageViwer.setImageURI(intent.getParcelableExtra<ModelCoverImages>("model").uriImage)
//
//            } else {
//                imageViwer.setImageResource(intent.getParcelableExtra<ModelCoverImages>("model").image)
//            }
            setCover.text = resources.getString(R.string.set_photo_as_cover_image)
        } else if ((intent.hasExtra("model1"))) {
            val image = intent.getParcelableExtra<ModelLogoImages>("model1")
            Glide.with(this).load(image.getImage).into(imageViwer)
            setCover.text = resources.getString(R.string.set_photo_as_logo_image)
        }


        setCover.setOnClickListener {
            if (intent.hasExtra("model")) {
                val model = intent.getParcelableExtra<ModelCoverImages>("model")
                model.status = true
                val intent = Intent(this, PicturesSellerActivity::class.java)
                intent.putExtra("model", model)
                setResult(1, intent)
                finish()
            } else if ((intent.hasExtra("model1"))) {

//                val model1 = intent.getParcelableExtra<ModelLogoImages>("model1")
//                model1.status = true
//                val intent = Intent(this, PicturesSellerActivity::class.java)
//                intent.putExtra("model1", model1)
//                setResult(2, intent)
//                finish()
            }
        }


//        if (intent.hasExtra("model1")) {
//            imageViwer.setImageURI(intent.getParcelableExtra<ModelLogoImages>("model1").uriImage)
//            setCover.text = resources.getString(R.string.set_photo_as_logo_image)
//        }

    }

    private fun setToolbar() {

        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white)
    }

    private fun initz() {
        toolbar = findViewById(R.id.toolbarImage)

    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.image_viewer_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }

        if (item.itemId == R.id.delete) {
            if (intent.hasExtra("model")) {
                val modelCover = intent.getParcelableExtra<ModelCoverImages>("model")
                val fragment = MenuFragmentDelete(viewModel, modelCover)
                fragment.show(supportFragmentManager, fragment.tag)
            }else if (intent.hasExtra("model1")) {
                val modelLogo = intent.getParcelableExtra<ModelLogoImages>("model1")
                val fragment = MenuFragmentDelete1(viewModel, modelLogo)
                fragment.show(supportFragmentManager, fragment.tag)
                }


        }
        return super.onOptionsItemSelected(item)
    }

    class MenuFragmentDelete(
        var viewModel: PicturesViewModel,
        var modelCover: ModelCoverImages
    ) : RoundedBottomSheetDialogFragment() {
        lateinit var mBinding: CreateConfirmImageBinding


        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            mBinding =
                DataBindingUtil.inflate(inflater, R.layout.create_confirm_image, container, false)

            return mBinding.root
        }

        override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
            super.onViewCreated(view, savedInstanceState)
            roundedImage()
            mBinding.yes.setOnClickListener {
                val auth = Constant.getPrefs(activity!!).getString(Constant.auth_code, "")
                viewModel.getDeleteImage(auth, modelCover.name)
            }

            observerInit()
            mBinding.no.setOnClickListener {
                dismiss()
            }
        }

        private fun observerInit() {
            viewModel.getmDataFailure().observe(this, Observer {
                if (it.msg == "Invalid auth code") {
                    Constant.commonAlert(activity!!)
                } else {
                    Toast.makeText(activity, it.msg, Toast.LENGTH_SHORT).show()
                }

            })

            viewModel.getmDataSuccess().observe(this, Observer {
                if (it.status == "true"){
                    dismiss()
                    activity!!.finish()
                }

            })

        }


        private fun roundedImage() {
            val curveRadius = 50F

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                mBinding.imageViewTop.outlineProvider = object : ViewOutlineProvider() {

                    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                    override fun getOutline(view: View?, outline: Outline?) {
                        outline?.setRoundRect(
                            0,
                            0,
                            view!!.width,
                            view.height + curveRadius.toInt(),
                            curveRadius
                        )
                    }
                }
                mBinding.imageViewTop.clipToOutline = true
            }

        }

    }




    class MenuFragmentDelete1(
        var viewModel: PicturesViewModel,
        var modelCover: ModelLogoImages
    ) : RoundedBottomSheetDialogFragment() {
        lateinit var mBinding: CreateConfirmImageBinding


        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            mBinding =
                DataBindingUtil.inflate(inflater, R.layout.create_confirm_image, container, false)

            return mBinding.root
        }

        override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
            super.onViewCreated(view, savedInstanceState)
            roundedImage()
            mBinding.yes.setOnClickListener {
                val auth = Constant.getPrefs(activity!!).getString(Constant.auth_code, "")
                viewModel.getDeleteImage(auth, modelCover.name)
//                startActivity(
//                    Intent(
//                        context!!,
//                        ConfirmDeleteImageActivity::class.java
//                    )
//                )
//                activity!!.finish()
//                dismiss()
            }

            observerInit()
            mBinding.no.setOnClickListener {
                dismiss()
            }
        }

        private fun observerInit() {
            viewModel.getmDataFailure().observe(this, Observer {
                if (it.msg == "Invalid auth code") {
                    Constant.commonAlert(activity!!)
                } else {
                    Toast.makeText(activity, it.msg, Toast.LENGTH_SHORT).show()
                }

            })

            viewModel.getmDataSuccess().observe(this, Observer {
                if (it.status == "true"){
                    dismiss()
                    activity!!.finish()
                }

            })

        }


        private fun roundedImage() {
            val curveRadius = 50F

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                mBinding.imageViewTop.outlineProvider = object : ViewOutlineProvider() {

                    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                    override fun getOutline(view: View?, outline: Outline?) {
                        outline?.setRoundRect(
                            0,
                            0,
                            view!!.width,
                            view.height + curveRadius.toInt(),
                            curveRadius
                        )
                    }
                }
                mBinding.imageViewTop.clipToOutline = true
            }

        }

    }

}
