package com.upscribber.upscribberSeller.store.storeMain

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.commonClasses.Constant
import com.upscribber.upscribberSeller.store.storeInfo.StoreProductInfoActivity
import com.upscribber.databinding.LayoutStoreFragmentListBinding
import com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.createSubs.CreateSubscriptionActivity
import com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.createSubs.CreateSubscriptionActivity.Companion.editData
import com.upscribber.upscribberSeller.subsriptionSeller.subscriptionMain.ModelMain
import kotlinx.android.synthetic.main.layout_store_fragment_list.view.*

class StoreAdapter(var context: Context) : RecyclerView.Adapter<StoreAdapter.MyViewHolder>() {
    private lateinit var binding: LayoutStoreFragmentListBinding
    private var items = ArrayList<StoreModel>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        binding =
            LayoutStoreFragmentListBinding.inflate(LayoutInflater.from(context), parent, false)
        return MyViewHolder(binding)

    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = items[position]
        holder.bind(model)

        holder.itemView.setOnClickListener {
            context.startActivity(
                Intent(
                    context,
                    StoreProductInfoActivity::class.java
                ).putExtra("modelStore", model)
            )
        }

//        if (model.subscriber == "0") {
//            holder.itemView.txtCreateSubscription.visibility = View.VISIBLE
//        } else {
//            holder.itemView.txtCreateSubscription.visibility = View.GONE
//        }

        holder.itemView.txtCreateSubscription.setOnClickListener {
            val mPrefs = context.getSharedPreferences("data", AppCompatActivity.MODE_PRIVATE)
            val prefsEditor = mPrefs.edit()
            prefsEditor.putString("detailsArray", "")
            prefsEditor.putString("pricing", "")
            prefsEditor.putString("basicData", "")
            prefsEditor.putString("campaign_id", "")
            prefsEditor.putString("bought", "")
            prefsEditor.apply()
            editData = ModelMain()
            context.startActivity(
                Intent(
                    context,
                    CreateSubscriptionActivity::class.java
                ).putExtra("productsData", model).putExtra("product","product")
            )
        }

    }


    class MyViewHolder(var binding: LayoutStoreFragmentListBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(obj: StoreModel) {
            binding.model = obj
            binding.executePendingBindings()
        }
    }

    fun update(items: ArrayList<StoreModel>) {
        this.items = items
        notifyDataSetChanged()
    }
}
