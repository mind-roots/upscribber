package com.upscribber.upscribberSeller.store.storeInfo

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.upscribber.commonClasses.ModelStatusMsg

class StoreSubsInfoViewModel(application: Application) : AndroidViewModel(application) {


    var storeSubsInfoRepository : StoreSubsInfoRepository =
        StoreSubsInfoRepository(application)

    fun getProductInfo(auth: String, id: String) {
        storeSubsInfoRepository.getProductInfo(auth,id)

    }

    fun getmDataStoreInfo() : LiveData<ModelMainStoreInfo>{
        return  storeSubsInfoRepository.getmDataStoreInfo()
    }

     fun getmDataStatus() : LiveData<ModelStatusMsg>{
        return  storeSubsInfoRepository.getmDataStatus()
    }



}