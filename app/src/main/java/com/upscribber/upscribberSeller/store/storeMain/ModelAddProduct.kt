package com.upscribber.upscribberSeller.store.storeMain

import android.os.Parcel
import android.os.Parcelable

class ModelAddProduct() : Parcelable{

 var productPrice: String = ""
 var productDescription: String = ""
 var productName: String = ""
 var status : String = ""
 var msg : String = ""

 constructor(parcel: Parcel) : this() {
  productPrice = parcel.readString()
  productDescription = parcel.readString()
  productName = parcel.readString()
  status = parcel.readString()
  msg = parcel.readString()
 }

 override fun writeToParcel(parcel: Parcel, flags: Int) {
  parcel.writeString(productPrice)
  parcel.writeString(productDescription)
  parcel.writeString(productName)
  parcel.writeString(status)
  parcel.writeString(msg)
 }

 override fun describeContents(): Int {
  return 0
 }

 companion object CREATOR : Parcelable.Creator<ModelAddProduct> {
  override fun createFromParcel(parcel: Parcel): ModelAddProduct {
   return ModelAddProduct(parcel)
  }

  override fun newArray(size: Int): Array<ModelAddProduct?> {
   return arrayOfNulls(size)
  }
 }
}
