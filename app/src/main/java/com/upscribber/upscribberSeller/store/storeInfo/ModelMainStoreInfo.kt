package com.upscribber.upscribberSeller.store.storeInfo

import android.os.Parcel
import android.os.Parcelable
import com.upscribber.filters.ModelSubCategory
import com.upscribber.subsciptions.merchantStore.merchantDetails.ModeTags

class ModelMainStoreInfo() : Parcelable{

    var arrayMainStoreInfo: ArrayList<StoreInfoModel> = ArrayList()
    var arrayStoreSubscription: ArrayList<StoreSubsInfoModel> = ArrayList()
    var modelTags: ArrayList<ModeTags> = ArrayList()
    var arrayParentTags: ArrayList<ModelSubCategory> = ArrayList()
    var status: String = ""
    var msg: String = ""

    constructor(parcel: Parcel) : this() {
        arrayMainStoreInfo = parcel.createTypedArrayList(StoreInfoModel)
        arrayStoreSubscription = parcel.createTypedArrayList(StoreSubsInfoModel)
        modelTags = parcel.createTypedArrayList(ModeTags)
        arrayParentTags = parcel.createTypedArrayList(ModelSubCategory)
        status = parcel.readString()
        msg = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeTypedList(arrayMainStoreInfo)
        parcel.writeTypedList(arrayStoreSubscription)
        parcel.writeTypedList(modelTags)
        parcel.writeTypedList(arrayParentTags)
        parcel.writeString(status)
        parcel.writeString(msg)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ModelMainStoreInfo> {
        override fun createFromParcel(parcel: Parcel): ModelMainStoreInfo {
            return ModelMainStoreInfo(parcel)
        }

        override fun newArray(size: Int): Array<ModelMainStoreInfo?> {
            return arrayOfNulls(size)
        }
    }

}
