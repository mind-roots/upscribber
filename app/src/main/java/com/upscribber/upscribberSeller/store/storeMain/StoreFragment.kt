package com.upscribber.upscribberSeller.store.storeMain

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.upscribber.upscribberSeller.subsriptionSeller.extras.ModelExtra
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.databinding.FragmentStoreBinding
import com.upscribber.upscribberSeller.store.storeInfo.AddProductActivity

class StoreFragment : Fragment() {

    private lateinit var mAdapter: StoreAdapter
    private lateinit var model: ArrayList<ModelExtra>
    private lateinit var mAdapterExtras: StoreFragmentAdapter
    lateinit var mBinding: FragmentStoreBinding
    var storeModel: ArrayList<StoreModel> = ArrayList()
    private lateinit var viewModel: StoreViewModel
    private lateinit var listener: switchFrag

    override fun onCreateView(
        inflater: LayoutInflater, container:
        ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_store, container, false)
        setHasOptionsMenu(false)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProviders.of(this)[StoreViewModel::class.java]
        apiImplimentation()
        setAdapter()
        clickListeners()
        observerInit()
    }

    private fun clickListeners() {
        mBinding.getStartedBtn.setOnClickListener {
            startActivity(Intent(activity, AddProductActivity::class.java))
        }
    }

    private fun observerInit() {
        viewModel.getAllProducts().observe(activity!!, Observer {
            if (it.size > 0) {
                mBinding.recyclerStoreProducts.visibility = View.VISIBLE
                mBinding.dummyTxt.visibility = View.GONE
                mBinding.card.visibility = View.GONE
                mBinding.card1.visibility = View.GONE
                mBinding.progressBar.visibility = View.GONE
                mBinding.imageView84.visibility = View.GONE
                mBinding.tvNoData.visibility = View.GONE
                mBinding.tvNoDataDescription.visibility = View.GONE
                mBinding.getStartedBtn.visibility = View.GONE
                storeModel = it
                it.reverse()
                mAdapter.update(it)
            } else {
                mBinding.dummyTxt.visibility = View.GONE
                mBinding.card.visibility = View.GONE
                mBinding.progressBar.visibility = View.GONE
                mBinding.card1.visibility = View.GONE
                mBinding.imageView84.visibility = View.VISIBLE
                mBinding.tvNoData.visibility = View.VISIBLE
                mBinding.tvNoDataDescription.visibility = View.VISIBLE

                val type = Constant.getPrefs(activity!!).getString(Constant.type, "")
                if (type == "3") {
                    mBinding.getStartedBtn.visibility = View.GONE
                } else {
                    mBinding.getStartedBtn.visibility = View.VISIBLE
                }

                mBinding.recyclerStoreProducts.visibility = View.GONE
            }
        })

        viewModel.getStatus().observe(this, Observer {
            if (it.msg == "Invalid auth code") {
                Constant.commonAlert(activity!!)
            } else {
                Toast.makeText(activity, it.msg, Toast.LENGTH_SHORT).show()
            }
        })

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        listener = context as switchFrag
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        listener = activity as switchFrag
    }

    private fun setAdapter() {
        mBinding.recyclerStoreProducts.layoutManager = LinearLayoutManager(activity!!)
        mAdapter = StoreAdapter(activity!!)
        mBinding.recyclerStoreProducts.adapter = mAdapter

    }

    private fun apiImplimentation() {
        viewModel.getProducts("")
        mBinding.progressBar.visibility = View.VISIBLE
    }

    override fun onResume() {
        super.onResume()
        viewModel.getProducts("")
        mBinding.progressBar.visibility = View.VISIBLE

        val sharedPreferences1 = activity!!.getSharedPreferences("done", Context.MODE_PRIVATE)
        if (sharedPreferences1.getString("product", "").isNotEmpty()) {
            val editor = sharedPreferences1.edit()
            editor.remove("product")
            editor.remove("done")
            editor.apply()
            listener.switch()
        }
    }

    interface switchFrag {
        fun switch()
    }

}


