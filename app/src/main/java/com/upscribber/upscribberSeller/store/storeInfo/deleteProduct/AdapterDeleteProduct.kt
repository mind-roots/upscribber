package com.upscribber.upscribberSeller.store.storeInfo.deleteProduct

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.R
import com.upscribber.databinding.DeleteProductRecyclerLayoutBinding

class AdapterDeleteProduct(var context: Context) : RecyclerView.Adapter<AdapterDeleteProduct.ViewHolder>() {

    private var itemss: List<ModelProductDeleted> = emptyList()
    lateinit var mBinding : DeleteProductRecyclerLayoutBinding


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.delete_product_recycler_layout, parent,false)
        return ViewHolder(mBinding.root)

    }

    override fun getItemCount(): Int {
        return  itemss.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = itemss[position]
        mBinding.model = model

    }

    fun update(items: List<ModelProductDeleted>) {
        this.itemss = items
        notifyDataSetChanged()
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)
}