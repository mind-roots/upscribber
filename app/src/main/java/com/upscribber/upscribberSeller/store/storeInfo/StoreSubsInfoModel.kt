package com.upscribber.upscribberSeller.store.storeInfo

import android.os.Parcel
import android.os.Parcelable
import java.math.BigDecimal
import java.math.RoundingMode
import kotlin.math.roundToInt

class StoreSubsInfoModel() : Parcelable {

    var campaign_id: String = ""
    var id: String = ""
    var name: String = ""
    var price: String = ""
    var discount_price: String = ""
    var frequency_type: String = ""
    var description: String = ""
    var unit: String = ""

    constructor(parcel: Parcel) : this() {
        campaign_id = parcel.readString()
        id = parcel.readString()
        name = parcel.readString()
        price = parcel.readString()
        discount_price = parcel.readString()
        frequency_type = parcel.readString()
        description = parcel.readString()
        unit = parcel.readString()
    }

    fun price(): String {
        val splitPos = price.split(".")
        var priceActual = ""
        priceActual = if (splitPos[1] == "00" || splitPos[1] == "0") {
            "$" + splitPos[0]
        } else {
            "$" + BigDecimal(price).setScale(2, RoundingMode.HALF_UP)
        }

        return priceActual
    }

//    fun discount(): String {
//        return discount_price.toDouble().toInt().toString() + "% off"
//    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(campaign_id)
        parcel.writeString(id)
        parcel.writeString(name)
        parcel.writeString(price)
        parcel.writeString(discount_price)
        parcel.writeString(frequency_type)
        parcel.writeString(description)
        parcel.writeString(unit)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<StoreSubsInfoModel> {
        override fun createFromParcel(parcel: Parcel): StoreSubsInfoModel {
            return StoreSubsInfoModel(parcel)
        }

        override fun newArray(size: Int): Array<StoreSubsInfoModel?> {
            return arrayOfNulls(size)
        }
    }

}