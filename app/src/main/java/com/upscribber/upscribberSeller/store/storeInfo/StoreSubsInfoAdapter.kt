package com.upscribber.upscribberSeller.store.storeInfo

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.upscribberSeller.subsriptionSeller.subscriptionView.ActivitySubscriptionSeller
import com.upscribber.databinding.StoreProductSubscriptionListBinding
import kotlinx.android.synthetic.main.store_product_subscription_list.view.*

class StoreSubsInfoAdapter(var context: Context) : RecyclerView.Adapter<StoreSubsInfoAdapter.MyViewHolder>() {

    private  var items: ArrayList<StoreSubsInfoModel> = ArrayList()
    lateinit var mBinding : StoreProductSubscriptionListBinding


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        mBinding = StoreProductSubscriptionListBinding.inflate(LayoutInflater.from(context), parent,false)
        return MyViewHolder(mBinding)


    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = items[position]
        holder.bind(model)

        if (model.discount_price == "0" || model.discount_price == "0.0" ||model.discount_price == "0.00"){
            holder.itemView.tv_off.visibility = View.GONE
        }else{
            holder.itemView.tv_off.visibility = View.GONE
            holder.itemView.tv_off.text = model.discount_price.toDouble().toInt().toString() + "% off"
        }


        holder.itemView.setOnClickListener {
            context.startActivity(Intent(context,ActivitySubscriptionSeller::class.java).putExtra("campaignId",model.campaign_id))
        }

    }


    class MyViewHolder(var  binding: StoreProductSubscriptionListBinding): RecyclerView.ViewHolder(binding.root) {

        fun bind(obj: StoreSubsInfoModel) {
            binding.model = obj
            binding.executePendingBindings()
        }
    }

    fun update(items: ArrayList<StoreSubsInfoModel>) {
        this.items = items
        notifyDataSetChanged()
    }
}