package com.upscribber.upscribberSeller.store.storeMain

import android.os.Parcel
import android.os.Parcelable
import kotlin.math.roundToInt

class StoreModel() : Parcelable {

    var eTDiscountPrice: String = ""
    var product_id: String = ""
    var productPrice: String = ""
    var productDescription: String = ""
    var productName: String = ""
    var msg: String = ""
    var status: Boolean = false
    var id: String = ""
    var name: String = ""
    var description: String = ""
    var price: String = ""
    var discount_price: String = ""
    var percentage: String = ""
    var business_id: String = ""
    var user_id: String = ""
    var image: String = ""
    var tags: String = ""
    var gross_revenue: String = ""
    var campaign_count: String = ""
    var gross_sales: String = ""
    var created_by: String = ""
    var created_at: String = ""
    var updated_at: String = ""
    var tags_name: String = ""
    var subscriber: String = ""
    var status1: String = ""

    constructor(parcel: Parcel) : this() {
        eTDiscountPrice = parcel.readString()
        product_id = parcel.readString()
        productPrice = parcel.readString()
        productDescription = parcel.readString()
        productName = parcel.readString()
        msg = parcel.readString()
        status = parcel.readByte() != 0.toByte()
        id = parcel.readString()
        name = parcel.readString()
        description = parcel.readString()
        price = parcel.readString()
        discount_price = parcel.readString()
        percentage = parcel.readString()
        business_id = parcel.readString()
        user_id = parcel.readString()
        image = parcel.readString()
        tags = parcel.readString()
        gross_revenue = parcel.readString()
        campaign_count = parcel.readString()
        gross_sales = parcel.readString()
        created_by = parcel.readString()
        created_at = parcel.readString()
        updated_at = parcel.readString()
        tags_name = parcel.readString()
        subscriber = parcel.readString()
        status1 = parcel.readString()
    }


    fun subscription(): String {
        var subscriptionText  = ""
        subscriptionText = when (subscriber) {
            "0" -> {
                "0 subscription"
            }
            "1" -> {
                "1 subscription"
            }
            else -> {
                "$subscriber subscriptions"
            }
        }

        return subscriptionText
    }

    fun price(): String {
        return "$" + price.toDouble().roundToInt().toString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(eTDiscountPrice)
        parcel.writeString(product_id)
        parcel.writeString(productPrice)
        parcel.writeString(productDescription)
        parcel.writeString(productName)
        parcel.writeString(msg)
        parcel.writeByte(if (status) 1 else 0)
        parcel.writeString(id)
        parcel.writeString(name)
        parcel.writeString(description)
        parcel.writeString(price)
        parcel.writeString(discount_price)
        parcel.writeString(percentage)
        parcel.writeString(business_id)
        parcel.writeString(user_id)
        parcel.writeString(image)
        parcel.writeString(tags)
        parcel.writeString(gross_revenue)
        parcel.writeString(campaign_count)
        parcel.writeString(gross_sales)
        parcel.writeString(created_by)
        parcel.writeString(created_at)
        parcel.writeString(updated_at)
        parcel.writeString(tags_name)
        parcel.writeString(subscriber)
        parcel.writeString(status1)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<StoreModel> {
        override fun createFromParcel(parcel: Parcel): StoreModel {
            return StoreModel(parcel)
        }

        override fun newArray(size: Int): Array<StoreModel?> {
            return arrayOfNulls(size)
        }
    }


}