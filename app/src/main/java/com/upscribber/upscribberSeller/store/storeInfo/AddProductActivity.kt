package com.upscribber.upscribberSeller.store.storeInfo

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Outline
import android.graphics.Paint
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.*
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.commonClasses.RoundedBottomSheetDialogFragment
import com.upscribber.filters.CategoryModel
import com.upscribber.filters.ModelSubCategory
import com.upscribber.upscribberSeller.store.storeMain.StoreModel
import com.upscribber.upscribberSeller.store.storeMain.StoreViewModel
import com.upscribber.upscribberSeller.subsriptionSeller.subscriptionView.subsManage.SuccessfullyDeleteActivity
import kotlinx.android.synthetic.main.activity_add_product.*
import kotlinx.android.synthetic.main.activity_discover.drawerLayout1
import kotlinx.android.synthetic.main.custom_dialog.*
import java.math.BigDecimal
import java.math.RoundingMode

@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class AddProductActivity : AppCompatActivity(), DrawerLayout.DrawerListener {

    private var fragmentMain: CategoryProductsFragment = CategoryProductsFragment()
    lateinit var toolbar: Toolbar
    lateinit var rvTag: ChipGroup
    lateinit var mChip: Chip
    private lateinit var eTProductCategory: TextView
    private lateinit var eTDiscountPrice: TextView
    private lateinit var tvLastText: TextView
    private lateinit var tvOff: TextView
    private lateinit var eTProductDescription: EditText
    var arrayTags: ArrayList<ModelSubCategory> = ArrayList()
    lateinit var arrayCategoriesMain: ArrayList<CategoryModel>
    private lateinit var fragmentManager: FragmentManager
    private lateinit var fragmentTransaction: FragmentTransaction
    private lateinit var mfilters: TextView
    private lateinit var toolbarTitle: TextView
    private lateinit var textView43: TextView
    private lateinit var filterImgBack: ImageView
    private lateinit var imgName: ImageView
    private lateinit var imagePrice: ImageView
    private lateinit var imageTags: ImageView
    private lateinit var imageView4: ImageView
    private lateinit var imageDesc: ImageView
    var drawerOpen = 0
    private lateinit var filter_done: TextView
    private lateinit var create: TextView
    private lateinit var mViewModel: StoreViewModel
    var modelStore = ModelMainStoreInfo()
    val matchedIds = ArrayList<String>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_product)
        mViewModel = ViewModelProviders.of(this)[StoreViewModel::class.java]
        initz()
        setToolbar()
        clickks()
        apiImplimentation()
        observerInit()
        setData()
    }

    private fun setData() {
        if (intent.hasExtra("editProduct")) {
            create.text = "Save"
            try {
                val modelStore1 = intent.getParcelableExtra<StoreModel>("modelStore")
                eTProductName.setText(modelStore1.name)
                eTProductDescription.setText(modelStore1.description)
                modelStore = intent.getParcelableExtra("modelStoreMain")
                var nameCategory = ""
                for (i in 0 until modelStore.arrayParentTags.size) {
                    nameCategory = if (nameCategory.isEmpty()) {
                        modelStore.arrayParentTags[i].parentName
                    } else {
                        "$nameCategory, ${modelStore.arrayParentTags[i].parentName}"
                    }
                }
                eTProductCategory.text = nameCategory
                val price = modelStore1.price.toDouble().toInt().toString()
                eTProductPrice.setText(price)
                eTProductPrice.setText(Constant.getCalculatedPrice(modelStore1.discount_price,modelStore1.price))
                tvLastText.text = price
                val discountPrice = modelStore1.discount_price.toDouble().toInt().toString()
                eTDiscountPrice.text = "$discountPrice%"
                tvOff.text = "$discountPrice % off"
                create.setBackgroundResource(R.drawable.bg_seller_button_blue)
                create.isEnabled = true
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

    }


    private fun observerInit() {
        mViewModel.getProductCategoryAll().observe(this, Observer {
            progressBar29.visibility = View.GONE
            if (it.size > 0) {
                if (intent.hasExtra("modelStoreMain")) {
                    modelStore = intent.getParcelableExtra("modelStoreMain")
                    for (i in it) {
                        for (j in i.arrayList) {
                            for (k in modelStore.arrayParentTags) {
                                if (j.parentName == k.parentName) {
                                    j.check = true
                                    i.check = true
                                    i.checkoption = true
                                    matchedIds.add(j.id)
                                }
                            }
                        }
                    }
                }
                val bundle = Bundle()
                bundle.putParcelableArrayList("tags", it)
                fragmentMain = CategoryProductsFragment()
                inflateFragment(fragmentMain)
                fragmentMain.arguments = bundle
                arrayCategoriesMain = it
            }
        })

        mViewModel.getmDataAddProduct().observe(this, Observer {
            progressBar29.visibility = View.GONE
            if (it.status1 == "true") {
                if (intent.hasExtra("editProduct")) {
                    finish()
                }else if(intent.hasExtra("fromBasics")) {
                    val mDialogView = LayoutInflater.from(this).inflate(R.layout.custom_dialog, null)
                    val mBuilder = android.app.AlertDialog.Builder(this).setView(mDialogView)
                    val mAlertDialog = mBuilder.show()
                    mAlertDialog.setCancelable(false)
                    mAlertDialog.window.setBackgroundDrawableResource(R.drawable.bg_alert)
                    mAlertDialog.addressText.visibility = View.GONE
                    mAlertDialog.addresssText.text = "Product created successfully."
                    mAlertDialog.okBtnClick.setOnClickListener {
                        mAlertDialog.dismiss()
                        finish()
                    }
                }else {
                    startActivity(
                        Intent(
                            this,
                            StoreProductInfoActivity::class.java
                        ).putExtra("modelStore", it)
                    )
//                startActivity(
//                    Intent(this, ProductSuccessfullyActivity::class.java).putExtra("modelData", it)
//                        .putExtra("arrayTags", arrayTags)
//                )
//
                    finish()
                }

            }
        })


        mViewModel.getmDataProductDelete().observe(this, Observer {
            progressBar29.visibility = View.GONE
            if (it.statuss == "true") {
                startActivity(
                    Intent(this, SuccessfullyDeleteActivity::class.java)
                )
                finish()
            }
        })



        mViewModel.getmDataEditProduct().observe(this, Observer {
            progressBar29.visibility = View.GONE
            if (it.status1 == "true") {
                startActivity(
                    Intent(
                        this,
                        StoreProductInfoActivity::class.java
                    ).putExtra("modelStore", it)
                )

//                startActivity(
//                    Intent(this, ProductSuccessfullyActivity::class.java).putExtra("modelData", it)
//                        .putExtra("arrayTags", arrayTags)
//                )
                finish()
            } else {
                Toast.makeText(this, "false_error", Toast.LENGTH_SHORT).show()
            }
        })

        mViewModel.getStatus().observe(this, Observer {
            progressBar29.visibility = View.GONE
            if (it.msg.isNotEmpty()) {
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
            }
        })
    }


    private fun apiImplimentation() {
        mViewModel.productCategory()
    }

    override fun onResume() {
        super.onResume()
        if (intent.hasExtra("editProduct")) {
            val sharedPreferences = Constant.getPrefs(this)
            if (sharedPreferences.getString("product", "").isNotEmpty()) {
                finish()
            }

        }
    }

    @SuppressLint("ClickableViewAccessibility", "SetTextI18n")
    private fun clickks() {


        eTDiscountPrice.setOnClickListener {
            Constant.hideKeyboard(this, eTDiscountPrice)
            val choices = getList()
            val charSequenceItems = choices.toArray(arrayOfNulls<CharSequence>(choices.size))
            val mBuilder = AlertDialog.Builder(this)
            mBuilder.setSingleChoiceItems(charSequenceItems, -1) { dialogInterface, i ->
                eTDiscountPrice.text = choices[i]
                if (eTProductPrice.text.isNotEmpty()) {
                    var retailPrice = eTProductPrice.text.toString().replace("$", "").toDouble()
                    val check = choices[i].replace("%", "")
                    val discount = check.toInt()
                    val discountPrice = ((discount * retailPrice) / 100)
                    retailPrice -= discountPrice

                    val splitPos = retailPrice.toString().split(".")
                    if (splitPos[1] == "00" || splitPos[1] == "0") {
                        subscPrice.text = "$" + splitPos[0]
                    } else {
                        subscPrice.text = "$" + BigDecimal(retailPrice).setScale(
                            2,
                            RoundingMode.HALF_UP
                        ).toString()
                    }

                    tvLastText.paintFlags = tvLastText.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG

                    tvLastText.text =
                        "$" + eTProductPrice.text.toString().replace("$", "").toDouble().toInt()
                    tvOff.text = choices[i] + "off"

                }

                dialogInterface.dismiss()

            }
            val mDialog = mBuilder.create()
            mDialog.show()
        }


        eTProductPrice.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if (eTDiscountPrice.text.isNotEmpty()) {
                    try {
                        val price = p0.toString()
                        var retailPrice = price.toDouble()
                        val check = eTDiscountPrice.text.toString()
                        val removePercent = check.replace("%", "")
                        val discount = removePercent.toDouble()
                        val discountPrice = ((discount * retailPrice) / 100)
                        retailPrice -= discountPrice


                        val splitPos = retailPrice.toString().split(".")
                        if (splitPos[1] == "00" || splitPos[1] == "0") {
                            subscPrice.text = "$" + splitPos[0]
                        } else {
                            subscPrice.text = "$" + BigDecimal(retailPrice).setScale(
                                2,
                                RoundingMode.HALF_UP
                            ).toString()

                        }

                        tvLastText.paintFlags =
                            tvLastText.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
                        val merchantDiscount = ((20 * retailPrice) / 100)
                        val splitDiscount = merchantDiscount.toString().split(".")
                        /*if (splitDiscount[1] == "00" || splitDiscount[1] == "0") {
                            textView330.text = "$" + splitDiscount[0]
                        } else {
                            textView330.text = "$" + BigDecimal(merchantDiscount).setScale(
                                2,
                                RoundingMode.HALF_UP
                            ).toString()

                        }*/

                        val merchantRetail = retailPrice - merchantDiscount
                        val splitWhatYouGet = merchantRetail.toString().split(".")
                        /*if (splitWhatYouGet[1] == "00" || splitWhatYouGet[1] == "0") {
                            whatYouGet.text = "$" + splitWhatYouGet[0]
                        } else {
                            whatYouGet.text = "$" + BigDecimal(merchantRetail).setScale(
                                2,
                                RoundingMode.HALF_UP
                            ).toString()

                        }*/

                        tvLastText.text =
                            "$" + eTProductPrice.text.toString().toDouble().toInt()
                        tvOff.text = check

                    } catch (e: Exception) {
                        e.printStackTrace()

                    }
                }

                if (p0!!.isEmpty()) {
                    var check = eTDiscountPrice.text.toString()
                    if (check.isEmpty()) {
                        check = "0"
                    }
                    tvLastText.paintFlags =
                        tvLastText.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
                    tvLastText.text = "$0"
                    subscPrice.text = "$0"
                    tvOff.text = check
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (getTextFieldsData()) {
                    create.setBackgroundResource(R.drawable.bg_seller_button_blue)

                } else {
                    create.setBackgroundResource(R.drawable.blue_button_background_opacity)

                }
            }

        })


        eTProductCategory.setOnClickListener {
            Constant.hideKeyboard(this, eTProductCategory)
            drawerOpen = 1
            drawerLayout1.openDrawer(GravityCompat.END)
            inflateFragment(fragmentMain)
            mfilters.text = "Category"
        }


        imgName.setOnClickListener {
            Constant.CommonIAlert(
                this,
                "Product Name",
                "Enter brief overview about this product or service"
            )
        }

        imageDesc.setOnClickListener {
            Constant.CommonIAlert(
                this,
                "Describe this product/service",
                "Describe what this product or service includes and explain potential variations or restrictions. For example, you can say “Shampoo, cut and style. Excludes updo, bridal styles and brading."
            )
        }


        imagePrice.setOnClickListener {
            Constant.CommonIAlert(
                this,
                "Add Price",
                "Enter the retail price for this product or service."
            )
        }

        imageTags.setOnClickListener {
            Constant.CommonIAlert(
                this,
                "Add Tags",
                "Tags are keywords your customers may use to find a specific subscription in the marketplace. Customers search and filter by tags, so be sure to add all that apply."
            )
        }

        imageView4.setOnClickListener {
            Constant.CommonIAlert(
                this,
                "Discount Price",
                "Select Discount Percentage"
            )
        }

        filterImgBack.setOnClickListener {
            onBackPressed()
        }

        filter_done.setOnClickListener {
            onDonePressed()
        }



        create.setOnClickListener {
            if (intent.hasExtra("editProduct")) {
                val modelStore = intent.getParcelableExtra<StoreModel>("modelStore")
                val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
                if (eTProductName.text.toString().isEmpty()) {
                    Toast.makeText(this, "Please enter product name", Toast.LENGTH_LONG).show()
                }  else if (eTProductPrice.text.toString().isEmpty()) {
                    Toast.makeText(this, "Please enter product price", Toast.LENGTH_LONG).show()
                }else if (eTDiscountPrice.text.toString().isEmpty()) {
                    Toast.makeText(this, "Please enter product discount percentage", Toast.LENGTH_LONG).show()
                } else if (arrayTags.size < 1) {
                    if (eTProductCategory.text.toString().trim().isNotEmpty()) {
                        progressBar29.visibility = View.VISIBLE
                        mViewModel.getEditSaveProduct(
                            auth,
                            eTProductName.text.toString().trim(),
                            eTProductDescription.text.toString().trim(),
                            eTProductPrice.text.toString(),
                            getarrayIds(),
                            modelStore.id,eTDiscountPrice.text.toString().trim().replace("%", "")
                        )
                    } else {
                        Toast.makeText(this, "Please enter product tags.", Toast.LENGTH_LONG).show()
                    }

                } else {
                    progressBar29.visibility = View.VISIBLE
                    mViewModel.getEditSaveProduct(
                        auth,
                        eTProductName.text.toString().trim(),
                        eTProductDescription.text.toString().trim(),
                        eTProductPrice.text.toString(),
                        getarrayIds(),
                        modelStore.id,eTDiscountPrice.text.toString().trim().replace("%", "")
                    )
                }

            } else {
                val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
//                if (eTProductName.text.toString().isEmpty() && eTProductDescription.text.toString().isEmpty() && eTProductPrice.text.toString().isEmpty() &&
//                    arrayTags.size < 1
//                ) {
//                    Toast.makeText(this, "All fields are mandatory!", Toast.LENGTH_LONG).show()
//                } else
                if (eTProductName.text.toString().isEmpty()) {
                    Toast.makeText(this, "Please enter product name", Toast.LENGTH_LONG).show()
                } else if (eTProductDescription.text.toString().isEmpty()) {
                    Toast.makeText(this, "Please enter product description", Toast.LENGTH_LONG)
                        .show()
                } else if (eTProductPrice.text.toString().isEmpty()) {
                    Toast.makeText(this, "Please enter product price", Toast.LENGTH_LONG).show()
                } else if (eTDiscountPrice.text.toString().isEmpty()) {
                    Toast.makeText(this, "Please enter product discount percentage", Toast.LENGTH_LONG).show()
                } else if (arrayTags.size < 1) {
                    Toast.makeText(this, "Please enter product name", Toast.LENGTH_LONG).show()
                } else {
                    progressBar29.visibility = View.VISIBLE
                    mViewModel.getSaveProduct(
                        auth,
                        eTProductName.text.toString().trim(),
                        eTProductDescription.text.toString().trim(),
                        eTProductPrice.text.toString(),
                        getarrayIds(),eTDiscountPrice.text.toString().trim().replace("%", "")
                    )
                }

            }
        }

        eTProductDescription.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                val result = "${s!!.length}/120"
                textView43.text = result

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })

        eTProductDescription.setOnTouchListener { view, event ->
            view.parent.requestDisallowInterceptTouchEvent(true)
            if ((event.action and MotionEvent.ACTION_MASK) == MotionEvent.ACTION_UP) {
                view.parent.requestDisallowInterceptTouchEvent(false)
            }
            return@setOnTouchListener false
        }

        eTProductName.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(
                s: CharSequence,
                start: Int,
                count: Int,
                after: Int
            ) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (getTextFieldsData()) {
                    create.setBackgroundResource(R.drawable.bg_seller_button_blue)
                } else {
                    create.setBackgroundResource(R.drawable.blue_button_background_opacity)
                }
            }

            override fun afterTextChanged(s: Editable) {
            }
        })

        eTDiscountPrice.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(
                s: CharSequence,
                start: Int,
                count: Int,
                after: Int
            ) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (getTextFieldsData()) {
                    create.setBackgroundResource(R.drawable.bg_seller_button_blue)
                } else {
                    create.setBackgroundResource(R.drawable.blue_button_background_opacity)
                }
            }

            override fun afterTextChanged(s: Editable) {
            }
        })

    }

    private fun getTextFieldsData(): Boolean {
        val name = eTProductName.text.toString().trim().length
        val price = eTProductPrice.text.toString().trim().length
        val discount = eTDiscountPrice.text.toString().trim().length

        if (name == 0) {
            return false
        }
        if (price == 0) {
            return false
        }
        if (discount == 0) {
            return false
        }

        if(arrayTags.size < 1){
            return false
        }

        return true

    }

    private fun getList(): ArrayList<String> {
        val arrayList: ArrayList<String> = ArrayList()

        for (i in 1..80) {
            if (i % 5 == 0)
                arrayList.add((i).toString() + "%")
        }


        return arrayList

    }

    private fun getarrayIds(): String {
        var ids = ""
        ids = if (arrayTags.size > 0) {
            val listTags = ArrayList<String>()
            for (model in arrayTags) {
                listTags.add(model.id)
            }
            TextUtils.join(",", listTags)
        } else {
            TextUtils.join(",", matchedIds)
        }
        return ids
    }

    private fun onDonePressed() {
        arrayTags = ArrayList()
        rvTag.removeAllViews()
        chipData(arrayCategoriesMain)
        onBackPressed()
    }

    override fun onDrawerSlide(drawerView: View, slideOffset: Float) {

        var f = slideOffset
    }

    override fun onDrawerClosed(drawerView: View) {
        drawerOpen = 0
    }

    override fun onDrawerOpened(drawerView: View) {
        drawerOpen = 1
        drawerLayout1.openDrawer(GravityCompat.END)

        mfilters.text = "Tags"

    }

    override fun onDrawerStateChanged(newState: Int) {
        var hh = newState

    }

    private fun initz() {
        toolbar = findViewById(R.id.toolbarAddProducts)
        mChip = findViewById(R.id.chip)
        imgName = findViewById(R.id.imgName)
        imageTags = findViewById(R.id.imageTags)
        imagePrice = findViewById(R.id.imagePrice)
        imageDesc = findViewById(R.id.imageDesc)
        rvTag = findViewById(R.id.rvTags)
        eTProductCategory = findViewById(R.id.eTProductCategory)
        eTProductDescription = findViewById(R.id.eTProductDescription)
        textView43 = findViewById(R.id.textView43)
        imageView4 = findViewById(R.id.imageView4)
        mfilters = findViewById(R.id.filters)
        filterImgBack = findViewById(R.id.filterImgBack)
        filter_done = findViewById(R.id.filter_done)
        toolbarTitle = findViewById(R.id.title1)
        tvLastText = findViewById(R.id.tvLastText)
        tvOff = findViewById(R.id.tvOff)
        eTDiscountPrice = findViewById(R.id.eTDiscountPrice)
        create = findViewById(R.id.create)
        drawerLayout1.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, GravityCompat.END)

    }

    private fun inflateFragment(fragment: Fragment) {

        fragmentManager = supportFragmentManager
        this.fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.containerSide1, fragment)
        fragmentTransaction.commit()
    }

    override fun onBackPressed() {
        if (drawerOpen == 1) {
            drawerOpen = 0
            drawerLayout1.closeDrawer(GravityCompat.END)
        } else {
            super.onBackPressed()
        }
    }


    private fun setToolbar() {
        setSupportActionBar(toolbar)
        title = ""
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)
        if (intent.hasExtra("editProduct")) {
            toolbarTitle.text = "Edit to Store"
        } else {
            toolbarTitle.text = "Add to Store"
        }

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        if (intent.hasExtra("editProduct")) {
            menuInflater.inflate(R.menu.product_edit_delete, menu)
        }

        return super.onCreateOptionsMenu(menu)
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        if (item.itemId == R.id.paymentAdd) {
            val fragment = MenuFragmentDelete(mViewModel, progressBar29)
            fragment.show(supportFragmentManager, fragment.tag)

        }
        return super.onOptionsItemSelected(item)
    }

    class MenuFragmentDelete(
        var mViewModel: StoreViewModel,
        var progressBar29: View
    ) : RoundedBottomSheetDialogFragment() {

        lateinit var yes: TextView
        lateinit var no: TextView

        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val view = inflater.inflate(R.layout.delete_product_sheet, container, false)

            val modelStore = activity!!.intent.getParcelableExtra<StoreModel>("modelStore")
            val auth = Constant.getPrefs(activity!!).getString(Constant.auth_code, "")

            yes = view.findViewById(R.id.yes)
            no = view.findViewById(R.id.no)
            val imageViewTop = view.findViewById<ImageView>(R.id.imageViewTop)
            roundImageView(imageViewTop)
            no.setOnClickListener {
                dismiss()
            }

            yes.setOnClickListener {
                progressBar29.visibility = View.VISIBLE
                mViewModel.getDeleteProduct(auth, modelStore.id)
                dismiss()
            }

            return view
        }

        private fun roundImageView(imageViewTop: ImageView) {
            val curveRadius = 50F

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                imageViewTop.outlineProvider = object : ViewOutlineProvider() {

                    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                    override fun getOutline(view: View?, outline: Outline?) {
                        outline?.setRoundRect(
                            0,
                            0,
                            view!!.width,
                            view.height + curveRadius.toInt(),
                            curveRadius
                        )
                    }
                }
                imageViewTop.clipToOutline = true
            }
        }
    }

    @SuppressLint("SetTextI18n", "ResourceType")
    private fun chipData(it: ArrayList<CategoryModel>) {

        for (category in it) {
            var categoryName = ""
            for (subaCat in category.arrayList)
                if (subaCat.check) {
                    eTProductCategory.text = ""
                    categoryName = category.categoryName
                    arrayTags.add(subaCat)


                }
            if (categoryName != "") {
                val chip = Chip(rvTag.context)
                chip.text = categoryName
                chip.isClickable = true
                chip.isCheckable = false
                chip.setTextAppearanceResource(R.style.ChipTextStyle_Selected)
                chip.setChipBackgroundColorResource(R.color.home_free)
                chip.isCheckedIconEnabled = true
                chip.setOnCloseIconClickListener {
                    rvTag.removeView(chip)
                }
                chip.chipEndPadding = 20.0f
                chip.gravity = Gravity.CENTER
                rvTag.addView(chip)
            }

        }
        rvTag.isSingleSelection = true
        if (getTextFieldsData()) {
            create.setBackgroundResource(R.drawable.bg_seller_button_blue)
        } else {
            create.setBackgroundResource(R.drawable.blue_button_background_opacity)
        }
    }
}
