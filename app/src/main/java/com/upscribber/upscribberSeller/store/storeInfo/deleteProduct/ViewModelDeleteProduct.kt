package com.upscribber.upscribberSeller.store.storeInfo.deleteProduct

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData

class ViewModelDeleteProduct(application: Application) : AndroidViewModel(application) {

    var RepositoryProductDeleted : RepositoryProductDeleted = RepositoryProductDeleted(application)

    fun getList() : LiveData<List<ModelProductDeleted>> {
        return  RepositoryProductDeleted.getList()

    }

}
