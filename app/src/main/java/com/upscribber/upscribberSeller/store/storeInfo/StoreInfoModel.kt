package com.upscribber.upscribberSeller.store.storeInfo

import android.os.Parcel
import android.os.Parcelable
import com.upscribber.filters.ModelSubCategory
import java.util.ArrayList

class StoreInfoModel() : Parcelable{

    var arrayParentTags : ArrayList<ModelSubCategory> = ArrayList()
    var tags: String = ""
    var name: String = ""
    var price: String = ""
    var description: String = ""
    var discount_price: String = ""

    constructor(parcel: Parcel) : this() {
        arrayParentTags = parcel.createTypedArrayList(ModelSubCategory)
        tags = parcel.readString()
        name = parcel.readString()
        price = parcel.readString()
        description = parcel.readString()
        discount_price = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeTypedList(arrayParentTags)
        parcel.writeString(tags)
        parcel.writeString(name)
        parcel.writeString(price)
        parcel.writeString(description)
        parcel.writeString(discount_price)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<StoreInfoModel> {
        override fun createFromParcel(parcel: Parcel): StoreInfoModel {
            return StoreInfoModel(parcel)
        }

        override fun newArray(size: Int): Array<StoreInfoModel?> {
            return arrayOfNulls(size)
        }
    }
}
