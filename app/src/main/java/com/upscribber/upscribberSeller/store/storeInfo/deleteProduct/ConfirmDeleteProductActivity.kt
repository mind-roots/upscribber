package com.upscribber.upscribberSeller.store.storeInfo.deleteProduct

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.upscribber.R
import com.upscribber.databinding.ActivityConfirmDeleteProductBinding
import com.upscribber.upscribberSeller.subsriptionSeller.subscriptionView.subsManage.SuccessfullyDeleteActivity

class ConfirmDeleteProductActivity : AppCompatActivity() {

    private lateinit var mAdapter: AdapterDeleteProduct
    lateinit var mBinding : ActivityConfirmDeleteProductBinding
    lateinit var viewModel : ViewModelDeleteProduct

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding= DataBindingUtil.setContentView(this,R.layout.activity_confirm_delete_product)
        viewModel = ViewModelProviders.of(this)[ViewModelDeleteProduct::class.java]
        setToolbar()
        clickListeners()
        setAdapter()
    }

    private fun setToolbar() {
        setSupportActionBar(mBinding.toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        title = ""
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white)

    }

    private fun clickListeners() {
        mBinding.continuee.setOnClickListener {
            startActivity(
                Intent(this,
                    SuccessfullyDeleteActivity::class.java)
            )
        }


    }

    override fun onResume() {
        super.onResume()
        val sharedPreferences1 = getSharedPreferences("customers", Context.MODE_PRIVATE)
        if (!sharedPreferences1.getString("customer", "").isEmpty()) {
            finish()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home){
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setAdapter() {
        mBinding.recyclerSubscriptionList.layoutManager = LinearLayoutManager(this)
        mBinding.recyclerSubscriptionList.hasFixedSize()
        mBinding.recyclerSubscriptionList.isNestedScrollingEnabled = false
        mAdapter = AdapterDeleteProduct(this)
        mBinding.recyclerSubscriptionList.adapter = mAdapter

        viewModel.getList().observe(this, Observer {
            mAdapter.update(it)
        })


    }
}
