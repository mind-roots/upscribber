package com.upscribber.upscribberSeller.store.storeInfo

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Outline
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.*
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.widget.Toolbar
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.databinding.ActivityStoreProductInfoBinding
import com.upscribber.filters.ModelSubCategory
import com.upscribber.subsciptions.merchantStore.merchantDetails.ModeTags
import com.upscribber.upscribberSeller.store.storeMain.StoreModel
import com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.createSubs.CreateSubscriptionActivity
import kotlinx.android.synthetic.main.activity_store_product_info.*
import java.math.BigDecimal
import java.math.RoundingMode
import kotlin.math.roundToInt

@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class StoreProductInfoActivity : AppCompatActivity() {

    private lateinit var modelProducts: StoreModel
    lateinit var toolbar: Toolbar
    lateinit var toolbarTitle: TextView
    lateinit var rvTag: ChipGroup
    lateinit var mChip: Chip
    var array_name = ArrayList<ModelSubCategory>()
    private lateinit var mbinding: ActivityStoreProductInfoBinding
    private lateinit var mViewModel: StoreSubsInfoViewModel
    private lateinit var mAdapter: StoreSubsInfoAdapter
    var modelMainStoreInfo = ModelMainStoreInfo()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mbinding = DataBindingUtil.setContentView(this, R.layout.activity_store_product_info)
        mbinding.lifecycleOwner = this
        mViewModel = ViewModelProviders.of(this)[StoreSubsInfoViewModel::class.java]
        initz()
        setToolbar()
        roundImage()
        apiImplementation()
        observerInit()
        setAdapter()
        clickListeners()
    }

    private fun clickListeners() {
        mbinding.txtCreateSubscription.setOnClickListener {
            val mPrefs = application.getSharedPreferences("data", MODE_PRIVATE)
            val prefsEditor = mPrefs.edit()
            prefsEditor.putString("detailsArray", "")
            prefsEditor.putString("pricing", "")
            prefsEditor.putString("basicData", "")
            prefsEditor.putString("campaign_id", "")
            prefsEditor.putString("bought", "")
            prefsEditor.apply()
            startActivity(
                Intent(
                    this,
                    CreateSubscriptionActivity::class.java
                ).putExtra("productsData", modelProducts).putExtra("product", "product")
            )
        }

    }

    private fun observerInit() {
        mViewModel.getmDataStoreInfo().observe(this, Observer {
            if (it.status == "true") {
                modelMainStoreInfo = it
                mbinding.progressBar27.visibility = View.GONE
                mbinding.productName.text = modelMainStoreInfo.arrayMainStoreInfo[0].name
                mbinding.productPrice.text =
                    modelMainStoreInfo.arrayMainStoreInfo[0].price.toDouble().roundToInt()
                        .toString()
                mbinding.productDescription.text =
                    modelMainStoreInfo.arrayMainStoreInfo[0].description
                mbinding.textView12.text =
                    "Discount - " + modelMainStoreInfo.arrayMainStoreInfo[0].discount_price.toDouble()
                        .roundToInt().toString() + "%"
                var discountedPercentage = ""
                discountedPercentage =
                    ((modelMainStoreInfo.arrayMainStoreInfo[0].price.toDouble() * modelMainStoreInfo.arrayMainStoreInfo[0].discount_price.toDouble()) / 100).toString()
                if (discountedPercentage.contains(".")) {
                    val splitPos = discountedPercentage.split(".")
                    if (splitPos[1] == "00" || splitPos[1] == "0") {
                        mbinding.productDiscountPercentage.text = "-$" + splitPos[0]
                    } else {
                        mbinding.productDiscountPercentage.text = "-$" + BigDecimal(discountedPercentage).setScale(2, RoundingMode.HALF_UP)
                    }
                } else {
                    mbinding.productDiscountPercentage.text ="-$" + BigDecimal(discountedPercentage).setScale(2, RoundingMode.HALF_UP)
                }

                var discountedPrice = ""
                discountedPrice = Constant.getCalculatedPrice(
                    modelMainStoreInfo.arrayMainStoreInfo[0].discount_price,
                    modelMainStoreInfo.arrayMainStoreInfo[0].price
                )
                if (discountedPrice.contains(".")) {
                    val splitPos = discountedPrice.split(".")
                    if (splitPos[1] == "00" || splitPos[1] == "0") {
                        mbinding.productDiscountPrice.text = "$" + splitPos[0]
                    } else {
                        mbinding.productDiscountPrice.text = "$$discountedPrice"
                    }
                } else {
                    mbinding.productDiscountPrice.text = "$$discountedPrice"
                }

                if (modelMainStoreInfo.modelTags.size > 0) {
                    chipData(modelMainStoreInfo.arrayParentTags)
                }
                if (modelMainStoreInfo.arrayStoreSubscription.size > 0) {
                    mAdapter.update(modelMainStoreInfo.arrayStoreSubscription)
                    mbinding.noData.visibility = View.GONE
                    mbinding.textView227.visibility = View.VISIBLE
                    mbinding.noData1.visibility = View.GONE
                    mbinding.txtCreateSubscription.visibility = View.GONE
                } else {
                    mbinding.noData.visibility = View.GONE
                    mbinding.noData1.visibility = View.VISIBLE
                    mbinding.textView227.visibility = View.GONE
                    mbinding.txtCreateSubscription.visibility = View.VISIBLE
                }

            }
        })

        mViewModel.getmDataStatus().observe(this, Observer {
            mbinding.progressBar27.visibility = View.GONE
            if (it.msg == "Invalid auth code") {
                Constant.commonAlert(this)
            } else {
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
            }
        })

    }

    private fun apiImplementation() {
        val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
        if (intent.hasExtra("modelStore")) {
            modelProducts = intent.getParcelableExtra("modelStore")
        }

        mbinding.progressBar27.visibility = View.VISIBLE
        mViewModel.getProductInfo(auth, modelProducts.id)

    }


    private fun setAdapter() {
        mbinding.recyclerListSubs.layoutManager = LinearLayoutManager(this)
        mAdapter = StoreSubsInfoAdapter(this)
        mbinding.recyclerListSubs.adapter = mAdapter
    }

    private fun roundImage() {

        val curveRadius = 50F

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            mbinding.constraintLayout9.outlineProvider = object : ViewOutlineProvider() {

                @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                override fun getOutline(view: View?, outline: Outline?) {
                    outline?.setRoundRect(
                        0,
                        -100,
                        view!!.width,
                        view.height,
                        curveRadius
                    )
                }
            }
            mbinding.constraintLayout9.clipToOutline = true
        }
    }

    private fun initz() {
        toolbar = findViewById(R.id.toolbarProducts)
        toolbarTitle = findViewById(R.id.title)
        mChip = findViewById(R.id.chip)
        rvTag = findViewById(R.id.rvTags)
    }


    @SuppressLint("SetTextI18n", "ResourceType")
    private fun chipData(it: ArrayList<ModelSubCategory>) {
        array_name.clear()
        array_name = it

        for (index in array_name.indices) {
            val chip = Chip(rvTags.context)
            chip.text = "${array_name[index].parentName}"
            chip.isClickable = true
            chip.isCheckable = false
            chip.setTextAppearanceResource(R.style.ChipTextStyle_Selected)
            chip.setChipBackgroundColorResource(R.color.blue)
            chip.chipEndPadding = 20.0f
            chip.textSize = 12.0f
            chip.gravity = Gravity.CENTER
            rvTags.addView(chip)
        }

        rvTags.isSingleSelection = true

    }

    private fun setToolbar() {
        setSupportActionBar(toolbar)
        title = ""
        toolbarTitle.text = "Store"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }

        if (item.itemId == R.id.storeProduct) {
            val editor = Constant.getPrefs(this).edit()
            editor.putString("editData", "back")
            editor.apply()
            finish()

            startActivity(
                Intent(this, AddProductActivity::class.java).putExtra("editProduct", "edit")
                    .putExtra("modelStore", intent.getParcelableExtra<StoreModel>("modelStore"))
                    .putExtra("modelStoreMain", modelMainStoreInfo)
            )
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onResume() {
        super.onResume()
//        array_name.clear()
//        apiImplementation()
        val sharedPreferences = Constant.getPrefs(this)
        if (sharedPreferences.getString("product", "").isNotEmpty()) {
            val editor = sharedPreferences.edit()
            editor.remove("product")
            editor.apply()
            finish()
        }

        val sharedPreferences1 = Constant.getPrefs(this)
        if (sharedPreferences1.getString("editData", "").isNotEmpty()) {
            val editor1 = Constant.getPrefs(this).edit()
            editor1.remove("editData")
            editor1.apply()
            finish()
        }


    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.store_product, menu)
        return super.onCreateOptionsMenu(menu)
    }

}
