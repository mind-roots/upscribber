package com.upscribber.upscribberSeller.store.storeMain

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.daimajia.swipe.SwipeLayout
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter
import com.upscribber.R
import com.upscribber.databinding.ExtraItemsBinding
import com.upscribber.upscribberSeller.subsriptionSeller.extras.ExtraInfoActivity
import com.upscribber.upscribberSeller.subsriptionSeller.extras.ModelExtra
import kotlinx.android.synthetic.main.extra_items.view.*

class StoreFragmentAdapter (var context: Context) : RecyclerSwipeAdapter<StoreFragmentAdapter.ViewHolder>() {

    override fun getSwipeLayoutResourceId(position: Int): Int {
        return R.id.swipe1
    }

    private var itemss: List<ModelExtra> = emptyList()
    lateinit var mBinding: ExtraItemsBinding

//    var edit = context as EditItems

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.extra_items, parent, false)
        return ViewHolder(mBinding.root)

    }

    override fun getItemCount(): Int {
        return itemss.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = itemss[position]
        mBinding.model = model



        if (model.isSelected) {
            holder.itemView.setBackgroundResource(R.drawable.bg_seller_blue_outline)
        } else {
            holder.itemView.setBackgroundResource(0)
        }

            holder.itemView.itemClick.setOnClickListener {
                context.startActivity(Intent(context, ExtraInfoActivity::class.java))


//
////           edit.editItems(position,model)
//        }
//            holder.itemView.swipe1.showMode = SwipeLayout.ShowMode.PullOut
//            holder.itemView.swipe1.toggle(true)
//            holder.itemView.swipe1.addDrag(
//                SwipeLayout.DragEdge.Right,
//                holder.itemView.swipe1
//                    .findViewById(R.id.bottom_wrapper)
//            )

        }

        holder.itemView.swipe1.addSwipeListener(object : SwipeLayout.SwipeListener {
            override fun onOpen(layout: SwipeLayout?) {
                mItemManger.closeAllExcept(layout)

            }

            override fun onUpdate(layout: SwipeLayout?, leftOffset: Int, topOffset: Int) {
            }

            override fun onStartOpen(layout: SwipeLayout?) {

            }

            override fun onStartClose(layout: SwipeLayout?) {

            }

            override fun onHandRelease(layout: SwipeLayout?, xvel: Float, yvel: Float) {
                if (layout != null) {
                    layout.close()
                }

            }

            override fun onClose(layout: SwipeLayout?) {

            }
        })

        mItemManger.bindView(holder.itemView, position)

    }


    fun update(items: List<ModelExtra>) {
        this.itemss = items
        notifyDataSetChanged()
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)

    interface EditItems {
        fun editItems(
            position: Int,
            model: ModelExtra
        )
    }
}
