package com.upscribber.upscribberSeller.store.storeInfo.deleteProduct

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

class RepositoryProductDeleted(application: Application) {
    fun getList(): LiveData<List<ModelProductDeleted>> {

        val mData = MutableLiveData<List<ModelProductDeleted>>()
        val arrayList = ArrayList<ModelProductDeleted>()

        var subscriptionListModel = ModelProductDeleted()
        subscriptionListModel.subscriptionName = "Manicure"
        subscriptionListModel.subscriptionEnjoyText = "Enjoy 30 mins Manicure"
        subscriptionListModel.subscriptionDiscount = "41% OFF"
        subscriptionListModel.subscriptionPrice = "$39.99"
        subscriptionListModel.subscriptionPeriod = "Monthly"
        subscriptionListModel.status = false
        arrayList.add(subscriptionListModel)

        subscriptionListModel = ModelProductDeleted()
        subscriptionListModel.subscriptionName = "Manicure"
        subscriptionListModel.subscriptionEnjoyText = "Enjoy 30 mins Padicure"
        subscriptionListModel.subscriptionDiscount = "41% OFF"
        subscriptionListModel.subscriptionPrice = "$56.99"
        subscriptionListModel.subscriptionPeriod = "Bi-Monthly"
        subscriptionListModel.status = false
        arrayList.add(subscriptionListModel)

        subscriptionListModel = ModelProductDeleted()
        subscriptionListModel.subscriptionName = "Manicure"
        subscriptionListModel.subscriptionEnjoyText = "Enjoy 30 mins Waxing"
        subscriptionListModel.subscriptionDiscount = "41% OFF"
        subscriptionListModel.subscriptionPrice = "$50.0"
        subscriptionListModel.subscriptionPeriod = "Monthly"
        subscriptionListModel.status = false
        arrayList.add(subscriptionListModel)

        subscriptionListModel = ModelProductDeleted()
        subscriptionListModel.subscriptionName = "Manicure"
        subscriptionListModel.subscriptionEnjoyText = "Enjoy 30 mins Manicure"
        subscriptionListModel.subscriptionDiscount = "41% OFF"
        subscriptionListModel.subscriptionPrice = "$10.99"
        subscriptionListModel.subscriptionPeriod = "Bi-Monthly"
        subscriptionListModel.status = false
        arrayList.add(subscriptionListModel)

        mData.value = arrayList
        return mData
    }

}
