package com.upscribber.upscribberSeller.store.storeMain

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.upscribber.commonClasses.Constant
import com.upscribber.commonClasses.ModelStatusMsg
import com.upscribber.commonClasses.WebServicesCustomers
import com.upscribber.filters.CategoryModel
import com.upscribber.filters.ModelSubCategory
import com.upscribber.upscribberSeller.store.storeInfo.deleteProduct.ModelProductDeleted
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import java.util.ArrayList
import org.json.JSONException


class StoreRepository(var application: Application) {

    var mData = MutableLiveData<ArrayList<StoreModel>>()
    var mDataCategories = MutableLiveData<ArrayList<CategoryModel>>()
    val mDataFailure = MutableLiveData<ModelStatusMsg>()
    val mDataAdd = MutableLiveData<StoreModel>()
    val mDataEdit = MutableLiveData<StoreModel>()
    val mDataProductDelete = MutableLiveData<ModelProductDeleted>()


    fun productCategories() {
        val auth = Constant.getPrefs(application).getString(Constant.auth_code, "")
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.productCategory(auth)
        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {

                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val arrayCategories = ArrayList<CategoryModel>()
                        if (status == "true") {
                            val data = json.optJSONObject("data")
                            val iter = data.keys()
                            while (iter.hasNext()) {
                                val key = iter.next()
                                try {
                                    val categoryModel = CategoryModel()
                                    categoryModel.categoryName = key
                                    val catName = data.optJSONObject(key)
                                    categoryModel.image = catName.optString("image")
                                    categoryModel.categoryId = catName.optString("parent_id")
                                    val value = catName.optJSONArray("sub_category")
                                    for (i in 0 until value.length()) {
                                        val objMainCategory = value.getJSONObject(i)
                                        val model = ModelSubCategory()
                                        model.name = objMainCategory.optString("name")
                                        model.id = objMainCategory.optString("id")
                                        model.parentName = key
                                        categoryModel.arrayList.add(model)
                                    }

                                    arrayCategories.add(categoryModel)
                                } catch (e: JSONException) {
                                    e.printStackTrace()
                                }

                            }
                            mDataCategories.value = arrayCategories
                        } else {
                            val modelStatus = ModelStatusMsg()
                            modelStatus.msg = msg
                        }

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus
            }

        })

    }

    fun getProductss(search: String) {
        val auth = Constant.getPrefs(application).getString(Constant.auth_code, "")

        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.getProducts(auth,search)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        val list: ArrayList<StoreModel> = ArrayList()

                        if (status == "true") {
                            val data = json.optJSONObject("data")
                            val products = data.optJSONArray("products")
                            for (i in 0 until products.length()) {
                                val obj = products.getJSONObject(i)
                                val storeModel = StoreModel()
                                storeModel.id = obj.optString("id")
                                storeModel.name = obj.optString("name")
                                storeModel.description = obj.optString("description")
                                storeModel.price = obj.optString("price")
                                storeModel.discount_price = obj.optString("discount_price")
                                storeModel.percentage = obj.optString("percentage")
                                storeModel.business_id = obj.optString("business_id")
                                storeModel.user_id = obj.optString("user_id")
                                storeModel.image = obj.optString("image")
                                storeModel.tags = obj.optString("tags")
                                storeModel.gross_revenue = obj.optString("gross_revenue")
                                storeModel.campaign_count = obj.optString("campaign_count")
                                storeModel.gross_sales = obj.optString("gross_sales")
                                storeModel.created_by = obj.optString("created_by")
                                storeModel.created_at = obj.optString("created_at")
                                storeModel.updated_at = obj.optString("updated_at")
                                storeModel.tags_name = obj.optString("tags_name")
                                storeModel.subscriber = obj.optString("subscriber")
                                storeModel.status1 = status
                                list.add(storeModel)
                            }
                            mData.value = list
                        } else {
                            modelStatus.status = status
                            modelStatus.msg = msg
                            mDataFailure.value = modelStatus
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus
            }

        })

    }


    fun getProductData(): MutableLiveData<ArrayList<StoreModel>> {
        return mData
    }

    fun getmDataFailure(): LiveData<ModelStatusMsg> {
        return mDataFailure
    }

    fun ProductCaregory(): MutableLiveData<ArrayList<CategoryModel>> {
        return mDataCategories
    }

    fun getAddProductData(
        auth: String,
        productName: String,
        productDescription: String,
        productPrice: String,
        getarrayIds: String,
        eTDiscountPrice: String
    ) {

        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> =
            service.saveProductMerchant(productName, auth, productPrice, productDescription, getarrayIds,eTDiscountPrice)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        val modelAdd = StoreModel()

                        if (status == "true") {
                            val data = json.optJSONObject("data")
                            modelAdd.status1 = status
                            modelAdd.msg = msg
                            modelAdd.productName = productName
                            modelAdd.id = data.optString("product_id")
                            modelAdd.productDescription = productDescription
                            modelAdd.productPrice = productPrice
                            modelAdd.eTDiscountPrice = eTDiscountPrice
                            mDataAdd.value = modelAdd
                        } else {
                            modelStatus.status = "false"
                            modelStatus.msg = msg
                            mDataFailure.value = modelStatus
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus

            }
        })
    }

    fun getmDataAddProduct(): LiveData<StoreModel> {
        return mDataAdd
    }

    fun getEditProductData(
        auth: String,
        productName: String,
        productDescription: String,
        productPrice: String,
        getarrayIds: String,
        id: String,
        discount: String
    ) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> =
            service.saveProductEditMerchant(productName, auth, productPrice, productDescription, getarrayIds, id,discount)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        val model = StoreModel()

                        if (status == "true") {
                            val data = json.optJSONObject("data")
                            model.status1 = status
                            model.msg = msg
                            model.productName = productName
                            model.productDescription = productDescription
                            model.eTDiscountPrice = discount
                            model.id = data.optString("product_id")
                            model.productPrice = productPrice
                            mDataEdit.value = model
                        } else {
                            modelStatus.status = status
                            modelStatus.msg = msg
                            mDataFailure.value = modelStatus
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus

            }
        })

    }

    fun getmDataEditProduct(): LiveData<StoreModel> {
        return mDataEdit
    }

    fun getDeleteProduct(auth: String, productId: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.deleteProduct(auth, productId)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        val model = ModelProductDeleted()

                        if (status == "true") {
                            model.statuss = status
                            model.msg = msg
//                            model.productName = productName
//                            model.productDescription = productDescription
//                            model.productPrice = productPrice
                            mDataProductDelete.value = model
                        } else {
                            modelStatus.status = status
                            modelStatus.msg = msg
                            mDataFailure.value = modelStatus
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg ="Network Error!"
                mDataFailure.value = modelStatus

            }
        })


    }

    fun getmDataProductDelete(): LiveData<ModelProductDeleted> {
        return mDataProductDelete
    }



}
