package com.upscribber.upscribberSeller.store.storeMain

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.upscribber.commonClasses.ModelStatusMsg
import com.upscribber.filters.CategoryModel
import com.upscribber.upscribberSeller.store.storeInfo.deleteProduct.ModelProductDeleted
import java.util.ArrayList

class StoreViewModel(application: Application) : AndroidViewModel(application) {

    var storeRepository: StoreRepository =
        StoreRepository(application)

    fun getAllProducts(): MutableLiveData<ArrayList<StoreModel>> {
        return storeRepository.getProductData()
    }

    fun getProductCategoryAll(): MutableLiveData<ArrayList<CategoryModel>> {
        return storeRepository.ProductCaregory()
    }

    fun getProducts(search: String) {
        return storeRepository.getProductss(search)
    }

    fun getStatus(): LiveData<ModelStatusMsg> {
        return storeRepository.getmDataFailure()
    }

    fun getmDataAddProduct(): LiveData<StoreModel> {
        return storeRepository.getmDataAddProduct()
    }

    fun getmDataEditProduct(): LiveData<StoreModel> {
        return storeRepository.getmDataEditProduct()
    }

    fun getmDataProductDelete(): LiveData<ModelProductDeleted> {
        return storeRepository.getmDataProductDelete()
    }


    fun productCategory() {
        storeRepository.productCategories()
    }

    fun getSaveProduct(
        auth: String,
        productName: String,
        productDescription: String,
        productPrice: String,
        getarrayIds: String,
        eTDiscountPrice: String
    ) {
        storeRepository.getAddProductData(auth, productName, productDescription, productPrice, getarrayIds,eTDiscountPrice)

    }

    fun getEditSaveProduct(
        auth: String,
        productName: String,
        productDescription: String,
        productPrice: String,
        getarrayIds: String,
        id: String,
        discount : String
    ) {
        storeRepository.getEditProductData(auth, productName, productDescription, productPrice, getarrayIds, id,discount)
    }

    fun getDeleteProduct(auth: String, productId: String) {
        storeRepository.getDeleteProduct(auth, productId)
    }

}