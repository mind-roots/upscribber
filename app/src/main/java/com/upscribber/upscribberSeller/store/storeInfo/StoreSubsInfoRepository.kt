package com.upscribber.upscribberSeller.store.storeInfo

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.upscribber.commonClasses.Constant
import com.upscribber.commonClasses.ModelStatusMsg
import com.upscribber.commonClasses.WebServicesMerchants
import com.upscribber.filters.ModelSubCategory
import com.upscribber.subsciptions.merchantStore.merchantDetails.ModeTags
import com.upscribber.upscribberSeller.store.storeMain.StoreModel
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class StoreSubsInfoRepository(var application: Application) {

    val mDataStoreInfo = MutableLiveData<ModelMainStoreInfo>()
    val mDataFailure = MutableLiveData<ModelStatusMsg>()

    fun getProductInfo(auth: String, id: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesMerchants::class.java)
        val call: Call<ResponseBody> = service.productDetails(auth, id)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        val arrayTags = ArrayList<ModeTags>()
                        val arrayParentTags = ArrayList<ModelSubCategory>()
                        val arrayMainStoreInfo = ArrayList<StoreInfoModel>()
                        val arrayStoreSubscription = ArrayList<StoreSubsInfoModel>()
                        val modelStore = ModelMainStoreInfo()

                        if (status == "true") {
                            val data = json.optJSONObject("data")
                            val product_details = data.optJSONArray("product_details")
                            val subscriptions = data.optJSONArray("subscriptions")


                            for (i in 0 until product_details.length()) {
                                val productTopInfo = product_details.optJSONObject(i)
                                val modelStore1 = StoreInfoModel()
                                modelStore1.name = productTopInfo.optString("name")
                                modelStore1.price = productTopInfo.optString("price")
                                modelStore1.description = productTopInfo.optString("description")
                                modelStore1.discount_price = productTopInfo.optString("discount_price")
                                val tagsArray = productTopInfo.optJSONArray("tags")
                                for (tags in 0 until tagsArray.length()) {
                                    val modelTags = ModeTags()
                                    modelTags.name = tagsArray[tags].toString()
                                    arrayTags.add(modelTags)
                                }

                                val parent_tags = productTopInfo.getJSONArray("parent_tags")
                                for (parent in 0 until parent_tags.length()) {
                                    val parentTagsInfo = parent_tags.optJSONObject(parent)
                                    val modelTags = ModelSubCategory()
                                    modelTags.parent_id = parentTagsInfo.optString("parent_id")
                                    modelTags.parentName = parentTagsInfo.optString("parent_name")
                                    arrayParentTags.add(modelTags)
                                }

                                modelStore1.arrayParentTags = arrayParentTags
                                arrayMainStoreInfo.add(modelStore1)
                            }



                            for (i in 0 until subscriptions.length()) {
                                val subscriptionsInfo = subscriptions.optJSONObject(i)
                                val modelSubscriptions = StoreSubsInfoModel()
                                modelSubscriptions.id = subscriptionsInfo.optString("id")
                                modelSubscriptions.campaign_id =
                                    subscriptionsInfo.optString("campaign_id")
                                modelSubscriptions.name = subscriptionsInfo.optString("name")
                                modelSubscriptions.price = subscriptionsInfo.optString("price")
                                modelSubscriptions.discount_price =
                                    subscriptionsInfo.optString("discount_price")
                                modelSubscriptions.frequency_type =
                                    subscriptionsInfo.optString("frequency_type")
                                modelSubscriptions.description =
                                    subscriptionsInfo.optString("description")
                                modelSubscriptions.unit = subscriptionsInfo.optString("unit")
                                arrayStoreSubscription.add(modelSubscriptions)

                            }
                            modelStore.arrayStoreSubscription = arrayStoreSubscription
                            modelStore.arrayMainStoreInfo = arrayMainStoreInfo
                            modelStore.modelTags = arrayTags
                            modelStore.arrayParentTags = arrayParentTags
                            modelStore.status = status
                            modelStore.msg = msg
                            mDataStoreInfo.value = modelStore

                        } else {
                            modelStatus.status = "false"
                            modelStatus.msg = msg
                            mDataFailure.value = modelStatus
                        }
                    }catch (e: Exception){
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus
            }
        })
    }

    fun getmDataStoreInfo() : LiveData<ModelMainStoreInfo>{
        return mDataStoreInfo
    }

    fun getmDataStatus() : LiveData<ModelStatusMsg>{
        return mDataFailure
    }

}