package com.upscribber.upscribberSeller.store.storeInfo

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.R
import com.upscribber.filters.CategoryModel
import com.upscribber.filters.ExpandListener


class CategoryProductsFragment : Fragment(), ExpandListener {

    private lateinit var mCategoryRecycler: RecyclerView
    private lateinit var categoryAdapter: CategoryAdapterProducts
    private lateinit var categoriesList: ArrayList<CategoryModel>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_category_filter, container, false)
        val tags = arguments?.getParcelableArrayList<CategoryModel>("tags")
        mCategoryRecycler = view!!.findViewById(R.id.category_recycler)
        setAdapter(tags)
        return view
    }


    private fun setAdapter(tags: ArrayList<CategoryModel>?) {
        mCategoryRecycler.layoutManager = LinearLayoutManager(activity)
        categoryAdapter = CategoryAdapterProducts(this.activity!!, this)
        mCategoryRecycler.adapter = categoryAdapter
        tags?.let { it ->
            it.sortBy { it.categoryName }
            categoriesList = it
            categoryAdapter.update(categoriesList)

        }
    }

    override fun onExpand(position: Int) {
        val model = categoriesList[position]
        if (model.checkoption) {
            model.checkoption = false
        } else {
            for (child in categoriesList) {
                child.checkoption = false
            }
            model.checkoption = true
        }
        categoriesList[position] = model
        categoryAdapter.notifyDataSetChanged()
    }

    override fun onSelectAll(position: Int, check: Boolean) {
        for (child in categoriesList[position].arrayList) {
            child.check = check
        }
    }

    override fun getSelection(position: Int, check: Boolean) {

    }
}