package com.upscribber.upscribberSeller.store.storeInfo

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.upscribber.R
import com.upscribber.upscribberSeller.store.storeMain.StoreAdapter
import com.upscribber.upscribberSeller.store.storeMain.StoreModel
import kotlinx.android.synthetic.main.activity_store_search_results.*

class StoreSearchResultsActivity : AppCompatActivity() {

    private lateinit var mAdapter: StoreAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_store_search_results)
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(this, R.color.home_free)
        setToolbar()
        clickListener()
        setAdapter()
    }

    private fun clickListener() {
        getStartedBtn.setOnClickListener {
            startActivity(Intent(this, AddProductActivity::class.java))
        }
    }

    private fun setAdapter() {
        val arrayList = intent.getParcelableArrayListExtra<StoreModel>("arrayStoreResults")
        if (arrayList.size > 0){
            imageView84.visibility = View.GONE
            tvNoData.visibility = View.GONE
            tvNoDataDescription.visibility = View.GONE
            getStartedBtn.visibility = View.GONE
        }else {
            imageView84.visibility = View.VISIBLE
            tvNoData.visibility = View.VISIBLE
            tvNoDataDescription.visibility = View.VISIBLE
            getStartedBtn.visibility = View.VISIBLE
        }
        recyclerViewProducts.layoutManager = LinearLayoutManager(this)
        mAdapter = StoreAdapter(this)
        recyclerViewProducts.adapter = mAdapter
        mAdapter.update(arrayList)

    }

    private fun setToolbar() {
        if (intent.hasExtra("productsSearchQuery")){
            val query = intent.getStringExtra("productsSearchQuery")
            setSupportActionBar(toolbar)
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white)
            title = ""
            toolbarTitle.text = query
        }
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}
