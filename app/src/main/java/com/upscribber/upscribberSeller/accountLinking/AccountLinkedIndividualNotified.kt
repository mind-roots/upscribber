package com.upscribber.upscribberSeller.accountLinking

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.upscribber.R
import kotlinx.android.synthetic.main.activity_account_linked_individual_notified.*

class AccountLinkedIndividualNotified : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_account_linked_individual_notified)

        startSeling.setOnClickListener {
            finish()

        }
    }
}
