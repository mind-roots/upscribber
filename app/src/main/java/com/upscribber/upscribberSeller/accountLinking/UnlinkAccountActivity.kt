package com.upscribber.upscribberSeller.accountLinking

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.upscribber.R
import com.upscribber.commonClasses.ModelStatusMsg
import kotlinx.android.synthetic.main.activity_unlink_account.*

class UnlinkAccountActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_unlink_account)


        if (intent.hasExtra("fromBottomSheet")){
            unlink1.visibility = View.GONE
            unlink2.visibility = View.VISIBLE
            deniedLinkDone.text = "Link"
        }else if (intent.hasExtra("profile")){
            var model = intent.getParcelableExtra<ModelStatusMsg>("profile")
            unlink1.text = "Your account has been unlinked from " + model.businessName + "."
            unlink1.visibility = View.VISIBLE
            unlink2.visibility = View.GONE
            deniedLinkDone.text = "Done"
        }else{
            unlink1.visibility = View.VISIBLE
            unlink2.visibility = View.GONE
            deniedLinkDone.text = "Done"
        }

        deniedLinkDone.setOnClickListener {
            val intent = Intent()
            setResult(1, intent)
            finish()
        }
    }

}
