package com.upscribber.upscribberSeller.accountLinking

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.commonClasses.ModelStatusMsg
import com.upscribber.notification.ModelNotificationCustomer
import kotlinx.android.synthetic.main.activity_account_linked_notified.*

class AccountLinkedMerchantNotifiedActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_account_linked_notified)
        val isSeller = Constant.getSharedPrefs(this).getBoolean(Constant.IS_SELLER, false)
        if (isSeller) {
            main.setBackgroundResource(R.drawable.bg_almost_there)
        }else{
            main.setBackgroundResource(R.drawable.bg_almost_there)
        }

        if (intent.hasExtra("profile")){
//            var model = intent.getParcelableExtra<ModelStatusMsg>("profile")
//            textDescription.text = "Congratulations! Your account has been relinked with " + model.businessName + "."
            textDescription.text = "Your relink request has been sent."
            textView205.text =  "Relink Request!"
        }else if (intent.hasExtra("linked")){
            textDescription.text = "Your relink request has been sent."
            textView205.text =  "Relink Request!"
        }else if (intent.hasExtra("model")){
            val model = intent.getParcelableExtra<ModelNotificationCustomer>("model")
            textDescription.text = "Congratulations! Your account has been relinked with " + model.business_name + "."
            textView205.text = "Relinked!"
        }

        linkingDone.setOnClickListener {
            val intent = Intent()
            setResult(1, intent)
            finish()
        }
    }
}
