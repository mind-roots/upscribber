package com.upscribber.upscribberSeller.accountLinking

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.upscribber.R

class AccountLinkDeniedMerchantNotified : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_account_link_denied_merchant_notified)
    }
}
