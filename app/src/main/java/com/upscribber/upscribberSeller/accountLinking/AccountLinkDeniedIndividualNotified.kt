package com.upscribber.upscribberSeller.accountLinking

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.upscribber.R
import com.upscribber.notification.ModelNotificationCustomer
import kotlinx.android.synthetic.main.activity_account_link_denied_individual_notified.*

class AccountLinkDeniedIndividualNotified : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_account_link_denied_individual_notified)
        setData()
        clickListeners()
    }

    private fun setData() {
        val model = intent.getParcelableExtra<ModelNotificationCustomer>("model")
        businessName.text = "You have declined the request to associate with "  + model.business_name
    }

    private fun clickListeners() {
        deniedLinkDone.setOnClickListener {
            finish()
        }

    }
}
