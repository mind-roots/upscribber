package com.upscribber.upscribberSeller.accountLinking

import android.content.Context
import android.content.Intent
import android.graphics.Outline
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewOutlineProvider
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.upscribber.commonClasses.RoundedBottomSheetDialogFragment
import com.upscribber.R
import com.upscribber.becomeseller.individual.IndividualViewModel
import com.upscribber.becomeseller.sellerWelcomeScreens.sellerWelcomActivity
import com.upscribber.commonClasses.Constant
import com.upscribber.notification.ModelNotificationCustomer
import kotlinx.android.synthetic.main.activity_linking_request.*

class LinkingRequestActivity : AppCompatActivity() {

    lateinit var linkRequest: TextView
    lateinit var mViewModel: IndividualViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_linking_request)
        initz()
        when {
            intent.hasExtra("individualList") -> {
                val model = intent.getParcelableExtra<ModelNotificationCustomer>("model")
                Glide.with(this).load(model.logo).placeholder(R.mipmap.circular_placeholder)
                    .into(imageView)
                tvName.text = model.business_name
                textView202.text = model.street
                textLink.text = model.business_name + " wants you to link to their business"
                linkRequest.text = "Link Account"
                goBackText.visibility = View.VISIBLE
            }
            intent.hasExtra("relink") -> {
                val model = intent.getParcelableExtra<ModelNotificationCustomer>("model")
                Glide.with(this).load(model.logo).placeholder(R.mipmap.circular_placeholder)
                    .into(imageView)
                tvName.text = model.business_name
                textView202.text = model.street
                textLink.text = model.business_name + " wants you to Relink to their business"
                linkRequest.text = "Relink Account"
                goBackText.visibility = View.VISIBLE
            }
            else -> {
                val model = intent.getParcelableExtra<ModelNotificationCustomer>("model")
                Glide.with(this).load(model.logo).placeholder(R.mipmap.circular_placeholder)
                    .into(imageView)
                tvName.text = model.business_name
                textView202.text = model.street
                textLink.text = model.business_name + " wants you to Relink to their business"
                linkRequest.text = "Relink Account"
                goBackText.visibility = View.VISIBLE
            }
        }

        click()
    }

    override fun onResume() {
        super.onResume()
        val sharedPreferences1 = getSharedPreferences("linkingNotification", Context.MODE_PRIVATE)
        if (!sharedPreferences1.getString("linkingNotification", "").isEmpty()) {
            finish()
        }
        val sharedPreferences2 = getSharedPreferences("individual", Context.MODE_PRIVATE)
        if (!sharedPreferences2.getString("individual", "").isEmpty()) {
            finish()
        }


    }

    private fun initz() {
        linkRequest = findViewById(R.id.linkRequest)
        mViewModel = ViewModelProviders.of(this)[IndividualViewModel::class.java]
    }


    private fun click() {
        linkRequest.setOnClickListener {
            if (intent.hasExtra("individualList")) {
                val fragment = MenuFragment(1, mViewModel)
                fragment.show(supportFragmentManager, fragment.tag)
            } else {
                val fragment = MenuFragment(3, mViewModel)
                fragment.show(supportFragmentManager, fragment.tag)
            }
        }

        goBackText.setOnClickListener {
                val fragment = MenuFragment(2, mViewModel)
                fragment.show(supportFragmentManager, fragment.tag)

        }

    }


    class MenuFragment(
        var i: Int,
        var mViewModel: IndividualViewModel
    ) : RoundedBottomSheetDialogFragment() {

        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val view = inflater.inflate(R.layout.request_linking_sheet, container, false)

            val yes = view.findViewById<TextView>(R.id.yes)
            val imageView = view.findViewById<ImageView>(R.id.imageView)
            val no = view.findViewById<TextView>(R.id.no)
            val tvTextTop = view.findViewById<TextView>(R.id.tvTextTop)
            val tvText = view.findViewById<TextView>(R.id.tvText)
            roundCorners(imageView)
            when (i) {
                1 -> {
                    tvText.text = "Are you sure you want to Link account?"
                    tvTextTop.text = "Link account"
                }
                2 -> {
                    tvText.text = "Are you sure you want to Decline account?"
                    tvTextTop.text = "Declined account"
                }
                else -> {
                    tvText.text = "Are you sure you want to Relink account?"
                    tvTextTop.text = "Relink account"
                }
            }
            yes.setOnClickListener {
                if (activity!!.intent.hasExtra("model")) {
                    val model = activity!!.intent.getParcelableExtra<ModelNotificationCustomer>("model")
                    when (i) {
                        1 -> {
                            if (activity!!.intent.hasExtra("fromNotification")) {
                                startActivity(
                                    Intent(context, sellerWelcomActivity::class.java)
                                        .putExtra("fromNotification", activity!!.intent.getStringExtra("fromNotification"))
                                        .putExtra("model", model)
                                )
                            } else {
                                startActivity(
                                    Intent(context, sellerWelcomActivity::class.java).putExtra("fromNotification", "0")
                                )
                            }
                            dismiss()
                        }
                        2 -> {
                            val auth = Constant.getPrefs(activity!!).getString(Constant.auth_code, "")
                            if (activity!!.intent.hasExtra("individualList")) {
                                mViewModel.rejectAssociatedRequest(
                                    auth,
                                    model.business_id,
                                    model.business_name,
                                    model.id
                                )

                            }else if (activity!!.intent.hasExtra("relink")){
                                val staff_id = activity!!.intent.getStringExtra("staffId")
                                mViewModel.rejectAssociatedRequest(
                                    auth,
                                    staff_id,
                                    model.business_name,
                                    model.id
                                )
                            }
                        }
                        else -> {
                            val staff_id = activity!!.intent.getStringExtra("staffId")
                            val auth = Constant.getPrefs(activity!!).getString(Constant.auth_code, "")
                            mViewModel.relinkStaff(auth, staff_id, model.business_id,model.id,model.business_name)

                        }

                    }

                }
            }

            observerInit()
            no.setOnClickListener {
                dismiss()
                activity!!.finish()
            }

            return view
        }

        private fun observerInit() {
            mViewModel.getmDataRejectAssociated().observe(this, Observer {
                if (it.status == "true") {
                   val intent = Intent(activity, AccountLinkDeniedIndividualNotified::class.java).putExtra("model",it)
                    intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
                    startActivity(intent)
                    activity!!.finish()

                }
            })

            mViewModel.getStatus().observe(this, Observer {
                if (it.msg == "Invalid auth code"){
                    Constant.commonAlert(activity!!)
                }else{
                    Toast.makeText(activity!!, it.msg, Toast.LENGTH_SHORT).show()
                }
            })


            mViewModel.getmDataRelink().observe(this, Observer {
                if (it.status == "true") {
                    val intent = Intent(activity, AccountLinkedMerchantNotifiedActivity::class.java)
                        .putExtra("model",it)
                    intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT)
                    startActivity(intent)
                    activity!!.finish()

                }
            })


        }

        private fun roundCorners(imageView: ImageView) {
            val curveRadius = 50F

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                imageView.outlineProvider = object : ViewOutlineProvider() {

                    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                    override fun getOutline(view: View?, outline: Outline?) {
                        outline?.setRoundRect(
                            0,
                            0,
                            view!!.width,
                            view.height + curveRadius.toInt(),
                            curveRadius
                        )
                    }
                }
                imageView.clipToOutline = true
            }

        }

    }

}
