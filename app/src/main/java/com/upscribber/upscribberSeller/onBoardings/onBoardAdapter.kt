package com.upscribber.upscribberSeller.onBoardings

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.viewpager.widget.PagerAdapter
import com.upscribber.R

class onBoardAdapter(var mData: ArrayList<onBoardModel>) : PagerAdapter() {
    var context: Context? = null

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun getCount(): Int {
        return mData.size
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val view = LayoutInflater.from(container.context)
            .inflate(R.layout.onboard_recycler, container, false)
        container.addView(view)
        bind(mData[position],view)

        return view

    }

    private fun bind(item: onBoardModel, view: View) {
        val titleTextView = view.findViewById(R.id.textViewHeading) as TextView
        val contentTextView = view.findViewById(R.id.textViewDesc) as TextView
        val contentImageView = view.findViewById(R.id.imageView) as ImageView
        titleTextView.text = item.mTextResource
        contentTextView.text = item.mTitleResource
        contentImageView.setImageResource(item.mImageResource)
    }



    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View?)
    }
}