package com.upscribber.upscribberSeller.onBoardings

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.TypedValue
import android.view.MenuItem
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.viewpager.widget.ViewPager
import com.upscribber.R
import me.relex.circleindicator.CircleIndicator
import me.relex.circleindicator.Config

class BoardingsActivity : AppCompatActivity() {

    var viewPagerboard1: ViewPager? = null
    var viewPagerboard2: ViewPager? = null
    var viewPagerboard3: ViewPager? = null
    var viewPagerboard4: ViewPager? = null
    private lateinit var circleIndi1: CircleIndicator
    private lateinit var circleIndi2: CircleIndicator
    private lateinit var circleIndi3: CircleIndicator
    private lateinit var circleIndi4: CircleIndicator
    var onBoardAdapter: onBoardAdapter? = null
    lateinit var toolbar: Toolbar
    lateinit var toolbar_title: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_boardings)
        initz()
        setToolbar()
        val array1 = ArrayList<onBoardModel>()

        array1.add(
            onBoardModel(
                R.mipmap.on_board1,
                "Update Business Info",
                "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem"
            )
        )
        array1.add(
            onBoardModel(
                R.mipmap.on_board1,
                "Update Business Info",
                "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem"
            )
        )
        array1.add(
            onBoardModel(
                R.mipmap.on_board1,
                "Update Business Info",
                "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem"
            )

        )
        val array2 = ArrayList<onBoardModel>()

        array2.add(
            onBoardModel(
                R.mipmap.on_board2,
                "Update Business Info",
                "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem"
            )
        )
        array2.add(
            onBoardModel(
                R.mipmap.on_board2,
                "Update Business Info",
                "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem"
            )
        )
        array2.add(
            onBoardModel(
                R.mipmap.on_board2,
                "Update Business Info",
                "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem"
            )

        )

        val array3 = ArrayList<onBoardModel>()

        array3.add(
            onBoardModel(
                R.mipmap.on_board3,
                "Update Business Info",
                "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem"
            )
        )
        array3.add(
            onBoardModel(
                R.mipmap.on_board3,
                "Update Business Info",
                "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem"
            )
        )
        array3.add(
            onBoardModel(
                R.mipmap.on_board3,
                "Update Business Info",
                "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem"
            )

        )

        val array4 = ArrayList<onBoardModel>()

        array4.add(
            onBoardModel(
                R.mipmap.on_board4,
                "Update Business Info",
                "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem"
            )
        )
        array4.add(
            onBoardModel(
                R.mipmap.on_board4,
                "Update Business Info",
                "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem"
            )
        )
        array4.add(
            onBoardModel(
                R.mipmap.on_board4,
                "Update Business Info",
                "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem"
            )

        )


        onBoardAdapter = onBoardAdapter(array1)
        viewPagerboard1!!.adapter = onBoardAdapter

        onBoardAdapter = onBoardAdapter(array2)
        viewPagerboard2!!.adapter = onBoardAdapter

        onBoardAdapter = onBoardAdapter(array3)
        viewPagerboard3!!.adapter = onBoardAdapter

        onBoardAdapter = onBoardAdapter(array4)
        viewPagerboard4!!.adapter = onBoardAdapter


        val indicatorWidth = (TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, 9.5f,
            resources.displayMetrics
        ) + 0.5f).toInt()
        val indicatorHeight = (TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, 2f,
            resources.displayMetrics
        ) + 0.5f).toInt()
        val indicatorMargin = (TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, 6f,
            resources.displayMetrics
        ) + 0.5f).toInt()

        val config = Config.Builder().width(indicatorWidth)
            .height(indicatorHeight)
            .margin(indicatorMargin)
            .drawable(R.drawable.invite_button_background)
            .build()

        circleIndi1.initialize(config)
        circleIndi2.initialize(config)
        circleIndi3.initialize(config)
        circleIndi4.initialize(config)
        viewPagerboard1!!.adapter = onBoardAdapter(array1)
        viewPagerboard2!!.adapter = onBoardAdapter(array2)
        viewPagerboard3!!.adapter = onBoardAdapter(array3)
        viewPagerboard4!!.adapter = onBoardAdapter(array4)
        circleIndi1.setViewPager(viewPagerboard1)
        circleIndi2.setViewPager(viewPagerboard2)
        circleIndi3.setViewPager(viewPagerboard3)
        circleIndi4.setViewPager(viewPagerboard4)
    }

    private fun setToolbar() {
        setSupportActionBar(toolbar)
        title = ""
        toolbar_title.text= "On Boarding"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)

    }


    private fun initz() {
        viewPagerboard1 = findViewById(R.id.viewPagerboard1)
        viewPagerboard2 = findViewById(R.id.viewPagerboard2)
        viewPagerboard3 = findViewById(R.id.viewPagerboard3)
        viewPagerboard4 = findViewById(R.id.viewPagerboard4)
        circleIndi1 = findViewById(R.id.circleIndi1)
        circleIndi2 = findViewById(R.id.circleIndi2)
        circleIndi3 = findViewById(R.id.circleIndi3)
        circleIndi4 = findViewById(R.id.circleIndi4)
        toolbar_title = findViewById(R.id.title)
        toolbar = findViewById(R.id.include3)

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }

        return super.onOptionsItemSelected(item)
    }
}
