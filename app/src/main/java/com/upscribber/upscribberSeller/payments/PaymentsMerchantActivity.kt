package com.upscribber.upscribberSeller.payments

import android.app.Activity
import android.content.Intent
import android.graphics.Outline
import android.os.Build
import android.os.Bundle
import android.view.*
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.commonClasses.RoundedBottomSheetDialogFragment
import com.upscribber.databinding.ActivityPaymentsMerchantBinding
import com.upscribber.databinding.DeleteCardSheetBinding
import com.upscribber.payment.customerPayments.PaymentsViewModel
import com.upscribber.payment.paymentCards.AddCardActivity
import com.upscribber.payment.paymentCards.PaymentCardModel
import com.upscribber.profile.ModelGetProfile
import com.upscribber.upscribberSeller.payments.bankAccount.AdapterBankCards
import com.upscribber.upscribberSeller.payments.bankAccount.AddBankAccountActivity
import com.upscribber.upscribberSeller.pictures.ConfirmDeleteImageActivity
import kotlinx.android.synthetic.main.activity_payments_merchant.*

class PaymentsMerchantActivity : AppCompatActivity(),
    AdapterBankCards.SetBankCheckedd, AdapterPaymentMerchantCards.PaymentCards {


    private var modelProfile = ModelGetProfile()

    lateinit var toolbar: Toolbar
    lateinit var toolbarTitle: TextView
    lateinit var recyclerCreditCard: RecyclerView
    lateinit var recyclerBankAccount: RecyclerView
    private lateinit var mAdapterBank: AdapterBankCards
    private lateinit var mAdapterCredit: AdapterPaymentMerchantCards
    private lateinit var binding: ActivityPaymentsMerchantBinding
    private lateinit var switchCreditCards: Switch
    private lateinit var switchBankCards: Switch
    private lateinit var addCards: ImageView
    lateinit var mViewModel: PaymentsViewModel


    companion object {
        private lateinit var categoriesList: ArrayList<PaymentCardModel>
        private lateinit var backgroundChange: ArrayList<PaymentCardModel>
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_payments_merchant)
        mViewModel = ViewModelProviders.of(this)[PaymentsViewModel::class.java]
        binding.recyclerBankAccount.clipToPadding = false
        initz()
        setToolbar()
        click()
        retreiveCardDetails()
        observerInit()
        setAdapter()


    }

    private fun observerInit() {
        mViewModel.getUpdatedPaymentMethod().observe(this, Observer {
            if (it.status == "true") {
                if (it.type == "card") {
                    val model = backgroundChange[it.position]
                    if (model.card) {
                        model.card = false
                    } else {
                        for (child in backgroundChange) {
                            child.card = false
                        }
                        model.card = true
                    }
                    backgroundChange[it.position] = model
                    mAdapterCredit.notifyDataSetChanged()
                }
            } else {
                val model = categoriesList[it.position]
                if (model.card) {
                    model.card = false

                } else {
                    for (child in categoriesList) {
                        child.card = false
                    }
                    model.card = true
                }


                categoriesList[it.position] = model

                mAdapterBank.notifyDataSetChanged()
            }


        })

    }


    override fun setAsDefault(
        model: PaymentCardModel,
        position: Int
    ) {
        val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
        mViewModel.updateMerchantPaymentMethod(auth, model.id, position, "card")
    }

    override fun deleteCard(model: PaymentCardModel, position: Int) {
        for (i in backgroundChange) {
            if (i.id == model.id) {
                Toast.makeText(this, "You cannot delete dafault payment card", Toast.LENGTH_SHORT)
                    .show()
            } else {
                val fragment = MenuFragmentDeleteCard()
                fragment.show(supportFragmentManager, fragment.tag)
            }
        }
    }

    private fun setAdapter() {
        var array: ArrayList<PaymentCardModel> = ArrayList()
        var array2: ArrayList<PaymentCardModel> = ArrayList()
        if (intent.hasExtra("card")) {
            array = intent.getParcelableArrayListExtra<PaymentCardModel>("card")
            backgroundChange = array
        } else {
            array = backgroundChange
        }

        if (intent.hasExtra("bank")) {
            array2 = intent.getParcelableArrayListExtra<PaymentCardModel>("bank")
            categoriesList = array2
        } else {
            array2 = categoriesList
        }



        if (array.size == 0 && array2.size == 0) {
            binding.imageView85.visibility = View.VISIBLE
            binding.noData.visibility = View.VISIBLE
            binding.noDataDescription.visibility = View.VISIBLE
            binding.recyclerCreditCard.visibility = View.GONE
            binding.textView359.visibility = View.GONE
            binding.textView180.visibility = View.GONE
            binding.recyclerBankAccount.visibility = View.GONE
            binding.textView3.visibility = View.GONE
            binding.textView181.visibility = View.GONE
        } else {
            if (array.size > 0) {
                binding.recyclerCreditCard.visibility = View.VISIBLE
                binding.textView359.visibility = View.VISIBLE
                binding.textView180.visibility = View.GONE
                binding.recyclerCreditCard.layoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
                mAdapterCredit = AdapterPaymentMerchantCards(this,Constant.getDisplayMetrics(windowManager), backgroundChange)
                mAdapterCredit.setItemMargin(16)
                mAdapterCredit.updateDisplayMetrics()
                PagerSnapHelper().attachToRecyclerView(binding.recyclerCreditCard)
                binding.recyclerCreditCard.adapter = mAdapterCredit
            } else {
                binding.recyclerCreditCard.visibility = View.GONE
                binding.textView359.visibility = View.GONE
                binding.textView180.visibility = View.GONE
            }

            if (array2.size > 0) {
                binding.recyclerBankAccount.visibility = View.VISIBLE
                binding.textView3.visibility = View.VISIBLE
                binding.textView181.visibility = View.VISIBLE
                binding.recyclerBankAccount.layoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
                mAdapterBank = AdapterBankCards(this, Constant.getDisplayMetrics(windowManager),categoriesList)
                mAdapterBank.setItemMargin(16)
                mAdapterBank.updateDisplayMetrics()
                PagerSnapHelper().attachToRecyclerView(binding.recyclerBankAccount)
                binding.recyclerBankAccount.adapter = mAdapterBank
            } else {
                binding.recyclerBankAccount.visibility = View.GONE
                binding.textView3.visibility = View.GONE
                binding.textView181.visibility = View.GONE
            }

        }


    }

    private fun retreiveCardDetails() {
        mViewModel.retreiveCredit()

    }


    override fun onResume() {
        super.onResume()

    }

    private fun click() {
        switchCreditCards.setOnCheckedChangeListener { p0, p1 ->
            if (p1) {
                switchBankCards.isChecked = false
                switchCreditCards.isChecked = p1
            } else {
                switchBankCards.isChecked = true
                switchCreditCards.isChecked = p1
            }

        }

        switchBankCards.setOnCheckedChangeListener { p0, p1 ->

            if (p1) {
                switchCreditCards.isChecked = false
                switchBankCards.isChecked = p1
            } else {
                switchCreditCards.isChecked = true
                switchBankCards.isChecked = p1
            }
        }

        addCards.setOnClickListener {
            val fragment = MenuFragment(modelProfile)
            fragment.show(supportFragmentManager, fragment.tag)

        }

        textView3.setOnClickListener {
            startActivityForResult(
                Intent(
                    this,
                    AddBankAccountActivity::class.java
                ).putExtra("modelProfile", modelProfile), 100
            )
        }

        textView359.setOnClickListener {
            startActivityForResult(
                Intent(this, AddCardActivity::class.java).putExtra("paymentsProfile","paymentsProfile").putExtra("profileModel",modelProfile),101
            )
        }


    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (data != null) {
                if (data.hasExtra("bankData")) {
                    categoriesList = data.getParcelableArrayListExtra("bankData")
                    mAdapterBank.update(categoriesList)
                }
            }

        }
        if (resultCode == Activity.RESULT_OK) {
            if (data != null) {
                if (data.hasExtra("card")) {
                    backgroundChange = data.getParcelableArrayListExtra("card")
                    mAdapterCredit.update(backgroundChange)
                }
            }

        }
    }

    class MenuFragment(var modelProfile: ModelGetProfile) : RoundedBottomSheetDialogFragment() {

        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val view = inflater.inflate(R.layout.add_cards_option_sheet, container, false)

            val cancelDialog = view.findViewById<TextView>(R.id.cancelDialog)
            val addCreditCard = view.findViewById<LinearLayout>(R.id.addCreditCard)
            val addBankAccount = view.findViewById<LinearLayout>(R.id.addBankAccount)
            cancelDialog.setOnClickListener {
                dismiss()
            }

            addCreditCard.setOnClickListener {
                startActivityForResult(
                    Intent(activity, AddCardActivity::class.java).putExtra("paymentsProfile","paymentsProfile").putExtra("profileModel",modelProfile),101
                )
                dismiss()

            }

            addBankAccount.setOnClickListener {
                startActivityForResult(
                    Intent(
                        activity,
                        AddBankAccountActivity::class.java
                    ).putExtra("modelProfile", modelProfile), 100
                )
                dismiss()
            }

            return view
        }

        override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
            super.onActivityResult(requestCode, resultCode, data)
            if (requestCode == 100 && resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    categoriesList = data.getParcelableArrayListExtra("bankData")
                }

            }

            if (requestCode == 101 && resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    backgroundChange = data.getParcelableArrayListExtra("card")
                }

            }
        }


    }


    private fun setToolbar() {

        setSupportActionBar(toolbar)
        title = ""

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)
        if (intent.hasExtra("profile")) {
            toolbarTitle.text = "Accounts"
        } else {
            toolbarTitle.text = "Payments"
        }

    }

    private fun initz() {

        toolbar = findViewById(R.id.toolbarPayments)
        toolbarTitle = findViewById(R.id.title)
        recyclerCreditCard = findViewById(R.id.recyclerCreditCard)
        recyclerBankAccount = findViewById(R.id.recyclerBankAccount)
        switchCreditCards = findViewById(R.id.switchCreditCards)
        switchBankCards = findViewById(R.id.switchBankCards)
        addCards = findViewById(R.id.addCards)

        if (intent.hasExtra("modelProfile")) {
            modelProfile = intent.getParcelableExtra("modelProfile")

        }
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }


    override fun setBankCardChecked(
        position: Int,
        model: PaymentCardModel
    ) {
        val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
        mViewModel.updateMerchantPaymentMethod(auth, model.id, position, "bank")

    }


    override fun BottomDelete(
        position: Int,
        model: PaymentCardModel
    ) {
        for (i in categoriesList) {
            if (i.id == model.id) {
                Toast.makeText(this, "You cannot delete dafault payment card", Toast.LENGTH_SHORT)
                    .show()
            } else {
                val fragment = MenuFragmentDeleteCard()
                fragment.show(supportFragmentManager, fragment.tag)
            }
        }

    }

    class MenuFragmentDeleteCard : RoundedBottomSheetDialogFragment() {

        lateinit var binding: DeleteCardSheetBinding


        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            binding =
                DataBindingUtil.inflate(inflater, R.layout.delete_card_sheet, container, false)

            binding.no.setOnClickListener {
                dismiss()
            }

            binding.yes.setOnClickListener {
                startActivity(Intent(activity, ConfirmDeleteImageActivity::class.java))
                dismiss()
//                activity.finish()
            }

            return binding.root


        }

        override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
            super.onViewCreated(view, savedInstanceState)
            roundedImage()
        }

        private fun roundedImage() {
            val curveRadius = 50F

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                binding.imageViewTop.outlineProvider = object : ViewOutlineProvider() {

                    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                    override fun getOutline(view: View?, outline: Outline?) {
                        outline?.setRoundRect(
                            0,
                            0,
                            view!!.width,
                            view.height + curveRadius.toInt(),
                            curveRadius
                        )
                    }
                }
                binding.imageViewTop.clipToOutline = true
            }

        }
    }
}
