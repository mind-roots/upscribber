package com.upscribber.upscribberSeller.payments.bankAccount

import android.content.Context
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.R
import com.upscribber.payment.paymentCards.PaymentCardModel

class AdapterBankCards(var mContext: Context, var metrics: DisplayMetrics, var list: ArrayList<PaymentCardModel>)  :
    RecyclerView.Adapter<AdapterBankCards.MyViewHolder>() {


    var checked = mContext as SetBankCheckedd
    private var itemMargin: Int = 0
    private var itemWidth: Int = 0

    internal fun setItemMargin(itemMargin: Int) {
        this.itemMargin = itemMargin
    }

    internal fun updateDisplayMetrics() {
        itemWidth = metrics.widthPixels - itemMargin * 2
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(mContext).inflate(R.layout.merchant_cards_layout1, parent, false)
        return MyViewHolder(v)

    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder. bank_name.text = list[position].bank_name
        holder. account_number.text = "xxxx-xxxx-xxxx-" +list[position].last4
        holder.account_holder_name.text = list[position].account_holder_name
        var currentItemWidth = itemWidth
        if (position == 0) {
            currentItemWidth += itemMargin
            holder.itemView.setPadding(itemMargin, 0, 0, 0)
        } else if (position == itemCount - 1) {
            currentItemWidth += itemMargin
            holder.itemView.setPadding(0, 0, itemMargin, 0)
        }

        val height = holder.itemView.layoutParams.height
        holder.itemView.layoutParams = ViewGroup.LayoutParams(currentItemWidth, height)

        holder.bgImage.setOnClickListener {
            checked.setBankCardChecked(position,list[position])
        }

        if (list[position].card){
            holder.checkedd.visibility=View.GONE
        }else{
            holder.checkedd.visibility=View.GONE
        }

        holder.crossDelete.setOnClickListener {
            checked.BottomDelete(position,list[position])
        }
    }

    fun update(it: java.util.ArrayList<PaymentCardModel>?) {
        list = it!!
        notifyDataSetChanged()

    }

    class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val bgImage = view.findViewById(R.id.bgImage) as ImageView
        val checkedd = view.findViewById(R.id.checked) as ImageView
        val crossDelete = view.findViewById(R.id.crossDelete) as ImageView
        val bank_name = view.findViewById(R.id.bank_name) as TextView
        val account_number = view.findViewById(R.id.account_number) as TextView
        val account_holder_name = view.findViewById(R.id.account_holder_name) as TextView
    }

    interface SetBankCheckedd {
        fun setBankCardChecked(
            position: Int,
            paymentCardModel: PaymentCardModel
        )
        fun BottomDelete(
            position: Int,
            paymentCardModel: PaymentCardModel
        )
    }

}