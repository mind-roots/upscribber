package com.upscribber.upscribberSeller.payments.bankAccount

import android.app.Activity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.upscribber.R
import com.upscribber.profile.ModelGetProfile
import com.upscribber.upscribberSeller.payments.PaymentMerchantViewModel
import kotlinx.android.synthetic.main.activity_add_bank_account.*

class AddBankAccountActivity : AppCompatActivity() {

    lateinit var toolbar: Toolbar
    lateinit var toolbarTitle: TextView
    lateinit var mViewModel: PaymentMerchantViewModel
    private var modelProfile = ModelGetProfile()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_bank_account)
        initz()
        setToolbar()
        click()
        observerInit()
    }

    private fun click() {

        etOwnersName.setOnFocusChangeListener { view, p1 ->
            if (p1) {
                etOwnersName.setBackgroundResource(R.drawable.bg_seller_blue_outline)
            } else {
                etOwnersName.setBackgroundResource(R.drawable.login_background)
            }
        }


        etRoutingNumber.setOnFocusChangeListener { view, p1 ->
            if (p1) {
                etRoutingNumber.setBackgroundResource(R.drawable.bg_seller_blue_outline)
            } else {
                etRoutingNumber.setBackgroundResource(R.drawable.login_background)
            }
        }

        etAccountNumber.setOnFocusChangeListener { view, p1 ->
            if (p1) {
                etAccountNumber.setBackgroundResource(R.drawable.bg_seller_blue_outline)
            } else {
                etAccountNumber.setBackgroundResource(R.drawable.login_background)
            }
        }




        addBank.setOnClickListener {
            if (etOwnersName.text.isEmpty() || etRoutingNumber.text.isEmpty() || etAccountNumber.text.isEmpty()) {
                Toast.makeText(this, "Please enter data in all fields", Toast.LENGTH_SHORT).show()
            } else if (etRoutingNumber.text.length <= 8) {
                Toast.makeText(this, "Please enter a valid routing number", Toast.LENGTH_SHORT)
                    .show()
            } else if (etAccountNumber.text.length <= 9) {
                Toast.makeText(this, "Please enter a valid account number", Toast.LENGTH_SHORT)
                    .show()
            } else {
                mViewModel.addBankAccount(
                    "0",
                    "1",
                    etOwnersName.text.toString(),
                    etRoutingNumber.text.toString(),
                    etAccountNumber.text.toString()
                )
                progress_small.visibility = View.VISIBLE
            }
            //finish()
        }
    }

    private fun observerInit() {
        mViewModel.getBankResponse().observe(this, Observer {
            if (it.status == "true") {
                mViewModel.getStripeResponse(modelProfile.stripe_account_id, "bank_account")
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
            } else {
                progress_small.visibility = View.GONE
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
            }
        })


        mViewModel.getDataBank().observe(this, Observer {
            intent.putParcelableArrayListExtra("bankData", it)
            setResult(Activity.RESULT_OK, intent)
            progress_small.visibility = View.GONE
            finish()
        })
    }

    private fun setToolbar() {
        setSupportActionBar(toolbar)
        title = ""
        toolbarTitle.text = "Add Bank Account"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)

    }

    private fun initz() {
        toolbar = findViewById(R.id.addCardToolbar)
        toolbarTitle = findViewById(R.id.title)
        mViewModel = ViewModelProviders.of(this)[PaymentMerchantViewModel::class.java]
        if (intent.hasExtra("modelProfile")) {
            modelProfile = intent.getParcelableExtra("modelProfile")
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}
