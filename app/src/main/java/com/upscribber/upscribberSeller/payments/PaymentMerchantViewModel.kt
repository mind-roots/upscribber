package com.upscribber.upscribberSeller.payments

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.upscribber.commonClasses.ModelStatusMsg
import com.upscribber.payment.paymentCards.PaymentCardModel

class PaymentMerchantViewModel(application: Application) : AndroidViewModel(application) {

    var paymentMerchantRepository: PaymentMerchantRepository =
        PaymentMerchantRepository(application)

    fun addBankAccount(
        default: String,
        type: String,
        bank_owner_name: String,
        routing_number: String,
        account_number: String
    ) {
        paymentMerchantRepository.addBankAccount(
            default,
            type,
            bank_owner_name,
            routing_number,
            account_number
        )
    }

    fun getBankResponse(): MutableLiveData<ModelStatusMsg> {
        return paymentMerchantRepository.getBankResponse()
    }

    fun getStripeResponse(stripeId: String, type: String) {
        paymentMerchantRepository.getBankCardDetails(stripeId,type)
    }



    fun getDataBank(): MutableLiveData<ArrayList<PaymentCardModel>> {
        return paymentMerchantRepository.getmDataBank()
    }

}