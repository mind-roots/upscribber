package com.upscribber.upscribberSeller.payments

import android.app.Application
import android.widget.Toast
import androidx.lifecycle.MutableLiveData
import com.upscribber.commonClasses.Constant
import com.upscribber.commonClasses.ModelStatusMsg
import com.upscribber.commonClasses.WebServicesCustomers
import com.upscribber.commonClasses.WebServicesMerchants
import com.upscribber.payment.paymentCards.PaymentCardModel
import com.upscribber.upscribberSeller.payments.bankAccount.CardDetailsTwoModel
import com.upscribber.upscribberSeller.payments.creditCard.ModelCreditCards
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit


class PaymentMerchantRepository(var application: Application) {
    val mData1 = MutableLiveData<ArrayList<CardDetailsTwoModel>>()
    val arrayList1 = ArrayList<CardDetailsTwoModel>()
    val bankData = MutableLiveData<ModelStatusMsg>()
    val mDataStripeDataCard = MutableLiveData<ArrayList<PaymentCardModel>>()
    val mDataStripeDataBank = MutableLiveData<ArrayList<PaymentCardModel>>()
    val mData2 = MutableLiveData<ArrayList<ModelCreditCards>>()
    val arrayList2 = ArrayList<ModelCreditCards>()


    fun addBankAccount(
        default: String,
        type: String,
        bank_owner_name: String,
        routing_number: String,
        account_number: String
    ) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()
        val auth = Constant.getPrefs(application).getString(Constant.auth_code, "")

        val service = retrofit.create(WebServicesMerchants::class.java)
        val call: Call<ResponseBody> = service.updateExternalAccount(
            auth,
            default,
            type,
            bank_owner_name,
            routing_number,
            account_number
        )
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                try {
                    if (response.isSuccessful) {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        if (status == "true") {
                            val modelStatusMsg = ModelStatusMsg()
                            modelStatusMsg.status = status
                            modelStatusMsg.msg = msg
                            bankData.value = modelStatusMsg
                        } else {
                            val modelStatusMsg = ModelStatusMsg()
                            modelStatusMsg.status = status
                            modelStatusMsg.msg = msg
                            bankData.value = modelStatusMsg
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatusMsg = ModelStatusMsg()
                modelStatusMsg.status = "false"
                modelStatusMsg.msg = "Network Error"
                bankData.value = modelStatusMsg
            }
        })
    }

    fun getBankResponse(): MutableLiveData<ModelStatusMsg> {
        return bankData
    }


    fun getBankCardDetails(stripeId: String, type: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.stripeTokenUrl)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.paymentCardBank(stripeId, type)
        call.enqueue(object : Callback<ResponseBody> {


            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val arrayData = ArrayList<PaymentCardModel>()
                        val cardModel = PaymentCardModel()
                        val data = json.optJSONArray("data")
                        for (i in 0 until data.length()) {
                            val stripeData = data.getJSONObject(i)

                            if (type == "card") {
                                cardModel.id = stripeData.optString("id")
                                cardModel.brand = stripeData.optString("brand")
                                cardModel.country = stripeData.optString("country")
                                cardModel.last4 = stripeData.optString("last4")
                            } else {
                                cardModel.id = stripeData.optString("id")
                                cardModel.account_holder_name =
                                    stripeData.optString("account_holder_name")
                                cardModel.account_holder_type =
                                    stripeData.optString("account_holder_type")
                                cardModel.bank_name = stripeData.optString("bank_name")
                                cardModel.routing_number = stripeData.optString("routing_number")
                                cardModel.last4 = stripeData.optString("last4")
                            }
                            arrayData.add(cardModel)
                        }
                        if (type == "card") {
                            mDataStripeDataCard.value = arrayData
                        } else {
                            mDataStripeDataBank.value = arrayData
                        }

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Toast.makeText(application, "Error!", Toast.LENGTH_LONG).show()
            }
        })
    }

    fun getmDataCard(): MutableLiveData<ArrayList<PaymentCardModel>> {
        return mDataStripeDataCard
    }

    fun getmDataBank(): MutableLiveData<ArrayList<PaymentCardModel>> {
        return mDataStripeDataBank
    }
}