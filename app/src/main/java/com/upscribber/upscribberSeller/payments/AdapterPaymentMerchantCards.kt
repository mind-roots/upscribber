package com.upscribber.upscribberSeller.payments

import java.util.ArrayList
import android.content.Context
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.R
import com.upscribber.payment.paymentCards.PaymentCardModel
import kotlinx.android.synthetic.main.cards_layout_checkout.view.CardNumber
import kotlinx.android.synthetic.main.cards_layout_merchant_payment.view.*

class AdapterPaymentMerchantCards(private val mContext: Context, var metrics: DisplayMetrics, private var list: ArrayList<PaymentCardModel>) :
    RecyclerView.Adapter<AdapterPaymentMerchantCards.MyViewHolder>() {

    var listener = mContext as PaymentCards
    private var itemMargin: Int = 0
    private var itemWidth: Int = 0

    internal fun setItemMargin(itemMargin: Int) {
        this.itemMargin = itemMargin
    }

    internal fun updateDisplayMetrics() {
        itemWidth = metrics.widthPixels - itemMargin * 2
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(mContext).inflate(R.layout.cards_layout_merchant_payment, parent, false)
        return MyViewHolder(v)

    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = list[position]

        var currentItemWidth = itemWidth
        if (position == 0) {
            currentItemWidth += itemMargin
            holder.itemView.setPadding(itemMargin, 0, 0, 0)
        } else if (position == itemCount - 1) {
            currentItemWidth += itemMargin
            holder.itemView.setPadding(0, 0, itemMargin, 0)
        }

        val height = holder.itemView.layoutParams.height
        holder.itemView.layoutParams = ViewGroup.LayoutParams(currentItemWidth, height)



        holder.itemView.CardNumber.text = "xxxx-xxxx-xxxx-" + model.last4
        when (model.brand) {
            "visa" -> {
                holder.itemView.cardCompany.setImageResource(R.drawable.new_visa_icon)
            }
            "mastercard" -> {
                holder.itemView.cardCompany.setImageResource(R.drawable.ic_mastercard_new)
            }
            else -> {
                holder.itemView.cardCompany.setImageResource(R.drawable.ic_card_dummy)
            }
        }


        holder.itemView.cardHolderName.text = model.name



//        if (list.size <= 1) {
//            holder.itemView.linear.layoutParams.width = ConstraintLayout.LayoutParams.MATCH_PARENT
//            holder.itemView.linear.requestLayout()
//            model.card = true
//        }else{
//            holder.itemView.linear.layoutParams.width = ConstraintLayout.LayoutParams.WRAP_CONTENT
//            holder.itemView.linear.requestLayout()
//        }

        holder.itemView.cl_cards.setOnClickListener{
            listener.setAsDefault(model,position)
        }

        holder.itemView.imgDelete.setOnClickListener{
            listener.deleteCard(model,position)
        }



        if (model.card) {
            holder.itemView.imgDefault.visibility = View.GONE
        } else {
            holder.itemView.imgDefault.visibility = View.GONE
        }



    }

    fun update(it: ArrayList<PaymentCardModel>?) {
        list = it!!
        notifyDataSetChanged()

    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    interface PaymentCards{
        fun setAsDefault(
            model: PaymentCardModel,
            position: Int
        )
        fun deleteCard(model: PaymentCardModel,position: Int)
    }
}