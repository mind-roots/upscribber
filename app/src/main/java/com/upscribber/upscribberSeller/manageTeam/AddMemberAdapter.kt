package com.upscribber.upscribberSeller.manageTeam

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.R
import com.upscribber.databinding.AddMemberAvatarImageBinding
import kotlinx.android.synthetic.main.add_member_avatar_image.view.*

class AddMemberAdapter(
    var context: Context
//    listener: ManageTeamActivity.MenuFragment
) : RecyclerView.Adapter<AddMemberAdapter.ViewHolder>() {

    private var item: List<AddMemberModel> = emptyList()
    lateinit var mBinding: AddMemberAvatarImageBinding
//    var listener = listener as selectionAvatarImages

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(context),
            R.layout.add_member_avatar_image, parent, false)
        return ViewHolder(mBinding.root)

    }

    override fun getItemCount(): Int {
        return item.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model1 = item[position]
        mBinding.model= model1

        holder.itemView.setOnClickListener {
//            listener.backgroundSelectedImages(position)
        }

        if (model1.status) {
            holder.itemView.imageGradient.visibility = View.VISIBLE
        } else {
            holder.itemView.imageGradient.visibility = View.INVISIBLE
        }

    }

    fun update1(items: List<AddMemberModel>) {
        this.item = items
        notifyDataSetChanged()
    }


    companion object {
        @JvmStatic
        @BindingAdapter("items")
        fun RecyclerView.bindItems(items: List<AddMemberModel>) {
            val adapter = adapter as AddMemberAdapter
            adapter.update1(items)
        }
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)

    interface selectionAvatarImages {
        fun backgroundSelectedImages(
            position: Int
        )
    }
}