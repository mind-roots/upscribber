package com.upscribber.upscribberSeller.manageTeam

import android.app.Activity
import android.content.Intent
import android.graphics.Outline
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.*
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.upscribber.commonClasses.Constant
import com.upscribber.commonClasses.RoundedBottomSheetDialogFragment
import com.upscribber.upscribberSeller.accountLinking.UnlinkAccountActivity
import com.upscribber.R
import com.upscribber.databinding.ActivityManageTeamBinding
import com.upscribber.filters.*
import com.upscribber.filters.Redemption.RedemptionFilterFragment
import com.upscribber.upscribberSeller.accountLinking.AccountLinkedMerchantNotifiedActivity
import com.upscribber.upscribberSeller.individualProfile.addIndividual.AddIndividualInvite
import com.upscribber.upscribberSeller.store.storeInfo.CategoryProductsFragment
import com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.createSubs.CreateSubscriptionDetailsActivity
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.filter_header.*


@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class ManageTeamActivity : AppCompatActivity(), ManageTeamAdapter.OpenMenuFragmet, FilterInterface,
    DrawerLayout.DrawerListener, FilterFragment.SetFilterHeaderBackground
//    AddMemberAdapter.selectionAvatarImages
{

    lateinit var toolbar: Toolbar
    lateinit var manageTeamRecycler: RecyclerView
    private lateinit var binding: ActivityManageTeamBinding
    private lateinit var viewModel: ManageTeamViewModel
    private lateinit var mAdapter: ManageTeamAdapter
    private lateinit var manageDone: ImageView
    private lateinit var imageUri: Uri
    private lateinit var filter_done: TextView
    private lateinit var fragmentManager: FragmentManager
    private lateinit var fragmentTransaction: FragmentTransaction
    private lateinit var mreset: TextView
    private lateinit var mfilters: TextView
    private lateinit var selection: ArrayList<ManageTeamModel>
    var drawerOpen = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_manage_team)
        binding.lifecycleOwner = this
        viewModel = ViewModelProviders.of(this)[ManageTeamViewModel::class.java]
        initz()
        clickLIsteners()
        visibilityConditions()
        apiInit()
        setAdapter()
        observerInit()

    }

    private fun setAdapter() {
        var value = ""
        if (intent.hasExtra("chooseTeam")) {
            value = intent.getStringExtra("chooseTeam")
        }
        binding.manageTeamRecycler.layoutManager = LinearLayoutManager(this)
        mAdapter = ManageTeamAdapter(this, value)
        binding.manageTeamRecycler.adapter = mAdapter

    }


    private fun visibilityConditions() {

        when {
            intent.hasExtra("notSelected") -> {
                binding.constraintBackground.visibility = View.VISIBLE
                binding.toolbarManage.visibility = View.GONE
                binding.done.visibility = View.GONE
                setSupportActionBar(binding.CustomerListToolbar)
                title = ""
                binding.titleToolbar.text = "Manage Staff"
                supportActionBar!!.setDisplayHomeAsUpEnabled(true)
                supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)
                binding.imgNoSubscription.setImageDrawable(resources.getDrawable(R.drawable.no_team_members))
                roundImage()
            }
            intent.hasExtra("chooseTeam") -> {
                binding.constraintBackground.visibility = View.GONE
                binding.toolbarManage.visibility = View.VISIBLE
                binding.done.visibility = View.GONE
                setSupportActionBar(toolbar)
                title = ""
                binding.title.text = "Select Members"
                supportActionBar!!.setDisplayHomeAsUpEnabled(true)
                supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)
                binding.imgNoSubscription.setImageDrawable(resources.getDrawable(R.drawable.no_select_team_members))
            }
            else -> {
                binding.constraintBackground.visibility = View.GONE
                binding.toolbarManage.visibility = View.VISIBLE
                binding.done.visibility = View.GONE
                setSupportActionBar(toolbar)
                title = ""
                binding.title.text = "Manage Staff"
                supportActionBar!!.setDisplayHomeAsUpEnabled(true)
                supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)
                binding.imgNoSubscription.setImageDrawable(resources.getDrawable(R.drawable.no_team_members))

            }
        }

    }

    private fun observerInit() {
        viewModel.getTeamMembersListtt().observe(this, Observer {
            binding.progressBar22.visibility = View.GONE
            binding.imgNoSubscription.visibility = View.GONE
            binding.tvNoData.visibility = View.GONE
            binding.tvNoDataDescription.visibility = View.GONE
            binding.getStartedBtn.visibility = View.GONE
            binding.manageTeamRecycler.visibility = View.VISIBLE
            if (it.size > 0) {
                it.reverse()
                selection = it
                if (intent.hasExtra("selectedArray")) {
                    val arraySelected = intent.getParcelableArrayListExtra<ManageTeamModel>("selectedArray")
                    for (i in 0 until selection.size) {
                        for (item in arraySelected) {
                            if (item.staff_id == selection[i].staff_id) {
                                selection[i].status1 = true
                                binding.done.visibility = View.VISIBLE
                            }
                        }

                    }
                }
                mAdapter.update(selection)
            } else {
                binding.done.visibility = View.GONE
                binding.imgNoSubscription.visibility = View.VISIBLE
                binding.tvNoData.visibility = View.VISIBLE
                binding.tvNoDataDescription.visibility = View.VISIBLE
                binding.getStartedBtn.visibility = View.VISIBLE
                binding.manageTeamRecycler.visibility = View.GONE

            }
        })

        viewModel.getmUnlinked().observe(this, Observer {
            if (it.status == "true") {
                binding.progressBar22.visibility = View.GONE
                startActivityForResult(Intent(this, AccountLinkedMerchantNotifiedActivity::class.java).putExtra("linked",it),1)
            }
        })

        viewModel.getmRelinked().observe(this, Observer {
            if (it.status == "true") {
                binding.progressBar22.visibility = View.GONE
                startActivityForResult(Intent(this, UnlinkAccountActivity::class.java),1)
            }
        })

        viewModel.getStatus().observe(this, Observer {
            if (it.msg == "Invalid auth code") {
                Constant.commonAlert(this)
            } else {
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
            }

        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == 65537){
            val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
            viewModel.getTeamMembersList(auth, "0", "", "1", "")
        }
    }

    private fun apiInit() {
        val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
        val isSeller = Constant.getSharedPrefs(this).getBoolean(Constant.IS_SELLER, false)

        var value = ""
        if (isSeller) {
            if (intent.hasExtra("chooseTeam")) {
                value = intent.getStringExtra("chooseTeam")
                viewModel.getTeamMembersList(auth, "3", value, "", "")
            } else {
                viewModel.getTeamMembersList(auth, "0", "", "1", "")
            }
        } else {
            viewModel.getTeamMembersList(auth, "", "", "", "")
        }

        binding.progressBar22.visibility = View.VISIBLE
    }


    override fun selection(position: Int) {
        var exist = 0
        val model = selection[position]
        model.status1 = !model.status1

        selection[position] = model

        for(items in selection){
            if(items.status1){
                exist = 1
                break
            }
        }
        if(exist == 1){
            binding.done.visibility = View.VISIBLE
        }else{
            binding.done.visibility = View.GONE
        }
        mAdapter.notifyDataSetChanged()

    }

    private fun clickLIsteners() {
        binding.btnFilter.setOnClickListener {
            drawerOpen = 1
            Constant.hideKeyboard(this, binding.search)
            binding.search.setQuery("", false)
            binding.search.clearFocus();
            binding.drawerLayout1.openDrawer(GravityCompat.END)
            inflateFragment(FilterFragment(this))
            filterImgBack.visibility = View.GONE
            mfilters.text = "Filters"
            mreset.visibility = View.VISIBLE

        }


        binding.search.setOnQueryTextListener(object : androidx.appcompat.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                Constant.hideKeyboard(this@ManageTeamActivity,binding.search)
                val auth = Constant.getPrefs(this@ManageTeamActivity).getString(Constant.auth_code, "")
                val isSeller = Constant.getSharedPrefs(this@ManageTeamActivity).getBoolean(Constant.IS_SELLER, false)

                var value = ""
                if (isSeller) {
                    if (intent.hasExtra("chooseTeam")) {
                        value = intent.getStringExtra("chooseTeam")
                        viewModel.getTeamMembersList(auth, "3", value,"",query.toString().trim())
                    } else {
                        viewModel.getTeamMembersList(auth, "0", "","1",query.toString().trim())
                    }
                } else {
                    viewModel.getTeamMembersList(auth, "", "","",query.toString().trim())
                }

                binding.progressBar22.visibility = View.VISIBLE
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {

                return true

            }


        })


        filter_done.setOnClickListener {
            onBackPressed()
        }

        binding.done.setOnClickListener {

            val arrayTeam: ArrayList<ManageTeamModel> = ArrayList()
            for (i in 0 until selection.size) {
                if (selection[i].status1) {
                    arrayTeam.add(selection[i])
                }
            }
            val intent = Intent(this, CreateSubscriptionDetailsActivity::class.java)
            intent.putExtra("model1", arrayTeam)
            setResult(Activity.RESULT_OK, intent)
            finish()
        }

        binding.getStartedBtn.setOnClickListener {
            val fragment = MenuFragmentAddStaff()
            fragment.show(supportFragmentManager, fragment.tag)
        }

    }

    private fun roundImage() {
        val curveRadius = 50F

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            binding.constraintBackground.outlineProvider = object : ViewOutlineProvider() {

                @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                override fun getOutline(view: View?, outline: Outline?) {
                    outline?.setRoundRect(
                        0,
                        -100,
                        view!!.width,
                        view.height,
                        curveRadius
                    )
                }
            }
            binding.constraintBackground.clipToOutline = true
        }

    }

    private fun initz() {
        toolbar = findViewById(R.id.toolbarManage)
        manageTeamRecycler = findViewById(R.id.manageTeamRecycler)
        manageDone = findViewById(R.id.manageDone)
        mreset = findViewById(R.id.reset)
        mfilters = findViewById(R.id.filters)
        filter_done = findViewById(R.id.filter_done)

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun clickToMenuFragments(
        position: Int,
        model: ManageTeamModel
    ) {

        if (position == 0) {
            val fragment1 = MenuFragment1(1, model, viewModel,
                binding.progressBar22 as LinearLayout
            )
            fragment1.show(supportFragmentManager, fragment1.tag)

        } else {
            val fragment1 = MenuFragment1(2, model, viewModel,binding.progressBar22 as LinearLayout)
            fragment1.show(supportFragmentManager, fragment1.tag)
        }


    }

    fun manage(view: View) {
        val fragment = MenuFragmentAddStaff()
        fragment.show(supportFragmentManager, fragment.tag)
    }


    class MenuFragmentAddStaff() : RoundedBottomSheetDialogFragment() {

        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
            val view = inflater.inflate(R.layout.invite_staff_nonstaff_sheet, container, false)
            val cancelDialog = view.findViewById<TextView>(R.id.cancelDialog)
            val takeContracter = view.findViewById<LinearLayout>(R.id.takeContracter)
            val layoutNonContracter = view.findViewById<LinearLayout>(R.id.layoutNonContracter)


            takeContracter.setOnClickListener {
                startActivityForResult(Intent(activity, AddIndividualInvite::class.java).putExtra("contractor","1"),1)
                dismiss()

            }


            layoutNonContracter.setOnClickListener {
                startActivityForResult(Intent(activity, AddIndividualInvite::class.java).putExtra("nonContractor", "2"),1)
                dismiss()
            }

            cancelDialog.setOnClickListener {
                dismiss()
            }

            return view
        }
    }


    class MenuFragment1(
        var i: Int,
        var model: ManageTeamModel,
        var viewModel: ManageTeamViewModel,
        var progressBar22: LinearLayout
    ) : RoundedBottomSheetDialogFragment() {

        lateinit var imageViewTop: ImageView
        lateinit var unlinkText1: TextView
        lateinit var unlinkText2: TextView
        lateinit var no: TextView
        lateinit var yes: TextView
        lateinit var nameRequestt: TextView
        lateinit var addressRequestt: TextView
        lateinit var logo: CircleImageView

        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val view = inflater.inflate(R.layout.link_unlink_sheet, container, false)
            imageViewTop = view.findViewById(R.id.imageViewTop)
            unlinkText1 = view.findViewById(R.id.unlinkText1)
            unlinkText2 = view.findViewById(R.id.unlinkText2)
            logo = view.findViewById(R.id.logo)
            no = view.findViewById(R.id.no)
            yes = view.findViewById(R.id.yes)
            nameRequestt = view.findViewById(R.id.nameRequestt)
            addressRequestt = view.findViewById(R.id.addressRequestt)
            roundImage()
            setData()

            if (i == 1) {
                unlinkText1.visibility = View.VISIBLE
                unlinkText2.visibility = View.GONE

            } else {
                unlinkText1.visibility = View.GONE
                unlinkText2.visibility = View.VISIBLE
            }
            yes.setOnClickListener {

                val businessId = Constant.getPrefs(activity!!).getString(Constant.last_associated_id, "")
                if (i == 1) {
                    viewModel.getlinkedUnlinked(
                        model.staff_id,
                        "2",
                        businessId,
                        ""
                    )
                    progressBar22.visibility = View.VISIBLE
                    dismiss()
                } else {
                    viewModel.getlinkedUnlinked(
                        model.staff_id,
                        "1",
                        businessId,
                        ""
                    )
                    progressBar22.visibility = View.VISIBLE
                    dismiss()
                }

            }

            no.setOnClickListener {
                dismiss()
            }

            return view

        }

        private fun setData() {
            val imagePath = Constant.getPrefs(activity!!).getString(Constant.dataImagePath, "")
            Glide.with(activity!!).load(imagePath + model.profile_image).placeholder(R.mipmap.circular_placeholder)
                .into(logo)
            nameRequestt.text = model.namee
            if (model.speciality.isEmpty()){
                addressRequestt.text = "No speciality added"
            }else{
                addressRequestt.text = model.speciality
            }

        }

        private fun roundImage() {
            val curveRadius = 50F

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                imageViewTop.outlineProvider = object : ViewOutlineProvider() {

                    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                    override fun getOutline(view: View?, outline: Outline?) {
                        outline?.setRoundRect(
                            0,
                            0,
                            view!!.width,
                            view.height + curveRadius.toInt(),
                            curveRadius
                        )
                    }
                }
                imageViewTop.clipToOutline = true
            }

        }
    }

    override fun nextFragments(i: Int) {
        when (i) {
            1 -> {
                inflateFragment(SortByFragment())
                mreset.visibility = View.GONE
                filterImgBack.visibility = View.VISIBLE
                filters.text = "Sort By"

                filterImgBack.setOnClickListener {
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"
                }

                filter_done.setOnClickListener {
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"
                }
            }


            2 -> {
                inflateFragment(CategoryProductsFragment())
                mreset.visibility = View.GONE
                filterImgBack.visibility = View.VISIBLE
                filters.text = "Category"

                filterImgBack.setOnClickListener {
                    Constant.hideKeyboard(this@ManageTeamActivity,binding.search)
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"

                }
                filter_done.setOnClickListener {
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"
                }
            }


            3 -> {
                inflateFragment(BrandsFilterFragment())
                mreset.visibility = View.GONE
                filterImgBack.visibility = View.VISIBLE
                filters.text = "Brands"

                filterImgBack.setOnClickListener {
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"

                }

                filter_done.setOnClickListener {
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"
                }
            }
            4 -> {
                inflateFragment(RedemptionFilterFragment())
                mreset.visibility = View.GONE
                filterImgBack.visibility = View.VISIBLE
                filters.text = "Redemption"

                filterImgBack.setOnClickListener {
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"

                }
                filter_done.setOnClickListener {
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"
                }
            }
        }

    }

    override fun onBackPressed() {
        if (drawerOpen == 1) {
            drawerOpen = 0
            binding.drawerLayout1.closeDrawer(GravityCompat.END)
        } else {
            super.onBackPressed()
        }
    }

    override fun onResume() {
        super.onResume()
        Constant.hideKeyboard(this, binding.search)
//        val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
//        viewModel.getTeamMembersList(auth, "0", "","1")
    }


    private fun inflateFragment(fragment: Fragment) {
        fragmentManager = supportFragmentManager
        this.fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.containerSide1, fragment)
        fragmentTransaction.commit()
    }

    override fun onDrawerStateChanged(newState: Int) {

    }

    override fun onDrawerSlide(drawerView: View, slideOffset: Float) {

    }

    override fun onDrawerClosed(drawerView: View) {
        drawerOpen = 0
    }

    override fun onDrawerOpened(drawerView: View) {
        drawerOpen = 1
        binding.drawerLayout1.openDrawer(GravityCompat.END)
        inflateFragment(FilterFragment(this))
        filterImgBack.visibility = View.GONE
        mfilters.text = "Filters"
        mreset.visibility = View.VISIBLE
    }

    override fun filterHeaderBackground(s: String) {
        if (s == "new") {
            filterHeader.setBackgroundDrawable(getDrawable(R.drawable.filter_header_select_category))


        } else {
            filterHeader.setBackgroundDrawable(getDrawable(R.drawable.filter_header))

        }
    }
}


