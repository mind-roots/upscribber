package com.upscribber.upscribberSeller.manageTeam

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.upscribberSeller.customerBottom.profile.CustomerProfileInfoActivity
import kotlinx.android.synthetic.main.manage_team_layout.view.*


@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class ManageTeamAdapter(
    var context: Context, var stringExtra: String
) :
    RecyclerView.Adapter<ManageTeamAdapter.MyViewHolder>() {

    private var items: ArrayList<ManageTeamModel> = arrayListOf()
    var listener = context as OpenMenuFragmet
    var listener1 = context as OpenMenuFragmet

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(context).inflate(R.layout.manage_team_layout, parent, false)
        return MyViewHolder(v)

    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = items[position]
        val imagePath = Constant.getPrefs(context)
            .getString(Constant.dataImagePath, "http://cloudart.com.au/projects/upscribbr/uploads/")
//
        Glide.with(context).load(imagePath + model.profile_image)
            .placeholder(R.mipmap.circular_placeholder)
            .into(holder.itemView.imageView)
        holder.itemView.memberName.text = model.namee
        if (model.speciality == ""){
            holder.itemView.customerNumber.text = "No speciality added"
//                Constant.getSplitPhoneNumber(model.contact_no, context)
        }else {
            holder.itemView.customerNumber.text = model.speciality
        }


        holder.itemView.constraint.setOnClickListener {
            if (stringExtra == "others1") {
                listener1.selection(position)
            } else {
                if (model.request_type == "1") {
//                    Toast.makeText(context, "You are not able to see this user profile yet!", Toast.LENGTH_SHORT).show()
                } else {
                    if (model.type == "2") {
                        context.startActivity(
                            Intent(context, CustomerProfileInfoActivity::class.java).putExtra("subProfile", "profile").putExtra("notSelected", "1").putExtra("contractor", "1").putExtra("modelCustomer", model))
                    } else {
                        context.startActivity(Intent(context,
                                CustomerProfileInfoActivity::class.java
                            ).putExtra("subProfile", "profile").putExtra(
                                "notSelected",
                                "2"
                            ).putExtra("staff", "2").putExtra("modelCustomer", model)
                        )
                    }
                }
            }

        }

        if (model.status1) {
            holder.itemView.tick.visibility = View.VISIBLE
            holder.itemView.constraint.setBackgroundResource(R.drawable.bg_customer_list_selection)
        } else {
            holder.itemView.tick.visibility = View.GONE
            holder.itemView.constraint.setBackgroundResource(R.drawable.bg_customer_list)
        }


        val isSeller = Constant.getSharedPrefs(context).getBoolean(Constant.IS_SELLER, false)
        val type = Constant.getPrefs(context).getString(Constant.type, "")

        if (stringExtra == "others1" || type == "2") {
            holder.itemView.linearLink.visibility = View.GONE
            holder.itemView.linearUnlink.visibility = View.GONE
        } else {
            if (model.business_id == "0") {
                holder.itemView.linearLink.visibility = View.GONE
                holder.itemView.linearUnlink.visibility = View.VISIBLE
                holder.itemView.tick.visibility = View.GONE
            }

            if (model.business_id != "0") {
                holder.itemView.linearLink.visibility = View.VISIBLE
                holder.itemView.linearUnlink.visibility = View.GONE
                holder.itemView.tick.visibility = View.GONE
            }

            if (model.request_type == "1") {
                holder.itemView.linearLink.visibility = View.GONE
                holder.itemView.linearUnlink.visibility = View.VISIBLE
                holder.itemView.linked.text = "Requested"
                holder.itemView.tick.visibility = View.GONE
            }
        }


        holder.itemView.linearLink.setOnClickListener {
            listener.clickToMenuFragments(0, model)
        }

        holder.itemView.linearUnlink.setOnClickListener {
            if (model.request_type != "1") {
                listener.clickToMenuFragments(1, model)
            }
        }

    }

    fun update(array: ArrayList<ManageTeamModel>) {
        this.items = array
        notifyDataSetChanged()
    }


    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    interface OpenMenuFragmet {
        fun clickToMenuFragments(
            position: Int,
            model: ManageTeamModel
        )

        fun selection(position: Int)
    }

}