package com.upscribber.upscribberSeller.manageTeam

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.upscribber.commonClasses.Constant
import com.upscribber.commonClasses.ModelStatusMsg
import com.upscribber.commonClasses.WebServicesCustomers
import com.upscribber.commonClasses.WebServicesMerchants
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import java.io.File

class ManageTeamRepository(var application: Application) {

    val mData = MutableLiveData<ArrayList<ManageTeamModel>>()
    val mDataFailure = MutableLiveData<ModelStatusMsg>()
    val mDataReLinked = MutableLiveData<ModelStatusMsg>()
    val mDataUnlinked = MutableLiveData<ModelStatusMsg>()
    val mDataContracters = MutableLiveData<ModelStatusMsg>()
    val mDataNonContracter = MutableLiveData<ModelStatusMsg>()
    val mDataTeamMemberAssign = MutableLiveData<ModelStatusMsg>()


    fun getTeamMembersList(
        auth: String,
        self: String,
        value: String,
        isInvited: String,
        search: String
    ) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.teamMember(auth,self,isInvited,search)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        val arrayTeamMembers = ArrayList<ManageTeamModel>()
                        if (status == "true") {
                            val data = json.optJSONArray("data")
//                            val team_members = data.optJSONArray("team_members")
                            for (i in 0 until data.length()) {
                                val teamData = data.getJSONObject(i)
                                val modelTeam = ManageTeamModel()
                                modelTeam.statusapi = status
                                modelTeam.msg = msg
                                modelTeam.business_id = teamData.optString("business_id")
                                modelTeam.request_type = teamData.optString("request_type")
                                modelTeam.contact_no = teamData.optString("contact_no")
                                modelTeam.user_id = teamData.optString("user_id")
                                modelTeam.staff_id = teamData.optString("staff_id")
                                modelTeam.type = teamData.optString("type")
                                modelTeam.speciality = teamData.optString("speciality")
                                modelTeam.profile_image = teamData.optString("profile_image")
                                modelTeam.namee = teamData.optString("name")

                                if (value == "others1"){
                                    val businessId = modelTeam.business_id
                                    if (businessId != "0"){
                                        arrayTeamMembers.add(modelTeam)
                                    }

                                }else{
                                    arrayTeamMembers.add(modelTeam)
                                }


                            }
                            mData.value = arrayTeamMembers
                        } else {
                            modelStatus.status = status
                            modelStatus.msg = msg
                            mDataFailure.value = modelStatus
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus
            }
        })
    }

    fun getmDataFailure(): LiveData<ModelStatusMsg> {
        return mDataFailure
    }


    fun getmUnlinked(): LiveData<ModelStatusMsg> {
        return mDataUnlinked
    }

    fun getmReLinked(): LiveData<ModelStatusMsg> {
        return mDataReLinked
    }


    fun getItems(): LiveData<ArrayList<ManageTeamModel>> {
        return mData
    }

    fun getlinkedUnlinked(
        staffId: String,
        type: String,
        businessId: String,
        businessName: String
    ) {
        val auth = Constant.getPrefs(application).getString(Constant.auth_code, "")
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.staffLink(auth, staffId, type,businessId)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        if (status == "true") {
                            modelStatus.businessName = businessName
                            modelStatus.msg = msg
                            modelStatus.status = status
                            if (type == "2") {
                                mDataReLinked.value = modelStatus
                            } else {
                                mDataUnlinked.value = modelStatus
                            }

                        } else {
                            modelStatus.msg = msg
                            modelStatus.status = status
                            mDataFailure.value = modelStatus
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus

            }
        })
    }

    fun getInviteOnPlatform(
        auth: String,
        type: String,
        contactNumber: String,
        name: String,
        email: String
    ) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.staffRequest(auth, type, contactNumber,name,email)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        if (status == "true") {
                            modelStatus.msg = msg
                            modelStatus.status = status
                            modelStatus.name = name
                            mDataContracters.value = modelStatus

                        } else {
                            modelStatus.msg = msg
                            modelStatus.status = status
                            mDataFailure.value = modelStatus
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus

            }
        })

    }

    fun mDataContracters(): LiveData<ModelStatusMsg> {
        return mDataContracters
    }

    fun completeInfoApi(
        file: File?,
        businessId: String,
        name: String,
        email: String,
        speciality: String,
        id: String,
        value1: String
    ) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()
        val auth = Constant.getPrefs(application).getString(Constant.auth_code, "")
        val service = retrofit.create(WebServicesMerchants::class.java)

        val file = file
        val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file)
        val body = MultipartBody.Part.createFormData("image", file!!.name, requestFile)

      val auth1 = RequestBody.create(MediaType.parse("multipart/form-data"), auth)
        val businessId = RequestBody.create(MediaType.parse("multipart/form-data"), businessId)// type
        val name = RequestBody.create(MediaType.parse("multipart/form-data"), name)// type
        val email = RequestBody.create(MediaType.parse("multipart/form-data"), email)// type
        val speciality = RequestBody.create(MediaType.parse("multipart/form-data"), speciality)// type
        val type = RequestBody.create(MediaType.parse("multipart/form-data"), "3")// type
        val step = RequestBody.create(MediaType.parse("multipart/form-data"), "1")// type
        val id = RequestBody.create(MediaType.parse("multipart/form-data"), id)// type

        val call: Call<ResponseBody> = service.merchantSignUpTeamMember(auth1,body,businessId,name,email,speciality,type,step, id)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        if (status == "true") {
                            modelStatus.msg = msg
                            modelStatus.status = status
                            modelStatus.type = value1
                            mDataNonContracter.value = modelStatus

                        } else {
                            modelStatus.msg = msg
                            modelStatus.status = status
                            mDataFailure.value = modelStatus
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }

            }


            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus

            }
        })


    }

    fun mDataNonContracter(): LiveData<ModelStatusMsg> {
        return mDataNonContracter
    }

    fun getTeamMemberAssign(auth: String, orderId: String, staffId: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.assignTeamMember(auth, orderId, staffId)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        if (status == "true") {
                            modelStatus.msg = msg
                            modelStatus.status = status
                            mDataTeamMemberAssign.value = modelStatus

                        } else {
                            modelStatus.msg = msg
                            modelStatus.status = status
                            mDataFailure.value = modelStatus
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus

            }
        })
    }

    fun getmTeamAssign(): LiveData<ModelStatusMsg> {
        return mDataTeamMemberAssign
    }

}