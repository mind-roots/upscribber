package com.upscribber.upscribberSeller.manageTeam

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by amrit on 11/4/19.
 */
class ManageTeamModel() : Parcelable {

    var user_id: String = ""
    var contact_no: String = ""
    var request_type: String = ""
    var namee: String = ""
    var business_id: String = ""
    var staff_id: String = ""
    var type: String = ""
    var speciality: String = ""
    var profile_image: String = ""
    var msg: String = ""
    var statusapi: String = ""
    var memberName: String = ""
    var status: Boolean = false
    var status1: Boolean = false

    fun speciality(): String {
        var spec = ""
        spec = if (speciality.isEmpty()) {
            "No speciality added"
        } else {
            speciality
        }
        return spec
    }

    constructor(parcel: Parcel) : this() {
        user_id = parcel.readString()
        contact_no = parcel.readString()
        request_type = parcel.readString()
        namee = parcel.readString()
        business_id = parcel.readString()
        staff_id = parcel.readString()
        type = parcel.readString()
        speciality = parcel.readString()
        profile_image = parcel.readString()
        msg = parcel.readString()
        statusapi = parcel.readString()
        memberName = parcel.readString()
        status = parcel.readByte() != 0.toByte()
        status1 = parcel.readByte() != 0.toByte()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(user_id)
        parcel.writeString(contact_no)
        parcel.writeString(request_type)
        parcel.writeString(namee)
        parcel.writeString(business_id)
        parcel.writeString(staff_id)
        parcel.writeString(type)
        parcel.writeString(speciality)
        parcel.writeString(profile_image)
        parcel.writeString(msg)
        parcel.writeString(statusapi)
        parcel.writeString(memberName)
        parcel.writeByte(if (status) 1 else 0)
        parcel.writeByte(if (status1) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ManageTeamModel> {
        override fun createFromParcel(parcel: Parcel): ManageTeamModel {
            return ManageTeamModel(parcel)
        }

        override fun newArray(size: Int): Array<ManageTeamModel?> {
            return arrayOfNulls(size)
        }
    }


}