package com.upscribber.upscribberSeller.manageTeam

import android.app.Application
import androidx.lifecycle.AndroidViewModel

class AddMemberViewModel(application: Application) : AndroidViewModel(application) {

    var addMemberRepository : AddMemberRepository = AddMemberRepository(application)
    fun getList(): List<AddMemberModel> {
        return  addMemberRepository.getAvatarImages()
    }

}