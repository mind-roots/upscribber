package com.upscribber.upscribberSeller.manageTeam

import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import android.util.Base64
import android.util.Log
import android.view.*
import android.widget.*
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.upscribber.commonClasses.Constant
import com.upscribber.R
import com.upscribber.databinding.ManageTeamAddMemberSheetBinding
import com.upscribber.commonClasses.RoundedBottomSheetDialogFragment
import com.upscribber.home.ModelHomeDataSubscriptions
import com.upscribber.notification.ModelNotificationCustomer
import com.upscribber.upscribberSeller.customerBottom.profile.activity.ActivityModel
import com.upscribber.upscribberSeller.individualProfile.addIndividual.InviteContracterConfirmation
import de.hdodenhof.circleimageview.CircleImageView
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS",
    "RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS"
)
class AddMemberActivity : AppCompatActivity() {

    lateinit var mBinding: ManageTeamAddMemberSheetBinding
    lateinit var mViewModel: ManageTeamViewModel
    val file: File? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.manage_team_add_member_sheet)
        mViewModel = ViewModelProviders.of(this)[ManageTeamViewModel::class.java]
        setToolbar()
        clickListeners()
        Constant.getPrefs(this).edit().remove("image").apply()
        apiInitiliazation()
        setData()
        observerInit()
    }

    private fun observerInit() {

        mViewModel.mDataNonContracter().observe(this, Observer {
            if (it.status == "true") {
                mBinding.progressBar24.visibility = View.GONE
                if (it.type == "1"){
                    startActivity(
                        Intent(this, InviteContracterConfirmation::class.java).putExtra("completeInfo", "0").putExtra(
                            "modelNotification", intent.getParcelableExtra<ModelNotificationCustomer>("model")
                        )
                    )
                }else if (it.type == "2"){
                    startActivity(
                        Intent(this, InviteContracterConfirmation::class.java).putExtra("completeInfo", "0").putExtra(
                            "modelActivity", intent.getParcelableExtra<ActivityModel>("modelActivity")
                        )
                    )
                }else if (it.type == "3"){
                    startActivity(
                        Intent(this, InviteContracterConfirmation::class.java).putExtra("completeInfo", "0").putExtra(
                            "home", intent.getParcelableExtra<ModelHomeDataSubscriptions>("home")
                        )
                    )
                }


                finish()
            } else {
                mBinding.progressBar24.visibility = View.GONE
            }
        })

        mViewModel.getStatus().observe(this, Observer {
            if (it.msg == "Invalid auth code") {
                Constant.commonAlert(this)
            } else {
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
            }
        })
    }

    fun decodeBase64(input: String): Bitmap {
        val decodedByte = Base64.decode(input, 0)
        return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.size)
    }

    private fun getImageUri22(inImage: Bitmap): File {
        val root: String = Environment.getExternalStorageDirectory().toString()
        val myDir = File("$root/req_images")
        myDir.mkdirs()
        val fname = "Image_profile.jpg"
        val file = File(myDir, fname)
        if (file.exists()) {
            file.delete()
        }

        try {
            val out = FileOutputStream(file)
            inImage.compress(Bitmap.CompressFormat.JPEG, 70, out)
            out.flush()
            out.close()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return file
    }

    private fun apiInitiliazation() {
        mBinding.buttonDone.setOnClickListener {

            val image = Constant.getPrefs(this).getString("image", "")
            if (image.isNotEmpty()) {
                val bitmap = decodeBase64(image)
                val file1 = getImageUri22(bitmap)

                if (image.isEmpty()) {
                    Toast.makeText(this, "Please select image first", Toast.LENGTH_SHORT).show()
                } else if (mBinding.editTextMemberName.text.isEmpty()) {
                    Toast.makeText(this, "Please enter your name", Toast.LENGTH_SHORT).show()
                } else if (mBinding.editTextEmail.text.isEmpty()) {
                    Toast.makeText(this, "Please enter your email", Toast.LENGTH_SHORT).show()
                } else if (!Constant.isValidEmailId(mBinding.editTextEmail.text.toString())) {
                    Toast.makeText(this, "Please Enter valid Email address", Toast.LENGTH_LONG)
                        .show()
                } else if (mBinding.editTextSpeciality.text.isEmpty()) {
                    Toast.makeText(this, "Please enter speciality", Toast.LENGTH_SHORT).show()
                } else {
                    mBinding.progressBar24.visibility = View.VISIBLE
                    if (intent.hasExtra("model")) {
                        val modelNotification =
                            intent.getParcelableExtra<ModelNotificationCustomer>("model")
                        mViewModel.getCompleteInfo(
                            file1,
                            modelNotification.business_id,
                            mBinding.editTextMemberName.text.toString(),
                            mBinding.editTextEmail.text.toString(),
                            mBinding.editTextSpeciality.text.toString(),
                            modelNotification.id, "1"
                        )
                    } else if (intent.hasExtra("modelActivity")) {
                        val modelNotification =
                            intent.getParcelableExtra<ActivityModel>("modelActivity")
                        mViewModel.getCompleteInfo(
                            file1,
                            modelNotification.model.business_id,
                            mBinding.editTextMemberName.text.toString(),
                            mBinding.editTextEmail.text.toString(),
                            mBinding.editTextSpeciality.text.toString(),
                            modelNotification.id, "2"
                        )
                    } else if (intent.hasExtra("home")) {
                        val modelHome =
                            intent.getParcelableExtra<ModelHomeDataSubscriptions>("home")
                        mViewModel.getCompleteInfo(
                            file1,
                            modelHome.business_id,
                            mBinding.editTextMemberName.text.toString(),
                            mBinding.editTextEmail.text.toString(),
                            mBinding.editTextSpeciality.text.toString(),
                            "", "3"
                        )


                    }

                }
            }else{
                Toast.makeText(this, "Please select image first", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun setData() {
        val arrayHomeProfile = Constant.getArrayListProfile(this, Constant.dataProfile)
        mBinding.editTextMemberName.setText(arrayHomeProfile.name)
        mBinding.editTextPhone.text = Constant.formatPhoneNumber(
            arrayHomeProfile.contact_no
        )
        mBinding.editTextEmail.setText(arrayHomeProfile.email)
    }


    @SuppressLint("ClickableViewAccessibility")
    private fun clickListeners() {
        mBinding.circleImageView.setOnClickListener {
            val fragment = MenuFragmentImage(mBinding.imageView, mViewModel, file)
            fragment.show(supportFragmentManager, fragment.tag)
        }

        mBinding.editTextSpeciality.setOnTouchListener { view, event ->
            view.parent.requestDisallowInterceptTouchEvent(true)
            if ((event.action and MotionEvent.ACTION_MASK) == MotionEvent.ACTION_UP) {
                view.parent.requestDisallowInterceptTouchEvent(false)
            }
            return@setOnTouchListener false
        }


        mBinding.editTextSpeciality.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                val result = "${s!!.length}/80"
                mBinding.tvSpecialityLimit.text = result

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

        })


    }

    class MenuFragmentImage(
        var imageView: CircleImageView,
        var mViewModel: ManageTeamViewModel,
        var file: File?
    ) : RoundedBottomSheetDialogFragment() {

        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val view = inflater.inflate(R.layout.change_profile_pic_layout, container, false)
            val isSeller = Constant.getSharedPrefs(context!!).getBoolean(Constant.IS_SELLER, false)
            val cancelDialog = view.findViewById<TextView>(R.id.cancelDialog)
            val takePicture = view.findViewById<LinearLayout>(R.id.takePicture)
            val galleryLayout = view.findViewById<LinearLayout>(R.id.galleryLayout)
            val textPicture = view.findViewById<TextView>(R.id.textPicture)
            val imgPicture = view.findViewById<ImageView>(R.id.imgPicture)
            val imgGallery = view.findViewById<ImageView>(R.id.imgGallery)
            val textGallery = view.findViewById<TextView>(R.id.textGallery)


            if (isSeller) {
                cancelDialog.setBackgroundResource(R.drawable.bg_seller_button_blue)
            } else {
                cancelDialog.setBackgroundResource(R.drawable.invite_button_background)
            }



            takePicture.setOnClickListener {
                val checkSelfPermission =
                    ContextCompat.checkSelfPermission(
                        this.activity!!,
                        android.Manifest.permission.CAMERA
                    )
                if (checkSelfPermission != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(
                        this.activity!!,
                        arrayOf(android.Manifest.permission.CAMERA),
                        2
                    )
                } else {
                    openCamera()
                }

            }

            galleryLayout.setOnClickListener {
                val checkSelfPermission =
                    ContextCompat.checkSelfPermission(
                        this.activity!!,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE
                    )
                if (checkSelfPermission != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(
                        this.activity!!,
                        arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE),
                        1
                    )
                } else {
                    openAlbum()
                }
            }

            cancelDialog.setOnClickListener {
                dismiss()
            }

            return view
        }

        private fun openAlbum() {
            val intent = Intent(Intent.ACTION_PICK)
            intent.type = "image/*"
            startActivityForResult(intent, 2)

        }

        private fun openCamera() {
            try {
                val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                startActivityForResult(intent, 1)

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }


        private fun getImageUri22(inImage: Bitmap): File {
            val root: String = Environment.getExternalStorageDirectory().toString()
            val myDir = File("$root/req_images")
            myDir.mkdirs()
            val fname = "Image_profile.jpg"
            val file = File(myDir, fname)
            if (file.exists()) {
                file.delete()
            }

            try {
                val out = FileOutputStream(file)
                inImage.compress(Bitmap.CompressFormat.JPEG, 70, out)
                out.flush()
                out.close()
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return file
        }


        override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
            super.onActivityResult(requestCode, resultCode, data)
            if (requestCode == 1) {
                try {
                    if (data != null) {
                        val bitmap = data.extras.get("data") as Bitmap
                        file = getImageUri22(bitmap)
                        imageView.setImageBitmap(bitmap)
                        val preferences = Constant.getPrefs(activity!!)
                        val editor = preferences.edit()
                        editor.putString("image", encodeTobase64(bitmap))
                        editor.apply()
                    }
                    dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            if (requestCode == 2) {
                try {

                    val selectedImage: Uri = data!!.data
                    file = File(getPath(selectedImage))
                    val bitmap =
                        MediaStore.Images.Media.getBitmap(activity!!.contentResolver, selectedImage)
                    try {
                        if (file != null) {
                            imageView.setImageBitmap(bitmap)

                            val preferences = Constant.getPrefs(activity!!)
                            val editor = preferences.edit()
                            editor.putString("image", encodeTobase64(bitmap))
                            editor.apply()
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                    dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

        }

        private fun encodeTobase64(image: Bitmap): String? {
            val immage = image
            val baos = ByteArrayOutputStream()
            immage.compress(Bitmap.CompressFormat.PNG, 100, baos)
            val b = baos.toByteArray()
            val imageEncoded = Base64.encodeToString(b, Base64.DEFAULT)

            Log.d("Image Log:", imageEncoded)
            return imageEncoded

        }


        fun getPath(uri: Uri): String {
            val projection = arrayOf(MediaStore.Images.Media.DATA)
            val cursor =
                activity!!.contentResolver.query(uri, projection, null, null, null) ?: return ""
            val column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
            cursor.moveToFirst()
            val s = cursor.getString(column_index)
            cursor.close()
            return s
        }
    }


    private fun setToolbar() {
        setSupportActionBar(mBinding.include11.toolbar)
        title = ""
        mBinding.include11.title.text = "Complete your Info"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)

    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }

        return super.onOptionsItemSelected(item)
    }
}
