package com.upscribber.upscribberSeller.manageTeam

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.upscribber.commonClasses.ModelStatusMsg
import java.io.File

class ManageTeamViewModel(application: Application) : AndroidViewModel(application) {

    var manageTeamRepository: ManageTeamRepository = ManageTeamRepository(application)

    fun getTeamMembersListtt(): LiveData<ArrayList<ManageTeamModel>> {
        return manageTeamRepository.getItems()
    }

    fun getTeamMembersList(
        auth: String,
        self: String,
        value: String,
        isInvited: String,
        search : String
    ) {
        manageTeamRepository.getTeamMembersList(auth,self,value,isInvited,search)
    }

    fun getStatus(): LiveData<ModelStatusMsg> {
        return manageTeamRepository.getmDataFailure()
    }


    fun getmUnlinked(): LiveData<ModelStatusMsg> {
        return manageTeamRepository.getmUnlinked()
    }

    fun getmRelinked(): LiveData<ModelStatusMsg> {
        return manageTeamRepository.getmReLinked()
    }


    fun getlinkedUnlinked(
        staffId: String,
        type: String,
        businessId: String,
        businessName: String
    ) {
        manageTeamRepository.getlinkedUnlinked(staffId, type,businessId,businessName)
    }

    fun getInviteOnPlatform(
        auth: String,
        type: String,
        contactNumber: String,
        name : String,
        email : String
    ) {
        manageTeamRepository.getInviteOnPlatform(auth, type,contactNumber,name , email)
    }

    fun mDataContracters(): LiveData<ModelStatusMsg> {
        return manageTeamRepository.mDataContracters()
    }


     fun mDataNonContracter(): LiveData<ModelStatusMsg> {
        return manageTeamRepository.mDataNonContracter()
    }

    fun getCompleteInfo(
        file: File?,
        businessId: String,
        name: String,
        email: String,
        speciality: String,
        id: String,
        value: String
    ) {
        manageTeamRepository.completeInfoApi(file,businessId,name,email,speciality, id,value)

    }

    fun getTeamMemberAssign(auth: String, orderId: String, staffId: String) {
        manageTeamRepository.getTeamMemberAssign(auth,orderId,staffId)

    }

  fun getTeamMemberAssign() : LiveData<ModelStatusMsg>{
       return manageTeamRepository.getmTeamAssign()

    }


}