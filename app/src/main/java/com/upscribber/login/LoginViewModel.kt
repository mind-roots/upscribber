package com.upscribber.login

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.upscribber.commonClasses.ModelStatusMsg

class LoginViewModel(application: Application) : AndroidViewModel(application){


    var loginRepository : LoginRepository = LoginRepository(application)

    fun getLogin(email: String, password: String, fireBaseToken: String) {
        loginRepository.getLogin(email,password,fireBaseToken)
    }

    fun getLoginData(): LiveData<LoginModel> {
        return loginRepository.getmData()
    }

    fun getStatuss(): LiveData<ModelStatusMsg> {
        return loginRepository.getmDataFailure()
    }

}