package com.upscribber.login

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.telephony.PhoneNumberFormattingTextWatcher
import android.text.Editable
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatCheckBox
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.upscribber.commonClasses.Constant
import com.upscribber.forgotPassword.ForgotPasswordActivity
import com.upscribber.dashboard.DashboardActivity
import com.upscribber.R
import com.upscribber.signUp.SignUpActivity
import com.upscribber.signUp.SignUpViewModel
import java.util.*

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class LoginActivity : AppCompatActivity() {

    lateinit var toolbar: Toolbar
    lateinit var login: TextView
    lateinit var checkbox: AppCompatCheckBox
    lateinit var et_password: EditText
    lateinit var etPhone: EditText
    lateinit var forgot_password: TextView
    lateinit var nextVerifyNumber: TextView
    lateinit var mViewModel: LoginViewModel
    lateinit var progressBar3: ConstraintLayout
    var flag = 0
    private var callbackManager: CallbackManager? = null
    private var url: String = ""
    lateinit var dialog: ProgressDialog
    var userEmail: String = ""
    var fbId: String = ""
    lateinit var mViewModel1: SignUpViewModel
    var userName: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        mViewModel = ViewModelProviders.of(this)[LoginViewModel::class.java]
        mViewModel1 = ViewModelProviders.of(this)[SignUpViewModel::class.java]
        FacebookSdk.sdkInitialize(FacebookSdk.getApplicationContext())
        callbackManager = CallbackManager.Factory.create()
        val accessToken = AccessToken.getCurrentAccessToken()
        dialog = ProgressDialog(this)
        initz()
        setToolbar()
        clickListeners()
        observersInit()
    }

    fun fbLogin(v: View) {
        LoginManager.getInstance()
            .logInWithReadPermissions(this, Arrays.asList("public_profile", "email"))
        LoginManager.getInstance()
            .registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
                override fun onSuccess(result: LoginResult?) {
                    val accessToken = result!!.accessToken
                    val request = GraphRequest.newMeRequest(accessToken)
                    { user, graphResponse ->
                        url =
                            "https://graph.facebook.com/${user.optString("id")}/picture?type=large&redirect=true&width=600&height=600"
                        mViewModel1.getLoginId(user.optString("id"))
                        userName = user.optString("name")
                        userEmail = user.optString("email")
                        fbId = user.optString("id")
                        dialog.setMessage("Loading...")
                        dialog.show()
                        dialog.setCancelable(false)
                    }.executeAsync()

                }


                override fun onCancel() {
                }

                override fun onError(error: FacebookException?) {
                    dialog.dismiss()
                    Log.i("FACEBOOK Error: ", error!!.message)
                    if (AccessToken.getCurrentAccessToken() != null) {
                        LoginManager.getInstance().logOut()
                    }
                }


            })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        callbackManager!!.onActivityResult(requestCode, resultCode, data)
    }

    private fun observersInit() {
        mViewModel.getLoginData().observe(this, Observer {
            progressBar3.visibility = View.GONE
            if (it.status1 == "true") {
                val intent = Intent(applicationContext, DashboardActivity::class.java)
                startActivity(intent)
            }
        })


        mViewModel1.getPhoneData().observe(this, Observer {
            if (it.status == "true") {
                val number = etPhone.text.toString().trim().replace(" ", "")
//                mViewModel.getOtp("+1"+number)
                mViewModel1.sendOtpWithMobileVerification("+1" + number, "3")
            } else {
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
            }
        })


        mViewModel1.getVerifyFacebook().observe(this, androidx.lifecycle.Observer {
            if (it.status == "true") {
                dialog.dismiss()
                startActivity(
                    Intent(this, SignUpActivity::class.java)
                        .putExtra("name", userName)
                        .putExtra("email", userEmail)
                        .putExtra("id", fbId)
                        .putExtra("login", "signup")
                )
            }
        })


        mViewModel1.getmDataOtpSigned().observe(this, Observer {
            progressBar3.visibility = View.GONE
            if (it.status == "true") {
                Toast.makeText(this, it.msg, Toast.LENGTH_LONG).show()
                startActivity(
                    Intent(this, LoginOtpActivity::class.java).putExtra(
                        "modelOtp",
                        it
                    ).putExtra("loginSigned", "loginSigned")
                )
            }
        })

        mViewModel.getStatuss().observe(this, androidx.lifecycle.Observer {
            progressBar3.visibility = View.GONE
            dialog.dismiss()
            if (it.msg == "User exists.") {
                startActivity(Intent(this, DashboardActivity::class.java))
            } else {
                Toast.makeText(this, it.msg, Toast.LENGTH_LONG).show()
            }
        })


        mViewModel1.getStatus().observe(this, androidx.lifecycle.Observer {
            progressBar3.visibility = View.GONE
            dialog.dismiss()
            if (it.msg == "User exists.") {
                startActivity(Intent(this, DashboardActivity::class.java))
            } else if (it.msg == "User does not exist") {
//                jyheruwcyuwcduyuhdc
                val number = etPhone.text.toString().trim().replace(" ", "")
                mViewModel1.getSignUpNumber1("+1" + number)
            } else {
                Toast.makeText(this, it.msg, Toast.LENGTH_LONG).show()
            }
        })

        mViewModel1.getPhoneData1().observe(this, Observer {
            if (it.status == "true") {
                val number = etPhone.text.toString().trim().replace(" ", "")
                mViewModel1.getOtp1("+1$number", "2")
            } else {
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
            }
        })

        mViewModel1.getmDataOtp().observe(this, Observer {
            progressBar3.visibility = View.GONE
            if (it.status == "true") {
                startActivity(Intent(this, LoginOtpActivity::class.java).putExtra("modelOtp", it))
                Toast.makeText(this, it.msg, Toast.LENGTH_LONG).show()

            }
//            if (it.status == "true") {
//                startActivity(Intent(this,LoginSignedUpActivity::class.java))
//                Toast.makeText(this, it.msg, Toast.LENGTH_LONG).show()
//
//            }
        })
    }

    private fun clickListeners() {
        val fireBaseToken: String = Constant.getPrefs(this).getString("deviceId", "")

        forgot_password.setOnClickListener {
            val intent = Intent(applicationContext, ForgotPasswordActivity::class.java)
            startActivity(intent)
        }

        login.setOnClickListener {
            Constant.hideKeyboard(this, etPhone)
            val phoneNumber = etPhone.text.toString().replace(" ", "")
            if (flag == 1) {
                progressBar3.visibility = View.VISIBLE
                mViewModel.getLogin(
                    "+1" + phoneNumber.trim(),
//                mViewModel.getLogin( Constant.getCountryDialCode(this) + phoneNumber.trim(),
                    et_password.text.toString().trim(),
                    fireBaseToken
                )
            } else {
                progressBar3.visibility = View.GONE
                Toast.makeText(
                    this,
                    "Please enter a valid 10 digit phone number",
                    Toast.LENGTH_LONG
                ).show()
            }

        }


        nextVerifyNumber.setOnClickListener {
            if (flag == 1) {
                Constant.hideKeyboard(this, etPhone)
                val phoneNumber = etPhone.text.toString().replace(" ", "")
                mViewModel1.sendOtpWithMobileVerification("+1" + phoneNumber, "3")
                progressBar3.visibility = View.VISIBLE
            } else {
                Toast.makeText(
                    this,
                    "Please enter 10 digit valid number to continue",
                    Toast.LENGTH_SHORT
                ).show()
            }
//            mViewModel1.getSignUpNumber(Constant.getCountryDialCode(this) + phoneNumber)
        }


        checkbox.setOnCheckedChangeListener { buttonView, isChecked ->
            if (!isChecked) {
                // show password
                et_password.transformationMethod = HideReturnsTransformationMethod.getInstance()
                checkbox.text = "Hide"
            } else {
                // hide password
                et_password.transformationMethod = PasswordTransformationMethod.getInstance()
                checkbox.text = "Show"
            }
        }

        etPhone.addTextChangedListener(object : PhoneNumberFormattingTextWatcher() {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                val text = etPhone.text.toString()
                val textLength = etPhone.text.length

                if (text.endsWith("-") || text.endsWith(" ") || text.endsWith(" "))
                    return
                if (textLength == 4) {
                    etPhone.setText(StringBuilder(text).insert(text.length - 1, " ").toString())
                    etPhone.setSelection(etPhone.text.length)
                }
                if (textLength == 8) {
                    etPhone.setText(StringBuilder(text).insert(text.length - 1, " ").toString())
                    etPhone.setSelection(etPhone.text.length)

                }

                if (textLength == 12) {
                    nextVerifyNumber.setBackgroundResource(R.drawable.otp_next)
                    flag = 1
                    Constant.hideKeyboard(this@LoginActivity, etPhone)
                } else {
                    nextVerifyNumber.setBackgroundResource(R.drawable.otp_next_opacity)
                    flag = 0
                }

            }

            override fun afterTextChanged(s: Editable) {

            }
        })


    }


    private fun setToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)

    }

    private fun initz() {
        toolbar = findViewById(R.id.toolbar_login)
        checkbox = findViewById(R.id.showPassword)
        nextVerifyNumber = findViewById(R.id.nextVerifyNumber)
        login = findViewById(R.id.goToSubscription)
        forgot_password = findViewById(R.id.forgot_password)
        et_password = findViewById(R.id.et_password)
        etPhone = findViewById(R.id.etPhone)
        progressBar3 = findViewById(R.id.progressBar3)
        etPhone.addTextChangedListener(PhoneNumberFormattingTextWatcher())
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }


}
