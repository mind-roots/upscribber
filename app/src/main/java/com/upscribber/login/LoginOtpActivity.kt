package com.upscribber.login

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.chaos.view.PinView
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.dashboard.DashboardActivity
import com.upscribber.signUp.SignUpViewModel
import com.upscribber.signUp.signUpOtp.ModelOtp


@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class LoginOtpActivity : AppCompatActivity() {

    lateinit var mPinView: PinView
    private lateinit var mPinOk: ImageView
    private lateinit var btn_resend: TextView
    private lateinit var tv_number: TextView
    private lateinit var tv_next: TextView
    private lateinit var tv_welcome_back: TextView
    private lateinit var back_to_phone: ImageView
    private lateinit var progressBar: ConstraintLayout
    var flag = 0
    lateinit var mViewModel: SignUpViewModel
    lateinit var modelOtp: ModelOtp

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_sign_up_otp)
        mViewModel = ViewModelProviders.of(this)[SignUpViewModel::class.java]
        modelOtp = intent.getParcelableExtra("modelOtp")
        initz()
        clickListeners()
        observerInit()
    }

    private fun observerInit() {
        mViewModel.getmDataOtp().observe(this, Observer {
            progressBar.visibility = View.GONE
            if (it.status == "true") {
                modelOtp.otp = it.otp
            }
        })

        mViewModel.getStatus().observe(this, Observer {
            progressBar.visibility = View.GONE
            if (it.msg.isNotEmpty()) {
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
            }

        })

        mViewModel.getSignedUp().observe(this, androidx.lifecycle.Observer {
            if (it.status == "true") {
                progressBar.visibility = View.GONE
                val intent = Intent(this, DashboardActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
            } else {
                Toast.makeText(this, it.msg, Toast.LENGTH_LONG).show()
            }
        })

    }

    private fun initz() {
        mPinView = findViewById(R.id.secondPinView)
        mPinOk = findViewById(R.id.pin_ok)
        btn_resend = findViewById(R.id.btn_resend)
        back_to_phone = findViewById(R.id.back_to_phone)
        tv_number = findViewById(R.id.tv_number)
        progressBar = findViewById(R.id.progressBar)
        tv_welcome_back = findViewById(R.id.tv_welcome_back)
        tv_next = findViewById(R.id.tv_next)
        tv_welcome_back.text = "Code"
        tv_number.text = Constant.formatPhoneNumber(modelOtp.contact)

    }

    private fun clickListeners() {
        btn_resend.setOnClickListener {
            mPinView.setText("")
            mViewModel.sendOtpWithMobileVerification(modelOtp.contact, "3")
            progressBar.visibility = View.VISIBLE
//            val mDialogView =
//                LayoutInflater.from(this).inflate(R.layout.alert_resend_code, null)
//            val mBuilder = AlertDialog.Builder(this)
//                .setView(mDialogView)
//            val mAlertDialog = mBuilder.show()
//            mAlertDialog.setCancelable(false)
//            mAlertDialog.window.setBackgroundDrawableResource(R.drawable.bg_alert1)
//            mAlertDialog.textDone.setOnClickListener {
//                mAlertDialog.dismiss()
//            }
        }

        tv_next.setOnClickListener {
            if (flag == 0){
                Toast.makeText(this,"Please enter the correct otp sent to your mobile number.",Toast.LENGTH_SHORT).show()
            }else {
                if (intent.hasExtra("loginSigned")){
                    startActivity(Intent(this,LoginSignedUpActivity::class.java).putExtra("modelOtp",intent.getParcelableExtra<ModelOtp>("modelOtp")))
                }else {
                    val fireBaseToken: String = Constant.getPrefs(this).getString("deviceId", "")
                    mViewModel.getAuthCode(modelOtp.contact, fireBaseToken, "android")
                    progressBar.visibility = View.VISIBLE
                }

            }
        }

        back_to_phone.setOnClickListener {
            finish()
        }


        mPinView.setTextColor(
            ResourcesCompat.getColor(resources, R.color.line_color, this.theme)
        )
        mPinView.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (s.length == 4) {
                    mViewModel.getOtpVerified(modelOtp.contact, modelOtp.otp)
                    Constant.hideKeyboard(this@LoginOtpActivity, mPinView)
                    mPinOk.visibility = View.VISIBLE
                    if (mPinView.text.toString() == modelOtp.otp) {
                        flag = 1
                        mPinOk.setImageResource(R.drawable.ic_done_black_24dp)
                    } else {
                        mPinOk.setImageResource(R.drawable.ic_cross_pink)
                    }
                } else {
                    mPinOk.visibility = View.GONE
                    flag = 0
                }
            }

            override fun afterTextChanged(s: Editable) {

            }
        })
        mPinView.setHideLineWhenFilled(false)

    }


}
