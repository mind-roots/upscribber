package com.upscribber.login

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.upscribber.commonClasses.Constant
import com.upscribber.commonClasses.ModelStatusMsg
import com.upscribber.commonClasses.WebServicesCustomers
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class LoginRepository(var application: Application) {
    val mData = MutableLiveData<LoginModel>()
    val mDataFailure = MutableLiveData<ModelStatusMsg>()

    fun getLogin(email: String, password: String, fireBaseToken: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.Login(email,password,fireBaseToken,"android")
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status1 = json.optString("status")
                        val msg = json.optString("msg")
                        val modelLogin = LoginModel()
                        val modelStatus = ModelStatusMsg()
                        if (status1 == "true") {
                            val data = json.getJSONObject("data").optJSONObject("user_details")
                            modelLogin.id = data.optString("id")
                            modelLogin.email = data.optString("email")
                            modelLogin.password = data.optString("password")
                            modelLogin.name = data.optString("name")
                            modelLogin.contact_no = data.optString("contact_no")
                            modelLogin.birthdate = data.optString("birthdate")
                            modelLogin.reg_type = data.optString("reg_type")
                            modelLogin.gender = data.optString("gender")
                            modelLogin.profile_image = data.optString("profile_image")
                            modelLogin.status = data.optString("status")
                            modelLogin.address = data.optString("address")
                            modelLogin.created_at = data.optString("created_at")
                            modelLogin.updated_at = data.optString("updated_at")
                            modelLogin.invite_code = data.optString("invite_code")
                            modelLogin.customer_wallet = data.optString("customer_wallet")
                            modelLogin.business_wallet = data.optString("business_wallet")
                            modelLogin.refer_by = data.optString("refer_by")
                            modelLogin.refer_type = data.optString("refer_type")
                            modelLogin.stripe_customer_id = data.optString("stripe_customer_id")
                            modelLogin.stripe_account_id = data.optString("stripe_account_id")
                            modelLogin.sales = data.optString("sales")
                            modelLogin.customers = data.optString("customers")
                            modelLogin.auth_code = data.optString("auth_code")
                            modelLogin.status1 = status1
                            modelStatus.status = status1
                            modelStatus.msg = msg


                            val editor = Constant.getPrefs(application).edit()
                            editor.putString(Constant.auth_code, modelLogin.auth_code)
                            editor.apply()
                            mData.value = modelLogin

                        } else {
                            modelLogin.status1 = status1
                            modelStatus.status = status1
                            modelStatus.msg = msg
                            mDataFailure.value = modelStatus
                        }



                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus

            }

        })

    }

    fun getmDataFailure(): LiveData<ModelStatusMsg> {
        return mDataFailure
    }

    fun getmData(): LiveData<LoginModel> {
        return mData
    }
}