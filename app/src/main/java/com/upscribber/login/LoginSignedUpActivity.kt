package com.upscribber.login

import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProviders
import com.instabug.library.ui.custom.CircularImageView
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.commonClasses.RoundedBottomSheetDialogFragment
import com.upscribber.dashboard.DashboardActivity
import com.upscribber.signUp.SignUpViewModel
import com.upscribber.signUp.signUpOtp.ModelOtp
import com.upscribber.signUp.signupInfo.SignUpFragment
import kotlinx.android.synthetic.main.fragment_sign_up.*
import java.io.File

class LoginSignedUpActivity : AppCompatActivity() {

    lateinit var mViewModel: SignUpViewModel

    companion object {
        var file: File? = null
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_sign_up)
        mViewModel = ViewModelProviders.of(this)[SignUpViewModel::class.java]
        clickListeners()
        observerInit()
    }

    private fun observerInit() {

        mViewModel.getEmail().observe(this, androidx.lifecycle.Observer {
            if (it.status == "true") {
                val fireBaseToken: String = Constant.getPrefs(this).getString("deviceId", "")
                val android_id = Settings.Secure.getString(contentResolver, Settings.Secure.ANDROID_ID)
                val modelOtp = intent.getParcelableExtra<ModelOtp>("modelOtp")
                val phoneNumber = modelOtp.contact.replace(" ","").replace("+91","").replace("+1","").replace("-","")
                mViewModel.getSignUp(
                    "+1$phoneNumber",
                    it.email,
                    "",
                    "1",
                    android_id,
                    fireBaseToken,
                    "",
                    "",
                    "",
                    et_name.text.toString().trim(), "", file
                )
            } else {
                Toast.makeText(this, it.msg, Toast.LENGTH_LONG).show()
            }
        })

        mViewModel.getSignedUp().observe(this, androidx.lifecycle.Observer {
            if (it.status == "true") {
                progressBarEmail.visibility = View.GONE
                val intent = Intent(this, DashboardActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
            } else {
                Toast.makeText(this, it.msg, Toast.LENGTH_LONG).show()
            }
        })

        mViewModel.getStatus().observe(this, androidx.lifecycle.Observer {
            if (it.msg.isNotEmpty()) {
                progressBarEmail.visibility = View.GONE
                Toast.makeText(this, it.msg, Toast.LENGTH_LONG).show()
            }
        })
    }

    private fun clickListeners() {
        back_to_otp.setOnClickListener {
            finish()
        }

        profilePictureClick.setOnClickListener {
            val fragment = MenuFragment(profilePictureClick,plusbtn)
            fragment.show(supportFragmentManager, fragment.tag)
        }

        goToSubscription.setOnClickListener {
            Constant.hideKeyboard(this, et_name)
            if (et_name.text.toString().trim().isEmpty() && et_email.text.toString().trim().isEmpty()) {
                Toast.makeText(
                    this,
                    "Please Enter the following information",
                    Toast.LENGTH_LONG
                )
                    .show()
            } else if (et_name.text.toString().trim().isEmpty()) {
                Toast.makeText(this, "Please enter your name to continue", Toast.LENGTH_LONG).show()
            } else if (et_email.text.toString().trim().isEmpty()) {
                Toast.makeText(this, "Please enter your email address to continue", Toast.LENGTH_LONG).show()
            } else if (!checkBox2.isChecked) {
                Toast.makeText(
                    this,
                    "Please select that you agree to Upscribbr's Terms & Conditions and Privacy Policy to continue",
                    Toast.LENGTH_LONG
                ).show()
            } else if (!Constant.isValidEmailId(et_email.text.toString().trim())) {
                Toast.makeText(this, "Please enter your valid email address to continue", Toast.LENGTH_LONG)
                    .show()
            } else {
                progressBarEmail.visibility = View.VISIBLE
                mViewModel.getEmailId(et_email.text.toString())
            }
        }

    }

    class MenuFragment(
        var imageView: ImageView,
        var plusbtn: ImageView
    ) : RoundedBottomSheetDialogFragment() {
        private lateinit var imageUri: Uri
        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val view = inflater.inflate(R.layout.change_profile_pic_layout, container, false)
            val isSeller = Constant.getSharedPrefs(context!!).getBoolean(Constant.IS_SELLER, false)
            val cancelDialog = view.findViewById<TextView>(R.id.cancelDialog)
            val takePicture = view.findViewById<LinearLayout>(R.id.takePicture)
            val galleryLayout = view.findViewById<LinearLayout>(R.id.galleryLayout)

            if (isSeller) {
                cancelDialog.setBackgroundResource(R.drawable.bg_seller_button_blue)
            } else {
                cancelDialog.setBackgroundResource(R.drawable.invite_button_background)
            }

            takePicture.setOnClickListener {
                val checkSelfPermission =
                    ContextCompat.checkSelfPermission(
                        this.activity!!,
                        android.Manifest.permission.CAMERA
                    )
                if (checkSelfPermission != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(
                        this.activity!!,
                        arrayOf(android.Manifest.permission.CAMERA),
                        2
                    )
                } else {
                    openCamera()
                }

            }

            galleryLayout.setOnClickListener {
                val checkSelfPermission =
                    ContextCompat.checkSelfPermission(
                        this.activity!!,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE
                    )
                if (checkSelfPermission != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(
                        this.activity!!,
                        arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE),
                        1
                    )
                } else {
                    openAlbum()
                }
            }

            cancelDialog.setOnClickListener {
                dismiss()
            }

            return view
        }

        private fun openAlbum() {
            val intent = Intent(Intent.ACTION_PICK)
            intent.type = "image/*"
            startActivityForResult(intent, 2)

        }

        private fun openCamera() {
            try {
                val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                startActivityForResult(intent, 1)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }


        override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
            super.onActivityResult(requestCode, resultCode, data)
            if (requestCode == 1) {
                try {
                    if (data != null) {
                        val bitmap = data.extras.get("data") as Bitmap
                        file = Constant.getImageUri(bitmap)
//                        imageView.setBackgroundResource(0)
                        plusbtn.visibility = View.GONE
                        imageView.setImageBitmap(bitmap)

                    }
                    dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            if (requestCode == 2) {
                try {
                    val selectedImage: Uri = data!!.data
                    file = File(getPath(selectedImage))
                    val bitmap =
                        MediaStore.Images.Media.getBitmap(activity!!.contentResolver, selectedImage)
                    try {
                        if (file != null) {
                            plusbtn.visibility = View.GONE
//                            imageView.setBackgroundResource(0)
                            imageView.setImageBitmap(bitmap)


                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                    dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

        }

        fun getPath(uri: Uri): String {
            val projection = arrayOf(MediaStore.Images.Media.DATA)
            val cursor =
                activity!!.contentResolver.query(uri, projection, null, null, null) ?: return ""
            val column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
            cursor.moveToFirst()
            val s = cursor.getString(column_index)
            cursor.close()
            return s
        }
    }

    private fun getfile(file1: File) {
        file = file1
    }
}
