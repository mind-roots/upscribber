package com.upscribber.login

class LoginModel {

    var password: String = ""
    var email: String = ""
    var id: String = ""
    var name: String = ""
    var contact_no: String = ""
    var birthdate: String = ""
    var reg_type: String = ""
    var gender: String = ""
    var profile_image: String = ""
    var status: String = ""
    var address: String = ""
    var created_at: String = ""
    var updated_at: String = ""
    var invite_code: String = ""
    var customer_wallet: String = ""
    var business_wallet: String = ""
    var refer_by: String = ""
    var refer_type: String = ""
    var stripe_customer_id: String = ""
    var stripe_account_id: String = ""
    var sales: String = ""
    var customers: String = ""
    var auth_code: String = ""
    var status1: String = ""
}

