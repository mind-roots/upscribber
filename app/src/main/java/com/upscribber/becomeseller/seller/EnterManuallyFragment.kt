package com.upscribber.becomeseller.seller

import android.app.Activity
import android.content.Intent
import android.location.Geocoder
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.widget.Autocomplete
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.filters.LocationModel
import com.upscribber.profile.ChangeLocationSetingsActivity
import kotlinx.android.synthetic.main.activity_enter_manually.*
import kotlin.collections.ArrayList


@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class EnterManuallyFragment : Fragment() {

    lateinit var manuallyDone: TextView
    lateinit var eTStreetAddress: EditText
    lateinit var eTState: TextView
    lateinit var eTCity: EditText
    lateinit var eTZip: EditText
    lateinit var searchAddress: TextView
    var arrayStates: ArrayList<ModelStates> = ArrayList()
    lateinit var mViewModel: BecomeSellerViewModel
    val locationModel = LocationModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.activity_enter_manually, container, false)
        mViewModel = ViewModelProviders.of(this)[BecomeSellerViewModel::class.java]
        initz(view)
        Places.initialize(activity!!, resources.getString(R.string.google_maps_key))
        clickEvents()
        val auth = Constant.getPrefs(activity!!).getString(Constant.auth_code, "")
        mViewModel.getStateList(auth)
        observerInit()
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        try {
            if (!arguments!!.isEmpty) {
                if (activity!!.intent.hasExtra("modelLocation")){
                    val model = arguments!!.getParcelable<LocationModel>("modelLocation")
                    eTStreetAddress.setText(model.street)
                    eTState.text = model.state
                    eTCity.setText(model.city)
                    eTZip.setText(model.zipcode)
                    locationModel.latitude = model.latitude
                    locationModel.longitude = model.longitude
                }else if (activity!!.intent.hasExtra("modelLocationn")){
                    val model = arguments!!.getParcelable<LocationModel>("modelLocationn")
                    eTStreetAddress.setText(model.billingStreet)
                    eTState.text = model.billingState
                    eTCity.setText(model.billingCity)
                    eTZip.setText(model.billingZip)
                    locationModel.billingLat = model.billingLat
                    locationModel.billingLong = model.billingLong
                }else if (activity!!.intent.hasExtra("checkoutMerchantBilling")){
                    val model = arguments!!.getParcelable<ModelAddress>("checkoutMerchantBilling")
                    eTStreetAddress.setText(model.billingStreet)
                    eTState.text = model.billingState
                    eTCity.setText(model.billingCity)
                    eTZip.setText(model.billingZip)
                    eTSuiteNumber.setText(model.billingSuite)
                }else if (activity!!.intent.hasExtra("checkoutBilling")){
                    val model = arguments!!.getParcelable<ModelAddress>("checkoutBilling")
                    eTStreetAddress.setText(model.billingStreet)
                    eTState.text = model.billingState
                    eTCity.setText(model.billingCity)
                    eTZip.setText(model.billingZip)
                    eTSuiteNumber.setText(model.billingSuite)
                }else if (activity!!.intent.hasExtra("checkout")){
                    val model = arguments!!.getParcelable<ModelAddress>("checkout")
                    eTStreetAddress.setText(model.streetAddress)
                    eTState.text = model.state
                    eTCity.setText(model.city)
                    eTZip.setText(model.zipCode)
                    eTSuiteNumber.setText(model.suiteNumber)
                }else {
                    val model = arguments!!.getParcelable<ModelAddress>("model")
                    eTStreetAddress.setText(model.streetAddress)
                    eTState.text = model.state
                    eTCity.setText(model.city)
                    eTZip.setText(model.zipCode)
                    eTSuiteNumber.setText(model.suiteNumber)
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun observerInit() {
        mViewModel.getmDataStates().observe(this, Observer {
            if (it.size > 0) {
                arrayStates = it
            }
        })

        mViewModel.getStatus().observe(this, Observer {
            if (it.msg == "Invalid auth code") {
                Constant.commonAlert(activity!!)
            } else {
                Toast.makeText(activity, it.msg, Toast.LENGTH_SHORT).show()
            }
        })

        mViewModel.getmDataUpdateAddress().observe(this, Observer {
            progressBar.visibility = View.GONE
            val intent = Intent()
            intent.putExtra("address", it)
            activity!!.setResult(1, intent)
            activity!!.finish()
        })

    }

    private fun initz(view: View) {
        manuallyDone = view.findViewById(R.id.manuallyDone)
        eTStreetAddress = view.findViewById(R.id.eTStreetAddress)
        eTState = view.findViewById(R.id.eTState)
        eTCity = view.findViewById(R.id.eTCity)
        eTZip = view.findViewById(R.id.eTZip)
        searchAddress = view.findViewById(R.id.searchAddress)
        val isSeller = Constant.getSharedPrefs(context!!).getBoolean(Constant.IS_SELLER, false)
        if(isSeller){
            manuallyDone.setBackgroundResource(R.drawable.bg_seller_button_blue)
        }else{
            manuallyDone.setBackgroundResource(R.drawable.invite_button_background)
        }


    }

    private fun getStatesList(): Array<CharSequence?> {
        val myArray = arrayOfNulls<CharSequence>(arrayStates.size)
        for (i in 0 until arrayStates.size) {
            myArray[i] =
                arrayStates[i].state_name // Whichever string you wanna store here from custom object
        }

        return myArray
    }


    private fun clickEvents() {

        searchAddress.setOnClickListener {
            val intent = Intent(activity, ChangeLocationSetingsActivity::class.java)
            startActivityForResult(intent, 102)

        }

        manuallyDone.setOnClickListener {
            val auth = Constant.getPrefs(activity!!).getString(Constant.auth_code, "")
            if (activity!!.intent.hasExtra("addCard")) {
                when {
                    eTStreetAddress.text.isEmpty() -> {
                        Toast.makeText(activity, "Please enter street address", Toast.LENGTH_SHORT)
                            .show()
                    }
                    eTCity.text.isEmpty() -> {
                        Toast.makeText(activity, "Please enter city name", Toast.LENGTH_SHORT)
                            .show()
                    }
                    eTState.text.isEmpty() -> {
                        Toast.makeText(activity, "Please enter state name", Toast.LENGTH_SHORT)
                            .show()
                    }
                    eTZip.text.isEmpty() -> {
                        Toast.makeText(activity, "Please enter Zip number", Toast.LENGTH_SHORT)
                            .show()
                    }
                    else -> {
                        progressBar.visibility = View.VISIBLE
                        mViewModel.updateBillingAddress(
                            auth,
                            eTStreetAddress.text.toString().trim(),
                            eTCity.text.toString().trim(),
                            eTState.text.toString().trim(),
                            eTZip.text.toString().trim(),
                            eTSuiteNumber.text.toString().trim(), "0"
                        )
                    }
                }
            } else {
//                if (eTSuiteNumber.text.isEmpty()) {
//                    Toast.makeText(activity, "Please enter suite number", Toast.LENGTH_SHORT).show()
//                } else {
                if (activity!!.intent.hasExtra("modelLocation")) {
                    if (locationModel.latitude == "" || locationModel.longitude == "" || locationModel.latitude == "1.0" || locationModel.longitude == "1.0") {
                        Toast.makeText(
                            activity,
                            "Please select your business address location from map as well in order to continue",
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        val model = ModelAddress()
                        model.streetAddress = eTStreetAddress.text.toString()
                        model.suiteNumber = eTSuiteNumber.text.toString()
                        model.city = eTCity.text.toString()
                        model.state = eTState.text.toString()
                        model.zipCode = eTZip.text.toString()
                        model.latitude = locationModel.latitude
                        model.longitude = locationModel.longitude

                        val intent = Intent()
                        intent.putExtra("address", model)
                        activity!!.setResult(20, intent)
                        activity!!.finish()
                    }


                }else if (activity!!.intent.hasExtra("modelLocationn")) {
                    if (locationModel.billingLat == "" || locationModel.billingLong == "" || locationModel.billingLat == "1.0" || locationModel.billingLong == "1.0") {
                        Toast.makeText(
                            activity,
                            "Please select your business address location from map as well in order to continue",
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        val model = ModelAddress()
                        model.billingStreet = eTStreetAddress.text.toString()
                        model.billingSuite = eTSuiteNumber.text.toString()
                        model.billingCity = eTCity.text.toString()
                        model.billingState = eTState.text.toString()
                        model.billingZip = eTZip.text.toString()
                        model.billingLat = locationModel.billingLat
                        model.billingLong = locationModel.billingLong

                        val intent = Intent()
                        intent.putExtra("address", model)
                        activity!!.setResult(21, intent)
                        activity!!.finish()
                    }


                }else if (activity!!.intent.hasExtra("checkout")){
                    val model = ModelAddress()
                    model.streetAddress = eTStreetAddress.text.toString()
                    model.suiteNumber = eTSuiteNumber.text.toString()
                    model.city = eTCity.text.toString()
                    model.state = eTState.text.toString()
                    model.zipCode = eTZip.text.toString()
                    model.latitude = locationModel.latitude
                    model.longitude = locationModel.longitude
                    progressBar.visibility = View.VISIBLE
                    mViewModel.updateBillingAddress(
                        auth,
                        eTStreetAddress.text.toString().trim(),
                        eTCity.text.toString().trim(),
                        eTState.text.toString().trim(),
                        eTZip.text.toString().trim(),
                        eTSuiteNumber.text.toString().trim(), "1"
                    )
                }else if (activity!!.intent.hasExtra("checkoutBilling")){
                    val model = ModelAddress()
                    model.billingStreet = eTStreetAddress.text.toString()
                    model.billingSuite = eTSuiteNumber.text.toString()
                    model.billingCity = eTCity.text.toString()
                    model.billingState = eTState.text.toString()
                    model.billingZip = eTZip.text.toString()
                    model.latitude = locationModel.latitude
                    model.longitude = locationModel.longitude
                    progressBar.visibility = View.VISIBLE
                    mViewModel.updateBillingAddressCheck(
                        auth,
                        eTStreetAddress.text.toString().trim(),
                        eTCity.text.toString().trim(),
                        eTState.text.toString().trim(),
                        eTZip.text.toString().trim(),
                        eTSuiteNumber.text.toString().trim(), "0"
                    )
                }else if (activity!!.intent.hasExtra("checkoutMerchantBilling")){
                    val model = ModelAddress()
                    model.billingStreet = eTStreetAddress.text.toString()
                    model.billingSuite = eTSuiteNumber.text.toString()
                    model.billingCity = eTCity.text.toString()
                    model.billingState = eTState.text.toString()
                    model.billingZip = eTZip.text.toString()
                    model.latitude = locationModel.latitude
                    model.longitude = locationModel.longitude
                    val intent = Intent()
                    intent.putExtra("address", model)
                    activity!!.setResult(1, intent)
                    activity!!.finish()
                }else{
                    val model = ModelAddress()
                    model.streetAddress = eTStreetAddress.text.toString()
                    model.suiteNumber = eTSuiteNumber.text.toString()
                    model.city = eTCity.text.toString()
                    model.state = eTState.text.toString()
                    model.zipCode = eTZip.text.toString()
                    model.billingStreet = eTStreetAddress.text.toString()
                    model.billingSuite = eTSuiteNumber.text.toString()
                    model.billingCity = eTCity.text.toString()
                    model.billingState = eTState.text.toString()
                    model.billingZip = eTZip.text.toString()
                    model.latitude = locationModel.latitude
                    model.longitude = locationModel.longitude

                    val intent = Intent()
                    intent.putExtra("address", model)
                    activity!!.setResult(1, intent)
                    activity!!.finish()
                }
            }


        }

        eTState.setOnClickListener {
            Constant.hideKeyboard(activity!!, eTState)

            val choices = getStatesList()

            val mBuilder = AlertDialog.Builder(activity!!)

            mBuilder.setSingleChoiceItems(choices, -1) { dialogInterface, i ->
                eTState.text = choices[i]
                dialogInterface.dismiss()

            }
            val mDialog = mBuilder.create()
            mDialog.show()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 102 && data != null) {
            val data1 = data.getParcelableExtra<LocationModel>("locationModel")
            eTStreetAddress.setText(data1.address)
            eTCity.setText(data1.city)
            eTState.text = data1.state
            eTZip.setText(data1.zipcode)
            locationModel.latitude = data.getStringExtra("latitude").toString()
            locationModel.longitude = data.getStringExtra("longitude").toString()
        }


        if (requestCode == 1) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    val place = Autocomplete.getPlaceFromIntent(data!!)
                    val latLng = place.latLng
                    val geocoder = Geocoder(activity)
                    val addresses =
                        latLng!!.latitude.let { geocoder.getFromLocation(it, latLng.longitude, 1) }
                    try {
                        if (null != addresses && addresses.isNotEmpty()) {
                            locationModel.address = addresses[0].getAddressLine(0)
                            locationModel.city = addresses[0].locality
                            locationModel.state = addresses[0].adminArea
                            locationModel.street = addresses[0].thoroughfare
                            locationModel.zipcode = addresses[0].postalCode
                            locationModel.latitude = latLng.latitude.toString()
                            locationModel.longitude = latLng.longitude.toString()
                            locationModel.billingZip = addresses[0].postalCode
                            locationModel.billingCity = addresses[0].locality
                            locationModel.billingState = addresses[0].adminArea
                            locationModel.billingStreet = addresses[0].thoroughfare

                            eTStreetAddress.setText(locationModel.address)
                            eTCity.setText(locationModel.city)
                            eTState.text = locationModel.state
                            eTZip.setText(locationModel.zipcode)

                        }

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    val intent = Intent(activity, ChangeLocationSetingsActivity::class.java)
                    intent.putExtra("latitude", place.latLng?.latitude.toString())
                    intent.putExtra("longitude", place.latLng?.longitude.toString())


                    startActivityForResult(intent, 102)
                }
            }
        }

    }


}