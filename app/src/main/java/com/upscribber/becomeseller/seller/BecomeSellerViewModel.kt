package com.upscribber.becomeseller.seller

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.upscribber.commonClasses.ModelStatusMsg

class BecomeSellerViewModel(application: Application) : AndroidViewModel(application) {

    var repositoryBecomeSeller: BecomeSellerRepository =
        BecomeSellerRepository(application)

    fun getCategories(auth: String) {
        repositoryBecomeSeller.getAllCategories(auth)
    }

    fun getStatus(): LiveData<ModelStatusMsg> {
        return repositoryBecomeSeller.getmDataFailure()
    }

    fun getmDataUpdateBusinessName(): LiveData<ModelStatusMsg> {
        return repositoryBecomeSeller.getmDataUpdateBusinessName()
    }


    fun getmDataUpdateAddress(): LiveData<ModelAddress> {
        return repositoryBecomeSeller.getmDataUpdateAddress()
    }


    fun getmDataCategories(): LiveData<ArrayList<ModelSellerTwo>> {
        return repositoryBecomeSeller.getmDataCategories()
    }

    fun getmDataCategoriesOnly(): LiveData<ModelBecomeSeller> {
        return repositoryBecomeSeller.getmDataCategoriesOnly()
    }

    fun getmDataTax(): LiveData<ModelBecomeSeller> {
        return repositoryBecomeSeller.getmDataTax()
    }

    fun getmDataBusinessInfo(): LiveData<ModelBecomeSeller> {
        return repositoryBecomeSeller.getmDataBusinessInfo()
    }

    fun getmDataPersonalsInfo(): LiveData<ModelBecomeSeller> {
        return repositoryBecomeSeller.getmDataPersonalsInfo()
    }


    fun getFirstStepCategoryApi(auth: String, list: String, steps: String) {
        repositoryBecomeSeller.getFirstStepCategoryApi(auth, list, steps)

    }

    fun getSecondStepCategoryApi(
        auth: String,
        businessName: String,
        ein: String,
        steps: String,
        businessId: String
    ) {
        repositoryBecomeSeller.getSecondStepCategoryApi(auth, businessName, ein, steps,businessId)

    }

    fun getThirdStepCategoryApi(
        auth: String,
        email: String,
        phoneNumber: String,
        address: String,
        website: String,
        steps: String,
        addressCity: String,
        addressState: String,
        addressZip: String,
        addressStreet: String,
        billingAdress: String,
        billingSate: String,
        billingCity: String,
        billingZip: String,
        billingType: Int,
        billingAddressfull: String,
        type: String,
        latitude: String,
        longitude: String,
        suite: String,
        businessId: String
    ) {
        repositoryBecomeSeller.getThirdStepCategoryApi(
            auth,email,phoneNumber,address,website,steps,addressCity,addressState,addressZip,addressStreet,billingAdress,billingSate,billingCity,billingZip,billingType,billingAddressfull,type,latitude,longitude,suite,businessId

        )
    }

    fun getStateList(auth: String) {
        repositoryBecomeSeller.getSateList(auth)
    }

    fun getmDataStates(): LiveData<ArrayList<ModelStates>> {
        return repositoryBecomeSeller.getmDataStates()
    }

    fun getFourthStep(
        auth: String,
        steps: String,
        legalName: String,
        lastName: String,
        homeAddress: String,
        dob: String,
        ssn: String,
        phoneNumber: String,
        addressStreet: String,
        addressCity: String,
        addressZip: String,
        addressState: String,
        lat: String,
        long: String,
        home_suite: String,
        legalNameComplete: String,
        businessId: String
    ) {
        repositoryBecomeSeller.getFourthStepApi(
            auth,
            steps,
            legalName,
            lastName,
            homeAddress,
            dob,
            ssn,
            phoneNumber,
            addressStreet,
            addressCity,
            addressZip,
            addressState,
            lat,
            long, home_suite, legalNameComplete,businessId
        )

    }

    fun getStripeData(
        auth: String,
        number: String,
        expMonth: String,
        expYear: String,
        cvc: String,
        payType: Int,
        step: String,
        businessId: String
    ) {
        repositoryBecomeSeller.getStripeData(auth, number, expMonth, expYear, cvc, payType, step,businessId)
    }

    fun getmDataAllCards(): LiveData<ModelBecomeSeller> {
        return repositoryBecomeSeller.getmDataDetails()
    }

    fun getStripeBankData(
        auth: String,
        routingNumber: String,
        accountNumber: String,
        ownersName: String,
        steps: String,
        payType: Int,
        businessId: String
    ) {
        repositoryBecomeSeller.getStripeBankData(
            auth,
            routingNumber,
            accountNumber,
            ownersName,
            steps,
            payType,businessId
        )

    }


    fun getmDataBankStep(): LiveData<ModelBecomeSeller> {
        return repositoryBecomeSeller.getmDataBankStep()
    }

    fun getmDataTerms(): LiveData<ModelBecomeSeller> {
        return repositoryBecomeSeller.getmDataTerms()
    }


    fun getLastStep(auth: String, step: String, businessId: String) {
        repositoryBecomeSeller.getLastStep(auth, step,businessId)
    }

    fun updateBillingAddress(
        auth: String,
        eTStreetAddress: String,
        eTCity: String,
        eTState: String,
        eTZip: String,
        eTSuite: String,
        type: String
    ) {
        repositoryBecomeSeller.updateBillingAddress(
            auth,
            eTStreetAddress,
            eTCity,
            eTState,
            eTZip,
            eTSuite,type
        )

    }

    fun validBusinessName(businessName: String) {
        repositoryBecomeSeller.validBusinessName(businessName)

    }

    fun updateBillingAddressCheck(
        auth: String,
        eTStreetAddress: String,
        eTCity: String,
        eTState: String,
        eTZip: String,
        eTSuite: String,
        type: String
    ) {
        repositoryBecomeSeller.updateBillingAddressCheck(
            auth,
            eTStreetAddress,
            eTCity,
            eTState,
            eTZip,
            eTSuite,type
        )

    }


}