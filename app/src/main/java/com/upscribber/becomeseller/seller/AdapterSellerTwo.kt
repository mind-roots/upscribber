package com.upscribber.becomeseller.seller

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import kotlinx.android.synthetic.main.seller_two_layout.view.*


class AdapterSellerTwo(private val mContext: Context
                       , listen: BecomeSeller
) :
    RecyclerView.Adapter<AdapterSellerTwo.MyViewHolder>() {

    var listener = listen as InterfaceSelection
    private var list: ArrayList<ModelSellerTwo> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val v = LayoutInflater.from(mContext)
            .inflate(R.layout.seller_two_layout, parent, false)

        return MyViewHolder(v)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = list[position]
        val imagePath = Constant.getPrefs(mContext).getString(Constant.dataImagePath, "")
        Glide.with(mContext).load(model.image).placeholder(R.mipmap.circular_placeholder).into(holder.itemView.listImage)
        holder.itemView.categories.text = model.name

        holder.itemView.setOnClickListener {
            listener.selection(position)
        }

        if (model.status) {
            holder.itemView.setBackgroundResource(R.drawable.bg_edit_selected)
            holder.itemView.done1.visibility = View.VISIBLE
            holder.itemView.categories.setTextColor(Color.parseColor("#ff3f55"))

        } else {
            holder.itemView.setBackgroundResource(0)
            holder.itemView.done1.visibility = View.GONE
            holder.itemView.categories.setTextColor(Color.parseColor("#0c0d2c"))
        }


    }

    fun getSelectedItems(): Int {
        var count=0
        for (i in 0 until list.size){
            if (list[i].status){
                count++
            }
        }
        return count
    }


    fun getSelectedCategories(): String {
        var ids= ""
        for (model in list){
            if (model.status){
                ids = if (ids.isEmpty()){
                    model.id
                }else{
                    "$ids,${model.id}"
                }
            }
        }
        return ids
    }

    fun update(it: java.util.ArrayList<ModelSellerTwo>?) {
        list = it!!
        notifyDataSetChanged()

    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)



}