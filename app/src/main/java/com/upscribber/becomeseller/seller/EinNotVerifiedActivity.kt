package com.upscribber.becomeseller.seller

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.databinding.DataBindingUtil
import com.upscribber.R
import com.upscribber.databinding.ActivityEinNotVerifiedBinding

class EinNotVerifiedActivity : AppCompatActivity() {

    lateinit var mBinding : ActivityEinNotVerifiedBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
       mBinding = DataBindingUtil.setContentView(this,R.layout.activity_ein_not_verified)
        setToolbar()
        clickListeners()
    }

    private fun clickListeners() {
        mBinding.textView346.setOnClickListener {
            finish()
        }

    }

    private fun setToolbar() {
        setSupportActionBar(mBinding.include14.toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)
        title = ""
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }
}
