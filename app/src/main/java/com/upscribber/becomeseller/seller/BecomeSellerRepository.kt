package com.upscribber.becomeseller.seller

import android.app.Application
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.upscribber.commonClasses.Constant
import com.upscribber.commonClasses.ModelStatusMsg
import com.upscribber.commonClasses.WebServicesCustomers
import com.upscribber.payment.paymentCards.PaymentCardModel
import com.upscribber.profile.ModelGetProfile
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class BecomeSellerRepository(var application: Application) {

    private val mDataStates = MutableLiveData<ArrayList<ModelStates>>()
    val mDataFailure = MutableLiveData<ModelStatusMsg>()
    val mDataUpdateBusinessName = MutableLiveData<ModelStatusMsg>()
    val mDataUpdateAddress = MutableLiveData<ModelAddress>()
    private val mDataCategories = MutableLiveData<ArrayList<ModelSellerTwo>>()
    private val mDataCategoriesOnly = MutableLiveData<ModelBecomeSeller>()
    private val mDataTax = MutableLiveData<ModelBecomeSeller>()
    private val mDataBusinessInfo = MutableLiveData<ModelBecomeSeller>()
    private val mDataPersonalInfo = MutableLiveData<ModelBecomeSeller>()
    private lateinit var arrayHomeProfile: ModelGetProfile
    private val mDataDetail = MutableLiveData<ModelBecomeSeller>()
    private val mDataBankStep = MutableLiveData<ModelBecomeSeller>()
    private val mDataTerms = MutableLiveData<ModelBecomeSeller>()

    //------------------------------ get Cateories Api ------------------------//
    fun getAllCategories(auth: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.getCategories(auth ,"")
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        val arrayCategories = ArrayList<ModelSellerTwo>()
                        if (status == "true") {
                            val data = json.getJSONArray("data")
                            for (i in 0 until data.length()) {
                                val dataCategries = data.getJSONObject(i)
                                val modelCategories = ModelSellerTwo()
                                modelCategories.name = dataCategries.optString("name")
                                modelCategories.subscription_count = dataCategries.optString("subscription_count")
                                modelCategories.image = dataCategries.optString("selected_image")
                                modelCategories.id = dataCategries.optString("id")
                                modelCategories.status1 = status
                                modelCategories.msg = msg
                                arrayCategories.add(modelCategories)
                            }
                            modelStatus.status = status
                            modelStatus.msg = msg
                            mDataCategories.value = arrayCategories
                        } else {
                            modelStatus.status = "false"
                            modelStatus.msg = msg
                            mDataFailure.value = modelStatus
                        }

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus

            }
        })
    }

    fun getmDataFailure(): LiveData<ModelStatusMsg> {
        return mDataFailure
    }

    fun getmDataUpdateBusinessName(): LiveData<ModelStatusMsg> {
        return mDataUpdateBusinessName
    }


    fun getmDataUpdateAddress(): LiveData<ModelAddress> {
        return mDataUpdateAddress
    }


    fun getmDataCategories(): LiveData<ArrayList<ModelSellerTwo>> {
        return mDataCategories
    }


    //------------------------------ get first step Api ------------------------//
    fun getFirstStepCategoryApi(auth: String, list: String, steps: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.merchantSignup(auth, list, steps, "1")
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        val modelBecomeSeller = ModelBecomeSeller()
                        if (status == "true") {
                            val data = json.getJSONObject("data")
                            modelBecomeSeller.business_id = data.optString("business_id")
                            modelBecomeSeller.birthdate = data.optString("birthdate")
                            modelBecomeSeller.status = status
                            modelBecomeSeller.msg = msg
                            modelStatus.status = status
                            modelStatus.msg = msg
                            mDataCategoriesOnly.value = modelBecomeSeller


                        } else {
                            modelStatus.status = "false"
                            modelStatus.msg = msg
                            mDataFailure.value = modelStatus
                        }


                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus

            }
        })

    }

    fun getmDataCategoriesOnly(): LiveData<ModelBecomeSeller> {
        return mDataCategoriesOnly
    }


    //------------------------------ get second step Api ------------------------//
    fun getSecondStepCategoryApi(
        auth: String,
        businessName: String,
        ein: String,
        steps: String,
        businessId: String
    ) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> =
            service.merchantSignupSecond(auth, businessName, ein, steps, "1",businessId)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        val modelBecomeSeller = ModelBecomeSeller()
                        if (status == "true") {
                            val data = json.getJSONObject("data")
                            modelBecomeSeller.business_id = data.optString("business_id")
                            modelBecomeSeller.status = status
                            modelBecomeSeller.msg = msg
                            modelStatus.status = status
                            modelStatus.msg = msg
                            mDataTax.value = modelBecomeSeller
                        } else {
                            modelStatus.status = "false"
                            modelStatus.msg = msg
                            mDataFailure.value = modelStatus
                        }


                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus

            }
        })

    }

    fun getmDataTax(): LiveData<ModelBecomeSeller> {
        return mDataTax
    }


    //------------------------------ get third step Api ------------------------//
    fun getThirdStepCategoryApi(
        auth: String,
        email: String,
        phoneNumber: String,
        address: String,
        website: String,
        steps: String,
        addressCity: String,
        addressState: String,
        addressZip: String,
        addressStreet: String,
        billingAdress: String,
        billingSate: String,
        billingCity: String,
        billingZip: String,
        billingType: Int,
        billingAddressfull: String,
        type1: String,
        latitude: String,
        longitude: String,
        suite: String,
        businessId: String
    ) {

        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.merchantSignupThird(
            auth,email,phoneNumber,address,website,steps,addressCity,addressState,addressZip,addressStreet,billingAdress,billingSate,billingCity,billingZip,billingType.toString(),billingAddressfull,type1,latitude,longitude,suite,businessId
        )
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        val modelBecomeSeller = ModelBecomeSeller()
                        if (status == "true") {
                            val data = json.getJSONObject("data")
                            modelBecomeSeller.business_id = data.optString("business_id")
                            modelBecomeSeller.status = status
                            modelBecomeSeller.msg = msg
                            modelStatus.status = status
                            modelStatus.msg = msg
                            mDataBusinessInfo.value = modelBecomeSeller
                        } else {
                            modelStatus.status = "false"
                            modelStatus.msg = msg
                            mDataFailure.value = modelStatus
                        }


                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus

            }
        })
    }

    fun getmDataBusinessInfo(): LiveData<ModelBecomeSeller> {
        return mDataBusinessInfo
    }

    fun getSateList(auth: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.getStates(auth)
        call.enqueue(object : Callback<ResponseBody> {


            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        val arrayStates = ArrayList<ModelStates>()

                        if (status == "true") {
                            val data = json.getJSONArray("data")
                            for (i in 0 until data.length()) {
                                val stateData = data.optJSONObject(i)
                                val modelStates = ModelStates()
                                modelStates.state_code = stateData.optString("state_code")
                                modelStates.state_name = stateData.optString("state_name")
                                modelStates.status = status
                                modelStates.msg = msg
                                arrayStates.add(modelStates)
                            }
                            modelStatus.status = status
                            modelStatus.msg = msg
                            mDataStates.value = arrayStates
                        } else {
                            modelStatus.status = "false"
                            modelStatus.msg = msg
                            mDataFailure.value = modelStatus
                        }


                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus

            }
        })

    }

    fun getmDataStates(): LiveData<ArrayList<ModelStates>> {
        return mDataStates
    }

    fun getFourthStepApi(
        auth: String,
        steps: String,
        legalName: String,
        lastName: String,
        homeAddress: String,
        dob: String,
        ssn: String,
        phoneNumber: String,
        addressStreet: String,
        addressCity: String,
        addressZip: String,
        addressState: String,
        lat: String,
        long: String,
        home_suite: String,
        legalNameComplete: String,
        businessId: String
    ) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.merchantSignupFourth(
            auth,
            steps,
            legalName,
            lastName,
            homeAddress,
            dob,
            ssn,
            phoneNumber,
            addressCity,
            addressState,
            addressZip,
            addressStreet,
            addressStreet,
            addressState,
            addressCity,
            addressZip,
            "1", lat, long, home_suite, legalNameComplete,businessId
        )
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        val modelBecomeSeller = ModelBecomeSeller()
                        if (status == "true") {
                            val data = json.getJSONObject("data")
                            modelBecomeSeller.business_id = data.optString("business_id")
                            modelBecomeSeller.status = status
                            modelBecomeSeller.msg = msg
                            modelStatus.status = status
                            modelStatus.msg = msg
                            mDataPersonalInfo.value = modelBecomeSeller
                        } else {
                            modelStatus.status = "false"
                            modelStatus.msg = msg
                            mDataFailure.value = modelStatus
                        }


                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus

            }
        })

    }

    fun getmDataPersonalsInfo(): LiveData<ModelBecomeSeller> {
        return mDataPersonalInfo
    }

    fun getStripeData(
        auth: String,
        number: String,
        expMonth: String,
        expYear: String,
        cvc: String,
        payType: Int,
        step: String,
        businessId: String
    ) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.stripeTokenUrl)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.tokensMerchant(number, expMonth, expYear, cvc, "USD")
        call.enqueue(object : Callback<ResponseBody> {


            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val cardModel = PaymentCardModel()
                        val id = json.optString("id")
                        cardModel.id = id
                        getFifthStepApi(auth, id, payType, step,businessId)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Toast.makeText(application, "Error!", Toast.LENGTH_LONG).show()
            }
        })

    }


    private fun getFifthStepApi(
        auth: String,
        stripe_token: String,
        payType: Int,
        step: String,
        businessId: String
    ) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.merchantSignupFifth(
            auth,
            stripe_token, payType.toString(), step,
            "1",businessId
        )
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        val modelBecomeSeller = ModelBecomeSeller()
                        if (status == "true") {
                            val data = json.getJSONObject("data")
                            modelBecomeSeller.business_id = data.optString("business_id")
                            modelBecomeSeller.status = status
                            modelBecomeSeller.msg = msg
                            modelStatus.status = status
                            modelStatus.msg = msg
                            mDataDetail.value = modelBecomeSeller
                        } else {
                            modelStatus.status = "false"
                            modelStatus.msg = msg
                            mDataFailure.value = modelStatus
                        }


                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus

            }
        })

    }

    fun getmDataDetails(): LiveData<ModelBecomeSeller> {
        return mDataDetail
    }

    fun getStripeBankData(
        auth: String,
        routingNumber: String,
        accountNumber: String,
        ownersName: String,
        steps: String,
        payType: Int,
        businessId: String
    ) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.stripeTokenUrl)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> =
            service.tokensBank(accountNumber, routingNumber, ownersName, "US", "USD")
        call.enqueue(object : Callback<ResponseBody> {


            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val cardModel = PaymentCardModel()
                        val id = json.optString("id")
                        cardModel.id = id
                        arrayHomeProfile =
                            Constant.getArrayListProfile(application, Constant.dataProfile)

                        getCustomerStripeBank(
                            auth,
                            routingNumber, accountNumber, ownersName,
                            steps,
                            payType,businessId
                        )

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Toast.makeText(application, "Error!", Toast.LENGTH_LONG).show()
            }
        })

    }

    private fun getCustomerStripeBank(
        auth: String,
        routingNumber: String,
        accountNumber: String,
        ownersName: String,
        steps: String,
        payType: Int,
        businessId: String
    ) {

        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        arrayHomeProfile = Constant.getArrayListProfile(application, Constant.dataProfile)

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.merchantSignuBank(
            auth,
            accountNumber, routingNumber, ownersName,
            steps, payType.toString()
            , "1",businessId
        )
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        val modelBecomeSeller = ModelBecomeSeller()
                        if (status == "true") {
                            val data = json.getJSONObject("data")
                            modelBecomeSeller.business_id = data.optString("business_id")
                            modelBecomeSeller.status = status
                            modelBecomeSeller.msg = msg
                            modelStatus.status = status
                            modelStatus.msg = msg
                            mDataBankStep.value = modelBecomeSeller
                        } else {
                            modelStatus.status = "false"
                            modelStatus.msg = msg
                            mDataFailure.value = modelStatus
                        }


                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus

            }
        })
    }


    fun getmDataBankStep(): LiveData<ModelBecomeSeller> {
        return mDataBankStep
    }

    fun getLastStep(auth: String, step: String, businessId: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.merchantSignuTerms(
            auth, step
            , "1",businessId
        )
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        val modelBecomeSeller = ModelBecomeSeller()
                        if (status == "true") {
                            val data = json.getJSONObject("data")
                            modelBecomeSeller.business_id = data.optString("business_id")
                            modelBecomeSeller.status = status
                            modelBecomeSeller.msg = msg
                            modelStatus.status = status
                            modelStatus.msg = msg
                            mDataTerms.value = modelBecomeSeller
                        } else {
                            val modelStatus = ModelStatusMsg()
                            modelStatus.status = "false"
                            modelStatus.msg = msg
                            mDataFailure.value = modelStatus
                        }


                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus

            }
        })

    }

    fun getmDataTerms(): LiveData<ModelBecomeSeller> {
        return mDataTerms
    }

    fun updateBillingAddress(
        auth: String,
        eTStreetAddress: String,
        eTCity: String,
        eTState: String,
        eTZip: String,
        eTSuite: String,
        type: String
    ) {

        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> =
            service.updateBillingAddress(auth, eTStreetAddress, eTState, eTCity, eTZip, eTSuite,type,"0")
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        val modelAddress = ModelAddress()
                        if (status == "true") {
                            modelAddress.streetAddress = eTStreetAddress
                            modelAddress.suiteNumber = eTSuite
                            modelAddress.city = eTCity
                            modelAddress.state = eTState
                            modelAddress.zipCode = eTZip
                            modelStatus.status = status
                            modelStatus.msg = msg
                            mDataUpdateAddress.value = modelAddress
                        } else {
                            modelStatus.status = "false"
                            modelStatus.msg = msg
                            mDataFailure.value = modelStatus
                        }


                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus

            }
        })
    }


     fun updateBillingAddressCheck(
        auth: String,
        eTStreetAddress: String,
        eTCity: String,
        eTState: String,
        eTZip: String,
        eTSuite: String,
        type: String
    ) {

        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> =
            service.updateBillingAddress1(auth, eTStreetAddress, eTState, eTCity, eTZip, eTSuite,type,"0")
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        val modelAddress = ModelAddress()
                        if (status == "true") {
                            modelAddress.billingStreet = eTStreetAddress
                            modelAddress.billingSuite = eTSuite
                            modelAddress.billingCity = eTCity
                            modelAddress.billingState = eTState
                            modelAddress.billingZip = eTZip
                            modelStatus.status = status
                            modelStatus.msg = msg
                            mDataUpdateAddress.value = modelAddress
                        } else {
                            modelStatus.status = "false"
                            modelStatus.msg = msg
                            mDataFailure.value = modelStatus
                        }


                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus

            }
        })
    }





    fun validBusinessName(businessName: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.validBusinessName(businessName)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        if (status == "true") {
                            modelStatus.status = status
                            modelStatus.msg = msg
                            mDataFailure.value = modelStatus
                        } else {
                            modelStatus.status = "false"
                            modelStatus.msg = msg
                            mDataUpdateBusinessName.value = modelStatus
                        }


                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus

            }
        })

    }

}