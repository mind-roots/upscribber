package com.upscribber.becomeseller.seller

import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.commonClasses.RoundedBottomSheetDialogFragment
import com.upscribber.profile.ProfileFragment.Companion.mViewModel
import com.upscribber.profile.ProfileViewModel
import com.upscribber.upscribberSeller.pictures.PicturesSellerActivity.Companion.viewModel
import com.upscribber.upscribberSeller.pictures.PicturesViewModel
import com.upscribber.upscribberSeller.pictures.logoImages.ModelLogoImages
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.activity_edit_profile.*
import kotlinx.android.synthetic.main.activity_upload_business_logo.*
import java.io.File
import java.io.FileOutputStream

class UploadBusinessLogo : AppCompatActivity() {

    private lateinit var circleImageView4: CircleImageView
    private lateinit var uploadPhoto: ImageView
    private lateinit var toolbar: Toolbar
    lateinit var progressBar3: LinearLayout
    lateinit var imageView31: ImageView

    companion object {
        lateinit var file: File
        lateinit var buttonDone: TextView


    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_upload_business_logo)
        viewModel = ViewModelProviders.of(this)[PicturesViewModel::class.java]
        mViewModel = ViewModelProviders.of(this)[ProfileViewModel::class.java]
        init()
        setToolbar()
        clickListeners()
        observerInit()
        checkIntent()
    }

    private fun checkIntent() {
        if (intent.hasExtra("profile")) {
            textView218.text = "Upload your profile picture to stand out from the crowd"
            textView219.text = "Upload Profile Picture"
            textView217.text =
                "Upload your profile picture as a PNG or JPEG file. Your profile picture will be cropped an displayed in the circle frame."

        }
    }

    private fun init() {
        toolbar = findViewById(R.id.include19)
        circleImageView4 = findViewById(R.id.circleImageView4)
        buttonDone = findViewById(R.id.buttonDone1)
        uploadPhoto = findViewById(R.id.uploadPhoto)
        progressBar3 = findViewById(R.id.progressBar3)
        imageView31 = findViewById(R.id.imageView31)
    }

    private fun setToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(com.upscribber.R.drawable.ic_arrow_back_black_24dp)

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        if (item!!.itemId == android.R.id.home) {
            onBackPressed()
        }

        return super.onOptionsItemSelected(item)
    }

    private fun observerInit() {
        viewModel.getIfImageUploaded().observe(this, Observer {
            if (it.status == "true") {
                progressBar3.visibility = View.GONE
                intent.putExtra("model", it.status)

                if (intent.hasExtra("profile")) {
                    setResult(910, intent)
                } else {
                    setResult(902, intent)
                }

                finish()
            } else {
                progressBar3.visibility = View.GONE
                buttonDone1.visibility = View.VISIBLE
            }

        })

        viewModel.getmDataFailure().observe(this, Observer {
            if (it.msg == "Invalid auth code") {
                progressBar3.visibility = View.GONE
                Constant.commonAlert(this)
            } else {
                progressBar3.visibility = View.GONE
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
            }

        })

        mViewModel.getImageSuccessful().observe(this, androidx.lifecycle.Observer {
            if (it.status == "true") {
                progressBar3.visibility = View.GONE
                intent.putExtra("model", it.status)

                if (intent.hasExtra("profile")) {
                    setResult(910, intent)
                } else {
                    setResult(902, intent)
                }

                finish()
            } else {
                progressBar3.visibility = View.GONE
                buttonDone1.visibility = View.VISIBLE
            }
        })
        mViewModel.getStatus().observe(this, androidx.lifecycle.Observer {
            if (it.msg == "Invalid auth code") {
                progressBar3.visibility = View.GONE
                Constant.commonAlert(this)
            } else {
                progressBar3.visibility = View.GONE
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
            }

        })


    }

    override fun onResume() {
        super.onResume()
    }

    private fun clickListeners() {
        relativeLayout.setOnClickListener {
            val fragment = MenuFragmentPictures(circleImageView4, uploadPhoto)
            fragment.show(supportFragmentManager, fragment.tag)
        }

        buttonDone.setOnClickListener {

            if (intent.hasExtra("profile")) {
                mViewModel.setImage(file, "1")
            } else {
                viewModel.setImage(file, "2")
            }

            progressBar3.visibility = View.VISIBLE
        }

        imageView31.setOnClickListener {

            Constant.CommonIAlert(
                this,
                "Logo",
                "Upload your logo as a PNG or JPEG file. Your logo will be cropped and displayed in a circle frame.”"
            )
        }


    }

    class MenuFragmentPictures(
        var circleImageView4: CircleImageView,
        var uploadPhoto: ImageView
    ) :
        RoundedBottomSheetDialogFragment() {

        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val view = inflater.inflate(R.layout.change_profile_pic_layout, container, false)
            val isSeller =
                Constant.getSharedPrefs(context!!).getBoolean(Constant.IS_SELLER, false)
            val cancelDialog = view.findViewById<TextView>(R.id.cancelDialog)
            val takePicture = view.findViewById<LinearLayout>(R.id.takePicture)
            val galleryLayout = view.findViewById<LinearLayout>(R.id.galleryLayout)

            if (isSeller) {
                cancelDialog.setBackgroundResource(R.drawable.bg_seller_button_blue)
            } else {
                cancelDialog.setBackgroundResource(R.drawable.invite_button_background)
            }


            takePicture.setOnClickListener {

                val checkSelfPermission =
                    ContextCompat.checkSelfPermission(
                        this.activity!!,
                        android.Manifest.permission.CAMERA
                    )
                if (checkSelfPermission != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(
                        this.activity!!,
                        arrayOf(android.Manifest.permission.CAMERA),
                        2
                    )
                } else {
                    openCamera()
                }

            }

            galleryLayout.setOnClickListener {

                val checkSelfPermission =
                    ContextCompat.checkSelfPermission(
                        this.activity!!,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE
                    )
                if (checkSelfPermission != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(
                        this.activity!!,
                        arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE),
                        1
                    )
                } else {
                    openAlbum()
                    // dismiss()
                }
            }

            cancelDialog.setOnClickListener {
                dismiss()
            }

            return view
        }

        private fun openCamera() {
            try {
                val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                startActivityForResult(intent, 29)

            } catch (e: Exception) {
                e.printStackTrace()

            }

        }

        private fun openAlbum() {
            val intent = Intent("android.intent.action.GET_CONTENT")
            intent.type = "image/*"
            startActivityForResult(intent, 143)

        }


        private fun getImageUri22(inImage: Bitmap): File {
            val root: String = Environment.getExternalStorageDirectory().toString()
            val myDir = File("$root/req_images")
            myDir.mkdirs()
            val fname = "Image_profile.jpg"
            val file = File(myDir, fname)
            if (file.exists()) {
                file.delete()
            }

            try {
                val out = FileOutputStream(file)
                inImage.compress(Bitmap.CompressFormat.JPEG, 70, out)
                out.flush()
                out.close()
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return file
        }

        override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
            super.onActivityResult(requestCode, resultCode, data)
            try {
                if (requestCode == 29) {
                    try {
                        if (data != null) {
                            val bitmap = data.extras.get("data") as Bitmap
                            file = getImageUri22(bitmap)
                            buttonDone.visibility = View.VISIBLE
                            uploadPhoto.visibility = View.GONE
                            circleImageView4.setImageBitmap(bitmap)
                            //viewModel.setImage(file, "2")
                        }
                        val model = ModelLogoImages()
                        model.image = 0
                        model.status = false
                        model.uriImage = data!!.data
                        dismiss()
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
                if (requestCode == 143) {
                    try {
                        val selectedImage: Uri = data!!.data
                        try {
                            val bitmap2: Uri = data.data
                            val uri = Constant.uploadImage(bitmap2, context!!)
                            file = Constant.uploadImage(bitmap2, context!!)
                            val bitmap = MediaStore.Images.Media.getBitmap(
                                activity!!.contentResolver,
                                bitmap2
                            )
                            uploadPhoto.visibility = View.GONE
                            circleImageView4.setImageBitmap(bitmap)
                            buttonDone.visibility = View.VISIBLE
                            //..................................Api FOr Update Image from gallery....................................................................//
                            //viewModel.setImage(file, "2")
                            dismiss()
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }

                dismiss()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

    }
}
