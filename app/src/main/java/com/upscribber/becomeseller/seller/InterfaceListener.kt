package com.upscribber.becomeseller.seller

interface InterfaceListener {

//    fun selection(isBusiness : Boolean, goToNextFragment : Int)
    fun nextFragmentId( i : Int)
//    fun nextFragment( i : Int)
    fun getFragments(i : Int)
    fun individualFragment()
    fun getIsBussiness(): Int

}