package com.upscribber.becomeseller.seller

import android.annotation.SuppressLint
import android.app.Activity
import android.app.DatePickerDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.res.Resources
import android.graphics.Color
import android.graphics.Paint
import android.location.Geocoder
import android.os.Bundle
import android.text.*
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.textfield.TextInputLayout
import com.stripe.android.view.StripeEditText
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.payment.paymentCards.AddCardViewModel
import kotlinx.android.synthetic.main.activity_become_seller.*
import kotlinx.android.synthetic.main.fragment_seller_add_cards.*
import kotlinx.android.synthetic.main.fragment_seller_business.*
import kotlinx.android.synthetic.main.fragment_seller_tax_info.*
import kotlinx.android.synthetic.main.fragment_seller_three.*
import kotlinx.android.synthetic.main.fragment_seller_two.*
import java.util.*
import kotlin.collections.ArrayList
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.util.TypedValue
import android.webkit.URLUtil
import android.widget.EditText
import androidx.viewpager.widget.ViewPager
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.upscribber.becomeseller.sellerWalkthrough.WalkthroghPagerAdapter
import com.upscribber.filters.LocationModel
import com.upscribber.home.ModelSteps
import com.upscribber.payment.paymentCards.WebPagesOpenActivity
import com.upscribber.profile.ChangeLocationSetingsActivity
import com.upscribber.profile.UpdateSkillsActivity
import com.upscribber.profile.UpdateSocialActivity
import kotlinx.android.synthetic.main.fragment_seller_add_cards.eTCardNumber1
import kotlinx.android.synthetic.main.fragment_seller_add_cards.eTextCvv
import kotlinx.android.synthetic.main.fragment_seller_add_cards.tvDateYear
import kotlinx.android.synthetic.main.fragment_seller_terms_condition.*
import kotlinx.android.synthetic.main.fragment_seller_three.textBilling
import kotlinx.android.synthetic.main.learn_to_sell.*
import kotlinx.android.synthetic.main.walk_through.*
import me.relex.circleindicator.Config


@Suppress(
    "NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS",
    "RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS"
)
class BecomeSeller : AppCompatActivity(), InterfaceListener, InterfaceSelection {

    lateinit var toolbarSeller: Toolbar
    private lateinit var toolbarTitle: TextView
    private lateinit var sellerNext: TextView
    var currentFragment: Int = 0
    var checked: Int = 0
    var flag = 0
    var phoneNumberLength = 0
    lateinit var mViewModel: BecomeSellerViewModel
    lateinit var mViewModelStripe: AddCardViewModel
    private var selection = ArrayList<ModelSellerTwo>()
    private lateinit var adapterSellerTwo: AdapterSellerTwo
    private lateinit var mExpiryTextInputLayout: TextInputLayout
    lateinit var mCvcEditText: StripeEditText
    var modelAddress: ModelAddress = ModelAddress()
    var modelAddressFourStep: ModelAddress = ModelAddress()
    val stepsCompleted: ArrayList<String> = ArrayList()
    var businessId = ""
    var website = 0
    val locationModel = LocationModel()
    var latitudesource = 1.0
    var longitudesource = 1.0
    var exist = 0
    var flagCardNumber = 0
    var flagCardCvv = 0
    var images = intArrayOf(R.mipmap.switch_to_dash, R.mipmap.manage_products, R.mipmap.create_subscription, R.mipmap.subscribe_now, R.mipmap.invite_subscription,R.mipmap.advertise_free)
    var myCustomPagerAdapter: WalkthroghPagerAdapter? = null
    var viewPager: ViewPager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_become_seller)
        mViewModel = ViewModelProviders.of(this)[BecomeSellerViewModel::class.java]
        mViewModelStripe = ViewModelProviders.of(this)[AddCardViewModel::class.java]
        initz()
        clickListeners()
        Places.initialize(applicationContext, resources.getString(R.string.google_maps_key))
        val homeProfile = Constant.getArrayListProfile(this, Constant.dataProfile)
        if (homeProfile.type != "1" || homeProfile.steps == null) {
            currentFragment = 0
        }
//        var stepVal = 0
//        if (homeProfile.type == "1") {
//            stepVal = homeProfile.steps.toInt()
//        }

//        if (homeProfile.type != "1" || homeProfile.steps == null) {
//            currentFragment = 0
//        }
//        var stepVal = 0
//        if (homeProfile.type == "1") {
//            stepVal = homeProfile.steps.toInt()
//        }Z
//        currentFragment = stepVal + 1
        currentFragment = 1
        phone.text = Constant.formatPhoneNumber(homeProfile.contact_no)
        myCustomPagerAdapter = WalkthroghPagerAdapter(this,images)
        viewPager!!.adapter = myCustomPagerAdapter
        settingIndicators()
        pagerListener()
        removeData()
        textView65.loadUrl("file:///android_asset/terms.html")
        apiCategoriesapi()
        nextFragmentId(currentFragment)
        setToolbar()
        setAdapter()
        observerInit()
        setData()
        setButtonVisible(currentFragment)
        spanableText()
    }

    private fun pagerListener() {
        viewPager!!.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {/*empty*/

                when (position) {
                    0 -> {
                        textView331.text = "Switch to Dashboard"
                        textView374.text = "Switch between the marketplace and\nyour dashboard with a tap"
                    }
                    1 -> {
                        textView331.text = "Manage your Store"
                        textView374.text = "Add products and services to your\nstore to create subscriptions"
                    }
                    2 -> {
                        textView331.text = "Create Subscriptions"
                        textView374.text = "Start with the basics and create a\nsubscription in a few easy steps"
                    }
                    3 -> {
                        textView331.text = "Subscribe Now"
                        textView374.text = "Don't let customers slip away -\nsubscribe them on the spot"
                    }
                    4 -> {
                        textView331.text = "Invite Customers"
                        textView374.text = "They already know and love your\nbusiness, invite them to subscribe"
                    }
                    else -> {
                        textView331.text = "Advertise for free"
                        textView374.text = "Publish your subscriptions in the\nmarketplace and let customers find you"
                    }
                }

            }

            override fun onPageSelected(position: Int) {
                when (position) {
                    0 -> {
                        textView331.text = "Switch to Dashboard"
                        textView374.text = "Switch between the marketplace and\nyour dashboard with a tap"
                    }
                    1 -> {
                        textView331.text = "Manage your Store"
                        textView374.text = "Add products and services to your\nstore to create subscriptions"
                    }
                    2 -> {
                        textView331.text = "Create Subscriptions"
                        textView374.text = "Start with the basics and create a\nsubscription in a few easy steps"
                    }
                    3 -> {
                        textView331.text = "Subscribe Now"
                        textView374.text = "Don't let customers slip away -\nsubscribe them on the spot"
                    }
                    4 -> {
                        textView331.text = "Invite Customers"
                        textView374.text = "They already know and love your\nbusiness, invite them to subscribe"
                    }
                    else -> {
                        textView331.text = "Advertise for free"
                        textView374.text = "Publish your subscriptions in the\nmarketplace and let customers find you"
                    }
                }
            }

            override fun onPageScrollStateChanged(state: Int) {/*empty*/
            }
        })

    }

    private fun settingIndicators() {
        val indicatorWidth = (TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, 10f,
            resources.displayMetrics
        ) + 0.5f).toInt()
        val indicatorHeight = (TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, 4f,
            resources.displayMetrics
        ) + 0.5f).toInt()
        val indicatorMargin = (TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, 4f,
            resources.displayMetrics
        ) + 0.5f).toInt()

        val config = Config.Builder().width(indicatorWidth)
            .height(indicatorHeight)
            .margin(indicatorMargin)
//            .animatorReverse(R.animator.indicator_animator_reverse)
            .drawable(R.drawable.bg_seller_button_blue)
            .drawableUnselected(R.drawable.bg_grey)
            .build()
        indicator_login.initialize(config)
        viewPager!!.adapter = WalkthroghPagerAdapter(this,images)
        indicator_login.setViewPager(viewPager)

    }

    private fun setButtonVisible(currentFragment: Int) {
        if (currentFragment == 2) {
            if (eTLegalName.text.isNotEmpty() && etEin.text.toString().isNotEmpty()) {
                sellerNext.setBackgroundResource(R.drawable.bg_cards_fill_new)
            } else {
                sellerNext.setBackgroundResource(R.drawable.bg_cards_fill_opacity)
            }

            eTLegalName.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(
                    s: CharSequence,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                    taxVisibleBtn()

                }

                override fun afterTextChanged(s: Editable) {
                }
            })


            etEin.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(
                    s: CharSequence,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                    val text = etEin.text.toString()
                    val textLength = etEin.text.length

                    if (text.endsWith("-") || text.endsWith(" ") || text.endsWith(" "))
                        return
                    if (textLength == 3) {
                        etEin.setText(StringBuilder(text).insert(text.length - 1, "-").toString())
                        (etEin as EditText).setSelection(etEin.text.length)
                    }
                    taxVisibleBtn()

                }

                override fun afterTextChanged(s: Editable) {
                }
            })

        } else if (currentFragment == 3) {
            sellerNext.setBackgroundResource(R.drawable.bg_cards_fill_opacity)


            eTPhoneNumber.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(
                    s: CharSequence,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                    val text = eTPhoneNumber.text.toString()
                    val phoneNumber = eTPhoneNumber.text.toString().length
                    if (getTextFieldsData()) {
                        sellerNext.setBackgroundResource(R.drawable.bg_cards_fill_new)
                    } else {
                        sellerNext.setBackgroundResource(R.drawable.bg_cards_fill_opacity)
                    }

                    if (text.endsWith("-") || text.endsWith(" ") || text.endsWith(" "))
                        return
                    if (phoneNumber == 4) {
                        eTPhoneNumber.setText(
                            StringBuilder(text).insert(text.length - 1, " ").toString()
                        )
                        (eTPhoneNumber as EditText).setSelection(eTPhoneNumber.text.length)
                    }
                    if (phoneNumber == 8) {
                        eTPhoneNumber.setText(
                            StringBuilder(text).insert(text.length - 1, " ").toString()
                        )
                        (eTPhoneNumber as EditText).setSelection(eTPhoneNumber.text.length)

                    }
                    phoneNumberLength = if (phoneNumber == 12) {
                        1
                    } else {
                        0
                    }

                }

                override fun afterTextChanged(s: Editable) {
                }
            })

            eTLegalEmail.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(
                    s: CharSequence,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                    if (getTextFieldsData()) {
                        sellerNext.setBackgroundResource(R.drawable.bg_cards_fill_new)
                    } else {
                        sellerNext.setBackgroundResource(R.drawable.bg_cards_fill_opacity)
                    }
                }

                override fun afterTextChanged(s: Editable) {
                }
            })


            txtAddress.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(
                    s: CharSequence,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                    if (getTextFieldsData()) {
                        sellerNext.setBackgroundResource(R.drawable.bg_cards_fill_new)
                    } else {
                        sellerNext.setBackgroundResource(R.drawable.bg_cards_fill_opacity)
                    }
                }

                override fun afterTextChanged(s: Editable) {
                }
            })

            eTBillingAddress.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(
                    s: CharSequence,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                    if (getTextFieldsData()) {
                        sellerNext.setBackgroundResource(R.drawable.bg_cards_fill_new)
                    } else {
                        sellerNext.setBackgroundResource(R.drawable.bg_cards_fill_opacity)
                    }
                }

                override fun afterTextChanged(s: Editable) {
                }
            })

            eTWebsite.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(
                    s: CharSequence,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                    if (getTextFieldsData()) {
                        sellerNext.setBackgroundResource(R.drawable.bg_cards_fill_new)
                    } else {
                        sellerNext.setBackgroundResource(R.drawable.bg_cards_fill_opacity)
                    }
                }

                override fun afterTextChanged(s: Editable) {
                }
            })


            switchBilling.setOnCheckedChangeListener { p0, p1 ->
                if (getTextFieldsData()) {
                    sellerNext.setBackgroundResource(R.drawable.bg_cards_fill_new)
                } else {
                    sellerNext.setBackgroundResource(R.drawable.bg_cards_fill_opacity)
                }
                if (p1) {
                    textBilling.visibility = View.GONE
                    eTBillingAddress.visibility = View.GONE
                } else {
                    textBilling.visibility = View.VISIBLE
                    eTBillingAddress.visibility = View.VISIBLE
                }
            }


            switchWebsite.setOnCheckedChangeListener { p0, p1 ->
                if (getTextFieldsData()) {
                    sellerNext.setBackgroundResource(R.drawable.bg_cards_fill_new)
                } else {
                    sellerNext.setBackgroundResource(R.drawable.bg_cards_fill_opacity)
                }
                if (p1) {
                    website = 1
                    eTWebsite.visibility = View.VISIBLE
                } else {
                    website = 0
                    eTWebsite.visibility = View.GONE
                }
            }

        } else if (currentFragment == 4) {
            sellerNext.setBackgroundResource(R.drawable.bg_cards_fill_opacity)
            eTLegalNamePersonal.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(
                    s: CharSequence,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                    if (getTextFieldsPersonalData()) {
                        sellerNext.setBackgroundResource(R.drawable.bg_cards_fill_new)
                    } else {
                        sellerNext.setBackgroundResource(R.drawable.bg_cards_fill_opacity)
                    }
                }

                override fun afterTextChanged(s: Editable) {
                }
            })



            etLastNamePersonal.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(
                    s: CharSequence,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                    if (getTextFieldsPersonalData()) {
                        sellerNext.setBackgroundResource(R.drawable.bg_cards_fill_new)
                    } else {
                        sellerNext.setBackgroundResource(R.drawable.bg_cards_fill_opacity)
                    }
                }

                override fun afterTextChanged(s: Editable) {
                }
            })

            eTLegalLastName.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(
                    s: CharSequence,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                    if (getTextFieldsPersonalData()) {
                        sellerNext.setBackgroundResource(R.drawable.bg_cards_fill_new)
                    } else {
                        sellerNext.setBackgroundResource(R.drawable.bg_cards_fill_opacity)
                    }
                }

                override fun afterTextChanged(s: Editable) {
                }
            })

            etAddress.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(
                    s: CharSequence,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                    if (getTextFieldsPersonalData()) {
                        sellerNext.setBackgroundResource(R.drawable.bg_cards_fill_new)
                    } else {
                        sellerNext.setBackgroundResource(R.drawable.bg_cards_fill_opacity)
                    }
                }

                override fun afterTextChanged(s: Editable) {
                }
            })


            etDob.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(
                    s: CharSequence,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                    if (getTextFieldsPersonalData()) {
                        sellerNext.setBackgroundResource(R.drawable.bg_cards_fill_new)
                    } else {
                        sellerNext.setBackgroundResource(R.drawable.bg_cards_fill_opacity)
                    }
                }

                override fun afterTextChanged(s: Editable) {
                }
            })

            ssn.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(
                    s: CharSequence,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                    if (getTextFieldsPersonalData()) {
                        sellerNext.setBackgroundResource(R.drawable.bg_cards_fill_new)
                    } else {
                        sellerNext.setBackgroundResource(R.drawable.bg_cards_fill_opacity)
                    }
                }

                override fun afterTextChanged(s: Editable) {
                }
            })

            phone.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(
                    s: CharSequence,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                    if (getTextFieldsPersonalData()) {
                        sellerNext.setBackgroundResource(R.drawable.bg_cards_fill_new)
                    } else {
                        sellerNext.setBackgroundResource(R.drawable.bg_cards_fill_opacity)
                    }
                }

                override fun afterTextChanged(s: Editable) {
                }
            })

        } else if (currentFragment == 5) {
            sellerNext.setBackgroundResource(R.drawable.bg_cards_fill_opacity)
            if (flag == 0) {
                eTextCvv.addTextChangedListener(object : TextWatcher {

                    override fun beforeTextChanged(
                        s: CharSequence,
                        start: Int,
                        count: Int,
                        after: Int
                    ) {


                    }

                    override fun onTextChanged(
                        s: CharSequence,
                        start: Int,
                        before: Int,
                        count: Int
                    ) {
                        flagCardCvv = if (s.length == 3) {
                            Constant.hideKeyboard(this@BecomeSeller, eTextCvv)
                            1
                        } else {
                            0
                        }
                    }

                    override fun afterTextChanged(s: Editable) {

                    }
                })




                eTCardNumber1.addTextChangedListener(object : TextWatcher {
                    override fun beforeTextChanged(
                        s: CharSequence,
                        start: Int,
                        count: Int,
                        after: Int
                    ) {
                    }

                    override fun onTextChanged(
                        s: CharSequence,
                        start: Int,
                        before: Int,
                        count: Int
                    ) {
                        if (getTextFieldsCardData()) {
                            sellerNext.setBackgroundResource(R.drawable.bg_cards_fill_new)
                        } else {
                            sellerNext.setBackgroundResource(R.drawable.bg_cards_fill_opacity)
                        }
                    }

                    override fun afterTextChanged(s: Editable) {
                    }
                })


                tvDateYear.addTextChangedListener(object : TextWatcher {
                    override fun beforeTextChanged(
                        s: CharSequence,
                        start: Int,
                        count: Int,
                        after: Int
                    ) {
                    }

                    override fun onTextChanged(
                        s: CharSequence,
                        start: Int,
                        before: Int,
                        count: Int
                    ) {
                        if (getTextFieldsCardData()) {
                            sellerNext.setBackgroundResource(R.drawable.bg_cards_fill_new)
                        } else {
                            sellerNext.setBackgroundResource(R.drawable.bg_cards_fill_opacity)
                        }
                    }

                    override fun afterTextChanged(s: Editable) {
                    }
                })

                eTextCvv.addTextChangedListener(object : TextWatcher {
                    override fun beforeTextChanged(
                        s: CharSequence,
                        start: Int,
                        count: Int,
                        after: Int
                    ) {
                    }

                    override fun onTextChanged(
                        s: CharSequence,
                        start: Int,
                        before: Int,
                        count: Int
                    ) {
                        if (getTextFieldsCardData()) {
                            sellerNext.setBackgroundResource(R.drawable.bg_cards_fill_new)
                        } else {
                            sellerNext.setBackgroundResource(R.drawable.bg_cards_fill_opacity)
                        }
                    }

                    override fun afterTextChanged(s: Editable) {
                    }
                })
            } else {
                etOwnersName.addTextChangedListener(object : TextWatcher {
                    override fun beforeTextChanged(
                        s: CharSequence,
                        start: Int,
                        count: Int,
                        after: Int
                    ) {
                    }

                    override fun onTextChanged(
                        s: CharSequence,
                        start: Int,
                        before: Int,
                        count: Int
                    ) {
                        if (getTextFieldsBankData()) {
                            sellerNext.setBackgroundResource(R.drawable.bg_cards_fill_new)
                        } else {
                            sellerNext.setBackgroundResource(R.drawable.bg_cards_fill_opacity)
                        }
                    }

                    override fun afterTextChanged(s: Editable) {
                    }
                })

                etRoutingNumber.addTextChangedListener(object : TextWatcher {
                    override fun beforeTextChanged(
                        s: CharSequence,
                        start: Int,
                        count: Int,
                        after: Int
                    ) {
                    }

                    override fun onTextChanged(
                        s: CharSequence,
                        start: Int,
                        before: Int,
                        count: Int
                    ) {
                        if (getTextFieldsBankData()) {
                            sellerNext.setBackgroundResource(R.drawable.bg_cards_fill_new)
                        } else {
                            sellerNext.setBackgroundResource(R.drawable.bg_cards_fill_opacity)
                        }
                    }

                    override fun afterTextChanged(s: Editable) {
                    }
                })

                etAccountNumber.addTextChangedListener(object : TextWatcher {
                    override fun beforeTextChanged(
                        s: CharSequence,
                        start: Int,
                        count: Int,
                        after: Int
                    ) {
                    }

                    override fun onTextChanged(
                        s: CharSequence,
                        start: Int,
                        before: Int,
                        count: Int
                    ) {
                        if (getTextFieldsBankData()) {
                            sellerNext.setBackgroundResource(R.drawable.bg_cards_fill_new)
                        } else {
                            sellerNext.setBackgroundResource(R.drawable.bg_cards_fill_opacity)
                        }
                    }

                    override fun afterTextChanged(s: Editable) {
                    }
                })

                confirmAccountNumber.addTextChangedListener(object : TextWatcher {
                    override fun beforeTextChanged(
                        s: CharSequence,
                        start: Int,
                        count: Int,
                        after: Int
                    ) {
                    }

                    override fun onTextChanged(
                        s: CharSequence,
                        start: Int,
                        before: Int,
                        count: Int
                    ) {
                        if (getTextFieldsBankData()) {
                            sellerNext.setBackgroundResource(R.drawable.bg_cards_fill_new)
                        } else {
                            sellerNext.setBackgroundResource(R.drawable.bg_cards_fill_opacity)
                        }
                    }

                    override fun afterTextChanged(s: Editable) {
                    }
                })


            }
        } else if (currentFragment == 6) {
            sellerNext.setBackgroundResource(R.drawable.bg_cards_fill_opacity)
            switchCompat.setOnCheckedChangeListener { buttonView, isChecked ->
                if (isChecked) {
                    sellerNext.setBackgroundResource(R.drawable.bg_cards_fill_new)
                } else {
                    sellerNext.setBackgroundResource(R.drawable.bg_cards_fill_opacity)
                }
            }
        }
    }

    private fun getTextFieldsBankData(): Boolean {
        val etOwnersName = etOwnersName.text.toString().trim().length
        val etRoutingNumber = etRoutingNumber.text.toString().trim().length
        val confirmAccountNumber = confirmAccountNumber.text.toString().trim().length
        val etAccountNumber = etAccountNumber.text.toString().trim().length


        if (etOwnersName == 0) {
            return false
        }
        if (etRoutingNumber == 0) {
            return false
        }
        if (etAccountNumber == 0) {
            return false
        }
        if (confirmAccountNumber == 0) {
            return false
        }

        return true

    }

    private fun getTextFieldsCardData(): Boolean {
        val eTCardNumber = eTCardNumber1.text.toString().trim().length
        val tvDateYear = tvDateYear.text.toString().trim().length
        val eTextCvv = eTextCvv.text.toString().trim().length

        if (eTCardNumber < 16) {
            return false
        }
        if (tvDateYear == 0) {
            return false
        }
        if (eTextCvv < 3) {
            return false
        }

        return true

    }

    private fun getTextFieldsPersonalData(): Boolean {
        val eTLegalNamePersonal = eTLegalNamePersonal.text.toString().trim().length
        val etLastNamePersonal = etLastNamePersonal.text.toString().trim().length
        val eTLegalLastName = eTLegalLastName.text.toString().trim().length
        val etAddress = etAddress.text.toString().trim().length
        val etDob = etDob.text.toString().trim().length
        val ssn = ssn.text.toString().trim().length
        val phone = phone.text.toString().trim().length

        if (eTLegalNamePersonal == 0) {
            return false
        }
        if (etLastNamePersonal == 0) {
            return false
        }
        if (eTLegalLastName == 0) {
            return false
        }
        if (etAddress == 0) {
            return false
        }
        if (etDob == 0) {
            return false
        }
        if (ssn == 0) {
            return false
        }
        if (phone == 0) {
            return false
        }


        return true

    }

    private fun getTextFieldsData(): Boolean {
        val phoneNumber = eTPhoneNumber.text.toString().trim().length
        val email = eTLegalEmail.text.toString().trim().length
        val address = txtAddress.text.toString().trim().length
        val eTBillingAddress = eTBillingAddress.text.toString().trim().length
        val eTWebsite = eTWebsite.text.toString().trim().length

        if (phoneNumber == 0) {
            return false
        }
        if (email == 0) {
            return false
        }
        if (address == 0) {
            return false
        }
        if (!switchBilling.isChecked) {
            if (eTBillingAddress == 0) {
                return false
            }
        }

        if (switchWebsite.isChecked) {
            if (eTWebsite == 0) {
                return false
            }
        }

        return true

    }

    private fun taxVisibleBtn() {
        val text = eTLegalName.text.toString()
        val textLength = eTLegalName.text.length
        val textLength1 = etEin.text.length

        if (text.endsWith("-") || text.endsWith(" ") || text.endsWith(" "))
            return
        if (textLength > 0 && textLength1 == 10) {
            sellerNext.setBackgroundResource(R.drawable.bg_cards_fill_new)
        } else {
            sellerNext.setBackgroundResource(R.drawable.bg_cards_fill_opacity)
        }

    }

    private fun removeData() {
        try {
            val sharedPreferences1 = getSharedPreferences("updateProfile", Context.MODE_PRIVATE)
            val editor = sharedPreferences1.edit()
            editor.remove("social")
            editor.apply()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun apiCategoriesapi() {
        val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
        mViewModel.getCategories(auth)
        showLoader()
    }

    override fun onResume() {
        super.onResume()

        try {
            val sharedPreferences1 = getSharedPreferences("updateProfile", Context.MODE_PRIVATE)
            if (!sharedPreferences1.getString("social", "").isEmpty()) {
                addSocial.isEnabled = false
                addSocial.paintFlags =
                    addSocial.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
                imageView81.visibility = View.VISIBLE
                if (stepsCompleted.contains("4")) {
                    stepsCompleted.remove("4")
                }
                stepsCompleted.add("4")

                if (stepsCompleted.size >= 5) {
                    startSellingButton.alpha = 1.0F
                } else {
                    startSellingButton.alpha = 0.4F
                }
                startSellingButton.isEnabled = stepsCompleted.size >= 5

            }
        } catch (e: Exception) {
            e.printStackTrace()
        }


    }


    private fun setCategories() {
        val prefs = Constant.getPrefs(this)
        val gson = Gson()
        val json11 = prefs.getString("step1", null)

        if ((json11 != "" || json11 != null) && selection.isNotEmpty()) {
            val arrayCategories = getArrayList("step1")
            for (i in 0 until arrayCategories.size) {
                for (j in 0 until selection.size)
                    if (selection[j].id == arrayCategories[i].trim()) {
                        val model = selection[j]
                        model.status = !model.status
                        sellerNext.setBackgroundResource(R.drawable.bg_cards_fill_new)
                        selection[j] = model
                        adapterSellerTwo.notifyDataSetChanged()
                    }
            }
        }
    }


    private fun setData() {
        try {
            val type = Constant.getPrefs(this).getString(Constant.type, "")
            if (type == "1") {
                val prefs = Constant.getPrefs(this)
                val json = prefs.getString("step2", null)
                if (json.isNotEmpty()) {
                    val stepTwoData = getData("step2")
                    eTLegalName.setText(stepTwoData.business_name)
                    etEin.setText(stepTwoData.ein)
                }

                val json1 = prefs.getString("step3", null)
                if (json1.isNotEmpty()) {
                    val stepThreeData = getData("step3")
                    eTPhoneNumber.setText(
                        Constant.getSplitPhoneNumberWithoutCode(
                            stepThreeData.business_phone,
                            this
                        )
                    )
                    eTLegalEmail.setText(stepThreeData.business_email)
                    eTWebsite.setText(stepThreeData.website)
                    txtAddress.text = stepThreeData.business_address
                    eTBillingAddress.text = stepThreeData.billing_address
                    modelAddress.city = stepThreeData.billing_city
                    modelAddress.streetAddress = stepThreeData.billing_street
                    modelAddress.state = stepThreeData.billing_state
                    modelAddress.suiteNumber = stepThreeData.suite
                    modelAddress.zipCode = stepThreeData.billing_zipcode

                    if (eTPhoneNumber.text.toString().trim()
                            .isNotEmpty() || eTLegalEmail.text.toString().trim()
                            .isNotEmpty() || eTWebsite.text.toString().trim().isNotEmpty() ||
                        txtAddress.text.toString().trim().isNotEmpty()
                    ) {
                        sellerNext.setBackgroundResource(R.drawable.bg_cards_fill_new)
                    } else {
                        sellerNext.setBackgroundResource(R.drawable.bg_cards_fill_opacity)
                    }

                }

                val json2 = prefs.getString("step4", null)
                if (json2.isNotEmpty()) {
                    val stepFourData = getData("step4")
                    eTLegalNamePersonal.setText(stepFourData.first_name)
                    etLastNamePersonal.setText(stepFourData.last_name)
                    eTLegalLastName.setText(stepFourData.legal_name)
                    etAddress.text = stepFourData.home_address
                    etDob.text = stepFourData.birthdate.replace("-", "/")
                    ssn.setText(stepFourData.ssn)
                    modelAddressFourStep.suiteNumber = stepFourData.home_suite
                    modelAddressFourStep.zipCode = stepFourData.home_zipcode
                    modelAddressFourStep.state = stepFourData.home_state
                    modelAddressFourStep.streetAddress = stepFourData.home_street
                    modelAddressFourStep.city = stepFourData.home_city

                    if (eTLegalNamePersonal.text.toString().trim().isNotEmpty() ||
                        etLastNamePersonal.text.toString().trim().isNotEmpty() ||
                        eTLegalLastName.text.toString().trim().isNotEmpty() ||
                        etAddress.text.toString().trim().isNotEmpty() ||
                        etDob.text.toString().trim().isNotEmpty() ||
                        ssn.text.toString().trim().isNotEmpty() ||
                        phone.text.toString().trim().isNotEmpty()
                    ) {
                        sellerNext.setBackgroundResource(R.drawable.bg_cards_fill_new)
                    } else {
                        sellerNext.setBackgroundResource(R.drawable.bg_cards_fill_opacity)
                    }


                    //Log.e("step4Data",stepFourData.business_phone)
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }


    }

    private fun getData(key: String): ModelSteps {
        val prefs = Constant.getPrefs(this)
        val gson = Gson()
        val json = prefs.getString(key, null)
        val type = gson.fromJson<ModelSteps>(
            json,
            object : TypeToken<ModelSteps>() {

            }.type
        )
        return type

    }


    fun getArrayList(key: String): ArrayList<String> {
        val prefs = Constant.getPrefs(this)
        val gson = Gson()
        val json = prefs.getString(key, null)
        val type = gson.fromJson<ArrayList<String>>(
            json,
            object : TypeToken<List<String>>() {

            }.type
        )
        return type
    }

    private fun spanableText() {
        val spannableString =
            SpannableString("By Registering your account,you agree to our Services Agreement and the Stripe connected account")
        val spannableString1 =
            SpannableString("I have read and agree to Upscribbr’s General Terms and the applicable additional terms including the Privacy Policy")
        spannableString.setSpan(clickableSpan, 45, 63, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        spannableString1.setSpan(
            clickableSpanGeneralTerms,
            37,
            50,
            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        spannableString1.setSpan(clickableSpanPrivacy, 101, 115, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        spannableString.setSpan(
            ForegroundColorSpan(Color.BLACK),
            45,
            63,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        spannableString1.setSpan(
            ForegroundColorSpan(resources.getColor(R.color.blue)),
            37,
            50,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        spannableString1.setSpan(
            ForegroundColorSpan(resources.getColor(R.color.blue)),
            101,
            115,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )

        textSpanBusiness.text = spannableString
        textSpanPersonal.text = spannableString
        textSpanTax.text = spannableString
        textSpanBank.text = spannableString
        textSpanDebit.text = spannableString
        textView66.text = spannableString1

        textSpanBusiness.movementMethod = LinkMovementMethod.getInstance()
        textSpanTax.movementMethod = LinkMovementMethod.getInstance()
        textSpanPersonal.movementMethod = LinkMovementMethod.getInstance()
        textSpanBank.movementMethod = LinkMovementMethod.getInstance()
        textSpanDebit.movementMethod = LinkMovementMethod.getInstance()
        textView66.movementMethod = LinkMovementMethod.getInstance()

        spannableString.setSpan(clickableSpanServices, 72, 96, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        spannableString.setSpan(
            ForegroundColorSpan(Color.BLACK),
            72,
            96,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        textSpanBusiness.text = spannableString
        textSpanPersonal.text = spannableString
        textSpanTax.text = spannableString
        textSpanBank.text = spannableString
        textSpanDebit.text = spannableString

    }

    val clickableSpanGeneralTerms = object : ClickableSpan() {
        @SuppressLint("SetJavaScriptEnabled")
        override fun onClick(textView: View) {
            startActivity(
                Intent(
                    this@BecomeSeller,
                    WebPagesOpenActivity::class.java
                ).putExtra("webTerms", "webTerms")
            )
        }

        override fun updateDrawState(ds: TextPaint) {
            super.updateDrawState(ds)
            ds.isUnderlineText = false
        }
    }
    val clickableSpanPrivacy = object : ClickableSpan() {
        @SuppressLint("SetJavaScriptEnabled")
        override fun onClick(textView: View) {
            startActivity(
                Intent(
                    this@BecomeSeller,
                    WebPagesOpenActivity::class.java
                ).putExtra("privacy", "privacy")
            )
        }

        override fun updateDrawState(ds: TextPaint) {
            super.updateDrawState(ds)
            ds.isUnderlineText = false
        }
    }

    val clickableSpan = object : ClickableSpan() {
        @SuppressLint("SetJavaScriptEnabled")
        override fun onClick(textView: View) {
            startActivity(
                Intent(
                    this@BecomeSeller,
                    WebPagesOpenActivity::class.java
                ).putExtra("services", "services")
            )
        }

        override fun updateDrawState(ds: TextPaint) {
            super.updateDrawState(ds)
            ds.isUnderlineText = false
        }
    }

    val clickableSpanServices = object : ClickableSpan() {
        @SuppressLint("SetJavaScriptEnabled")
        override fun onClick(textView: View) {
            startActivity(
                Intent(
                    this@BecomeSeller,
                    WebPagesOpenActivity::class.java
                ).putExtra("legal", "legal")
            )
        }

        override fun updateDrawState(ds: TextPaint) {
            super.updateDrawState(ds)
            ds.isUnderlineText = false
        }
    }


    @SuppressLint("SetTextI18n")
    private fun clickListeners() {

        eTBillingAddress.setOnClickListener {
            Constant.hideKeyboard(this, eTBillingAddress)
            Places.createClient(this)
            val fields =
                listOf(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG)
            // Start the autocomplete intent.
            val intent = Autocomplete.IntentBuilder(
                    AutocompleteActivityMode.FULLSCREEN, fields
                )
                .build(this)
            startActivityForResult(intent, 12)
//            val intent =
//                Intent(this, ManualBusinessAddress::class.java).putExtra("model", modelAddress)
//            startActivityForResult(intent, 1)
        }

        txtAddress.setOnClickListener {
            Constant.hideKeyboard(this, eTBillingAddress)
            Places.createClient(this)
            val fields =
                listOf(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG)
            // Start the autocomplete intent.
            val intent = Autocomplete.IntentBuilder(
                    AutocompleteActivityMode.FULLSCREEN, fields
                )
                .build(this)
            startActivityForResult(intent, 11)
//            val intent =
//                Intent(this, ManualBusinessAddress::class.java).putExtra("model", modelAddress)
//            startActivityForResult(intent, 1)
        }


        etAddress.setOnClickListener {
            Constant.hideKeyboard(this, eTBillingAddress)
            Places.createClient(this)
            val fields =
                listOf(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG)
            // Start the autocomplete intent.
            val intent = Autocomplete.IntentBuilder(
                    AutocompleteActivityMode.FULLSCREEN, fields
                )
                .build(this)
            startActivityForResult(intent, 11)
//            val intent =
//                Intent(this, ManualBusinessAddress::class.java).putExtra(
//                    "modelStep4",
//                    modelAddressFourStep
//                )
//            startActivityForResult(intent, 1)
        }

        etDob.setOnClickListener {
            Constant.hideKeyboard(this, etDob)
            val c = Calendar.getInstance()
            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)
            val day = c.get(Calendar.DAY_OF_MONTH)

            val dpd = DatePickerDialog(
                this,
                DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
                    etDob.text = "" + year + "/" + (month + 1) + "/" + dayOfMonth
                },
                year,
                month,
                day
            )

            dpd.datePicker.maxDate = System.currentTimeMillis() - 410240376000
            dpd.show()
        }





        tvDateYear.setOnClickListener {
            val dialog = datePickerDialog()
            dialog.show()
            val year = dialog.findViewById<View>(
                Resources.getSystem().getIdentifier("android:id/day", null, null)
            )
            if (year != null) {
                year.visibility = View.GONE
            }
        }


        tVSubscription.setOnClickListener {
            flag = 0
            etRoutingNumber.setText("")
            etAccountNumber.setText("")
            confirmAccountNumber.setText("")
            etOwnersName.setText("")
            debit.visibility = View.VISIBLE
            bankAccount.visibility = View.GONE
            tVSubscription.setBackgroundResource(R.drawable.bg_cards_fill)
            textView70.setBackgroundResource(R.drawable.bg_cards_change)
            tVSubscription.setTextColor(resources.getColor(R.color.colorWhite))
            textView70.setTextColor(resources.getColor(R.color.upscribber_tv))
            Constant.hideKeyboard(this, tVSubscription)
            hideLoader()
        }

        textView70.setOnClickListener {
            flag = 1
//            card!!.number!!.toString("")
//            etCardExpireDate.setText("")
//            tvCvv.setText("")
            debit.visibility = View.GONE
            bankAccount.visibility = View.VISIBLE
            tVSubscription.setBackgroundResource(R.drawable.bg_cards_change)
            textView70.setBackgroundResource(R.drawable.bg_cards_fill)
            tVSubscription.setTextColor(resources.getColor(R.color.upscribber_tv))
            textView70.setTextColor(resources.getColor(R.color.colorWhite))
            Constant.hideKeyboard(this, tVSubscription)
            hideLoader()
        }

        startSellingButton.setOnClickListener {
            //Toast.makeText(this, "Enabled", Toast.LENGTH_SHORT).show()
            val sharedPreferences = Constant.getPrefs(this)
            val editor = sharedPreferences.edit()
            editor.putString("dashBoardClose", "Ok")
            editor.putString(Constant.type, "1")
            editor.apply()
            removeData()
            finish()
        }

        startSellingNext.setOnClickListener {
            //Toast.makeText(this, "Enabled", Toast.LENGTH_SHORT).show()
            val sharedPreferences = Constant.getPrefs(this)
            val editor = sharedPreferences.edit()
            editor.putString("dashBoardClose", "Ok")
            editor.putString(Constant.type, "1")
            editor.apply()
            removeData()
            finish()
        }


        txtProfilePicture.setOnClickListener {
            startActivityForResult(
                Intent(this, UploadBusinessLogo::class.java).putExtra(
                    "profile",
                    "profile"
                ), 910
            )
        }

        txtBusinessLogo.setOnClickListener {
            startActivityForResult(
                Intent(this, UploadBusinessLogo::class.java).putExtra(
                    "logo",
                    "logo"
                ), 901
            )
        }


        txtAboutBusiness.setOnClickListener {
            startActivityForResult(
                Intent(this, UpdateSkillsActivity::class.java)
                    .putExtra("modelProfileBusinessInfo", "modelProfileBusinessInfo"), 903
            )
        }

        txtSkills.setOnClickListener {
            startActivityForResult(
                Intent(
                    this,
                    UpdateSkillsActivity::class.java
                ).putExtra("getData", "getData"), 905
            )
        }

        addSocial.setOnClickListener {
            startActivity(
                Intent(
                    this,
                    UpdateSocialActivity::class.java
                ).putExtra("getSocial", "getSocial")
            )
        }




        phone.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                val text = phone.text.toString()
                val textLength = phone.text.length

                if (text.endsWith("-") || text.endsWith(" ") || text.endsWith(" "))
                    return
                if (textLength == 4) {
                    phone.text = StringBuilder(text).insert(text.length - 1, " ").toString()
                    (phone as EditText).setSelection(phone.text.length)
                }
                if (textLength == 8) {
                    phone.text = StringBuilder(text).insert(text.length - 1, " ").toString()
                    (phone as EditText).setSelection(phone.text.length)

                }
                phoneNumberLength = if (textLength == 12) {
                    1
                } else {
                    0
                }

            }

            override fun afterTextChanged(s: Editable) {
            }
        })

    }

    @SuppressLint("SetTextI18n")
    fun datePickerDialog(): DatePickerDialog {
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        // Date Picker Dialog
        val datePickerDialog = DatePickerDialog(
            this,
            R.style.AlertDialogCustom1,
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                // Display Selected date in textbox
                Log.e("checkDate", "${monthOfYear + 1}, $year")
                tvDateYear.setText("${monthOfYear + 1}/$year")
            },
            year,
            month,
            day

        )
//        datePickerDialog.setButton(
//            DatePickerDialog.BUTTON_POSITIVE,
//            "Ok",
//            DialogInterface.OnClickListener() { dialog, which ->
//                tvDateYear.setText("$month/$year")
//            })

        datePickerDialog.setButton(
            DatePickerDialog.BUTTON_NEGATIVE,
            null,
            null as DialogInterface.OnClickListener?
        )
        return datePickerDialog
    }

    @SuppressLint("SetTextI18n")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == 20 && data != null) {
            val address = data.getParcelableExtra<ModelAddress>("address")
            if (currentFragment == 4) {
                modelAddressFourStep = address
                if (address.suiteNumber == "") {
                    etAddress.text =
                        address.streetAddress + "," + address.city + "," + address.state + "," + address.zipCode
                } else {
                    etAddress.text =
                        address.suiteNumber + "," + address.streetAddress + "," + address.city + "," + address.state + "," + address.zipCode
                }
            } else if (currentFragment == 3) {
                modelAddress = address
                if (address.suiteNumber == "") {
                    txtAddress.text =
                        address.streetAddress + "," + address.city + "," + address.state + "," + address.zipCode
                } else {
                    txtAddress.text =
                        address.suiteNumber + "," + address.streetAddress + "," + address.city + "," + address.state + "," + address.zipCode
                }
            }
        }

        if (resultCode == 21 && data != null) {
            val address = data.getParcelableExtra<ModelAddress>("address")
            if (currentFragment == 4) {
                modelAddressFourStep = address
                if (address.suiteNumber == "") {
                    etAddress.text =
                        address.streetAddress + "," + address.city + "," + address.state + "," + address.zipCode
                } else {
                    etAddress.text =
                        address.suiteNumber + "," + address.streetAddress + "," + address.city + "," + address.state + "," + address.zipCode
                }
            } else if (currentFragment == 3) {
                modelAddress = address
                if (address.billingSuite == "") {
                    eTBillingAddress.text =
                        address.billingStreet + "," + address.billingCity + "," + address.billingState + "," + address.billingZip
                } else {
                    eTBillingAddress.text =
                        address.billingSuite + "," + address.billingCity + "," + address.billingCity + "," + address.billingState + "," + address.billingZip
                }

            }
        }

        if (requestCode == 901 && data != null) {
            txtBusinessLogo.isEnabled = false
            txtBusinessLogo.paintFlags =
                txtBusinessLogo.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
            imageView78.visibility = View.VISIBLE
            stepsCompleted.add("1")
            startSellingButton.isEnabled = stepsCompleted.size >= 5
            if (stepsCompleted.size >= 5) {
                startSellingButton.alpha = 1.0F
            } else {
                startSellingButton.alpha = 0.4F
            }
        }

        if (requestCode == 903 && data != null) {
            txtAboutBusiness.isEnabled = false
            txtAboutBusiness.paintFlags =
                txtAboutBusiness.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
            imageView79.visibility = View.VISIBLE
            stepsCompleted.add("2")
            startSellingButton.isEnabled = stepsCompleted.size >= 5
            if (stepsCompleted.size >= 5) {
                startSellingButton.alpha = 1.0F
            } else {
                startSellingButton.alpha = 0.4F
            }
        }

        if (requestCode == 905 && data != null) {
            txtSkills.isEnabled = false
            txtSkills.paintFlags =
                txtSkills.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
            imageView80.visibility = View.VISIBLE
            stepsCompleted.add("3")
            startSellingButton.isEnabled = stepsCompleted.size >= 5
            if (stepsCompleted.size >= 5) {
                startSellingButton.alpha = 1.0F
            } else {
                startSellingButton.alpha = 0.4F
            }
        }

        if (requestCode == 910 && data != null) {
            txtProfilePicture.isEnabled = false
            txtProfilePicture.paintFlags =
                txtSkills.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
            imageView82.visibility = View.VISIBLE
            stepsCompleted.add("5")
            startSellingButton.isEnabled = stepsCompleted.size >= 5
            if (stepsCompleted.size >= 5) {
                startSellingButton.alpha = 1.0F
            } else {
                startSellingButton.alpha = 0.4F
            }
        }




        if (requestCode == 11) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    try {
                        val place = Autocomplete.getPlaceFromIntent(data!!)
                        val latLng = place.latLng
                        val latitude = latLng!!.latitude
                        val longitude = latLng.longitude
                        latitudesource = latitude
                        longitudesource = longitude
                        val geocoder = Geocoder(this)
                        val addresses = latitude.let { geocoder.getFromLocation(it, longitude, 1) }
                        try {
                            if (null != addresses && addresses.isNotEmpty()) {
                                locationModel.address = addresses[0].getAddressLine(0)
                                locationModel.street = addresses[0].thoroughfare
                                locationModel.city = addresses[0].locality
                                locationModel.state = addresses[0].adminArea
                                locationModel.zipcode = addresses[0].postalCode
                                locationModel.street = addresses[0].thoroughfare
                                locationModel.latitude = latitudesource.toString()
                                locationModel.longitude = longitudesource.toString()
                                startActivityForResult(
                                    Intent(
                                        this,
                                        ChangeLocationSetingsActivity::class.java
                                    ).putExtra("seller", "seller").putExtra(
                                        "location",
                                        locationModel
                                    ),
                                    20
                                )
                            }
                        } catch (e: Exception) {
                            Toast.makeText(this, "Could not find the location!", Toast.LENGTH_SHORT)
                                .show()
                            e.printStackTrace()
                        }
                    } catch (e: Exception) {
                        Toast.makeText(this, "Could not find the location!", Toast.LENGTH_SHORT)
                            .show()

                        e.printStackTrace()
                    }

                }
            }
        }

        if (requestCode == 12) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    try {
                        val place = Autocomplete.getPlaceFromIntent(data!!)
                        val latLng = place.latLng
                        val latitude = latLng!!.latitude
                        val longitude = latLng.longitude
                        latitudesource = latitude
                        longitudesource = longitude
                        val geocoder = Geocoder(this)
                        val addresses = latitude.let { geocoder.getFromLocation(it, longitude, 1) }
                        try {
                            if (null != addresses && addresses.isNotEmpty()) {
                                locationModel.billingAddress = addresses[0].getAddressLine(0)
                                locationModel.billingStreet = addresses[0].thoroughfare
                                locationModel.billingCity = addresses[0].locality
                                locationModel.billingState = addresses[0].adminArea
                                locationModel.billingZip = addresses[0].postalCode
                                locationModel.billingLat = latitudesource.toString()
                                locationModel.billingLong = longitudesource.toString()
                                startActivityForResult(
                                    Intent(
                                        this,
                                        ChangeLocationSetingsActivity::class.java
                                    ).putExtra("sellerBilling", "seller").putExtra(
                                        "locationn",
                                        locationModel
                                    ),
                                    21
                                )
                            }
                        } catch (e: Exception) {
                            Toast.makeText(this, "Could not find the location!", Toast.LENGTH_SHORT)
                                .show()
                            e.printStackTrace()
                        }
                    } catch (e: Exception) {
                        Toast.makeText(this, "Could not find the location!", Toast.LENGTH_SHORT)
                            .show()

                        e.printStackTrace()
                    }

                }
            }
        }


    }

    private fun setAdapter() {
        recyclerSellerTwo.layoutManager = LinearLayoutManager(this)
        adapterSellerTwo = AdapterSellerTwo(this, this)
        recyclerSellerTwo.adapter = adapterSellerTwo

    }

    private fun observerInit() {
        mViewModel.getStatus().observe(this, Observer {
            hideLoader()
            if (it.msg == "Invalid auth code") {
                Constant.commonAlert(this)
            } else if (it.msg != "") {
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
            }
        })

        mViewModel.getmDataCategories().observe(this, Observer {
            hideLoader()
            if (it.size > 0) {
                selection = it
                adapterSellerTwo.update(it)
                setCategories()
            }
        })


        mViewModel.getmDataCategoriesOnly().observe(this, Observer {
            hideLoader()
            currentFragment += 1
            nextFragmentId(currentFragment)
            businessId = it.business_id

        })

        mViewModel.getmDataUpdateBusinessName().observe(this, Observer {
            val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
            mViewModel.getSecondStepCategoryApi(
                auth,
                eTLegalName.text.toString().trim(),
                etEin.text.toString().trim(),
                currentFragment.toString(), businessId
            )
        })


        mViewModel.getmDataTax().observe(this, Observer {
            hideLoader()
            currentFragment += 1
            nextFragmentId(currentFragment)
            businessId = it.business_id

        })

        mViewModel.getmDataBusinessInfo().observe(this, Observer {
            hideLoader()
            currentFragment += 1
            nextFragmentId(currentFragment)
            businessId = it.business_id

        })

        mViewModel.getmDataPersonalsInfo().observe(this, Observer {
            hideLoader()
            currentFragment += 1
            nextFragmentId(currentFragment)
            businessId = it.business_id

        })
        mViewModel.getmDataAllCards().observe(this, Observer {
            hideLoader()
            currentFragment += 1
            nextFragmentId(currentFragment)
            businessId = it.business_id

        })

        mViewModel.getmDataBankStep().observe(this, Observer {
            hideLoader()
            currentFragment += 1
            nextFragmentId(currentFragment)
            businessId = it.business_id

        })

        mViewModel.getmDataTerms().observe(this, Observer {
            hideLoader()
            currentFragment += 1
            nextFragmentId(currentFragment)
            businessId = it.business_id

        })
    }

//    fun  getLatLong() : String{
//        val addressCity = model.city
//        var lat = ""
//         var long = ""
//        if (Geocoder.isPresent()) {
//            try {
//                val location = addressCity
//                val gc = Geocoder(this)
//                val addresses = gc.getFromLocationName(location, 5) // get the found Address Objects
//
//                val ll = ArrayList<LatLng>(addresses.size) // A list to save the coordinates if they are available
//                for (a in addresses) {
//                    if (a.hasLatitude() && a.hasLongitude()) {
//                        ll.add(LatLng(a.latitude, a.longitude))
//                    }
//                }
//
//                lat = ll[0].latitude.toString()
//                long = ll[0].longitude.toString()
//            } catch (e: IOException) {
//                Toast.makeText(this,"No location found",Toast.LENGTH_LONG).show()
//            }
//
//        }
//
//        return lat + long
//    }ss

    private fun nextApis(i: Int) {
        val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
        val billingType = 0
        var payType = 1
        val addressStreet = modelAddress.streetAddress
        val billingAdress = modelAddress.billingStreet
        val addressCity = modelAddress.city
        val billingCity = modelAddress.billingCity
        val addressZip = modelAddress.zipCode
        val billingZip = modelAddress.billingZip
        val addressState = modelAddress.state
        val billingSate = modelAddress.billingState
        val latitude = modelAddress.latitude
        val billinglatitude = modelAddress.billingLat
        val longitude = modelAddress.longitude
        val billinglongitude = modelAddress.billingLong
        val suite = modelAddress.suiteNumber
        val billingSuite = modelAddress.billingSuite
        val list = adapterSellerTwo.getSelectedCategories()
//        var latLong = getLatLong()


        if (i == 1) {
            val count: Int = adapterSellerTwo.getSelectedItems()
            if (count == 0) {
                Toast.makeText(this, "Please select categories first!", Toast.LENGTH_LONG).show()
            } else {
                showLoader()
                mViewModel.getFirstStepCategoryApi(auth, list, currentFragment.toString())
            }

            return
        }
        if (i == 2) {
            if (eTLegalName.text.isEmpty() && etEin.text.isEmpty()) {
                Toast.makeText(this, "Please Enter all the details!", Toast.LENGTH_LONG).show()
            } else if (eTLegalName.text.isEmpty()) {
                Toast.makeText(this, "Please Enter business name", Toast.LENGTH_LONG).show()
            } else if (etEin.text.isEmpty()) {
                Toast.makeText(this, "Please Enter EIN", Toast.LENGTH_LONG).show()
            } else {
                showLoader()
                val prefs = Constant.getPrefs(this)
                val json = prefs.getString("step2", "")
                val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
                if (json.isNotEmpty() && eTLegalName.text.toString().trim().isNotEmpty()) {
                    mViewModel.getSecondStepCategoryApi(
                        auth,
                        eTLegalName.text.toString().trim(),
                        etEin.text.toString().trim(),
                        currentFragment.toString(),
                        businessId
                    )
                } else {
                    mViewModel.validBusinessName(eTLegalName.text.toString())
                }
            }
            return
        }

        if (i == 3) {
            val phone =
                eTPhoneNumber.text.toString().replace("(", "").replace(")", "").replace("-", "")
                    .replace(" ", "")
            if (switchBilling.isChecked) {
                if (eTPhoneNumber.text.isEmpty() || eTLegalEmail.text.isEmpty() || txtAddress.text.isEmpty()) {
                    Toast.makeText(this, "Please Enter all the details!", Toast.LENGTH_LONG).show()
                } else if (eTPhoneNumber.text.isEmpty()) {
                    Toast.makeText(this, "Please enter your phone number", Toast.LENGTH_LONG).show()
                } else if (!Constant.isValidEmailId(eTLegalEmail.text.toString())) {
                    Toast.makeText(this, "Please Enter valid Email address", Toast.LENGTH_LONG)
                        .show()
                } else if (eTLegalEmail.text.isEmpty()) {
                    Toast.makeText(this, "Please enter your Email", Toast.LENGTH_LONG).show()
                } else if (website == 1 && eTWebsite.text.toString().isEmpty()) {
                    Toast.makeText(this, "Please enter your website", Toast.LENGTH_LONG).show()
                } else if (URLUtil.isValidUrl(eTWebsite.text.toString())) {
                    Toast.makeText(this, "Please enter your valid website", Toast.LENGTH_LONG)
                        .show()
                } else if (txtAddress.text.isEmpty()) {
                    Toast.makeText(this, "Please enter your website", Toast.LENGTH_LONG).show()
                } else if (phoneNumberLength == 0) {
                    Toast.makeText(this, "Please enter your valid phone number", Toast.LENGTH_LONG)
                        .show()
                } else {
                    showLoader()
                    mViewModel.getThirdStepCategoryApi(
                        auth, eTLegalEmail.text.toString(),
                        "+1" + phone,
                        txtAddress.text.toString(),
                        eTWebsite.text.toString(),
                        currentFragment.toString(),
                        addressCity,
                        addressState,
                        addressZip,
                        addressStreet,
                        "",
                        "", "", "", billingType, "", "1", latitude, longitude, suite, businessId

                    )
                }

            } else {
                if (eTPhoneNumber.text.isEmpty() || eTLegalEmail.text.isEmpty() || eTBillingAddress.text.isEmpty()) {
                    Toast.makeText(this, "Please Enter all the details!", Toast.LENGTH_LONG).show()
                } else if (eTPhoneNumber.text.isEmpty()) {
                    Toast.makeText(this, "Please enter your phone number", Toast.LENGTH_LONG).show()
                } else if (eTLegalEmail.text.isEmpty()) {
                    Toast.makeText(this, "Please enter your email address", Toast.LENGTH_LONG)
                        .show()
                } else if (!Constant.isValidEmailId(eTLegalEmail.text.toString())) {
                    Toast.makeText(this, "Please Enter valid Email address", Toast.LENGTH_LONG)
                        .show()
                } else if (URLUtil.isValidUrl(eTWebsite.text.toString())) {
                    Toast.makeText(this, "Please enter your valid website", Toast.LENGTH_LONG)
                        .show()
                } else if (website == 1 && eTWebsite.text.toString().isEmpty()) {
                    Toast.makeText(this, "Please enter your website", Toast.LENGTH_LONG).show()
                } else if (eTBillingAddress.text.isEmpty()) {
                    Toast.makeText(this, "Please enter billing address", Toast.LENGTH_LONG).show()
                } else if (phoneNumberLength == 0) {
                    Toast.makeText(this, "Please enter your valid phone number", Toast.LENGTH_LONG)
                        .show()
                } else {
                    showLoader()
                    mViewModel.getThirdStepCategoryApi(
                        auth,
                        eTLegalEmail.text.toString(),
                        "+1" + phone,
                        txtAddress.text.toString(),
                        eTWebsite.text.toString(),
                        currentFragment.toString(),
                        addressCity,
                        addressState,
                        addressZip,
                        addressStreet,
                        billingAdress,
                        billingSate,
                        billingCity,
                        billingZip,
                        billingType,
                        eTBillingAddress.text.toString(),
                        "1",
                        billinglatitude,
                        billinglongitude,
                        suite,
                        businessId

                    )
                }
            }
            return
        }

        if (i == 4) {
            val phoneNumber = phone.text.toString().trim().replace(" ", "")
            if (eTLegalLastName.text.toString()
                    .isEmpty() && eTLegalNamePersonal.text.isEmpty() && etLastNamePersonal.text.isEmpty() && etAddress.text.isEmpty() && etDob.text.isEmpty() && ssn.text.isEmpty() && phone.text.isEmpty()
            ) {
                Toast.makeText(this, "Please Enter all the details!", Toast.LENGTH_LONG).show()
            } else if (eTLegalNamePersonal.text.isEmpty()) {
                Toast.makeText(this, "Please enter your first name", Toast.LENGTH_LONG).show()
            } else if (etLastNamePersonal.text.isEmpty()) {
                Toast.makeText(this, "Please enter your last name", Toast.LENGTH_LONG).show()
            } else if (etAddress.text.isEmpty()) {
                Toast.makeText(this, "Please enter your address", Toast.LENGTH_LONG).show()
            } else if (etDob.text.isEmpty()) {
                Toast.makeText(this, "Please enter your date of birth", Toast.LENGTH_LONG).show()
            } else if (ssn.text.isEmpty()) {
                Toast.makeText(this, "Please enter last 4 digit of your ssn", Toast.LENGTH_LONG)
                    .show()
            } else if (phone.text.isEmpty()) {
                Toast.makeText(this, "Please enter your phone number", Toast.LENGTH_LONG).show()
            } else if (eTLegalLastName.text.isEmpty()) {
                Toast.makeText(this, "Please enter your phone number", Toast.LENGTH_LONG).show()
            } else if (phoneNumberLength == 0) {
                Toast.makeText(this, "Please enter your valid phone number", Toast.LENGTH_LONG)
                    .show()
            } else {
                showLoader()
                mViewModel.getFourthStep(
                    auth,
                    currentFragment.toString(),
                    eTLegalNamePersonal.text.toString(),
                    etLastNamePersonal.text.toString(),
                    etAddress.text.toString(),
                    etDob.text.toString(),
                    ssn.text.toString(),
                    "+1" + phoneNumber,
                    modelAddressFourStep.streetAddress,
                    modelAddressFourStep.city,
                    modelAddressFourStep.zipCode,
                    modelAddressFourStep.state,
                    modelAddressFourStep.latitude,
                    modelAddressFourStep.longitude,
                    modelAddressFourStep.suiteNumber,
                    eTLegalLastName.text.toString().trim(), businessId
                )
            }
            return
        }

        if (i == 5) {
            if (flag == 0) {
                showLoader()
                payType = 1
                Constant.hideKeyboard(this, eTCardNumber1)

                if (eTCardNumber1.text.toString().trim().isEmpty() || tvDateYear.text.toString()
                        .trim().isEmpty() || eTextCvv.text.toString().trim().isEmpty()
                ) {
                    Toast.makeText(this, "PLease Enter Card Details", Toast.LENGTH_LONG).show()
                } else if (eTCardNumber1.text.toString().trim().isEmpty()) {
                    Toast.makeText(this, "PLease Enter Card Number", Toast.LENGTH_LONG).show()
                } else if (tvDateYear.text.toString().trim().isEmpty()) {
                    Toast.makeText(this, "PLease Enter Card Expiry Date", Toast.LENGTH_LONG).show()
                } else if (eTextCvv.text.toString().trim().isEmpty()) {
                    Toast.makeText(this, "PLease Enter valid cvv", Toast.LENGTH_LONG).show()
                } else {
                    val splitDate = tvDateYear.text.toString().trim().split("/")
                    mViewModel.getStripeData(
                        auth,
                        eTCardNumber1.text.toString().trim(),
                        splitDate[0],
                        splitDate[1],
                        eTextCvv.text.toString().trim(),
                        payType,
                        currentFragment.toString(),
                        businessId
                    )
                }


//                    val card = eTCardNumber.card
//                    mViewModel.getStripeData(
//                        auth,
//                        card!!.number.toString(),
//                        card.expMonth.toString(),
//                        card.expYear.toString(),
//                        card.cvc.toString()
//                        , payType, currentFragment.toString(), businessId
//                    )
            } else {

                if (etRoutingNumber.text.toString().isEmpty() && etAccountNumber.text.toString()
                        .isEmpty()
                    && etOwnersName.text.toString()
                        .isEmpty() && confirmAccountNumber.text.toString().isEmpty()
                ) {
                    Toast.makeText(this, "Please Enter all the details!", Toast.LENGTH_LONG).show()
                } else if (etRoutingNumber.text.toString().isEmpty()) {
                    Toast.makeText(this, "Please enter routing number", Toast.LENGTH_LONG).show()
                } else if (etOwnersName.text.toString().isEmpty()) {
                    Toast.makeText(this, "Please enter bank account owners name", Toast.LENGTH_LONG)
                        .show()
                } else if (etAccountNumber.text.toString().isEmpty()) {
                    Toast.makeText(this, "Please enter your account number", Toast.LENGTH_LONG)
                        .show()
                } else if (confirmAccountNumber.text.toString().isEmpty()) {
                    Toast.makeText(
                        this,
                        "Please enter your confirm account number",
                        Toast.LENGTH_LONG
                    ).show()
                } else if (etAccountNumber.text.toString() != confirmAccountNumber.text.toString()) {
                    Toast.makeText(this, "Account number does not match!", Toast.LENGTH_LONG).show()
                } else {
                    showLoader()
                    payType = 0
                    mViewModel.getStripeBankData(
                        auth,
                        etRoutingNumber.text.toString(),
                        etAccountNumber.text.toString(),
                        etOwnersName.text.toString(),
                        currentFragment.toString()
                        , payType, businessId
                    )
                }

            }
            return

        }

        if (i == 6) {
            if (!switchCompat.isChecked) {
                Toast.makeText(
                    this,
                    "Please accept Terms & Conditions to continue",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                mViewModel.getLastStep(
                    auth, currentFragment.toString(), businessId
                )
            }
        }


        if (i == 7) {
            val sharedPreferences = Constant.getPrefs(this)
            val editor = sharedPreferences.edit()
            editor.putString("dashBoardClose", "Ok")
            editor.putString(Constant.type, "1")
            editor.apply()
            finish()
        }

    }

    private fun showLoader() {
        progressBar.visibility = View.VISIBLE
    }

    private fun hideLoader() {
        progressBar.visibility = View.GONE
    }


    private fun initz() {
        toolbarSeller = findViewById(R.id.toolbarSeller)
        viewPager = findViewById(R.id.viewPager2)
        sellerNext = findViewById(R.id.sellerNext)
        mExpiryTextInputLayout = findViewById(R.id.tl_add_source_expiry_ml)
        mCvcEditText = findViewById(R.id.et_add_source_cvc_ml)
        toolbarTitle = findViewById(R.id.toolbarTitle)
        startSellingButton.isEnabled = false
        startSellingButton.alpha = 0.4F
    }


    override fun selection(position: Int) {
        val model = selection[position]
        model.status = !model.status

        selection[position] = model
        var exist = 0
        for (i in selection) {
            if (i.status) {
                exist = 1
                break
            }
        }

        if (exist == 0) {
            sellerNext.setBackgroundResource(R.drawable.bg_cards_fill_opacity)
        } else {
            sellerNext.setBackgroundResource(R.drawable.bg_cards_fill_new)
        }

        adapterSellerTwo.notifyDataSetChanged()

    }

    private fun setToolbar() {
        if (currentFragment != 6) {
            setSupportActionBar(toolbarSeller)
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)
            title = ""
        } else {
            supportActionBar!!.setDisplayHomeAsUpEnabled(false)
            supportActionBar!!.setHomeAsUpIndicator(0)
            title = ""

        }


    }


    override fun nextFragmentId(i: Int) {
        if (i == 1) {
//            toolbarTitle.text = "Category"
            sellerCategories.visibility = View.VISIBLE
            sellerTax.visibility = View.GONE
            sellerBusinessInfo.visibility = View.GONE
            sellerPersonalInfo.visibility = View.GONE
            sellerCards.visibility = View.GONE
            sellerTermsConditions.visibility = View.GONE
            sellerLearnToSell.visibility = View.GONE
            sellerWalkThrough.visibility = View.GONE
//            sellerNext.visibility = View.VISIBLE
            sellerNext.text = "Next"
            setButtonVisible(i)
            return
        }
        if (i == 2) {
//            toolbarTitle.text = "Tax"
            sellerCategories.visibility = View.GONE
            sellerTax.visibility = View.VISIBLE
            sellerBusinessInfo.visibility = View.GONE
            sellerPersonalInfo.visibility = View.GONE
            sellerCards.visibility = View.GONE
            sellerTermsConditions.visibility = View.GONE
            sellerLearnToSell.visibility = View.GONE
            sellerWalkThrough.visibility = View.GONE
//            sellerNext.visibility = View.VISIBLE
            sellerNext.text = "Next"
            setButtonVisible(i)
            return

        }
        if (currentFragment == 3) {
//            toolbarTitle.text = "Info"
            sellerCategories.visibility = View.GONE
            sellerTax.visibility = View.GONE
            sellerBusinessInfo.visibility = View.VISIBLE
            sellerPersonalInfo.visibility = View.GONE
            sellerCards.visibility = View.GONE
            sellerTermsConditions.visibility = View.GONE
            sellerLearnToSell.visibility = View.GONE
            sellerWalkThrough.visibility = View.GONE
//            sellerNext.visibility = View.VISIBLE
            sellerNext.text = "Next"
            setButtonVisible(i)
            return
        }
        if (currentFragment == 4) {
//            toolbarTitle.text = "Personal Info"
            sellerCategories.visibility = View.GONE
            sellerTax.visibility = View.GONE
            sellerBusinessInfo.visibility = View.GONE
            sellerPersonalInfo.visibility = View.VISIBLE
            sellerCards.visibility = View.GONE
            sellerTermsConditions.visibility = View.GONE
            sellerLearnToSell.visibility = View.GONE
            sellerWalkThrough.visibility = View.GONE
//            sellerNext.visibility = View.VISIBLE
            sellerNext.text = "Next"
            setButtonVisible(i)
            return
        }
        if (currentFragment == 5) {
//            toolbarTitle.text = "Payment"
            sellerCategories.visibility = View.GONE
            sellerTax.visibility = View.GONE
            sellerBusinessInfo.visibility = View.GONE
            sellerPersonalInfo.visibility = View.GONE
            sellerCards.visibility = View.VISIBLE
            sellerTermsConditions.visibility = View.GONE
            sellerLearnToSell.visibility = View.GONE
            sellerWalkThrough.visibility = View.GONE
//            sellerNext.visibility = View.VISIBLE
            sellerNext.text = "Next"
            setButtonVisible(i)
            return
        }
        if (currentFragment == 6) {
//            toolbarTitle.text = "User Agreement"
            sellerCategories.visibility = View.GONE
            sellerTax.visibility = View.GONE
            sellerBusinessInfo.visibility = View.GONE
            sellerPersonalInfo.visibility = View.GONE
            sellerCards.visibility = View.GONE
            sellerTermsConditions.visibility = View.VISIBLE
            sellerLearnToSell.visibility = View.GONE
            sellerWalkThrough.visibility = View.GONE
//            sellerNext.visibility = View.VISIBLE
            sellerNext.text = "Next"
            setButtonVisible(i)
            return
        }
        if (currentFragment == 7) {
//            toolbarTitle.text = ""
            sellerCategories.visibility = View.GONE
            sellerTax.visibility = View.GONE
            sellerBusinessInfo.visibility = View.GONE
            sellerPersonalInfo.visibility = View.GONE
            sellerCards.visibility = View.GONE
            sellerTermsConditions.visibility = View.GONE
            sellerLearnToSell.visibility = View.GONE
            sellerWalkThrough.visibility = View.VISIBLE
//
            sellerNext.text = "Skip"
            sellerNext.setBackgroundResource(0)
            sellerNext.setTextColor(resources.getColor(R.color.colorHeading))
            return
        }

    }


    fun onNextClick(v: View) {
        Constant.hideKeyboard(this, eTLegalNamePersonal)
        nextApis(currentFragment)

    }

    override fun getIsBussiness(): Int {
        return currentFragment
    }

    override fun individualFragment() {
        sellerNext.visibility = View.GONE

    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        Constant.hideKeyboard(this, eTLegalNamePersonal)
        if (currentFragment != 6) {
            if (item!!.itemId == android.R.id.home) {
                onBackPressed()
//            fragmentTransaction.remove(fragment)
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        if (currentFragment == 1) {
            finish()
        } else {
            if (currentFragment != 6) {
                currentFragment -= 1
                nextFragmentId(currentFragment)
            }

        }
    }


    override fun getFragments(i: Int) {


    }
}




