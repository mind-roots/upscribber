package com.upscribber.becomeseller.seller

import android.os.Parcel
import android.os.Parcelable

class ModelAddress() : Parcelable {

    var billingLong: String = ""
    var billingLat: String = ""
    var value: String = ""
    var userId: String = ""
    var billingSuite : String = ""
    var billingStreet: String = ""
    var billingState: String = ""
    var billingCity: String = ""
    var billingZip: String = ""
    var streetAddress: String = ""
    var city: String = ""
    var state: String = ""
    var zipCode: String = ""
    var latitude: String = ""
    var longitude: String = ""
    var suiteNumber: String = ""
    var address: String = ""

    constructor(parcel: Parcel) : this() {
        billingLong = parcel.readString()
        billingLat = parcel.readString()
        value = parcel.readString()
        userId = parcel.readString()
        billingSuite = parcel.readString()
        billingStreet = parcel.readString()
        billingState = parcel.readString()
        billingCity = parcel.readString()
        billingZip = parcel.readString()
        streetAddress = parcel.readString()
        city = parcel.readString()
        state = parcel.readString()
        zipCode = parcel.readString()
        latitude = parcel.readString()
        longitude = parcel.readString()
        suiteNumber = parcel.readString()
        address = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(billingLong)
        parcel.writeString(billingLat)
        parcel.writeString(value)
        parcel.writeString(userId)
        parcel.writeString(billingSuite)
        parcel.writeString(billingStreet)
        parcel.writeString(billingState)
        parcel.writeString(billingCity)
        parcel.writeString(billingZip)
        parcel.writeString(streetAddress)
        parcel.writeString(city)
        parcel.writeString(state)
        parcel.writeString(zipCode)
        parcel.writeString(latitude)
        parcel.writeString(longitude)
        parcel.writeString(suiteNumber)
        parcel.writeString(address)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ModelAddress> {
        override fun createFromParcel(parcel: Parcel): ModelAddress {
            return ModelAddress(parcel)
        }

        override fun newArray(size: Int): Array<ModelAddress?> {
            return arrayOfNulls(size)
        }
    }


}
