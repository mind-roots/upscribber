package com.upscribber.becomeseller.sellerWelcomeScreens

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

class sellerPagerAdapter(mcontext: Context, fm: FragmentManager) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        when (position) {
            0 -> return BecomeSellerFrg1()
            1 -> return BecomeSellerFrg2()
            2 -> return BecomeSellerFrg3()
            3 -> return BecomeSellerFrg4()
            4 -> return BecomeSellerFrg5()
        }
        return null!!
    }

    override fun getCount(): Int {
        return 5
    }

}