package com.upscribber.becomeseller.sellerWelcomeScreens

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.upscribber.R
import kotlinx.android.synthetic.main.fragment_seller1.*

class BecomeSellerFrg1 : Fragment() {

    lateinit var imageView : ImageView


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_seller1, container, false)
        imageView = view.findViewById(R.id.imageView70)
        Glide.with(this).load(R.drawable.gif_seller1)
            .into(imageView)
        return view
    }
}