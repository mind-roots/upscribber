package com.upscribber.becomeseller.sellerWelcomeScreens

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.TypedValue
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.viewpager.widget.ViewPager
import com.upscribber.becomeseller.seller.BecomeSeller
import com.upscribber.R
import com.upscribber.becomeseller.individual.IndividualMainLayout
import com.upscribber.notification.ModelNotificationCustomer
import me.relex.circleindicator.CircleIndicator
import me.relex.circleindicator.Config

class sellerWelcomActivity : AppCompatActivity() {

    private lateinit var toolbarWelcomeSeller : Toolbar
    lateinit var skip : TextView
    lateinit var viewPagerSeller : ViewPager
    lateinit var indicatorSeller : CircleIndicator
    lateinit var mCardAdapter : sellerPagerAdapter
    lateinit var titletoolbar : TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_seller_welcom)

        initz()
        setToolbar()
        clickEvents()
        setAdapter()
    }

    private fun setAdapter() {
        mCardAdapter = sellerPagerAdapter(this,supportFragmentManager)
        viewPagerSeller.adapter = mCardAdapter

        val indicatorWidth = (TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, 10f,
            resources.displayMetrics
        ) + 0.5f).toInt()
        val indicatorHeight = (TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, 4f,
            resources.displayMetrics
        ) + 0.5f).toInt()
        val indicatorMargin = (TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP, 6f,
            resources.displayMetrics
        ) + 0.5f).toInt()


        val config = Config.Builder().width(indicatorWidth)
            .height(indicatorHeight)
            .margin(indicatorMargin)
            .drawable(R.drawable.black_radius_square)
            .drawableUnselected(R.drawable.black_radius_square)
            .build()
        indicatorSeller.initialize(config)

        viewPagerSeller.adapter = sellerPagerAdapter(this,supportFragmentManager)
        indicatorSeller.setViewPager(viewPagerSeller)
    }

    private fun setToolbar() {
        setSupportActionBar(toolbarWelcomeSeller)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)
        title = ""

        viewPagerSeller.setOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {


            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
                when (position) {
                    0 -> {
                        titletoolbar.text ="Become a Seller"

                    }
                    1 -> {
                        titletoolbar.text ="How it Works"
                    }
                    2 -> {
                        titletoolbar.text ="Increase Loyalty"
                    }
                    3 -> {
                        titletoolbar.text ="Smart Features"

                    }
                    else -> {
                        titletoolbar.text ="Control Everything"
                    }
                }

            }

            override fun onPageSelected(position: Int) {

            }

        }

        )

    }



    private fun initz() {
        toolbarWelcomeSeller = findViewById(R.id.toolbarWelcomeSeller)
        skip = findViewById(R.id.skip)
        indicatorSeller = findViewById(R.id.indicatorSeller)
        viewPagerSeller = findViewById(R.id.viewPagerSeller)
        titletoolbar = findViewById(R.id.title)

    }

    var individualKey = ""

    private fun clickEvents() {

        skip.setOnClickListener {
            if (intent.getStringExtra("fromNotification") == "fromNotification") {
                startActivity(Intent(this, IndividualMainLayout::class.java).putExtra("model",intent.getParcelableExtra<ModelNotificationCustomer>("model")))
            }else{
                startActivity(Intent(this, BecomeSeller::class.java))
            }
            finish()
        }

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }


    fun goToMain(view: View){
        if (intent.getStringExtra("fromNotification") == "fromNotification") {
            startActivity(Intent(this, IndividualMainLayout::class.java).
                putExtra("model",intent.getParcelableExtra<ModelNotificationCustomer>("model"))
//                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            )
        }else{
            startActivity(Intent(this, BecomeSeller::class.java))
        }
        finish()
    }

}
