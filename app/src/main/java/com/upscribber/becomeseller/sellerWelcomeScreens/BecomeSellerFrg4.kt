package com.upscribber.becomeseller.sellerWelcomeScreens

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.upscribber.R

class BecomeSellerFrg4 : Fragment() {

    lateinit var imageView : ImageView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = inflater.inflate(R.layout.fragment_seller4, container, false)
        imageView = view.findViewById(R.id.imageView75)
        Glide.with(this).load(R.drawable.gif_seller4)
            .into(imageView)
        return view
    }
}