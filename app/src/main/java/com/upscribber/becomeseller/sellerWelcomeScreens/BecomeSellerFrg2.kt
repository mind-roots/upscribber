package com.upscribber.becomeseller.sellerWelcomeScreens

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.upscribber.R

class BecomeSellerFrg2: Fragment() {

    lateinit var imageSeller : ImageView


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_seller2, container, false)
        imageSeller = view.findViewById(R.id.imageSeller)
        Glide.with(this).load(R.drawable.gif_seller2)
            .into(imageSeller)
        return view
    }
}