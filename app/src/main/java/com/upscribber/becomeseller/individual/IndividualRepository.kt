package com.upscribber.becomeseller.individual

import android.app.Application
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.upscribber.becomeseller.seller.ModelBecomeSeller
import com.upscribber.commonClasses.Constant
import com.upscribber.commonClasses.ModelStatusMsg
import com.upscribber.commonClasses.WebServicesCustomers
import com.upscribber.commonClasses.WebServicesMerchants
import com.upscribber.notification.ModelNotificationCustomer
import com.upscribber.payment.paymentCards.PaymentCardModel
import com.upscribber.profile.ModelGetProfile
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import java.io.File

class IndividualRepository(var application: Application) {

    private val mDataStepOneIndividual = MutableLiveData<ModelBecomeSeller>()
    private val mDataStepTaxIndividual = MutableLiveData<ModelBecomeSeller>()
    private val mDataDetail = MutableLiveData<ModelBecomeSeller>()
    private val mDataBankStep = MutableLiveData<ModelBecomeSeller>()
    private val mDataTerms = MutableLiveData<ModelBecomeSeller>()
    private val mDataRejectAssociated = MutableLiveData<ModelNotificationCustomer>()
    private val mDataRelink = MutableLiveData<ModelNotificationCustomer>()
    val mDataFailure = MutableLiveData<ModelStatusMsg>()
    private lateinit var arrayHomeProfile: ModelGetProfile

    fun getFirstStepIndividual(
        auth: String,
        legalName: String,
        email: String,
        phone: String,
        address: String,
        step: String,
        addressStreet: String,
        addressCity: String,
        addressZip: String,
        addressState: String,
        billingType: Int,
        businessId: String,
        id: String,
        firstName: String,
        lastName: String,
        latitude: String,
        longitude: String,
        suiteNumber: String
    ) {

        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesMerchants::class.java)
        val call: Call<ResponseBody> = service.merchantSignupThird(
            auth,
            email,
            phone,
            address,
            legalName,
            step,
            addressCity,
            addressState,
            addressZip,
            addressStreet,
            addressStreet,
            addressState,
            addressCity,
            addressZip,
            billingType.toString(),
            address,
            "2",
            businessId, id,firstName,lastName,latitude,longitude,suiteNumber
        )
        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        val modelBecomeSeller = ModelBecomeSeller()
                        if (status == "true") {
                            val data = json.getJSONObject("data")
                            modelBecomeSeller.business_id = data.optString("business_id")
                            modelBecomeSeller.birthdate = data.optString("birthdate")
                            modelBecomeSeller.status = status
                            modelBecomeSeller.msg = msg
                            modelStatus.status = status
                            modelStatus.msg = msg
                            mDataStepOneIndividual.value = modelBecomeSeller
                        } else {
                            modelStatus.status = "false"
                            modelStatus.msg = msg
                            mDataFailure.value = modelStatus
                        }


                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus

            }
        })
    }

    fun getmDataPersonalsInfo(): LiveData<ModelBecomeSeller> {
        return mDataStepOneIndividual
    }


    fun getmDataFailure(): LiveData<ModelStatusMsg> {
        return mDataFailure
    }

    fun getStepTax(
        auth: String,
        ssn: String,
        birthdate: String,
        steps: String,
        businessId: String
    ) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesMerchants::class.java)
        val call: Call<ResponseBody> = service.merchantSignupTax(auth, ssn, birthdate, "2", steps, businessId)
        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        val modelBecomeSeller = ModelBecomeSeller()
                        if (status == "true") {
                            val data = json.getJSONObject("data")
                            modelBecomeSeller.business_id = data.optString("business_id")
                            modelBecomeSeller.birthdate = data.optString("birthdate")
                            modelBecomeSeller.status = status
                            modelBecomeSeller.msg = msg
                            modelStatus.status = status
                            modelStatus.msg = msg
                            mDataStepTaxIndividual.value = modelBecomeSeller
                        } else {
                            modelStatus.status = "false"
                            modelStatus.msg = msg
                            mDataFailure.value = modelStatus
                        }


                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus

            }
        })
    }

    fun getmDataTaxInfo(): LiveData<ModelBecomeSeller> {
        return mDataStepTaxIndividual
    }

    fun getStripeData(
        auth: String,
        number: String,
        expMonth: String,
        expYear: String,
        cvc: String,
        payType: Int,
        step: String,
        businessId: String
    ) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.stripeTokenUrl)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.tokensMerchant(number, expMonth, expYear, cvc, "USD")
        call.enqueue(object : Callback<ResponseBody> {


            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val cardModel = PaymentCardModel()
                        val id = json.optString("id")
                        cardModel.id = id

                        getPaymentStripe(auth, id, payType, step, businessId)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Toast.makeText(application, "Error!", Toast.LENGTH_LONG).show()
            }
        })
    }

    private fun getPaymentStripe(auth: String, stripe_token: String, payType: Int, step: String, businessId: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesMerchants::class.java)
        val call: Call<ResponseBody> = service.merchantSignupStripe(
            auth,
            stripe_token, payType.toString(), step,
            "2", businessId
        )
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        val modelBecomeSeller = ModelBecomeSeller()
                        if (status == "true") {
                            val data = json.getJSONObject("data")
                            modelBecomeSeller.business_id = data.optString("business_id")
                            modelBecomeSeller.status = status
                            modelBecomeSeller.msg = msg
                            modelStatus.status = status
                            modelStatus.msg = msg
                            mDataDetail.value = modelBecomeSeller
                        } else {
                            modelStatus.status = "false"
                            modelStatus.msg = msg
                            mDataFailure.value = modelStatus
                        }


                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg ="Network Error!"
                mDataFailure.value = modelStatus

            }
        })
    }

    fun getmDataDetailInfo(): LiveData<ModelBecomeSeller> {
        return mDataDetail
    }

    fun getStripeBankData(
        auth: String,
        routingNumber: String,
        accountNumber: String,
        ownersName: String,
        steps: String,
        payType: Int,
        businessId: String
    ) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.stripeTokenUrl)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.tokensBank(accountNumber, routingNumber, ownersName, "US", "USD")
        call.enqueue(object : Callback<ResponseBody> {


            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val cardModel = PaymentCardModel()
                        val id = json.optString("id")
                        cardModel.id = id
                        arrayHomeProfile = Constant.getArrayListProfile(application, Constant.dataProfile)

                        getCustomerStripeBank(
                            auth,
                            routingNumber, accountNumber, ownersName,
                            steps,
                            payType, businessId
                        )

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }

            }

            private fun getCustomerStripeBank(
                auth: String,
                routingNumber: String,
                accountNumber: String,
                ownersName: String,
                steps: String,
                payType: Int,
                businessId: String
            ) {

                val retrofit = Retrofit.Builder()
                    .baseUrl(Constant.BASE_URL)
                    .build()

                arrayHomeProfile = Constant.getArrayListProfile(application, Constant.dataProfile)

                val service = retrofit.create(WebServicesMerchants::class.java)
                val call: Call<ResponseBody> = service.merchantSignuBank(
                    auth,
                    accountNumber, routingNumber, ownersName,
                    steps, payType.toString()
                    , "2", businessId
                )
                call.enqueue(object : Callback<ResponseBody> {

                    override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                        if (response.isSuccessful) {
                            try {
                                val res = response.body()!!.string()
                                val json = JSONObject(res)
                                val status = json.optString("status")
                                val msg = json.optString("msg")
                                val modelStatus = ModelStatusMsg()
                                val modelBecomeSeller = ModelBecomeSeller()
                                if (status == "true") {
                                    val data = json.getJSONObject("data")
                                    modelBecomeSeller.business_id = data.optString("business_id")
                                    modelBecomeSeller.status = status
                                    modelBecomeSeller.msg = msg
                                    modelStatus.status = status
                                    modelStatus.msg = msg
                                    mDataBankStep.value = modelBecomeSeller
                                } else {
                                    modelStatus.status = "false"
                                    modelStatus.msg = msg
                                    mDataFailure.value = modelStatus
                                }


                            } catch (e: Exception) {
                                e.printStackTrace()
                            }
                        }
                    }

                    override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                        val modelStatus = ModelStatusMsg()
                        modelStatus.status = "false"
                        modelStatus.msg = "Network Error!"
                        mDataFailure.value = modelStatus

                    }
                })
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Toast.makeText(application, "Error!", Toast.LENGTH_LONG).show()
            }
        })
    }

    fun getmDataBankStep(): LiveData<ModelBecomeSeller> {
        return mDataBankStep
    }

    fun getTermsSteps(
        auth: String,
        steps: String,
        businessId: String,
        id: String
    ) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesMerchants::class.java)
        val call: Call<ResponseBody> = service.merchantSignuTerms(auth,steps,"2",businessId, id)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        val modelBecomeSeller = ModelBecomeSeller()
                        if (status == "true") {
                            val data = json.getJSONObject("data")
                            modelBecomeSeller.business_id = data.optString("business_id")
                            modelBecomeSeller.status = status
                            modelBecomeSeller.msg = msg
                            modelStatus.status = status
                            modelStatus.msg = msg
                            mDataTerms.value = modelBecomeSeller
                        } else {
                            modelStatus.status = "false"
                            modelStatus.msg = msg
                            mDataFailure.value = modelStatus
                        }


                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg ="Network Error!"
                mDataFailure.value = modelStatus

            }
        })
    }


    fun getmDataTermsInfo(): LiveData<ModelBecomeSeller> {
        return mDataTerms
    }


    fun getmDataRejectAssociated(): LiveData<ModelNotificationCustomer> {
        return mDataRejectAssociated
    }

    fun getmDataRelink(): LiveData<ModelNotificationCustomer> {
        return mDataRelink
    }


    fun rejectAssociatedRequest(
        auth: String,
        businessId: String,
        businessName: String,
        id: String
    ) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesMerchants::class.java)
        val call: Call<ResponseBody> = service.rejectAssociationRequest(auth,businessId, id)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        val modelBecomeSeller = ModelNotificationCustomer()
                        if (status == "true") {
                            modelBecomeSeller.business_name = businessName
                            modelStatus.status = status
                            modelStatus.msg = msg
                            modelBecomeSeller.status = status
                            mDataRejectAssociated.value = modelBecomeSeller
                        } else {
                            modelStatus.status = "false"
                            modelStatus.msg = msg
                            mDataFailure.value = modelStatus
                        }


                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg ="Network Error!"
                mDataFailure.value = modelStatus

            }
        })

    }

    fun sendStaffId(
        auth: String,
        staffId: String,
        business_id: String,
        id: String,
        businessName: String
    ) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesMerchants::class.java)
        val call: Call<ResponseBody> = service.relinkStaff(auth,staffId, business_id,id)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        val modelBecomeSeller = ModelNotificationCustomer()
                        if (status == "true") {
                            modelBecomeSeller.msg = msg
                            modelBecomeSeller.status = status
                            modelBecomeSeller.business_name = businessName
                            mDataRelink.value = modelBecomeSeller
                        } else {
                            modelStatus.status = "false"
                            modelStatus.msg = msg
                            mDataFailure.value = modelStatus
                        }


                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg ="Network Error!"
                mDataFailure.value = modelStatus

            }
        })


    }

    fun uploadDocumentStripe(auth: String, fileFront : File, fileBack : File) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val front = fileFront
        val back  = fileBack

        val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), front)
        val requestFileBack = RequestBody.create(MediaType.parse("multipart/form-data"), back)
        val bodyFront = MultipartBody.Part.createFormData("profile_image", front.name, requestFile)
        val bodyBack = MultipartBody.Part.createFormData("profile_image", back.name, requestFileBack)
        val authPart = RequestBody.create(MediaType.parse("multipart/form-data"), auth)

        val service = retrofit.create(WebServicesMerchants::class.java)
        val call: Call<ResponseBody> = service.uploadDocumentStripe(authPart,bodyFront, bodyBack)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        val modelBecomeSeller = ModelNotificationCustomer()
                        if (status == "true") {
                            modelBecomeSeller.msg = msg
                            modelBecomeSeller.status = status
                            mDataRelink.value = modelBecomeSeller
                        } else {
                            modelStatus.status = "false"
                            modelStatus.msg = msg
                            mDataFailure.value = modelStatus
                        }


                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg ="Network Error!"
                mDataFailure.value = modelStatus

            }
        })

    }


}