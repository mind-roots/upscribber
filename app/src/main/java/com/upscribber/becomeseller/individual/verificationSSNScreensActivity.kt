package com.upscribber.becomeseller.individual

import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.makeramen.roundedimageview.RoundedImageView
import com.upscribber.commonClasses.Constant
import com.upscribber.R
import com.upscribber.databinding.ActivityVerificationSsnscreensBinding
import com.upscribber.commonClasses.RoundedBottomSheetDialogFragment
import java.io.File
import java.lang.Exception

class verificationSSNScreensActivity : AppCompatActivity() {

    lateinit var mBinding: ActivityVerificationSsnscreensBinding
    var status: Int = 0
    var fileFront : File? = null
    var fileBack : File? = null
    lateinit var mViewModel : IndividualViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(
            this@verificationSSNScreensActivity,
            R.layout.activity_verification_ssnscreens
        )
        mViewModel = ViewModelProviders.of(this)[IndividualViewModel::class.java]
        setToolbar()
        clickListeners()
    }

    private fun setToolbar() {
        setSupportActionBar(mBinding.include15.toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)
        title = ""

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun clickListeners() {
        mBinding.image1.setOnClickListener {
            val fragment = MenuFragment(mBinding.image1, mBinding.camera1,1)
            fragment.show(supportFragmentManager, fragment.tag)
        }

        mBinding.image2.setOnClickListener {
            val fragment = MenuFragment(mBinding.image2, mBinding.camera2,2)
            fragment.show(supportFragmentManager, fragment.tag)
        }

        mBinding.startSelling.setOnClickListener {
            if (fileFront == null || fileBack == null){
                Toast.makeText(this,"Please upload document to continue",Toast.LENGTH_SHORT).show()
            }else{
                val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
                mViewModel.uploadDocumentStripe(auth, fileFront!!, fileBack!!)
            }

        }

    }

    override fun onResume() {
        super.onResume()
        if (fileFront == null || fileBack == null){
            mBinding.startSelling.setBackgroundResource(R.drawable.invite_button_background_opacity)
        }else{
            mBinding.startSelling.setBackgroundResource(R.drawable.invite_button_background)
        }
    }

    class MenuFragment(
        var image : RoundedImageView,
        var cameraPic : ImageView,
        var type: Int
    ) : RoundedBottomSheetDialogFragment() {

        var file: File? = null

        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val view = inflater.inflate(R.layout.change_profile_pic_layout, container, false)
            val isSeller = Constant.getSharedPrefs(context!!).getBoolean(Constant.IS_SELLER, false)
            val cancelDialog = view.findViewById<TextView>(R.id.cancelDialog)
            val takePicture = view.findViewById<LinearLayout>(R.id.takePicture)
            val galleryLayout = view.findViewById<LinearLayout>(R.id.galleryLayout)

            if (isSeller) {
                cancelDialog.setBackgroundResource(R.drawable.bg_seller_button_blue)
            } else {
                cancelDialog.setBackgroundResource(R.drawable.invite_button_background)
            }


            takePicture.setOnClickListener {
                val checkSelfPermission =
                    ContextCompat.checkSelfPermission(
                        this.activity!!,
                        android.Manifest.permission.CAMERA
                    )
                if (checkSelfPermission != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(
                        this.activity!!,
                        arrayOf(android.Manifest.permission.CAMERA),
                        2
                    )
                } else {
                    openCamera()
                }

            }

            galleryLayout.setOnClickListener {
                val checkSelfPermission =
                    ContextCompat.checkSelfPermission(
                        this.activity!!,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE
                    )
                if (checkSelfPermission != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(
                        this.activity!!,
                        arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE),
                        1
                    )
                } else {
                    openAlbum()
                    // dismiss()
                }
            }

            cancelDialog.setOnClickListener {
                dismiss()
            }

            return view
        }

        private fun openCamera() {
            try {
                val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                startActivityForResult(intent, 1)
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }

        private fun openAlbum() {
            val intent = Intent(Intent.ACTION_PICK)
            intent.type = "image/*"
            startActivityForResult(intent, 2)

        }


        override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

            super.onActivityResult(requestCode, resultCode, data)
            try {
                if (requestCode == 1) {
                    try {
                        if (data != null) {
                            val bitmap = data.extras.get("data") as Bitmap
                            file = Constant.getImageUri(bitmap)
                            (activity as verificationSSNScreensActivity).getfile(file!!,type)
                            if (type == 1){
                                image.setImageBitmap(bitmap)
                            }else{
                                image.setImageBitmap(bitmap)
                            }
                            cameraPic.visibility = View.GONE
                        }
                        dismiss()
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                } else if (requestCode == 2) {
                    try {
                        val selectedImage: Uri = data!!.data
                        file = File(getPath(selectedImage))
                        (activity as verificationSSNScreensActivity).getfile(file!!,type)
                        val bitmap = MediaStore.Images.Media.getBitmap(
                            activity!!.contentResolver,
                            selectedImage
                        )
                        try {
                            if (file != null) {
                                if (type == 1){
                                    image.setImageBitmap(bitmap)
                                }else{
                                    image.setImageBitmap(bitmap)
                                }
                                cameraPic.visibility = View.GONE
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }

                        dismiss()
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
            dismiss()
        }

        fun getPath(uri: Uri): String {
            val projection = arrayOf(MediaStore.Images.Media.DATA)
            val cursor =
                activity!!.contentResolver.query(uri, projection, null, null, null) ?: return ""
            val column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
            cursor.moveToFirst()
            val s = cursor.getString(column_index)
            cursor.close()
            return s
        }

    }

    private fun getfile(file1: File, type: Int) {
        if (type == 1){
            fileFront = file1
        }else{
            fileBack = file1
        }



    }


}
