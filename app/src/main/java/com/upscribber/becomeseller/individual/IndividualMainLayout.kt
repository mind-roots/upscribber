package com.upscribber.becomeseller.individual

import android.annotation.SuppressLint
import android.app.Activity
import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.location.Geocoder
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.*
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.upscribber.R
import com.upscribber.becomeseller.seller.InterfaceListener
import com.upscribber.becomeseller.seller.ModelAddress
import com.upscribber.businessAddress.ManualBusinessAddress
import com.upscribber.commonClasses.Constant
import com.upscribber.filters.LocationModel
import com.upscribber.home.ModelSteps
import com.upscribber.notification.ModelNotificationCustomer
import com.upscribber.payment.paymentCards.WebPagesOpenActivity
import com.upscribber.profile.ChangeLocationSetingsActivity
import kotlinx.android.synthetic.main.activity_individual_main_layout.*
import kotlinx.android.synthetic.main.activity_individual_main_layout.sellerLearnToSell
import kotlinx.android.synthetic.main.activity_individual_main_layout.toolbarSeller
import kotlinx.android.synthetic.main.fragment_individual_one.*
import kotlinx.android.synthetic.main.fragment_individual_one.imageView
import kotlinx.android.synthetic.main.fragment_personal_info.*
import kotlinx.android.synthetic.main.fragment_personal_info.eTBillingAddress
import kotlinx.android.synthetic.main.fragment_personal_info.eTLegalEmail
import kotlinx.android.synthetic.main.fragment_personal_info.eTLegalName
import kotlinx.android.synthetic.main.fragment_personal_info.textBilling
import kotlinx.android.synthetic.main.fragment_seller_add_cards.*
import kotlinx.android.synthetic.main.fragment_seller_terms_condition.*
import kotlinx.android.synthetic.main.fragment_seller_three.*
import kotlinx.android.synthetic.main.fragment_tax_info.*
import kotlinx.android.synthetic.main.fragment_tax_info.eTBirthdate
import java.util.*

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS",
    "RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS"
)
class IndividualMainLayout : AppCompatActivity(), InterfaceListener {

    var currentFragment: Int = 0
    var flag = 0
    var status = 0
    var statusSSN = 0
    var modelAddress: ModelAddress = ModelAddress()
    lateinit var mViewModel: IndividualViewModel
    val locationModel = LocationModel()
    var latitudesource = 1.0
    var longitudesource = 1.0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_individual_main_layout)
        mViewModel = ViewModelProviders.of(this)[IndividualViewModel::class.java]
        Places.initialize(applicationContext, resources.getString(R.string.google_maps_key))
        setToolbar()
        setData()
        clickListeners()
        textView65.loadUrl("file:///android_asset/terms.html")

        val homeProfile = Constant.getArrayListProfile(this, Constant.dataProfile)
        if (homeProfile.type != "2" || homeProfile.steps == null) {
            currentFragment = 0
        }
//        var stepVal = 0
//        if (homeProfile.type == "2") {
//            stepVal = homeProfile.steps.toInt()
//        }

        currentFragment =  1
        eTPhone.text = Constant.formatPhoneNumber(homeProfile.contact_no)
        removeData()
        nextFragmentId(currentFragment)
        observerInit()
        spanableText()

    }

    private fun spanableText() {
        val spannableString = SpannableString("By Registering your account,you agree to our Services Agreement and the Stripe connected account")
        spannableString.setSpan(clickableSpan, 45, 63, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        spannableString.setSpan(ForegroundColorSpan(Color.BLACK), 45, 63, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        textTax.text = spannableString
        textPersonal.text = spannableString
        textSpanDebit.text = spannableString

        textTax.movementMethod = LinkMovementMethod.getInstance()
        textPersonal.movementMethod = LinkMovementMethod.getInstance()
        textSpanDebit.movementMethod = LinkMovementMethod.getInstance()

        spannableString.setSpan(clickableSpanServices, 72, 96, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        spannableString.setSpan(ForegroundColorSpan(Color.BLACK), 72, 96, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        textTax.text = spannableString
        textPersonal.text = spannableString
        textSpanDebit.text = spannableString

    }


    val clickableSpan = object : ClickableSpan() {
        @SuppressLint("SetJavaScriptEnabled")
        override fun onClick(textView: View) {
            startActivity(Intent(this@IndividualMainLayout, WebPagesOpenActivity::class.java).putExtra("services","services"))
        }

        override fun updateDrawState(ds: TextPaint) {
            super.updateDrawState(ds)
            ds.isUnderlineText = false
        }
    }

    val clickableSpanServices = object : ClickableSpan() {
        @SuppressLint("SetJavaScriptEnabled")
        override fun onClick(textView: View) {
            startActivity(Intent(this@IndividualMainLayout, WebPagesOpenActivity::class.java).putExtra("legal","legal"))
        }

        override fun updateDrawState(ds: TextPaint) {
            super.updateDrawState(ds)
            ds.isUnderlineText = false
        }
    }

    private fun observerInit() {
        mViewModel.getStatus().observe(this, Observer {
            progressBar.visibility = View.GONE
            if (it.msg == "Invalid auth code") {
                Constant.commonAlert(this)
            } else {
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
            }
        })


        mViewModel.getmDataPersonalsInfo().observe(this, Observer {
            //   hideLoader()
            progressBar.visibility = View.GONE
            currentFragment += 1
            nextFragmentId(currentFragment)

        })

        mViewModel.getmDataTaxInfo().observe(this, Observer {
            progressBar.visibility = View.GONE
            currentFragment += 1
            nextFragmentId(currentFragment)
        })

        mViewModel.getmDataDetailInfo().observe(this, Observer {
            progressBar.visibility = View.GONE
            currentFragment += 1
            nextFragmentId(currentFragment)
        })

        mViewModel.getmDataBankStep().observe(this, Observer {
            progressBar.visibility = View.GONE
            currentFragment += 1
            nextFragmentId(currentFragment)
        })

        mViewModel.getmDataTermsInfo().observe(this, Observer {
            progressBar.visibility = View.GONE
            val sharedPreferences = Constant.getPrefs(this)
            val editor = sharedPreferences.edit()
            editor.putString("dashBoardClose", "Ok")
            editor.putString(Constant.type, "2")
            editor.putString("individual", "individual")
            editor.apply()
            removeData()
            finish()
        })


    }

    private fun clickListeners() {
        searchAddressIndividual.setOnClickListener {
            Constant.hideKeyboard(this, searchAddressIndividual)
            Places.createClient(this)
            val fields =
                listOf(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG)
            // Start the autocomplete intent.
            val intent = Autocomplete.IntentBuilder(
                AutocompleteActivityMode.FULLSCREEN, fields
            )
                .build(this)
            startActivityForResult(intent, 11)
//            val intent =
//                Intent(this, ManualBusinessAddress::class.java).putExtra("model", modelAddress)
//            startActivityForResult(intent, 1)
        }

        ssnVerified.setOnClickListener {
            startActivity(Intent(this,SsnNotVerified::class.java))
        }


        eTPhone.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                val text = eTPhone.text.toString()
                val textLength = eTPhone.text.length

                if (text.endsWith("-") || text.endsWith(" ") || text.endsWith(" "))
                    return
                if (textLength == 4) {
                    eTPhone.setText(StringBuilder(text).insert(text.length - 1, " ").toString())
                    (eTPhone as EditText).setSelection(eTPhone.text.length)
                }
                if (textLength == 8) {
                    eTPhone.setText(StringBuilder(text).insert(text.length - 1, " ").toString())
                    (eTPhone as EditText).setSelection(eTPhone.text.length)

                }
                if (textLength == 15) {
                    status = 1
                    Constant.hideKeyboard(this@IndividualMainLayout, eTPhone)
                } else {
                    status = 0
                }

            }

            override fun afterTextChanged(s: Editable) {
                status = if (s.length == 15) {
                    1
                } else {
                    0
                }
            }
        })


        editSsn.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                val textLength = editSsn.text.length

                if (textLength == 9) {
                    statusSSN = 1
                    Constant.hideKeyboard(this@IndividualMainLayout, editSsn)
                } else {
                    statusSSN = 0
                }

            }

            override fun afterTextChanged(s: Editable) {

            }
        })


        eTBillingAddress.setOnClickListener {
            Constant.hideKeyboard(this, eTBillingAddress)
            Places.createClient(this)
            val fields =
                listOf(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG)
            // Start the autocomplete intent.
            val intent = Autocomplete.IntentBuilder(
                AutocompleteActivityMode.FULLSCREEN, fields
            )
                .build(this)
            startActivityForResult(intent, 11)

//            val intent =
//                Intent(this, ManualBusinessAddress::class.java).putExtra("model", modelAddress)
//            startActivityForResult(intent, 1)
        }

        switch3.setOnCheckedChangeListener { p0, p1 ->
            if (p1) {
                textBilling.visibility = View.GONE
                eTBillingAddress.visibility = View.GONE
            } else {
                textBilling.visibility = View.VISIBLE
                eTBillingAddress.visibility = View.VISIBLE
            }
        }

        eTBirthdate.setOnClickListener {
            Constant.hideKeyboard(this, eTLegalEmail)
            val c = Calendar.getInstance()
            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)
            val day = c.get(Calendar.DAY_OF_MONTH)

            val dpd = DatePickerDialog(
                this,
                DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
                    eTBirthdate.setText("" + year + "/" + (month + 1) + "/" + dayOfMonth)
                },
                year,
                month,
                day
            )

            dpd.datePicker.maxDate = System.currentTimeMillis() - 568024668000
            dpd.show()
        }

        tVSubscription.setOnClickListener {
            flag = 0
            etRoutingNumber.setText("")
            etAccountNumber.setText("")
            confirmAccountNumber.setText("")
            etOwnersName.setText("")
            debit.visibility = View.VISIBLE
            bankAccount.visibility = View.GONE
            tVSubscription.setBackgroundResource(R.drawable.bg_cards_fill)
            textView70.setBackgroundResource(R.drawable.bg_cards_change)
            tVSubscription.setTextColor(resources.getColor(R.color.colorWhite))
            textView70.setTextColor(resources.getColor(R.color.upscribber_tv))
            Constant.hideKeyboard(this, tVSubscription)
//            hideLoader()
        }

        textView70.setOnClickListener {
            flag = 1
            debit.visibility = View.GONE
            bankAccount.visibility = View.VISIBLE
            tVSubscription.setBackgroundResource(R.drawable.bg_cards_change)
            textView70.setBackgroundResource(R.drawable.bg_cards_fill)
            tVSubscription.setTextColor(resources.getColor(R.color.upscribber_tv))
            textView70.setTextColor(resources.getColor(R.color.colorWhite))
            Constant.hideKeyboard(this, tVSubscription)
//            hideLoader()
        }


//        startSellingButton.setOnClickListener {
//            val sharedPreferences = Constant.getPrefs(this)
//            val editor = sharedPreferences.edit()
//            editor.putString("dashBoardClose", "Ok")
//            editor.putString(Constant.type, "2")
//            editor.apply()
//            removeData()
//            finish()
//        }

    }

    private fun removeData() {
        try {
            val sharedPreferences1 = getSharedPreferences("updateProfile", Context.MODE_PRIVATE)
            val editor = sharedPreferences1.edit()
            editor.remove("social")
            editor.apply()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    @SuppressLint("SetTextI18n")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == 20 && data != null) {
            val address = data.getParcelableExtra<ModelAddress>("address")
            modelAddress = address
            if (currentFragment == 2) {
                if (switch3.isChecked) {
                    if (address.suiteNumber.isEmpty()) {
                        searchAddressIndividual.text =
                            address.streetAddress + "," + address.city + "," + address.state + "," + address.zipCode
                    } else {
                        searchAddressIndividual.text =
                            address.suiteNumber + "," + address.streetAddress + "," + address.city + "," + address.state + "," + address.zipCode
                    }
                } else {
                    if (address.suiteNumber.isEmpty()) {
                        eTBillingAddress.text =
                                address.streetAddress + "," + address.city + "," + address.state + "," + address.zipCode
                    } else {
                        eTBillingAddress.text = address.suiteNumber + "," +
                                address.streetAddress + "," + address.city + "," + address.state + "," + address.zipCode
                    }
                }
            }
        }

        if (requestCode == 11) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    try {
                        val place = Autocomplete.getPlaceFromIntent(data!!)
                        val latLng = place.latLng
                        val latitude = latLng!!.latitude
                        val longitude = latLng.longitude
                        latitudesource = latitude
                        longitudesource = longitude
                        val geocoder = Geocoder(this)
                        val addresses = latitude.let { geocoder.getFromLocation(it, longitude, 1) }
                        try {
                            if (null != addresses && addresses.isNotEmpty()) {
                                locationModel.address = addresses[0].getAddressLine(0)
                                locationModel.street = addresses[0].thoroughfare
                                locationModel.city = addresses[0].subAdminArea
                                locationModel.state = addresses[0].adminArea
                                locationModel.zipcode = addresses[0].postalCode
                                locationModel.street = addresses[0].thoroughfare
                                locationModel.latitude = latitudesource.toString()
                                locationModel.longitude = longitude.toString()
                                startActivityForResult(Intent(this, ChangeLocationSetingsActivity::class.java).putExtra("seller","seller").putExtra("location", locationModel),20)
                            }
                        } catch (e: Exception) {
                            Toast.makeText(this,"Could not find the location!",Toast.LENGTH_SHORT).show()
                            e.printStackTrace()
                        }
                    } catch (e: Exception) {
                        Toast.makeText(this,"Could not find the location!",Toast.LENGTH_SHORT).show()
                        e.printStackTrace()
                    }

                }
            }
        }

    }


    @SuppressLint("SetTextI18n")
    private fun setData() {
        val dataNotification = intent.getParcelableExtra<ModelNotificationCustomer>("model")
        Glide.with(this).load(dataNotification.logo).placeholder(R.mipmap.circular_placeholder)
            .into(imageView)
        textName.text = dataNotification.business_name
        textLocation.text = dataNotification.street + " " + dataNotification.city

        joinRequest.setOnClickListener {
            currentFragment = 2
//            toolbarTitle.text = "Info"
            joinTeam.visibility = View.GONE
            personalInfo.visibility = View.VISIBLE
            taxInfo.visibility = View.GONE
            cardsInfo.visibility = View.GONE
            termsInfo.visibility = View.GONE
            sellerLearnToSell.visibility = View.GONE
            IndividualNext.visibility = View.VISIBLE
        }

        try {
            val type = Constant.getPrefs(this).getString(Constant.type, "")
            if (type == "2") {
                val prefs = Constant.getPrefs(this)
                val json = prefs.getString("step1", null)
                if (json.isNotEmpty()) {
                    val stepTwoData = getData("step1")
                    eTLegalNamePersonal.setText(stepTwoData.first_name)
                    etLastNamePersonal.setText(stepTwoData.last_name)
                    eTLegalName.setText(stepTwoData.legal_name)
                    eTLegalEmail.setText(stepTwoData.email)
                    searchAddressIndividual.text = stepTwoData.billing_address
                    modelAddress.city = stepTwoData.home_city
                    modelAddress.streetAddress = stepTwoData.home_street
                    modelAddress.state = stepTwoData.home_state
                    modelAddress.suiteNumber = stepTwoData.suite
                    modelAddress.zipCode = stepTwoData.home_zipcode
                    if (stepTwoData.contact_no.isNotEmpty()) {
                        status = 1
                    }
                }

                val json2 = prefs.getString("step2", null)
                if (json2.isNotEmpty()) {
                    val stepTwoData = getData("step2")
                    editSsn.setText(stepTwoData.ssn)
                    eTBirthdate.setText(stepTwoData.birthdate)
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun getData(key: String): ModelSteps {
        val prefs = Constant.getPrefs(this)
        val gson = Gson()
        val json = prefs.getString(key, null)
        val type = gson.fromJson<ModelSteps>(
            json,
            object : TypeToken<ModelSteps>() {

            }.type
        )
        return type

    }

    private fun setToolbar() {
        setSupportActionBar(toolbarSeller)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)
        title = ""

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        Constant.hideKeyboard(this,eTLegalName)
        if (currentFragment == 1) {
            finish()
        } else {
            currentFragment -= 1
            nextFragmentId(currentFragment)
        }

    }


    override fun nextFragmentId(i: Int) {
        if (i == 1) {
//            toolbarTitle.text = "Join"
            joinTeam.visibility = View.VISIBLE
            personalInfo.visibility = View.GONE
            taxInfo.visibility = View.GONE
            cardsInfo.visibility = View.GONE
            termsInfo.visibility = View.GONE
            IndividualNext.visibility = View.GONE
            sellerLearnToSell.visibility = View.GONE
            return
        }
        if (i == 2) {
//            toolbarTitle.text = "Info"
            joinTeam.visibility = View.GONE
            personalInfo.visibility = View.VISIBLE
            taxInfo.visibility = View.GONE
            cardsInfo.visibility = View.GONE
            termsInfo.visibility = View.GONE
            sellerLearnToSell.visibility = View.GONE
            IndividualNext.visibility = View.VISIBLE
            return
        }
        if (currentFragment == 3) {
//            toolbarTitle.text = "Tax"
            joinTeam.visibility = View.GONE
            personalInfo.visibility = View.GONE
            taxInfo.visibility = View.VISIBLE
            cardsInfo.visibility = View.GONE
            termsInfo.visibility = View.GONE
            sellerLearnToSell.visibility = View.GONE
            IndividualNext.visibility = View.VISIBLE
            return
        }
        if (currentFragment == 4) {
//            toolbarTitle.text = "Payment"
            joinTeam.visibility = View.GONE
            personalInfo.visibility = View.GONE
            taxInfo.visibility = View.GONE
            cardsInfo.visibility = View.VISIBLE
            termsInfo.visibility = View.GONE
            sellerLearnToSell.visibility = View.GONE
            IndividualNext.visibility = View.VISIBLE
            return
        }
        if (currentFragment == 5) {
//            toolbarTitle.text = "User Agreement"
            joinTeam.visibility = View.GONE
            personalInfo.visibility = View.GONE
            taxInfo.visibility = View.GONE
            cardsInfo.visibility = View.GONE
            termsInfo.visibility = View.VISIBLE
            sellerLearnToSell.visibility = View.GONE
            IndividualNext.visibility = View.VISIBLE
            return
        }
//        if (currentFragment == 6) {
//            toolbarTitle.text = ""
//            joinTeam.visibility = View.GONE
//            personalInfo.visibility = View.GONE
//            taxInfo.visibility = View.GONE
//            cardsInfo.visibility = View.GONE
//            termsInfo.visibility = View.GONE
//            sellerLearnToSell.visibility = View.VISIBLE
//            IndividualNext.visibility = View.GONE
//            return
//        }
    }


    override fun getFragments(i: Int) {

    }

    override fun individualFragment() {

    }

    override fun getIsBussiness(): Int {
        return currentFragment
    }


    fun onNextClick(v: View) {
        Constant.hideKeyboard(this, eTLegalEmail)
        nextApis(currentFragment)
    }

    private fun nextApis(i: Int) {
        val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
        val modelNotification = intent.getParcelableExtra<ModelNotificationCustomer>("model")

        val billingType = 0
        var payType = 1
        val addressStreet = modelAddress.streetAddress
        val addressCity = modelAddress.city
        val addressZip = modelAddress.zipCode
        val addressState = modelAddress.state
        val latitude = modelAddress.latitude
        val longitude = modelAddress.longitude
        var suiteNumber = modelAddress.suiteNumber

        if (i == 2) {
            val number = eTPhone.text.toString().trim()
            val phoneNumber = number.replace("+1","").replace("+91 ","").replace("(","").replace(")","").replace("-","").replace(" ","")
            if (switch3.isChecked) {
                if (eTLegalNamePersonal.text.toString().trim().isEmpty() && etLastNamePersonal.text.toString().trim().isEmpty() &&
                    eTLegalName.text.toString().trim().isEmpty() && eTLegalEmail.text.toString().trim().isEmpty() && phoneNumber.isEmpty() && searchAddressIndividual.text.toString().trim().isEmpty()
                ) {
                    Toast.makeText(this, "Please Enter all the details!", Toast.LENGTH_LONG).show()
                } else if (eTLegalName.text.toString().trim().isEmpty()) {
                    Toast.makeText(this, "Please enter your name", Toast.LENGTH_LONG).show()
                } else if (!Constant.isValidEmailId(eTLegalEmail.text.toString())) {
                    Toast.makeText(this, "Please Enter valid Email address", Toast.LENGTH_LONG)
                        .show()
                } else if (eTLegalEmail.text.toString().trim().isEmpty()) {
                    Toast.makeText(this, "Please enter your email", Toast.LENGTH_LONG).show()
                } else if (phoneNumber.isEmpty() && eTPhone.text.length < 0) {
                    Toast.makeText(this, "Please enter your phone number", Toast.LENGTH_LONG).show()
                } else if (searchAddressIndividual.text.toString().trim().isEmpty()) {
                    Toast.makeText(this, "Please enter your website", Toast.LENGTH_LONG).show()
                } else if (status == 0) {
                    Toast.makeText(
                        this,
                        "Please enter valid 10 digit phone number",
                        Toast.LENGTH_LONG
                    ).show()
                } else {
                    showLoader()
                    mViewModel.getThirdStepCategoryApi(
                        auth,
                        eTLegalName.text.toString().trim(),
                        eTLegalEmail.text.toString().trim(),
                        "+1" + phoneNumber,
                        searchAddressIndividual.text.toString().trim(),
                        (currentFragment - 1).toString(),
                        addressStreet.trim(),
                        addressCity.trim(),
                        addressZip.trim(),
                        addressState.trim(),
                        billingType,
                        modelNotification.business_id,
                        modelNotification.id,
                        eTLegalNamePersonal.text.toString().trim(),
                        etLastNamePersonal.text.toString().trim(),
                        latitude,
                        longitude,
                        suiteNumber
                    )

                }
            } else {
                if (eTLegalName.text.toString().trim().isEmpty() && eTLegalEmail.text.toString().trim().isEmpty() && eTPhone.text.toString().trim().isEmpty() && eTBillingAddress.text.toString().trim().isEmpty()) {
                    Toast.makeText(this, "Please Enter all the details!", Toast.LENGTH_LONG).show()
                } else if (eTLegalName.text.toString().trim().isEmpty()) {
                    Toast.makeText(this, "Please enter your name", Toast.LENGTH_LONG).show()
                } else if (eTLegalEmail.text.toString().trim().isEmpty()) {
                    Toast.makeText(this, "Please enter your email address", Toast.LENGTH_LONG)
                        .show()
                } else if (!Constant.isValidEmailId(eTLegalEmail.text.toString())) {
                    Toast.makeText(this, "Please Enter valid Email address", Toast.LENGTH_LONG)
                        .show()
                } else if (phoneNumber.isEmpty() && eTPhone.text.length < 0) {
                    Toast.makeText(this, "Please enter your phone number", Toast.LENGTH_LONG).show()
                } else if (eTBillingAddress.text.toString().trim().isEmpty()) {
                    Toast.makeText(this, "Please enter billing address", Toast.LENGTH_LONG).show()
                } else if (status == 0) {
                    Toast.makeText(
                        this,
                        "Please enter valid 10 digit phone number",
                        Toast.LENGTH_LONG
                    ).show()
                } else {
                    showLoader()
                    mViewModel.getThirdStepCategoryApi(
                        auth,
                        eTLegalName.text.toString().trim(),
                        eTLegalEmail.text.toString().trim(),
                        "+1" + phoneNumber,
                        eTBillingAddress.text.toString().trim(),
                        (currentFragment - 1).toString(),
                        addressStreet.trim(),
                        addressCity.trim(),
                        addressZip.trim(),
                        addressState.trim(),
                        billingType,
                        modelNotification.business_id,
                        modelNotification.id,
                        eTLegalNamePersonal.text.toString().trim(),
                        etLastNamePersonal.text.toString().trim(),
                        latitude,
                        longitude,
                        suiteNumber

                    )
                }
            }
            return
        }

        if (i == 3) {
            if (editSsn.text.isEmpty() && eTBirthdate.text.isEmpty()) {
                Toast.makeText(this, "Please Enter all the details!", Toast.LENGTH_LONG).show()
            } else if (editSsn.text.isEmpty()) {
                Toast.makeText(this, "Please Enter your ssn", Toast.LENGTH_LONG).show()
            } else if (eTBirthdate.text.isEmpty()) {
                Toast.makeText(this, "Please Enter birthdate", Toast.LENGTH_LONG).show()
            } else if (statusSSN == 0 && editSsn.text.toString().trim().isEmpty()) {
                Toast.makeText(this, "Please Enter valid ssn", Toast.LENGTH_LONG).show()
            } else {
                showLoader()
                mViewModel.getStepTax(
                    auth,
                    editSsn.text.toString().trim(),
                    eTBirthdate.text.toString().trim(),
                    (currentFragment - 1).toString(), modelNotification.business_id
                )
            }
            return
        }

        if (i == 4) {
            if (flag == 0) {
                if (!eTCardNumber.validateAllFields()) {
                    Toast.makeText(this, "PLease Enter Valid Card Details", Toast.LENGTH_LONG)
                        .show()
                } else {
                    showLoader()
                    payType = 1
                    val card = eTCardNumber.card
                    mViewModel.getStripeData(
                        auth,
                        card!!.number.toString(),
                        card.expMonth.toString(),
                        card.expYear.toString(),
                        card.cvc.toString()
                        , payType, (currentFragment - 1).toString(), modelNotification.business_id
                    )
                }
            } else {

                if (etRoutingNumber.text.toString().isEmpty() && etAccountNumber.text.toString().isEmpty()
                    && etOwnersName.text.toString().isEmpty() && confirmAccountNumber.text.toString().isEmpty()
                ) {
                    Toast.makeText(this, "Please Enter all the details!", Toast.LENGTH_LONG).show()
                } else if (etRoutingNumber.text.toString().isEmpty()) {
                    Toast.makeText(this, "Please enter routing number", Toast.LENGTH_LONG).show()
                } else if (etOwnersName.text.toString().isEmpty()) {
                    Toast.makeText(this, "Please enter bank account owners name", Toast.LENGTH_LONG)
                        .show()
                } else if (etAccountNumber.text.toString().isEmpty()) {
                    Toast.makeText(this, "Please enter your account number", Toast.LENGTH_LONG)
                        .show()
                } else if (confirmAccountNumber.text.toString().isEmpty()) {
                    Toast.makeText(
                        this,
                        "Please enter your confirm account number",
                        Toast.LENGTH_LONG
                    ).show()
                } else if (etAccountNumber.text.toString() != confirmAccountNumber.text.toString()) {
                    Toast.makeText(this, "Account number does not match!", Toast.LENGTH_LONG).show()
                } else {
                    showLoader()
                    payType = 0
                    mViewModel.getStripeBankData(
                        auth,
                        etRoutingNumber.text.toString(),
                        etAccountNumber.text.toString(),
                        etOwnersName.text.toString(),
                        (currentFragment - 1).toString()
                        , payType, modelNotification.business_id
                    )
                }
            }
            return
        }

        if (i == 5) {
            if (!switchCompat.isChecked) {
                Toast.makeText(
                    this,
                    "Please accept Terms & Conditions to continue",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                mViewModel.getLastStep(
                    auth,
                    (currentFragment - 1).toString(),
                    modelNotification.business_id,
                    modelNotification.id
                )
            }
        }


    }

    private fun showLoader() {
        progressBar.visibility = View.VISIBLE
    }

}