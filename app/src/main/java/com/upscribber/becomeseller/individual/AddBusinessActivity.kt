package com.upscribber.becomeseller.individual

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.R

class AddBusinessActivity : AppCompatActivity(), AddBusinessSearchAdapter.selection {

    private lateinit var toolbar5: Toolbar
    private lateinit var addBusineesRecycler: RecyclerView
    private lateinit var individualDone: TextView
    var selectedModel: AddBusinessSearchModel? = null
    private lateinit var list: ArrayList<AddBusinessSearchModel>
    private lateinit var addBusinessSearchAdapter: AddBusinessSearchAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView( R.layout.activity_add_business)
        initz()
        setToolbar()

        click()
        setAdapter()
    }

    private fun initz() {
        toolbar5 = findViewById(R.id.toolbar5)
        individualDone = findViewById(R.id.individualDone)
        addBusineesRecycler = findViewById(R.id.addBusineesRecycler)

    }

    private fun setToolbar() {
        setSupportActionBar(toolbar5)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)

    }

    private fun click() {

        individualDone.setOnClickListener {
            if (selectedModel != null) {
//                var intent = Intent(this, IndividualFragmentOne::class.java)
//                intent.putExtra("model", selectedModel)
//                setResult(Activity.RESULT_OK, intent)
                finish()
//                update1.setSelectedBussines(selectedModel!!)
            } else {
                Toast.makeText(this, "Select a business first", Toast.LENGTH_LONG).show()
            }

        }
    }

    private fun setAdapter() {

        addBusineesRecycler.layoutManager = LinearLayoutManager(this)
        list = getList()
        addBusinessSearchAdapter = AddBusinessSearchAdapter(this, list, this)
        addBusineesRecycler.adapter = addBusinessSearchAdapter
    }

    override fun selectList(position: Int, status: Boolean) {
        selectedModel = null
        if (!status) {
            selectedModel = list[position]
        }

        addBusinessSearchAdapter.updateArray(position)

    }


    private fun getList(): ArrayList<AddBusinessSearchModel> {
        val arrayList: ArrayList<AddBusinessSearchModel> = ArrayList()

        var addBusiness1 = AddBusinessSearchModel()
        addBusiness1.imgRequest = R.drawable.ic_lotus
        addBusiness1.nameRequest = "Mars Spa"
        addBusiness1.addressRequest = "1234,Smith St. Minnepolis,MN 0056"
        arrayList.add(addBusiness1)

        addBusiness1 = AddBusinessSearchModel()
        addBusiness1.imgRequest = R.drawable.ic_lotus
        addBusiness1.addressRequest = "1234,Smith St. Minnepolis,MN 0056"
        addBusiness1.nameRequest = "Mars Spa2"
        arrayList.add(addBusiness1)

        addBusiness1 = AddBusinessSearchModel()
        addBusiness1.imgRequest = R.drawable.ic_lotus
        addBusiness1.addressRequest = "1234,Smith St. Minnepolis,MN 0056"
        addBusiness1.nameRequest = "Mars Spa3"
        arrayList.add(addBusiness1)


        return arrayList
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }

        return super.onOptionsItemSelected(item)
    }

}
