package com.upscribber.becomeseller.individual

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.R
import kotlinx.android.synthetic.main.ind_add_recycler_layout.view.*

class AddBusinessSearchAdapter(
    private val mContext: Context,
    private val list: ArrayList<AddBusinessSearchModel>,
    listen: AddBusinessActivity
)
    : RecyclerView.Adapter<AddBusinessSearchAdapter.MyViewHolder>() {

    var listener = listen as selection

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(mContext)
            .inflate(R.layout.ind_add_recycler_layout, parent, false)

        return MyViewHolder(v)

    }

    override fun getItemCount(): Int {
        return list.size

    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = list[position]
        holder.itemView.imgRequest.setImageResource(model.imgRequest)
        holder.itemView.addressRequestt.text = model.addressRequest
        holder.itemView.nameRequestt.text = model.nameRequest

        holder.itemView.setOnClickListener {
            if (list[position].status){
                listener.selectList(position,list[position].status)
            }else{
                listener.selectList(position,list[position].status)
            }
//            listener.selectList(position)
        }


        if (model.status){
            holder.itemView.imageCheck.visibility = View.VISIBLE
        }else{
            holder.itemView.imageCheck.visibility = View.GONE
        }

    }

    fun updateArray(position: Int) {

        val selectedModel = list[position]
        if (selectedModel.status) {
            selectedModel.status = false

        } else {
            for (child in list) {
                child.status = false
            }
            list[position].status = true
        }

        notifyDataSetChanged()
    }



    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)


    interface selection{
        fun selectList(position: Int, status: Boolean)
    }
}