package com.upscribber.becomeseller.individual

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.upscribber.becomeseller.seller.ModelBecomeSeller
import com.upscribber.commonClasses.ModelStatusMsg
import com.upscribber.notification.ModelNotificationCustomer
import java.io.File

class IndividualViewModel(application: Application) : AndroidViewModel(application) {

    var repositoryIndividual: IndividualRepository = IndividualRepository(application)

    fun getThirdStepCategoryApi(
        auth: String,
        legalName: String,
        email: String,
        phone: String,
        address: String,
        step: String,
        addressStreet: String,
        addressCity: String,
        addressZip: String,
        addressState: String,
        billingType: Int,
        businessId: String,
        id: String,
        firstName: String,
        lastName: String,
        latitude: String,
        longitude: String,
        suiteNumber: String
    ) {
        repositoryIndividual.getFirstStepIndividual(
            auth,
            legalName,
            email,
            phone,
            address,
            step,
            addressStreet,
            addressCity,
            addressZip,
            addressState,
            billingType,
            businessId,
            id,
            firstName,lastName,latitude,longitude,suiteNumber
        )
    }


    fun getStatus(): LiveData<ModelStatusMsg> {
        return repositoryIndividual.getmDataFailure()
    }

    fun getmDataPersonalsInfo(): LiveData<ModelBecomeSeller> {
        return repositoryIndividual.getmDataPersonalsInfo()
    }

    fun getmDataTaxInfo(): LiveData<ModelBecomeSeller> {
        return repositoryIndividual.getmDataTaxInfo()
    }

    fun getmDataDetailInfo(): LiveData<ModelBecomeSeller> {
        return repositoryIndividual.getmDataDetailInfo()
    }


    fun getmDataBankStep(): LiveData<ModelBecomeSeller> {
        return repositoryIndividual.getmDataBankStep()
    }

      fun getmDataTermsInfo(): LiveData<ModelBecomeSeller> {
        return repositoryIndividual.getmDataTermsInfo()
    }


  fun getmDataRejectAssociated(): LiveData<ModelNotificationCustomer> {
        return repositoryIndividual.getmDataRejectAssociated()
    }


  fun getmDataRelink(): LiveData<ModelNotificationCustomer> {
        return repositoryIndividual.getmDataRelink()
    }



    fun getStepTax(
        auth: String,
        ssn: String,
        birthdate: String,
        steps: String,
        businessId: String
    ) {
        repositoryIndividual.getStepTax(auth, ssn, birthdate, steps, businessId)
    }


    fun getStripeData(
        auth: String,
        number: String,
        expMonth: String,
        expYear: String,
        cvc: String,
        payType: Int,
        step: String,
        businessId: String
    ) {
        repositoryIndividual.getStripeData(auth, number, expMonth, expYear, cvc, payType, step, businessId)
    }

    fun getStripeBankData(
        auth: String,
        routingNumber: String,
        accountNumber: String,
        ownersName: String,
        steps: String,
        payType: Int,
        businessId: String
    ) {
        repositoryIndividual.getStripeBankData(
            auth,
            routingNumber,
            accountNumber,
            ownersName,
            steps,
            payType,
            businessId
        )
    }

    fun getLastStep(
        auth: String,
        steps: String,
        businessId: String,
        id: String
    ) {
        repositoryIndividual.getTermsSteps(auth,steps,businessId, id)

    }

    fun rejectAssociatedRequest(
        auth: String,
        businessId: String,
        businessName: String,
        id: String
    ) {
        repositoryIndividual.rejectAssociatedRequest(auth,businessId,businessName, id)

    }

    fun relinkStaff(
        auth: String,
        staffId: String,
        business_id: String,
        id: String,
        businessName: String
    ) {

        repositoryIndividual.sendStaffId(auth, staffId, business_id,id,businessName)

    }

    fun uploadDocumentStripe(auth: String, fileFront : File, fileBack : File) {
        repositoryIndividual.uploadDocumentStripe(auth,fileFront,fileBack)

    }
}