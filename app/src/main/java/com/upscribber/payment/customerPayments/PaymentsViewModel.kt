package com.upscribber.payment.customerPayments

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.upscribber.commonClasses.ModelStatusMsg
import com.upscribber.upscribberSeller.customerBottom.profile.activity.ActivityModel

class PaymentsViewModel(application: Application) : AndroidViewModel(application) {
    var repositoryPayments : PaymentsRepository = PaymentsRepository(application)

    fun getAllTransactions(
        auth: String,
        type: String,
        customerId: String,
        tab: String,
        transPage: String,
        payPage: String,
        fromWhere: String
    ) {
        repositoryPayments.getAllTransactions(auth,type,customerId,tab,transPage,payPage,fromWhere)
    }

    fun getStatus(): LiveData<ModelStatusMsg> {
        return repositoryPayments.getmAllStatus()
    }

    fun getmDataInstantPaymentMethod(): LiveData<ModelStatusMsg> {
        return repositoryPayments.getmDataInstantPaymentMethod()
    }

    fun getUpdatedPaymentMethod(): LiveData<ModelStatusMsg> {
        return repositoryPayments.getUpdatedPaymentMethod()
    }



     fun getmDataUnredeem(): LiveData<ModelStatusMsg> {
        return repositoryPayments.getmDataUnredeem()
    }



     fun getmDataGenerateRefund(): LiveData<ModelStatusMsg> {
        return repositoryPayments.getmDataGenerateRefund()
    }

    fun getmAllTransactions():  LiveData<ModelPayouts> {
        return repositoryPayments.getmAllTransactions()
    }

     fun getmDataRefund(): LiveData<ModelStatusMsg> {
        return repositoryPayments.getmDataRefund()
    }

     fun getmDataDenied(): LiveData<ModelStatusMsg> {
        return repositoryPayments.getmDataDenied()
    }



    fun getRefund(auth: String, orderId: String, merchantId: String, transactionId: String) {
        repositoryPayments.getRefundApi(auth,orderId,merchantId,transactionId)
    }

    fun generateRefund(
        auth: String,
        transactionId: String,
        merchantAmount: String,
        partial_amount: String,
        adminAmount: String,
        type: String
    ) {
        repositoryPayments.generateRefund(auth,transactionId,merchantAmount,partial_amount,adminAmount,type)
    }

    fun getAllActivities(auth: String, type: String, tab: String) {
        repositoryPayments.getAllActivities(auth,type,tab)
    }

    fun getActivitiesData(): LiveData<ArrayList<ActivityModel>>{
        return repositoryPayments.getActivitiesData()
    }

     fun getCustomerList(): LiveData<ModelCustomers>{
        return repositoryPayments.getCustomerList()
    }

    fun getCampaignInvitedCustomer(auth: String, campaignId: String) {
        repositoryPayments.getCampaignInvitedCustomer(auth,campaignId)
    }

    fun retreiveCredit() {
       repositoryPayments.getCreditCards()

    }

    fun getUnredeem(auth: String, redeemId: String, campaignName: String) {
        repositoryPayments.getUnredeem(auth,redeemId,campaignName)
    }

    fun deniedRefund(auth: String, order_id: String) {
        repositoryPayments.getDeniedRefund(auth,order_id)

    }

    fun updateMerchantPaymentMethod(
        auth: String,
        id: String,
        position: Int,
        type : String
    ) {
        repositoryPayments.updateMerchantPaymentMethod(auth,id,position,type)
    }

    fun changePayoutMethod(auth: String, payout : String) {
        repositoryPayments.changePayoutMethod(auth,payout)

    }


}