package com.upscribber.payment.customerPayments

import android.app.Application
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.upscribber.commonClasses.Constant
import com.upscribber.commonClasses.ModelStatusMsg
import com.upscribber.commonClasses.WebServicesCustomers
import com.upscribber.commonClasses.WebServicesMerchants
import com.upscribber.home.ModelHomeLocation
import com.upscribber.payment.PaymentTransModel
import com.upscribber.payment.paymentCards.PaymentCardModel
import com.upscribber.upscribberSeller.customerBottom.profile.activity.ActivityModel
import com.upscribber.upscribberSeller.customerBottom.profile.activity.ModelActivityData
import com.upscribber.upscribberSeller.customerBottom.scanning.customerlistselection.ModelScanCustomers
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class PaymentsRepository(var application: Application) {

    private val mDataActivities = MutableLiveData<ArrayList<ActivityModel>>()
    private val mDataCustomer = MutableLiveData<ModelCustomers>()
    val mDataFailure = MutableLiveData<ModelStatusMsg>()
    val mDataInstantPaymentMethod = MutableLiveData<ModelStatusMsg>()
    val mDataUnredeem = MutableLiveData<ModelStatusMsg>()
    val mDataDenied = MutableLiveData<ModelStatusMsg>()
    val mDataPaymentMethod = MutableLiveData<ModelStatusMsg>()
    val mDataRefund = MutableLiveData<ModelStatusMsg>()
    val mDataGenerateRefund = MutableLiveData<ModelStatusMsg>()
    val mDataAllTransactions = MutableLiveData<ModelPayouts>()
    val mRetrieveCard = MutableLiveData<ArrayList<PaymentCardModel>>()
    val arrayPaymentsTransactions: java.util.ArrayList<PaymentTransModel> = java.util.ArrayList()
    val arrayPayoutsTransactions: java.util.ArrayList<PaymentTransModel> = java.util.ArrayList()

    fun getAllTransactions(
        auth: String,
        type: String,
        customerId: String,
        tab: String,
        transPage: String,
        payPage: String,
        fromWhere: String
    ) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesMerchants::class.java)
        val call: Call<ResponseBody> =
            service.getTransactions(auth, type, customerId, tab, "1", "1")
        call.enqueue(object : Callback<ResponseBody> {


            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        val modelPayouts = ModelPayouts()
                        if (status == "true") {

                            if (fromWhere == "customer") {
                                if(arrayPaymentsTransactions.size>0){
                                    arrayPaymentsTransactions.clear()
                                }
                            }

                            if (transPage.isNotEmpty()) {
                                if (transPage.toInt() == 1) {
                                    arrayPaymentsTransactions.clear()
                                }
                            }

                            if (payPage.isNotEmpty()) {
                                if (payPage.toInt() == 1) {
                                    arrayPayoutsTransactions.clear()
                                }
                            }
                            val data = json.getJSONObject("data")
                            val transactions = data.getJSONArray("transactions")
                            val payout = data.getJSONArray("payout")
                            val tcount = data.optInt("tcount")
                            val pcount = data.optInt("pcount")
                            if (transactions.length() > 0) {
                                for (i in 0 until transactions.length()) {
                                    val dataPayments = transactions.getJSONObject(i)
                                    val modelPayments = PaymentTransModel()
                                    modelPayments.transaction_id =
                                        dataPayments.optString("transaction_id")
                                    modelPayments.order_id = dataPayments.optString("order_id")
                                    modelPayments.customer_id =
                                        dataPayments.optString("customer_id")
                                    modelPayments.total_amount =
                                        dataPayments.optString("total_amount")
                                    modelPayments.transaction_status =
                                        dataPayments.optString("transaction_status")
                                    modelPayments.transaction_type =
                                        dataPayments.optString("transaction_type")
                                    modelPayments.created_at = dataPayments.optString("created_at")
                                    modelPayments.campaign_name =
                                        dataPayments.optString("campaign_name")
                                    modelPayments.customer_name =
                                        dataPayments.optString("customer_name")
                                    modelPayments.frequency_type =
                                        dataPayments.optString("frequency_type")
                                    modelPayments.business_name =
                                        dataPayments.optString("business_name")
                                    modelPayments.unit = dataPayments.optString("unit")
                                    modelPayments.redeemtion_cycle_qty =
                                        dataPayments.optString("redeemtion_cycle_qty")
                                    modelPayments.card_id = dataPayments.optString("card_id")
                                    modelPayments.merchant_id =
                                        dataPayments.optString("merchant_id")
                                    modelPayments.feature_image =
                                        dataPayments.optString("feature_image")
                                    modelPayments.description =
                                        dataPayments.optString("description")
                                    modelPayments.profile_image =
                                        dataPayments.optString("profile_image")
                                    modelPayments.order_status =
                                        dataPayments.optString("order_status")
                                    modelPayments.is_refunded =
                                        dataPayments.optString("is_refunded")
                                    modelPayments.is_pause = dataPayments.optString("is_pause")
                                    modelPayments.is_recur = dataPayments.optString("is_recur")
                                    modelPayments.payment_type =
                                        dataPayments.optString("payment_type")
                                    modelPayments.payment_method_name =
                                        dataPayments.optString("payment_method_name")
                                    modelPayments.last_4 = dataPayments.optString("last_4")
                                    arrayPaymentsTransactions.add(modelPayments)
                                }
                                modelPayouts.arrayPaymentsTransactions.addAll(
                                    arrayPaymentsTransactions
                                )

                            } else {
                                modelStatus.msg = msg
                                modelStatus.status = status
                                mDataFailure.value = modelStatus
                            }

                            if (payout.length() > 0) {
                                for (i in 0 until payout.length()) {
                                    val dataPayments = payout.getJSONObject(i)
                                    val modelPayments = PaymentTransModel()
                                    modelPayments.payout_id = dataPayments.optString("payout_id")
                                    modelPayments.amount = dataPayments.optString("amount")
                                    modelPayments.arrival_date =
                                        dataPayments.optString("arrival_date")
                                    modelPayments.last_4 = dataPayments.optString("last_4")
                                    modelPayments.deposit_name =
                                        dataPayments.optString("deposit_name")
                                    arrayPayoutsTransactions.add(modelPayments)
                                }
                                modelPayouts.arrayPayoutsTransactions.addAll(
                                    arrayPayoutsTransactions
                                )
                            } else {
                                modelStatus.msg = msg
                                modelStatus.status = status
                                mDataFailure.value = modelStatus
                            }

                            modelPayouts.tcount = tcount
                            modelPayouts.pcount = pcount
                            mDataAllTransactions.value = modelPayouts


                        } else {
                            modelStatus.msg = msg
                            modelStatus.status = status
                            mDataFailure.value = modelStatus
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus

            }
        })
    }

    fun getmAllTransactions(): LiveData<ModelPayouts> {
        return mDataAllTransactions
    }


    fun getmAllStatus(): LiveData<ModelStatusMsg> {
        return mDataFailure
    }

    fun getmDataInstantPaymentMethod(): LiveData<ModelStatusMsg> {
        return mDataInstantPaymentMethod
    }


    fun getUpdatedPaymentMethod(): LiveData<ModelStatusMsg> {
        return mDataPaymentMethod
    }


    fun getmDataUnredeem(): LiveData<ModelStatusMsg> {
        return mDataUnredeem
    }

    fun getmDataGenerateRefund(): LiveData<ModelStatusMsg> {
        return mDataGenerateRefund
    }


    fun getRefundApi(
        auth: String,
        orderId: String,
        merchantId: String,
        transactionId: String
    ) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesMerchants::class.java)
        val call: Call<ResponseBody> =
            service.refundRequest(auth, orderId, merchantId, transactionId)
        call.enqueue(object : Callback<ResponseBody> {


            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        if (status == "true") {
                            modelStatus.status = status
                            modelStatus.msg = msg
                            mDataRefund.value = modelStatus
                        } else {
                            modelStatus.status = status
                            modelStatus.msg = msg
                            mDataFailure.value = modelStatus
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus
            }
        })
    }

    fun getmDataRefund(): LiveData<ModelStatusMsg> {
        return mDataRefund
    }

    fun getmDataDenied(): LiveData<ModelStatusMsg> {
        return mDataDenied
    }


    fun generateRefund(
        auth: String,
        transactionId: String,
        merchantAmount: String,
        partialAmount: String,
        adminAmount: String,
        type: String
    ) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()
//
        val service = retrofit.create(WebServicesMerchants::class.java)
        val call: Call<ResponseBody> = service.genrateRefund(
            auth,
            transactionId,
            merchantAmount,
            partialAmount,
            adminAmount,
            type
        )
        call.enqueue(object : Callback<ResponseBody> {


            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        if (status == "true") {
                            val modelStatus = ModelStatusMsg()
                            modelStatus.status = status
                            modelStatus.msg = msg
                            mDataGenerateRefund.value = modelStatus
                        } else {
                            val modelStatus = ModelStatusMsg()
                            modelStatus.status = status
                            modelStatus.msg = msg
                            mDataFailure.value = modelStatus
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus
            }
        })

    }

    fun getAllActivities(auth: String, type: String, tab: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.getActivity(auth, type, tab)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        val arrayLocations = ArrayList<ModelHomeLocation>()
                        val arrayAllActivity = ArrayList<ActivityModel>()
                        if (status == "true") {
                            val data = json.optJSONArray("data")
//                            if (tab == "1" ) {
                            for (i in 0 until data.length()) {
                                val activityData = data.getJSONObject(i)
                                val activityModel = ActivityModel()
                                activityModel.id = activityData.optString("id")
                                activityModel.msg = activityData.optString("msg")
                                activityModel.recipient_id = activityData.optString("recipient_id")
                                activityModel.sender_id = activityData.optString("sender_id")
                                activityModel.activity_type =
                                    activityData.optString("activity_type")
                                activityModel.type = activityData.optString("type")
                                activityModel.reference_id = activityData.optString("reference_id")
                                activityModel.created_at = activityData.optString("created_at")
                                activityModel.action_taken = activityData.optString("action_taken")
                                activityModel.sub_type = activityData.optString("sub_type")
                                activityModel.updated_at = activityData.optString("updated_at")
                                activityModel.tab = tab
                                val data1 = activityData.optJSONObject("data")
                                val modelActivity = ModelActivityData()
                                modelActivity.assign_to = data1.optString("assign_to")
                                modelActivity.price = data1.optString("price")
                                modelActivity.campaign_name = data1.optString("campaign_name")
                                modelActivity.customer_id = data1.optString("customer_id")
                                modelActivity.total_amount = data1.optString("total_amount")
                                modelActivity.created_at = data1.optString("created_at")
                                modelActivity.cancel_reason = data1.optString("cancel_reason")
                                modelActivity.customer_name = data1.optString("customer_name")
                                modelActivity.business_name = data1.optString("business_name")
                                modelActivity.team_member = data1.optString("team_member")
                                modelActivity.location_id = data1.optString("location_id")
                                modelActivity.contact_no = data1.optString("contact_no")
                                modelActivity.profile_image = data1.optString("profile_image")
                                modelActivity.remaining_visits = data1.optString("remaining_visits")
                                modelActivity.frequency_type = data1.optString("frequency_type")
                                modelActivity.frequency_value = data1.optString("frequency_value")
                                modelActivity.unit = data1.optString("unit")
                                modelActivity.redeemtion_cycle_qty =
                                    data1.optString("redeemtion_cycle_qty")
                                modelActivity.rtype = data1.optString("rtype")
                                modelActivity.remaining_visits = data1.optString("remaining_visits")
                                modelActivity.description = data1.optString("description")
                                modelActivity.order_id = data1.optString("order_id")
                                modelActivity.business_id = data1.optString("business_id")
                                modelActivity.business_phone = data1.optString("business_phone")
                                modelActivity.logo = data1.optString("logo")
                                modelActivity.merchant_id = data1.optString("merchant_id")
                                modelActivity.team_member = data1.optString("team_member")
                                modelActivity.redeem_id = data1.optString("redeem_id")
                                modelActivity.transaction_id = data1.optString("transaction_id")
                                modelActivity.redeem_count = data1.optString("redeem_count")
                                modelActivity.campaign_id = data1.optString("campaign_id")
                                modelActivity.subscription_id = data1.optString("subscription_id")
                                modelActivity.campaign_owner = data1.optString("campaign_owner")
                                modelActivity.free_trial = data1.optString("free_trial")
                                modelActivity.merchant_user_id = data1.optString("free_trial")
                                modelActivity.transaction_status =
                                    data1.optString("transaction_status")
                                modelActivity.transaction_type = data1.optString("transaction_type")
                                modelActivity.card_id = data1.optString("card_id")
                                modelActivity.feature_image = data1.optString("feature_image")
                                modelActivity.order_status = data1.optString("order_status")
                                modelActivity.is_refunded = data1.optString("is_refunded")
                                modelActivity.redeem_count = data1.optString("redeem_count")
                                modelActivity.admin_amount = data1.optString("admin_amount")
                                modelActivity.merchant_amount = data1.optString("merchant_amount")
                                modelActivity.stripe_fee = data1.optString("stripe_fee")
                                modelActivity.contact_no = data1.optString("contact_no")
//                                    val locations = data1.optJSONArray("locations")
//                                    if (locations.length() > 0) {
//                                        for (j in 0 until locations.length()) {
//                                            val locationData = locations.getJSONObject(j)
//                                            val modelLocation = ModelHomeLocation()
//                                            modelLocation.id = locationData.optString("id")
//                                            modelLocation.business_id = locationData.optString("business_id")
//                                            modelLocation.user_id = locationData.optString("user_id")
//                                            modelLocation.street = locationData.optString("street")
//                                            modelLocation.city = locationData.optString("city")
//                                            modelLocation.state = locationData.optString("state")
//                                            modelLocation.zip_code = locationData.optString("zip_code")
//                                            modelLocation.lat = locationData.optString("lat")
//                                            modelLocation.long = locationData.optString("long")
//                                            modelLocation.store_timing = locationData.optString("store_timing")
//                                            modelLocation.created_at = locationData.optString("created_at")
//                                            modelLocation.updated_at = locationData.optString("updated_at")
//                                            arrayLocations.add(modelLocation)
//                                        }
//                                        modelActivity.locations = arrayLocations
//                                    }


                                activityModel.model = modelActivity
                                arrayAllActivity.add(activityModel)

                            }


                            modelStatus.status = status
                            modelStatus.msg = msg
                            mDataActivities.value = arrayAllActivity
                        } else {
                            modelStatus.status = status
                            modelStatus.msg = msg
                            mDataFailure.value = modelStatus
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus
            }
        })

    }

    fun getActivitiesData(): LiveData<ArrayList<ActivityModel>> {
        return mDataActivities
    }

    fun getCampaignInvitedCustomer(auth: String, campaignId: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesMerchants::class.java)
        val call: Call<ResponseBody> = service.getCampaignInvitedCustomer(auth, campaignId)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        if (status == "true") {
                            val data = json.optJSONObject("data")
                            val model = ModelCustomers()
                            val arrayCustomers = ArrayList<ModelScanCustomers>()
                            model.count = data.optString("count")
                            val customer = data.optJSONArray("customer")
                            for (i in 0 until customer.length()) {
                                val customersData = customer.getJSONObject(i)
                                val modelCustomers = ModelScanCustomers()
                                modelCustomers.name = customersData.optString("name")
                                modelCustomers.contact_no = customersData.optString("contact_no")
                                modelCustomers.profile_image =
                                    customersData.optString("profile_image")
                                arrayCustomers.add(modelCustomers)
                            }

                            model.customerDataArray = arrayCustomers
                            modelStatus.status = status
                            modelStatus.msg = msg

                            mDataCustomer.value = model

                        } else {
                            modelStatus.status = status
                            modelStatus.msg = msg
                            mDataFailure.value = modelStatus
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus
            }
        })

    }

    fun getCustomerList(): LiveData<ModelCustomers> {
        return mDataCustomer
    }

    fun getCreditCards() {
        val homeProfileData = Constant.getArrayListProfile(application, Constant.dataProfile)
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.stripeBaseUrl)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> =
            service.stripeRetrieve(homeProfileData.stripe_customer_id, "100")
        call.enqueue(object : Callback<ResponseBody> {


            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {

                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)

                        val data = json.optJSONArray("data")

                        Constant.cardArrayListData.clear()

                        for (i in 0 until data.length()) {
                            val stripeData = data.getJSONObject(i)
                            val cardModel = PaymentCardModel()
                            cardModel.id = stripeData.optString("id")
                            cardModel.brand = stripeData.optString("brand")
                            cardModel.country = stripeData.optString("country")
                            cardModel.last4 = stripeData.optString("last4")
                            cardModel.account_holder_name =
                                stripeData.optString("account_holder_name")
                            cardModel.name = stripeData.optString("name")
                            Constant.cardArrayListData.add(cardModel)
                        }
                        mRetrieveCard.value = Constant.cardArrayListData

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {

                Toast.makeText(application, "Error!", Toast.LENGTH_LONG).show()
            }
        })
    }


    fun getmDataStripeData(): LiveData<ArrayList<PaymentCardModel>> {
        return mRetrieveCard
    }

    fun getUnredeem(auth: String, redeemId: String, campaignName: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesMerchants::class.java)
        val call: Call<ResponseBody> = service.getUnredeem(auth, redeemId)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        if (status == "true") {
                            modelStatus.status = status
                            modelStatus.msg = msg
                            modelStatus.campaign_name = campaignName
                            mDataUnredeem.value = modelStatus

                        } else {
                            modelStatus.status = status
                            modelStatus.msg = msg
                            mDataFailure.value = modelStatus
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus
            }
        })

    }

    fun getDeniedRefund(auth: String, orderId: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesMerchants::class.java)
        val call: Call<ResponseBody> = service.getDeniedRefund(auth, orderId)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        if (status == "true") {
                            modelStatus.status = status
                            modelStatus.msg = msg
                            mDataDenied.value = modelStatus

                        } else {
                            modelStatus.status = status
                            modelStatus.msg = msg
                            mDataFailure.value = modelStatus
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus
            }
        })

    }

    fun updateMerchantPaymentMethod(
        auth: String,
        id: String,
        position: Int,
        type: String
    ) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesMerchants::class.java)
        val call: Call<ResponseBody> = service.updateMerchantPaymentMethod(auth, id)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        if (status == "true") {
                            modelStatus.status = status
                            modelStatus.position = position
                            modelStatus.msg = msg
                            modelStatus.type = type
                            mDataPaymentMethod.value = modelStatus

                        } else {
                            modelStatus.status = status
                            modelStatus.msg = msg
                            mDataFailure.value = modelStatus
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus
            }
        })

    }

    fun changePayoutMethod(auth: String, payout: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesMerchants::class.java)
        val call: Call<ResponseBody> = service.changePayoutMethod(auth, payout)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        if (status == "true") {
                            modelStatus.status = status
                            modelStatus.msg = msg
                            mDataInstantPaymentMethod.value = modelStatus

                        } else {
                            modelStatus.status = status
                            modelStatus.msg = msg
                            mDataFailure.value = modelStatus
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus
            }
        })


    }

}

