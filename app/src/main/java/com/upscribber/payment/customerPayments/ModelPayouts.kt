package com.upscribber.payment.customerPayments

import com.upscribber.payment.PaymentTransModel

class ModelPayouts {

    var pcount: Int = 0
    var tcount: Int = 0
    val arrayPaymentsTransactions: ArrayList<PaymentTransModel> = ArrayList()
    val arrayPayoutsTransactions: ArrayList<PaymentTransModel> = ArrayList()
}
