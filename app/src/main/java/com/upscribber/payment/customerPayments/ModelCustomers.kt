package com.upscribber.payment.customerPayments

import android.os.Parcel
import android.os.Parcelable
import com.upscribber.upscribberSeller.customerBottom.scanning.customerlistselection.ModelScanCustomers
import java.util.ArrayList

class ModelCustomers() : Parcelable {

    var customerDataArray: ArrayList<ModelScanCustomers> = ArrayList()
    var count: String = ""

    constructor(parcel: Parcel) : this() {
        count = parcel.readString()
        customerDataArray = parcel.createTypedArrayList(ModelScanCustomers)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(count)
        parcel.writeTypedList(customerDataArray)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ModelCustomers> {
        override fun createFromParcel(parcel: Parcel): ModelCustomers {
            return ModelCustomers(parcel)
        }

        override fun newArray(size: Int): Array<ModelCustomers?> {
            return arrayOfNulls(size)
        }
    }

}
