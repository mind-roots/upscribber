package com.upscribber.payment

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.upscribber.commonClasses.Constant
import com.upscribber.R
import kotlinx.android.synthetic.main.activity_refund_flow.*

class RefundFlowActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_refund_flow)


        done.setOnClickListener {
            if(intent.hasExtra("from")){
                val intent=Intent()
                setResult(1,intent)
                finish()
            }else{
                finish()
            }

        }

        val isSeller = Constant.getSharedPrefs(this).getBoolean(Constant.IS_SELLER, false)

        if(isSeller){
            constraint.setBackgroundResource(R.drawable.bg_almost_there)
            textView204.text = "Refund Initiated!"
            refundText.text = "Changes will be reflected in your next billing cycle."
        }else{
            constraint.setBackgroundResource(R.drawable.side_nav_bar)
            textView204.text = " Refund Requested!"
            refundText.text = "Your refund request has been sent and will be updated once processed."

        }
    }
}
