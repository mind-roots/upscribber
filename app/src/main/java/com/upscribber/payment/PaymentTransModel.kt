package com.upscribber.payment

import android.os.Parcel
import android.os.Parcelable
import java.sql.Time
import java.util.*

/**
 * Created by amrit on 8/3/19.
 */
class PaymentTransModel() : Parcelable{

    var payment_method_name: String = ""
    var payment_type: String = ""
    var is_recur: String = ""
    var is_pause: String = ""
    var deposit_name: String = ""
    var last_4: String = ""
    var arrival_date: String = ""
    var amount: String = ""
    var payout_id: String = ""
    var order_status: String = ""
    var is_refunded: String = ""
    var transaction_id: String = ""
    var order_id: String = ""
    var customer_id: String = ""
    var total_amount: String = ""
    var transaction_status: String = ""
    var transaction_type: String = ""
    var created_at: String = ""
    var campaign_name: String = ""
    var customer_name: String = ""
    var frequency_type: String = ""
    var business_name: String = ""
    var unit: String = ""
    var redeemtion_cycle_qty: String = ""
    var card_id: String = ""
    var merchant_id: String = ""
    var feature_image: String = ""
    var description: String = ""
    var profile_image: String = ""
    var CustomerName: String = ""
    var status : Int = 0

    constructor(parcel: Parcel) : this() {
        payment_method_name = parcel.readString()
        payment_type = parcel.readString()
        is_recur = parcel.readString()
        is_pause = parcel.readString()
        deposit_name = parcel.readString()
        last_4 = parcel.readString()
        arrival_date = parcel.readString()
        amount = parcel.readString()
        payout_id = parcel.readString()
        order_status = parcel.readString()
        is_refunded = parcel.readString()
        transaction_id = parcel.readString()
        order_id = parcel.readString()
        customer_id = parcel.readString()
        total_amount = parcel.readString()
        transaction_status = parcel.readString()
        transaction_type = parcel.readString()
        created_at = parcel.readString()
        campaign_name = parcel.readString()
        customer_name = parcel.readString()
        frequency_type = parcel.readString()
        business_name = parcel.readString()
        unit = parcel.readString()
        redeemtion_cycle_qty = parcel.readString()
        card_id = parcel.readString()
        merchant_id = parcel.readString()
        feature_image = parcel.readString()
        description = parcel.readString()
        profile_image = parcel.readString()
        CustomerName = parcel.readString()
        status = parcel.readInt()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(payment_method_name)
        parcel.writeString(payment_type)
        parcel.writeString(is_recur)
        parcel.writeString(is_pause)
        parcel.writeString(deposit_name)
        parcel.writeString(last_4)
        parcel.writeString(arrival_date)
        parcel.writeString(amount)
        parcel.writeString(payout_id)
        parcel.writeString(order_status)
        parcel.writeString(is_refunded)
        parcel.writeString(transaction_id)
        parcel.writeString(order_id)
        parcel.writeString(customer_id)
        parcel.writeString(total_amount)
        parcel.writeString(transaction_status)
        parcel.writeString(transaction_type)
        parcel.writeString(created_at)
        parcel.writeString(campaign_name)
        parcel.writeString(customer_name)
        parcel.writeString(frequency_type)
        parcel.writeString(business_name)
        parcel.writeString(unit)
        parcel.writeString(redeemtion_cycle_qty)
        parcel.writeString(card_id)
        parcel.writeString(merchant_id)
        parcel.writeString(feature_image)
        parcel.writeString(description)
        parcel.writeString(profile_image)
        parcel.writeString(CustomerName)
        parcel.writeInt(status)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PaymentTransModel> {
        override fun createFromParcel(parcel: Parcel): PaymentTransModel {
            return PaymentTransModel(parcel)
        }

        override fun newArray(size: Int): Array<PaymentTransModel?> {
            return arrayOfNulls(size)
        }
    }


}