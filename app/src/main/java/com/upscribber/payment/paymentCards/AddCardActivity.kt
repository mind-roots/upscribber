package com.upscribber.payment.paymentCards

import android.annotation.SuppressLint
import android.app.Activity
import android.app.DatePickerDialog
import android.content.DialogInterface
import android.content.Intent
import android.content.res.Resources
import android.location.Geocoder
import android.os.Bundle
import android.text.*
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.text.style.UnderlineSpan
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.google.android.material.textfield.TextInputLayout
import com.kal.rackmonthpicker.MonthType
import com.kal.rackmonthpicker.RackMonthPicker
import com.stripe.android.model.Card
import com.stripe.android.model.SourceParams
import com.stripe.android.view.CardMultilineWidget
import com.stripe.android.view.StripeEditText
import com.upscribber.commonClasses.Constant
import com.upscribber.R
import com.upscribber.becomeseller.seller.ModelAddress
import com.upscribber.businessAddress.ManualBusinessAddress
import com.upscribber.checkout.CheckOutTwo
import com.upscribber.checkout.ModelCheckoutOne
import com.upscribber.profile.ModelGetProfile
import com.upscribber.subsciptions.merchantSubscriptionPages.ModelCampaignInfo
import com.upscribber.subsciptions.merchantSubscriptionPages.ModelSubscriptionData
import com.upscribber.subsciptions.merchantSubscriptionPages.plan.PlanModel
import com.upscribber.upscribberSeller.subsriptionSeller.subscriptionMain.ModelSellerSubscriptions
import com.upscribber.upscribberSeller.subsriptionSeller.subscriptionMain.customerCheckout.CompleteCheckOutMerchant
import kotlinx.android.synthetic.main.activity_add_card.*
import kotlinx.android.synthetic.main.activity_add_card.view.*
import java.util.*


class AddCardActivity : AppCompatActivity() {

    lateinit var toolbar: Toolbar
    lateinit var toolbarTitle: TextView
    lateinit var textView159: TextView
    lateinit var addCard: TextView
    lateinit var textView172: TextView
    lateinit var tvDateYear: EditText
    lateinit var clAddCard: ConstraintLayout
    private lateinit var mExpiryTextInputLayout: TextInputLayout
    lateinit var eTCardNumber: CardMultilineWidget
    lateinit var eTCardNumber1: EditText
    lateinit var eTextCvv: EditText
    lateinit var mCvcEditText: StripeEditText
    var flagCardNumber = 0
    var flagCardCvv = 0
    //    val stripe = Stripe(this.applicationContext, "pk_test_DEAl5hakQ6ks0QzUQf0pkhHH")
    lateinit var mViewModel: AddCardViewModel
    var latitude = 1.0
    var longitude = 1.0
    var modelProfile = ModelGetProfile()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_card)
        mViewModel = ViewModelProviders.of(this)[AddCardViewModel::class.java]
        initz()
        Places.initialize(applicationContext, resources.getString(R.string.google_maps_key))
        setToolbar()
        setSpan()
        val isSeller = Constant.getSharedPrefs(this).getBoolean(Constant.IS_SELLER, false)
        if (isSeller) {
            focus1()
//            setSpanMerchant()

        } else {
            focus()

        }
        setButtonVisible()
        setData()
        clickListeners()
        observerInit()
    }

    private fun setButtonVisible() {
        val isSeller = Constant.getSharedPrefs(this).getBoolean(Constant.IS_SELLER, false)
        if (isSeller){
            addCard.setBackgroundResource(R.drawable.blue_button_background_opacity)
        }else{
            addCard.setBackgroundResource(R.drawable.invite_button_background_opacity)
        }

        eTCardNumber1.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(
                s: CharSequence,
                start: Int,
                count: Int,
                after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence,
                start: Int,
                before: Int,
                count: Int
            ) {
                if (getTextFieldsCardData()) {
                    if (isSeller){
                        addCard.setBackgroundResource(R.drawable.bg_seller_button_blue)
                    }else{
                        addCard.setBackgroundResource(R.drawable.invite_button_background)
                    }

                } else {
                    if (isSeller){
                        addCard.setBackgroundResource(R.drawable.blue_button_background_opacity)
                    }else {
                        addCard.setBackgroundResource(R.drawable.invite_button_background_opacity)
                    }
                }
            }

            override fun afterTextChanged(s: Editable) {
            }
        })


        tvDateYear.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(
                s: CharSequence,
                start: Int,
                count: Int,
                after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence,
                start: Int,
                before: Int,
                count: Int
            ) {
                if (getTextFieldsCardData()) {
                    if (isSeller){
                        addCard.setBackgroundResource(R.drawable.bg_seller_button_blue)
                    }else{
                        addCard.setBackgroundResource(R.drawable.invite_button_background)
                    }

                } else {
                    if (isSeller){
                        addCard.setBackgroundResource(R.drawable.blue_button_background_opacity)
                    }else {
                        addCard.setBackgroundResource(R.drawable.invite_button_background_opacity)
                    }
                }
            }

            override fun afterTextChanged(s: Editable) {
            }
        })

        eTextCvv.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(
                s: CharSequence,
                start: Int,
                count: Int,
                after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence,
                start: Int,
                before: Int,
                count: Int
            ) {
                if (getTextFieldsCardData()) {
                    if (isSeller){
                        addCard.setBackgroundResource(R.drawable.bg_seller_button_blue)
                    }else{
                        addCard.setBackgroundResource(R.drawable.invite_button_background)
                    }

                } else {
                    if (isSeller){
                        addCard.setBackgroundResource(R.drawable.blue_button_background_opacity)
                    }else {
                        addCard.setBackgroundResource(R.drawable.invite_button_background_opacity)
                    }
                }
            }

            override fun afterTextChanged(s: Editable) {
            }
        })

    }

    private fun getTextFieldsCardData(): Boolean {
        val eTCardNumber = eTCardNumber1.text.toString().trim().length
        val tvDateYear = tvDateYear.text.toString().trim().length
        val eTextCvv = eTextCvv.text.toString().trim().length

        if (eTCardNumber < 16) {
            return false
        }
        if (tvDateYear == 0) {
            return false
        }
        if (eTextCvv<3) {
            return false
        }

        return true

    }

    @SuppressLint("SetTextI18n")
    private fun setData() {
        val stripeName = Constant.getPrefs(this).getString(Constant.stripe_customer_name, "")
        if (intent.hasExtra("modelGetProfile")) {
            val modelGetProfile = intent.getParcelableExtra<ModelGetProfile>("modelGetProfile")
            if (modelGetProfile.billing_address.billing_street == "" || modelGetProfile.billing_address.billing_city == "" ||
                modelGetProfile.billing_address.billing_state == "" || modelGetProfile.billing_address.billing_zipcode == ""
            ) {
                etBillingAddress.text = ""
                etBillingAddress.hint = "Enter Address"
            } else {
                etBillingAddress.text =
                    modelGetProfile.billing_address.billing_street + " " + modelGetProfile.billing_address.billing_city + " " +
                            modelGetProfile.billing_address.billing_state + " " + modelGetProfile.billing_address.billing_zipcode
            }
        } else if (intent.hasExtra("checkoutOne")) {
            val modelCard = intent.getParcelableExtra<ModelCheckoutOne>("checkoutOne")
            etBillingAddress.text =
                modelCard.billingStreet + " " + modelCard.billingCity + " " +
                        modelCard.billingState + " " + modelCard.billingZip
        }

        when {
            intent.hasExtra("addFromPayments") -> {
                textBilling.visibility = View.VISIBLE
                etBillingAddress.visibility = View.VISIBLE
                textBillingUpdate.visibility = View.GONE
                etCardName.setText(stripeName)
            }
            intent.hasExtra("updateCard") -> {
                textBilling.visibility = View.VISIBLE
                etBillingAddress.visibility = View.VISIBLE
                val isSeller = Constant.getSharedPrefs(this).getBoolean(Constant.IS_SELLER, false)
                if (isSeller) {
                    textBillingUpdate.setTextColor(resources.getColor(R.color.blue))
                }else{
                    textBillingUpdate.setTextColor(resources.getColor(R.color.accent))
                }
                textBillingUpdate.visibility = View.VISIBLE
                etCardName.setText(stripeName)
            }
            intent.hasExtra("profilePayment") -> {
                textBilling.visibility = View.VISIBLE
                etBillingAddress.visibility = View.VISIBLE
                textBillingUpdate.visibility = View.GONE
                etCardName.setText(stripeName)
            }
            else -> {
                textBilling.visibility = View.GONE
                etBillingAddress.visibility = View.GONE
                textBillingUpdate.visibility = View.GONE
            }
        }

        textBillingUpdate.setOnClickListener {
            Constant.hideKeyboard(this, etBillingAddress)
            Places.createClient(this)
            val fields =
                listOf(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG)
            // Start the autocomplete intent.
            val intent = Autocomplete.IntentBuilder(
                AutocompleteActivityMode.FULLSCREEN, fields
            )
                .build(this)
            startActivityForResult(intent, 10)

        }

    }

    private fun clickListeners() {
        addCard.setOnClickListener {
            Constant.hideKeyboard(this, etCardName)

            if (eTCardNumber1.text.toString().trim().isEmpty() || tvDateYear.text.toString().trim().isEmpty() || eTextCvv.text.toString().trim().isEmpty()) {
                Toast.makeText(this, "PLease Enter Card Details", Toast.LENGTH_LONG).show()
            } else if (eTCardNumber1.text.toString().trim().isEmpty()) {
                Toast.makeText(this, "PLease Enter Card Number", Toast.LENGTH_LONG).show()
            } else if (tvDateYear.text.toString().trim().isEmpty()) {
                Toast.makeText(this, "PLease Enter Card Expiry Date", Toast.LENGTH_LONG).show()
            } else if (eTextCvv.text.toString().trim().isEmpty()) {
                Toast.makeText(this, "PLease Enter valid cvv", Toast.LENGTH_LONG).show()
            }else {
                val splitDate = tvDateYear.text.toString().trim().split("/")
                when {
                    intent.hasExtra("merchantCheckOut") -> {
                        val value = intent.getStringExtra("merchantCheckOut")
                        val value2 = intent.getParcelableExtra<ModelGetProfile>("modelProfile")
                        mViewModel.getStripeData(
                            eTCardNumber1.text.toString().trim(),
                            splitDate[0],
                            splitDate[1],
                            eTextCvv.text.toString().trim(),
                            etCardName.text.toString().trim(),
                            value,
                            value2.stripe_customer_id
                        )
                    }
                    intent.hasExtra("merchantCheckOutt") -> {
                        val value2 = intent.getParcelableExtra<ModelGetProfile>("modelProfile")
                        val value = intent.getStringExtra("merchantCheckOutt")
                        mViewModel.getStripeData(
                            eTCardNumber1.text.toString().trim(),
                            splitDate[0],
                            splitDate[1],
                            eTextCvv.text.toString().trim(),
                            etCardName.text.toString().trim(),
                            value,
                            value2.stripe_customer_id
                        )
                    }
                    intent.hasExtra("paymentsProfile") -> {
                        val value = intent.getStringExtra("paymentsProfile")
                        modelProfile = intent.getParcelableExtra("profileModel")
                        mViewModel.getStripeData(
                            eTCardNumber1.text.toString().trim(),
                            splitDate[0],
                            splitDate[1],
                            eTextCvv.text.toString().trim(),
                            etCardName.text.toString().trim(),
                            value,
                            ""
                        )
                    }else -> {
                        mViewModel.getStripeData(
                            eTCardNumber1.text.toString().trim(),
                            splitDate[0],
                            splitDate[1],
                            eTextCvv.text.toString().trim(),
                            etCardName.text.toString().trim(),
                            "",
                            ""
                        )
                    }
                }

                progressBar10.visibility = View.VISIBLE
            }


//            if (!eTCardNumber.validateAllFields()) {
//                Toast.makeText(this, "PLease Enter Valid Card Details", Toast.LENGTH_LONG).show()
//
//            } else {
//                val card = eTCardNumber.card
//                if (card != null) {
//                    tokenizeCard(card)
//                }
//
//                mViewModel.getStripeData(
//                    card!!.number.toString(), card.expMonth.toString(), card.expYear.toString(),
//                    card.cvc.toString(), etCardName.text.toString()
//                )
//                progressBar10.visibility = View.VISIBLE
//            }
        }



        eTCardNumber1.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                flagCardNumber = if (s.length == 16) {
                    Constant.hideKeyboard(this@AddCardActivity, eTCardNumber1)
                    1
                } else {
                    0
                }
            }

            override fun afterTextChanged(s: Editable) {

            }
        })




        tvDateYear.setOnClickListener {
            val dialog = datePickerDialog()
            dialog.show()
            val year = dialog.findViewById<View>(Resources.getSystem().getIdentifier("android:id/day", null, null))
            if (year != null) {
                year.visibility = View.GONE
            }
        }


    }

    @SuppressLint("SetTextI18n")
    fun datePickerDialog(): DatePickerDialog {
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        // Date Picker Dialog
        val datePickerDialog = DatePickerDialog(
            this,
            R.style.AlertDialogCustom1,
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                // Display Selected date in textbox
                Log.e("checkDate", "${monthOfYear+1}, $year")
                tvDateYear.setText("${monthOfYear+1}/$year")
            },
            year,
            month,
            day

        )
//        datePickerDialog.setButton(
//            DatePickerDialog.BUTTON_POSITIVE,
//            "Ok",
//            DialogInterface.OnClickListener() { dialog, which ->
//                tvDateYear.setText("$month/$year")
//            })

        datePickerDialog.setButton(
            DatePickerDialog.BUTTON_NEGATIVE,
            null,
            null as DialogInterface.OnClickListener?
        )
        return datePickerDialog
    }

    private fun observerInit() {
        mViewModel.getmDataAllBrands().observe(this, androidx.lifecycle.Observer {
            progressBar10.visibility = View.GONE
            when {
                intent.hasExtra("checkout") -> {
                    startActivity(
                        Intent(this, CheckOutTwo::class.java)
                            .putExtra("allData", intent.getParcelableExtra<PlanModel>("allData"))
                            .putExtra(
                                "modelSubscription",
                                intent.getParcelableExtra<ModelCampaignInfo>("modelSubscription")
                            )
                            .putExtra(
                                "modelCheckOut",
                                intent.getParcelableExtra<ModelCheckoutOne>("modelCheckOut")
                            )
                            .putExtra("checkOut", intent.hasExtra("checkout"))
                            .putExtra("totalPrice", intent.getStringExtra("totalPrice")).putExtra("subscriptionData",intent.getParcelableExtra<ModelSubscriptionData>("subscriptionData"))
                    )

                }
                intent.hasExtra("merchantCheckOut") -> {
                    if (it.value2.isNotEmpty()) {
                        mViewModel.getRetrieveCards(it.value2)
                    } else {
                        mViewModel.getRetrieveCards("")
                    }

                }
                else -> {
                    startActivity(Intent(this, CardConfirmationActivity::class.java))
                    finish()
                }
            }

        })


        mViewModel.getmRetrieveCard().observe(this, androidx.lifecycle.Observer {
            startActivity(
                Intent(
                    this,
                    CompleteCheckOutMerchant::class.java
                ).putExtra(
                    "modelSubscriptionModel",
                    intent.getParcelableExtra<ModelSellerSubscriptions>("modelSubscriptionModel")
                )
                    .putExtra("totalPriceText", intent.getStringExtra("totalPriceText"))
                    .putExtra(
                        "modelProfile",
                        intent.getParcelableExtra<ModelGetProfile>("modelProfile")
                    )
                    .putExtra(
                        "modelCheckoutMerchant",
                        intent.getParcelableExtra<ModelCheckoutOne>("modelCheckoutMerchant")
                    )
            )
        })


        mViewModel.getmDataFailure().observe(this, androidx.lifecycle.Observer {
            progressBar10.visibility = View.GONE
            Toast.makeText(this, "Please give valid card details!", Toast.LENGTH_SHORT).show()
        })


        mViewModel.getmDataCard().observe(this, Observer {
            if (it.status == "true") {
                mViewModel.getStripeResponse(modelProfile.stripe_account_id, "card")
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
            } else {
                progressBar10.visibility = View.GONE
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
            }
        })

        mViewModel.getDataCard().observe(this, Observer {
            intent.putParcelableArrayListExtra("card", it)
            setResult(Activity.RESULT_OK, intent)
            progressBar10.visibility = View.GONE
            finish()
        })

    }

    @SuppressLint("SetTextI18n")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == 1 && data != null) {
            val modelGetProfile = data.getParcelableExtra<ModelAddress>("address")
            if (modelGetProfile.suiteNumber == "") {
                etBillingAddress.text =
                    modelGetProfile.streetAddress + "," + modelGetProfile.city + "," +
                            modelGetProfile.state + "," + modelGetProfile.zipCode
            } else {
                etBillingAddress.text =
                    modelGetProfile.streetAddress + "," + modelGetProfile.suiteNumber + "," + modelGetProfile.city + "," +
                            modelGetProfile.state + "," + modelGetProfile.zipCode
            }

        }

        if (requestCode == 10) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    val place = Autocomplete.getPlaceFromIntent(data!!)
                    val latLng = place.latLng
                    latitude = latLng!!.latitude
                    longitude = latLng.longitude
                    val geocoder = Geocoder(this)
                    val addresses =
                        latLng.latitude.let {
                            geocoder.getFromLocation(
                                it,
                                latLng.longitude,
                                1
                            )
                        }
                    val locationModel = ModelAddress()
                    locationModel.streetAddress = addresses[0].thoroughfare
                    locationModel.city = addresses[0].locality
                    locationModel.state = addresses[0].adminArea
                    locationModel.latitude = latitude.toString()
                    locationModel.longitude = longitude.toString()
                    locationModel.zipCode = addresses[0].postalCode
                    startActivityForResult(
                        Intent(
                            this,
                            ManualBusinessAddress::class.java
                        ).putExtra("addCard", "addCard").putExtra("locationModel", locationModel),
                        10
                    )

                }
            }
        }


    }


    private fun initz() {
        toolbar = findViewById(R.id.addCardToolbar)
        toolbarTitle = findViewById(R.id.toolbarTitle)
        textView159 = findViewById(R.id.textView159)
        addCard = findViewById(R.id.addCard)
        tvDateYear = findViewById(R.id.tvDateYear)
        clAddCard = findViewById(R.id.clAddCard)
        textView172 = findViewById(R.id.textView172)
        eTCardNumber1 = findViewById(R.id.eTCardNumber1)
        eTextCvv = findViewById(R.id.eTextCvv)
        mExpiryTextInputLayout = findViewById(R.id.tl_add_source_expiry_ml)
        eTCardNumber = findViewById(R.id.eTCardNumber)
        mCvcEditText = findViewById(R.id.et_add_source_cvc_ml)
        if (intent.hasExtra("modelProfile")) {
            modelProfile = intent.getParcelableExtra("modelProfile")
        }
    }

    private fun setToolbar() {
        setSupportActionBar(toolbar)
        title = ""

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)

        when {
            intent.hasExtra("checkout") -> {
                toolbarTitle.text = ""
                textView172.visibility = View.VISIBLE
            }
            intent.hasExtra("updateCard") -> {
                toolbarTitle.text = "Edit Card"
                textView172.visibility = View.GONE
            }
            else -> {
                toolbarTitle.text = "Add Card"
                textView172.visibility = View.GONE
            }
        }
    }

    private fun focus1() {

        eTCardNumber.setOnFocusChangeListener { view, p1 ->
            if (p1) {
                eTCardNumber.setBackgroundResource(R.drawable.bg_seller_blue_outline)
            } else {
                eTCardNumber.setBackgroundResource(R.drawable.login_background)
            }
        }


        eTextCvv.setOnFocusChangeListener { view, p1 ->
            if (p1) {
                eTextCvv.setBackgroundResource(R.drawable.bg_seller_blue_outline)
            } else {
                eTextCvv.setBackgroundResource(R.drawable.login_background)
            }
        }

        etCardName.setOnFocusChangeListener { view, p1 ->
            if (p1) {
                view.etCardName.setBackgroundResource(R.drawable.bg_seller_blue_outline)
            } else {
                view.etCardName.setBackgroundResource(R.drawable.login_background)
            }
        }

    }


    private fun focus() {
        tvDateYear.requestFocus()

        eTCardNumber.setOnFocusChangeListener { view, p1 ->
            if (p1) {
                eTCardNumber.setBackgroundResource(R.drawable.bg_edit_selected)
            } else {
                eTCardNumber.setBackgroundResource(R.drawable.login_background)
            }
        }


        eTextCvv.setOnFocusChangeListener { view, p1 ->
            if (p1) {
                eTextCvv.setBackgroundResource(R.drawable.bg_edit_selected)
            } else {
                eTextCvv.setBackgroundResource(R.drawable.login_background)
            }
        }

        etCardName.setOnFocusChangeListener { view, p1 ->
            if (p1) {
                view.etCardName.setBackgroundResource(R.drawable.bg_edit_selected)
            } else {
                view.etCardName.setBackgroundResource(R.drawable.login_background)
            }
        }

    }

    private fun tokenizeCard(card: Card) {
        val cardSourceParams = SourceParams.createCardParams(card)
//        stripe.createSource(
//            cardSourceParams,
//            object : ApiResultCallback<Source> {
//                override fun onSuccess(source: Source) {
////                    MyServer.chargeToken(token)
//                }
//
//                override fun onError(error: Exception) {
//                    Toast.makeText(this@AddCardActivity,
//                        error.localizedMessage,
//                        Toast.LENGTH_LONG).show()
//                }
//            })

    }


    private fun setSpan() {


        val isSeller = Constant.getSharedPrefs(this).getBoolean(Constant.IS_SELLER, false)
        if (isSeller) {
            val spannableString =
                SpannableString(" By Tapping Add Card you have agreed to Upscibbr's Terms & Conditions and Privacy Policy and the stripe Connected Account")

            //1st click
            spannableString.setSpan(
                clickableSpanforSigleText, 51, 69, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )
            //2nd click
            spannableString.setSpan(
                clickableSpanforPrivacyPolicy, 74, 88, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )

            //3rd click
            spannableString.setSpan(
                clickableSpanforStripeConnected, 97, 121, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )


            textView159.movementMethod = LinkMovementMethod.getInstance()


            spannableString.setSpan(
                ForegroundColorSpan(resources.getColor(R.color.blue)),
                51, 69,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            )

            spannableString.setSpan(
                ForegroundColorSpan(resources.getColor(R.color.blue)),
                74, 88,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            )

            spannableString.setSpan(
                ForegroundColorSpan(resources.getColor(R.color.blue)),
                97, 121,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            )


            spannableString.setSpan(UnderlineSpan(), 51, 69, 0)
            textView159.text = spannableString

            spannableString.setSpan(UnderlineSpan(), 74, 88, 0)
            textView159.text = spannableString

            spannableString.setSpan(UnderlineSpan(), 97, 121, 0)
            textView159.text = spannableString

        } else {
            val spannableString = SpannableString(" By Tapping Add Card you have agreed to Upscibbr's Terms & Conditions and Privacy Policy and the Powered by Stripe")

            //1st click
            spannableString.setSpan(
                clickableSpanforSigleText, 51, 69, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )
            //2nd click
            spannableString.setSpan(
                clickableSpanforPrivacyPolicy, 74, 88, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )

            spannableString.setSpan(
                clickableSpanforStripeConnected, 97, 114, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
            )


            textView159.movementMethod = LinkMovementMethod.getInstance()



            spannableString.setSpan(
                ForegroundColorSpan(resources.getColor(R.color.colorSkip)),
                51, 69,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            )


            spannableString.setSpan(
                ForegroundColorSpan(resources.getColor(R.color.colorSkip)),
                74, 88,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            )

            spannableString.setSpan(
                ForegroundColorSpan(resources.getColor(R.color.colorSkip)),
                97, 114,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            )



            spannableString.setSpan(UnderlineSpan(), 51, 69, 0)
            textView159.text = spannableString

            spannableString.setSpan(UnderlineSpan(), 74, 88, 0)
            textView159.text = spannableString

            spannableString.setSpan(UnderlineSpan(), 97, 114, 0)
            textView159.text = spannableString
        }


    }

    //set the click on the spannable
    val clickableSpanforSigleText = object : ClickableSpan() {
        @SuppressLint("SetJavaScriptEnabled")
        override fun onClick(textView: View) {
            startActivity(
                Intent(
                    this@AddCardActivity,
                    WebPagesOpenActivity::class.java
                ).putExtra("terms", "terms")
            )
//            val theWebPage = WebView(this@AddCardActivity)
//            theWebPage.settings.javaScriptEnabled = true
//            theWebPage.settings.pluginState = WebSettings.PluginState.ON
//            setContentView(theWebPage)
//            theWebPage.loadUrl("https://www.upscribbr.com/terms/")

        }

        override fun updateDrawState(ds: TextPaint) {
            super.updateDrawState(ds)
            ds.isUnderlineText = false
        }
    }

    //set the click on the spannable
    val clickableSpanforPrivacyPolicy = object : ClickableSpan() {
        @SuppressLint("SetJavaScriptEnabled")
        override fun onClick(textView: View) {
            startActivity(
                Intent(
                    this@AddCardActivity,
                    WebPagesOpenActivity::class.java
                ).putExtra("privacy", "privacy")
            )
//            val theWebPage = WebView(this@AddCardActivity)
//            theWebPage.settings.javaScriptEnabled = true
//            theWebPage.settings.pluginState = WebSettings.PluginState.ON
//            setContentView(theWebPage)
//            theWebPage.loadUrl("https://www.upscribbr.com/privacy-policy/")


        }

        override fun updateDrawState(ds: TextPaint) {
            super.updateDrawState(ds)
            ds.isUnderlineText = false
        }
    }

    val clickableSpanforStripeConnected = object : ClickableSpan() {
        @SuppressLint("SetJavaScriptEnabled")
        override fun onClick(textView: View) {
            startActivity(
                Intent(
                    this@AddCardActivity,
                    WebPagesOpenActivity::class.java
                ).putExtra("legal", "legal")
            )
//            val theWebPage = WebView(this@AddCardActivity)
//            theWebPage.settings.javaScriptEnabled = true
//            theWebPage.settings.pluginState = WebSettings.PluginState.ON
//            setContentView(theWebPage)
//            theWebPage.loadUrl("https://stripe.com/en-ca/connect-account/legal/")


        }

        override fun updateDrawState(ds: TextPaint) {
            super.updateDrawState(ds)
            ds.isUnderlineText = false
        }
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}
