package com.upscribber.payment.paymentCards

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.upscribber.R
import kotlinx.android.synthetic.main.activity_card_confirmation.*

class CardConfirmationActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_card_confirmation)
        clickListeners()
    }

    private fun clickListeners() {
        done.setOnClickListener {
            finish()
        }

    }
}
