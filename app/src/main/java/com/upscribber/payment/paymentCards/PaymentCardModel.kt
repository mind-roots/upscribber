package com.upscribber.payment.paymentCards

import android.os.Parcel
import android.os.Parcelable

class PaymentCardModel() :Parcelable {
     var value2: String = ""
    var status: String = ""
    var id: String = ""
    var name = ""
    var last4 = ""
    var brand = ""
    var account_holder_name = ""
    var account_holder_type = ""
    var bank_name = ""
    var country = ""
    var currency = ""
    var routing_number = ""
    var card : Boolean = false

    constructor(parcel: Parcel) : this() {
        value2 = parcel.readString()
        status = parcel.readString()
        id = parcel.readString()
        name = parcel.readString()
        last4 = parcel.readString()
        brand = parcel.readString()
        account_holder_name = parcel.readString()
        account_holder_type = parcel.readString()
        bank_name = parcel.readString()
        country = parcel.readString()
        currency = parcel.readString()
        routing_number = parcel.readString()
        card = parcel.readByte() != 0.toByte()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(value2)
        parcel.writeString(status)
        parcel.writeString(id)
        parcel.writeString(name)
        parcel.writeString(last4)
        parcel.writeString(brand)
        parcel.writeString(account_holder_name)
        parcel.writeString(account_holder_type)
        parcel.writeString(bank_name)
        parcel.writeString(country)
        parcel.writeString(currency)
        parcel.writeString(routing_number)
        parcel.writeByte(if (card) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PaymentCardModel> {
        override fun createFromParcel(parcel: Parcel): PaymentCardModel {
            return PaymentCardModel(parcel)
        }

        override fun newArray(size: Int): Array<PaymentCardModel?> {
            return arrayOfNulls(size)
        }
    }

}
