package com.upscribber.payment.paymentCards

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.webkit.WebView
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import com.upscribber.R
import kotlinx.android.synthetic.main.activity_web_pages_open.*

class WebPagesOpenActivity : AppCompatActivity() {

    lateinit var toolbar: Toolbar
    lateinit var toolbarTitle: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web_pages_open)
        init()
        setToolbar()
        setData()
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun setData() {
        textWebView.settings.javaScriptEnabled = true
        textWebView.settings.loadWithOverviewMode = true
        textWebView.settings.useWideViewPort = true
        textWebView.scrollBarStyle = WebView.SCROLLBARS_OUTSIDE_OVERLAY
        textWebView.isScrollbarFadingEnabled = false

        if (intent.hasExtra("legal")){
            textWebView.loadUrl("https://stripe.com/en-ca/connect-account/legal")
        }else if(intent.hasExtra("privacy")){
            textWebView.loadUrl("https://app.upscribbr.com/privacy-policy")
        }else if(intent.hasExtra("terms")){
            textWebView.loadUrl("https://app.upscribbr.com/terms-and-conditions/")
        }else if (intent.hasExtra("help")){
            textWebView.loadUrl("https://app.upscribbr.com/FAQs-customer")
        }else if (intent.hasExtra("helpMerchant")){
            textWebView.loadUrl("https://app.upscribbr.com/FAQs-merchant")
        }else if (intent.hasExtra("services")){
            textWebView.loadUrl("file:///android_asset/terms.html")
        }else if (intent.hasExtra("aboutUs")){
            textWebView.loadUrl("https://www.upscribbr.com")
        }else if (intent.hasExtra("webTerms")){
            textWebView.loadUrl("https://www.upscribbr.com/terms")
        }


    }

    private fun init() {
        toolbar = findViewById(R.id.toolbar)
        toolbarTitle = findViewById(R.id.title)

    }

    private fun setToolbar() {
        setSupportActionBar(toolbar)
        title = ""
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)
        toolbarTitle.text = ""
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }

        return super.onOptionsItemSelected(item)
    }
}


