package com.upscribber.payment.paymentCards

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.upscribber.commonClasses.ModelStatusMsg

class AddCardViewModel(application: Application) : AndroidViewModel(application) {

    var addCardRepository: AddCardRepository = AddCardRepository(application)

    fun getStripeData(
        cardNumber: String,
        cardMonth: String,
        cardYear: String,
        cardCvc: String,
        name: String,
        value: String,
        value2: String
    ) {
        addCardRepository.getStripeData(
            cardNumber,
            cardMonth,
            cardYear,
            cardCvc,
            name,
            value,
            value2
        )

    }

    fun getmDataAllBrands(): LiveData<PaymentCardModel> {
        return addCardRepository.getmDataDetails()
    }


    fun getmRetrieveCard(): LiveData<ArrayList<PaymentCardModel>> {
        return addCardRepository.getmRetrieveCard()
    }


    fun getmDataFailure(): LiveData<ModelStatusMsg> {
        return addCardRepository.getmDataFailure()
    }


    fun getmDataCard(): LiveData<ModelStatusMsg> {
        return addCardRepository.getmDataCard()
    }

    fun getRetrieveCards(cusId: String) {
        addCardRepository.getRetrieveCards(cusId)

    }

    fun getStripeResponse(stripeAccountId: String, type : String) {
        addCardRepository.getBankCardDetails(stripeAccountId,type)

    }

    fun getDataCard(): MutableLiveData<ArrayList<PaymentCardModel>> {
        return addCardRepository.getmDataCardData()
    }


}