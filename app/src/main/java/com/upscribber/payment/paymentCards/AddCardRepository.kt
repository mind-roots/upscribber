package com.upscribber.payment.paymentCards

import android.app.Application
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.android.tools.build.jetifier.core.utils.Log
import com.upscribber.commonClasses.Constant
import com.upscribber.commonClasses.ModelStatusMsg
import com.upscribber.commonClasses.WebServicesCustomers
import com.upscribber.commonClasses.WebServicesMerchants
import com.upscribber.profile.ModelGetProfile
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class AddCardRepository(var application: Application) {

    private val mDataStripeDataCard = MutableLiveData<ArrayList<PaymentCardModel>>()
    private lateinit var arrayHomeProfile: ModelGetProfile
    private val mDataDetail = MutableLiveData<PaymentCardModel>()
    val mDataFailure = MutableLiveData<ModelStatusMsg>()
    val mDataCard = MutableLiveData<ModelStatusMsg>()
    val mRetrieveCard = MutableLiveData<ArrayList<PaymentCardModel>>()


    fun getmDataFailure(): LiveData<ModelStatusMsg> {
        return mDataFailure
    }

    fun getmDataCard(): LiveData<ModelStatusMsg> {
        return mDataCard
    }


    fun getmRetrieveCard(): LiveData<ArrayList<PaymentCardModel>> {
        return mRetrieveCard
    }

    fun getStripeData(
        cardNumber: String,
        cardMonth: String,
        cardYear: String,
        cardCvc: String,
        name: String,
        value: String,
        value2: String
    ) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.stripeTokenUrl)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> =
            service.tokens(cardNumber, cardMonth, cardYear, cardCvc, name,"USD")
        call.enqueue(object : Callback<ResponseBody> {


            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val cardModel = PaymentCardModel()
                        val id = json.optString("id")
                        cardModel.id = id
                        var stripe_customer_id = ""

                        if (value == "merchantCheckout" || value == "merchantCheckOutt") {
                            stripe_customer_id = Constant.getPrefs(application)
                                .getString(Constant.stripe_new_customer_from_merchant, "")
                            getCustomerStripe(stripe_customer_id, id, value, value2)
                        } else if (value == "paymentsProfile") {
                            addCardPaymentsProfile(id)

                        } else {
                            arrayHomeProfile =
                                Constant.getArrayListProfile(application, Constant.dataProfile)
                            stripe_customer_id = arrayHomeProfile.stripe_customer_id
                            getCustomerStripe(stripe_customer_id, id, value, value2)
                        }


//                        mDataDetail.value = cardModel

                    } catch (e: Exception) {
                        val modelStatus = ModelStatusMsg()
                        modelStatus.status = "false"
                        mDataFailure.value = modelStatus
                        e.printStackTrace()
                    }
                } else {
                    val modelStatus = ModelStatusMsg()
                    modelStatus.status = "false"
                    mDataFailure.value = modelStatus
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                mDataFailure.value = modelStatus
                Toast.makeText(application, "Error!", Toast.LENGTH_LONG).show()
            }
        })
    }


    fun addCardPaymentsProfile(id: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()
        val auth = Constant.getPrefs(application).getString(Constant.auth_code, "")

        val service = retrofit.create(WebServicesMerchants::class.java)
        val call: Call<ResponseBody> = service.updateExternalCardAccount(
            auth,
            "0",
            "2",
            id
        )
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                try {
                    if (response.isSuccessful) {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        if (status == "true") {
                            val modelStatusMsg = ModelStatusMsg()
                            modelStatusMsg.status = status
                            modelStatusMsg.msg = msg
                            mDataCard.value = modelStatusMsg
                        } else {
                            val modelStatusMsg = ModelStatusMsg()
                            modelStatusMsg.status = status
                            modelStatusMsg.msg = msg
                            mDataFailure.value = modelStatusMsg
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatusMsg = ModelStatusMsg()
                modelStatusMsg.status = "false"
                modelStatusMsg.msg = "Network Error"
                mDataFailure.value = modelStatusMsg
            }
        })
    }


    private fun getCustomerStripe(
        stripeCustomerId: String,
        tokenId: String,
        value: String,
        value2: String
    ) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.stripeBaseUrl)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.stripe_customers(stripeCustomerId, tokenId)
        call.enqueue(object : Callback<ResponseBody> {


            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val cardModel = PaymentCardModel()
                        Log.e("cardCustomer:=", json.toString())
                        cardModel.id = json.optString("id")
                        cardModel.brand = json.optString("brand")
                        cardModel.last4 = json.optString("last4")
                        cardModel.account_holder_name = json.optString("account_holder_name")
                        cardModel.value2 = value2
                        cardModel.name = json.optString("name")

                        when (value) {
                            "merchantCheckout" -> {
                                Constant.cardArrayListDataMerchant.add(cardModel)
                            }
                            "merchantCheckOutt" -> {
                                Constant.cardArrayListDataMerchant.add(cardModel)
                            }
                            else -> {
                                Constant.cardArrayListData.add(cardModel)
                            }
                        }

                        mDataDetail.value = cardModel

                    } catch (e: Exception) {
                        val modelStatus = ModelStatusMsg()
                        modelStatus.status = "false"
                        mDataFailure.value = modelStatus
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                mDataFailure.value = modelStatus
                Toast.makeText(application, "Error!", Toast.LENGTH_LONG).show()

            }
        })
    }


    fun getmDataDetails(): LiveData<PaymentCardModel> {
        return mDataDetail
    }

    fun getRetrieveCards(cusId: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.stripeBaseUrl)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> =
            service.stripeRetrieve(cusId, "100")
        call.enqueue(object : Callback<ResponseBody> {


            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {

                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)

                        val data = json.optJSONArray("data")

                        Constant.cardArrayListDataMerchant.clear()

                        for (i in 0 until data.length()) {
                            val stripeData = data.getJSONObject(i)
                            val cardModel = PaymentCardModel()
                            cardModel.id = stripeData.optString("id")
                            cardModel.brand = stripeData.optString("brand")
                            cardModel.country = stripeData.optString("country")
                            cardModel.last4 = stripeData.optString("last4")
                            cardModel.account_holder_name =
                                stripeData.optString("account_holder_name")
                            cardModel.name = stripeData.optString("name")
                            Constant.cardArrayListDataMerchant.add(cardModel)
                            val editor = Constant.getPrefs(application).edit()
                            editor.putString(Constant.stripe_customer_name, cardModel.name)
                            editor.apply()
                        }

                        mRetrieveCard.value = Constant.cardArrayListDataMerchant

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {

                Toast.makeText(application, "Error!", Toast.LENGTH_LONG).show()
            }
        })
    }

    fun getBankCardDetails(stripeAccountId: String, type: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.stripeTokenUrl)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.paymentCardBank(stripeAccountId, type)
        call.enqueue(object : Callback<ResponseBody> {


            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val arrayData = ArrayList<PaymentCardModel>()
                        val cardModel = PaymentCardModel()
                        val data = json.optJSONArray("data")
                        for (i in 0 until data.length()) {
                            val stripeData = data.getJSONObject(i)
                            cardModel.id = stripeData.optString("id")
                            cardModel.brand = stripeData.optString("brand")
                            cardModel.country = stripeData.optString("country")
                            cardModel.last4 = stripeData.optString("last4")
                            arrayData.add(cardModel)
                        }

                        mDataStripeDataCard.value = arrayData
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Toast.makeText(application, "Error!", Toast.LENGTH_LONG).show()
            }
        })

    }

    fun getmDataCardData(): MutableLiveData<ArrayList<PaymentCardModel>> {
        return mDataStripeDataCard
    }


}