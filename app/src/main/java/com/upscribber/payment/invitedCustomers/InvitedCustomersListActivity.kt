package com.upscribber.payment.invitedCustomers

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.upscribber.R
import com.upscribber.databinding.ActivityInvitedCustomersListBinding
import com.upscribber.payment.customerPayments.ModelCustomers

class InvitedCustomersListActivity : AppCompatActivity() {

    private lateinit var mAdapter: AdapterCustomerListInvite
    lateinit var mBinding : ActivityInvitedCustomersListBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this,R.layout.activity_invited_customers_list)
        setToolbar()
        setAdapter()

    }

    private fun setAdapter() {
        val arrayList = intent.getParcelableExtra<ModelCustomers>("customerList")
        mBinding.recyclerInvitedCustomers.layoutManager = LinearLayoutManager(this)
        mAdapter = AdapterCustomerListInvite(this,arrayList.customerDataArray)
        mBinding.recyclerInvitedCustomers.adapter = mAdapter


    }

    private fun setToolbar() {
        setSupportActionBar(mBinding.include12.toolbar)
        title = ""
        mBinding.include12.title.text = "Invited Customers"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)

    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}
