package com.upscribber.payment.invitedCustomers

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.upscribber.upscribberSeller.customerBottom.profile.CustomerProfileInfoActivity
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.upscribberSeller.customerBottom.scanning.customerlistselection.ModelScanCustomers
import kotlinx.android.synthetic.main.customer_recycler_list.view.*

class AdapterCustomerListInvite(
    var context: Context,
    var itemss : ArrayList<ModelScanCustomers>
) : RecyclerView.Adapter<AdapterCustomerListInvite.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(context)
            .inflate(R.layout.customer_recycler_list, parent, false)
        return ViewHolder(v)

    }

    override fun getItemCount(): Int {
        return  itemss.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = itemss[position]
        val imagePath  = Constant.getPrefs(context).getString(Constant.dataImagePath, "")
        holder.itemView.textView198.text = model.name
        holder.itemView.customerNumber.text = Constant.formatPhoneNumber(
            model.contact_no
        )
        Glide.with(context).load(imagePath + model.profile_image).placeholder(R.mipmap.circular_placeholder).into(holder.itemView.imageView)

        holder.itemView.setOnClickListener {
            context.startActivity(Intent(context,CustomerProfileInfoActivity::class.java).putExtra("modelList",model).putExtra("customerList","customers"))
        }

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)


}