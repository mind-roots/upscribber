package com.upscribber.payment

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import kotlinx.android.synthetic.main.payment_layout.view.*

class paymentTransAdapter(
    private val mContext: Context,
   var  stringExtra: String

) : RecyclerView.Adapter<paymentTransAdapter.MyViewHolder>()
    {

        private var list: ArrayList<PaymentTransModel> = ArrayList()
        var listener = mContext as ClickActivity

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
            val v = LayoutInflater.from(mContext).inflate(R.layout.payment_layout, parent, false)
            return MyViewHolder(v)

        }

        override fun getItemCount(): Int {
            return list.size

        }

        override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
            val model = list[position]
            val date = Constant.getDateMonthYear(model.created_at)
            val time = Constant.getTime(model.created_at)

            holder.itemView.textView30.text = model.campaign_name
            holder.itemView.textView32.text = model.total_amount
            holder.itemView.textView33.text = date
            holder.itemView.paymentTime.text = time
            holder.itemView.textView36.text = model.CustomerName

//            if (model.status == 0){
//                holder.typePayment.setBackgroundResource(R.drawable.bg_refund)
//            }else
                if (model.transaction_status == "1"){
                holder.itemView.typePayment.text = "PURCHASE"
                holder.typePayment.setBackgroundResource(R.drawable.bg_sea_green_rounded)
            }
                else if (model.transaction_status == "4"){
                    holder.itemView.typePayment.text = "REFUND COMPLETE"
                    holder.typePayment.setBackgroundResource(R.drawable.bg_refund)
//                holder.typePayment.setBackgroundResource(R.drawable.bg_yellow_rounded)
            }
                else if (model.transaction_status == "3") {
                holder.itemView.typePayment.text = "REFUND"
                holder.typePayment.setBackgroundResource(R.drawable.bg_refund)
            }else{
                holder.typePayment.setBackgroundResource(R.drawable.bg_pink_rounded)
            }

            if (stringExtra == "deatils"){
                holder.itemView.textView36.visibility = View.VISIBLE

            }else{
                holder.itemView.textView36.visibility = View.GONE
            }

            holder.card_view4.setOnClickListener {
                listener.openDetails(model)
            }
        }

        fun update(it: java.util.ArrayList<PaymentTransModel>?) {
            if (it != null) {
                list = it
            }
            notifyDataSetChanged()

        }

        class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
            var card_view4= itemView.findViewById<View>(R.id.card_view4)
            var typePayment= itemView.findViewById<TextView>(R.id.typePayment)
            var textView36= itemView.findViewById<TextView>(R.id.textView36)
        }

        interface ClickActivity{
            fun openDetails(model: PaymentTransModel)
        }
    }