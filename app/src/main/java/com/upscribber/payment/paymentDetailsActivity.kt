package com.upscribber.payment

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.graphics.Outline
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.*
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.makeramen.roundedimageview.RoundedImageView
import com.upscribber.commonClasses.Constant
import com.upscribber.commonClasses.RoundedBottomSheetDialogFragment
import com.upscribber.R
import com.upscribber.payment.customerPayments.ModelCustomers
import com.upscribber.payment.customerPayments.PaymentsViewModel
import com.upscribber.payment.invitedCustomers.InvitedCustomersListActivity
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.activity_payment_details.*

class paymentDetailsActivity : AppCompatActivity() {

    lateinit var toolbar: Toolbar
    lateinit var toolbarTitle: TextView
    lateinit var tvRefund: TextView
    lateinit var textView73: TextView
    lateinit var textView81: TextView
    lateinit var textView80: TextView
    lateinit var view12: View
    lateinit var view: View
    lateinit var cancelReason: TextView
    lateinit var linearCustomers: LinearLayout
    lateinit var cancelReasonspecific: TextView
    lateinit var mViewModel: PaymentsViewModel
    lateinit var modelCustomers: ModelCustomers

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment_details)
        mViewModel = ViewModelProviders.of(this)[PaymentsViewModel::class.java]
        initz()
        setToolbar()
        clickList()
        setData()
        observerInit()


    }

    private fun observerInit() {
        mViewModel.getStatus().observe(this, Observer {
            progressBar25.visibility = View.GONE
            if (it.msg.isNotEmpty()) {
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
            }
        })

        mViewModel.getmDataGenerateRefund().observe(this, Observer {
            progressBar25.visibility = View.GONE
            startActivity(Intent(this, RefundFlowActivity::class.java))
            finish()
        })

        mViewModel.getmDataRefund().observe(this, Observer {
            progressBar25.visibility = View.GONE
            startActivityForResult(Intent(this, RefundFlowActivity::class.java).putExtra("from","activityPayment"),1)
        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == 1){
            val intent=Intent()
            setResult(Activity.RESULT_OK,intent)
            finish()
        }
    }

    @SuppressLint("SetTextI18n")
    private fun setData() {
        val isSeller = Constant.getSharedPrefs(this).getBoolean(Constant.IS_SELLER, false)
        val modelPayments = intent.getParcelableExtra<PaymentTransModel>("position")
        textBusinessName.text = modelPayments.campaign_name
        textDescription.text = modelPayments.description
        textFrequency.text = modelPayments.frequency_type
        if (modelPayments.redeemtion_cycle_qty == "500000") {
            textQuantity.text = "Unlimited"
        } else {
            textQuantity.text = modelPayments.redeemtion_cycle_qty
        }
        name.text = modelPayments.customer_name
        merchantName.text = modelPayments.business_name
        textAmount.text = "$" + modelPayments.total_amount

        val date = Constant.getDateMonthYear(modelPayments.created_at)
        val time = Constant.getTime(modelPayments.created_at)
        textDate.text = date
        textTime.text = time
        textView81.text = "xxxx - xxxx - xxxx - " + modelPayments.last_4


        when (modelPayments.payment_method_name) {
            "visa" -> {
                textView81.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.new_visa_icon, 0, 0, 0)

            }
            "mastercard" -> {
                textView81.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_mastercard_new, 0, 0, 0)
            }
            else -> {
                textView81.setCompoundDrawablesWithIntrinsicBounds(
                    R.drawable.ic_card_dummy, 0, 0, 0)

            }
        }

        if (isSeller) {
            if (modelPayments.transaction_status == "4") {
                tvRefund.visibility = View.VISIBLE
                tvRefund.setBackgroundResource(R.drawable.blue_button_background_opacity)
            } else if (modelPayments.transaction_status == "3") {
                tvRefund.visibility = View.VISIBLE
                tvRefund.setBackgroundResource(R.drawable.bg_seller_button_blue)
            } else if (modelPayments.transaction_status == "1") {
                tvRefund.visibility = View.GONE
            }
        } else {
            if (modelPayments.transaction_status == "4" && modelPayments.is_refunded == "0" || modelPayments.transaction_status == "1" && modelPayments.is_refunded == "1") {
                tvRefund.setBackgroundResource(R.drawable.invite_button_background_opacity)
                tvRefund.visibility = View.VISIBLE
            } else if (modelPayments.transaction_status == "3" && modelPayments.is_refunded == "0") {
                tvRefund.visibility = View.GONE
            } else {
                tvRefund.visibility = View.VISIBLE
                tvRefund.setBackgroundResource(R.drawable.invite_button_background)
            }
        }


        if (modelPayments.transaction_status == "3") {
            textView73.setBackgroundResource(R.drawable.bg_refund)
            textView73.text = "REFUND"
            cancelReason.visibility = View.GONE
            cancelReasonspecific.visibility = View.GONE
            textView80.visibility = View.VISIBLE
            textView81.visibility = View.VISIBLE
            view12.visibility = View.VISIBLE
            linearCustomers.visibility = View.GONE
            view.visibility = View.GONE
        }
        else if (modelPayments.transaction_status == "1") {
            textView73.text = "PURCHASED"
            textView73.setBackgroundResource(R.drawable.bg_sea_green_rounded)
            cancelReason.visibility = View.GONE
            cancelReasonspecific.visibility = View.GONE
            textView80.visibility = View.VISIBLE
            textView81.visibility = View.VISIBLE
            view12.visibility = View.VISIBLE
            linearCustomers.visibility = View.GONE
            view.visibility = View.GONE
        } else if (modelPayments.transaction_status == "4") {
            textView73.text = "REFUND COMPLETE"
            textView73.setBackgroundResource(R.drawable.bg_refund)
            cancelReason.visibility = View.GONE
            cancelReasonspecific.visibility = View.GONE
            textView80.visibility = View.VISIBLE
            textView81.visibility = View.VISIBLE
            view12.visibility = View.VISIBLE
            linearCustomers.visibility = View.GONE
            view.visibility = View.GONE


        }
    }


    private fun setToolbar() {
        val modelPayments = intent.getParcelableExtra<PaymentTransModel>("position")
        setSupportActionBar(toolbar)
        title = ""
        toolbarTitle.text = modelPayments.business_name
//        toolbarTitle.text =  "Payments"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)

    }

    private fun clickList() {
        val modelPayments = intent.getParcelableExtra<PaymentTransModel>("position")
        tvRefund.setOnClickListener {
                if (modelPayments.is_refunded != "1" && modelPayments.transaction_status != "4") {
                    val fragment = MenuFragment(modelPayments, mViewModel,
                        progressBar25 as LinearLayout
                    )
                    fragment.show(supportFragmentManager, fragment.tag)
                }

        }

        linearCustomers.setOnClickListener {
            startActivity(
                Intent(
                    this,
                    InvitedCustomersListActivity::class.java
                ).putExtra("customerList", modelCustomers)
            )
        }

    }

    private fun initz() {
        toolbar = findViewById(R.id.toolbar)
        toolbarTitle = findViewById(R.id.title)
        tvRefund = findViewById(R.id.tvRefund)
        textView73 = findViewById(R.id.textView73)
        cancelReason = findViewById(R.id.cancelReason)
        cancelReasonspecific = findViewById(R.id.cancelReasonspecific)
        textView80 = findViewById(R.id.textView80)
        textView81 = findViewById(R.id.textView81)
        view12 = findViewById(R.id.view12)
        view = findViewById(R.id.view)
        linearCustomers = findViewById(R.id.linearCustomers)

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    class MenuFragment(
        var modelPayments: PaymentTransModel,
        var mViewModel: PaymentsViewModel,
        var progressBar25: LinearLayout
    ) : RoundedBottomSheetDialogFragment() {

        lateinit var imageView28: ImageView

        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val view = inflater.inflate(R.layout.payment_confirmation_sheet, container, false)
            val yes = view.findViewById<TextView>(R.id.yes)
            val imagePath = Constant.getPrefs(activity!!).getString(Constant.dataImagePath, "")
            imageView28 = view.findViewById(R.id.imageView28)
            val imageView16 = view.findViewById<ImageView>(R.id.imageView16)
            val imageCircle = view.findViewById<RoundedImageView>(R.id.imageCircle)
            val textView97 = view.findViewById<TextView>(R.id.textView97)
            val textView17 = view.findViewById<TextView>(R.id.textView17)
            val textView86 = view.findViewById<TextView>(R.id.textView86)
            val textView124 = view.findViewById<TextView>(R.id.textView124)
            val textView125 = view.findViewById<TextView>(R.id.textView125)
            val textView130 = view.findViewById<TextView>(R.id.textView130)
            val textView81 = view.findViewById<TextView>(R.id.textView81)
            val no = view.findViewById<TextView>(R.id.no)
            val view6 = view.findViewById<View>(R.id.view6)
            roundImage()
            Glide.with(activity!!).load(imagePath + modelPayments.profile_image)
                .placeholder(R.mipmap.placeholder_subscription_square).into(imageCircle)
            textView124.text = modelPayments.business_name
            textView130.text = "$" + modelPayments.total_amount

            val isSeller = Constant.getSharedPrefs(activity!!).getBoolean(Constant.IS_SELLER, false)
            if (isSeller) {
                yes.setBackgroundResource(R.drawable.bg_seller_button_blue)
                textView97.setTextColor(resources.getColor(R.color.colorWhite))
                textView97.text =
                    resources.getString(R.string.are_you_sure_you_want_to_process_refund)
                imageView16.setImageDrawable(resources.getDrawable(R.mipmap.redeem_one))
                textView17.text = modelPayments.campaign_name
                textView124.text = modelPayments.customer_name
                textView125.text = "Name"
                textView86.text = "Subscription"
                imageCircle.visibility = View.VISIBLE
                imageCircle.setImageResource(R.mipmap.profile_image)
                view6.visibility = View.GONE
                textView81.visibility = View.GONE
                textView17.visibility = View.VISIBLE
                imageView16.visibility = View.VISIBLE
                textView86.visibility = View.VISIBLE
                imageView28.setImageResource(R.mipmap.bg_popup)

            } else {
                yes.setBackgroundResource(R.drawable.invite_button_background)
                textView97.setTextColor(resources.getColor(R.color.colorWhite))
                textView97.text =
                    resources.getString(R.string.are_you_sure_you_want_to_process_refund1)
                imageCircle.setImageResource(R.drawable.ic_lotus_back)
                imageView28.setImageResource(R.mipmap.ms_bg)
                textView124.text = modelPayments.campaign_name
                textView125.text = "Subscription"
                imageCircle.visibility = View.VISIBLE
                textView81.visibility = View.VISIBLE

                textView17.visibility = View.GONE
                imageView16.visibility = View.GONE
                textView86.visibility = View.GONE


                view6.visibility = View.GONE


            }

            val auth = Constant.getPrefs(activity!!).getString(Constant.auth_code, "")
            yes.setOnClickListener {
                progressBar25.visibility = View.VISIBLE
                if (isSeller) {
                    mViewModel.generateRefund(
                        auth,
                        modelPayments.transaction_id,
                        "",
                        "",
                        "",
                        "2"
                    )
                } else {
                    mViewModel.getRefund(auth, modelPayments.order_id, modelPayments.merchant_id, modelPayments.transaction_id)
                }
                dismiss()
            }

            no.setOnClickListener {
                dismiss()
            }

            return view
        }

        private fun roundImage() {
            val curveRadius = 50F

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                imageView28.outlineProvider = object : ViewOutlineProvider() {

                    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                    override fun getOutline(view: View?, outline: Outline?) {
                        outline?.setRoundRect(
                            0,
                            0,
                            view!!.width,
                            view.height + curveRadius.toInt(),
                            curveRadius
                        )
                    }
                }
                imageView28.clipToOutline = true
            }

        }

    }


//    class MenuFragment1(
//        var modelPayments: ActivityModel,
//        var mViewModel: PaymentsViewModel,
//        var progressBar25: ProgressBar
//    ) : RoundedBottomSheetDialogFragment() {
//
//        lateinit var imageView28: ImageView
//
//        override fun onCreateView(
//            inflater: LayoutInflater,
//            container: ViewGroup?,
//            savedInstanceState: Bundle?
//        ): View? {
//            val view = inflater.inflate(R.layout.payment_confirmation_sheet, container, false)
//            val yes = view.findViewById<TextView>(R.id.yes)
//            val imagePath = Constant.getPrefs(activity!!).getString(Constant.dataImagePath, "")
//            imageView28 = view.findViewById(R.id.imageView28)
//            val imageView16 = view.findViewById<ImageView>(R.id.imageView16)
//            val imageCircle = view.findViewById<CircleImageView>(R.id.imageCircle)
//            val textView97 = view.findViewById<TextView>(R.id.textView97)
//            val textView17 = view.findViewById<TextView>(R.id.textView17)
//            val textView86 = view.findViewById<TextView>(R.id.textView86)
//            val textView124 = view.findViewById<TextView>(R.id.textView124)
//            val textView125 = view.findViewById<TextView>(R.id.textView125)
//            val textView130 = view.findViewById<TextView>(R.id.textView130)
//            val textView81 = view.findViewById<TextView>(R.id.textView81)
//            val no = view.findViewById<TextView>(R.id.no)
//            val view6 = view.findViewById<View>(R.id.view6)
//            roundImage()
//            Glide.with(activity!!).load(imagePath + modelPayments.model.profile_image)
//                .placeholder(R.mipmap.circular_placeholder).into(imageCircle)
//            textView124.text = modelPayments.model.business_name
//            textView130.text = "$" + modelPayments.model.total_amount
//
//            val isSeller = Constant.getSharedPrefs(activity!!).getBoolean(Constant.IS_SELLER, false)
//            if (isSeller) {
//                yes.setBackgroundResource(R.drawable.bg_seller_button_blue)
//                textView97.setTextColor(resources.getColor(R.color.colorWhite))
//                textView97.text =
//                    resources.getString(R.string.are_you_sure_you_want_to_process_refund)
//                imageView16.setImageDrawable(resources.getDrawable(R.mipmap.redeem_one))
//                textView17.text = "Ultra Spot Remover"
//                textView124.text = "James C.Culen"
//                textView125.text = "Name"
//                textView86.text = "Subscription"
//                imageCircle.visibility = View.VISIBLE
//                imageCircle.setImageResource(R.mipmap.profile_image)
//                view6.visibility = View.GONE
//                textView81.visibility = View.GONE
//                textView17.visibility = View.VISIBLE
//                imageView16.visibility = View.VISIBLE
//                textView86.visibility = View.VISIBLE
//                imageView28.setImageResource(R.mipmap.bg_popup)
//
//            } else {
//                yes.setBackgroundResource(R.drawable.invite_button_background)
//                textView97.setTextColor(resources.getColor(R.color.colorWhite))
//                textView97.text =
//                    resources.getString(R.string.are_you_sure_you_want_to_process_refund1)
//                imageCircle.setImageResource(R.drawable.ic_lotus_back)
//                imageView28.setImageResource(R.mipmap.ms_bg)
////                imageView16.setImageDrawable(resources.getDrawable(R.drawable.ic_check_plan))
////                textView17.text = "James C.Culen"
//                textView124.text = "Ultra Spot Remover"
////                textView86.text = "Name"
//                textView125.text = "Subscription"
//                imageCircle.visibility = View.VISIBLE
//                textView81.visibility = View.VISIBLE
//
//                textView17.visibility = View.GONE
//                imageView16.visibility = View.GONE
//                textView86.visibility = View.GONE
//
//
//                view6.visibility = View.VISIBLE
//
//
//            }
//
//            val auth = Constant.getPrefs(activity!!).getString(Constant.auth_code, "")
//            yes.setOnClickListener {
//                progressBar25.visibility = View.VISIBLE
//                if (isSeller) {
//                    mViewModel.generateRefund(auth, modelPayments.model.transaction_id)
//                } else {
//                    mViewModel.getRefund(
//                        auth,
//                        modelPayments.model.order_id,
//                        modelPayments.model.merchant_id,
//                        modelPayments.model.transaction_id
//                    )
//                }
//
//            }
//
//            no.setOnClickListener {
//                dismiss()
//            }
//
//            return view
//        }
//
//        private fun roundImage() {
//            val curveRadius = 50F
//
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//
//                imageView28.outlineProvider = object : ViewOutlineProvider() {
//
//                    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
//                    override fun getOutline(view: View?, outline: Outline?) {
//                        outline?.setRoundRect(
//                            0,
//                            0,
//                            view!!.width,
//                            view.height + curveRadius.toInt(),
//                            curveRadius
//                        )
//                    }
//                }
//                imageView28.clipToOutline = true
//            }
//
//        }
//
//    }


}
