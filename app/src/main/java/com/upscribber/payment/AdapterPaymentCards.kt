package com.upscribber.payment

import android.content.Context
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.payment.paymentCards.PaymentCardModel
import kotlinx.android.synthetic.main.cards_layout_checkout.view.CardNumber
import kotlinx.android.synthetic.main.cards_layout_payment.view.*

class AdapterPaymentCards(private val mContext: Context, private var list: ArrayList<PaymentCardModel>) :
    RecyclerView.Adapter<AdapterPaymentCards.MyViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(mContext).inflate(R.layout.cards_layout_payment, parent, false)
        return MyViewHolder(v)

    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = list[position]
        holder.itemView.CardNumber.text = "xxxx-xxxx-xxxx-" + model.last4
        when (model.brand) {
            "visa" -> {
                holder.itemView.cardCompany.setImageResource(R.drawable.new_visa_icon)
            }
            "mastercard" -> {
                holder.itemView.cardCompany.setImageResource(R.drawable.ic_mastercard_new)
            }
            else -> {
                holder.itemView.cardCompany.setImageResource(R.drawable.ic_card_dummy)
            }
        }


        holder.itemView.cardHolderName.text = model.name

        if (list.size <= 1) {
            holder.itemView.linear.layoutParams.width = LinearLayout.LayoutParams.MATCH_PARENT
            holder.itemView.linear.requestLayout()
        }else{
            holder.itemView.linear.layoutParams.width = LinearLayout.LayoutParams.WRAP_CONTENT
            holder.itemView.linear.requestLayout()
        }



    }

    fun update(it: java.util.ArrayList<PaymentCardModel>?) {
        list = it!!
        notifyDataSetChanged()

    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}