package com.upscribber.payment

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.commonClasses.Constant
import com.google.android.material.appbar.AppBarLayout
import com.upscribber.R
import com.upscribber.payment.customerPayments.PaymentsViewModel
import com.upscribber.payment.paymentCards.AddCardActivity
import com.upscribber.profile.ModelGetProfile
import kotlinx.android.synthetic.main.activity_payment_transaction.*

class PaymentTransactionActivity : AppCompatActivity(), paymentTransAdapter.ClickActivity {

    private lateinit var adapterCards: AdapterPaymentCards
    lateinit var toolbar: Toolbar
    lateinit var CustomerListToolbar: Toolbar
    lateinit var toolbarTitle: TextView
    lateinit var title1: TextView
    lateinit var view: View
    lateinit var textView34: TextView
    lateinit var mViewModel: PaymentsViewModel
    lateinit var recyclerTransactions: RecyclerView
    lateinit var appbar: AppBarLayout
    lateinit var mAdapterPayments: paymentTransAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment_transaction)
        mViewModel = ViewModelProviders.of(this)[PaymentsViewModel::class.java]
        initz()
        setToolbar()
        setAdapter()
        clickListeners()
        apiImplimentation()
        observerInit()

    }

    private fun apiImplimentation() {
        val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
        mViewModel.getAllTransactions(auth, "1", "", "","","","customer")
        progressBar.visibility = View.VISIBLE
    }

    private fun setAdapter() {
        recyclerTransactions.layoutManager = LinearLayoutManager(this)
        mAdapterPayments = paymentTransAdapter(this, "")
        recyclerTransactions.adapter = mAdapterPayments

    }

    override fun openDetails(model: PaymentTransModel) {
        startActivityForResult(
            Intent(this, paymentDetailsActivity::class.java)
                .putExtra("position", model), 1
        )
    }


    @SuppressLint("WrongConstant")
    private fun observerInit() {
        mViewModel.getStatus().observe(this, Observer {
            progressBar.visibility = View.GONE
            if (it.msg == "Invalid auth code") {
                Constant.commonAlert(this)
            } else if (it.msg.isEmpty()) {
                noDAta.visibility = View.VISIBLE
            } else {
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
            }
        })

        mViewModel.getmAllTransactions().observe(this, Observer {
            progressBar.visibility = View.GONE
            if (it.arrayPaymentsTransactions.size > 0) {
                recyclerTransactions.visibility = View.VISIBLE
                noDAta.visibility = View.GONE
                mAdapterPayments.update(it.arrayPaymentsTransactions)
            } else {
                recyclerTransactions.visibility = View.GONE
                noDAta.visibility = View.VISIBLE
            }

        })

    }


    private fun setData() {
        recyclerCards.layoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
        adapterCards = AdapterPaymentCards(this, Constant.cardArrayListData)
        recyclerCards.adapter = adapterCards

    }

    override fun onResume() {
        super.onResume()
        if (Constant.cardArrayListData.size > 0) {
            setData()
            adapterCards.update(Constant.cardArrayListData)
        }

    }


    private fun clickListeners() {
        var modelGetProfile = ModelGetProfile()
        if (intent.hasExtra("modelProfile")) {
            modelGetProfile = intent.getParcelableExtra("modelProfile")
        } else if (intent.hasExtra("modelGetProfile")) {
            modelGetProfile = intent.getParcelableExtra("modelGetProfile")
        }

        addCardBtn.setOnClickListener {
            startActivity(
                Intent(this, AddCardActivity::class.java).putExtra("addFromPayments", "payments")
                    .putExtra("modelGetProfile", modelGetProfile)
            )
        }

        getStarted.setOnClickListener {
            startActivity(
                Intent(this, AddCardActivity::class.java).putExtra("addFromPayments", "payments")
                    .putExtra("modelGetProfile", modelGetProfile)
            )
        }



        txtViewUpdate.setOnClickListener {
            startActivity(
                Intent(this, AddCardActivity::class.java).putExtra("updateCard", "update")
                    .putExtra("modelGetProfile", modelGetProfile)
            )
        }

    }

    private fun setToolbar() {
        setSupportActionBar(toolbar)
        title = ""
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1 && resultCode == Activity.RESULT_OK ) {
            apiImplimentation()
        }

    }


    @SuppressLint("WrongConstant")
    private fun initz() {
        toolbar = findViewById(R.id.toolbar_payments)
        toolbarTitle = findViewById(R.id.title2)
        textView34 = findViewById(R.id.textView34)
        view = findViewById(R.id.view)
        recyclerTransactions = findViewById(R.id.recyclerTransactions)
        appbar = findViewById(R.id.appbar)
        toolbarTitle.text = "Payments"
        textView34.scrollTo(0, 0)
        addCardBtn.visibility = View.VISIBLE
        if (Constant.cardArrayListData.size > 0) {
            imageView85.visibility = View.GONE
            noData.visibility = View.GONE
            noDataDescription.visibility = View.GONE
            getStarted.visibility = View.GONE
            cordinatorMain.visibility = View.VISIBLE
        } else {
            imageView85.visibility = View.VISIBLE
            noData.visibility = View.VISIBLE
            noDataDescription.visibility = View.VISIBLE
            getStarted.visibility = View.GONE
            cordinatorMain.visibility = View.GONE
        }

    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }

        return super.onOptionsItemSelected(item)
    }
}
