package com.upscribber.dashboard

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.Outline
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.appcompat.widget.SwitchCompat
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.perf.FirebasePerformance
import com.google.gson.Gson
import com.squareup.otto.Subscribe
import com.upscribber.R
import com.upscribber.becomeseller.sellerWelcomeScreens.sellerWelcomActivity
import com.upscribber.categories.mainCategories.CategoryActivity
import com.upscribber.categories.subCategories.SubCategoriesActivity
import com.upscribber.checkout.ConfirmCheckoutActivity
import com.upscribber.checkout.ModelBuySubscription
//import com.upscribber.commonClasses.BusProvider
import com.upscribber.commonClasses.Constant
import com.upscribber.commonClasses.DashboardProfileActivity
import com.upscribber.commonClasses.GetSatertedClick
import com.upscribber.commonClasses.RoundedBottomSheetDialogFragment
import com.upscribber.favourites.FavouritesFragment
import com.upscribber.filters.*
import com.upscribber.filters.Redemption.RedemptionFilterFragment
import com.upscribber.helpCenter.HelpCenterActivity
import com.upscribber.home.FavoriteInterface
import com.upscribber.home.HomeFragment
import com.upscribber.home.ModelHomeDataSubscriptions
import com.upscribber.notification.notifications.NotificationActivity
import com.upscribber.payment.PaymentTransactionActivity
import com.upscribber.profile.EditProfileActivity
import com.upscribber.profile.InterfaceSwitch
import com.upscribber.profile.ProfileFragment
import com.upscribber.requestbusiness.RequestBusiness
import com.upscribber.search.SearchFragment
import com.upscribber.signUp.InviteCodeActivity
import com.upscribber.subsciptions.manageSubscriptions.redeem.AdapterRedeem
import com.upscribber.subsciptions.manageSubscriptions.redeem.ModelRedeem
import com.upscribber.subsciptions.manageSubscriptions.redeem.RedeemScreenNew
import com.upscribber.subsciptions.manageSubscriptions.redeem.ReedemActivity
import com.upscribber.subsciptions.mySubscriptionDetails.MySubscriptionViewModel
import com.upscribber.subsciptions.mySubscriptionDetails.MySubscriptionsModel
import com.upscribber.subsciptions.mySubscriptionDetails.SubscriptionsFragment
import com.upscribber.upscribberSeller.DashboardProfileSellerActivity
import com.upscribber.upscribberSeller.customerBottom.CustomersFragment
import com.upscribber.upscribberSeller.customerBottom.scanning.SanningManuallyActivity
import com.upscribber.upscribberSeller.dashboardfragment.dashboard.DashboardFragment
import com.upscribber.upscribberSeller.dashboardfragment.dashboard.EventUpdateTitle
import com.upscribber.upscribberSeller.manageTeam.ManageTeamActivity
import com.upscribber.upscribberSeller.navigationCustomer.CustomerSellerActivity
import com.upscribber.upscribberSeller.navigationCustomer.InviteSubscription
import com.upscribber.upscribberSeller.reportingDetail.ReportingActivity
import com.upscribber.upscribberSeller.store.storeInfo.AddProductActivity
import com.upscribber.upscribberSeller.store.storeInfo.StoreSearchResultsActivity
import com.upscribber.upscribberSeller.store.storeMain.StoreFragment
import com.upscribber.upscribberSeller.store.storeMain.StoreViewModel
import com.upscribber.upscribberSeller.subsriptionSeller.createSubscription.createSubs.CreateSubscriptionActivity
import com.upscribber.upscribberSeller.subsriptionSeller.subscriptionMain.ModelMain
import com.upscribber.upscribberSeller.subsriptionSeller.subscriptionMain.SubscriptionSearchResultActivity
import com.upscribber.upscribberSeller.subsriptionSeller.subscriptionMain.SubscriptionSellerFragment
import com.upscribber.upscribberSeller.subsriptionSeller.subscriptionMain.SubscriptionSellerViewModel
import com.upscribber.upscribberSeller.subsriptionSeller.subscriptionMain.customerCheckout.ConfirmCustomerCheckoutActivity
import com.upscribber.upscribberSeller.subsriptionSeller.subscriptionMain.customerCheckout.ScannedCustomerActivity
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.activity_dashboard.*
import kotlinx.android.synthetic.main.filter_header.*
import kotlinx.android.synthetic.main.redeem_sheet_quantity.*

@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class DashboardActivity : AppCompatActivity(), DrawerLayout.DrawerListener,
    BottomNavigationView.OnNavigationItemSelectedListener, FilterInterface,
    AdapterRedeem.DismissSheet,
    FilterFragment.SetFilterHeaderBackground, FilterHomeInterface, InterfaceSwitch,
    BrandAdapter.Selection,
    SortByAdapter.Selection, HomeFragment.apiData, FavoriteInterface, GetSatertedClick,
    StoreFragment.switchFrag {

    private var valueNoti: String = ""
    private var homeData = ArrayList<ModelHomeDataSubscriptions>()
    private lateinit var allFragment: Fragment
    private lateinit var animation: Animation
    private lateinit var toolbarTitle: TextView
    private var modelData = ModelData()
    private lateinit var mreset: TextView
    private lateinit var mfilters: TextView
    private lateinit var mfilterDone: TextView
    private lateinit var filterImgBack: ImageView
    private var rightDrawer: LinearLayout? = null
    private lateinit var filterHeader: LinearLayout
    private lateinit var linearGradient: LinearLayout
    private lateinit var toolbar: Toolbar
    private lateinit var qr: ImageView
    private lateinit var invite: ImageView
    private lateinit var showNewNotificationImage: ImageView
    private lateinit var qr_black: ImageView
    private lateinit var invitecode: TextView
    private lateinit var imageLogo: ImageView
    private lateinit var addSubscription: LinearLayout
    private lateinit var mBottomNavigation: BottomNavigationView
    private lateinit var mBottomNavigationSeller: BottomNavigationView
    private lateinit var fragmentManager: FragmentManager
    private lateinit var fragmentTransaction: FragmentTransaction
    private lateinit var rv_search: RecyclerView
    private lateinit var Framecontainer: FrameLayout
    lateinit var fragmentMenu: MenuFragment
    lateinit var adapterRedeem: AdapterRedeem
    lateinit var searchLay: LinearLayout
    lateinit var array: ArrayList<ModelRedeem>
    lateinit var sellingSwitchLarge: SwitchCompat
    lateinit var layRightSeller: RelativeLayout
    lateinit var mainRelative: ConstraintLayout
    lateinit var proPic: CircleImageView
    lateinit var filter_done: TextView
    lateinit var cancel: TextView
    lateinit var reset: TextView
    lateinit var searchIcon: ImageView
    lateinit var searchQuery: SearchView
    lateinit var notificationIcon: ImageView
    lateinit var constraintNotifications: ConstraintLayout
    lateinit var tVEdit: ImageView
    lateinit var relativeLayout3: RelativeLayout
    lateinit var enableNotificationLayout: ConstraintLayout
    lateinit var becomeSeller: TextView
    lateinit var requestBusinessCustomer: TextView
    lateinit var requestBusiness: TextView
    lateinit var fragment: Fragment
    lateinit var checkFragment: Fragment
    var position = 0
    var drawerOpen = 0
    var search = false
    lateinit var adapter: CustomRecyclerAdapter
    var checked: Int = 0
    var statusMerchant: String = ""
    lateinit var actionbar: ActionBar
    lateinit var progressBar: LinearLayout
    private val AF_DEV_KEY = "9ZoQoVJhU3FRyk8oC5X9kA"
    private lateinit var firebaseAnalytics: FirebaseAnalytics
    lateinit var mViewModel: MySubscriptionViewModel
    var modelHome = ModelHomeDataSubscriptions()
    lateinit var homeFragment: Fragment
    private lateinit var viewModel: StoreViewModel
    private lateinit var mViewModelSubscriptions: SubscriptionSellerViewModel
    var productsSearchQuery = ""
    var subscriptionSearchQuery = ""

    companion object {
        var businessId = ""
        var businesName = "All"
        var sortName = "No Filter Selected"
        var checkedItems = ArrayList<ModelSubCategory>()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)
        mViewModel = ViewModelProviders.of(this)[MySubscriptionViewModel::class.java]
        viewModel = ViewModelProviders.of(this)[StoreViewModel::class.java]
        mViewModelSubscriptions = ViewModelProviders.of(this)[SubscriptionSellerViewModel::class.java]
        setToolbar()
        init()
        clearData()
        adapterRedeem = AdapterRedeem(this)
        if (checked == 0) {
            checked = 1
        }
        checkchangelistener(Constant.getSharedPrefs(this).getBoolean(Constant.IS_SELLER, false))
        listeners()
        inflateFragment(FilterFragment(this))
        openDrawerOperation()
        observerInit()
        observerInitSubscription()
        //------This is to disable swipe of filter-----------
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, GravityCompat.END)
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED, GravityCompat.START)

        Constant.setLoggedStatus(this)

        if (intent.hasExtra("user")) {
            val mInt = Intent(this, NotificationActivity::class.java)
            mInt.putExtra("user", intent.getStringExtra("user"))
            startActivity(mInt)

        }

        // Obtain the FirebaseAnalytics instance.
        firebaseAnalytics = FirebaseAnalytics.getInstance(this)

        val myTrace = FirebasePerformance.getInstance().newTrace("test_trace")
        myTrace.start()
        myTrace.stop()


    }


    fun clearData() {
        val mPrefs = getSharedPreferences("data", MODE_PRIVATE)
        val prefsEditor = mPrefs.edit()
        prefsEditor.putString("filterData", "")
        prefsEditor?.putString("categoryData", "")
        prefsEditor.apply()
    }




    //    find id
    @SuppressLint("WrongConstant")
    private fun init() {
        filterHeader = findViewById(R.id.filterHeader)
        constraintNotifications = findViewById(R.id.constraintNotifications)
        enableNotificationLayout = findViewById(R.id.enableNotificationLayout)
        imageLogo = findViewById(R.id.imageLogo)
        mBottomNavigation = findViewById(R.id.bottomNavigation)
        proPic = findViewById(R.id.proPic)
        mBottomNavigationSeller = findViewById(R.id.bottomNavigationSeller)
        rightDrawer = findViewById(R.id.rightDrawer)
        mreset = findViewById(R.id.reset)
        mfilters = findViewById(R.id.filters)
        mfilterDone = findViewById(R.id.filter_done)
        filterImgBack = findViewById(R.id.filterImgBack)
        rv_search = findViewById(R.id.rv_search)
        Framecontainer = findViewById(R.id.Framecontainer)
        filter_done = findViewById(R.id.filter_done)
        reset = findViewById(R.id.reset)
        searchIcon = findViewById(R.id.searchIcon)
        searchQuery = findViewById(R.id.searchQuery)
        qr = findViewById(R.id.qr)
        showNewNotificationImage = findViewById(R.id.showNewNotificationImage)
        invite = findViewById(R.id.invite)
        qr_black = findViewById(R.id.qr_black)
        notificationIcon = findViewById(R.id.notificationIcon)
        tVEdit = findViewById(R.id.tVEdit)
        invitecode = findViewById(R.id.invitecode)
        mBottomNavigation.setOnNavigationItemSelectedListener(this)
        mBottomNavigationSeller.setOnNavigationItemSelectedListener(this)
        sellingSwitchLarge = findViewById(R.id.sellingSwitchLarge)
        mainRelative = findViewById(R.id.mainRelative)
        layRightSeller = findViewById(R.id.layRightSeller)
        relativeLayout3 = findViewById(R.id.relativeLayout3)
        becomeSeller = findViewById(R.id.becomeSeller)
        requestBusinessCustomer = findViewById(R.id.requestBusinessCustomer)
        requestBusiness = findViewById(R.id.requestBusiness)
        addSubscription = findViewById(R.id.addSubscription)
        linearGradient = findViewById(R.id.linearGradient)
        searchLay = findViewById(R.id.searchLay)
        cancel = findViewById(R.id.cancel)
        progressBar = findViewById(R.id.progressBar)

        fragmentManager = supportFragmentManager
        this.fragmentTransaction = fragmentManager.beginTransaction()
        homeFragment = HomeFragment(homeData)
        fragmentTransaction.replace(R.id.Framecontainer, HomeFragment(homeData))

        fragmentTransaction.commit()
        searchIcon.visibility = View.GONE
        imageLogo.visibility = View.VISIBLE
        searchLay.visibility = View.GONE
        toolbarTitle.visibility = View.VISIBLE
        searchIcon.setColorFilter(Color.parseColor("#181C1F"))
        notificationIcon.visibility = View.VISIBLE
        constraintNotifications.visibility = View.VISIBLE
        notificationIcon.setImageResource(R.drawable.ic_notification_customer)
        tVEdit.visibility = View.GONE
        invitecode.visibility = View.GONE
        qr.visibility = View.GONE
        invite.visibility = View.GONE
        qr_black.visibility = View.VISIBLE
        becomeSeller.visibility = View.VISIBLE
        requestBusinessCustomer.visibility = View.VISIBLE
        requestBusiness.visibility = View.GONE

        openDrawerOperation()

        rv_search.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)

        adapter = CustomRecyclerAdapter(this, Helper.getVersionsList())
        rv_search.adapter = adapter

        valueNoti = Constant.getPrefs(this).getString(Constant.new_notification_status, "")

        if (valueNoti == "1"){
            showNewNotificationImage.visibility = View.VISIBLE
        }else{
            showNewNotificationImage.visibility = View.GONE
        }

    }

    @Subscribe
    fun getTitleUpdate(event: EventUpdateTitle) {
        when {
            event.position == 0 -> toolbarTitle.text = resources.getString(R.string.app_name)
            event.position == 1 -> toolbarTitle.text = "Sales"
            event.position == 2 -> toolbarTitle.text = "Reviews"
            event.position == 3 -> toolbarTitle.text = "Customers"
            event.position == 4 -> toolbarTitle.text = "Wallet"
            event.position == 5 -> toolbarTitle.text = "On Boarding"
            event.position == 6 -> toolbarTitle.text = "On Boarding"
            event.position == 7 -> toolbarTitle.text = "On Boarding"
            event.position == 8 -> toolbarTitle.text = "On Boarding"
            event.position == 9 -> toolbarTitle.text = "On Boarding"
        }
    }


    fun categoryClicked(v: View) {
        val intent = Intent(this, CategoryActivity::class.java)
        startActivity(intent)
    }

    fun redeemClicked(v: View) {
        fragmentMenu = MenuFragment(adapterRedeem, mViewModel)
        fragmentMenu.show(supportFragmentManager, fragmentMenu.tag)
    }


    fun paymentClicked(v: View) {
        val intent = Intent(this, PaymentTransactionActivity::class.java)
        startActivity(intent)
    }

    fun notificationClicked(v: View) {
        val intent = Intent(this, NotificationActivity::class.java)
        startActivity(intent)
    }

    fun businessClicked(v: View) {
        val intent = Intent(this, RequestBusiness::class.java)
        startActivity(intent)
    }

    fun sellerClicked(v: View) {
        val intent = Intent(this, sellerWelcomActivity::class.java)
        startActivity(intent)
    }

    fun aboutClicked(v: View) {

    }

    fun ActivityClicked(v: View) {
        val intent = Intent(this, PaymentTransactionActivity::class.java).putExtra("nav", "others")
        startActivity(intent)
    }

    fun teamsClickedd(v: View) {
        val intent =
            Intent(this, ManageTeamActivity::class.java).putExtra("notSelected", "SelectionList")
        startActivity(intent)
    }

    fun helpClicked(v: View) {
        val intent = Intent(this, HelpCenterActivity::class.java)
        startActivity(intent)
    }


    fun feedbackClicked(v: View) {
        val emailIntent = Intent(
            Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto", "upscribr@gmail.com", null
            )
        )
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Feedback")
        emailIntent.putExtra(
            Intent.EXTRA_TEXT,
            ""
        )
        this.startActivity(Intent.createChooser(emailIntent, "Send email"))
    }

    fun openProfile(v: View) {
        if (!sellingSwitchLarge.isChecked) {
            val intent = Intent(this, DashboardProfileActivity::class.java)
            startActivity(intent)
        } else {
            val intent = Intent(this, DashboardProfileSellerActivity::class.java)
            startActivity(intent)
        }

    }


    fun customerClicked(v: View) {
        val intent = Intent(this, CustomerSellerActivity::class.java)
        startActivity(intent)
    }

    fun earningsClicked(v: View) {
        startActivity(Intent(this, ReportingActivity::class.java).putExtra("value", 1))
    }

    fun helpSellerClicked(v: View) {
        val intent = Intent(this, HelpCenterActivity::class.java)
        startActivity(intent)
    }

    fun feedbackSellerClicked(v: View) {
        val emailIntent = Intent(
            Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto", "upscribr@gmail.com", null
            )
        )
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Feedback")
        emailIntent.putExtra(
            Intent.EXTRA_TEXT,
            ""
        )
        this.startActivity(Intent.createChooser(emailIntent, "Send email"))
    }

    fun IndividualSellerClicked(v: View) {

    }


    override fun openFilter(i: Int) {

        drawerLayout.openDrawer(GravityCompat.END)
        inflateFragment(FilterFragment(this))
        filterImgBack.visibility = View.GONE
        mfilters.text = "Filters"
        mreset.visibility = View.VISIBLE

    }

    // event listeners
    private fun listeners() {

        drawerLayout.addDrawerListener(this)

        filter_done.setOnClickListener {
            if (checkFragment is FilterFragment) {
                modelData = (checkFragment as FilterFragment).getData()
                val mPrefs = getSharedPreferences("data", MODE_PRIVATE)
                val prefsEditor = mPrefs.edit()
                val gson = Gson()
                val json = gson.toJson(modelData)
                prefsEditor.putString("filterData", json)
                prefsEditor.apply()
                if (modelData.latitude != "" && modelData.longitude != "") {

                    if (homeFragment is HomeFragment) {
                        (homeFragment as HomeFragment).runApi(this)
                    }
                } else {
                    if (homeFragment is HomeFragment) {
                        (homeFragment as HomeFragment).runApi(this)
                    }
                }
            }
            onBackPressed()
        }

        reset.setOnClickListener {
            businessId = ""
            businesName = ""
            sortName = "No Filter Selected"
            checkedItems.clear()
            val mPrefs = getSharedPreferences("data", MODE_PRIVATE)
            val prefsEditor = mPrefs.edit()
            prefsEditor.putString("filterData", "")
            prefsEditor.apply()
            if (homeFragment is HomeFragment) {
                (homeFragment as HomeFragment).runApi(this)
            }
            onBackPressed()
        }


        sellingSwitchLarge.setOnCheckedChangeListener { p0, p1 ->
            //            sellingSwitchMini.isChecked = p1
            if (checked == 0) {
                checked = 1
            }
            checkchangelistener(p1)

        }

        qr.setOnClickListener {
            val isSeller = Constant.getSharedPrefs(this).getBoolean(Constant.IS_SELLER, false)
            if (isSeller) {
                val fragment = MenuFragmentScan()
                fragment.show(fragmentManager, fragment.tag)
            } else {
                startActivity(
                    Intent(
                        this,
                        ScannedCustomerActivity::class.java
                    ).putExtra("generateCode", "generateCode")
                )
            }

        }

        invite.setOnClickListener {
            startActivity(Intent(this, InviteSubscription::class.java).putExtra("toolbar", "other"))
        }

        notificationIcon.setOnClickListener {
            startActivity(Intent(this, NotificationActivity::class.java))
        }

        tVEdit.setOnClickListener {
            startActivity(Intent(this, EditProfileActivity::class.java))
        }

        invitecode.setOnClickListener {
            startActivity(Intent(this, InviteCodeActivity::class.java))
        }

        qr_black.setOnClickListener {
            fragmentMenu = MenuFragment(adapterRedeem, mViewModel)
            fragmentMenu.show(supportFragmentManager, fragmentMenu.tag)
        }
    }


    override fun switchingProjects(p1: Boolean) {
        position = 0
        if (checked == 0) {
            checked = 1
        }
        checkchangelistener(p1)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 1 || requestCode == 65537) {
            if (grantResults.contains(-1)) {
                enableNotificationLayout.visibility = View.VISIBLE
                allowNotificationBtn.setOnClickListener {
                    ActivityCompat.requestPermissions(
                        this,
                        arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                        HomeFragment.LOCATION_PERMISSION_REQUEST_CODE
                    )
                }

//                Toast.makeText(this, "Please accept permission", Toast.LENGTH_SHORT).show()
//                Handler().postDelayed({
//                    ActivityCompat.requestPermissions(
//                        this,
//                        arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
//                        HomeFragment.LOCATION_PERMISSION_REQUEST_CODE
//                    )
//                }, 2000)

            } else {
                enableNotificationLayout.visibility = View.GONE
                if (homeFragment is HomeFragment) {
                    (homeFragment as HomeFragment).runApiFirst(this)
                }
            }
        }
    }


    class MenuFragmentScan : RoundedBottomSheetDialogFragment() {

        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val view = inflater.inflate(R.layout.add_scan_sheet, container, false)

            val cancelDialog = view.findViewById<TextView>(R.id.cancelDialog)
            val addCreditCard = view.findViewById<LinearLayout>(R.id.addCreditCard)
            val addBankAccount = view.findViewById<LinearLayout>(R.id.addBankAccount)

            cancelDialog.setOnClickListener {
                dismiss()
            }

            addCreditCard.setOnClickListener {
                val mPrefs1 = activity!!.getSharedPreferences("reedeem", Context.MODE_PRIVATE)
                val prefsEditor1 = mPrefs1.edit()
                prefsEditor1.putString("reedd", "")
                prefsEditor1.apply()
                startActivity(Intent(activity, RedeemScreenNew::class.java))
                addCreditCard.setBackgroundResource(R.drawable.bg_change_pic)
                dismiss()
            }

            addBankAccount.setOnClickListener {
                startActivity(Intent(activity, SanningManuallyActivity::class.java))
                addBankAccount.setBackgroundResource(R.drawable.bg_change_pic)
                dismiss()
            }
            return view
        }

    }


    override fun filterHeaderBackground(s: String) {
        if (s == "new") {
            filterHeader.setBackgroundDrawable(getDrawable(R.drawable.filter_header_select_category))
        } else {
            filterHeader.setBackgroundDrawable(getDrawable(R.drawable.filter_header))
        }
    }


    private fun checkchangelistener(p1: Boolean) {
        val editor = Constant.getSharedPrefs(this).edit()
        editor.putBoolean("isSeller", p1)
        editor.apply()
//        if (Constant.getArrayListProfile(this, Constant.dataProfile) != null){
        val profileHomeData = Constant.getArrayListProfile(this, Constant.dataProfile)
        if (p1) {

//                if (profileHomeData.is_merchant == "1"){
            sellerSwitchSelected()
//                }else{
//                    startActivity(Intent(this,sellerWelcomActivity::class.java))
//                }
        } else {
            sellerSwitchNotSelected()
        }
//        }


        Handler().postDelayed({
            checked = 0
        }, 1000)

    }

    override fun getStarted(position1: Int) {
        if (position1 == 1) {
            sellerSwitchNotSelected()
        } else if (position1 == 2) {
            fragment = SubscriptionsFragment()
            searchIcon.visibility = View.GONE
            qr.visibility = View.GONE
            invite.visibility = View.GONE
            imageLogo.visibility = View.GONE
            qr_black.visibility = View.GONE
            searchLay.visibility = View.GONE
            toolbarTitle.visibility = View.VISIBLE
            addSubscription.visibility = View.GONE
            notificationIcon.visibility = View.VISIBLE
            constraintNotifications.visibility = View.VISIBLE
            notificationIcon.setImageResource(R.drawable.ic_notification_customer)
            tVEdit.visibility = View.GONE
            invitecode.visibility = View.GONE
            loadFragment(fragment)
            linearLayoutCustomer.visibility = View.GONE
            toolbarTitle.text = "My Subscriptions"
            position = 2
            search = false
            mBottomNavigation.menu.getItem(2).isChecked = true
            toolbarTitle.setTextColor(Color.parseColor("#181C1F"))
            setSupportActionBar(toolbar)
        }

    }

    /**
     * Show view for Customer
     */
    private fun sellerSwitchNotSelected() {
        openDrawerOperation()
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(this, R.color.colorPrimaryDark)
        searchIcon.visibility = View.GONE
        searchLay.visibility = View.GONE
        toolbarTitle.visibility = View.GONE
        searchIcon.setColorFilter(Color.parseColor("#181C1F"))
        qr.visibility = View.GONE
        invite.visibility = View.GONE
        qr_black.visibility = View.VISIBLE
        addSubscription.visibility = View.GONE
        notificationIcon.visibility = View.VISIBLE
        constraintNotifications.visibility = View.VISIBLE
        notificationIcon.setImageResource(R.drawable.ic_notification_customer)
        tVEdit.visibility = View.GONE
        invitecode.visibility = View.GONE
        becomeSeller.visibility = View.VISIBLE
        requestBusinessCustomer.visibility = View.VISIBLE
        requestBusiness.visibility = View.GONE
        toolbar.setBackgroundResource(R.color.colorWhite)
        imageLogo.visibility = View.VISIBLE
        toolbar.visibility = View.VISIBLE
        relativeLayout3.setBackgroundResource(R.drawable.side_nav_bar)
        layoutCustomer.visibility = View.VISIBLE
        layoutSeller.visibility = View.GONE
        mBottomNavigation.visibility = View.VISIBLE
//        toolbarTitle.text = getString(R.string.app_name)
        mBottomNavigationSeller.visibility = View.GONE
        linearLayoutCustomer.visibility = View.GONE
        mBottomNavigation.menu.getItem(0).isChecked = true
        mBottomNavigationSeller.menu.getItem(0).isChecked = false
        fragmentManager = supportFragmentManager

        fragment = HomeFragment(homeData)
        homeFragment = HomeFragment(homeData)
        loadFragment(fragment)
        proPic.setImageResource(R.mipmap.profile_image)
        linearGradient.setBackgroundResource(R.mipmap.bg_profile_circle_customer)
        proPic.setBackgroundResource(0)

        toolbarTitle.setTextColor(Color.parseColor("#181C1F"))
        setSupportActionBar(toolbar)
        searchIcon.setOnClickListener {
            startActivity(Intent(this, SearchActivity::class.java))
            val editor = Constant.getSharedPrefs(this).edit()
            editor.putBoolean("dataSearch", true)
            editor.apply()
        }


        if (valueNoti == "1"){
            showNewNotificationImage.visibility = View.VISIBLE
        }else{
            showNewNotificationImage.visibility = View.GONE
        }
    }

    /**
     * Show view for Seller
     */
    private fun sellerSwitchSelected() {
        fragment = DashboardFragment()
        toolbarTitle.text = resources.getString(R.string.app_name)
        loadFragment(fragment)
        searchLay.visibility = View.GONE
        toolbarTitle.visibility = View.VISIBLE
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(this, R.color.home_free)
        searchIcon.visibility = View.GONE
        qr.visibility = View.VISIBLE
        qr.setImageResource(R.drawable.ic_qr)
        invite.visibility = View.GONE
        qr_black.visibility = View.GONE
        addSubscription.visibility = View.GONE
        notificationIcon.visibility = View.VISIBLE
        constraintNotifications.visibility = View.VISIBLE
        notificationIcon.setImageResource(R.drawable.ic_notification)
        tVEdit.visibility = View.GONE
        invitecode.visibility = View.GONE
        imageLogo.visibility = View.GONE
        toolbar.setBackgroundResource(R.drawable.bg_toolbar_gradient)
        toolbar.visibility = View.VISIBLE
        relativeLayout3.setBackgroundColor(Color.parseColor("#000000"))
        layoutCustomer.visibility = View.GONE
        layoutSeller.visibility = View.VISIBLE
        mBottomNavigation.visibility = View.GONE
        mBottomNavigationSeller.visibility = View.VISIBLE
        mBottomNavigationSeller.menu.getItem(0).isChecked = true
        mBottomNavigation.menu.getItem(0).isChecked = false
        proPic.setImageResource(R.drawable.ic_lotus_back)
        linearGradient.setBackgroundResource(R.mipmap.bg_profile_circle_thick)
        proPic.setBackgroundResource(R.drawable.favorite_background_circle1)
        becomeSeller.visibility = View.GONE
        requestBusinessCustomer.visibility = View.GONE
        requestBusiness.visibility = View.VISIBLE
        toolbarTitle.setTextColor(Color.parseColor("#ffffff"))
        setSupportActionBar(toolbar)
        addSubscription.setOnClickListener {
            if (position == 1) {
                startActivity(Intent(this, AddProductActivity::class.java))

            } else {
                val mPrefs = application.getSharedPreferences("data", MODE_PRIVATE)
                val mPrefs1 = application.getSharedPreferences("customers", MODE_PRIVATE)
                val prefsEditor = mPrefs.edit()
                val prefsEditor1 = mPrefs1.edit()
                prefsEditor1.putString("customers", "")
                prefsEditor.putString("detailsArray", "")
                prefsEditor.putString("pricing", "")
                prefsEditor.putString("basicData", "")
                prefsEditor.putString("campaign_id", "")
                prefsEditor.putString("bought", "")
                prefsEditor.apply()
                prefsEditor1.apply()
                CreateSubscriptionActivity.editData = ModelMain()
                startActivity(Intent(this, CreateSubscriptionActivity::class.java))
            }
        }

        if (valueNoti == "1"){
            showNewNotificationImage.visibility = View.VISIBLE
        }else{
            showNewNotificationImage.visibility = View.GONE
        }

    }



    override fun addToFav(position: Int) {
        if (position == 1) {
            invite.visibility = View.VISIBLE
        } else {
            invite.visibility = View.VISIBLE
//            invite.visibility = View.GONE
        }

    }

    @SuppressLint("ResourceType")
    override fun onBackPressed() {
        if (drawerOpen == 1) {
            drawerOpen = 0
            drawerLayout.closeDrawer(GravityCompat.END)
        } else
            if (position != 0) {
                position = 0
                val isSeller = Constant.getSharedPrefs(this).getBoolean(Constant.IS_SELLER, false)
//                if (!sellingSwitchLarge.isChecked) {
                if (!isSeller) {
                    val fm = supportFragmentManager
                    fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
                    val transaction = supportFragmentManager.beginTransaction()
                    homeFragment = HomeFragment(homeData)
                    transaction.replace(R.id.Framecontainer, HomeFragment(homeData))
                    toolbar.setBackgroundResource(R.color.colorWhite)
                    linearLayoutCustomer.visibility = View.GONE
                    notificationIcon.setImageResource(R.drawable.ic_notification_customer)
                    searchIcon.setColorFilter(Color.parseColor("#181C1F"))
                    searchIcon.visibility = View.GONE
                    searchLay.visibility = View.GONE
                    toolbarTitle.visibility = View.GONE
                    qr.visibility = View.GONE
                    invite.visibility = View.GONE
                    qr_black.visibility = View.VISIBLE
                    addSubscription.visibility = View.GONE
                    notificationIcon.visibility = View.VISIBLE
                    constraintNotifications.visibility = View.VISIBLE
                    constraintNotifications.visibility = View.VISIBLE
                    tVEdit.visibility = View.GONE
                    invitecode.visibility = View.GONE
                    imageLogo.visibility = View.VISIBLE
                    transaction.addToBackStack("0")
                    proPic.setImageResource(R.drawable.ic_lotus_back)
                    linearGradient.setBackgroundResource(R.mipmap.bg_profile_circle_thick)
                    transaction.commit()
//                    toolbarTitle.text = getString(R.string.app_name)
                    mBottomNavigation.menu.getItem(0).isChecked = true
                    mBottomNavigationSeller.menu.getItem(0).isChecked = false
                    becomeSeller.visibility = View.VISIBLE
                    requestBusinessCustomer.visibility = View.VISIBLE
                    requestBusiness.visibility = View.GONE
                    toolbarTitle.setTextColor(Color.parseColor("#181C1F"))
                    setSupportActionBar(toolbar)
                    actionbar = this.supportActionBar!!
                    searchIcon.setOnClickListener {
                        startActivity(Intent(this, SearchActivity::class.java))
                        val editor = Constant.getSharedPrefs(this).edit()
                        editor.putBoolean("dataSearch", true)
                        editor.apply()

                    }

                } else {
                    val fm = supportFragmentManager
                    fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
                    val transaction = supportFragmentManager.beginTransaction()
                    transaction.replace(
                        R.id.Framecontainer,
                        DashboardFragment()
                    )
                    toolbar.setBackgroundResource(R.drawable.bg_toolbar_gradient)
                    transaction.addToBackStack("0")
                    transaction.commit()
                    linearLayoutCustomer.visibility = View.GONE
                    imageLogo.visibility = View.GONE
                    searchLay.visibility = View.GONE
                    toolbarTitle.visibility = View.VISIBLE
                    proPic.setImageResource(R.mipmap.profile_image)
                    linearGradient.setBackgroundResource(R.mipmap.bg_profile_circle_customer)
                    proPic.setBackgroundResource(0)
                    window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
                    window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                    window.statusBarColor = ContextCompat.getColor(this, R.color.colorPrimaryDark)

                    toolbar.visibility = View.VISIBLE
                    toolbarTitle.text = resources.getString(R.string.app_name)
                    mBottomNavigationSeller.menu.getItem(0).isChecked = true
                    mBottomNavigation.menu.getItem(0).isChecked = false
                    searchIcon.visibility = View.GONE
                    qr.visibility = View.VISIBLE
                    qr.setImageResource(R.drawable.ic_qr)
                    invite.visibility = View.GONE
                    qr_black.visibility = View.GONE
                    addSubscription.visibility = View.GONE
                    notificationIcon.visibility = View.VISIBLE
                    constraintNotifications.visibility = View.VISIBLE
                    notificationIcon.setImageResource(R.drawable.ic_notification)
                    tVEdit.visibility = View.GONE
                    invitecode.visibility = View.GONE
                    becomeSeller.visibility = View.GONE
                    requestBusinessCustomer.visibility = View.GONE
                    requestBusiness.visibility = View.VISIBLE
                    toolbarTitle.setTextColor(Color.parseColor("#ffffff"))
                    setSupportActionBar(toolbar)
                }


            } else {
                val builder = AlertDialog.Builder(this)
                builder.setTitle("Exit")
                builder.setMessage("Are you sure you want to Exit?")
                builder.setPositiveButton("YES")
                { dialog, which ->
                    finishAffinity() // Close all activites
                    System.exit(0)
//                    finish()
                }
                builder.setNegativeButton("No") { dialog, which ->
                }
                val dialog: AlertDialog = builder.create()
                dialog.show()
            }

    }

    // set toolbarTitle bar of the screen
    private fun setToolbar() {

        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        title = ""
        toolbarTitle = toolbar.findViewById(R.id.title) as TextView
//        toolbarTitle.text = getString(R.string.app_name)

    }


    // Drawer callback methods
    override fun onDrawerOpened(drawerView: View) {
        if (drawerLayout.isDrawerVisible(GravityCompat.END)) {
            Constant.hideKeyboard(this, toolbarTitle)
            drawerOpen = 1
        }
    }

    override fun onDrawerSlide(drawerView: View, slideOffset: Float) {

    }

    override fun onDrawerStateChanged(newState: Int) {

    }

    override fun onDrawerClosed(drawerView: View) {
        Constant.hideKeyboard(this,toolbarTitle)
        if (drawerLayout.isDrawerVisible(GravityCompat.END)) {
            drawerOpen = 0
        }
//        closeDrawerOperation()
    }

    // menu items event handles
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
//                drawerLayout.openDrawer(GravityCompat.START)
                Constant.hideKeyboard(this, toolbarTitle)
                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }


    // Here is what happen on expand drawer arrow click
    private fun openDrawerOperation() {
        layRightSeller.visibility = View.VISIBLE
        iconSelling.visibility = View.VISIBLE
        dividerMiniFirst.visibility = View.VISIBLE
        dividerMiniSecond.visibility = View.VISIBLE
        animation = AnimationUtils.loadAnimation(applicationContext, android.R.anim.fade_in)
        var animation2 = AnimationUtils.loadAnimation(applicationContext, android.R.anim.fade_out)

        this.animation.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationRepeat(animation: Animation?) {

            }

            override fun onAnimationEnd(animation: Animation?) {
                iconSelling.visibility = View.VISIBLE
                dividerMiniFirst.visibility = View.VISIBLE
                dividerMiniSecond.visibility = View.VISIBLE
            }

            override fun onAnimationStart(animation: Animation?) {
            }

        })
    }


    override fun onResume() {
        //BusProvider.getInstance().register(this)
        super.onResume()

        if (fragment is SearchFragment) {
            (fragment as SearchFragment).setAdapterSearch()
        }

        Constant.hideKeyboard(this, cancel)
        invalidateOptionsMenu()
        val sharedPreferences = getSharedPreferences("dashboard", Context.MODE_PRIVATE)
        if (!sharedPreferences.getString("dashLayout", "").isEmpty()) {
            if (sharedPreferences.getString("dashLayout", "") == "subscription") {
                fragment = SubscriptionsFragment()
                searchIcon.visibility = View.GONE
                qr.visibility = View.GONE
                invite.visibility = View.GONE
                imageLogo.visibility = View.GONE
//                EditText.visibility = View.GONE
                qr_black.visibility = View.GONE
                searchLay.visibility = View.GONE
                toolbarTitle.visibility = View.VISIBLE
                addSubscription.visibility = View.GONE
                notificationIcon.visibility = View.VISIBLE
                constraintNotifications.visibility = View.VISIBLE
                notificationIcon.setImageResource(R.drawable.ic_notification_customer)
                tVEdit.visibility = View.GONE
                invitecode.visibility = View.GONE
                loadFragment(fragment)
                linearLayoutCustomer.visibility = View.GONE
                toolbarTitle.text = "My Subscriptions"
                position = 2
                search = false
                mBottomNavigation.menu.getItem(2).isChecked = true
                toolbarTitle.setTextColor(Color.parseColor("#181C1F"))
                setSupportActionBar(toolbar)
                val editor = sharedPreferences.edit()
                editor.remove("dashLayout")
                editor.apply()
                return
            }
            val editor = sharedPreferences.edit()
            editor.remove("dashLayout")
            editor.apply()

            startActivity(
                Intent(this, ConfirmCheckoutActivity::class.java).putExtra(
                    "model",
                    intent.getParcelableExtra<ModelBuySubscription>("model")
                )
            )

        }


        val sharedPreferences5 = getSharedPreferences("dashLayoutMerchant", Context.MODE_PRIVATE)
        if (!sharedPreferences5.getString("dashLayoutMerchantt", "").isEmpty()) {
            if (sharedPreferences5.getString("dashLayoutMerchantt", "") == "subscriptionMerchant") {
                val type = Constant.getPrefs(this).getString(Constant.type, "")
                productsSearchQuery = ""
                subscriptionSearchQuery = ""
                searchQuery.setQuery("", true)

                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                window.statusBarColor = ContextCompat.getColor(this, R.color.home_free)
                fragment =
                    SubscriptionSellerFragment()
                loadFragment(fragment)
                toolbarTitle.text = "Subscriptions"
                setSupportActionBar(toolbar)
                searchLay.visibility = View.GONE
                toolbarTitle.visibility = View.VISIBLE
                imageLogo.visibility = View.GONE
                mBottomNavigationSeller.menu.getItem(2).isChecked = true
                mBottomNavigation.menu.getItem(2).isChecked = false
                linearLayoutCustomer.visibility = View.GONE
                setSupportActionBar(toolbar)
                supportActionBar!!.elevation = 0f
                position = 2
                toolbar.visibility = View.VISIBLE
                search = false
                searchIcon.setColorFilter(Color.parseColor("#ffffff"))
                searchIcon.visibility = View.VISIBLE
                if (type == "3") {
                    addSubscription.visibility = View.VISIBLE
                    addSubscription.alpha = 0.4f
                    addSubscription.isClickable = false
                } else {
                    addSubscription.isClickable = true
                    addSubscription.visibility = View.VISIBLE
                }
                notificationIcon.visibility = View.GONE
                constraintNotifications.visibility = View.GONE
                tVEdit.visibility = View.GONE
                invitecode.visibility = View.GONE
                qr.visibility = View.GONE
                invite.visibility = View.GONE
                qr_black.visibility = View.GONE
                searchIcon.setOnClickListener {
                    Constant.showKeyBoard(this@DashboardActivity)
                    productsSearchQuery = ""
                    subscriptionSearchQuery = ""
                    searchQuery.setQuery("", true)
                    searchLay.visibility = View.VISIBLE
                    toolbarTitle.visibility = View.GONE
                    searchIcon.visibility = View.GONE
                    imageLogo.visibility = View.GONE
                    addSubscription.visibility = View.GONE

//                    if (type == "3") {
//                        addSubscription.visibility = View.VISIBLE
//                        addSubscription.alpha = 0.4f
//                        addSubscription.isClickable = false
//                    } else {
//                        addSubscription.isClickable = true
//                        addSubscription.visibility = View.VISIBLE
//                    }
                    notificationIcon.visibility = View.GONE
                    constraintNotifications.visibility = View.GONE
                    qr.visibility = View.GONE
                    invite.visibility = View.GONE
                    qr_black.visibility = View.GONE
                    tVEdit.visibility = View.GONE
                    invitecode.visibility = View.GONE
                    setSupportActionBar(toolbar)
                }
                cancel.setOnClickListener {
                    Constant.hideKeyboard(this, cancel)
                    productsSearchQuery = ""
                    subscriptionSearchQuery = ""
                    searchQuery.setQuery("", true)
                    searchLay.visibility = View.GONE
                    toolbarTitle.visibility = View.VISIBLE
                    searchIcon.visibility = View.VISIBLE
                    imageLogo.visibility = View.GONE
                    if (type == "3") {
                        addSubscription.visibility = View.VISIBLE
                        addSubscription.alpha = 0.4f
                        addSubscription.isClickable = false
                    } else {
                        addSubscription.isClickable = true
                        addSubscription.visibility = View.VISIBLE
                    }

                    notificationIcon.visibility = View.GONE
                    constraintNotifications.visibility = View.GONE
                    qr.visibility = View.GONE
                    invite.visibility = View.GONE
                    qr_black.visibility = View.GONE
                    tVEdit.visibility = View.GONE
                    invitecode.visibility = View.GONE
                    setSupportActionBar(toolbar)
                }


                searchQuery.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                    override fun onQueryTextSubmit(query: String): Boolean {
                        subscriptionSearchQuery = query

                        apiImplimentationSubscription(query)
                        return false
                    }

                    override fun onQueryTextChange(newText: String?): Boolean {

                        return false

                    }


                })


                invalidateOptionsMenu()
                val editor5 = sharedPreferences5.edit()
                editor5.remove("dashLayoutMerchantt")
                editor5.apply()
                return
            }
            val editor5 = sharedPreferences5.edit()
            editor5.remove("dashLayoutMerchantt")
            editor5.apply()

            if (intent.hasExtra("failure")) {
                startActivity(
                    Intent(this, ConfirmCustomerCheckoutActivity::class.java).putExtra(
                        "failure",
                        intent.getParcelableExtra<ModelBuySubscription>("failure")
                    )
                )
            } else {
                startActivity(
                    Intent(this, ConfirmCustomerCheckoutActivity::class.java).putExtra(
                        "model",
                        intent.getParcelableExtra<ModelBuySubscription>("model")
                    )
                )
            }

        }

        val sharedPreferencess =
            getSharedPreferences("dashLayoutCancelMerchant", Context.MODE_PRIVATE)
        if (!sharedPreferencess.getString("dashLayoutCancelMerchantt", "").isEmpty()) {
            if (sharedPreferencess.getString("dashLayoutCancelMerchantt", "") == "3") {
                val type = Constant.getPrefs(this).getString(Constant.type, "")
                productsSearchQuery = ""
                subscriptionSearchQuery = ""
                searchQuery.setQuery("", true)

                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                window.statusBarColor = ContextCompat.getColor(this, R.color.home_free)
                fragment =
                    SubscriptionSellerFragment()
                loadFragment(fragment)
                toolbarTitle.text = "Subscriptions"
                setSupportActionBar(toolbar)
                searchLay.visibility = View.GONE
                toolbarTitle.visibility = View.VISIBLE
                imageLogo.visibility = View.GONE
                mBottomNavigationSeller.menu.getItem(2).isChecked = true
                mBottomNavigation.menu.getItem(2).isChecked = false
                linearLayoutCustomer.visibility = View.GONE
                setSupportActionBar(toolbar)
                supportActionBar!!.elevation = 0f
                position = 2
                toolbar.visibility = View.VISIBLE
                search = false
                searchIcon.setColorFilter(Color.parseColor("#ffffff"))
                searchIcon.visibility = View.VISIBLE
                if (type == "3") {
                    addSubscription.visibility = View.VISIBLE
                    addSubscription.alpha = 0.4f
                    addSubscription.isClickable = false
                } else {
                    addSubscription.isClickable = true
                    addSubscription.visibility = View.VISIBLE
                }
                notificationIcon.visibility = View.GONE
                constraintNotifications.visibility = View.GONE
                tVEdit.visibility = View.GONE
                invitecode.visibility = View.GONE
                qr.visibility = View.GONE
                invite.visibility = View.GONE
                qr_black.visibility = View.GONE
                searchIcon.setOnClickListener {
                    Constant.showKeyBoard(this@DashboardActivity)
                    productsSearchQuery = ""
                    subscriptionSearchQuery = ""
                    searchQuery.setQuery("", true)
                    searchLay.visibility = View.VISIBLE
                    toolbarTitle.visibility = View.GONE
                    searchIcon.visibility = View.GONE
                    imageLogo.visibility = View.GONE
                    addSubscription.visibility = View.GONE

//                    if (type == "3") {
//                        addSubscription.visibility = View.VISIBLE
//                        addSubscription.alpha = 0.4f
//                        addSubscription.isClickable = false
//                    } else {
//                        addSubscription.isClickable = true
//                        addSubscription.visibility = View.VISIBLE
//                    }
                    notificationIcon.visibility = View.GONE
                    constraintNotifications.visibility = View.GONE
                    qr.visibility = View.GONE
                    invite.visibility = View.GONE
                    qr_black.visibility = View.GONE
                    tVEdit.visibility = View.GONE
                    invitecode.visibility = View.GONE
                    setSupportActionBar(toolbar)
                }
                cancel.setOnClickListener {
                    Constant.hideKeyboard(this, cancel)
                    productsSearchQuery = ""
                    subscriptionSearchQuery = ""
                    searchQuery.setQuery("", true)
                    searchLay.visibility = View.GONE
                    toolbarTitle.visibility = View.VISIBLE
                    searchIcon.visibility = View.VISIBLE
                    imageLogo.visibility = View.GONE
                    if (type == "3") {
                        addSubscription.visibility = View.VISIBLE
                        addSubscription.alpha = 0.4f
                        addSubscription.isClickable = false
                    } else {
                        addSubscription.isClickable = true
                        addSubscription.visibility = View.VISIBLE
                    }

                    notificationIcon.visibility = View.GONE
                    constraintNotifications.visibility = View.GONE
                    qr.visibility = View.GONE
                    invite.visibility = View.GONE
                    qr_black.visibility = View.GONE
                    tVEdit.visibility = View.GONE
                    invitecode.visibility = View.GONE
                    setSupportActionBar(toolbar)
                }


                searchQuery.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                    override fun onQueryTextSubmit(query: String): Boolean {
                        subscriptionSearchQuery = query

                        apiImplimentationSubscription(query)
                        return false
                    }

                    override fun onQueryTextChange(newText: String?): Boolean {

                        return false

                    }


                })


                invalidateOptionsMenu()
                val editorr = sharedPreferencess.edit()
                editorr.remove("dashLayoutCancelMerchantt")
                editorr.apply()
                return
            }
            val editorr = sharedPreferencess.edit()
            editorr.remove("dashLayoutCancelMerchantt")
            editorr.apply()

        }


        val mPrefs = getSharedPreferences("ProductArray", 0)
        val prefsEditor = mPrefs.getString("redeemShare", "")
        val prefsEditor2 = mPrefs.getString("share", "")
        if (!prefsEditor.isEmpty()) {
            for (i in 0 until array.size) {
                if (prefsEditor == array[i].itemId) {
                    array[i].status = prefsEditor2 == "share"

                }
            }
            adapterRedeem.notifyDataSetChanged()

            val prefsEditor = mPrefs.edit()
            prefsEditor.clear()
            prefsEditor.apply()
        }

        val sharedPreferences1 = Constant.getPrefs(this)
        if (!sharedPreferences1.getString("dashBoardClose", "").isEmpty()) {
            if (checked == 0) {
                checked = 1
            }
            drawerLayout.closeDrawers()
            sellingSwitchLarge.isChecked = true
            checkchangelistener(true)
        }
        val editor = sharedPreferences1.edit()
        editor.remove("dashBoardClose")
        editor.remove("Register")
        editor.apply()
        val type = Constant.getPrefs(this).getString(Constant.type, "")

        if (fragment is StoreFragment) {
            productsSearchQuery = ""
            searchQuery.setQuery("", true)
            searchLay.visibility = View.GONE
            toolbarTitle.visibility = View.VISIBLE
            searchIcon.visibility = View.VISIBLE
            imageLogo.visibility = View.GONE
            if (type == "3") {
                addSubscription.visibility = View.VISIBLE
                addSubscription.alpha = 0.4f
                addSubscription.isClickable = false
            } else {
                addSubscription.isClickable = true
                addSubscription.visibility = View.VISIBLE
            }
            notificationIcon.visibility = View.GONE
            constraintNotifications.visibility = View.GONE
            qr.visibility = View.GONE
            invite.visibility = View.GONE
            tVEdit.visibility = View.GONE
            invitecode.visibility = View.GONE
            setSupportActionBar(toolbar)
        }



        if (fragment is SubscriptionSellerFragment) {
            productsSearchQuery = ""
            subscriptionSearchQuery = ""
            searchQuery.setQuery("", true)
            searchLay.visibility = View.GONE
            imageLogo.visibility = View.GONE
            toolbarTitle.visibility = View.VISIBLE
            searchIcon.visibility = View.VISIBLE
            if (type == "3") {
                addSubscription.visibility = View.VISIBLE
                addSubscription.alpha = 0.4f
                addSubscription.isClickable = false
            } else {
                addSubscription.isClickable = true
                addSubscription.visibility = View.VISIBLE
            }
            notificationIcon.visibility = View.GONE
            constraintNotifications.visibility = View.GONE
            qr.visibility = View.GONE
            invite.visibility = View.GONE
            qr_black.visibility = View.GONE
            tVEdit.visibility = View.GONE
            invitecode.visibility = View.GONE
            setSupportActionBar(toolbar)
        }
    }

    override fun onStop() {
        super.onStop()
        Constant.hideKeyboard(this, cancel)
    }


    override fun updateDataBackground(i: ModelHomeDataSubscriptions) {
        modelHome = i

    }

    private fun loadFragment(fragment: Fragment) {

        allFragment = fragment
        homeFragment = fragment
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.Framecontainer, fragment)
        // transaction.addToBackStack(null)
        transaction.commit()
    }


    //    bottom navigation view
    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        toolbar.visibility = View.VISIBLE
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(this, R.color.colorPrimaryDark)
        when (item.itemId) {

            R.id.home -> {
                //Clearing data on switch for filters
                productsSearchQuery = ""
                subscriptionSearchQuery = ""
                searchQuery.setQuery("", true)

                SubCategoriesActivity.businessName = ""
                SubCategoriesActivity.businessId = ""
                SubCategoriesActivity.sortName = ""
                SubCategoriesActivity.checkedItems.clear()
                businessId = ""
                businesName = ""
                sortName = "No Filter Selected"
                checkedItems.clear()
                val mPrefs = getSharedPreferences("data", MODE_PRIVATE)
                val prefsEditor = mPrefs.edit()
                val gson = Gson()
                val json = mPrefs.getString("filterData", "")

                if (json != "") {
                    val model = gson.fromJson(json, ModelData::class.java)
                    model.distanceMin = "0"
                    model.distanceMax = "1000"
                    model.priceMax = "0"
                    model.priceMin = "2000"
                    val putFilterData = gson.toJson(modelData)
                    prefsEditor.putString("filterData", putFilterData)
                    prefsEditor.apply()
                } else {
                    prefsEditor.putString("filterData", "")
                    prefsEditor.apply()
                }
                homeFragment = HomeFragment(homeData)
                fragment = HomeFragment(homeData)
                loadFragment(fragment)
//                toolbarTitle.text = getString(R.string.app_name)
                linearLayoutCustomer.visibility = View.GONE
                mBottomNavigation.menu.getItem(0).isChecked = true
                mBottomNavigationSeller.menu.getItem(0).isChecked = false
                searchLay.visibility = View.GONE
                toolbarTitle.visibility = View.GONE
                imageLogo.visibility = View.VISIBLE
                searchIcon.visibility = View.GONE
                searchIcon.setColorFilter(Color.parseColor("#181C1F"))
                addSubscription.visibility = View.GONE
                qr.visibility = View.GONE
                invite.visibility = View.GONE
                qr_black.visibility = View.VISIBLE
                notificationIcon.visibility = View.VISIBLE
                constraintNotifications.visibility = View.VISIBLE
                notificationIcon.setImageResource(R.drawable.ic_notification_customer)
                toolbar.setBackgroundResource(R.color.colorWhite)
                tVEdit.visibility = View.GONE
                invitecode.visibility = View.GONE
                position = 0
                search = false
                toolbarTitle.setTextColor(Color.parseColor("#181C1F"))
                setSupportActionBar(toolbar)
                searchIcon.setOnClickListener {
                    startActivity(Intent(this, SearchActivity::class.java))
                    val editor = Constant.getSharedPrefs(this).edit()
                    editor.putBoolean("dataSearch", true)
                    editor.apply()

                }

                return true
            }
            R.id.search -> {
                productsSearchQuery = ""
                subscriptionSearchQuery = ""
                searchQuery.setQuery("", true)

                //Clearing data on switch for filters
                SubCategoriesActivity.businessName = ""
                SubCategoriesActivity.sortName = ""
                SubCategoriesActivity.checkedItems.clear()
                businessId = ""
                businesName = ""
                sortName = "No Filter Selected"
                val mPrefs = getSharedPreferences("data", MODE_PRIVATE)
                val prefsEditor = mPrefs.edit()
                val gson = Gson()
                val json = mPrefs.getString("filterData", "")

                if (json != "") {
                    val model = gson.fromJson(json, ModelData::class.java)
                    model.distanceMin = "0"
                    model.distanceMax = "1000"
                    model.priceMax = "0"
                    model.priceMin = "2000"
                    val putFilterData = gson.toJson(modelData)
                    prefsEditor.putString("filterData", putFilterData)
                    prefsEditor.apply()
                } else {
                    prefsEditor.putString("filterData", "")
                    prefsEditor.apply()
                }
                checkedItems.clear()
                fragment = SearchFragment()
                loadFragment(fragment)
                toolbarTitle.text = "Search"
                linearLayoutCustomer.visibility = View.GONE
                position = 1
                searchLay.visibility = View.GONE
                imageLogo.visibility = View.GONE
                toolbarTitle.visibility = View.VISIBLE
                mBottomNavigation.menu.getItem(1).isChecked = true
                mBottomNavigationSeller.menu.getItem(1).isChecked = false
                searchIcon.visibility = View.GONE
                qr.visibility = View.GONE
                invite.visibility = View.GONE
                qr_black.visibility = View.GONE
                addSubscription.visibility = View.GONE
                toolbar.setBackgroundResource(R.color.colorWhite)
                notificationIcon.visibility = View.VISIBLE
                constraintNotifications.visibility = View.VISIBLE
                notificationIcon.setImageResource(R.drawable.ic_notification_customer)
                tVEdit.visibility = View.GONE
                invitecode.visibility = View.GONE
                search = false
                toolbarTitle.setTextColor(Color.parseColor("#181C1F"))
                setSupportActionBar(toolbar)
                return true
            }
            R.id.subscription -> {
                productsSearchQuery = ""
                subscriptionSearchQuery = ""
                searchQuery.setQuery("", true)

                fragment = SubscriptionsFragment()
                searchIcon.visibility = View.GONE
                qr.visibility = View.GONE
                invite.visibility = View.GONE
                imageLogo.visibility = View.GONE
                qr_black.visibility = View.GONE
                addSubscription.visibility = View.GONE
                notificationIcon.visibility = View.VISIBLE
                constraintNotifications.visibility = View.VISIBLE
                notificationIcon.setImageResource(R.drawable.ic_notification_customer)
                tVEdit.visibility = View.GONE
                invitecode.visibility = View.GONE
                loadFragment(fragment)
                searchLay.visibility = View.GONE
                toolbarTitle.visibility = View.VISIBLE
                linearLayoutCustomer.visibility = View.GONE
                toolbarTitle.text = "My Subscriptions"
                toolbar.setBackgroundResource(R.color.colorWhite)
                mBottomNavigation.menu.getItem(2).isChecked = true
                mBottomNavigationSeller.menu.getItem(2).isChecked = false
                position = 2
                search = false
                toolbarTitle.setTextColor(Color.parseColor("#181C1F"))
                setSupportActionBar(toolbar)
                return true
            }
            R.id.favorites -> {
                fragment = FavouritesFragment()
                loadFragment(fragment)
                toolbarTitle.text = "Favorites"
                productsSearchQuery = ""
                subscriptionSearchQuery = ""
                searchQuery.setQuery("", true)
                mBottomNavigation.menu.getItem(3).isChecked = true
                mBottomNavigationSeller.menu.getItem(3).isChecked = false
                linearLayoutCustomer.visibility = View.GONE
                searchIcon.visibility = View.GONE
                qr.visibility = View.GONE
                invite.visibility = View.GONE
                qr_black.visibility = View.GONE
                addSubscription.visibility = View.GONE
                notificationIcon.visibility = View.VISIBLE
                constraintNotifications.visibility = View.VISIBLE
                imageLogo.visibility = View.GONE
                notificationIcon.setImageResource(R.drawable.ic_notification_customer)
                toolbar.setBackgroundResource(R.color.colorWhite)
                tVEdit.visibility = View.GONE
                invitecode.visibility = View.GONE
                position = 3
                search = false
                searchLay.visibility = View.GONE
                toolbarTitle.visibility = View.VISIBLE
                toolbarTitle.setTextColor(Color.parseColor("#181C1F"))
                setSupportActionBar(toolbar)
                return true
            }
            R.id.profile -> {
                fragment = ProfileFragment()
                loadFragment(fragment)
                toolbarTitle.text = "Profile"
                productsSearchQuery = ""
                subscriptionSearchQuery = ""
                searchQuery.setQuery("", true)
                imageLogo.visibility = View.GONE
                searchIcon.visibility = View.GONE
                qr.visibility = View.VISIBLE
                qr.setImageResource(R.drawable.ic_qr_black)
                invite.visibility = View.GONE
                qr_black.visibility = View.GONE
                addSubscription.visibility = View.GONE
                notificationIcon.visibility = View.VISIBLE
                constraintNotifications.visibility = View.VISIBLE
                tVEdit.visibility = View.GONE
                invitecode.visibility = View.GONE
                toolbar.setBackgroundResource(R.color.editBack_button)
                searchLay.visibility = View.GONE
                toolbarTitle.visibility = View.VISIBLE
                linearLayoutCustomer.visibility = View.GONE
                mBottomNavigation.menu.getItem(4).isChecked = true
                mBottomNavigationSeller.menu.getItem(4).isChecked = false
                position = 4
                search = false
                toolbarTitle.setTextColor(Color.parseColor("#181C1F"))
                setSupportActionBar(toolbar)

                return true
            }


            R.id.dashboardSeller -> {
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                window.statusBarColor = ContextCompat.getColor(this, R.color.home_free)
                fragment = DashboardFragment()
                loadFragment(fragment)
                toolbarTitle.text = resources.getString(R.string.app_name)
                productsSearchQuery = ""
                subscriptionSearchQuery = ""
                searchQuery.setQuery("", true)
                imageLogo.visibility = View.GONE
                toolbar.setBackgroundResource(R.drawable.bg_toolbar_gradient)
                linearLayoutCustomer.visibility = View.GONE
                mBottomNavigationSeller.menu.getItem(0).isChecked = true
                mBottomNavigation.menu.getItem(0).isChecked = false
                position = 0
                search = false
                searchIcon.visibility = View.GONE
                qr.visibility = View.VISIBLE
                qr.setImageResource(R.drawable.ic_qr)
                invite.visibility = View.GONE
                qr_black.visibility = View.GONE
                searchLay.visibility = View.GONE
                toolbarTitle.visibility = View.VISIBLE
                addSubscription.visibility = View.GONE
                notificationIcon.visibility = View.VISIBLE
                constraintNotifications.visibility = View.VISIBLE
                toolbarTitle.setTextColor(Color.parseColor("#ffffff"))
                setSupportActionBar(toolbar)
                tVEdit.visibility = View.GONE
                invitecode.visibility = View.GONE

                invalidateOptionsMenu()
                return true
            }

            R.id.storeSeller -> {
                val type = Constant.getPrefs(this).getString(Constant.type, "")
                productsSearchQuery = ""
                subscriptionSearchQuery = ""
                searchQuery.setQuery("", true)

                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                window.statusBarColor = ContextCompat.getColor(this, R.color.home_free)
                fragment = StoreFragment()
                toolbar.setBackgroundResource(R.drawable.bg_toolbar_gradient)
                loadFragment(fragment)
                toolbarTitle.text = "Store"
                searchLay.visibility = View.GONE
                toolbarTitle.visibility = View.VISIBLE
                imageLogo.visibility = View.GONE
                setSupportActionBar(toolbar)
                linearLayoutCustomer.visibility = View.GONE
                mBottomNavigationSeller.menu.getItem(1).isChecked = true
                mBottomNavigation.menu.getItem(1).isChecked = false
                setSupportActionBar(toolbar)
                supportActionBar!!.elevation = 0f
                position = 1
                toolbar.visibility = View.VISIBLE
                search = false
                searchIcon.visibility = View.VISIBLE
                if (type == "3") {
                    addSubscription.visibility = View.VISIBLE
                    addSubscription.alpha = 0.4f
                    addSubscription.isClickable = false
                } else {
                    addSubscription.isClickable = true
                    addSubscription.visibility = View.VISIBLE
                }

                searchIcon.setColorFilter(Color.parseColor("#ffffff"))
                tVEdit.visibility = View.GONE
                invitecode.visibility = View.GONE
                qr.visibility = View.GONE
                invite.visibility = View.GONE
                qr_black.visibility = View.GONE
                notificationIcon.visibility = View.GONE
                constraintNotifications.visibility = View.GONE
                searchIcon.setOnClickListener {
                    Constant.showKeyBoard(this@DashboardActivity)
                    productsSearchQuery = ""
                    subscriptionSearchQuery = ""
                    searchQuery.setQuery("", true)
                    searchLay.visibility = View.VISIBLE
                    imageLogo.visibility = View.GONE
                    toolbarTitle.visibility = View.GONE
                    searchIcon.visibility = View.GONE
                    addSubscription.visibility = View.GONE
//                    if (type == "3") {
//                        addSubscription.visibility = View.VISIBLE
//                        addSubscription.alpha = 0.4f
//                        addSubscription.isClickable = false
//                    } else {
//                        addSubscription.isClickable = true
//                        addSubscription.visibility = View.VISIBLE
//                    }

                    notificationIcon.visibility = View.GONE
                    constraintNotifications.visibility = View.GONE
                    qr.visibility = View.GONE
                    invite.visibility = View.GONE
                    tVEdit.visibility = View.GONE
                    invitecode.visibility = View.GONE
                    setSupportActionBar(toolbar)

                }
                cancel.setOnClickListener {
                    Constant.hideKeyboard(this, cancel)
                    productsSearchQuery = ""
                    subscriptionSearchQuery = ""
                    searchQuery.setQuery("", true)
                    searchLay.visibility = View.GONE
                    toolbarTitle.visibility = View.VISIBLE
                    imageLogo.visibility = View.GONE
                    searchIcon.visibility = View.VISIBLE
                    if (type == "3") {
                        addSubscription.visibility = View.VISIBLE
                        addSubscription.alpha = 0.4f
                        addSubscription.isClickable = false
                    } else {
                        addSubscription.isClickable = true
                        addSubscription.visibility = View.VISIBLE
                    }
                    notificationIcon.visibility = View.GONE
                    constraintNotifications.visibility = View.GONE
                    qr.visibility = View.GONE
                    invite.visibility = View.GONE
                    tVEdit.visibility = View.GONE
                    invitecode.visibility = View.GONE
                    setSupportActionBar(toolbar)
                }

                searchQuery.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                    override fun onQueryTextSubmit(query: String): Boolean {
                        productsSearchQuery = query

                        apiImplimentationProducts(query)
                        return false
                    }

                    override fun onQueryTextChange(newText: String?): Boolean {

                        return false

                    }


                })

                invalidateOptionsMenu()
                return true
            }
            R.id.subscriptionSeller -> {
                val type = Constant.getPrefs(this).getString(Constant.type, "")
                productsSearchQuery = ""
                subscriptionSearchQuery = ""
                searchQuery.setQuery("", true)

                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                window.statusBarColor = ContextCompat.getColor(this, R.color.home_free)
                fragment =
                    SubscriptionSellerFragment()
                loadFragment(fragment)
                toolbarTitle.text = "Subscriptions"
                setSupportActionBar(toolbar)
                searchLay.visibility = View.GONE
                toolbarTitle.visibility = View.VISIBLE
                imageLogo.visibility = View.GONE
                mBottomNavigationSeller.menu.getItem(2).isChecked = true
                mBottomNavigation.menu.getItem(2).isChecked = false
                linearLayoutCustomer.visibility = View.GONE
                setSupportActionBar(toolbar)
                supportActionBar!!.elevation = 0f
                position = 2
                toolbar.visibility = View.VISIBLE
                search = false
                searchIcon.setColorFilter(Color.parseColor("#ffffff"))
                searchIcon.visibility = View.VISIBLE
                if (type == "3") {
                    addSubscription.visibility = View.VISIBLE
                    addSubscription.alpha = 0.4f
                    addSubscription.isClickable = false
                } else {
                    addSubscription.isClickable = true
                    addSubscription.visibility = View.VISIBLE
                }
                notificationIcon.visibility = View.GONE
                constraintNotifications.visibility = View.GONE
                tVEdit.visibility = View.GONE
                invitecode.visibility = View.GONE
                qr.visibility = View.GONE
                invite.visibility = View.GONE
                qr_black.visibility = View.GONE
                searchIcon.setOnClickListener {
                    Constant.showKeyBoard(this@DashboardActivity)
                    productsSearchQuery = ""
                    subscriptionSearchQuery = ""
                    searchQuery.setQuery("", true)
                    searchLay.visibility = View.VISIBLE
                    toolbarTitle.visibility = View.GONE
                    searchIcon.visibility = View.GONE
                    imageLogo.visibility = View.GONE
                    addSubscription.visibility = View.GONE
//                    if (type == "3") {
//                        addSubscription.visibility = View.VISIBLE
//                        addSubscription.alpha = 0.4f
//                        addSubscription.isClickable = false
//                    } else {
//                        addSubscription.isClickable = true
//                        addSubscription.visibility = View.VISIBLE
//                    }
                    notificationIcon.visibility = View.GONE
                    constraintNotifications.visibility = View.GONE
                    qr.visibility = View.GONE
                    invite.visibility = View.GONE
                    qr_black.visibility = View.GONE
                    tVEdit.visibility = View.GONE
                    invitecode.visibility = View.GONE
                    setSupportActionBar(toolbar)
                }
                cancel.setOnClickListener {
                    Constant.hideKeyboard(this, cancel)
                    productsSearchQuery = ""
                    subscriptionSearchQuery = ""
                    searchQuery.setQuery("", true)
                    searchLay.visibility = View.GONE
                    toolbarTitle.visibility = View.VISIBLE
                    searchIcon.visibility = View.VISIBLE
                    imageLogo.visibility = View.GONE
                    if (type == "3") {
                        addSubscription.visibility = View.VISIBLE
                        addSubscription.alpha = 0.4f
                        addSubscription.isClickable = false
                    } else {
                        addSubscription.isClickable = true
                        addSubscription.visibility = View.VISIBLE
                    }

                    notificationIcon.visibility = View.GONE
                    constraintNotifications.visibility = View.GONE
                    qr.visibility = View.GONE
                    invite.visibility = View.GONE
                    qr_black.visibility = View.GONE
                    tVEdit.visibility = View.GONE
                    invitecode.visibility = View.GONE
                    setSupportActionBar(toolbar)
                }


                searchQuery.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                    override fun onQueryTextSubmit(query: String): Boolean {
                        subscriptionSearchQuery = query

                        apiImplimentationSubscription(query)
                        return false
                    }

                    override fun onQueryTextChange(newText: String?): Boolean {

                        return false

                    }


                })


                invalidateOptionsMenu()
                return true
            }

            R.id.customers -> {
                productsSearchQuery = ""
                searchQuery.setQuery("", true)
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                window.statusBarColor = ContextCompat.getColor(this, R.color.home_free)
                setSupportActionBar(toolbar)
                supportActionBar!!.elevation = 0f
                searchLay.visibility = View.GONE
                toolbarTitle.visibility = View.VISIBLE
                fragment = CustomersFragment()
                loadFragment(fragment)
                toolbarTitle.text = "Customers"
                mBottomNavigationSeller.menu.getItem(3).isChecked = true
                mBottomNavigation.menu.getItem(3).isChecked = false
                toolbar.visibility = View.VISIBLE
                searchIcon.setColorFilter(Color.parseColor("#ffffff"))
                linearLayoutCustomer.visibility = View.GONE
                searchIcon.visibility = View.VISIBLE
                addSubscription.visibility = View.GONE
                notificationIcon.visibility = View.GONE
                constraintNotifications.visibility = View.GONE
                tVEdit.visibility = View.GONE
                invitecode.visibility = View.GONE
                imageLogo.visibility = View.GONE
                qr.visibility = View.GONE
                qr_black.visibility = View.GONE
//
                position = 3
                search = false
                searchIcon.setOnClickListener {
                    productsSearchQuery = ""
                    searchQuery.setQuery("", true)
                    searchLay.visibility = View.VISIBLE
                    toolbarTitle.visibility = View.GONE
                    searchIcon.visibility = View.GONE
                    addSubscription.visibility = View.GONE
                    imageLogo.visibility = View.GONE
                    notificationIcon.visibility = View.GONE
                    constraintNotifications.visibility = View.GONE
                    qr.visibility = View.GONE
                    invite.visibility = View.GONE
                    qr_black.visibility = View.GONE
                    tVEdit.visibility = View.GONE
                    invitecode.visibility = View.GONE
                    setSupportActionBar(toolbar)

                }
                cancel.setOnClickListener {
                    Constant.hideKeyboard(this, cancel)
                    productsSearchQuery = ""
                    searchQuery.setQuery("", true)
                    searchLay.visibility = View.GONE
                    toolbarTitle.visibility = View.VISIBLE
                    searchIcon.visibility = View.VISIBLE
                    addSubscription.visibility = View.GONE
                    imageLogo.visibility = View.GONE
                    notificationIcon.visibility = View.GONE
                    constraintNotifications.visibility = View.GONE
                    qr.visibility = View.GONE
                    qr_black.visibility = View.GONE
                    invite.visibility = View.VISIBLE
                    tVEdit.visibility = View.GONE
                    invitecode.visibility = View.GONE
                    setSupportActionBar(toolbar)
                }

                invalidateOptionsMenu()
                return true
            }

            R.id.profileSeller -> {
                productsSearchQuery = ""
                searchQuery.setQuery("", true)
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                window.statusBarColor = ContextCompat.getColor(this, R.color.home_free)
                fragment = ProfileFragment()
                loadFragment(fragment)
                toolbarTitle.text = "Profile"
                toolbar.setBackgroundResource(R.drawable.bg_toolbar_gradient)
                linearLayoutCustomer.visibility = View.GONE
                mBottomNavigationSeller.menu.getItem(4).isChecked = true
                mBottomNavigation.menu.getItem(4).isChecked = false
                searchIcon.visibility = View.GONE
                qr.visibility = View.GONE
                invite.visibility = View.GONE
                qr_black.visibility = View.GONE
                imageLogo.visibility = View.GONE
                addSubscription.visibility = View.GONE
                notificationIcon.visibility = View.VISIBLE
                constraintNotifications.visibility = View.VISIBLE
                tVEdit.visibility = View.GONE
                invitecode.visibility = View.GONE
                position = 4
                searchLay.visibility = View.GONE
                toolbarTitle.visibility = View.VISIBLE
                search = false
                toolbarTitle.setTextColor(Color.parseColor("#ffffff"))
                setSupportActionBar(toolbar)
                invalidateOptionsMenu()
                return true
            }
        }
        return false

    }

    private fun apiImplimentationSubscription(query: String) {
        val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
        mViewModelSubscriptions.getAllSubscriptionsData(auth, query)
        progressBar.visibility = View.VISIBLE

    }

    private fun observerInit() {
        viewModel.getAllProducts().observe(this, Observer {
            progressBar.visibility = View.GONE
            try {
//                if (it[0].status1 == "true") {
                val intent = Intent(this, StoreSearchResultsActivity::class.java)
                intent.putExtra("arrayStoreResults", it)
                intent.putExtra("productsSearchQuery", productsSearchQuery)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
//                }
            } catch (e: Exception) {
                e.printStackTrace()
            }

        })

        viewModel.getStatus().observe(this, Observer {
            progressBar.visibility = View.GONE
            if (it.msg == "Invalid auth code") {
                Constant.commonAlert(this)
            } else {
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
            }
        })


    }


    private fun observerInitSubscription() {
        mViewModelSubscriptions.getmAllCampaignsData().observe(this, Observer {
            progressBar.visibility = View.GONE
            try {
//                if (it[0].status == "true") {
                val intent = Intent(this, SubscriptionSearchResultActivity::class.java)
                intent.putExtra("arraySubscriptions", it)
                intent.putExtra("subscriptionSearchQuery", subscriptionSearchQuery)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
//                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        })
    }


    private fun apiImplimentationProducts(query: String) {
        progressBar.visibility = View.VISIBLE
        viewModel.getProducts(query)

    }

    override fun nextFragments(i: Int) {
        when (i) {
            1 -> {
                inflateFragment(SortByFragment())
                mreset.visibility = View.GONE
                filterImgBack.visibility = View.VISIBLE
                filters.text = "Sort By"

                filterImgBack.setOnClickListener {
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"
                }

                filter_done.setOnClickListener {
                    if (checkFragment is FilterFragment) {
                        modelData = (checkFragment as FilterFragment).getData()
                        val mPrefs = getSharedPreferences("data", MODE_PRIVATE)
                        val prefsEditor = mPrefs.edit()
                        val gson = Gson()
                        val json = gson.toJson(modelData)
                        prefsEditor.putString("filterData", json)
                        prefsEditor.apply()
                        if (modelData.latitude != "" && modelData.longitude != "") {

                            if (homeFragment is HomeFragment) {
                                (homeFragment as HomeFragment).runApi(this)
                            }
                        } else {
                            if (homeFragment is HomeFragment) {
                                (homeFragment as HomeFragment).runApi(this)
                            }
                        }
                    } else {
                        if (homeFragment is HomeFragment) {
                            (homeFragment as HomeFragment).runApi(this)
                        }
                    }


                    onBackPressed()
                }

                reset.setOnClickListener {
                    inflateFragment(FilterFragment(this))
                    businessId = ""
                    businesName = ""
                    sortName = "No Filter Selected"
                    checkedItems.clear()
                    val mPrefs = getSharedPreferences("data", MODE_PRIVATE)
                    val prefsEditor = mPrefs.edit()
                    prefsEditor.putString("filterData", "")
                    prefsEditor.apply()
                    if (homeFragment is HomeFragment) {
                        (homeFragment as HomeFragment).runApi(this)
                    }
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"

                }
            }


            2 -> {
//                checkedItems.clear()
                inflateFragment(CategoryFilterFragment(this))
                mreset.visibility = View.GONE
                filterImgBack.visibility = View.VISIBLE
                filters.text = "Category"

                filterImgBack.setOnClickListener {
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"

                }
                filter_done.setOnClickListener {
                    // inflateFragment(FilterFragment())
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"


                    if (checkFragment is FilterFragment) {
                        modelData = (checkFragment as FilterFragment).getData()
                        val mPrefs = getSharedPreferences("data", MODE_PRIVATE)
                        val prefsEditor = mPrefs.edit()
                        val gson = Gson()
                        val json = gson.toJson(modelData)
                        prefsEditor.putString("filterData", json)
                        prefsEditor.apply()
                        if (modelData.latitude != "" && modelData.longitude != "") {

                            if (homeFragment is HomeFragment) {
                                (homeFragment as HomeFragment).runApi(this)
                            }
                        } else {
                            if (homeFragment is HomeFragment) {
                                (homeFragment as HomeFragment).runApi(this)
                            }
                        }
                    } else {
                        if (checkFragment is CategoryFilterFragment) {
                            checkedItems =
                                (checkFragment as CategoryFilterFragment).getCheckedCategories()
                        }
                        if (homeFragment is HomeFragment) {
                            (homeFragment as HomeFragment).runApi(this)
                        }
                    }
                    onBackPressed()
                }

                reset.setOnClickListener {
                    businessId = ""
                    businesName = ""
                    sortName = "No Filter Selected"
                    checkedItems.clear()
                    val mPrefs = getSharedPreferences("data", MODE_PRIVATE)
                    val prefsEditor = mPrefs.edit()
                    prefsEditor.putString("filterData", "")
                    prefsEditor.apply()
                    if (homeFragment is HomeFragment) {
                        (homeFragment as HomeFragment).runApi(this)
                    }
                    if (homeFragment is HomeFragment) {
                        (homeFragment as HomeFragment).runApi(this)
                    }
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"
                }
            }


            3 -> {
                inflateFragment(BrandsFilterFragment())
                mreset.visibility = View.GONE
                filterImgBack.visibility = View.VISIBLE
                filters.text = "Brands"

                filterImgBack.setOnClickListener {
                    Constant.hideKeyboard(this, cancel)
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"

                }

                filter_done.setOnClickListener {

                    if (checkFragment is FilterFragment) {
                        modelData = (checkFragment as FilterFragment).getData()
                        val mPrefs = getSharedPreferences("data", MODE_PRIVATE)
                        val prefsEditor = mPrefs.edit()
                        val gson = Gson()
                        val json = gson.toJson(modelData)
                        prefsEditor.putString("filterData", json)
                        prefsEditor.apply()
                        if (modelData.latitude != "" && modelData.longitude != "") {

                            if (homeFragment is HomeFragment) {
                                (homeFragment as HomeFragment).runApi(this)
                            }
                        } else {
                            if (homeFragment is HomeFragment) {
                                (homeFragment as HomeFragment).runApi(this)
                            }
                        }
                        onBackPressed()
                    } else {
                        inflateFragment(FilterFragment(this))
                        mreset.visibility = View.VISIBLE
                        filterImgBack.visibility = View.GONE
                        filters.text = "Filters"
                    }

                }

                reset.setOnClickListener {
                    businessId = ""
                    businesName = ""
                    sortName = "No Filter Selected"
                    checkedItems.clear()
                    val mPrefs = getSharedPreferences("data", MODE_PRIVATE)
                    val prefsEditor = mPrefs.edit()
                    prefsEditor.putString("filterData", "")
                    prefsEditor.apply()
                    if (homeFragment is HomeFragment) {
                        (homeFragment as HomeFragment).runApi(this)
                    }
                    inflateFragment(FilterFragment(this))
                    if (homeFragment is HomeFragment) {
                        (homeFragment as HomeFragment).runApi(this)
                    }
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"


                }
            }
            4 -> {
                inflateFragment(RedemptionFilterFragment())
                mreset.visibility = View.GONE
                filterImgBack.visibility = View.VISIBLE
                filters.text = "Redemption"

                filterImgBack.setOnClickListener {
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"

                }
                filter_done.setOnClickListener {
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"
                }

                reset.setOnClickListener {
                    businessId = ""
                    businesName = ""
                    sortName = "No Filter Selected"
                    checkedItems.clear()
                    val mPrefs = getSharedPreferences("data", MODE_PRIVATE)
                    val prefsEditor = mPrefs.edit()
                    prefsEditor.putString("filterData", "")
                    prefsEditor.apply()
                    if (homeFragment is HomeFragment) {
                        (homeFragment as HomeFragment).runApi(this)
                    }
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"
                }
            }
        }

    }

    private fun inflateFragment(fragment: Fragment) {
        checkFragment = fragment
        fragmentManager = supportFragmentManager
        this.fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.containerSide, fragment)
        fragmentTransaction.commit()
    }

    @SuppressLint("ValidFragment")
    class MenuFragment(
        var adapterRedeem: AdapterRedeem,
        var mViewModel: MySubscriptionViewModel
    ) : RoundedBottomSheetDialogFragment() {

        lateinit var rvRedeem: RecyclerView
        lateinit var cl_redeem: ConstraintLayout
        lateinit var image: ImageView
        lateinit var imageView82: ImageView
        lateinit var tvNoData: TextView
        lateinit var tvNoDataDescription: TextView
        lateinit var inviteCode: TextView
        lateinit var progressBar: LinearLayout

        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val view = inflater.inflate(R.layout.redeem_sheet, container, false)
            rvRedeem = view.findViewById(R.id.rvRedeem)
            image = view.findViewById(R.id.topBg)
            cl_redeem = view.findViewById(R.id.cl_redeem)
            imageView82 = view.findViewById(R.id.imageView82)
            tvNoData = view.findViewById(R.id.tvNoData)
            tvNoDataDescription = view.findViewById(R.id.tvNoDataDescription)
            progressBar = view.findViewById(R.id.progressBar)
            inviteCode = view.findViewById(R.id.inviteCode)
            inviteCode.visibility = View.GONE
            clickListeners()
            roundedImage()
            apiImplimentation()
            observerInit()
            return view
        }

        private fun clickListeners() {
            inviteCode.setOnClickListener {
                startActivity(Intent(activity, InviteCodeActivity::class.java))
            }

        }

        private fun observerInit() {
            mViewModel.getStatus().observe(this, Observer {
                progressBar.visibility = View.GONE
                if (it.msg == "No subscription exists.") {
                    imageView82.visibility = View.VISIBLE
                    tvNoData.visibility = View.VISIBLE
                    tvNoDataDescription.visibility = View.VISIBLE
                } else {
                    imageView82.visibility = View.GONE
                    tvNoData.visibility = View.GONE
                    tvNoDataDescription.visibility = View.GONE
                    Toast.makeText(activity, it.msg, Toast.LENGTH_SHORT).show()
                }
            })

            mViewModel.getmDataSubscriptions().observe(this, Observer {
                progressBar.visibility = View.GONE
                rvRedeem.layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
                rvRedeem.adapter = adapterRedeem
                adapterRedeem.update(it)
            })

        }


        private fun apiImplimentation() {
            val auth = Constant.getPrefs(activity!!).getString(Constant.auth_code, "")
            progressBar.visibility = View.VISIBLE
            mViewModel.getMySubscriptionList(auth,"1")
        }

//        private fun setAdapter() {
//            rvRedeem.layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
//            rvRedeem.isNestedScrollingEnabled = true
//            rvRedeem.adapter = adapterRedeem
//
//        }

        private fun roundedImage() {
            val curveRadius = 50F
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                image.outlineProvider = object : ViewOutlineProvider() {
                    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                    override fun getOutline(view: View?, outline: Outline?) {
                        outline?.setRoundRect(
                            0,
                            0,
                            view!!.width,
                            view.height + curveRadius.toInt(),
                            curveRadius
                        )
                    }
                }
                image.clipToOutline = true
            }
        }
    }

//    override fun dismissSheet() {
////        if (fragmentMenu != null) {
////            fragmentMenu.dismiss()
////        }
//    }

    override fun OpenSheet(
        position: Int,
        model: MySubscriptionsModel
    ) {
        val fragment = MenuFragmentRedeem(model, fragmentMenu)
        fragment.show(fragmentManager, fragment.tag)
//        fragmentMenu.dismiss()

//        startActivity(
//            Intent(this, ReedemActivity::class.java).putExtra(
//                "quantity")
//                .putExtra("model", model)
//        )
//        fragmentMenu.dismiss()


    }

    class MenuFragmentRedeem(
        var model: MySubscriptionsModel,
        var fragmentMenu: MenuFragment
    ) : RoundedBottomSheetDialogFragment() {

        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val view = inflater.inflate(R.layout.redeem_sheet_quantity, container, false)

            val invite = view.findViewById<TextView>(R.id.invite)
            val etQuantity = view.findViewById<TextView>(R.id.etQuantity)
            val imageViewTop = view.findViewById<ImageView>(R.id.imageViewTop)
            roundImage(imageViewTop)

            invite.setOnClickListener {
                if (etQuantity.text.isEmpty()) {
                    Toast.makeText(activity, "Please select quantity first!", Toast.LENGTH_LONG)
                        .show()
                } else {
                    startActivity(
                        Intent(this.activity, ReedemActivity::class.java).putExtra(
                                "quantity",
                                etQuantity.text.toString()
                            )
                            .putExtra("model", model)
                    )
                    fragmentMenu.dismiss()
                    dismiss()
                }
            }

            etQuantity.setOnClickListener {
                val choices = getList()
                val charSequenceItems = choices.toArray(arrayOfNulls<CharSequence>(choices.size))
                val mBuilder = AlertDialog.Builder(activity!!)
                mBuilder.setSingleChoiceItems(charSequenceItems, -1) { dialogInterface, i ->
                    etQuantity.text = choices[i]
                    dialogInterface.dismiss()

                }
                val mDialog = mBuilder.create()
                mDialog.show()
            }

            etQuantity.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(
                    s: CharSequence,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                    if (getTextFieldsData()) {
                        invite.setBackgroundResource(R.drawable.favorite_delete_background)
                    } else {
                        invite.setBackgroundResource(R.drawable.favorite_delete_background_opacity)
                    }
                }

                override fun afterTextChanged(s: Editable) {
                }
            })
            return view
        }

        private fun getTextFieldsData(): Boolean {
            val quantity = etQuantity.text.toString().trim().length

            if (quantity == 0) {
                return false
            }

            return true

        }

        private fun roundImage(imageViewTop: ImageView) {
            val curveRadius = 50F

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                imageViewTop.outlineProvider = object : ViewOutlineProvider() {

                    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                    override fun getOutline(view: View?, outline: Outline?) {
                        outline?.setRoundRect(
                            0,
                            0,
                            view!!.width,
                            view.height + curveRadius.toInt(),
                            curveRadius

                        )
                    }
                }

                imageViewTop.clipToOutline = true

            }

        }

        private fun getList(): ArrayList<String> {
            val arrayList: ArrayList<String> = ArrayList()


            if (model.free_trial_qty.toInt() > 0) {
                for (i in 0 until (model.free_trial_qty.toInt() - model.redeem_count.toInt())) {

                    arrayList.add((i + 1).toString())
                }
                if (arrayList.size == 0) {
                    if (model.redeemtion_cycle_qty.toInt() > 100) {
                        for (i in 0 until 500) {
                            arrayList.add((i + 1).toString())
                        }
                    } else {
                        for (i in 0 until (model.redeemtion_cycle_qty.toInt() - model.redeem_count.toInt())) {

                            arrayList.add((i + 1).toString())
                        }
                    }
                }
            } else {
                if (model.redeemtion_cycle_qty.toInt() > 100) {
                    for (i in 0 until 500) {
                        arrayList.add((i + 1).toString())
                    }
                } else {
                    for (i in 0 until (model.redeemtion_cycle_qty.toInt() - model.redeem_count.toInt())) {

                        arrayList.add((i + 1).toString())
                    }
                }
            }
            return arrayList
        }
    }

    @SuppressLint("SetTextI18n")
    override fun getSelectedBrand(
        id: String,
        position: Int,
        businessName: String
    ) {
        inflateFragment(FilterFragment(this))
        businessId = id
        businesName = businessName
        mreset.visibility = View.VISIBLE
        filterImgBack.visibility = View.GONE
        filters.text = "Filters"
        if (checkFragment is FilterFragment) {
            (checkFragment as FilterFragment).updateBrandData(businessName)
        }
    }

    override fun getSelectedSort(name: String, position: Int) {
        sortName = name

    }

    override fun sendData(homeModelMain: ArrayList<ModelHomeDataSubscriptions>) {
        homeData = homeModelMain
    }


    override fun switch() {
        val type = Constant.getPrefs(this).getString(Constant.type, "")
        productsSearchQuery = ""
        subscriptionSearchQuery = ""
        searchQuery.setQuery("", true)

        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        window.statusBarColor = ContextCompat.getColor(this, R.color.home_free)
        fragment =
            SubscriptionSellerFragment()
        loadFragment(fragment)
        toolbarTitle.text = "Subscriptions"
        setSupportActionBar(toolbar)
        searchLay.visibility = View.GONE
        toolbarTitle.visibility = View.VISIBLE
        imageLogo.visibility = View.GONE
        mBottomNavigationSeller.menu.getItem(2).isChecked = true
        mBottomNavigation.menu.getItem(2).isChecked = false
        linearLayoutCustomer.visibility = View.GONE
        setSupportActionBar(toolbar)
        supportActionBar!!.elevation = 0f
        position = 2
        toolbar.visibility = View.VISIBLE
        search = false
        searchIcon.setColorFilter(Color.parseColor("#ffffff"))
        searchIcon.visibility = View.VISIBLE
        if (type == "3") {
            addSubscription.visibility = View.VISIBLE
            addSubscription.alpha = 0.4f
            addSubscription.isClickable = false
        } else {
            addSubscription.isClickable = true
            addSubscription.visibility = View.VISIBLE
        }
        notificationIcon.visibility = View.GONE
        constraintNotifications.visibility = View.GONE
        tVEdit.visibility = View.GONE
        invitecode.visibility = View.GONE
        qr.visibility = View.GONE
        invite.visibility = View.GONE
        qr_black.visibility = View.GONE
        searchIcon.setOnClickListener {
            Constant.showKeyBoard(this@DashboardActivity)
            productsSearchQuery = ""
            subscriptionSearchQuery = ""
            searchQuery.setQuery("", true)
            searchLay.visibility = View.VISIBLE
            toolbarTitle.visibility = View.GONE
            searchIcon.visibility = View.GONE
            imageLogo.visibility = View.GONE
            addSubscription.visibility = View.GONE
//            if (type == "3") {
//                addSubscription.visibility = View.VISIBLE
//                addSubscription.alpha = 0.4f
//                addSubscription.isClickable = false
//            } else {
//                addSubscription.isClickable = true
//                addSubscription.visibility = View.VISIBLE
//            }
            notificationIcon.visibility = View.GONE
            constraintNotifications.visibility = View.GONE
            qr.visibility = View.GONE
            invite.visibility = View.GONE
            qr_black.visibility = View.GONE
            tVEdit.visibility = View.GONE
            invitecode.visibility = View.GONE
            setSupportActionBar(toolbar)
        }
        cancel.setOnClickListener {
            Constant.hideKeyboard(this, cancel)
            productsSearchQuery = ""
            subscriptionSearchQuery = ""
            searchQuery.setQuery("", true)
            searchLay.visibility = View.GONE
            toolbarTitle.visibility = View.VISIBLE
            searchIcon.visibility = View.VISIBLE
            imageLogo.visibility = View.GONE
            if (type == "3") {
                addSubscription.visibility = View.VISIBLE
                addSubscription.alpha = 0.4f
                addSubscription.isClickable = false
            } else {
                addSubscription.isClickable = true
                addSubscription.visibility = View.VISIBLE
            }

            notificationIcon.visibility = View.GONE
            constraintNotifications.visibility = View.GONE
            qr.visibility = View.GONE
            invite.visibility = View.GONE
            qr_black.visibility = View.GONE
            tVEdit.visibility = View.GONE
            invitecode.visibility = View.GONE
            setSupportActionBar(toolbar)
        }


        searchQuery.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                subscriptionSearchQuery = query

                apiImplimentationSubscription(query)
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {

                return false

            }


        })
        invalidateOptionsMenu()
    }
}