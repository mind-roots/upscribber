package com.upscribber.dashboard

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.upscribber.commonClasses.Constant
import com.upscribber.filters.*
import com.upscribber.filters.Redemption.RedemptionFilterFragment
import com.upscribber.R
import com.upscribber.home.ModelHomeDataSubscriptions
import kotlinx.android.synthetic.main.activity_dashboard.*
import kotlinx.android.synthetic.main.filter_header.*

class SearchActivity : AppCompatActivity(), FilterInterface, DrawerLayout.DrawerListener,
    FilterFragment.SetFilterHeaderBackground, FilterHomeInterface, BrandAdapter.Selection, SortByAdapter.Selection {

    private lateinit var toolbar: Toolbar
    private lateinit var toolbarTitle: TextView
    private lateinit var filter_done: TextView
    var drawerOpen = 0
    private lateinit var fragmentManager: FragmentManager
    private lateinit var fragmentTransaction: FragmentTransaction
    private lateinit var mreset: TextView
    private lateinit var btnFilter: TextView
    private lateinit var mfilters: TextView
    private var businessName = ""
    private var brandPosition = -1
    lateinit var checkFragment: Fragment

    companion object {
        var businessName = ""
        var sortName = "None"
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        initz()
        setToolbar()

    }

    private fun setToolbar() {
        setSupportActionBar(toolbar)
        title = ""
        toolbarTitle.text = "Search"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)

    }

    private fun initz() {
        toolbar = findViewById(R.id.include4)
        toolbarTitle = findViewById(R.id.title)
        btnFilter = findViewById(R.id.btnFilter)
        mreset = findViewById(R.id.reset)
        mfilters = findViewById(R.id.filters)
        filter_done = findViewById(R.id.filter_done)

    }

    override fun onResume() {
        super.onResume()
        Constant.hideKeyboard(this, filter_done)
    }

    override fun updateDataBackground(i: ModelHomeDataSubscriptions) {


    }

    override fun openFilter(i: Int) {
        drawerLayout.openDrawer(GravityCompat.END)
        Constant.hideKeyboard(this, filter_done)
        if (filter_done != null) {
//            filter_done.setQuery("", false);
            filter_done.clearFocus();
        }
        inflateFragment(FilterFragment(this))
        filterImgBack.visibility = View.GONE
        mfilters.text = "Filters"
        mreset.visibility = View.VISIBLE
    }

    private fun inflateFragment(fragment: Fragment) {
        checkFragment = fragment
        fragmentManager = supportFragmentManager
        this.fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.containerSide2, fragment)
        fragmentTransaction.commit()
    }

    override fun onBackPressed() {
        if (drawerOpen == 1) {
            drawerOpen = 0
            drawerLayout.closeDrawer(GravityCompat.END)
        } else {
            super.onBackPressed()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun nextFragments(i: Int) {
        when (i) {
            1 -> {
                inflateFragment(SortByFragment())
                mreset.visibility = View.GONE
                filterImgBack.visibility = View.VISIBLE
                filters.text = "Sort By"

                filterImgBack.setOnClickListener {
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"
                }

                filter_done.setOnClickListener {
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"
                }
            }


            2 -> {
                inflateFragment(CategoryFilterFragment(this))
                mreset.visibility = View.GONE
                filterImgBack.visibility = View.VISIBLE
                filters.text = "Category"

                filterImgBack.setOnClickListener {
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"

                }
                filter_done.setOnClickListener {
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"
                }
            }


            3 -> {
                inflateFragment(BrandsFilterFragment())
                mreset.visibility = View.GONE
                filterImgBack.visibility = View.VISIBLE
                filters.text = "Brands"

                filterImgBack.setOnClickListener {
                    Constant.hideKeyboard(this, filter_done)
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"

                }

                filter_done.setOnClickListener {
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"

                }
            }
            4 -> {
                inflateFragment(RedemptionFilterFragment())
                mreset.visibility = View.GONE
                filterImgBack.visibility = View.VISIBLE
                filters.text = "Redemption"

                filterImgBack.setOnClickListener {
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"

                }
                filter_done.setOnClickListener {
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"
                }
            }
        }
    }

    override fun onDrawerStateChanged(newState: Int) {


    }

    override fun onDrawerSlide(drawerView: View, slideOffset: Float) {

    }

    override fun onDrawerClosed(drawerView: View) {
        drawerOpen = 0

    }

    override fun filterHeaderBackground(s: String) {

        if (s == "new") {
            filterHeader.setBackgroundDrawable(getDrawable(R.drawable.filter_header_select_category))


        } else {
            filterHeader.setBackgroundDrawable(getDrawable(R.drawable.filter_header))

        }
    }

    override fun onDrawerOpened(drawerView: View) {
        drawerOpen = 1
        drawerLayout.openDrawer(GravityCompat.END)
        inflateFragment(FilterFragment(this))
        filterImgBack.visibility = View.GONE
        mfilters.text = "Filters"
        mreset.visibility = View.VISIBLE
    }

    override fun getSelectedBrand(
        name: String,
        position: Int,
        nameBusines: String
    ) {
        businessName = name
        inflateFragment(FilterFragment(this))
        mreset.visibility = View.VISIBLE
        filterImgBack.visibility = View.GONE
        filters.text = "Filters"

        if (checkFragment is FilterFragment) {
            (checkFragment as FilterFragment).updateBrandData(businessName)
        }
    }

    override fun getSelectedSort(name: String, position: Int) {
        sortName = name
    }

    //    override fun getDataSelection(name: String, position: Int) {
//        businessName = name
//        brandPosition = position
//    }
}