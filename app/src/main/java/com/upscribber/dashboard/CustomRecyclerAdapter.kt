package com.upscribber.dashboard

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.R

class CustomRecyclerAdapter(val context : Context, var courseList: ArrayList<SearchModel>)
    : RecyclerView.Adapter<CustomRecyclerAdapter.ViewHolder>() {

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        p0?.txtTitle?.text = courseList[p1].name
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val v = LayoutInflater.from(p0?.context).inflate(R.layout.search_recycler_view, p0, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return courseList.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val txtTitle = itemView.findViewById<TextView>(R.id.tv_weather)
    }

    // To get the data to search Category
    fun filterList(filteredCourseList: ArrayList<SearchModel>) {
        this.courseList = filteredCourseList
        notifyDataSetChanged()
    }
}