package com.upscribber.dashboard

import com.upscribber.home.ModelHomeDataSubscriptions

interface FilterHomeInterface {

    fun openFilter(i : Int)
    fun updateDataBackground(i: ModelHomeDataSubscriptions)
}