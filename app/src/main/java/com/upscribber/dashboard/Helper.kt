package com.upscribber.dashboard



class Helper{
    companion object {
        fun <ArrayList> getVersionsList(): ArrayList {
            var courseList = ArrayList<SearchModel>()
            courseList.add(SearchModel("Kotlin Android"))
            courseList.add(SearchModel("Java Android"))
            courseList.add(SearchModel("Swift iOS"))
            courseList.add(SearchModel("Objective C iOS"))
            courseList.add(SearchModel("Python"))
            courseList.add(SearchModel("React"))

            return courseList as ArrayList
        }
    }
}