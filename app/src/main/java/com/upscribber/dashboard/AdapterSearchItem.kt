package com.upscribber.dashboard

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.R
import kotlinx.android.synthetic.main.search_recycler_view.view.*

class AdapterSearchItem( var arrayMain: ArrayList<SearchModel>,
                         var array: ArrayList<SearchModel>,
                         var context: Context)
    : RecyclerView.Adapter<AdapterSearchItem.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(context)
            .inflate(R.layout.search_recycler_view, parent, false)
        return ViewHolder(v)    }

    override fun getItemCount(): Int {
        return array.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = arrayMain[position]

        holder.itemView.tv_weather.text = model.name


    }


    class ViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView)

}
