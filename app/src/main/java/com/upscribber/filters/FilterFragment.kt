package com.upscribber.filters


import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.location.Geocoder
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.google.gson.Gson
import com.upscribber.R
import com.upscribber.categories.subCategories.SubCategoriesActivity
import com.upscribber.commonClasses.Constant
import com.upscribber.dashboard.DashboardActivity
import com.upscribber.dashboard.SearchActivity
import com.upscribber.home.MapsSearchActivity


class FilterFragment(val mContext: Context) : Fragment() {

    lateinit var updateClick: FilterInterface
    lateinit var backHeader: SetFilterHeaderBackground
    private lateinit var msortLayout: LinearLayout
    private lateinit var dateRange: LinearLayout
    private lateinit var productsLayout: LinearLayout
    private lateinit var subscriptionLayout: LinearLayout
    private lateinit var redemptionLayout: LinearLayout
    private lateinit var cancellationLayout: LinearLayout
    private lateinit var sharingLayout: LinearLayout
    private lateinit var viewsLayout: LinearLayout
    private lateinit var newView: LinearLayout
    private lateinit var oldView: LinearLayout
    private lateinit var mcategoryLayout: LinearLayout
    private lateinit var mbrandsLayout: LinearLayout
    private lateinit var mLl_parent: LinearLayout
    private lateinit var linearLayLocation: LinearLayout
    private lateinit var rangeSeekbar1: CrystalRangeSeekbar
    private lateinit var rangeSeekbar2: CrystalRangeSeekbar
    private lateinit var textMin1: TextView
    private lateinit var textMax1: TextView
    private lateinit var textMin2: TextView
    private lateinit var textMax2: TextView
    private lateinit var brandName: TextView
    private lateinit var sortBy: TextView
    private lateinit var location: TextView
    private lateinit var category: TextView
    private var brandNameValue = "All"
    private var sortNameValue = "No Filter Selected"
    private var latitude = ""
    private var longitude = ""
    var minDistance = "0"
    var maxDistance = "1000"
    var minPrice = "0"
    var maxPrice = "1000"
    var modelData = ModelData()
    var latitude1 = 0.0
    var longitude1 = 0.0


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.filter, container, false)

        viewsLayout = view.findViewById(R.id.viewsLayout)
        sharingLayout = view.findViewById(R.id.sharingLayout)
        cancellationLayout = view.findViewById(R.id.cancellationLayout)
        redemptionLayout = view.findViewById(R.id.redemptionLayout)
        subscriptionLayout = view.findViewById(R.id.subscriptionLayout)
        productsLayout = view.findViewById(R.id.productsLayout)
        dateRange = view.findViewById(R.id.dateRange)
        msortLayout = view.findViewById(R.id.sort_layout)
        mcategoryLayout = view.findViewById(R.id.category_layout)
        mbrandsLayout = view.findViewById(R.id.brands_layout)
        mLl_parent = view.findViewById(R.id.ll_parent)
        newView = view.findViewById(R.id.newView)
        oldView = view.findViewById(R.id.oldView)
        rangeSeekbar1 = view.findViewById(R.id.rangeSeekbar1)
        rangeSeekbar2 = view.findViewById(R.id.rangeSeekbar2)
        textMin1 = view.findViewById(R.id.textMin1)
        textMax1 = view.findViewById(R.id.textMax1)
        textMin2 = view.findViewById(R.id.textMin2)
        textMax2 = view.findViewById(R.id.textMax2)
        brandName = view.findViewById(R.id.brandName)
        sortBy = view.findViewById(R.id.sortBy)
        category = view.findViewById(R.id.category)
        location = view.findViewById(R.id.location)
        linearLayLocation = view.findViewById(R.id.linearLayLocation)
        Places.initialize(
            activity!!.applicationContext,
            resources.getString(R.string.google_maps_key)
        )

        val isSeller = Constant.getSharedPrefs(activity!!).getBoolean(Constant.IS_SELLER, false)

        if (isSeller) {
            newView.visibility = View.VISIBLE
            oldView.visibility = View.GONE
            backHeader.filterHeaderBackground("new")
        } else {
            newView.visibility = View.GONE
            oldView.visibility = View.VISIBLE
            backHeader.filterHeaderBackground("old")
        }
        setRangeSeekbar1()
        setRangeSeekbar2()
        clickEvents()
        return view
    }

    private fun init() {

    }

    private fun clickEvents() {
        msortLayout.setOnClickListener {
            updateClick.nextFragments(1)
        }

        mcategoryLayout.setOnClickListener {
            updateClick.nextFragments(2)
        }

        mbrandsLayout.setOnClickListener {
            updateClick.nextFragments(3)
        }

        linearLayLocation.setOnClickListener {
            val placesClient = Places.createClient(activity!!)
            val fields =
                listOf(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS)
            // Start the autocomplete intent.
            val intent = Autocomplete.IntentBuilder(
                AutocompleteActivityMode.FULLSCREEN, fields
            )
                .build(activity!!)
            startActivityForResult(intent, 1)


//            startActivityForResult(
//                Intent(context, MapsActivity::class.java).putExtra(
//                    "filter",
//                    "addLoc"
//                ), 1
//            )
        }

        dateRange.setOnClickListener {

        }

        productsLayout.setOnClickListener {
            updateClick.nextFragments(2)
        }

        subscriptionLayout.setOnClickListener {

        }

        redemptionLayout.setOnClickListener {
            updateClick.nextFragments(4)
        }

        cancellationLayout.setOnClickListener {

        }

        sharingLayout.setOnClickListener {

        }

        viewsLayout.setOnClickListener {

        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        try {
            if (requestCode == 1) {
                when (resultCode) {
                    Activity.RESULT_OK -> {
                        val place = Autocomplete.getPlaceFromIntent(data!!)
                        val latLng = place.latLng
                        latitude1 = latLng!!.latitude
                        longitude1 = latLng.longitude
                        val geocoder = Geocoder(activity)
                        val addresses =
                            latLng.latitude.let {
                                geocoder.getFromLocation(
                                    it,
                                    latLng.longitude,
                                    1
                                )
                            }
                        val locationModel = LocationModel()
                        locationModel.address = addresses[0].getAddressLine(0)
                        try {
                            locationModel.city = addresses[0].locality
                            locationModel.state = addresses[0].adminArea
                            locationModel.zipcode = addresses[0].postalCode
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }

                        locationModel.latitude = latitude1.toString()
                        locationModel.longitude = longitude1.toString()

                        startActivityForResult(
                            Intent(
                                activity!!,
                                MapsActivity::class.java
                            ).putExtra("location", locationModel).putExtra("filter", "addLoc"), 2
                        )

                    }
                }
            }

            if (requestCode == 2) {
                try {
                    when (resultCode) {
                        Activity.RESULT_OK -> {
                            val locationModel =
                                data!!.getParcelableExtra<LocationModel>("locationModel")
                            location.text = locationModel.address
                            latitude = data.getStringExtra("latitude")
                            longitude = data.getStringExtra("longitude")
                            modelData.latitude = latitude
                            modelData.longitude = longitude
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }


    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        sortBy.text = "No Filter Selected"
        val mPrefs = activity!!.getSharedPreferences("data", AppCompatActivity.MODE_PRIVATE)
        val gson = Gson()
        val json = mPrefs.getString("filterData", "")
        val json1 = mPrefs.getString("filterDataMaps", "")

        if (mContext is DashboardActivity || mContext is SubCategoriesActivity) {
            if (json != "") {
                val model = gson.fromJson(json, ModelData::class.java)
                textMin1.text = model.distanceMin
                textMax1.text = model.distanceMax
                textMin2.text = model.priceMin
                textMax2.text = model.priceMax
                modelData = model


                if (model.latitude != "" && model.longitude != "") {

                    modelData.latitude = model.latitude
                    modelData.longitude = model.longitude

                    val geocoder = Geocoder(activity)
                    val addresses =
                        model.latitude.toDouble()
                            .let { geocoder.getFromLocation(it, model.longitude.toDouble(), 1) }
                    try {
                        if (null != addresses && addresses.isNotEmpty()) {
                            location.text = addresses[0].getAddressLine(0)

                        }

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }


            }
        }



        if (mContext is MapsSearchActivity) {
            if (json1 != "") {
                val model = gson.fromJson(json1, ModelData::class.java)
                textMin1.text = model.distanceMin
                textMax1.text = model.distanceMax
                textMin2.text = model.priceMin
                textMax2.text = model.priceMax
                modelData = model


                if (model.latitude != "" && model.longitude != "") {

                    modelData.latitude = model.latitude
                    modelData.longitude = model.longitude

                    val geocoder = Geocoder(activity)
                    val addresses =
                        model.latitude.toDouble()
                            .let { geocoder.getFromLocation(it, model.longitude.toDouble(), 1) }
                    try {
                        if (null != addresses && addresses.isNotEmpty()) {
                            location.text = addresses[0].getAddressLine(0)

                        }

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }


            }

        }
//        if (arguments != null) {
//            val model = arguments!!.getParcelable<ModelData>("model")
//
//
//
//
//        }


        brandNameValue = if (mContext is MapsSearchActivity) {
            MapsSearchActivity.businessNames
        } else if (mContext is DashboardActivity) {
            DashboardActivity.businesName
        } else if (mContext is SubCategoriesActivity) {
            SubCategoriesActivity.businessName
        } else {
            SearchActivity.businessName
        }

        if (brandNameValue != "") {
            brandName.text = brandNameValue
        } else {
            brandName.text = "All"
        }



        when (mContext) {
            is MapsSearchActivity -> {
                sortBy.text = MapsSearchActivity.sortName
            }
            is DashboardActivity -> {
                sortBy.text = DashboardActivity.sortName
            }
            is SubCategoriesActivity -> {
                sortBy.text = SubCategoriesActivity.sortName
            }

        }

        if (sortBy.text.toString() == "") {
            sortBy.text = "No Filter Selected"
        }

        when (mContext) {
            is MapsSearchActivity -> {
                val parentName = ArrayList<String>()
                for (items in MapsSearchActivity.checkedItems) {
                    parentName.add(items.parentName)
                }
                val newList = ArrayList<String>()
                for (item in parentName) {
                    if (!newList.contains(item)) {
                        newList.add(item)
                    }
                }
                if (MapsSearchActivity.checkedItems.isNotEmpty()) {
                    if (newList.size > 1) {
                        category.text =
                            MapsSearchActivity.checkedItems[0].parentName + " " + "and " +
                                    (newList.size - 1) + " more"
                    } else {
                        category.text =
                            MapsSearchActivity.checkedItems[0].parentName
                    }
                }

            }
            is DashboardActivity -> {
                val parentName = ArrayList<String>()
                for (items in DashboardActivity.checkedItems) {
                    parentName.add(items.parentName)
                }
                val newList = ArrayList<String>()
                for (item in parentName) {
                    if (!newList.contains(item)) {
                        newList.add(item)
                    }
                }
                if (DashboardActivity.checkedItems.isNotEmpty()) {
                    if (newList.size > 1) {
                        category.text =
                            DashboardActivity.checkedItems[0].parentName + " " + "and " + (newList.size - 1) + " more"
                    } else {
                        category.text = DashboardActivity.checkedItems[0].parentName
                    }
                }

            }
            is SubCategoriesActivity -> {
                val parentName = ArrayList<String>()
                for (items in SubCategoriesActivity.checkedItems) {
                    parentName.add(items.parentName)
                }
                val newList = ArrayList<String>()
                for (item in parentName) {
                    if (!newList.contains(item)) {
                        newList.add(item)
                    }
                }
                if (SubCategoriesActivity.checkedItems.isNotEmpty()) {
                    if (newList.size > 1) {
                        category.text =
                            SubCategoriesActivity.checkedItems[0].parentName + " " + "and " +
                                    (newList.size - 1) + " more"
                    } else {
                        category.text =
                            SubCategoriesActivity.checkedItems[0].parentName
                    }
                }

            }
        }

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            updateClick = context as FilterInterface
            backHeader = context as SetFilterHeaderBackground
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    private fun setRangeSeekbar1() {

        // set listener
        rangeSeekbar1.setOnRangeSeekbarChangeListener { minValue, maxValue ->
            rangeSeekbar1.isSaveEnabled = true
            minDistance = minValue.toString()
            maxDistance = maxValue.toString()
            modelData.distanceMin = minDistance
            modelData.distanceMax = maxDistance
            textMin1.text = minValue.toString()
            textMax1.text = maxValue.toString()
        }

    }

    private fun setRangeSeekbar2() {

        // set listener
        rangeSeekbar2.setOnRangeSeekbarChangeListener { minValue, maxValue ->
            minPrice = minValue.toString()
            maxPrice = maxValue.toString()
            modelData.priceMin = minPrice
            modelData.priceMax = maxPrice
            textMin2.text = minValue.toString()
            textMax2.text = maxValue.toString()
        }
    }

    interface SetFilterHeaderBackground {
        fun filterHeaderBackground(s: String)
    }


    fun updateBrandData(name: String) {
        brandNameValue = if (name.isNotEmpty()) {
            name
        } else {
            "All"
        }
    }


    fun updateSortData(sort: String) {
        sortNameValue = if (sort.isNotEmpty()) {
            sort
        } else {
            "All"
        }
    }


    fun getData(): ModelData {
        return modelData
    }


}