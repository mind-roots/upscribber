package com.upscribber.filters

interface ExpandListener {
    fun onExpand(position: Int)
    fun onSelectAll(position: Int, check: Boolean)
    fun getSelection(position: Int, check: Boolean)
}
