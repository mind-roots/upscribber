package com.upscribber.filters

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import android.graphics.Bitmap
import android.graphics.Canvas
import android.location.Address
import android.location.Geocoder
import android.os.Handler
import android.util.Log
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.upscribber.commonClasses.Constant
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.upscribber.R
import com.upscribber.upscribberSeller.subsriptionSeller.location.days.AddLocationActivity


class MapsActivity : AppCompatActivity(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener {


    private var addresses: MutableList<Address> = ArrayList()
    lateinit var shareToolbar: Toolbar
    lateinit var setLoc: TextView
    lateinit var location: TextView
    lateinit var searchView: TextView
    private lateinit var mMap: GoogleMap
    //private lateinit var searchView: SearchView
    private lateinit var relativeLayout: RelativeLayout
    private lateinit var lastLocation: Location
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    val locationModel = LocationModel()
    var latitude = 0.0
    var longitude = 0.0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        Places.initialize(applicationContext, resources.getString(R.string.google_maps_key))
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        initzz()
        setToolbar()
        sellerCondition()
        clickListeners()
        setData()
    }

    private fun setData() {
        if (intent.hasExtra("location")) {
            val modelLocation = intent.getParcelableExtra<LocationModel>("location")
            latitude = modelLocation.latitude.toDouble()
            longitude = modelLocation.longitude.toDouble()
            searchView.text = modelLocation.address
            val latitude1 = modelLocation.latitude.toDouble()
            val longitude1 = modelLocation.longitude.toDouble()
            val geocoder = Geocoder(this)
            addresses = latitude1.let { geocoder.getFromLocation(it, longitude1, 1) }
            Handler().postDelayed({
                placeMarkerOnMap(LatLng(latitude1, longitude1))
                mMap.animateCamera(
                    CameraUpdateFactory.newLatLngZoom(
                        LatLng(latitude1, longitude1),
                        12f
                    )
                )
            }, 200)

            try {
                if (addresses.isNotEmpty()) {
                    searchView.text = modelLocation.address
                    locationModel.address = modelLocation.address
                    searchView.text = locationModel.address
                    locationModel.city = modelLocation.city
                    locationModel.state = modelLocation.state
                    locationModel.zipcode = modelLocation.zipcode
                    locationModel.latitude = modelLocation.latitude
                    locationModel.street = modelLocation.street
                    locationModel.longitude = modelLocation.longitude

                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        } else if (intent.hasExtra("location1")) {
            val modelLocation = intent.getParcelableExtra<LocationModel>("location1")
            searchView.text = modelLocation.address
            latitude = modelLocation.latitude.toDouble()
            longitude = modelLocation.longitude.toDouble()
            val latitude1 = modelLocation.latitude.toDouble()
            val longitude1 = modelLocation.longitude.toDouble()
            val geocoder = Geocoder(this)
            addresses = latitude1.let { geocoder.getFromLocation(it, longitude1, 1) }
            Handler().postDelayed({
                placeMarkerOnMap(LatLng(latitude1, longitude1))
                mMap.animateCamera(
                    CameraUpdateFactory.newLatLngZoom(
                        LatLng(latitude1, longitude1),
                        12f
                    )
                )
            }, 200)

            try {
                if (addresses.isNotEmpty()) {
                    searchView.text = modelLocation.address
                    locationModel.address = modelLocation.address
                    searchView.text = locationModel.address
                    locationModel.city = modelLocation.city
                    locationModel.state = modelLocation.state
                    locationModel.zipcode = modelLocation.zipcode
                    locationModel.street = modelLocation.street
                    locationModel.latitude = modelLocation.latitude
                    locationModel.longitude = modelLocation.longitude

                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

    }

    private fun clickListeners() {
        setLoc.setOnClickListener {
            if (intent.hasExtra("filter")) {
                val intent = Intent(this, AddLocationActivity::class.java)
                intent.putExtra("locationModel", locationModel)
                intent.putExtra("latitude", latitude.toString())
                intent.putExtra("longitude", longitude.toString())
                setResult(Activity.RESULT_OK, intent)
                finish()
            } else if (intent.hasExtra("location1")) {
                startActivity(
                    Intent(
                        this,
                        AddLocationActivity::class.java
                    ).putExtra("locationModel", locationModel)
                )
//                intent.putExtra("locationModel", locationModel)
//                intent.putExtra("latitude", latitude.toString())
//                intent.putExtra("longitude", longitude.toString())
//                setResult(Activity.RESULT_OK, intent)
                finish()
            } else {
                val intent = Intent(this, AddLocationActivity::class.java)
                intent.putExtra("locationModel", locationModel)
                intent.putExtra("latitude", latitude.toString())
                intent.putExtra("longitude", longitude.toString())
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                setResult(Activity.RESULT_OK, intent)
                finish()
            }

        }

        relativeLayout.setOnClickListener {
            Places.createClient(this)
            val fields =
                listOf(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS)
            // Start the autocomplete intent.
            val intent = Autocomplete.IntentBuilder(
                AutocompleteActivityMode.FULLSCREEN, fields
            )
                .build(this)
            startActivityForResult(intent, 2)
        }

        searchView.setOnClickListener {
            Places.createClient(this)
            val fields =
                listOf(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG, Place.Field.ADDRESS)
            // Start the autocomplete intent.
            val intent = Autocomplete.IntentBuilder(
                AutocompleteActivityMode.FULLSCREEN, fields
            )
                .build(this)
            startActivityForResult(intent, 2)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 2) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    val place = Autocomplete.getPlaceFromIntent(data!!)
                    val latLng = place.latLng
                    latitude = latLng!!.latitude
                    longitude = latLng.longitude
                    mMap.clear()
                    placeMarkerOnMap(place.latLng!!)
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(place.latLng!!, 12f))
                    locationModel.address = place.address.toString()
                    Log.e("Address", place.address)
                    Log.e("Address", place.addressComponents.toString())
                    val geocoder = Geocoder(this)
                    addresses =
                        latLng.latitude.let { geocoder.getFromLocation(it, latLng.longitude, 1) }
                    try {
                        if (addresses.isNotEmpty()) {
                            locationModel.address = addresses[0].getAddressLine(0)
                            locationModel.street = addresses[0].thoroughfare
                            searchView.text = locationModel.address
                            locationModel.city = addresses[0].locality
                            locationModel.state = addresses[0].adminArea
                            locationModel.zipcode = addresses[0].postalCode
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }
        }
    }

    private fun sellerCondition() {
        val isSeller = Constant.getSharedPrefs(this).getBoolean(Constant.IS_SELLER, false)

        if (isSeller) {
            setLoc.setBackgroundResource(R.drawable.bg_seller_button_blue)
        } else {
            setLoc.setBackgroundResource(R.drawable.invite_button_background)
        }

        if (intent.hasExtra("filter")) {
            location.text = "Change Location"
        } else {
            location.text = "Add Location"
        }

    }

    private fun setToolbar() {
        setSupportActionBar(shareToolbar)
        title = ""
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)


    }

    private fun initzz() {
        shareToolbar = findViewById(R.id.shareToolbar)
        setLoc = findViewById(R.id.setLoc)
        location = findViewById(R.id.location)
        relativeLayout = findViewById(R.id.relativeLayout)
        searchView = findViewById(R.id.searchView)

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap.uiSettings.isCompassEnabled = true
        setUpMap()
        mMap.setOnMapClickListener { latlng ->
            latitude = latlng.latitude
            longitude = latlng.longitude
            mMap.clear()
            placeMarkerOnMap(latlng)
        }
    }

    override fun onMarkerClick(p0: Marker?): Boolean {
        mMap.uiSettings.isZoomControlsEnabled = true
        mMap.setOnMarkerClickListener(this)
        return true
    }

    companion object {
        const val LOCATION_PERMISSION_REQUEST_CODE = 1
    }

    private fun setUpMap() {
        if (ActivityCompat.checkSelfPermission(
                this,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                LOCATION_PERMISSION_REQUEST_CODE
            )
            return
        }

//                fusedLocationClient.lastLocation.addOnSuccessListener(this) { location ->
//            // Got last known location. In some rare situations this can be null.
//            if (location != null) {
//                lastLocation = location
//                latitude = lastLocation.latitude
//                longitude = lastLocation.longitude
//                val currentLatLng = LatLng(location.latitude, location.longitude)
//                placeMarkerOnMap(currentLatLng)
//                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 12f))
//                val geocoder = Geocoder(this)
//                val addresses =
//                    location.latitude.let { geocoder.getFromLocation(it, location.longitude, 1) }
//                if (null != addresses && addresses.isNotEmpty()) {
//                    locationModel.address =
//                        addresses[0].locality + " " + addresses[0].adminArea + " " + addresses[0].postalCode
//                    locationModel.city = addresses[0].subAdminArea
//                    locationModel.state = addresses[0].adminArea
//                    locationModel.zipcode = addresses[0].postalCode
//                }
//
//            }
//        }
    }

    //    custom marker
    private fun placeMarkerOnMap(location: LatLng) {
        mMap.addMarker(
            MarkerOptions()
                .position(location)
                .icon(bitmapDescriptorFromVector(this, R.drawable.ic_location_blue_icon))
        )

    }

    //custom marker using bitmap
    private fun bitmapDescriptorFromVector(context: Context, vectorResId: Int): BitmapDescriptor {
        val vectorDrawable = ContextCompat.getDrawable(context, vectorResId)
        vectorDrawable!!.setBounds(
            0,
            0,
            vectorDrawable.intrinsicWidth,
            vectorDrawable.intrinsicHeight
        )
        val bitmap =
            Bitmap.createBitmap(
                vectorDrawable.intrinsicWidth,
                vectorDrawable.intrinsicHeight,
                Bitmap.Config.ARGB_8888
            )
        val canvas = Canvas(bitmap)
        vectorDrawable.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }


}