package com.upscribber.filters

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.upscribber.commonClasses.ModelStatusMsg

class ViewModelFilters(application: Application) : AndroidViewModel(application) {
    var filtersRepository: FiltersRepository = FiltersRepository(application)

    fun getAllBusiness(search: String) {
        filtersRepository.getAllBusiness(search)
    }

    fun getStatus(): LiveData<ModelStatusMsg> {
        return filtersRepository.getmDataFailure()
    }

    fun getBrandData(): MutableLiveData<ArrayList<BrandModel>> {
        return filtersRepository.getmDataBrand()
    }
}