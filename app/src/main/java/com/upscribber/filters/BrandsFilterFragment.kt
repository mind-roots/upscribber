package com.upscribber.filters

import android.app.Activity
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import kotlinx.android.synthetic.main.fragment_brand_filter.*


class BrandsFilterFragment : Fragment() {

    private lateinit var mbrandRrecycler: RecyclerView
    private lateinit var brand_search: androidx.appcompat.widget.SearchView
    private lateinit var viewModel: ViewModelFilters
    private lateinit var brandAdapter: BrandAdapter
    private lateinit var noBrand: TextView
    private lateinit var linearLayout: LinearLayout


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_brand_filter, container, false)
        viewModel = ViewModelProviders.of(this)[ViewModelFilters::class.java]
        mbrandRrecycler = view.findViewById(R.id.brand_recycler)
        brand_search = view.findViewById(R.id.brand_search)
        noBrand = view.findViewById(R.id.noBrand)
        clickListener()


        return view
    }

    private fun clickListener() {
        brand_search.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextChange(s: String): Boolean {
                // make a server call
                progressBar.visibility = View.VISIBLE
                Handler().postDelayed({
                    viewModel.getAllBusiness(s)
                }, 500)



                return true
            }

            override fun onQueryTextSubmit(s: String): Boolean {
                Constant.hideKeyboard(context!!, brand_search)
                return true
            }


        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        apiImplimentation()
        observerInit()
    }


    private fun apiImplimentation() {
        viewModel.getAllBusiness("")
        progressBar.visibility = View.VISIBLE
    }

    private fun observerInit() {
        viewModel.getBrandData().observe(this, Observer {
            progressBar.visibility = View.GONE
            if (it.size > 0) {
                mbrandRrecycler.visibility = View.VISIBLE
                noBrand.visibility = View.GONE
                it.sortBy {it.business_name.toLowerCase()}
                setAdapter()
                brandAdapter.update(it)
            } else {
                mbrandRrecycler.visibility = View.GONE
                noBrand.visibility = View.VISIBLE
            }


        })


    }

    private fun setAdapter() {
        mbrandRrecycler.layoutManager = LinearLayoutManager(activity)
        brandAdapter = BrandAdapter(this.activity!!, this)
        mbrandRrecycler.adapter = brandAdapter
    }


    //    private fun getList(): ArrayList<BrandModel> {
//
//        val arrayList: java.util.ArrayList<BrandModel> = arrayListOf()
//
//        val brandModel1 = BrandModel()
//        brandModel1.business_name = "Any Brand(None)"
//        arrayList.add(brandModel1)
//
//        val brandModel2 = BrandModel()
//        brandModel2.business_name = "Tram Hair Styling"
//        arrayList.add(brandModel2)
//
//        val brandModel3 = BrandModel()
//        brandModel3.business_name = "DC Hair Design"
//        arrayList.add(brandModel3)
//
//        val brandModel4 = BrandModel()
//        brandModel4.business_name = "Head to Toe"
//        arrayList.add(brandModel4)
//
//        val brandModel5 = BrandModel()
//        brandModel5.business_name = "Tangled Beauty Parlour"
//        arrayList.add(brandModel5)
//
//        val brandModel6 = BrandModel()
//        brandModel6.business_name = "Go Go Hair Salon"
//        arrayList.add(brandModel6)
//
//        val brandModel7 = BrandModel()
//        brandModel7.business_name = "Cost Cutters"
//        arrayList.add(brandModel7)
//
//        val brandModel8 = BrandModel()
//        brandModel8.business_name = "Curl Power Salon"
//        arrayList.add(brandModel8)
//
//
//        return arrayList
//    }
}