package com.upscribber.filters

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by amrit on 6/3/19.
 */
class CategoryModel() : Parcelable {

    var categoryName: String = ""
    var checkoption: Boolean = false
    var image: String = ""
    var campaignId = ""
    var check = false
    var categoryId : String = ""
    var arrayList: ArrayList<ModelSubCategory> = ArrayList()

    constructor(parcel: Parcel) : this() {
        categoryName = parcel.readString()
        checkoption = parcel.readByte() != 0.toByte()
        image = parcel.readString()
        campaignId = parcel.readString()
        check = parcel.readByte() != 0.toByte()
        categoryId = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(categoryName)
        parcel.writeByte(if (checkoption) 1 else 0)
        parcel.writeString(image)
        parcel.writeString(campaignId)
        parcel.writeByte(if (check) 1 else 0)
        parcel.writeString(categoryId)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CategoryModel> {
        override fun createFromParcel(parcel: Parcel): CategoryModel {
            return CategoryModel(parcel)
        }

        override fun newArray(size: Int): Array<CategoryModel?> {
            return arrayOfNulls(size)
        }
    }


}