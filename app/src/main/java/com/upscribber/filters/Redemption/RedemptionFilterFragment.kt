package com.upscribber.filters.Redemption

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.upscribber.R
import com.upscribber.databinding.FragmentRedemptionFilterBinding

class RedemptionFilterFragment : Fragment(), AdapterRedem.UpdateItem {


    lateinit var mBinding: FragmentRedemptionFilterBinding
    lateinit var viewModel: ViewModelRedemption
    lateinit var adapter: AdapterRedem
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_redemption_filter, container, false)
        viewModel = ViewModelProviders.of(this)[ViewModelRedemption::class.java]
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.getValues().observe(this, Observer {
            mBinding.redemRecycler.layoutManager = LinearLayoutManager(activity)
            adapter = AdapterRedem(this.activity!!,this)
            mBinding.redemRecycler.adapter = adapter
            adapter.update(it)
        })
    }

    override fun updateCheckedItem(position: Int) {

        viewModel.updateItem(position).observe(this, Observer {
            adapter.update(it)
        })
    }

}