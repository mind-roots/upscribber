package com.upscribber.filters.Redemption


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.filters.ModelRedem
import com.upscribber.R
import com.upscribber.databinding.RedemptionItemFilterBinding
import kotlinx.android.synthetic.main.redemption_item_filter.view.*

class AdapterRedem(
    var context: Context,
    var redemptionFilterFragment: RedemptionFilterFragment
) : RecyclerView.Adapter<AdapterRedem.ViewHolder>() {

    private var itemss: List<ModelRedem> = ArrayList()
    lateinit var mBinding: RedemptionItemFilterBinding
    var update = redemptionFilterFragment as UpdateItem

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.redemption_item_filter, parent, false)
        return ViewHolder(mBinding.root)
    }

    override fun getItemCount(): Int {
        return itemss.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = itemss[position]
        mBinding.modelR = model
        if (model.status) {
            holder.itemView.check.visibility = View.VISIBLE
        } else {
            holder.itemView.check.visibility = View.GONE
        }
        holder.itemView.itemLayout.setOnClickListener {
            update.updateCheckedItem(position)
        }

    }

    fun update(items: List<ModelRedem>) {
        this.itemss = items
        notifyDataSetChanged()
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)
    interface UpdateItem {
        fun updateCheckedItem(position: Int)
    }
}