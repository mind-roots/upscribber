package com.upscribber.filters.Redemption

import android.app.Application
import androidx.lifecycle.MutableLiveData
import com.upscribber.filters.ModelRedem

class RepoRedemption(application: Application) {
    var mData = MutableLiveData<ArrayList<ModelRedem>>()
    val array = ArrayList<ModelRedem>()
    fun getValues(): MutableLiveData<ArrayList<ModelRedem>> {

        var model = ModelRedem()
        model.name = "Used"
        model.status = false
        array.add(model)
        model = ModelRedem()
        model.name = "Unused"
        model.status = false
        array.add(model)
        mData.value = array
        return mData

    }

    fun updateItem(position: Int): MutableLiveData<ArrayList<ModelRedem>> {

        for (i in 0 until array.size){
            array[i].status=false
        }
        array[position].status=true
        mData.value = array
        return mData
    }
}