package com.upscribber.filters.Redemption

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.upscribber.filters.ModelRedem

class ViewModelRedemption(application: Application):AndroidViewModel(application) {
    var refVar=RepoRedemption(application)

    fun getValues():MutableLiveData<ArrayList<ModelRedem>>{
        return refVar.getValues()
    }

    fun updateItem(position: Int): MutableLiveData<ArrayList<ModelRedem>> {

        return refVar.updateItem(position)
    }
}