package com.upscribber.filters

import android.os.Parcel
import android.os.Parcelable

class ModelData() : Parcelable {


    var distanceMin = "0"
    var distanceMax = "1000"
    var priceMin = "0"
    var priceMax = "2000"
    var latitude = ""
    var longitude = ""

    constructor(parcel: Parcel) : this() {
        distanceMin = parcel.readString()
        distanceMax = parcel.readString()
        priceMin = parcel.readString()
        priceMax = parcel.readString()
        latitude = parcel.readString()
        longitude = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(distanceMin)
        parcel.writeString(distanceMax)
        parcel.writeString(priceMin)
        parcel.writeString(priceMax)
        parcel.writeString(latitude)
        parcel.writeString(longitude)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ModelData> {
        override fun createFromParcel(parcel: Parcel): ModelData {
            return ModelData(parcel)
        }

        override fun newArray(size: Int): Array<ModelData?> {
            return arrayOfNulls(size)
        }
    }
}