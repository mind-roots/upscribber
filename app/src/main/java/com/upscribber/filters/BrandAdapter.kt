package com.upscribber.filters

import android.view.LayoutInflater
import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.R
import kotlinx.android.synthetic.main.brand_listlayout.view.*


class BrandAdapter(var mContext: Context, context: BrandsFilterFragment) :
    RecyclerView.Adapter<BrandAdapter.MyViewHolder>() {

    private var list = ArrayList<BrandModel>()
    var businessSelected = mContext as Selection

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BrandAdapter.MyViewHolder {

        val v = LayoutInflater.from(mContext)
            .inflate(R.layout.brand_listlayout, parent, false)

        return BrandAdapter.MyViewHolder(v)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: BrandAdapter.MyViewHolder, position: Int) {

        val model = list[position]
        if(model.business_name.isNotEmpty()){
            holder.itemView.brand_list.text = model.business_name
        }
        holder.itemView.brand_list.setOnClickListener {
            com.upscribber.commonClasses.Constant.hideKeyboard(mContext, holder.itemView.brand_list)
            businessSelected.getSelectedBrand(model.id, position,model.business_name)
        }
    }


    fun update(items: ArrayList<BrandModel>) {
        list = items
        notifyDataSetChanged()

    }


    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    interface Selection {

        fun getSelectedBrand(id: String, position: Int, businessName: String)
    }

}