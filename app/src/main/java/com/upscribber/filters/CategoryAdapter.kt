package com.upscribber.filters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.upscribber.R
import kotlinx.android.synthetic.main.category_listlayout.view.*

/**
 * Created by amrit on 6/3/19.
 */
class CategoryAdapter(
    private val mContext: Context,

    val listen: CategoryFilterFragment
) :
    RecyclerView.Adapter<CategoryAdapter.MyViewHolder>() {

    var listener = listen as ExpandListener
    private var list: ArrayList<CategoryModel> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val v = LayoutInflater.from(mContext)
            .inflate(R.layout.category_listlayout, parent, false)

        return MyViewHolder(v)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = list[position]
        holder.itemView.category_list.text = model.categoryName
        holder.itemView.selectAll.isChecked = model.check

        Glide.with(mContext).load(model.image).placeholder(R.mipmap.circular_placeholder)
            .into(holder.itemView.imageCategory)

        //holder.itemView.imageCategory.setImageResource(model.categoryImage)
        holder.itemView.recyclerView_subcategory.layoutManager = LinearLayoutManager(mContext)
        holder.itemView.recyclerView_subcategory.adapter =
            SubCategoryAdapter(mContext, model.arrayList, listen)
        holder.itemView.category_list.setOnClickListener {
            listener.onExpand(position)
        }


        holder.itemView.category_list.setOnClickListener {
            listener.onExpand(position)
            list[position].check = !model.check
            listener.onSelectAll(position, model.check)
            notifyDataSetChanged()

        }

        holder.itemView.imageCategory.setOnClickListener {
            listener.onExpand(position)
            list[position].check = !model.check
            listener.onSelectAll(position, model.check)
            notifyDataSetChanged()

        }


        holder.itemView.selectAll.setOnClickListener {

        }



        if (model.check) {
            holder.itemView.selectAll.visibility = View.GONE
            holder.itemView.recyclerView_subcategory.visibility = View.GONE
            holder.itemView.imageView17.visibility = View.VISIBLE
        } else {
            holder.itemView.selectAll.visibility = View.GONE
            holder.itemView.recyclerView_subcategory.visibility = View.GONE
            holder.itemView.imageView17.visibility = View.INVISIBLE

        }

    }

    fun update(items: ArrayList<CategoryModel>) {
        list = items
        notifyDataSetChanged()
    }


    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)


}