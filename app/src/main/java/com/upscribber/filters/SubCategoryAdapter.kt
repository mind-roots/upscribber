package com.upscribber.filters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.commonClasses.Constant
import com.upscribber.R
import kotlinx.android.synthetic.main.layout_recycler.view.*

class SubCategoryAdapter(
    private val mContext: Context,
    private val list: ArrayList<ModelSubCategory>,
    listen: Fragment
) :
    RecyclerView.Adapter<SubCategoryAdapter.MyViewHolder>() {

    var listener = listen as ExpandListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val v = LayoutInflater.from(mContext)
            .inflate(R.layout.layout_recycler, parent, false)
        return MyViewHolder(v)

    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = list[position]
        holder.itemView.radio_subcategory.text = model.name
        holder.itemView.radio_subcategory.isChecked = model.check


        holder.itemView.radio_subcategory.setOnClickListener {
            list[position].check = !model.check
            listener.getSelection(position, model.check)
            notifyDataSetChanged()
        }

        val isSeller = Constant.getSharedPrefs(mContext).getBoolean(Constant.IS_SELLER, false)

        if (isSeller) {
            holder.itemView.radio_subcategory.setButtonDrawable(R.drawable.custom_checkbox_purple)

        } else {
            holder.itemView.radio_subcategory.setButtonDrawable(R.drawable.custom_checkbox)

        }
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}