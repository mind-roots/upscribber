package com.upscribber.filters

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.R
import com.upscribber.categories.subCategories.SubCategoriesActivity
import com.upscribber.dashboard.DashboardActivity
import com.upscribber.dashboard.SearchActivity
import com.upscribber.home.MapsSearchActivity
import com.upscribber.search.SearchFragment
import com.upscribber.upscribberSeller.store.storeMain.StoreViewModel
import kotlinx.android.synthetic.main.fragment_category_filter.*
import kotlin.collections.ArrayList

class CategoryFilterFragment(val mContext: Context) : Fragment(), ExpandListener {

    private lateinit var mCategoryRecycler: RecyclerView
    private lateinit var categoriesList: ArrayList<CategoryModel>
    private lateinit var categoryAdapter: CategoryAdapter
    private lateinit var mViewModel: StoreViewModel
    var positionParent = -1
    var childSelection = 0
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_category_filter, container, false)
        mCategoryRecycler = view.findViewById(R.id.category_recycler)
        mViewModel = ViewModelProviders.of(this)[StoreViewModel::class.java]
        setAdapter()

        return view
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        apiImplimentation()
        observerInit()

    }

    private fun apiImplimentation() {
        mViewModel.productCategory()
        progressBar.visibility = View.VISIBLE
    }

    private fun observerInit() {
        mViewModel.getProductCategoryAll().observe(this, Observer { it ->
            if (it.size > 0) {
                progressBar.visibility = View.GONE
                it.sortBy { it.categoryName }
                categoriesList = it
                if (mContext is MapsSearchActivity) {
                    if (MapsSearchActivity.checkedItems.isNotEmpty()) {
                        for (items in categoriesList) {
                            for (item in MapsSearchActivity.checkedItems) {
                                if (items.categoryName == item.parentName) {
                                    items.check = true
                                    for (i in items.arrayList) {
                                        i.check = true
                                    }
                                }
                            }
                        }
                    }
                } else if (mContext is DashboardActivity) {
                    if (DashboardActivity.checkedItems.isNotEmpty()) {
                        for (items in categoriesList) {
                            for (item in DashboardActivity.checkedItems) {
                                if (items.categoryName == item.parentName) {
                                    items.check = true
                                    for (i in items.arrayList) {
                                        i.check = true
                                    }
                                }
                            }
                        }
                    }
                } else if (mContext is SubCategoriesActivity) {
                    if (SubCategoriesActivity.checkedItems.isNotEmpty()) {
                        for (items in categoriesList) {
                            for (item in SubCategoriesActivity.checkedItems) {
                                if (items.categoryName == item.parentName) {
                                    items.check = true
                                    for (i in items.arrayList) {
                                        i.check = true
                                    }
                                }
                            }
                        }
                    }
                }
                categoryAdapter.update(categoriesList)
            }
        })
    }

    private fun setAdapter() {
        mCategoryRecycler.layoutManager = LinearLayoutManager(activity)
        mCategoryRecycler.itemAnimator = DefaultItemAnimator()
        mCategoryRecycler.isNestedScrollingEnabled = false
        categoryAdapter = CategoryAdapter(this.activity!!, this)
        mCategoryRecycler.adapter = categoryAdapter
    }


    override fun onExpand(position: Int) {
        positionParent = position

    }

    override fun onSelectAll(position: Int, check: Boolean) {
        for (child in categoriesList[position].arrayList) {
            child.check = check
        }
        childSelection = if (check) {
            categoriesList[position].arrayList.size
        } else {
            0
        }
    }

    override fun getSelection(position: Int, check: Boolean) {
        val model = categoriesList[positionParent]
        if (check) {
            childSelection++
            model.check = childSelection >= categoriesList[positionParent].arrayList.size
            categoryAdapter.notifyDataSetChanged()
        } else {
            if (childSelection > 0) {
                childSelection--
            }
            model.check = false
            categoryAdapter.notifyDataSetChanged()
        }
    }

    fun getCheckedCategories(): ArrayList<ModelSubCategory> {
        val categoriesList2: ArrayList<ModelSubCategory> = ArrayList()
        for (i in 0 until categoriesList.size) {
            for (j in 0 until categoriesList[i].arrayList.size)
                if (categoriesList[i].arrayList[j].check) {
                    categoriesList2.add(categoriesList[i].arrayList[j])
                }
        }
        return categoriesList2
    }
}