package com.upscribber.filters

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by amrit on 7/3/19.
 */
class ModelSubCategory() : Parcelable {
    var parent_id: String = ""
    var parentName: String = ""
    //    lateinit var subcategoryName: String
    var subcategoryId: Int = 0
    var parentId: Int = 0
    var radioButton: String = ""
    var check: Boolean = false
    var name: String = ""
    var id: String = ""

    constructor(parcel: Parcel) : this() {
        parent_id = parcel.readString()
        parentName = parcel.readString()
        subcategoryId = parcel.readInt()
        parentId = parcel.readInt()
        radioButton = parcel.readString()
        check = parcel.readByte() != 0.toByte()
        name = parcel.readString()
        id = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(parent_id)
        parcel.writeString(parentName)
        parcel.writeInt(subcategoryId)
        parcel.writeInt(parentId)
        parcel.writeString(radioButton)
        parcel.writeByte(if (check) 1 else 0)
        parcel.writeString(name)
        parcel.writeString(id)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ModelSubCategory> {
        override fun createFromParcel(parcel: Parcel): ModelSubCategory {
            return ModelSubCategory(parcel)
        }

        override fun newArray(size: Int): Array<ModelSubCategory?> {
            return arrayOfNulls(size)
        }
    }
}