package com.upscribber.filters

/**
 * Created by amrit on 6/3/19.
 */
interface FilterInterface {

    fun nextFragments(i: Int)
}