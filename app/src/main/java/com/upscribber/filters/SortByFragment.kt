package com.upscribber.filters

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.R

class SortByFragment : Fragment() {

    private lateinit var mSortRecycler: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_sortby, container, false)
        mSortRecycler = view.findViewById(R.id.sort_by_recycler)
        setAdapter()
        return view
    }


    private fun setAdapter() {
        mSortRecycler.layoutManager = LinearLayoutManager(activity)
        mSortRecycler.adapter = SortByAdapter(this.activity!!, getList())

    }

    private fun getList(): ArrayList<SortByModel> {

        val arrayList: ArrayList<SortByModel> = ArrayList()

        val sortByModel1 = SortByModel()
        sortByModel1.headings = "Best Match"
        sortByModel1.checkBox1 = false
        arrayList.add(sortByModel1)

        val sortByModel2 = SortByModel()
        sortByModel2.headings = "Price Low to High"
        sortByModel1.checkBox1 = false
        arrayList.add(sortByModel2)

        val sortByModel3 = SortByModel()
        sortByModel3.headings = "Price High to Low"
        sortByModel1.checkBox1 = false
        arrayList.add(sortByModel3)

        val sortByModel4 = SortByModel()
        sortByModel4.headings = "Distance"
        sortByModel1.checkBox1 = false
        arrayList.add(sortByModel4)

        val sortByModel5 = SortByModel()
        sortByModel5.headings = "Rating"
        sortByModel1.checkBox1 = false
        arrayList.add(sortByModel5)

        return arrayList
    }



}

