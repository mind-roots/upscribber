package com.upscribber.filters

import android.os.Parcel
import android.os.Parcelable

class LocationModel() : Parcelable {

    var billingLong: String = ""
    var billingLat: String = ""
    var billingAddress: String = ""
    var street: String = ""
    var address: String = ""
    var city: String = ""
    var state: String = ""
    var zipcode: String = ""
    var latitude: String = ""
    var longitude: String = ""
    var billingSuite : String = ""
    var billingStreet: String = ""
    var billingState: String = ""
    var billingCity: String = ""
    var billingZip: String = ""

    constructor(parcel: Parcel) : this() {
        billingLong = parcel.readString()
        billingLat = parcel.readString()
        billingAddress = parcel.readString()
        street = parcel.readString()
        address = parcel.readString()
        city = parcel.readString()
        state = parcel.readString()
        zipcode = parcel.readString()
        latitude = parcel.readString()
        longitude = parcel.readString()
        billingSuite = parcel.readString()
        billingStreet = parcel.readString()
        billingState = parcel.readString()
        billingCity = parcel.readString()
        billingZip = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(billingLong)
        parcel.writeString(billingLat)
        parcel.writeString(billingAddress)
        parcel.writeString(street)
        parcel.writeString(address)
        parcel.writeString(city)
        parcel.writeString(state)
        parcel.writeString(zipcode)
        parcel.writeString(latitude)
        parcel.writeString(longitude)
        parcel.writeString(billingSuite)
        parcel.writeString(billingStreet)
        parcel.writeString(billingState)
        parcel.writeString(billingCity)
        parcel.writeString(billingZip)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<LocationModel> {
        override fun createFromParcel(parcel: Parcel): LocationModel {
            return LocationModel(parcel)
        }

        override fun newArray(size: Int): Array<LocationModel?> {
            return arrayOfNulls(size)
        }
    }


}