package com.upscribber.filters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.R
import com.upscribber.categories.subCategories.SubCategoriesActivity
import com.upscribber.dashboard.DashboardActivity
import com.upscribber.home.MapsSearchActivity
import kotlinx.android.synthetic.main.sortby_listlayout.view.*


class SortByAdapter(private val mContext: Context, private val list: ArrayList<SortByModel>) :
    RecyclerView.Adapter<SortByAdapter.MyViewHolder>() {

    var selectedposition = -1
    var sortSelected = mContext as Selection
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val v = LayoutInflater.from(mContext)
            .inflate(R.layout.sortby_listlayout, parent, false)
        return MyViewHolder(v)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        val model = list[position]
        holder.itemView.checkBox.text = model.headings
        if (DashboardActivity.sortName.toLowerCase() == model.headings.toLowerCase()) {
            selectedposition = position
        }
        if (SubCategoriesActivity.sortName.toLowerCase() == model.headings.toLowerCase()) {
            selectedposition = position
        }

        if (MapsSearchActivity.sortName.toLowerCase() == model.headings.toLowerCase()) {
            selectedposition = position
        }

        // holder.itemView.checkBox.isChecked = model.checkBox1

        holder.itemView.checkBox.isChecked = position == selectedposition

        holder.itemView.checkBox.setOnClickListener {
            selectedposition = position
            list[position].checkBox1 = !model.checkBox1
            sortSelected.getSelectedSort(model.headings, position)
            notifyDataSetChanged()
        }

        holder.itemView.setOnClickListener {
            selectedposition = position
            list[position].checkBox1 = !model.checkBox1
            sortSelected.getSelectedSort(model.headings, position)
            notifyDataSetChanged()
        }


    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    interface Selection {

        fun getSelectedSort(name: String, position: Int)
    }

}