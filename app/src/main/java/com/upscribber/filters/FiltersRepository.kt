package com.upscribber.filters

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.upscribber.commonClasses.Constant
import com.upscribber.commonClasses.ModelStatusMsg
import com.upscribber.commonClasses.WebServicesCustomers
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class FiltersRepository(var application: Application) {


    val mDataBrand = MutableLiveData<ArrayList<BrandModel>>()
    val mDataFailure = MutableLiveData<ModelStatusMsg>()

    fun getAllBusiness(search: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()


        val auth = Constant.getPrefs(application).getString(Constant.auth_code, "")

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.getAllBusiness(auth, search, "1")
        call.enqueue(object : Callback<ResponseBody> {


            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {

                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")

                        if (status == "true") {
                            val data = json.optJSONObject("data")
                            val business = data.optJSONArray("business")
                            val arrayList = ArrayList<BrandModel>()
                            for (i in 0 until business.length()) {
                                val business_info = business.getJSONObject(i)
                                val brandModel = BrandModel()
                                brandModel.business_name = business_info.optString("business_name")
                                brandModel.id = business_info.optString("id")
                                if (brandModel.business_name != ""){
                                    arrayList.add(brandModel)
                                }
                            }

                            mDataBrand.value = arrayList
                        }

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }

            }


            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus

            }


        })
    }

    fun getmDataBrand(): MutableLiveData<ArrayList<BrandModel>> {
        return mDataBrand
    }

    fun getmDataFailure(): LiveData<ModelStatusMsg> {
        return mDataFailure
    }

}