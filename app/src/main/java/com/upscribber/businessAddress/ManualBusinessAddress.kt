package com.upscribber.businessAddress

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import androidx.appcompat.widget.Toolbar
import com.google.android.gms.common.api.Status
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener
import com.upscribber.R
import com.google.android.libraries.places.api.Places.createClient
import com.google.android.libraries.places.api.net.PlacesClient
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.google.android.libraries.places.widget.Autocomplete
import android.content.Intent
import com.google.android.libraries.places.api.model.Place
import java.util.*
import com.google.android.libraries.places.widget.AutocompleteActivity
import android.app.Activity
import android.widget.EditText
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.upscribber.becomeseller.seller.EnterManuallyFragment
import com.upscribber.becomeseller.seller.ModelAddress
import com.upscribber.filters.LocationModel
import com.upscribber.filters.MapsActivity
import kotlinx.android.synthetic.main.activity_add_location.*


class ManualBusinessAddress : AppCompatActivity() {

    private lateinit var toolbar: Toolbar
    private lateinit var fragmentManager: FragmentManager
    private lateinit var fragmentTransaction: FragmentTransaction

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_manual_business_address)
        initiz()
        setToolbar()
        clickListener()


        when {
            intent.hasExtra("model") -> {
                val preFilledData = intent.getParcelableExtra<ModelAddress>("model")
                val bundle = Bundle()
                bundle.putParcelable("model", preFilledData)
                val fragment = EnterManuallyFragment()
                fragment.arguments = bundle
                fragmentManager = supportFragmentManager
                fragmentTransaction = fragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment, fragment)
                fragmentTransaction.commit()
            }
            intent.hasExtra("modelStep4") -> {
                val preFilledData = intent.getParcelableExtra<ModelAddress>("modelStep4")
                val bundle = Bundle()
                bundle.putParcelable("model", preFilledData)
                val fragment = EnterManuallyFragment()
                fragment.arguments = bundle
                fragmentManager = supportFragmentManager
                fragmentTransaction = fragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment, fragment)
                fragmentTransaction.commit()
            }
            intent.hasExtra("checkout") -> {
                val preFilledData = intent.getParcelableExtra<ModelAddress>("checkout")
                val bundle = Bundle()
                bundle.putParcelable("checkout", preFilledData)
                val fragment = EnterManuallyFragment()
                fragment.arguments = bundle
                fragmentManager = supportFragmentManager
                fragmentTransaction = fragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment, fragment)
                fragmentTransaction.commit()
            }intent.hasExtra("checkoutBilling") -> {
                val preFilledData = intent.getParcelableExtra<ModelAddress>("checkoutBilling")
                val bundle = Bundle()
                bundle.putParcelable("checkoutBilling", preFilledData)
                val fragment = EnterManuallyFragment()
                fragment.arguments = bundle
                fragmentManager = supportFragmentManager
                fragmentTransaction = fragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment, fragment)
                fragmentTransaction.commit()
            }

            intent.hasExtra("checkoutMerchant") -> {
                val preFilledData = intent.getParcelableExtra<ModelAddress>("checkoutMerchant")
                val bundle = Bundle()
                bundle.putParcelable("model", preFilledData)
                val fragment = EnterManuallyFragment()
                fragment.arguments = bundle
                fragmentManager = supportFragmentManager
                fragmentTransaction = fragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment, fragment)
                fragmentTransaction.commit()
            }intent.hasExtra("checkoutMerchantBilling") -> {
                val preFilledData = intent.getParcelableExtra<ModelAddress>("checkoutMerchantBilling")
                val bundle = Bundle()
                bundle.putParcelable("checkoutMerchantBilling", preFilledData)
                val fragment = EnterManuallyFragment()
                fragment.arguments = bundle
                fragmentManager = supportFragmentManager
                fragmentTransaction = fragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment, fragment)
                fragmentTransaction.commit()
            }

            intent.hasExtra("locationModel") -> {
                val preFilledData = intent.getParcelableExtra<ModelAddress>("locationModel")
                val bundle = Bundle()
                bundle.putParcelable("model", preFilledData)
                val fragment = EnterManuallyFragment()
                fragment.arguments = bundle
                fragmentManager = supportFragmentManager
                fragmentTransaction = fragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment, fragment)
                fragmentTransaction.commit()
            }intent.hasExtra("modelLocation") -> {
                val preFilledData = intent.getParcelableExtra<LocationModel>("modelLocation")
                val bundle = Bundle()
                bundle.putParcelable("modelLocation", preFilledData)
                val fragment = EnterManuallyFragment()
                fragment.arguments = bundle
                fragmentManager = supportFragmentManager
                fragmentTransaction = fragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment, fragment)
                fragmentTransaction.commit()
            }intent.hasExtra("modelLocationn") -> {
                val preFilledData = intent.getParcelableExtra<LocationModel>("modelLocationn")
                val bundle = Bundle()
                bundle.putParcelable("modelLocationn", preFilledData)
                val fragment = EnterManuallyFragment()
                fragment.arguments = bundle
                fragmentManager = supportFragmentManager
                fragmentTransaction = fragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment, fragment)
                fragmentTransaction.commit()
            }
            else -> {
                val fragment = EnterManuallyFragment()
                fragmentManager = supportFragmentManager
                fragmentTransaction = fragmentManager.beginTransaction()
                fragmentTransaction.replace(R.id.fragment, fragment)
                fragmentTransaction.commit()
            }
        }

    }

    private fun clickListener() {

    }

    private fun initiz() {
        toolbar = findViewById(R.id.toolbar4)

    }

    private fun setToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(com.upscribber.R.drawable.ic_arrow_back_black_24dp)

    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }

        return super.onOptionsItemSelected(item)
    }
}
