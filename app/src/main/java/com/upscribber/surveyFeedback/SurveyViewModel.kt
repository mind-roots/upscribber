package com.upscribber.surveyFeedback

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData

class SurveyViewModel(application: Application) : AndroidViewModel(application) {

    var survey: SurveyRepository = SurveyRepository(application)

    fun getFeedbackData(obj: String) {
        survey.sendFeedBack(obj)
    }

    fun getFeedbackObserve() : LiveData<String>{
         return survey.feedbackObserver()
    }

}