package com.upscribber.surveyFeedback

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.subsciptions.manageSubscriptions.shareSubscription.ShareSuccessful
import com.upscribber.subsciptions.merchantSubscriptionPages.SuccessfullShared
import kotlinx.android.synthetic.main.activity_survey_feedback.*
import kotlinx.android.synthetic.main.can_do_better.*
import kotlinx.android.synthetic.main.rate_experience.*
import kotlinx.android.synthetic.main.why_rate_question.*
import org.json.JSONObject
import java.lang.Exception

class SurveyFeedbackActivity : AppCompatActivity(), FeedbackAdapter.SaveData,
    FeedbackAdapterTwo.SaveData {


    private var arrayTwo: ArrayList<FeedbackModel> = ArrayList()
    private var arrayOne: ArrayList<FeedbackModel> = ArrayList()
    private var answerArrays: ArrayList<AnswerArray> = ArrayList()
    private var feedBackModel: FeedbackModel = FeedbackModel()
    private var selected: String = "0"
    lateinit var feedbackAdapter: FeedbackAdapter
    lateinit var feedbackAdapterTwo: FeedbackAdapterTwo
    var pagePosition: Int = 0
    private lateinit var toolbar: Toolbar
    private lateinit var toolbarTitle: TextView
    lateinit var mViewModel: SurveyViewModel
    lateinit var progressBar3: LinearLayout


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_survey_feedback)
        init()
        setToolbar()
        setAdapter()
        setSecondData()
        getData()
        observerInit()
    }

    private fun init() {
        mViewModel = ViewModelProviders.of(this)[SurveyViewModel::class.java]
        progressBar3 = findViewById(R.id.progressBar3)
    }

    private fun getData() {
        val arrayFeedback = Constant.getFeedbackArrayList(this, Constant.feedArray)
        answerArrays = arrayFeedback
        val answerModel = AnswerArray()
        getData(arrayFeedback, answerModel)
        nextButtonClick(arrayFeedback)
        backButtonClick(arrayFeedback)
    }

    private fun observerInit() {
        mViewModel.getFeedbackObserve().observe(this, Observer {
            if (it == "true") {
                val answerArray = ArrayList<AnswerArray>()
                val gson = Gson()
                val json = gson.toJson(answerArray)
                val editor = Constant.getPrefs1(this).edit()
                editor.putString(Constant.feedArray, json)
                editor.apply()
                progressBar3.visibility = View.GONE
                startActivity(
                    Intent(this, SuccessfullShared::class.java).putExtra(
                        "feedback",
                        "feedback"
                    )
                )
                finish()
            }
        })
    }


    private fun getData(
        arrayFeedback: ArrayList<AnswerArray>,
        answerModel: AnswerArray
    ) {


        if (arrayFeedback.size == 0) {
            arrayFeedback.add(answerModel)
            arrayFeedback.add(answerModel)
            arrayFeedback.add(answerModel)
            arrayFeedback.add(answerModel)
            arrayFeedback.add(answerModel)
        } else {

            setPrefieldData(arrayFeedback)

            for (i in 0 until arrayFeedback.size) {

                if (arrayFeedback[i].selectedAnswer == "") {

//                    if (arrayFeedback[i].toString() == arrayFeedback[i].pagePosition.toString()) {
                    openNotSelectedAnswer(i, arrayFeedback)
                    pagePosition = i
                    break
//                    }
                }
            }
        }
    }

    private fun openNotSelectedAnswer(
        pagePosition: Int,
        arrayFeedback: ArrayList<AnswerArray>
    ) {

        viewLayoutAccPosition(pagePosition, arrayFeedback)
    }


    private fun setToolbar() {

        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        title = ""
        toolbarTitle = toolbar.findViewById(R.id.title) as TextView

        if (pagePosition == 0) {
            toolbarTitle.text = "1-5"
        }

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)
    }


    private fun setAdapter() {

        val manager1 = LinearLayoutManager(this)
        manager1.orientation = LinearLayoutManager.VERTICAL
        arrayOne = setAdapterData()

        feedbackAdapter = FeedbackAdapter(this, arrayOne)

        rvSelect.layoutManager = manager1
        rvSelect.adapter = feedbackAdapter
    }

    private fun setSecondData() {

        val manager2 = LinearLayoutManager(this)
        manager2.orientation = LinearLayoutManager.VERTICAL
        arrayTwo = setAdapterDataOne()
        feedbackAdapterTwo = FeedbackAdapterTwo(this, arrayTwo)
        rvSelectTwo.layoutManager = manager2
        rvSelectTwo.adapter = feedbackAdapterTwo
    }

    private fun setAdapterData(): ArrayList<FeedbackModel> {
        val feedbackArray = ArrayList<FeedbackModel>()

        var feedbackModel = FeedbackModel()
        feedbackModel.textOption = getString(R.string.page_1)
        feedbackModel.selectOne = false

        feedbackArray.add(feedbackModel)

        feedbackModel = FeedbackModel()
        feedbackModel.textOption = getString(R.string.page_1_1)
        feedbackModel.selectOne = false

        feedbackArray.add(feedbackModel)

        feedbackModel = FeedbackModel()
        feedbackModel.textOption = getString(R.string.page_1_2)
        feedbackModel.selectOne = false

        feedbackArray.add(feedbackModel)

        feedbackModel = FeedbackModel()
        feedbackModel.textOption = getString(R.string.page_1_3)
        feedbackModel.selectOne = false

        feedbackArray.add(feedbackModel)

        feedbackModel = FeedbackModel()
        feedbackModel.textOption = getString(R.string.page_1_4)
        feedbackModel.selectOne = false

        feedbackArray.add(feedbackModel)

        feedbackModel = FeedbackModel()
        feedbackModel.textOption = getString(R.string.page_1_5)
        feedbackModel.selectOne = false

        feedbackArray.add(feedbackModel)

        return feedbackArray
    }

    private fun setAdapterDataOne(): ArrayList<FeedbackModel> {
        val feedbackArray = ArrayList<FeedbackModel>()

        var feedbackModel = FeedbackModel()
        feedbackModel.textOption = getString(R.string.page_2)
        feedbackModel.selectOne = false
        feedbackArray.add(feedbackModel)

        feedbackModel = FeedbackModel()
        feedbackModel.textOption = getString(R.string.page_2_1)
        feedbackModel.selectOne = false
        feedbackArray.add(feedbackModel)


        feedbackModel = FeedbackModel()
        feedbackModel.textOption = getString(R.string.page_2_2)
        feedbackModel.selectOne = false
        feedbackArray.add(feedbackModel)

        feedbackModel = FeedbackModel()
        feedbackModel.textOption = getString(R.string.page_2_3)
        feedbackModel.selectOne = false
        feedbackArray.add(feedbackModel)

        feedbackModel = FeedbackModel()
        feedbackModel.textOption = getString(R.string.page_2_4)
        feedbackModel.selectOne = false
        feedbackArray.add(feedbackModel)


        return feedbackArray
    }


    override fun setSelectedData(
        model: FeedbackModel,
        newPosition: String
    ) {

        feedBackModel = model

        putDataInArray(feedBackModel, newPosition)

    }

    private fun putDataInArray(
        model: FeedbackModel, newPosition: String
    ) {

        val answerModel = AnswerArray()

        answerModel.selectedAnswer = newPosition
        answerModel.itemPosition = model.selectOne
        answerModel.pagePosition = pagePosition

        answerArrays[pagePosition] = answerModel

    }

    private fun saveToSharedPreference(answerArray: ArrayList<AnswerArray>) {
        val gson = Gson()
        val json = gson.toJson(answerArray)
        val editor = Constant.getPrefs1(this).edit()
        editor.putString(Constant.feedArray, json)
        editor.apply()
    }


    @SuppressLint("SetTextI18n")
    private fun nextButtonClick(answerArrays: ArrayList<AnswerArray>) {

        continueButton.setOnClickListener {

            when (pagePosition) {
                0 -> {

                    saveToSharedPreference(answerArrays)

                    if (answerArrays[pagePosition].selectedAnswer.isNotEmpty() && answerArrays[pagePosition].itemPosition) {
                        pagePosition += 1
                        viewLayoutAccPosition(pagePosition, answerArrays)
                    } else {
                        Toast.makeText(this, "Please select one of them", Toast.LENGTH_SHORT)
                            .show()
                    }
                }

                1 -> {

                    saveToSharedPreference(answerArrays)

                    if (answerArrays[pagePosition].selectedAnswer.isNotEmpty() && answerArrays[pagePosition].itemPosition) {
                        pagePosition += 1
                        viewLayoutAccPosition(pagePosition, answerArrays)
                    } else {
                        Toast.makeText(this, "Please select one of them", Toast.LENGTH_SHORT)
                            .show()
                    }
                }

                2 -> {

                    saveToSharedPreference(answerArrays)
                    if (answerArrays[pagePosition].selectedAnswer.isNotEmpty()) {
                        pagePosition += 1
                        viewLayoutAccPosition(pagePosition, answerArrays)
                    } else {
                        Toast.makeText(this, "Please select one of them", Toast.LENGTH_SHORT)
                            .show()
                    }

                }
                3 -> {

                    if (etWhyQuestion.text.isEmpty()) {
                        Toast.makeText(this, "Please enter some text", Toast.LENGTH_SHORT)
                            .show()
                    } else {

                        val answerModel = answerArrays[pagePosition]
                        answerModel.selectedAnswer = etWhyQuestion.text.toString()
                        answerModel.pagePosition = pagePosition
                        answerArrays[pagePosition] = answerModel
                        saveToSharedPreference(answerArrays)
                        pagePosition += 1
                        viewLayoutAccPosition(pagePosition, answerArrays)
                    }
                }
                4 -> {

                    continueButton.text = "Submit"

                    if (etDoBetter.text.isEmpty()) {
                        Toast.makeText(this, "Please enter some text", Toast.LENGTH_SHORT)
                            .show()
                    } else {
                        val answerModel = answerArrays[pagePosition]
                        answerModel.selectedAnswer = etDoBetter.text.toString()
                        answerModel.pagePosition = pagePosition
                        answerArrays[pagePosition] = answerModel

                        saveToSharedPreference(answerArrays)
                        sendDataToApi(answerArrays)
                    }

                }
            }
        }
    }


    @SuppressLint("SetTextI18n")
    private fun backButtonClick(answerArrays: ArrayList<AnswerArray>) {

        tvBackPage.setOnClickListener {

            when (pagePosition) {
                0 -> {
                    tvBackPage.isEnabled = false
                    Toast.makeText(this, "You can not go back", Toast.LENGTH_SHORT).show()

                }

                1 -> {

                    saveToSharedPreference(answerArrays)
                    pagePosition -= 1
                    setAdapter()
                    arrayOne[answerArrays[0].selectedAnswer.toInt() - 1].selectOne = true
                    feedbackAdapter.update(arrayOne)

                    toolbarTitle.text = "1-5"
                    rvSelect.visibility = View.VISIBLE
                    tvFeedTitle.visibility = View.VISIBLE
                    rvSelectTwo.visibility = View.INVISIBLE
                    layout_two.visibility = View.INVISIBLE
                    tvBackPage.setBackgroundResource(R.drawable.back_page)
                    tvQuestion.text = getString(R.string.question_one)
                }
                2 -> {

                    saveToSharedPreference(answerArrays)
                    pagePosition -= 1
                    arrayTwo[answerArrays[0].selectedAnswer.toInt() - 1].selectOne = true
                    feedbackAdapterTwo.update(arrayTwo)

                    toolbarTitle.text = "2-5"
                    rvSelect.visibility = View.INVISIBLE
                    rvSelectTwo.visibility = View.VISIBLE
                    layout_two.visibility = View.INVISIBLE
                    tvBackPage.setBackgroundResource(R.drawable.next_page)
                    tvNextPage.setBackgroundResource(R.drawable.next_page)

                    tvFeedTitle.visibility = View.GONE
                    tvQuestion.text = getString(R.string.quetion_two)
                }
                3 -> {

                    val answerModel = answerArrays[pagePosition]
                    answerModel.selectedAnswer = etWhyQuestion.text.toString()
                    answerModel.pagePosition = pagePosition
                    this.answerArrays[pagePosition] = answerModel
                    saveToSharedPreference(answerArrays)
                    pagePosition -= 1
                    tvBackPage.setBackgroundResource(R.drawable.next_page)
                    tvNextPage.setBackgroundResource(R.drawable.next_page)
                    viewLayoutTwo(pagePosition, answerArrays)

                }
                4 -> {

                    continueButton.text = "Submit"

                    val answerModel = answerArrays[pagePosition]
                    answerModel.selectedAnswer = etDoBetter.text.toString()
                    answerModel.pagePosition = pagePosition
                    this.answerArrays[pagePosition] = answerModel
                    saveToSharedPreference(answerArrays)
                    pagePosition -= 1
                    tvBackPage.setBackgroundResource(R.drawable.next_page)
                    tvNextPage.setBackgroundResource(R.drawable.next_page)
                    viewLayoutThree(pagePosition, answerArrays)
                }
            }

        }
    }


    @SuppressLint("SetTextI18n")
    private fun viewLayoutTwo(
        pagePosition: Int,
        arrayFeedback: ArrayList<AnswerArray>
    ) {

        if (pagePosition == 2) {
            continueButton.text = "Continue"
            toolbarTitle.text = "3-5"
            tvFeedTitle.visibility = View.GONE
            tvQuestion.text = getString(R.string.question_three)
            rvSelect.visibility = View.INVISIBLE
            rvSelectTwo.visibility = View.GONE
            layout_three.visibility = View.GONE
            layout_two.visibility = View.VISIBLE
            tvBackPage.setBackgroundResource(R.drawable.next_page)
        }
        if (arrayFeedback[pagePosition].selectedAnswer.isNotEmpty()) {

            if (arrayFeedback[pagePosition].selectedAnswer == "1") {
                ivGood.setImageResource(R.drawable.ic_thumbsup_selected)
                ivBad.setImageResource(R.drawable.ic_thumbsdown_unselected)
            } else {
                ivBad.setImageResource(R.drawable.ic_thumbsdown_selected)
                ivGood.setImageResource(R.drawable.ic_thumbsup_unselected)
            }
        }
        cvGood.setOnClickListener {
            selected = "1"
            ivGood.setImageResource(R.drawable.ic_thumbsup_selected)
            ivBad.setImageResource(R.drawable.ic_thumbsdown_unselected)
            saveRateExperience(selected)
        }
        cvBad.setOnClickListener {
            selected = "2"
            ivBad.setImageResource(R.drawable.ic_thumbsdown_selected)
            ivGood.setImageResource(R.drawable.ic_thumbsup_unselected)
            saveRateExperience(selected)
        }

    }

    private fun saveRateExperience(selected: String) {

        val answerModel = AnswerArray()
        answerModel.selectedAnswer = selected
        answerModel.pagePosition = pagePosition
        answerArrays[pagePosition] = answerModel
    }


    @SuppressLint("SetTextI18n")
    private fun viewLayoutThree(
        pagePosition: Int,
        arrayFeedback: ArrayList<AnswerArray>
    ) {

        if (pagePosition == 3) {
            continueButton.text = "Continue"
            toolbarTitle.text = "4-5"
            tvFeedTitle.visibility = View.GONE
            tvQuestion.text = getString(R.string.question_four)
            layout_three.visibility = View.VISIBLE
            layout_four.visibility = View.GONE
            layout_two.visibility = View.GONE
//            rvSelectTwo.visibility = View.GONE
            rvSelect.visibility = View.INVISIBLE

            tvBackPage.setBackgroundResource(R.drawable.next_page)
        }

        if (arrayFeedback[pagePosition].selectedAnswer.isNotEmpty()) {
            etWhyQuestion.setText(arrayFeedback[pagePosition].selectedAnswer)
        }

        skipClick(answerArrays)


    }


    @SuppressLint("SetTextI18n")
    private fun viewLayoutFour(
        pagePosition: Int,
        arrayFeedback: ArrayList<AnswerArray>
    ) {

        if (pagePosition == 4) {
            continueButton.text = "Submit"
            toolbarTitle.text = "5-5"
            tvFeedTitle.visibility = View.GONE
            tvQuestion.text = getString(R.string.question_five)
            layout_three.visibility = View.GONE
            layout_four.visibility = View.VISIBLE
            rvSelectTwo.visibility = View.GONE
            rvSelect.visibility = View.INVISIBLE
            tvNextPage.setBackgroundResource(R.drawable.back_page)
            tvBackPage.setBackgroundResource(R.drawable.next_page)
        }

        if (arrayFeedback[pagePosition].selectedAnswer.isNotEmpty()) {
            etDoBetter.setText(arrayFeedback[pagePosition].selectedAnswer)
        }
        skipClick(answerArrays)
    }


    private fun skipClick(answerArrays: ArrayList<AnswerArray>) {

        skipOne.setOnClickListener {

            pagePosition += 1
            viewLayoutFour(pagePosition, answerArrays)
        }

        tvSkipTwo.setOnClickListener {
            sendDataToApi(answerArrays)
        }

    }

    private fun sendDataToApi(answerArrays: ArrayList<AnswerArray>) {
        val obj = JSONObject()
        progressBar3.visibility = View.VISIBLE
        for (i in 0 until answerArrays.size) {
            obj.put((i + 1).toString(), answerArrays[i].selectedAnswer)
        }
        mViewModel.getFeedbackData(obj.toString())
        print(obj)
    }

    @SuppressLint("SetTextI18n")
    private fun viewLayoutAccPosition(
        pagePosition: Int,
        arrayFeedback: ArrayList<AnswerArray>
    ) {

        when (pagePosition) {
            0 -> {
                continueButton.text = "Continue"
                try {
                    if (arrayFeedback.size >= 1) {
                        arrayOne[arrayFeedback[0].selectedAnswer.toInt() - 1].selectOne = true
                        feedbackAdapter.update(arrayOne)

                    }
                } catch (e: Exception) {
                    e.printStackTrace()

                }


                toolbarTitle.text = "1-5"
                tvBackPage.setBackgroundResource(R.drawable.back_page)
                tvNextPage.setBackgroundResource(R.drawable.next_page)
                tvFeedTitle.visibility = View.VISIBLE
                tvQuestion.text = getString(R.string.question_one)
                rvSelect.visibility = View.VISIBLE
                layout_two.visibility = View.GONE
                rvSelectTwo.visibility = View.GONE

            }
            1 -> {
                continueButton.text = "Continue"

                try {
                    if (arrayFeedback.size >= 2) {
                        if (arrayFeedback[1].selectedAnswer.isNotEmpty()) {
                            arrayTwo[arrayFeedback[1].selectedAnswer.toInt() - 1].selectOne = true
                        }
                        feedbackAdapterTwo.update(arrayTwo)
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }



                toolbarTitle.text = "2-5"
                tvBackPage.setBackgroundResource(R.drawable.next_page)
                tvNextPage.setBackgroundResource(R.drawable.next_page)
                tvFeedTitle.visibility = View.GONE
                tvQuestion.text = getString(R.string.quetion_two)
                rvSelectTwo.visibility = View.VISIBLE
                layout_two.visibility = View.GONE
                rvSelect.visibility = View.INVISIBLE
            }
            2 -> {
                continueButton.text = "Continue"
                viewLayoutTwo(pagePosition, answerArrays)
            }
            3 -> {
                continueButton.text = "Continue"
                viewLayoutThree(pagePosition, answerArrays)
            }
            4 -> {
                continueButton.text = "Submit"
                viewLayoutFour(pagePosition, answerArrays)
            }
        }
    }

    @SuppressLint("SetTextI18n")
    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            when (pagePosition) {
                0 -> {
                    finish()

                }

                1 -> {

                    saveToSharedPreference(answerArrays)
                    pagePosition -= 1
                    setAdapter()
                    arrayOne[answerArrays[0].selectedAnswer.toInt() - 1].selectOne = true
                    feedbackAdapter.update(arrayOne)

                    toolbarTitle.text = "1-5"
                    rvSelect.visibility = View.VISIBLE
                    tvFeedTitle.visibility = View.VISIBLE
                    rvSelectTwo.visibility = View.INVISIBLE
                    layout_two.visibility = View.GONE
                    tvBackPage.setBackgroundResource(R.drawable.back_page)
                    tvQuestion.text = getString(R.string.question_one)
                }
                2 -> {

                    saveToSharedPreference(answerArrays)
                    pagePosition -= 1
                    arrayTwo[answerArrays[1].selectedAnswer.toInt() - 1].selectOne = true
                    feedbackAdapterTwo.update(arrayTwo)

                    toolbarTitle.text = "2-5"
                    rvSelect.visibility = View.INVISIBLE
                    rvSelectTwo.visibility = View.VISIBLE
                    layout_two.visibility = View.GONE
                    tvBackPage.setBackgroundResource(R.drawable.next_page)
                    tvNextPage.setBackgroundResource(R.drawable.next_page)

                    tvFeedTitle.visibility = View.GONE
                    tvQuestion.text = getString(R.string.quetion_two)
                }
                3 -> {

                    val answerModel = answerArrays[pagePosition]
                    answerModel.selectedAnswer = etWhyQuestion.text.toString()
                    answerModel.pagePosition = pagePosition
                    this.answerArrays[pagePosition] = answerModel
                    saveToSharedPreference(answerArrays)
                    pagePosition -= 1
                    tvBackPage.setBackgroundResource(R.drawable.next_page)
                    tvNextPage.setBackgroundResource(R.drawable.next_page)
                    viewLayoutTwo(pagePosition, answerArrays)

                }
                4 -> {
                    continueButton.text = "Submit"
                    val answerModel = answerArrays[pagePosition]
                    answerModel.selectedAnswer = etDoBetter.text.toString()
                    answerModel.pagePosition = pagePosition
                    this.answerArrays[pagePosition] = answerModel
                    saveToSharedPreference(answerArrays)
                    pagePosition -= 1
                    tvBackPage.setBackgroundResource(R.drawable.next_page)
                    tvNextPage.setBackgroundResource(R.drawable.next_page)
                    viewLayoutThree(pagePosition, answerArrays)
                }
            }
        }

        return super.onOptionsItemSelected(item)
    }

    private fun setPrefieldData(arrayFeedback: ArrayList<AnswerArray>) {

        try {

            if (arrayFeedback[0].selectedAnswer.isNotEmpty()) {
                arrayOne[arrayFeedback[0].selectedAnswer.toInt() - 1].selectOne = true
            }
            feedbackAdapter.update(arrayOne)

            if (arrayFeedback[1].selectedAnswer.isNotEmpty()) {
                arrayTwo[arrayFeedback[1].selectedAnswer.toInt() - 1].selectOne = true
            }
            feedbackAdapterTwo.update(arrayTwo)

        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

}
