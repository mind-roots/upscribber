package com.upscribber.surveyFeedback

import android.app.Application
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.upscribber.commonClasses.Constant
import com.upscribber.commonClasses.WebServicesCustomers
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class SurveyRepository(var application: Application) {

    private val mFeedback = MutableLiveData<String>()


    fun feedbackObserver(): LiveData<String> {
        return mFeedback
    }

    fun sendFeedBack(obj: String) {

        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()
        val auth = Constant.getPrefs(application).getString(Constant.auth_code, "")

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.saveFeedback(auth, obj)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {

                if (response.isSuccessful) {

                    try {

                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")

                        if (status == "true") {
                            mFeedback.value = status
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Toast.makeText(application, "failure", Toast.LENGTH_SHORT).show()
            }


        })
    }

}