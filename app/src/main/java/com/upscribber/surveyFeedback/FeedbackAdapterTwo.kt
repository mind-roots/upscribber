package com.upscribber.surveyFeedback

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.R
import kotlinx.android.synthetic.main.feedback_layout.view.*
import java.util.ArrayList

class FeedbackAdapterTwo(
    var mContext: Context,
    var arrayList: ArrayList<FeedbackModel>
) : RecyclerView.Adapter<FeedbackAdapterTwo.ViewHolder>() {

    private var newPosition: String = ""
    var selectedposition = -1
    var arrayToShow : ArrayList<FeedbackModel> = arrayList
    var saveData = mContext as SaveData


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val view =
            LayoutInflater.from(mContext).inflate(R.layout.feedback_layout, parent, false)
        return ViewHolder(view)

    }

    override fun getItemCount(): Int {

        return arrayToShow.size

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = arrayToShow[position]

        holder.itemView.tvSelect.text = model.textOption

        //newPosition = (position + 1).toString()
        holder.itemView.rbSelect.isChecked = model.selectOne == true


        holder.itemView.rbSelect.setOnClickListener {

            if (arrayToShow[position].selectOne) {
                arrayToShow[position].selectOne = false
                model.selectOne = false
            } else {
                for (items in 0 until arrayToShow.size) {
                    arrayToShow[items].selectOne = false
                }
                arrayToShow[position].selectOne = !model.selectOne
            }
            saveData.setSelectedData(model, (position + 1).toString())
            notifyDataSetChanged()
        }

        holder.itemView.setOnClickListener {

            if (arrayToShow[position].selectOne) {
                arrayToShow[position].selectOne = false
                model.selectOne = false
            } else {
                for (items in 0 until arrayToShow.size) {
                    arrayToShow[items].selectOne = false
                }
                arrayToShow[position].selectOne = !model.selectOne
            }


            saveData.setSelectedData(model, (position + 1).toString())

            notifyDataSetChanged()
        }

    }

    fun update(feedbackArray: ArrayList<FeedbackModel>) {

        arrayToShow = feedbackArray
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    interface SaveData {

        fun setSelectedData(
            model: FeedbackModel,
            newPosition: String
        )

    }

}