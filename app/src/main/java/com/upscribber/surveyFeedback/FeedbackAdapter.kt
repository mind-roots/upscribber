package com.upscribber.surveyFeedback

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.R
import kotlinx.android.synthetic.main.feedback_layout.view.*
import java.util.ArrayList

class FeedbackAdapter(
    var mContext: Context,
    var arrayList: ArrayList<FeedbackModel>
) : RecyclerView.Adapter<FeedbackAdapter.ViewHolder>() {

    private var newPosition: String = ""
    var selectedposition = -1
    var saveData = mContext as SaveData


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val view =
            LayoutInflater.from(mContext).inflate(R.layout.feedback_layout, parent, false)
        return ViewHolder(view)

    }

    override fun getItemCount(): Int {

        return arrayList.size

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = arrayList[position]

        holder.itemView.tvSelect.text = model.textOption


//        if (position == selectedposition) {
//            holder.itemView.rbSelect.isChecked = true
//        } else {
//            holder.itemView.rbSelect.isChecked = false
//            arrayList[position].selectOne = false
//        }
        holder.itemView.rbSelect.isChecked = model.selectOne == true

        holder.itemView.rbSelect.setOnClickListener {

            if (arrayList[position].selectOne) {
                arrayList[position].selectOne = false
                model.selectOne = false
            } else {
                for (items in 0 until arrayList.size) {
                    arrayList[items].selectOne = false
                }
                arrayList[position].selectOne = !model.selectOne
            }

            saveData.setSelectedData(model, (position + 1).toString())

            notifyDataSetChanged()
        }

        holder.itemView.setOnClickListener {

            if (arrayList[position].selectOne) {
                arrayList[position].selectOne = false
                model.selectOne = false
            } else {
                for (items in 0 until arrayList.size) {
                    arrayList[items].selectOne = false
                }
                arrayList[position].selectOne = !model.selectOne
            }

            saveData.setSelectedData(model, (position + 1).toString())
            notifyDataSetChanged()
        }

    }

    fun update(feedbackArray: ArrayList<FeedbackModel>) {

        this.arrayList = feedbackArray
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    interface SaveData {

        fun setSelectedData(
            model: FeedbackModel,
            newPosition: String
        )

    }

}
