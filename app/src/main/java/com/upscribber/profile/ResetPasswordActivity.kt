package com.upscribber.profile

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.upscribber.commonClasses.Constant
import com.upscribber.R
import com.upscribber.databinding.ActivityResetPasswordBinding
import kotlinx.android.synthetic.main.activity_reset_password.*

class ResetPasswordActivity : AppCompatActivity() {

    lateinit var mBinding: ActivityResetPasswordBinding
    lateinit var mViewModel: ProfileViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_reset_password)
        mViewModel = ViewModelProviders.of(this)[ProfileViewModel::class.java]
        setToolbar()
        observersInit()
        val isSeller = Constant.getSharedPrefs(this).getBoolean(Constant.IS_SELLER, false)
        if (isSeller) {
            mBinding.textDone.setBackgroundResource(R.drawable.bg_seller_button_blue)
        } else {
            mBinding.textDone.setBackgroundResource(R.drawable.invite_button_background)
        }
    }

    private fun observersInit() {
        mViewModel.getmDataPassword().observe(this, Observer {
            if (it.status == "true") {
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
                val sharedPreferences = getSharedPreferences("updatePassword", Context.MODE_PRIVATE)
                val editor = sharedPreferences.edit()
                editor.putString("profilePass","back")
                editor.apply()
                finish()
            }
        })

        mViewModel.getStatus().observe(this, androidx.lifecycle.Observer {
            if (it.msg.isNotEmpty()) {
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
            }

        })

    }

    private fun setToolbar() {
        setSupportActionBar(mBinding.include6.toolbar)
        title = ""
        mBinding.include6.title.text = "Change Password"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)

    }

    fun saveClick(v: View) {
        val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
        if (etNewPass.text.toString() == textNewPassConfirm.text.toString()){
        mViewModel.changePass(auth, etOldPass.text.toString().trim(), etNewPass.text.toString().trim())
        }else{
            Toast.makeText(this,"New Password & Confirm Password does not match!",Toast.LENGTH_LONG).show()
        }

    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}
