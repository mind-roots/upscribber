package com.upscribber.profile

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.databinding.ActivityUpdateSocialBinding
import kotlinx.android.synthetic.main.activity_update_social.*

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class UpdateSocialActivity : AppCompatActivity() {

    private lateinit var mViewModel: ProfileViewModel
    lateinit var mBinding: ActivityUpdateSocialBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_update_social)
        mViewModel = ViewModelProviders.of(this)[ProfileViewModel::class.java]
        setToolbar()
        setData()
        clickListener()
        observerInit()
    }

    private fun observerInit() {
        mViewModel.getmDataUpdateSkill().observe(this, Observer {
            if (it.status == "true") {
                mBinding.progressBar.visibility = View.GONE
                val sharedPreferences = getSharedPreferences("updateProfile", Context.MODE_PRIVATE)
                val editor = sharedPreferences.edit()
                editor.putString("profile", "back")
                editor.apply()

                when {
                    intent.hasExtra("modelProfileBusinessInfo") -> {
                        intent.putExtra("status", it.status)
                        setResult(902, intent)
                        finish()

                    }
                    intent.hasExtra("getData") -> {
                        intent.putExtra("status", it.status)
                        setResult(904, intent)
                        finish()

                    }
                    else -> {
                        editor.putString("social", "data")
                        editor.apply()
                        finish()
                    }
                }

            }
        })

        mViewModel.getStatus().observe(this, androidx.lifecycle.Observer {
            mBinding.progressBar.visibility = View.GONE
            if (it.msg == "Invalid auth code") {
                Constant.commonAlert(this)
            } else {
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
            }
        })

    }

    private fun getTextFieldsData(): Boolean {
        val facebook = mBinding.editTextFacebook.text.toString().trim().length
        val instagram = mBinding.edittextInstagram.text.toString().trim().length
        val twitter = mBinding.editTextTwitter.text.toString().trim().length
        val appoint = mBinding.editTextAppoint.text.toString().trim().length


        if (facebook == 0 && instagram == 0 && twitter == 0 && appoint == 0) {
            return false
        }

        return true

    }


    private fun clickListener() {
        val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
        val type = Constant.getPrefs(this).getString(Constant.type, "")

        mBinding.editTextFacebook.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(
                s: CharSequence,
                start: Int,
                count: Int,
                after: Int
            ) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (getTextFieldsData()) {
                    mBinding.saveButton.setBackgroundResource(R.drawable.bg_seller_button_blue)
                } else {
                    mBinding.saveButton.setBackgroundResource(R.drawable.blue_button_background_opacity)
                }
            }

            override fun afterTextChanged(s: Editable) {
            }
        })
        mBinding.editTextTwitter.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(
                s: CharSequence,
                start: Int,
                count: Int,
                after: Int
            ) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (getTextFieldsData()) {
                    mBinding.saveButton.setBackgroundResource(R.drawable.bg_seller_button_blue)
                } else {
                    mBinding.saveButton.setBackgroundResource(R.drawable.blue_button_background_opacity)
                }
            }

            override fun afterTextChanged(s: Editable) {
            }
        })
        mBinding.edittextInstagram.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(
                s: CharSequence,
                start: Int,
                count: Int,
                after: Int
            ) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (getTextFieldsData()) {
                    mBinding.saveButton.setBackgroundResource(R.drawable.bg_seller_button_blue)
                } else {
                    mBinding.saveButton.setBackgroundResource(R.drawable.blue_button_background_opacity)
                }
            }

            override fun afterTextChanged(s: Editable) {
            }
        })
        mBinding.editTextAppoint.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(
                s: CharSequence,
                start: Int,
                count: Int,
                after: Int
            ) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (getTextFieldsData()) {
                    mBinding.saveButton.setBackgroundResource(R.drawable.bg_seller_button_blue)
                } else {
                    mBinding.saveButton.setBackgroundResource(R.drawable.blue_button_background_opacity)
                }
            }

            override fun afterTextChanged(s: Editable) {
            }
        })



        mBinding.saveButton.setOnClickListener {
            if (mBinding.editTextFacebook.text.toString()
                    .isEmpty() && mBinding.edittextInstagram.text.toString()
                    .isEmpty() && mBinding.editTextTwitter.text.toString()
                    .isEmpty() && mBinding.editTextAppoint.text.toString().isEmpty()
            ) {
                Toast.makeText(
                    this,
                    "Please enter the details to update your social accounts!",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                mViewModel.updateSocial(
                    auth,
                    type,
                    mBinding.editTextFacebook.text.toString().trim(),
                    mBinding.edittextInstagram.text.toString().trim(),
                    mBinding.editTextTwitter.text.toString().trim(),
                    mBinding.editTextAppoint.text.toString().trim()

                )
                mBinding.progressBar.visibility = View.VISIBLE
            }
        }
    }

    private fun setData() {
        if (intent.hasExtra("modelProfile")) {
            val model = intent.getParcelableExtra<ModelGetProfile>("modelProfile")
            textView37.text = "Update Social"
            titleDescription.text = "Provide your Update Socials here"
            editTextFacebook.setText(model.facebook)
            edittextInstagram.setText(model.instagram)
            editTextTwitter.setText(model.twitter)
            editTextAppoint.setText(model.schedule)
            if (model.facebook.isNotEmpty() || model.twitter.isNotEmpty() || model.instagram.isNotEmpty() || model.schedule.isNotEmpty()) {
                mBinding.saveButton.setBackgroundResource(R.drawable.bg_seller_button_blue)

            } else {
                mBinding.saveButton.setBackgroundResource(R.drawable.blue_button_background_opacity)

            }
        } else if (intent.hasExtra("onlySchedule")) {
            val model = intent.getParcelableExtra<ModelGetProfile>("onlySchedule")
            textView37.text = "Schedule Link"
            titleDescription.text =
                "Please add your scheduling link so that customers can schedule an appointment with you"
            imageView60.visibility = View.GONE
            textFacebook.visibility = View.GONE
            editTextFacebook.visibility = View.GONE
            imageView65.visibility = View.GONE
            textInstagram.visibility = View.GONE
            edittextInstagram.visibility = View.GONE
            imageView20.visibility = View.GONE
            textTwitter.visibility = View.GONE
            editTextTwitter.visibility = View.GONE
            editTextAppoint.setText(model.schedule)
            if (model.schedule.isNotEmpty()) {
                mBinding.saveButton.setBackgroundResource(R.drawable.bg_seller_button_blue)

            } else {
                mBinding.saveButton.setBackgroundResource(R.drawable.blue_button_background_opacity)

            }
        }
    }

    override fun onResume() {
        super.onResume()
//        val sharedPreferences1 = getSharedPreferences("updateProfile", Context.MODE_PRIVATE)
//        if (!sharedPreferences1.getString("profile", "").isEmpty()) {
//            val editor = sharedPreferences1.edit()
//            editor.remove("profile")
//            editor.apply()
//            finish()
//        }
    }


//    fun twitter(view: View) {
//        if (intent.hasExtra("modelProfile")) {
//            startActivity(
//                Intent(this, UpdateSkillsActivity::class.java).putExtra("twitter", "twitter")
//                    .putExtra(
//                        "modelProfile",
//                        intent.getParcelableExtra<ModelGetProfile>("modelProfile")
//                    )
//            )
//        } else {
//            startActivity(
//                Intent(this, UpdateSkillsActivity::class.java).putExtra("twitter", "twitter")
//            )
//        }
//    }
//
//    fun instagram(view: View) {
//        if (intent.hasExtra("modelProfile")) {
//            startActivity(
//                Intent(this, UpdateSkillsActivity::class.java).putExtra("instagram", "instagram")
//                    .putExtra(
//                        "modelProfile",
//                        intent.getParcelableExtra<ModelGetProfile>("modelProfile")
//                    )
//            )
//        } else {
//            startActivity(
//                Intent(this, UpdateSkillsActivity::class.java).putExtra("instagram", "instagram")
//            )
//
//        }
//    }
//
//    fun facebook(view: View) {
//        if (intent.hasExtra("modelProfile")) {
//            startActivity(
//                Intent(this, UpdateSkillsActivity::class.java).putExtra("facebook", "facebook")
//                    .putExtra(
//                        "modelProfile",
//                        intent.getParcelableExtra<ModelGetProfile>("modelProfile")
//                    )
//            )
//        } else {
//            startActivity(
//                Intent(this, UpdateSkillsActivity::class.java).putExtra("facebook", "facebook")
//            )
//
//        }
//    }

    private fun setToolbar() {
        setSupportActionBar(mBinding.include18.toolbar)
//        mBinding.include18.title.text = "Update Social"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)
        title = ""
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}
