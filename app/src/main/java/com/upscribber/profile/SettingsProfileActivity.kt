package com.upscribber.profile

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Outline
import android.location.Geocoder
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.*
import androidx.databinding.DataBindingUtil
import com.upscribber.commonClasses.Constant
import com.upscribber.R
import com.upscribber.databinding.ActivitySettingsProfileBinding
import com.upscribber.payment.PaymentTransactionActivity
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.upscribber.commonClasses.RoundedBottomSheetDialogFragment
import com.upscribber.filters.LocationModel
import com.upscribber.payment.paymentCards.WebPagesOpenActivity
import com.upscribber.subsciptions.merchantSubscriptionPages.SuccessfullShared


class SettingsProfileActivity : AppCompatActivity() {

    lateinit var mBinding: ActivitySettingsProfileBinding
    lateinit var mViewModel: ProfileViewModel
    var latitude1 = 0.0
    var longitude1 = 0.0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_settings_profile)
        mViewModel = ViewModelProviders.of(this)[ProfileViewModel::class.java]
        Places.initialize(applicationContext, resources.getString(R.string.google_maps_key))
        setToolbar()
        clickListeners()
        observerInit()
    }

    private fun observerInit() {
        mViewModel.getDeleteAccountCustomer().observe(this, Observer {
            mBinding.progressBar.visibility = View.GONE
            startActivity(
                Intent(this , SuccessfullShared::class.java).putExtra(
                    "deleteAccount",
                    "deleteAccount"
                )
            )

        })

    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun clickListeners() {
        val isSeller = Constant.getSharedPrefs(this).getBoolean(Constant.IS_SELLER, false)
        if (isSeller) {
            mBinding.changeLocation.visibility = View.GONE
            mBinding.updateSkills.visibility = View.VISIBLE
            mBinding.space11.visibility = View.VISIBLE
            mBinding.view11.visibility = View.VISIBLE
            mBinding.space12.visibility = View.VISIBLE
            mBinding.updatePayment.visibility = View.GONE
            mBinding.space1.visibility = View.GONE
            mBinding.space2.visibility = View.GONE
            mBinding.view1.visibility = View.GONE
            mBinding.space3.visibility = View.GONE
            mBinding.space4.visibility = View.GONE
            mBinding.view2.visibility = View.GONE
            mBinding.scheduleAppointMent.visibility = View.VISIBLE
            mBinding.spaceSchedul1.visibility = View.VISIBLE
            mBinding.spaceSchedul2.visibility = View.VISIBLE
            mBinding.viewSchedul1.visibility = View.VISIBLE
            mBinding.doneBtn.setBackgroundResource(R.drawable.bg_seller_button_blue)
        } else {
            mBinding.changeLocation.visibility = View.VISIBLE
            mBinding.updatePayment.visibility = View.GONE
            mBinding.updateSkills.visibility = View.GONE
            mBinding.space11.visibility = View.GONE
            mBinding.view11.visibility = View.GONE
            mBinding.space12.visibility = View.GONE
            mBinding.space1.visibility = View.VISIBLE
            mBinding.space2.visibility = View.VISIBLE
            mBinding.view1.visibility = View.VISIBLE
            mBinding.space3.visibility = View.GONE
            mBinding.space4.visibility = View.GONE
            mBinding.view2.visibility = View.GONE
            mBinding.scheduleAppointMent.visibility = View.GONE
            mBinding.spaceSchedul1.visibility = View.GONE
            mBinding.spaceSchedul2.visibility = View.GONE
            mBinding.viewSchedul1.visibility = View.GONE
            mBinding.doneBtn.setBackgroundResource(R.drawable.invite_button_background)
        }

        val regtrationType = Constant.getPrefs(this).getString(Constant.regtrationType, "")
        if (regtrationType == "1") {
            mBinding.changePassword.visibility = View.GONE
            mBinding.spacePass1.visibility = View.GONE
            mBinding.viewPass1.visibility = View.GONE
            mBinding.spacePass2.visibility = View.GONE
            if (isSeller) {
                mBinding.space11.visibility = View.VISIBLE
                mBinding.view11.visibility = View.VISIBLE
                mBinding.space12.visibility = View.VISIBLE
            } else {
                mBinding.space11.visibility = View.GONE
                mBinding.view11.visibility = View.GONE
                mBinding.space12.visibility = View.GONE
            }


        } else {
            mBinding.changePassword.visibility = View.GONE
            mBinding.space11.visibility = View.GONE
            mBinding.view11.visibility = View.GONE
            mBinding.space12.visibility = View.GONE
            if (isSeller) {
                mBinding.spacePass1.visibility = View.VISIBLE
                mBinding.viewPass1.visibility = View.VISIBLE
                mBinding.spacePass2.visibility = View.VISIBLE
            } else {
                mBinding.spacePass1.visibility = View.GONE
                mBinding.viewPass1.visibility = View.GONE
                mBinding.spacePass2.visibility = View.GONE
            }
        }



        mBinding.changeLocation.setOnClickListener {
            Places.createClient(this)
            val fields =
                listOf(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG)
            // Start the autocomplete intent.
            val intent = Autocomplete.IntentBuilder(
                AutocompleteActivityMode.FULLSCREEN, fields
            )
                .build(this)
            startActivityForResult(intent, 1)


        }

        mBinding.scheduleAppointMent.setOnClickListener{
            startActivity(Intent(this,UpdateSocialActivity::class.java).putExtra(
                "onlySchedule",
                intent.getParcelableExtra<ModelGetProfile>("modelProfile")
            ))
        }



        mBinding.updateSkills.setOnClickListener {
            startActivity(
                Intent(this, UpdateSkillsActivity::class.java).putExtra(
                    "modelProfile",
                    intent.getParcelableExtra<ModelGetProfile>("modelProfile")
                )
            )
        }

        mBinding.changePassword.setOnClickListener {
            startActivity(Intent(this, ResetPasswordActivity::class.java))
        }

        mBinding.editAccountInfo.setOnClickListener {
            startActivity(
                Intent(this, EditProfileActivity::class.java).putExtra(
                    "modelProfile",
                    intent.getParcelableExtra<ModelGetProfile>("modelProfile")
                )
            )
        }

        mBinding.updatePayment.setOnClickListener {
            startActivity(
                Intent(this, PaymentTransactionActivity::class.java).putExtra(
                    "modelProfile",
                    intent.getParcelableExtra<ModelGetProfile>("modelProfile")
                )
            )

        }

        mBinding.terms.setOnClickListener {
            startActivity(Intent(this, WebPagesOpenActivity::class.java).putExtra("terms", "terms"))
//            val theWebPage = WebView(this)
//            theWebPage.settings.javaScriptEnabled = true
//            theWebPage.settings.pluginState = PluginState.ON
//            setContentView(theWebPage)
//            theWebPage.loadUrl("https://www.upscribbr.com/terms/")
//
        }

        mBinding.privacy.setOnClickListener {
            startActivity(
                Intent(this, WebPagesOpenActivity::class.java).putExtra(
                    "privacy",
                    "privacy"
                )
            )
//            val theWebPage = WebView(this)
//            theWebPage.settings.javaScriptEnabled = true
//            theWebPage.settings.pluginState = PluginState.ON
//            setContentView(theWebPage)
//            theWebPage.loadUrl("https://www.upscribbr.com/privacy-policy/")


//            startActivity(Intent(this, TermsAndConditionActivity::class.java).putExtra("privacy", "other"))
        }

        mBinding.about.setOnClickListener {
            startActivity(
                Intent(this, WebPagesOpenActivity::class.java).putExtra(
                    "aboutUs",
                    "aboutUs"
                )
            )


//            val theWebPage = WebView(this)
//            theWebPage.settings.javaScriptEnabled = true
//            theWebPage.settings.pluginState = PluginState.ON
//            setContentView(theWebPage)
//            theWebPage.loadUrl("https://www.upscribbr.com")
        }


        mBinding.doneBtn.setOnClickListener {
            val fragment = MenuFragmentDelete(mViewModel, mBinding.progressBar)
            fragment.show(supportFragmentManager, fragment.tag)
        }

    }

    class MenuFragmentDelete(
        var mViewModel: ProfileViewModel,
        var progressBar: View
    ) :
        RoundedBottomSheetDialogFragment() {

        lateinit var imageViewTop: ImageView
        lateinit var yes: TextView
        lateinit var no: TextView

        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val view = inflater.inflate(R.layout.confirm_delete_upscribbr_account, container, false)
            imageViewTop = view.findViewById(R.id.imageViewTop)
            yes = view.findViewById(R.id.yes)
            no = view.findViewById(R.id.no)
            roundImage(imageViewTop)
            clickListeners()

            return view
        }




        private fun clickListeners() {
            no.setOnClickListener {
                dismiss()
            }

            yes.setOnClickListener {
                dismiss()
                val isSeller =
                    Constant.getSharedPrefs(activity!!).getBoolean(Constant.IS_SELLER, false)
                if (!isSeller) {
                    progressBar.visibility = View.VISIBLE
                    val profileData =
                        activity!!.intent.getParcelableExtra<ModelGetProfile>("modelProfile")
                    mViewModel.deleteCustomer(profileData.contact_no)
                }

            }

        }

        private fun roundImage(imageViewTop: ImageView) {
            val curveRadius = 50F

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                imageViewTop.outlineProvider = object : ViewOutlineProvider() {

                    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                    override fun getOutline(view: View?, outline: Outline?) {
                        outline?.setRoundRect(
                            0,
                            0,
                            view!!.width,
                            view.height + curveRadius.toInt(),
                            curveRadius
                        )
                    }
                }
                imageViewTop.clipToOutline = true

            }
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 1) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    try {
                        val place = Autocomplete.getPlaceFromIntent(data!!)
                        val latLng = place.latLng
                        latitude1 = latLng!!.latitude
                        longitude1 = latLng.longitude
                        val geocoder = Geocoder(this)
                        val addresses =
                            latLng.latitude.let {
                                geocoder.getFromLocation(
                                    it,
                                    latLng.longitude,
                                    1
                                )
                            }
                        val locationModel = LocationModel()
                        locationModel.address = addresses[0].getAddressLine(0)
                        locationModel.city = addresses[0].locality
                        locationModel.state = addresses[0].adminArea
                        locationModel.latitude = latitude1.toString()
                        locationModel.longitude = longitude1.toString()
                        locationModel.street = addresses[0].thoroughfare
                        locationModel.zipcode = addresses[0].postalCode
                        startActivity(
                            Intent(
                                this,
                                ChangeLocationSetingsActivity::class.java
                            ).putExtra("profile", "123").putExtra("location", locationModel)
                        )
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                }
            }
        }
    }

    private fun setToolbar() {
        setSupportActionBar(mBinding.include13.toolbar)
        mBinding.include13.title.text = "Settings"
        mBinding.include13.toolbar.setBackgroundColor(resources.getColor(R.color.editBack_button))
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)
        title = ""

    }

    override fun onResume() {
        super.onResume()
        val sharedPreferences1 = getSharedPreferences("updateProfile", Context.MODE_PRIVATE)
        if (!sharedPreferences1.getString("profile", "").isEmpty()) {
            val editor = sharedPreferences1.edit()
            editor.remove("profile")
            editor.apply()
            finish()
        }



        if (!sharedPreferences1.getString("profilePass", "").isEmpty()) {
            val editor1 = sharedPreferences1.edit()
            editor1.remove("profilePass")
            editor1.apply()
            finish()
        }

    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}
