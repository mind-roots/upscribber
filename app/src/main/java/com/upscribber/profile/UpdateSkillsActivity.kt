package com.upscribber.profile

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.MotionEvent
import android.view.View
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.databinding.ActivityUpdateSkillsBinding
import kotlinx.android.synthetic.main.activity_update_skills.*

class UpdateSkillsActivity : AppCompatActivity() {

    lateinit var mBinding: ActivityUpdateSkillsBinding
    lateinit var mViewModel: ProfileViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_update_skills)
        mViewModel = ViewModelProviders.of(this)[ProfileViewModel::class.java]
        setToolbar()
        setData()
        clickListeners()
        observerInit()
    }

    private fun setData() {
        if (intent.hasExtra("modelProfile")) {
            val modelProfile = intent.getParcelableExtra<ModelGetProfile>("modelProfile")
            if (intent.hasExtra("twitter")) {
                mBinding.editTxtUpdateProfile.setText(modelProfile.twitter)
                editTxtUpdateProfile.hint = "Enter your Details"
                imageView40.visibility = View.GONE
                titleToolbar.visibility = View.GONE
                titleToolbarMessage.visibility = View.GONE
            } else if (intent.hasExtra("instagram")) {
                mBinding.editTxtUpdateProfile.setText(modelProfile.instagram)
                editTxtUpdateProfile.hint = "Enter your Details"
                imageView40.visibility = View.GONE
                titleToolbar.visibility = View.GONE
                titleToolbarMessage.visibility = View.GONE
            } else if (intent.hasExtra("facebook")) {
                mBinding.editTxtUpdateProfile.setText(modelProfile.facebook)
                imageView40.visibility = View.GONE
                titleToolbar.visibility = View.GONE
                titleToolbarMessage.visibility = View.GONE
                editTxtUpdateProfile.hint = "Enter your Details"
            } else if (intent.hasExtra("modelProfileBusinessInfo")) {
                editTxtUpdateProfile.hint = "Tell us about ur Business"
                titleToolbarMessage.hint = "Provide your Business info here"
                mBinding.editTxtUpdateProfile.setText(modelProfile.about)
                imageView40.visibility = View.VISIBLE
                titleToolbar.visibility = View.VISIBLE
                titleToolbarMessage.visibility = View.VISIBLE
                imageView40.setImageDrawable(resources.getDrawable(R.drawable.ic_business_info_img))
            } else {
                titleToolbarMessage.hint = "Add your specialties, skills and certifications"
                titleToolbar.visibility = View.VISIBLE
                titleToolbarMessage.visibility = View.VISIBLE
                imageView40.setImageDrawable(resources.getDrawable(R.drawable.ic_upload_skills))
                imageView40.visibility = View.VISIBLE
                editTxtUpdateProfile.hint = "example, if you are a barber, you can say, “Straight razor shave, beard shaping, shear cut” or if you are a massage therapist, you can say “Swedish massage, pediatric massage"
                mBinding.editTxtUpdateProfile.setText(modelProfile.speciality)
            }
        }
    }

    private fun observerInit() {
        mViewModel.getmDataUpdateSkill().observe(this, Observer {
            if (it.status == "true") {
                mBinding.progressBar28.visibility = View.GONE
                val sharedPreferences = getSharedPreferences("updateProfile", Context.MODE_PRIVATE)
                val editor = sharedPreferences.edit()
                editor.putString("profile", "back")
                editor.apply()

                when {
                    intent.hasExtra("modelProfileBusinessInfo") -> {
                        intent.putExtra("status", it.status)
                        setResult(902, intent)
                        finish()

                    }
                    intent.hasExtra("getData") -> {
                        intent.putExtra("status", it.status)
                        setResult(904, intent)
                        finish()

                    }
                    else -> {
                        editor.putString("social", "data")
                        editor.apply()
                        finish()
                    }
                }

            }
        })

        mViewModel.getStatus().observe(this, androidx.lifecycle.Observer {
            mBinding.progressBar28.visibility = View.GONE
            if (it.msg == "Invalid auth code") {
                Constant.commonAlert(this)
            } else {
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
            }
        })

    }

    private fun clickListeners() {
        var valueTwitter = ""
        var instagram = ""
        var valueFacebook = ""
        var valueAbout = ""
        if (intent.hasExtra("twitter")) {
            valueTwitter = intent.getStringExtra("twitter")
        }

        if (intent.hasExtra("instagram")) {
            instagram = intent.getStringExtra("instagram")
        }

        if (intent.hasExtra("facebook")) {
            valueFacebook = intent.getStringExtra("facebook")
        }

        if (intent.hasExtra("modelProfileBusinessInfo")) {
            valueAbout = intent.getStringExtra("modelProfileBusinessInfo")
        }


        val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
        val type = Constant.getPrefs(this).getString(Constant.type, "")
        mBinding.saveButton.setOnClickListener {
            if (mBinding.editTxtUpdateProfile.text.isEmpty()) {
                Toast.makeText(this, "Please Enter your details!", Toast.LENGTH_SHORT).show()
            } else if (intent.hasExtra("twitter")) {
                mBinding.progressBar28.visibility = View.VISIBLE
                mViewModel.updateSkills(
                    auth,
                    type,
                    mBinding.editTxtUpdateProfile.text.toString().trim(),
                    valueTwitter
                )
            } else if (intent.hasExtra("instagram")) {
                mBinding.progressBar28.visibility = View.VISIBLE
                mViewModel.updateSkills(
                    auth,
                    type,
                    mBinding.editTxtUpdateProfile.text.toString().trim(), instagram
                )
            } else if (intent.hasExtra("facebook")) {
                mBinding.progressBar28.visibility = View.VISIBLE
                mViewModel.updateSkills(
                    auth,
                    type,
                    mBinding.editTxtUpdateProfile.text.toString().trim(),
                    valueFacebook
                )
            } else if (intent.hasExtra("modelProfileBusinessInfo")) {
                mBinding.progressBar28.visibility = View.VISIBLE
                mViewModel.updateSkills(
                    auth,
                    "1",
                    mBinding.editTxtUpdateProfile.text.toString().trim(),
                    valueAbout
                )
            } else {
                mBinding.progressBar28.visibility = View.VISIBLE
                mViewModel.updateSkills(
                    auth,
                    type,
                    mBinding.editTxtUpdateProfile.text.toString().trim(),
                    ""
                )
            }
        }


        editTxtUpdateProfile.setOnTouchListener { view, event ->
            view.parent.requestDisallowInterceptTouchEvent(true)
            if ((event.action and MotionEvent.ACTION_MASK) == MotionEvent.ACTION_UP) {
                view.parent.requestDisallowInterceptTouchEvent(false)
            }
            return@setOnTouchListener false
        }


    }

    private fun setToolbar() {
        setSupportActionBar(mBinding.include13.toolbar)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)
        title = ""

        if (intent.hasExtra("twitter")) {
            mBinding.include13.title.text = "Update Social"
        } else if (intent.hasExtra("instagram")) {
            mBinding.include13.title.text = "Update Social"
        } else if (intent.hasExtra("facebook")) {
            mBinding.include13.title.text = "Update Social"
        } else if (intent.hasExtra("modelProfileBusinessInfo")) {
            mBinding.titleToolbar.text = "Business Info"
        } else {
            mBinding.titleToolbar.text = "Update Skills"
        }


    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}
