package com.upscribber.profile

import android.os.Parcel
import android.os.Parcelable

class ModelBillingAddress() : Parcelable {
    var billing_suite: String = ""
    var billing_street: String = ""
    var billing_city: String = ""
    var billing_state: String = ""
    var billing_zipcode: String = ""

    constructor(parcel: Parcel) : this() {
        billing_suite = parcel.readString()
        billing_street = parcel.readString()
        billing_city = parcel.readString()
        billing_state = parcel.readString()
        billing_zipcode = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(billing_suite)
        parcel.writeString(billing_street)
        parcel.writeString(billing_city)
        parcel.writeString(billing_state)
        parcel.writeString(billing_zipcode)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ModelBillingAddress> {
        override fun createFromParcel(parcel: Parcel): ModelBillingAddress {
            return ModelBillingAddress(parcel)
        }

        override fun newArray(size: Int): Array<ModelBillingAddress?> {
            return arrayOfNulls(size)
        }
    }

}
