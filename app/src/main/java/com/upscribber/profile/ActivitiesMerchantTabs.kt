package com.upscribber.profile

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.payment.customerPayments.PaymentsViewModel
import com.upscribber.upscribberSeller.customerBottom.profile.activity.ActivityModel
import kotlinx.android.synthetic.main.activity_activities_merchant_tabs.*

@SuppressLint("Registered")
class ActivitiesMerchantTabs : AppCompatActivity() ,ActivityMerchantAdapter.MyActivities{

    private lateinit var mAdapterPayments: ActivityMerchantAdapter
    lateinit var  toolbar : Toolbar
    lateinit var  toolbartitle : TextView
    lateinit var  mViewModel : PaymentsViewModel
    var tabPosition = "1"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_activities_merchant_tabs)
        mViewModel = ViewModelProviders.of(this)[PaymentsViewModel::class.java]
        setToolbar()
        setAdapter()
        apiImplimentation()
        createTabs()
        observerInit()

    }

    private fun observerInit() {
        mViewModel.getActivitiesData().observe(this, Observer {
            progressBar.visibility = View.GONE
            if (it.size > 0) {
                noData.visibility = View.GONE
                imageView85.visibility = View.GONE
                noDataDescription.visibility = View.GONE
                recyclerView4.visibility = View.VISIBLE
//                tabLayout2.visibility = View.VISIBLE
//                view.visibility = View.VISIBLE
                mAdapterPayments.update(it)
            }else{
                imageView85.visibility = View.VISIBLE
                noDataDescription.visibility = View.VISIBLE
                noData.visibility = View.VISIBLE
//                tabLayout2.visibility = View.GONE
//                view.visibility = View.GONE
                recyclerView4.visibility = View.GONE
            }
        })

        mViewModel.getStatus().observe(this, Observer {
            progressBar.visibility = View.GONE
            if (it.msg == "Invalid auth code") {
                Constant.commonAlert(this)
            } else if(it.msg.isEmpty()){
                noData.visibility = View.VISIBLE
                imageView85.visibility = View.VISIBLE
                noDataDescription.visibility = View.VISIBLE
                recyclerView4.visibility = View.GONE
//                tabLayout2.visibility = View.VISIBLE
//                view.visibility = View.VISIBLE
            }else{
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
            }
        })

    }

    override fun getResult(model: ActivityModel) {
        startActivityForResult(Intent(this,ActivityMerchantDetailPage::class.java).putExtra("modelActivities", model),1)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == 1 ) {
            apiImplimentation()
        }



    }

    private fun apiImplimentation() {
        val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
        mViewModel.getAllActivities(auth, "2", tabPosition )
        progressBar.visibility = View.VISIBLE
    }

    private fun createTabs() {
        val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
        payments.setOnClickListener {
            tabPosition = "1"
            payments.setTextColor(resources.getColor(R.color.colorWhite))
            payments.setBackgroundResource(R.drawable.bg_seller_blue1_padding)
            refunds.setTextColor(resources.getColor(R.color.payments))
            cancelled.setTextColor(resources.getColor(R.color.payments))
            refunds.setBackgroundResource(R.drawable.bg_seller_grey_nonselected)
            cancelled.setBackgroundResource(R.drawable.bg_seller_grey_nonselected)
            mViewModel.getAllActivities(auth, "2", "1")
            progressBar.visibility = View.VISIBLE
        }

        refunds.setOnClickListener {
            tabPosition = "2"
            payments.setTextColor(resources.getColor(R.color.payments))
            payments.setBackgroundResource(R.drawable.bg_seller_grey_nonselected)
            refunds.setTextColor(resources.getColor(R.color.colorWhite))
            cancelled.setTextColor(resources.getColor(R.color.payments))
            refunds.setBackgroundResource((R.drawable.bg_seller_blue1_padding))
            cancelled.setBackgroundResource(R.drawable.bg_seller_grey_nonselected)
           mViewModel.getAllActivities(auth, "2", "2")
            progressBar.visibility = View.VISIBLE

        }

        cancelled.setOnClickListener {
            tabPosition = "3"
            payments.setTextColor(resources.getColor(R.color.payments))
            payments.setBackgroundResource(R.drawable.bg_seller_grey_nonselected)
            refunds.setTextColor(resources.getColor(R.color.payments))
            cancelled.setTextColor(resources.getColor(R.color.colorWhite))
            refunds.setBackgroundResource(R.drawable.bg_seller_grey_nonselected)
            cancelled.setBackgroundResource((R.drawable.bg_seller_blue1_padding))
            mViewModel.getAllActivities(auth, "2", "3")
            progressBar.visibility = View.VISIBLE
        }


    }

    private fun setAdapter() {
        recyclerView4.layoutManager = LinearLayoutManager(this)
        mAdapterPayments = ActivityMerchantAdapter(this,  "")
        recyclerView4.adapter = mAdapterPayments

    }

    private fun setToolbar() {
        toolbar = findViewById(R.id.toolbar)
        toolbartitle = findViewById(R.id.title)
        setSupportActionBar(toolbar)
        title = ""
        toolbartitle.text = "Activity"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)
    }



    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}
