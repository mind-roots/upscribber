package com.upscribber.profile

import com.upscribber.home.ModelHomeLocation

class ModelMainProfile {

    var modelGetProfileCustomer : ModelGetProfile = ModelGetProfile()
    var modelGetProfileMerchant : ArrayList<ModelHomeLocation> = ArrayList()
    var status : String = ""
    var msg : String = ""

}
