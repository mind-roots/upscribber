package com.upscribber.profile

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.location.Address
import android.location.Geocoder
import android.location.Location
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.upscribber.R
import com.upscribber.becomeseller.seller.ModelAddress
import com.upscribber.businessAddress.ManualBusinessAddress
import com.upscribber.commonClasses.Constant
import com.upscribber.databinding.ActivityChangeLocationSetingsBinding
import com.upscribber.filters.LocationModel
import com.upscribber.requestbusiness.RequestBusinessFragment2

class ChangeLocationSetingsActivity : AppCompatActivity(), OnMapReadyCallback,
    GoogleMap.OnMarkerClickListener {

    lateinit var mBinding: ActivityChangeLocationSetingsBinding
    private var addresses: MutableList<Address> = ArrayList()
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var mMap: GoogleMap
    private lateinit var lastLocation: Location
    val locationModel = LocationModel()
    var latitudesource = 1.0
    var longitudesource = 1.0
    lateinit var mViewModel: ProfileViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_change_location_setings)
        mViewModel = ViewModelProviders.of(this)[ProfileViewModel::class.java]
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        Places.initialize(applicationContext, resources.getString(R.string.google_maps_key))
        clickListeners()
        setData()
    }

    private fun setData() {
        if (intent.hasExtra("location")) {
            val modelLocation = intent.getParcelableExtra<LocationModel>("location")
            mBinding.searchView.text = modelLocation.address
            mBinding.savedAddress.text = modelLocation.address
            val latitude1 = modelLocation.latitude.toDouble()
            val longitude1 = modelLocation.longitude.toDouble()
            val geocoder = Geocoder(this)
            addresses = latitude1.let { geocoder.getFromLocation(it, longitude1, 1) }
            Handler().postDelayed({
                placeMarkerOnMap(LatLng(latitude1, longitude1))
                mMap.animateCamera(
                    CameraUpdateFactory.newLatLngZoom(
                        LatLng(latitude1, longitude1),
                        12f
                    )
                )
            }, 200)

            try {
                if (addresses.isNotEmpty()) {
                        mBinding.searchView.text = modelLocation.address
                        mBinding.savedAddress.text = modelLocation.address
                        locationModel.address = modelLocation.address
                        locationModel.city = modelLocation.city
                        locationModel.state = modelLocation.state
                        locationModel.zipcode = modelLocation.zipcode
                        locationModel.latitude = modelLocation.latitude
                        locationModel.longitude = modelLocation.longitude
                        locationModel.street = modelLocation.street
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }else if (intent.hasExtra("locationn")) {
            val modelLocation = intent.getParcelableExtra<LocationModel>("locationn")
            val latitude1 = modelLocation.billingLat.toDouble()
            val longitude1 = modelLocation.billingLong.toDouble()
            val geocoder = Geocoder(this)
            addresses = latitude1.let { geocoder.getFromLocation(it, longitude1, 1) }
            Handler().postDelayed({
                placeMarkerOnMap(LatLng(latitude1, longitude1))
                mMap.animateCamera(
                    CameraUpdateFactory.newLatLngZoom(
                        LatLng(latitude1, longitude1),
                        12f
                    )
                )
            }, 200)

            try {
                if (addresses.isNotEmpty()) {
                        mBinding.searchView.text = modelLocation.billingAddress
                        mBinding.savedAddress.text = modelLocation.billingAddress
                        locationModel.billingAddress = modelLocation.billingAddress
                        locationModel.billingCity = modelLocation.billingCity
                        locationModel.billingState = modelLocation.billingState
                        locationModel.billingZip = modelLocation.billingZip
                        locationModel.billingLat = modelLocation.billingLat
                        locationModel.billingLong = modelLocation.billingLong
                        locationModel.billingStreet = modelLocation.billingStreet
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

    }

    private fun clickListeners() {
        mBinding.backOne.setOnClickListener {
            finish()
        }

        mBinding.update.setOnClickListener {
            if (locationModel.address != "" ||locationModel.billingAddress != "") {
                if (intent.hasExtra("profile")) {
                    val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
                    mViewModel.setLocation(
                        auth,
                        locationModel.street,
                        locationModel.state,
                        locationModel.city,
                        locationModel.zipcode,
                        "1"
                    )

                } else if (intent.hasExtra("seller")) {
                    startActivityForResult(Intent(this, ManualBusinessAddress::class.java)
                        .putExtra("modelLocation", locationModel)
                        .putExtra("latitude", latitudesource.toString())
                        .putExtra("longitude", longitudesource.toString()),20)
                }else if (intent.hasExtra("sellerBilling")) {
                    startActivityForResult(Intent(this, ManualBusinessAddress::class.java)
                        .putExtra("modelLocationn", locationModel)
                        .putExtra("latitude", latitudesource.toString())
                        .putExtra("longitude", longitudesource.toString()),21)
                } else {
                    setResult(
                        101, intent.putExtra("locationModel", locationModel)
                            .putExtra("latitude", latitudesource.toString())
                            .putExtra("longitude", longitudesource.toString())
                    )
                    finish()
                }


            } else {
                finish()
            }
        }

        mViewModel.getStatusSuccessful().observe(this, Observer {
            if (it.status == "true") {
                finish()

            }
        })
        mBinding.searchView.setOnClickListener {
            Constant.hideKeyboard(this, mBinding.searchView)
            Places.createClient(this)
            val fields =
                listOf(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG)
            // Start the autocomplete intent.
            val intent = Autocomplete.IntentBuilder(
                AutocompleteActivityMode.FULLSCREEN, fields
            )
                .build(this)
            startActivityForResult(intent, 1)
        }


    }


    override fun onMapReady(p0: GoogleMap?) {
        mMap = p0!!
        mMap.uiSettings.isCompassEnabled = true
        setUpMap()


        mMap.setOnMapClickListener { latlng ->
            latitudesource = latlng.latitude
            longitudesource = latlng.longitude
            mMap.clear()
            placeMarkerOnMap(latlng)
        }

    }


    private fun setUpMap() {
        if (ActivityCompat.checkSelfPermission(
                this,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                RequestBusinessFragment2.LOCATION_PERMISSION_REQUEST_CODE
            )
            return
        }
        if (intent.hasExtra("latitude") && intent.hasExtra("longitude")) {
            try {
                latitudesource = intent.getStringExtra("latitude").toDouble()
                longitudesource = intent.getStringExtra("longitude").toDouble()
                val currentLatLng = LatLng(latitudesource, longitudesource)
                placeMarkerOnMap(currentLatLng)
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 12f))
                val geocoder = Geocoder(this)
                val addresses =
                    latitudesource.let { geocoder.getFromLocation(it, longitudesource, 1) }
                if (null != addresses && addresses.isNotEmpty()) {
                    locationModel.address =
                        addresses[0].getAddressLine(0)
                    locationModel.city = addresses[0].locality
                    locationModel.state = addresses[0].adminArea
                    locationModel.zipcode = addresses[0].postalCode
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        } else {
            fusedLocationClient.lastLocation.addOnSuccessListener(this) { location ->
                // Got last known location. In some rare situations this can be null.
                if (location != null) {
                    latitudesource = location.latitude
                    longitudesource = location.longitude
                    lastLocation = location
                    val currentLatLng = LatLng(location.latitude, location.longitude)
                    placeMarkerOnMap(currentLatLng)
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 12f))
                }
            }
        }
    }

    private fun placeMarkerOnMap(currentLatLng: LatLng) {
        mMap.addMarker(
            MarkerOptions()
                .position(currentLatLng)
                .icon(bitmapDescriptorFromVector(this, R.drawable.ic_location_blue_icon))
        )
//        val geocoder = Geocoder(this)
//        val addresses =
//            currentLatLng.latitude.let { geocoder.getFromLocation(it, currentLatLng.longitude, 1) }
//        try {
//            if (addresses.isNotEmpty()) {
//
//                locationModel.address = addresses[0].getAddressLine(0)
//                mBinding.savedAddress.text = locationModel.address
//                mBinding.searchView.text = locationModel.address
//                locationModel.city = addresses[0].subAdminArea
//                locationModel.state = addresses[0].adminArea
//                locationModel.zipcode = addresses[0].postalCode
//
//            }
//        } catch (e: Exception) {
//            e.printStackTrace()
//        }

    }

    private fun bitmapDescriptorFromVector(context: Context, vectorResId: Int): BitmapDescriptor {
        val vectorDrawable = ContextCompat.getDrawable(context, vectorResId)
        vectorDrawable!!.setBounds(
            0,
            0,
            vectorDrawable.intrinsicWidth,
            vectorDrawable.intrinsicHeight
        )
        val bitmap =
            Bitmap.createBitmap(
                vectorDrawable.intrinsicWidth,
                vectorDrawable.intrinsicHeight,
                Bitmap.Config.ARGB_8888
            )
        val canvas = Canvas(bitmap)
        vectorDrawable.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }

    override fun onMarkerClick(p0: Marker?): Boolean {
        mMap.uiSettings.isZoomControlsEnabled = true
        mMap.setOnMarkerClickListener(this)
        return true

    }

    @SuppressLint("SetTextI18n")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    try {
                        val place = Autocomplete.getPlaceFromIntent(data!!)
                        val latLng = place.latLng
                        val latitude = latLng!!.latitude
                        val longitude = latLng.longitude
                        latitudesource = latitude
                        longitudesource = longitude
                        val geocoder = Geocoder(this)
                        val address = ""
                        val addresses = latitude.let { geocoder.getFromLocation(it, longitude, 1) }
                        try {
                            if (null != addresses && addresses.isNotEmpty()) {
                                locationModel.address = addresses[0].getAddressLine(0)
                                locationModel.city = addresses[0].locality
                                locationModel.state = addresses[0].adminArea
                                locationModel.zipcode = addresses[0].postalCode
                                locationModel.street = addresses[0].thoroughfare
                                mBinding.savedAddress.text = address
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                        mBinding.savedAddress.text = locationModel.address
                        mMap.clear()
                        placeMarkerOnMap(place.latLng!!)
                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(place.latLng!!, 12f))
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                }
            }
        }


        if (resultCode == 20){
            val address = data!!.getParcelableExtra<ModelAddress>("address")
            val intent = Intent()
            intent.putExtra("address", address)
            setResult(20, intent)
            finish()

        }
        if (resultCode == 21){
            val address = data!!.getParcelableExtra<ModelAddress>("address")
            val intent = Intent()
            intent.putExtra("address", address)
            setResult(21, intent)
            finish()

        }
    }
}
