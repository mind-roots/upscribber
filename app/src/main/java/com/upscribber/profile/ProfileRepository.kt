package com.upscribber.profile

import android.app.Application
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.upscribber.commonClasses.Constant
import com.upscribber.commonClasses.ModelStatusMsg
import com.upscribber.commonClasses.WebServicesCustomers
import com.upscribber.home.ModelHomeLocation
import com.upscribber.payment.paymentCards.PaymentCardModel
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import java.io.File
import kotlin.Exception

class ProfileRepository(var application: Application) {


    private val mDataupdatePassword = MutableLiveData<ModelUpdatePassword>()
    val mDataFailure = MutableLiveData<ModelStatusMsg>()
    val mDataVerifyEmail = MutableLiveData<ModelStatusMsg>()
    val mDataDeleteCustomet = MutableLiveData<ModelStatusMsg>()
    val mSuccessfull = MutableLiveData<ModelStatusMsg>()
    val mSuccessfullImage = MutableLiveData<ModelStatusMsg>()
    val mData = MutableLiveData<ModelMainProfile>()
    val mDataUpdate = MutableLiveData<ModelGetProfile>()
    val mDataUpdateSkills = MutableLiveData<ModelGetProfile>()
    val mDataStripeDataCard = MutableLiveData<ArrayList<PaymentCardModel>>()
    val mDataStripeDataBank = MutableLiveData<ArrayList<PaymentCardModel>>()
    private val mLogout = MutableLiveData<Boolean>()


    fun getProfileData(auth: String, type: String, viewType: String, viewId: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.getProfile(auth, type, viewType, viewId)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")

                        val modelStatus = ModelStatusMsg()
                        val modelGetProfile = ModelGetProfile()
                        val modelProfile = ModelMainProfile()
                        val arrayLocation = ArrayList<ModelHomeLocation>()

                        if (status == "true") {
                            val data = json.getJSONObject("data")
                            getBankCardDetails(data.optString("stripe_account_id"), "card")
                            getBankCardDetails(data.optString("stripe_account_id"), "bank_account")
                            if (type == "0") {
                                val primary_address = data.optJSONObject("primary_address")
                                val billing_address = data.optJSONObject("billing_address")
                                modelGetProfile.profile_image = data.optString("profile_image")
                                modelGetProfile.notification_count = data.optString("notification_count")
                                modelGetProfile.zip_code = data.optString("zip_code")
                                modelGetProfile.name = data.optString("name")
                                modelGetProfile.contact_no = data.optString("contact_no")
                                modelGetProfile.email = data.optString("email")
                                modelGetProfile.gender = data.optString("gender")
                                modelGetProfile.stripe_customer_id = data.optString("stripe_customer_id")
                                modelGetProfile.birthdate = data.optString("birthdate")
                                modelGetProfile.reg_type = data.optString("reg_type")
                                modelGetProfile.last_name = data.optString("last_name")
                                modelGetProfile.is_merchant = data.optString("is_merchant")
                                modelGetProfile.steps = data.optString("steps")
                                modelGetProfile.push_setting = data.optString("push_setting")
                                modelGetProfile.tax = data.optInt("tax")
                                modelGetProfile.business_id = data.optString("business_id")
                                modelGetProfile.email_verify = data.optString("email_verify")
                                modelGetProfile.invite_code = data.optString("invite_code")
                                modelGetProfile.social_id = data.optString("social_id")
                                modelGetProfile.cash_earned = data.optString("cash_earned")
                                modelGetProfile.new_notification_status = data.optString("new_notification_status")
                                modelGetProfile.invite_count = data.optString("invite_count")
                                modelGetProfile.merchant_commission = data.optInt("merchant_commission")
                                modelGetProfile.type = data.optString("type")
                                modelGetProfile.primary_address.street = primary_address.optString("street")
                                modelGetProfile.primary_address.city = primary_address.optString("city")
                                modelGetProfile.primary_address.state = primary_address.optString("state")
                                modelGetProfile.primary_address.zip_code = primary_address.optString("zip_code")
                                modelGetProfile.billing_address.billing_street = billing_address.optString("billing_street")
                                modelGetProfile.billing_address.billing_city =billing_address.optString("billing_city")
                                modelGetProfile.billing_address.billing_state = billing_address.optString("billing_state")
                                modelGetProfile.billing_address.billing_zipcode = billing_address.optString("billing_zipcode")
                                modelStatus.status = status
                                modelStatus.msg = msg
                                modelProfile.modelGetProfileCustomer = modelGetProfile
                                val editor = Constant.getPrefs(application).edit()
                                editor.putString(Constant.type, modelGetProfile.type)
                                editor.putString(Constant.regtrationType, modelGetProfile.reg_type)
                                editor.putString(Constant.push_setting, modelGetProfile.push_setting)
                                editor.putString(Constant.new_notification_status, modelGetProfile.new_notification_status)
                                editor.putString(Constant.dataProfile, data.toString())
                                editor.apply()

                            } else if (type == "1") {
                                val location = data.optJSONArray("location")
                                modelGetProfile.profile_image = data.optString("profile_image")
                                modelGetProfile.contact_no = data.optString("contact_no")
                                modelGetProfile.email = data.optString("email")
                                modelGetProfile.notification_count = data.optString("notification_count")
                                modelGetProfile.business_id = data.optString("business_id")
                                modelGetProfile.business_name = data.optString("business_name")
                                modelGetProfile.business_phone = data.optString("business_phone")
                                modelGetProfile.speciality = data.optString("speciality")
                                modelGetProfile.business_email = data.optString("business_email")
                                modelGetProfile.facebook = data.optString("facebook")
                                modelGetProfile.instagram = data.optString("instagram")
                                modelGetProfile.schedule = data.optString("schedule")
                                modelGetProfile.twitter = data.optString("twitter")
                                modelGetProfile.payment_id = data.optString("payment_id")
                                modelGetProfile.payment_method = data.optString("payment_method")
                                modelGetProfile.stripe_status = data.optString("stripe_status")
                                modelGetProfile.stripe_account_id = data.optString("stripe_account_id")
                                modelGetProfile.payment_method_available = data.optString("payment_method_available")
                                modelGetProfile.selected_payout_method = data.optString("selected_payout_method")
                                modelGetProfile.available_payout = data.optString("available_payout")
                                modelGetProfile.payout_date = data.optString("payout_date")
                                modelGetProfile.about = data.optString("about")
                                modelGetProfile.push_setting = data.optString("push_setting")
                                modelGetProfile.new_notification_status = data.optString("new_notification_status")

                                for (i in 0 until location.length()) {
                                    val locionData = location.optJSONObject(i)
                                    val modeLocation = ModelHomeLocation()
                                    modeLocation.id = locionData.optString("id")
                                    modeLocation.business_id = locionData.optString("business_id")
                                    modeLocation.user_id = locionData.optString("user_id")
                                    modeLocation.street = locionData.optString("street")
                                    modeLocation.city = locionData.optString("city")
                                    modeLocation.state = locionData.optString("state")
                                    modeLocation.zip_code = locionData.optString("zip_code")
                                    modeLocation.lat = locionData.optString("lat")
                                    modeLocation.long = locionData.optString("long")
                                    modeLocation.store_timing = locionData.optString("store_timing")
                                    modeLocation.created_at = locionData.optString("created_at")
                                    modeLocation.updated_at = locionData.optString("updated_at")
                                    arrayLocation.add(modeLocation)
                                }
                                modelProfile.modelGetProfileCustomer = modelGetProfile
                                modelProfile.modelGetProfileMerchant = arrayLocation

                                val editor = Constant.getPrefs(application).edit()
//                                editor.putString(Constant.type, modelGetProfile.type)
                                editor.putString(Constant.regtrationType, modelGetProfile.reg_type)
                                editor.putString(Constant.push_setting, modelGetProfile.push_setting)
                                editor.putString(Constant.new_notification_status, modelGetProfile.new_notification_status)
//                                editor.putString(Constant.dataProfile, data.toString())
                                editor.apply()



                            } else if (type == "2") {
                                val locationIndividual = data.getJSONArray("location")
                                modelGetProfile.profile_image = data.optString("profile_image")
                                modelGetProfile.name = data.optString("name")
                                modelGetProfile.contact_no = data.optString("contact_no")
                                modelGetProfile.email = data.optString("email")
                                modelGetProfile.notification_count = data.optString("notification_count")
                                modelGetProfile.speciality = data.optString("speciality")
                                modelGetProfile.business_name = data.optString("business_name")
                                modelGetProfile.business_id = data.optString("business_id")
                                modelGetProfile.logo = data.optString("logo")
                                modelGetProfile.facebook = data.optString("facebook")
                                modelGetProfile.instagram = data.optString("instagram")
                                modelGetProfile.twitter = data.optString("twitter")
                                modelGetProfile.schedule = data.optString("schedule")
                                modelGetProfile.new_notification_status = data.optString("new_notification_status")
                                modelGetProfile.push_setting = data.optString("push_setting")
                                modelGetProfile.last_associated_id =
                                    data.optString("last_associated_id")
                                modelGetProfile.merchant_contact_no =
                                    data.optString("merchant_contact_no")
                                modelGetProfile.about = data.optString("about")
                                modelGetProfile.payment_method = data.optString("payment_method")
                                modelGetProfile.stripe_account_id =
                                    data.optString("stripe_account_id")
                                modelGetProfile.payment_method_available =
                                    data.optString("payment_method_available")
                                modelGetProfile.selected_payout_method =
                                    data.optString("selected_payout_method")
                                modelGetProfile.available_payout =
                                    data.optString("available_payout")
                                modelGetProfile.payout_date = data.optString("payout_date")

                                for (i in 0 until locationIndividual.length()) {
                                    val locionData = locationIndividual.optJSONObject(i)
                                    val modeLocation = ModelHomeLocation()
                                    modeLocation.id = locionData.optString("id")
                                    modeLocation.street = locionData.optString("street")
                                    modeLocation.city = locionData.optString("city")
                                    modeLocation.state = locionData.optString("state")
                                    modeLocation.zip_code = locionData.optString("zip_code")
                                    modeLocation.lat = locionData.optString("lat")
                                    modeLocation.long = locionData.optString("long")
                                    arrayLocation.add(modeLocation)
                                }


                                val editor = Constant.getPrefs(application).edit()
//                                editor.putString(Constant.type, modelGetProfile.type)
                                editor.putString(Constant.regtrationType, modelGetProfile.reg_type)
                                editor.putString(Constant.push_setting, modelGetProfile.push_setting)
//                                editor.putString(Constant.dataProfile, data.toString())
                                editor.putString(Constant.new_notification_status, modelGetProfile.new_notification_status)
                                editor.putString(Constant.last_associated_id, modelGetProfile.last_associated_id)
                                editor.apply()


                                modelProfile.modelGetProfileCustomer = modelGetProfile
                                modelProfile.modelGetProfileMerchant = arrayLocation
                            } else if (type == "3") {

                                val locationTeam = data.getJSONArray("location")
                                modelGetProfile.profile_image = data.optString("profile_image")
                                modelGetProfile.name = data.optString("name")
                                modelGetProfile.contact_no = data.optString("contact_no")
                                modelGetProfile.email = data.optString("email")
                                modelGetProfile.notification_count = data.optString("notification_count")
                                modelGetProfile.speciality = data.optString("speciality")
                                modelGetProfile.business_name = data.optString("business_name")
                                modelGetProfile.business_phone = data.optString("business_phone")
                                modelGetProfile.business_email = data.optString("business_email")
                                modelGetProfile.speciality = data.optString("speciality")
                                modelGetProfile.business_id = data.optString("business_id")
                                modelGetProfile.facebook = data.optString("facebook")
                                modelGetProfile.instagram = data.optString("instagram")
                                modelGetProfile.twitter = data.optString("twitter")
                                modelGetProfile.schedule = data.optString("schedule")
                                modelGetProfile.logo = data.optString("logo")
                                modelGetProfile.about = data.optString("about")
                                modelGetProfile.new_notification_status = data.optString("new_notification_status")
                                modelGetProfile.push_setting = data.optString("push_setting")

                                for (i in 0 until locationTeam.length()) {
                                    val locionData = locationTeam.optJSONObject(i)
                                    val modeLocation = ModelHomeLocation()
                                    modeLocation.id = locionData.optString("id")
                                    modeLocation.business_id = locionData.optString("business_id")
                                    modeLocation.user_id = locionData.optString("user_id")
                                    modeLocation.street = locionData.optString("street")
                                    modeLocation.city = locionData.optString("city")
                                    modeLocation.state = locionData.optString("state")
                                    modeLocation.zip_code = locionData.optString("zip_code")
                                    modeLocation.created_at = locionData.optString("created_at")
                                    modeLocation.updated_at = locionData.optString("updated_at")
                                    modeLocation.lat = locionData.optString("lat")
                                    modeLocation.long = locionData.optString("long")
                                    arrayLocation.add(modeLocation)
                                }

                                val editor = Constant.getPrefs(application).edit()
//                                editor.putString(Constant.type, modelGetProfile.type)
                                editor.putString(Constant.regtrationType, modelGetProfile.reg_type)
                                editor.putString(Constant.push_setting, modelGetProfile.push_setting)
                                editor.putString(Constant.last_associated_id, modelGetProfile.last_associated_id)
                                editor.putString(Constant.dataProfile, data.toString())
                                editor.apply()

                                modelProfile.modelGetProfileCustomer = modelGetProfile
                                modelProfile.modelGetProfileMerchant = arrayLocation
                            }

                            modelProfile.status = status
                            modelGetProfile.msg = msg

                            mData.value = modelProfile

                        } else {
                            modelStatus.msg = msg
                            mDataFailure.value = modelStatus
                        }

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus


            }


        })
    }


    private fun getBankCardDetails(stripeId: String, type: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.stripeTokenUrl)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.paymentCardBank(stripeId, type)
        call.enqueue(object : Callback<ResponseBody> {


            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val arrayData = ArrayList<PaymentCardModel>()
                        val cardModel = PaymentCardModel()
                        val data = json.optJSONArray("data")
                        for (i in 0 until data.length()) {
                            val stripeData = data.getJSONObject(i)

                            if (type == "card") {
                                cardModel.id = stripeData.optString("id")
                                cardModel.brand = stripeData.optString("brand")
                                cardModel.country = stripeData.optString("country")
                                cardModel.last4 = stripeData.optString("last4")
                            } else {
                                cardModel.id = stripeData.optString("id")
                                cardModel.account_holder_name =
                                    stripeData.optString("account_holder_name")
                                cardModel.account_holder_type =
                                    stripeData.optString("account_holder_type")
                                cardModel.bank_name = stripeData.optString("bank_name")
                                cardModel.routing_number = stripeData.optString("routing_number")
                                cardModel.last4 = stripeData.optString("last4")
                            }
                            arrayData.add(cardModel)
                        }
                        if (type == "card") {
                            mDataStripeDataCard.value = arrayData
                        } else {
                            mDataStripeDataBank.value = arrayData
                        }

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                Toast.makeText(application, "Error!", Toast.LENGTH_LONG).show()
            }
        })
    }

    fun getmDataFailure(): LiveData<ModelStatusMsg> {
        return mDataFailure
    }

    fun getmDataVerifyEmail(): LiveData<ModelStatusMsg> {
        return mDataVerifyEmail
    }


    fun getmDataDeleteCustomet(): LiveData<ModelStatusMsg> {
        return mDataDeleteCustomet
    }


    fun getmDataSuccessful(): LiveData<ModelStatusMsg> {
        return mSuccessfull
    }


    fun getmData(): LiveData<ModelMainProfile> {
        return mData
    }

    fun getmDataCard(): MutableLiveData<ArrayList<PaymentCardModel>> {
        return mDataStripeDataCard
    }

    fun getmDataBank(): MutableLiveData<ArrayList<PaymentCardModel>> {
        return mDataStripeDataBank
    }


    //    ---------------------------------------------------Update Profile-------------------------------------------------------------------------//
    fun updateProfileData(
        auth: String,
        type: String,
        name: String,
        email: String,
        phone: String,
        image: File?
    ) {
        try {
            val retrofit = Retrofit.Builder()
                .baseUrl(Constant.BASE_URL)
                .build()

            val service = retrofit.create(WebServicesCustomers::class.java)

            val file = image
            lateinit var call: Call<ResponseBody>
            if (file != null) {
                val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file)
                val body =
                    MultipartBody.Part.createFormData("profile_image", file.name, requestFile)
                val authPart = RequestBody.create(MediaType.parse("multipart/form-data"), auth)
                val typePart = RequestBody.create(MediaType.parse("multipart/form-data"), type)
                val namee = RequestBody.create(MediaType.parse("multipart/form-data"), name)
                val emaill = RequestBody.create(MediaType.parse("multipart/form-data"), email)
                val phone1 = RequestBody.create(MediaType.parse("multipart/form-data"), phone)

                call = service.updateProfile(authPart, typePart, namee, emaill, body, phone1)
            } else {
                val authPart = RequestBody.create(MediaType.parse("multipart/form-data"), auth)
                val typePart = RequestBody.create(MediaType.parse("multipart/form-data"), type)
                val namee = RequestBody.create(MediaType.parse("multipart/form-data"), name)
                val emaill = RequestBody.create(MediaType.parse("multipart/form-data"), email)
                val phone1 = RequestBody.create(MediaType.parse("multipart/form-data"), phone)

                call = service.updateProfile2(authPart, typePart, namee, emaill, phone1)
            }

            call.enqueue(object : Callback<ResponseBody> {

                override fun onResponse(
                    call: Call<ResponseBody>,
                    response: Response<ResponseBody>
                ) {
                    if (response.isSuccessful) {
                        try {
                            val res = response.body()!!.string()
                            val json = JSONObject(res)
                            val status = json.optString("status")
                            val msg = json.optString("msg")
                            val modelUpdateProfile = ModelGetProfile()
                            val modelStatusMsg = ModelStatusMsg()
                            if (status == "true") {
                                val data = json.optJSONObject("data")
                                modelUpdateProfile.profile_image = data.optString("profile_image")
                                modelUpdateProfile.is_merchant = data.optString("is_merchant")
                                modelUpdateProfile.name = data.optString("name")
                                modelUpdateProfile.contact_no = data.optString("contact_no")
                                modelUpdateProfile.email = data.optString("email")
                                modelUpdateProfile.gender = data.optString("gender")
                                modelUpdateProfile.stripe_customer_id =
                                    data.optString("stripe_customer_id")
                                modelUpdateProfile.tax = data.optInt("tax")
                                modelUpdateProfile.status = status
                                modelUpdateProfile.msg = msg
                                modelStatusMsg.msg = msg
                                modelStatusMsg.status = status
                                mDataUpdate.value = modelUpdateProfile

                            } else {
                                modelStatusMsg.msg = msg
                                mDataFailure.value = modelStatusMsg
                            }
//

                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                }

                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                    val modelStatus = ModelStatusMsg()
                    modelStatus.status = "false"
                    modelStatus.msg = "Network Error!"
                    mDataFailure.value = modelStatus


                }


            })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun getmDataUpdate(): LiveData<ModelGetProfile> {
        return mDataUpdate
    }


    //------------------------------------- Change Password Api ---------------------------- //
    fun updatePassword(auth: String, oldPass: String, newPass: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.updatePassword(auth, oldPass, newPass)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelUpdatePassword = ModelUpdatePassword()
                        val modelStatusMsg = ModelStatusMsg()
                        if (status == "true") {
                            val data = json.optJSONObject("data")
                            modelUpdatePassword.status = status
                            modelUpdatePassword.msg = msg
                            modelStatusMsg.msg = msg
                            modelStatusMsg.status = status
                        } else {
                            modelStatusMsg.msg = msg
                        }
                        mDataupdatePassword.value = modelUpdatePassword
                        mDataFailure.value = modelStatusMsg
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus
            }
        })

    }

    fun getmDataPassword(): LiveData<ModelUpdatePassword> {
        return mDataupdatePassword
    }


//    -----------------------------------------------Logout------------------------------------------------------//

    fun logout(authCode: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.logout(authCode)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {

                mLogout.value = true

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                mLogout.value = true
            }

        })
    }

    fun logoutObserver(): LiveData<Boolean> {
        return mLogout
    }

    fun updateSkills(auth: String, type: String, skills: String, value: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()
        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody>
        call = when (value) {
            "twitter" -> {
                service.updateTwitter(auth, type, skills)
            }
            "instagram" -> {
                service.updateInstagram(auth, type, skills)
            }
            "facebook" -> {
                service.updateFacebook(auth, type, skills)
            }
            "modelProfileBusinessInfo" -> {
                service.updateBusinessInfo(auth, type, skills)
            }
            else -> {
                service.updateSkills(auth, type, skills)
            }
        }

        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelUpdateProfile = ModelGetProfile()
                        val modelStatusMsg = ModelStatusMsg()
                        if (status == "true") {
                            val data = json.optJSONObject("data")
                            modelUpdateProfile.profile_image = data.optString("profile_image")
                            modelUpdateProfile.is_merchant = data.optString("is_merchant")
                            modelUpdateProfile.name = data.optString("name")
                            modelUpdateProfile.contact_no = data.optString("contact_no")
                            modelUpdateProfile.email = data.optString("email")
                            modelUpdateProfile.gender = data.optString("gender")
                            modelUpdateProfile.stripe_customer_id =
                                data.optString("stripe_customer_id")
                            modelUpdateProfile.tax = data.optInt("tax")
                            modelUpdateProfile.twitter = data.optString("twitter")
                            modelUpdateProfile.facebook = data.optString("facebook")
                            modelUpdateProfile.about = data.optString("about")
                            modelUpdateProfile.instagram = data.optString("instagram")
                            modelUpdateProfile.status = status
                            modelUpdateProfile.msg = msg
                            modelStatusMsg.msg = msg
                            modelStatusMsg.status = status
                            mDataUpdateSkills.value = modelUpdateProfile

                        } else {
                            modelStatusMsg.msg = msg
                            mDataFailure.value = modelStatusMsg
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus
            }
        })

    }


    fun getmDataUpdateSkill(): LiveData<ModelGetProfile> {
        return mDataUpdateSkills
    }


    fun updateLocation(
        auth: String,
        street: String,
        state: String,
        city: String,
        zipcode: String,
        type: String
    ) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> =
            service.updateBillingAddress(auth, street, state, city, zipcode, "", type, "0")
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    val res = response.body()!!.string()
                    val json = JSONObject(res)
                    val status = json.optString("status")
                    val msg = json.optString("msg")
                    val modelStatusMsg = ModelStatusMsg()
                    if (status == "true") {
                        modelStatusMsg.msg = msg
                        modelStatusMsg.status = status
                        mSuccessfull.value = modelStatusMsg
                    } else {
                        modelStatusMsg.msg = msg
                        mDataFailure.value = modelStatusMsg
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus
            }

        })

    }


    //-------------------------------------------------Update Image----------------------------------------------//


    fun updateImage(uri: File, type: String) {

        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()
        val auth = Constant.getPrefs(application).getString(Constant.auth_code, "")
        val service = retrofit.create(WebServicesCustomers::class.java)
        //..........................technique to storing the image in the web server......................//

        //pass it like this
        val file = uri
        val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file)
        // MultipartBody.Part is used to send also the actual file name
        val body = MultipartBody.Part.createFormData("profile_image", file.name, requestFile)
        // add another part within the multipart request
        val fullName = RequestBody.create(MediaType.parse("multipart/form-data"), auth)
        val type = RequestBody.create(MediaType.parse("multipart/form-data"), type)// type

        val call: Call<ResponseBody> = service.updateImage(fullName, type, body)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {

                if (response.isSuccessful) {
                    val res = response.body()!!.string()
                    val json = JSONObject(res)
                    val status = json.optString("status")
                    val msg = json.optString("msg")
                    val data = json.optJSONObject("data")

                    val model = ModelStatusMsg()
                    model.status = status
                    model.msg = msg
                    model.image = data.optString("profile_image")
                    mSuccessfullImage.value = model
                } else {
                    val modelStatus = ModelStatusMsg()
                    modelStatus.status = "false"
                    modelStatus.msg = "Network Error!"
                    mDataFailure.value = modelStatus
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus
            }
        })
    }


    fun getmDataSuccessfulImage(): LiveData<ModelStatusMsg> {
        return mSuccessfullImage
    }

    fun updateSocial(
        auth: String,
        type: String,
        facebook: String,
        insta: String,
        twitter: String,
        schedule: String
    ) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()
        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody>
        call = service.updateSocial(auth, type, facebook, insta, twitter, schedule)

        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelUpdateProfile = ModelGetProfile()
                        val modelStatusMsg = ModelStatusMsg()
                        if (status == "true") {
                            val data = json.optJSONObject("data")
                            modelUpdateProfile.profile_image = data.optString("profile_image")
                            modelUpdateProfile.is_merchant = data.optString("is_merchant")
                            modelUpdateProfile.name = data.optString("name")
                            modelUpdateProfile.contact_no = data.optString("contact_no")
                            modelUpdateProfile.email = data.optString("email")
                            modelUpdateProfile.gender = data.optString("gender")
                            modelUpdateProfile.stripe_customer_id =
                                data.optString("stripe_customer_id")
                            modelUpdateProfile.tax = data.optInt("tax")
                            modelUpdateProfile.twitter = data.optString("twitter")
                            modelUpdateProfile.facebook = data.optString("facebook")
                            modelUpdateProfile.about = data.optString("about")
                            modelUpdateProfile.instagram = data.optString("instagram")
                            modelUpdateProfile.status = status
                            modelUpdateProfile.msg = msg
                            modelStatusMsg.msg = msg
                            modelStatusMsg.status = status
                            mDataUpdateSkills.value = modelUpdateProfile

                        } else {
                            modelStatusMsg.msg = msg
                            mDataFailure.value = modelStatusMsg
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus
            }
        })


    }

    fun sendEmailVerification() {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val auth = Constant.getPrefs(application).getString(Constant.auth_code, "")

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody>
        call = service.sendEmailVerification(auth)

        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatusMsg = ModelStatusMsg()
                        if (status == "true") {
                            modelStatusMsg.msg = msg
                            modelStatusMsg.status = status
                            mDataVerifyEmail.value = modelStatusMsg

                        } else {
                            modelStatusMsg.msg = msg
                            mDataFailure.value = modelStatusMsg
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus
            }
        })

    }

    fun deleteCustomer(contactNo: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val auth = Constant.getPrefs(application).getString(Constant.auth_code, "")

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody>
        call = service.deleteCustomer(auth, contactNo)

        call.enqueue(object : Callback<ResponseBody> {
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatusMsg = ModelStatusMsg()
                        if (status == "true") {
                            modelStatusMsg.msg = msg
                            modelStatusMsg.status = status
                            mDataDeleteCustomet.value = modelStatusMsg

                        } else {
                            modelStatusMsg.msg = msg
                            mDataFailure.value = modelStatusMsg
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus
            }
        })

    }
}
