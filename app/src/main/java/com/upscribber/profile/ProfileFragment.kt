package com.upscribber.profile

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.Outline
import android.location.Geocoder
import android.media.MediaScannerConnection
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.text.Spannable
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.text.method.LinkMovementMethod
import android.text.method.ScrollingMovementMethod
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.view.*
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.widget.SwitchCompat
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.Autocomplete
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode
import com.kal.rackmonthpicker.MonthType
import com.kal.rackmonthpicker.RackMonthPicker
import com.upscribber.R
import com.upscribber.becomeseller.sellerWelcomeScreens.sellerWelcomActivity
import com.upscribber.commonClasses.Constant
import com.upscribber.commonClasses.RoundedBottomSheetDialogFragment
import com.upscribber.filters.LocationModel
import com.upscribber.home.InviteFriendMerchantActivity
import com.upscribber.home.ModelHomeLocation
import com.upscribber.loginSignUpScreen.LoginorSignUpActivity
import com.upscribber.notification.notifications.NotificationActivity
import com.upscribber.payment.PaymentTransactionActivity
import com.upscribber.payment.paymentCards.PaymentCardModel
import com.upscribber.payment.paymentCards.WebPagesOpenActivity
import com.upscribber.requestbusiness.RequestBusiness
import com.upscribber.subsciptions.merchantSubscriptionPages.SuccessfullShared
import com.upscribber.upscribberSeller.accountLinking.AccountLinkedMerchantNotifiedActivity
import com.upscribber.upscribberSeller.accountLinking.UnlinkAccountActivity
import com.upscribber.upscribberSeller.customerBottom.profile.activity.ProfileActivityListActivity
import com.upscribber.upscribberSeller.customerBottom.profile.subscriptionList.SubscriptionListActivity
import com.upscribber.upscribberSeller.manageTeam.ManageTeamActivity
import com.upscribber.upscribberSeller.manageTeam.ManageTeamViewModel
import com.upscribber.upscribberSeller.navigationCustomer.CustomerSellerActivity
import com.upscribber.upscribberSeller.navigationWallet.WalletSellerActivity
import com.upscribber.upscribberSeller.payments.PaymentsMerchantActivity
import com.upscribber.upscribberSeller.pictures.PicturesSellerActivity
import com.upscribber.upscribberSeller.reportingDetail.ReportingActivity
import com.upscribber.upscribberSeller.subsriptionSeller.location.searchLoc.SelectLocationActivity
import de.hdodenhof.circleimageview.CircleImageView
import io.fabric.sdk.android.services.settings.IconRequest.build
import kotlinx.android.synthetic.main.business_info_alert.*
import kotlinx.android.synthetic.main.logout_alert.*
import kotlinx.android.synthetic.main.profile_account_info.*
import kotlinx.android.synthetic.main.profile_header.*
import kotlinx.android.synthetic.main.profile_other_layout.*
import kotlinx.android.synthetic.main.profile_sellerother2.*
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.math.BigDecimal
import java.math.RoundingMode
import java.util.*

@Suppress(
    "NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS",
    "RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS"
)
class ProfileFragment : Fragment() {

    private var splitNumber: String = ""
    lateinit var payTrans: ConstraintLayout
    lateinit var becomeSeller: ConstraintLayout
    lateinit var activity: ConstraintLayout
    lateinit var tvLogout: TextView
    lateinit var others2: TextView
    //    lateinit var updateSkillsIndividual: TextView
    lateinit var updateSkillsTeamMember: TextView
    lateinit var updateSkillsMerchant: TextView
    lateinit var sellerOthers: TextView
    lateinit var skillsIndividuals: TextView
    lateinit var linearSkillss: LinearLayout
    lateinit var updateSocialTeam: LinearLayout
    lateinit var linearBusinessInfo: LinearLayout
    lateinit var skillsMerchant: TextView
    lateinit var updateBusinessInfoBtn: TextView
    lateinit var view1: View
    lateinit var view66: View
    lateinit var view79: View
    lateinit var view77: View
    lateinit var view33: View
    lateinit var view34: View
    lateinit var view45: View
    lateinit var viewBusiness: View
    lateinit var viewTeam: View
    lateinit var space1: Space
    lateinit var spaceTeam: Space
    lateinit var space2Team: Space
    lateinit var space65: Space
    lateinit var space66: Space
    lateinit var space68: Space
    lateinit var space79: Space
    lateinit var space78: Space
    lateinit var space77: Space
    lateinit var space35: Space
    lateinit var space5: Space
    lateinit var space46: Space
    lateinit var space45: Space
    lateinit var space36: Space
    lateinit var space3: Space
    lateinit var space33: Space
    lateinit var spaceBusiness1: Space
    lateinit var spaceBusiness2: Space
    lateinit var space34: Space
    lateinit var textVerified: TextView
    lateinit var txtSkills: TextView
    lateinit var textVerify: TextView
    lateinit var tvAddress: TextView
    lateinit var textViewUpdate: TextView
    lateinit var txtPhone: TextView
    //    lateinit var layoutAccountSeller: LinearLayout
    lateinit var includeStats: LinearLayout
    lateinit var linearTeamSubscription: LinearLayout
    lateinit var linearSkills: LinearLayout
    lateinit var includePayments: LinearLayout
    lateinit var helpCenterCustomer: LinearLayout
    lateinit var imageView: CircleImageView
    lateinit var logoMerchant: CircleImageView
    lateinit var imageViewRefer: ImageView
    lateinit var imageViewGradient: CircleImageView
    lateinit var imageViewGradient1: CircleImageView
    lateinit var imageView1: CircleImageView
    lateinit var tvAccount: TextView
    lateinit var textView14: TextView
    lateinit var includeSeller2: LinearLayout
    lateinit var includeOther: LinearLayout
    lateinit var relativeGrow: RelativeLayout
    lateinit var subscriptions: RelativeLayout
    lateinit var customerList: RelativeLayout
    lateinit var cardInvite: CardView
    lateinit var cardRefer: CardView
    lateinit var earn_next: ConstraintLayout
    lateinit var layoutAccount: LinearLayout
    lateinit var feedbackMerchant: LinearLayout
    lateinit var feedbackCustomers: LinearLayout
    //    lateinit var layoutMerchantAddress: LinearLayout
    lateinit var tvInviteFriends: TextView
    lateinit var progressBar3: LinearLayout
    lateinit var progressBar_small: ConstraintLayout
    lateinit var imageViewLinear: LinearLayout
    lateinit var imageProgressBar: ProgressBar
    lateinit var tv_earn: TextView
    lateinit var clAccountLinked: LinearLayout
    lateinit var helpCenterMerchant: LinearLayout
    lateinit var notificationMerchant: LinearLayout
    lateinit var tvBusinessInfo: TextView
    //    lateinit var businessName: TextView
//    lateinit var merchantAddress: TextView
    lateinit var other1: TextView
    //    lateinit var businessPhone: TextView
//    lateinit var businessEmail: TextView
    lateinit var paymentsText: TextView
    lateinit var skillsTeamMembers: TextView
    lateinit var address: TextView
    lateinit var statusLinking: TextView
    lateinit var textAddAddress: TextView
    lateinit var merchantName: TextView
    lateinit var businessInfo: TextView
    lateinit var textViewNamee: TextView
    lateinit var textViewAddress: TextView
    lateinit var linearCustomerOther1: LinearLayout
    lateinit var linearLinked: LinearLayout
    lateinit var sellerOther: LinearLayout
    lateinit var linearother2: LinearLayout
    lateinit var paymentInfo: LinearLayout
    lateinit var manageTeam: LinearLayout
    lateinit var manageExtras: LinearLayout
    lateinit var managePictures: LinearLayout
    lateinit var updateSocial: LinearLayout
    lateinit var stats: LinearLayout
    lateinit var cardSWitch: CardView
    lateinit var activityMerchant: LinearLayout
    lateinit var nestedScrollView: NestedScrollView
    lateinit var payments: LinearLayout
    lateinit var storeLoc: LinearLayout
    lateinit var settingsCustomers: LinearLayout
    lateinit var settingsMerchant: LinearLayout
    lateinit var sellingSwitchLarge: SwitchCompat
    var checked: Int = 0
    lateinit var update: InterfaceSwitch
    var latitude = 0.0
    var longitude = 0.0

    companion object {
        lateinit var mViewModel: ProfileViewModel
    }

    var modelGetProfile: ModelGetProfile = ModelGetProfile()
    lateinit var modelHomeLocation: ArrayList<ModelHomeLocation>
    var bankDetailArray: ArrayList<PaymentCardModel> = ArrayList()
    var cardDetailArray: ArrayList<PaymentCardModel> = ArrayList()
    private lateinit var viewModel: ManageTeamViewModel


    override fun onCreateView(
        inflater: LayoutInflater, container:
        ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        val view: View = inflater.inflate(R.layout.fragment_profile, container, false)
        mViewModel = ViewModelProviders.of(this)[ProfileViewModel::class.java]
        viewModel = ViewModelProviders.of(this)[ManageTeamViewModel::class.java]
        Places.initialize(context!!, resources.getString(R.string.google_maps_key))
        initz(view)
        clickListener()
        presentLayout()
        //apiGetProfile()
        observerInit()



        sellingSwitchLarge.setOnCheckedChangeListener { p0, p1 ->
            checkchangelistener(p1)
        }

        return view
    }

    override fun onResume() {
        super.onResume()
        val auth = Constant.getPrefs(context!!).getString(Constant.auth_code, "")
        val type = Constant.getPrefs(context!!).getString(Constant.type, "")
        val isSeller = Constant.getSharedPrefs(context!!).getBoolean(Constant.IS_SELLER, false)
        progressBar3.visibility = View.VISIBLE
        if (isSeller) {
            when (type) {
                "1" -> {
                    mViewModel.getProfileData(auth, "1", "0", "0")
                }
                "2" -> {
                    mViewModel.getProfileData(auth, "2", "0", "0")
                }
                "3" -> {
                    mViewModel.getProfileData(auth, "3", "0", "0")
                }
            }
        } else {
            mViewModel.getProfileData(auth, "0", "0", "0")
        }

    }


    @SuppressLint("SetTextI18n")
    private fun observerInit() {
        val isSeller = Constant.getSharedPrefs(context!!).getBoolean(Constant.IS_SELLER, false)
        val type = Constant.getPrefs(context!!).getString(Constant.type, "")
        val imagePath = Constant.getPrefs(context!!).getString(Constant.dataImagePath, "")

        mViewModel.getDataCard().observe(this, androidx.lifecycle.Observer {
            cardDetailArray.clear()
            cardDetailArray.addAll(it)
        })
        mViewModel.getDataBank().observe(this, androidx.lifecycle.Observer {
            bankDetailArray.clear()
            bankDetailArray.addAll(it)
        })

        mViewModel.getmDataVerifyEmail().observe(this, androidx.lifecycle.Observer {
            progressBar_small.visibility = View.GONE
            startActivity(Intent(context, SuccessfullShared::class.java).putExtra("email", "email"))

        })


        mViewModel.getmDataProfile().observe(this, androidx.lifecycle.Observer {
            progressBar3.visibility = View.GONE
            imageProgressBar.visibility = View.GONE
            if (it.status == "true") {
                modelGetProfile = it.modelGetProfileCustomer
                modelHomeLocation = it.modelGetProfileMerchant
                if (isSeller) {
                    textViewAddress.visibility = View.GONE
                    textViewNamee.visibility = View.VISIBLE
                    textView14.visibility = View.GONE
                    textView15.visibility = View.GONE
                    textView16.visibility = View.GONE
                    textView17.visibility = View.GONE
                    friends_invited.visibility = View.GONE
                    txtCashEarned.visibility = View.GONE

                    if (type == "1") {
                        textViewName.text = it.modelGetProfileCustomer.business_name
                        textViewNamee.text = it.modelGetProfileCustomer.business_name
                        textViewName.text = it.modelGetProfileCustomer.business_name
                        tVProfilePhoneNumber.text =
                            Constant.formatPhoneNumber(it.modelGetProfileCustomer.business_phone)
                        tvEmail.text = it.modelGetProfileCustomer.business_email
                        notiNext.text = it.modelGetProfileCustomer.notification_count


                        if (it.modelGetProfileCustomer.speciality.isEmpty()) {
                            skillsIndividuals.text = ""
                        } else {
                            skillsIndividuals.text = it.modelGetProfileCustomer.speciality
                        }

                        if (it.modelGetProfileCustomer.about.isEmpty()) {
                            businessInfo.text = ""
                        } else {
                            businessInfo.text = it.modelGetProfileCustomer.about
                            makeTextViewResizable(
                                businessInfo,
                                2,
                                "..ReadMore",
                                false,
                                it.modelGetProfileCustomer.about
                            )

                        }

                        if (it.modelGetProfileMerchant.size > 0) {
                            if ((it.modelGetProfileMerchant[0].street == "") &&
                                (it.modelGetProfileMerchant[0].city == "")
                            ) {
                                textViewAddress.text = "No address added"
                            } else {
                                textViewAddress.text =
                                    it.modelGetProfileMerchant[0].street + "," + it.modelGetProfileMerchant[0].city + "," + it.modelGetProfileMerchant[0].zip_code
                            }
                        } else {
                            textViewAddress.text = "No address added"
                        }

//
                    } else if (type == "2") {
                        textViewName.text = it.modelGetProfileCustomer.business_name
                        textViewNamee.text = it.modelGetProfileCustomer.business_name
//                        textView15.text = it.modelGetProfileCustomer.speciality
                        textViewName.text = it.modelGetProfileCustomer.business_name
                        notiNext.text = it.modelGetProfileCustomer.notification_count
                        tVProfilePhoneNumber.text =
                            Constant.formatPhoneNumber(
                                it.modelGetProfileCustomer.contact_no
                            )
                        tvEmail.text = it.modelGetProfileCustomer.email

                        if (it.modelGetProfileCustomer.speciality.isEmpty()) {
                            skillsIndividuals.text = ""
                        } else {
                            skillsIndividuals.text = it.modelGetProfileCustomer.speciality
                        }

                        if (it.modelGetProfileCustomer.business_id != "0") {
                            merchantName.text = it.modelGetProfileCustomer.business_name
                            statusLinking.text = "Unlink"
                            Glide.with(this).load(imagePath + it.modelGetProfileCustomer.logo)
                                .placeholder(R.mipmap.circular_placeholder).into(logoMerchant)

                        } else {
                            merchantName.text = "No Business Linked"
                            statusLinking.text = "Relink Now"
                            logoMerchant.setImageResource(R.drawable.ic_cross_white_border)
                        }

                        try {
                            if (it.modelGetProfileCustomer.about.isEmpty() || it.modelGetProfileCustomer.about == "null") {
                                businessInfo.text = ""
                            } else {
                                businessInfo.text = it.modelGetProfileCustomer.about
                                makeTextViewResizable(
                                    businessInfo,
                                    2,
                                    "..ReadMore",
                                    false,
                                    it.modelGetProfileCustomer.about
                                )
                            }


                            if (it.modelGetProfileMerchant.size > 0) {
                                if ((it.modelGetProfileMerchant[0].street == "") &&
                                    (it.modelGetProfileMerchant[0].city == "")
                                ) {
                                    textViewAddress.text = "No address added"
                                } else {
                                    textViewAddress.text =
                                        it.modelGetProfileMerchant[0].street + "," + it.modelGetProfileMerchant[0].city + "," + it.modelGetProfileMerchant[0].zip_code
                                }
                            } else {
                                textViewAddress.text = "No address added"
                            }

                        } catch (e: Exception) {
                            e.printStackTrace()
                        }

                    } else if (type == "3") {
                        try {
                            textViewName.text = it.modelGetProfileCustomer.name
                            textViewNamee.text = it.modelGetProfileCustomer.name
//                        textView15.text = it.modelGetProfileCustomer.speciality
                            textViewName.text = it.modelGetProfileCustomer.name
                            tVProfilePhoneNumber.text =
                                Constant.formatPhoneNumber(
                                    it.modelGetProfileCustomer.contact_no
                                )
                            tvEmail.text = it.modelGetProfileCustomer.email
                            notiNext.text = it.modelGetProfileCustomer.notification_count
                            if (it.modelGetProfileCustomer.speciality.isEmpty()) {
                                skillsIndividuals.text = ""
                            } else {
                                skillsIndividuals.text = it.modelGetProfileCustomer.speciality
                            }

                            if (it.modelGetProfileCustomer.business_id != "0") {
                                merchantName.text = it.modelGetProfileCustomer.business_name
                                statusLinking.text = "Unlink"
                                Glide.with(this).load(imagePath + it.modelGetProfileCustomer.logo)
                                    .placeholder(R.mipmap.circular_placeholder).into(logoMerchant)

                            } else {
                                merchantName.text = "No Business Linked"
                                statusLinking.text = "Relink Now"
                                logoMerchant.setImageResource(R.drawable.ic_cross_white_border)
                            }

//                        if (it.modelGetProfileCustomer.business_id != "0") {
//                            merchantName.text = it.modelGetProfileCustomer.business_name
//                            Glide.with(this).load(it.modelGetProfileCustomer.logo)
//                                .placeholder(R.mipmap.circular_placeholder).into(logoMerchant)
//
//                        } else {
//                            merchantName.text = "No Business Linked"
//                            logoMerchant.setImageResource(R.drawable.ic_cross_white_border)
//                        }

                            if (it.modelGetProfileCustomer.about.isEmpty() || it.modelGetProfileCustomer.about == "null") {
                                businessInfo.text = ""
                            } else {
                                businessInfo.text = it.modelGetProfileCustomer.about
                                makeTextViewResizable(
                                    businessInfo,
                                    2,
                                    "..ReadMore",
                                    false,
                                    it.modelGetProfileCustomer.about
                                )

                            }

                            if (it.modelGetProfileMerchant.size > 0) {
                                if ((it.modelGetProfileMerchant[0].street == "") &&
                                    (it.modelGetProfileMerchant[0].city == "")
                                ) {
                                    textViewAddress.text = "No address added"
                                } else {
                                    textViewAddress.text =
                                        it.modelGetProfileMerchant[0].street + "," + it.modelGetProfileMerchant[0].city + "," + it.modelGetProfileMerchant[0].zip_code
                                }
                            } else {
                                textViewAddress.text = "No address added"
                            }


                        }catch (e: Exception){
                            e.printStackTrace()
                        }


                    }

                } else {
                    textViewAddress.visibility = View.GONE
                    textViewNamee.visibility = View.GONE
                    textView14.visibility = View.VISIBLE
                    textView15.visibility = View.GONE
                    textView16.visibility = View.VISIBLE
                    textView17.visibility = View.VISIBLE
                    friends_invited.visibility = View.VISIBLE
                    txtCashEarned.visibility = View.VISIBLE
                    textView16.text = it.modelGetProfileCustomer.invite_count

                    if (it.modelGetProfileCustomer.cash_earned.contains(".")){
                        val splitPos = it.modelGetProfileCustomer.cash_earned.split(".")
                        if (splitPos[1] == "00" || splitPos[1] == "0") {
                            textView17.text = "$" + splitPos[0]
                        } else {
                            textView17.text = "$" + BigDecimal(it.modelGetProfileCustomer.cash_earned).setScale(2, RoundingMode.HALF_UP).toString()
                        }
                    }else{
                        textView17.text = "$" + BigDecimal(it.modelGetProfileCustomer.cash_earned).setScale(2, RoundingMode.HALF_UP).toString()
                    }


                    textView14.text = it.modelGetProfileCustomer.name
                    if (it.modelGetProfileCustomer.primary_address.city != "" ||
                        it.modelGetProfileCustomer.primary_address.state != "" ||
                        it.modelGetProfileCustomer.primary_address.street != "" ||
                        it.modelGetProfileCustomer.primary_address.zip_code != ""
                    ) {
                        textAddAddress.visibility = View.GONE
                        textView15.text = it.modelGetProfileCustomer.primary_address.street + "," +
                                it.modelGetProfileCustomer.primary_address.city + "," +
                                it.modelGetProfileCustomer.primary_address.state + "," +
                                it.modelGetProfileCustomer.primary_address.zip_code

                        tvAddress.text = it.modelGetProfileCustomer.primary_address.street + "," +
                                it.modelGetProfileCustomer.primary_address.city + "," +
                                it.modelGetProfileCustomer.primary_address.state + "," +
                                it.modelGetProfileCustomer.primary_address.zip_code
                    } else if (it.modelGetProfileCustomer.billing_address.billing_city != "" ||
                        it.modelGetProfileCustomer.billing_address.billing_state != "" ||
                        it.modelGetProfileCustomer.billing_address.billing_street != "" ||
                        it.modelGetProfileCustomer.billing_address.billing_zipcode != ""
                    ) {
                        textView15.text =
                            it.modelGetProfileCustomer.billing_address.billing_street + "," +
                                    it.modelGetProfileCustomer.billing_address.billing_city + "," +
                                    it.modelGetProfileCustomer.billing_address.billing_state + "," +
                                    it.modelGetProfileCustomer.billing_address.billing_zipcode

                        tvAddress.text =
                            it.modelGetProfileCustomer.billing_address.billing_street + "," +
                                    it.modelGetProfileCustomer.billing_address.billing_city + "," +
                                    it.modelGetProfileCustomer.billing_address.billing_state + "," +
                                    it.modelGetProfileCustomer.billing_address.billing_zipcode
                    } else {
                        textView15.text = "No Address"
                        textAddAddress.visibility = View.VISIBLE
                    }

                    textViewName.text = it.modelGetProfileCustomer.name
                    if (it.modelGetProfileCustomer.email_verify == "0") {
                        textVerify.visibility = View.VISIBLE
                    } else {
                        textVerify.visibility = View.GONE
                    }

                    tVProfilePhoneNumber.text =
                        Constant.formatPhoneNumber(
                            it.modelGetProfileCustomer.contact_no
                        )

                    tvEmail.text = it.modelGetProfileCustomer.email
                    next.text = it.modelGetProfileCustomer.notification_count
                }

                if (isSeller) {
                    cardSWitch.visibility = View.VISIBLE
                    becomeSeller.visibility = View.GONE
                    space1.visibility = View.GONE
                    space3.visibility = View.GONE
                    view1.visibility = View.GONE
                } else {
                    if (it.modelGetProfileCustomer.steps == "6" && it.modelGetProfileCustomer.type == "1") {
                        cardSWitch.visibility = View.VISIBLE
                        becomeSeller.visibility = View.GONE
                        space1.visibility = View.GONE
                        space3.visibility = View.GONE
                        view1.visibility = View.GONE

                    } else if (it.modelGetProfileCustomer.steps == "4" && it.modelGetProfileCustomer.type == "2") {
                        cardSWitch.visibility = View.VISIBLE
                        becomeSeller.visibility = View.VISIBLE
                        space1.visibility = View.VISIBLE
                        space3.visibility = View.VISIBLE
                        view1.visibility = View.VISIBLE
                    } else if (it.modelGetProfileCustomer.steps == "0" && it.modelGetProfileCustomer.type == "3") {
                        cardSWitch.visibility = View.VISIBLE
                        becomeSeller.visibility = View.VISIBLE
                        space1.visibility = View.VISIBLE
                        space3.visibility = View.VISIBLE
                        view1.visibility = View.VISIBLE
                    } else {
                        cardSWitch.visibility = View.GONE
                        becomeSeller.visibility = View.VISIBLE
                        space1.visibility = View.VISIBLE
                        space3.visibility = View.VISIBLE
                        view1.visibility = View.VISIBLE
                    }
                }

                if (modelGetProfile.profile_image.isEmpty()) {
                    if (isSeller) {
                        Glide.with(context!!).load(R.mipmap.circular_placeholder)
                            .placeholder(R.mipmap.circular_placeholder)
                            .into(imageView)
                    } else {
                        if (modelGetProfile.gender == "") {
                            Glide.with(context!!).load(R.drawable.ic_male_dummy)
                                .placeholder(R.mipmap.circular_placeholder).into(imageView1)
                        }
//                        if (modelGetProfile.gender == "female") {
//                            Glide.with(context!!).load(R.drawable.ic_female_dummy)
//                                .placeholder(R.mipmap.circular_placeholder).into(imageView1)
//                        } else if (modelGetProfile.gender == "male") {
//                            Glide.with(context!!).load(R.drawable.ic_male_dummy)
//                                .placeholder(R.mipmap.circular_placeholder)
//                                .into(imageView1)
//                        }
                    }
                } else {
                    if (isSeller) {
                        Glide.with(context!!)
                            .load(imagePath + it.modelGetProfileCustomer.profile_image)
                            .placeholder(R.mipmap.circular_placeholder).into(imageView)
                    } else {
                        Glide.with(context!!)
                            .load(imagePath + it.modelGetProfileCustomer.profile_image)
                            .placeholder(R.mipmap.circular_placeholder).into(imageView1)
                    }
                }
            }

        })

        mViewModel.getStatus().observe(this, androidx.lifecycle.Observer {
            if (it.msg == "Invalid auth code") {
                progressBar3.visibility = View.GONE
                progressBar_small.visibility = View.GONE
                Constant.commonAlert(context!!)
            } else {
                Toast.makeText(context, it.msg, Toast.LENGTH_SHORT).show()
            }

        })

        //------------------------------------Logout Observer---------------------------------------//

        mViewModel.observeLogout().observe(this, androidx.lifecycle.Observer {
            if (it) {
                progressBar3.visibility = View.GONE
                Constant.getPrefs(context!!).edit().clear().apply()
                Constant.getSharedPrefs(context!!).edit().clear().apply()
                Constant.getPrefs1(context!!).edit().clear().apply()
                val intent = Intent(context, LoginorSignUpActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
            }
        })

        mViewModel.getImageSuccessful().observe(this, androidx.lifecycle.Observer {
            val isSeller = Constant.getSharedPrefs(context!!)
                .getBoolean(Constant.IS_SELLER, false)
            val imagePath = Constant.getPrefs(context!!).getString(Constant.dataImagePath, "")

            if (isSeller) {

                Glide.with(context!!)
                    .load(imagePath + it.image)
                    .placeholder(R.mipmap.circular_placeholder).into(imageView)
            } else {

                Glide.with(context!!)
                    .load(imagePath + it.image)
                    .placeholder(R.mipmap.circular_placeholder).into(imageView1)
            }

            apiGetImage()


        })


        viewModel.getmUnlinked().observe(this, androidx.lifecycle.Observer {
            progressBar_small.visibility = View.GONE
            if (it.status == "true") {
                startActivity(
                    Intent(
                        context,
                        AccountLinkedMerchantNotifiedActivity::class.java
                    ).putExtra("profile", it)
                )
            }
        })

        viewModel.getmRelinked().observe(this, androidx.lifecycle.Observer {
            progressBar_small.visibility = View.GONE
            if (it.status == "true") {
//                    progressBar3.visibility = View.GONE
                startActivity(
                    Intent(context, UnlinkAccountActivity::class.java).putExtra(
                        "profile",
                        it
                    )
                )
            }
        })


    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    try {
                        val place = Autocomplete.getPlaceFromIntent(data!!)
                        val latLng = place.latLng
                        latitude = latLng!!.latitude
                        longitude = latLng.longitude
                        val geocoder = Geocoder(context!!)
                        val addresses =
                            latLng.latitude.let {
                                geocoder.getFromLocation(
                                    it,
                                    latLng.longitude,
                                    1
                                )
                            }
                        val locationModel = LocationModel()
                        locationModel.address = addresses[0].getAddressLine(0)
                        locationModel.city = addresses[0].locality
                        locationModel.state = addresses[0].adminArea
                        locationModel.latitude = latitude.toString()
                        locationModel.longitude = longitude.toString()
                        locationModel.street = addresses[0].thoroughfare
                        locationModel.zipcode = addresses[0].postalCode
                        startActivity(
                            Intent(
                                context!!,
                                ChangeLocationSetingsActivity::class.java
                            ).putExtra("profile", "123").putExtra("location", locationModel)
                        )
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                }
            }
        }

    }

    private fun makeTextViewResizable(
        businessInfo: TextView,
        maxLine: Int,
        expandText: String,
        viewMore: Boolean,
        about: String
    ) {


        if (businessInfo.tag == null) {
            businessInfo.tag = businessInfo.text
        }
        val vto = businessInfo.viewTreeObserver
        vto.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {

            @SuppressWarnings("deprecation")
            @Override
            override fun onGlobalLayout() {

                var text = ""
                var lineEndIndex = 0
                val obs = businessInfo.viewTreeObserver
                obs.removeGlobalOnLayoutListener(this)
                if (maxLine == 0) {
                    lineEndIndex = businessInfo.layout.getLineEnd(0)
                    text = businessInfo.text.subSequence(
                        0,
                        lineEndIndex - expandText.length + 1
                    ).toString() + " " + expandText.trim()
                } else if (maxLine > 0 && businessInfo.lineCount >= maxLine) {
                    lineEndIndex = businessInfo.layout.getLineEnd(maxLine - 1)
                    text = businessInfo.text.subSequence(
                        0,
                        lineEndIndex - expandText.length
                    ).toString() + "" + expandText.trim()
                } else {
                    lineEndIndex = businessInfo.layout.getLineEnd(businessInfo.layout.lineCount - 1)
                    text = businessInfo.text.subSequence(0, lineEndIndex).toString()
                }

                businessInfo.text = text

                businessInfo.movementMethod = LinkMovementMethod.getInstance()
                businessInfo.setText(
                    addClickablePartTextViewResizable(
                        businessInfo.text.toString(), expandText.trim(),
                        about
                    ), TextView.BufferType.NORMAL
                )
            }
        })
    }


    private fun addClickablePartTextViewResizable(
        strSpanned: String,
        spanableText: String,
        about: String
    ): SpannableStringBuilder? {
        val str = strSpanned
        val ssb = SpannableStringBuilder(strSpanned)
        if (str.contains(spanableText)) {
            ssb.setSpan(object : MySpannable(false) {
                override fun onClick(widget: View) {
                    alertDialog(about)

                }
            }, str.indexOf(spanableText), str.indexOf(spanableText) + spanableText.length, 0)
        }
        return ssb
    }

    private fun alertDialog(it: String) {
        val mDialogView = LayoutInflater.from(context).inflate(R.layout.business_info_alert, null)
        val mBuilder = AlertDialog.Builder(context)
            .setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.setCancelable(false)
//        mBuilder.setTitle("Business Info")
        mAlertDialog.window.setBackgroundDrawableResource(R.drawable.bg_alert_new)
        mAlertDialog.textView249.text = it
        mAlertDialog.textView249.movementMethod = ScrollingMovementMethod()
        mAlertDialog.okBtn.setOnClickListener {
            mAlertDialog.dismiss()
        }
    }


    private fun apiGetProfile() {
        val auth = Constant.getPrefs(context!!).getString(Constant.auth_code, "")
        val isSeller = Constant.getSharedPrefs(context!!).getBoolean(Constant.IS_SELLER, false)
        val type = Constant.getPrefs(context!!).getString(Constant.type, "")
        progressBar3.visibility = View.VISIBLE
        if (isSeller) {
            when (type) {
                "1" -> {
                    mViewModel.getProfileData(auth, "1", "0", "0")
                }
                "2" -> {
                    mViewModel.getProfileData(auth, "2", "0", "0")
                }
                "3" -> {
                    mViewModel.getProfileData(auth, "3", "0", "0")
                }
            }

        } else {
            mViewModel.getProfileData(auth, "0", "0", "0")
        }

    }

    private fun apiGetImage() {
        val auth = Constant.getPrefs(context!!).getString(Constant.auth_code, "")
        val isSeller = Constant.getSharedPrefs(context!!).getBoolean(Constant.IS_SELLER, false)
        val type = Constant.getPrefs(context!!).getString(Constant.type, "")
        imageProgressBar.visibility = View.VISIBLE
        if (isSeller) {
            when (type) {
                "1" -> {
                    mViewModel.getProfileData(auth, "1", "0", "0")
                }
                "2" -> {
                    mViewModel.getProfileData(auth, "2", "0", "0")
                }
                "3" -> {
                    mViewModel.getProfileData(auth, "3", "0", "0")
                }
            }

        } else {
            mViewModel.getProfileData(auth, "0", "0", "0")
        }

    }


    private fun initz(view: View) {
        payTrans = view.findViewById(R.id.payTrans)
        textViewNamee = view.findViewById(R.id.textViewNamee)
        textViewAddress = view.findViewById(R.id.textViewAddress)
        textAddAddress = view.findViewById(R.id.textAddAddress)
        businessInfo = view.findViewById(R.id.businessInfo)
        spaceBusiness1 = view.findViewById(R.id.spaceBusiness1)
        spaceBusiness2 = view.findViewById(R.id.spaceBusiness2)
        imageViewLinear = view.findViewById(R.id.imageViewLinear)
        viewBusiness = view.findViewById(R.id.viewBusiness)
        updateSocialTeam = view.findViewById(R.id.updateSocialTeam)
        space2Team = view.findViewById(R.id.space2Team)
        linearBusinessInfo = view.findViewById(R.id.linearBusinessInfo)
        viewTeam = view.findViewById(R.id.viewTeam)
        spaceTeam = view.findViewById(R.id.spaceTeam)
        paymentsText = view.findViewById(R.id.paymentsText)
        includePayments = view.findViewById(R.id.includePayments)
        updateBusinessInfoBtn = view.findViewById(R.id.updateBusinessInfoBtn)
        updateSkillsTeamMember = view.findViewById(R.id.updateSkillsTeamMember)
        updateSkillsMerchant = view.findViewById(R.id.updateSkillsMerchant)
        customerList = view.findViewById(R.id.customerList)
        subscriptions = view.findViewById(R.id.subscriptions)
        skillsTeamMembers = view.findViewById(R.id.skillsTeamMembers)
        view77 = view.findViewById(R.id.view77)
        space79 = view.findViewById(R.id.space79)
        includeStats = view.findViewById(R.id.includeStats)
        space65 = view.findViewById(R.id.space65)
        space66 = view.findViewById(R.id.space66)
        space68 = view.findViewById(R.id.space68)
        view66 = view.findViewById(R.id.view66)
        linearTeamSubscription = view.findViewById(R.id.linearTeamSubscription)
        txtPhone = view.findViewById(R.id.txtPhone)
        space5 = view.findViewById(R.id.space5)
        space77 = view.findViewById(R.id.space77)
        view79 = view.findViewById(R.id.view79)
        space78 = view.findViewById(R.id.space78)
        tvAddress = view.findViewById(R.id.tvAddress)
        linearSkills = view.findViewById(R.id.linearSkills)
//        merchantAddress = view.findViewById(R.id.merchantAddress)
//        layoutMerchantAddress = view.findViewById(R.id.layoutMerchantAddress)
        updateSocial = view.findViewById(R.id.updateSocial)
        statusLinking = view.findViewById(R.id.statusLinking)
        linearLinked = view.findViewById(R.id.linearLinked)
        view33 = view.findViewById(R.id.view33)
        view34 = view.findViewById(R.id.view34)
        view45 = view.findViewById(R.id.view45)
        space35 = view.findViewById(R.id.space35)
        space46 = view.findViewById(R.id.space46)
        space45 = view.findViewById(R.id.space45)
        address = view.findViewById(R.id.address)
        merchantName = view.findViewById(R.id.merchantName)
        logoMerchant = view.findViewById(R.id.logoMerchant)
        space36 = view.findViewById(R.id.space36)
        space33 = view.findViewById(R.id.space33)
        nestedScrollView = view.findViewById(R.id.nestedScrollView)
        skillsIndividuals = view.findViewById(R.id.skillsIndividuals)
        linearSkillss = view.findViewById(R.id.linearSkillss)
//        skillsMerchant = view.findViewById(R.id.skills)
        space34 = view.findViewById(R.id.space34)
        txtSkills = view.findViewById(R.id.txtSkills)
        textVerified = view.findViewById(R.id.textVerified)
        textVerify = view.findViewById(R.id.textVerify)
        progressBar3 = view.findViewById(R.id.progressBar3)
        imageProgressBar = view.findViewById(R.id.imageProgressBar)
        cardRefer = view.findViewById(R.id.cardRefer)
        cardSWitch = view.findViewById(R.id.cardSWitch)
        settingsMerchant = view.findViewById(R.id.settingsMerchant)
        settingsCustomers = view.findViewById(R.id.settingsCustomers)
        storeLoc = view.findViewById(R.id.storeLoc)
        payments = view.findViewById(R.id.payments)
        stats = view.findViewById(R.id.stats)
        notificationMerchant = view.findViewById(R.id.notificationMerchant)
        feedbackCustomers = view.findViewById(R.id.feedbackCustomers)
        feedbackMerchant = view.findViewById(R.id.feedbackMerchant)
        helpCenterCustomer = view.findViewById(R.id.help_customers)
        activity = view.findViewById(R.id.activity)
//        businessEmail = view.findViewById(R.id.businessEmail)
        imageViewRefer = view.findViewById(R.id.image)
        tvLogout = view.findViewById(R.id.tvLogout)
        others2 = view.findViewById(R.id.others2)
        textViewUpdate = view.findViewById(R.id.textViewUpdate)
        imageView = view.findViewById(R.id.imageView)
        imageViewGradient = view.findViewById(R.id.imageViewGradient)
        imageView1 = view.findViewById(R.id.imageView1)
        imageViewGradient1 = view.findViewById(R.id.imageViewGradient1)
        layoutAccount = view.findViewById(R.id.layoutAccount)
        helpCenterMerchant = view.findViewById(R.id.helpCenterMerchant)
        tvAccount = view.findViewById(R.id.tvAccount)
//        layoutAccountSeller = view.findViewById(R.id.layoutAccountSeller)
        relativeGrow = view.findViewById(R.id.relativeGrow)
        tvInviteFriends = view.findViewById(R.id.tvInviteFriends)
        tvBusinessInfo = view.findViewById(R.id.tvBusinessInfo)
        clAccountLinked = view.findViewById(R.id.clAccountLinked)
        other1 = view.findViewById(R.id.other1)
        sellerOthers = view.findViewById(R.id.sellerOthers)
//        linearSellerLocations = view.findViewById(R.id.linearSellerLocations)
        linearCustomerOther1 = view.findViewById(R.id.linearCustomerOther1)
        becomeSeller = view.findViewById(R.id.becomeSeller)
        linearother2 = view.findViewById(R.id.linearother2)
        sellerOther = view.findViewById(R.id.sellerOther)
        includeSeller2 = view.findViewById(R.id.includeSeller2)
        includeOther = view.findViewById(R.id.includeOther)
        paymentInfo = view.findViewById(R.id.paymentInfo)
        manageTeam = view.findViewById(R.id.manageTeam)
        manageExtras = view.findViewById(R.id.manageExtras)
        managePictures = view.findViewById(R.id.managePictures)
        earn_next = view.findViewById(R.id.earn_next)
        textView14 = view.findViewById(R.id.textView14)
        cardInvite = view.findViewById(R.id.cardInvite)
        tv_earn = view.findViewById(R.id.tv_earn)
        sellingSwitchLarge = view.findViewById(R.id.sellingSwitchLarge)
        activityMerchant = view.findViewById(R.id.activityMerchant)
        space1 = view.findViewById(R.id.space1)
        space3 = view.findViewById(R.id.space2)
        view1 = view.findViewById(R.id.viewBecome)
        progressBar_small = view.findViewById(R.id.progressBar_small)
//        businessName = view.findViewById(R.id.businessName)
//        businessPhone = view.findViewById(R.id.businessPhone)

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        update = context as InterfaceSwitch
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        update = activity as InterfaceSwitch
    }

    private fun checkchangelistener(p1: Boolean) {
        update.switchingProjects(p1)
    }

    private fun presentLayout() {

    }

//    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
//        menu!!.clear()
//        inflater!!.inflate(R.menu.profile, menu)
//        super.onCreateOptionsMenu(menu, inflater)
//    }



    private fun clickListener() {
        val type = Constant.getPrefs(context!!).getString(Constant.type, "")

        statusLinking.setOnClickListener {
            if (modelGetProfile.business_id != "0") {
                val fragment =
                    MenuFragmentlinking("unlink", modelGetProfile, viewModel, progressBar_small)
                fragment.show(fragmentManager!!, fragment.tag)

            } else {
                val fragment =
                    MenuFragmentlinking("relink", modelGetProfile, viewModel, progressBar_small)
                fragment.show(fragmentManager!!, fragment.tag)
            }
        }

        textAddAddress.setOnClickListener {
            Places.createClient(context!!)
            val fields =
                listOf(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG)
            // Start the autocomplete intent.
            val intent = Autocomplete.IntentBuilder(
                AutocompleteActivityMode.FULLSCREEN, fields
            )
                .build(context!!)
            startActivityForResult(intent, 1)
        }


        textVerify.setOnClickListener {
            progressBar_small.visibility = View.VISIBLE
            mViewModel.sendEmailVerification()
        }


        customerList.setOnClickListener {
            startActivity(
                Intent(
                    context,
                    CustomerSellerActivity::class.java
                ).putExtra("profileTeam", "1")
            )
        }

        updateSkillsMerchant.setOnClickListener {
            startActivity(
                Intent(context, UpdateSkillsActivity::class.java).putExtra(
                    "modelProfile",
                    modelGetProfile
                )
            )
        }


        updateSkillsTeamMember.setOnClickListener {
            startActivity(
                Intent(context, UpdateSkillsActivity::class.java).putExtra(
                    "modelProfile",
                    modelGetProfile
                )
            )
        }




        subscriptions.setOnClickListener {
            val intent = Intent(context, SubscriptionListActivity::class.java)
            intent.putExtra("subscription", "invite")
            startActivity(intent)
        }

        settingsCustomers.setOnClickListener {
            startActivity(
                Intent(context, SettingsProfileActivity::class.java).putExtra(
                    "modelProfile",
                    modelGetProfile
                )
            )
        }

        updateSocial.setOnClickListener {
            startActivity(
                Intent(context, UpdateSocialActivity::class.java).putExtra(
                    "modelProfile",
                    modelGetProfile
                )
            )
        }


//        updateSocialTeam.setOnClickListener {
//            startActivity(
//                Intent(context, UpdateSocialActivity::class.java).putExtra(
//                    "modelProfile",
//                    modelGetProfile
//                )
//            )
//        }


        updateBusinessInfoBtn.setOnClickListener {
            startActivity(
                Intent(context, UpdateSkillsActivity::class.java).putExtra(
                    "modelProfile",
                    modelGetProfile
                ).putExtra("modelProfileBusinessInfo", "modelProfileBusinessInfo")
            )
        }


        settingsMerchant.setOnClickListener {
            startActivity(
                Intent(context, SettingsProfileActivity::class.java).putExtra(
                    "modelProfile",
                    modelGetProfile
                )
            )
        }

        storeLoc.setOnClickListener {
            val intent =
                Intent(context, SelectLocationActivity::class.java).putExtra(
                    "navigation",
                    "others"
                )
            startActivity(intent)
        }

        payments.setOnClickListener {
            startActivity(
                Intent(context, WalletSellerActivity::class.java).putExtra(
                    "modelProfile",
                    modelGetProfile
                )
            )
        }

        feedbackCustomers.setOnClickListener {
            val emailIntent = Intent(
                Intent.ACTION_SENDTO, Uri.fromParts(
                    "mailto", "contactus@upscribbr.com", null
                )
            )
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Feedback")
            emailIntent.putExtra(
                Intent.EXTRA_TEXT,
                ""
            )
            this.startActivity(Intent.createChooser(emailIntent, "Send email"))
        }

        feedbackMerchant.setOnClickListener {
            val emailIntent = Intent(
                Intent.ACTION_SENDTO, Uri.fromParts(
                    "mailto", "contactus@upscribbr.com", null
                )
            )
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Feedback")
            emailIntent.putExtra(
                Intent.EXTRA_TEXT,
                ""
            )
            this.startActivity(Intent.createChooser(emailIntent, "Send email"))
        }


        helpCenterCustomer.setOnClickListener {
            startActivity(
                Intent(context, WebPagesOpenActivity::class.java).putExtra(
                    "help",
                    "help"
                )
            )
        }

        helpCenterMerchant.setOnClickListener {
            startActivity(
                Intent(context, WebPagesOpenActivity::class.java).putExtra(
                    "helpMerchant",
                    "helpMerchant"
                )
            )
        }

        becomeSeller.setOnClickListener {
            startActivity(Intent(context, sellerWelcomActivity::class.java))
        }

        payTrans.setOnClickListener {
            startActivity(
                Intent(
                    context,
                    PaymentTransactionActivity::class.java
                ).putExtra("modelGetProfile", modelGetProfile)
            )
//            if (Constant.cardArrayListData.size > 0) {
//                startActivity(
//                    Intent(
//                        context,
//                        PaymentTransactionActivity::class.java
//                    ).putExtra("modelGetProfile", modelGetProfile)
//                )
//            } else {
//                startActivity(
//                    Intent(
//                        context,
//                        AddCardActivity::class.java
//                    ).putExtra("profilePayment", "payments").putExtra(
//                        "modelGetProfile",
//                        modelGetProfile
//                    )
//                )
//            }
        }

        stats.setOnClickListener {
            startActivity(Intent(context, ReportingActivity::class.java).putExtra("value", 1))
        }

        activityMerchant.setOnClickListener {
            val intent =
                Intent(context, ActivitiesMerchantTabs::class.java).putExtra("nav", "others")
            startActivity(intent)
        }


        tvLogout.setOnClickListener {
            val mDialogView = LayoutInflater.from(context).inflate(R.layout.logout_alert, null)
            val mBuilder = AlertDialog.Builder(context)
                .setView(mDialogView)
            val mAlertDialog = mBuilder.show()
            val isSeller =
                Constant.getSharedPrefs(context!!).getBoolean(Constant.IS_SELLER, false)
            if (isSeller) {
                mAlertDialog.no.setTextColor(resources.getColor(R.color.blue))
                mAlertDialog.yes.setTextColor(resources.getColor(R.color.blue))
            } else {
                mAlertDialog.no.setTextColor(resources.getColor(R.color.accent))
                mAlertDialog.yes.setTextColor(resources.getColor(R.color.accent))
            }

            mAlertDialog.no.setOnClickListener {
                mAlertDialog.dismiss()
            }
            mAlertDialog.setCancelable(false)
            mAlertDialog.window.setBackgroundDrawableResource(R.drawable.bg_alert1)
            mAlertDialog.yes.setOnClickListener {
                progressBar3.visibility = View.VISIBLE
                val auth = Constant.getPrefs(context!!).getString(Constant.auth_code, "")
                mViewModel.logoutWebservice(auth)
                mAlertDialog.dismiss()
            }


        }

        earn_next.setOnClickListener {
            startActivity(Intent(context, NotificationActivity::class.java))
        }

        notificationMerchant.setOnClickListener {
            startActivity(Intent(context, NotificationActivity::class.java))
        }


        activity.setOnClickListener {
            startActivity(Intent(context, ProfileActivityListActivity::class.java))
        }



        textViewUpdate.setOnClickListener {
                val fragment = MenuFragment(imageView, imageView1)
                fragment.show(fragmentManager!!, fragment.tag)
        }



        imageViewLinear.setOnClickListener {
            val fragment = MenuFragment(imageView, imageView1)
            fragment.show(fragmentManager!!, fragment.tag)
        }




        managePictures.setOnClickListener {
            startActivity(
                Intent(context, PicturesSellerActivity::class.java).putExtra(
                    "profile",
                    "other"
                )
            )
        }

        cardInvite.setOnClickListener {
            startActivity(
                Intent(context, InviteFriendMerchantActivity::class.java)
                    .putExtra("invite", modelGetProfile.invite_code)
            )
        }

        cardRefer.setOnClickListener {
            startActivity(Intent(context, RequestBusiness::class.java))
        }


        val spannableString =
            SpannableString("Get 20% off. Invite a Friend Now")
        spannableString.setSpan(
            ForegroundColorSpan(Color.BLACK),
            13, 32,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        tv_earn.text = spannableString


        val isSeller = Constant.getSharedPrefs(context!!).getBoolean(Constant.IS_SELLER, false)
        sellingSwitchLarge.isChecked = isSeller
        if (isSeller) {
            if (type == "1") {
                view33.visibility = View.VISIBLE
                space33.visibility = View.VISIBLE
                txtSkills.visibility = View.VISIBLE
                linearSkillss.visibility = View.VISIBLE
                updateSkillsMerchant.visibility = View.VISIBLE
                view34.visibility = View.VISIBLE
                space35.visibility = View.VISIBLE
                space5.visibility = View.VISIBLE
                space34.visibility = View.VISIBLE
                linearLinked.visibility = View.GONE
                space36.visibility = View.GONE
                address.visibility = View.GONE
                tvAddress.visibility = View.GONE
                space45.visibility = View.GONE
                view45.visibility = View.GONE
                space46.visibility = View.GONE
                spaceBusiness1.visibility = View.GONE
                spaceBusiness2.visibility = View.GONE
                viewBusiness.visibility = View.GONE
                txtPhone.text = "Phone"
                space77.visibility = View.VISIBLE
                linearBusinessInfo.visibility = View.VISIBLE
                view77.visibility = View.VISIBLE
                space78.visibility = View.VISIBLE
                space79.visibility = View.VISIBLE
                view79.visibility = View.VISIBLE
                space65.visibility = View.VISIBLE
                view66.visibility = View.VISIBLE
                space68.visibility = View.VISIBLE
                space66.visibility = View.VISIBLE
                feedbackMerchant.visibility = View.VISIBLE
                notificationMerchant.visibility = View.VISIBLE
                helpCenterMerchant.visibility = View.VISIBLE
                paymentsText.visibility = View.VISIBLE
                includePayments.visibility = View.VISIBLE
                updateSocialTeam.visibility = View.GONE
                space2Team.visibility = View.GONE
                viewTeam.visibility = View.GONE
                spaceTeam.visibility = View.GONE

                imageViewRefer.setImageResource(R.drawable.ic_refer_a_business_blue)
                sellerOthers.visibility = View.VISIBLE
                other1.visibility = View.GONE
                tvLogout.setTextColor(resources.getColor(R.color.blue))
                tvAccount.text = getString(R.string.seller_info)
                other1.text = getString(R.string.location)
                layoutAccount.visibility = View.VISIBLE
//                layoutAccountSeller.visibility = View.VISIBLE
                tvInviteFriends.visibility = View.GONE
                relativeGrow.visibility = View.GONE
                cardInvite.visibility = View.GONE
                tvBusinessInfo.visibility = View.VISIBLE
                clAccountLinked.visibility = View.VISIBLE
                linearCustomerOther1.visibility = View.GONE
                linearother2.visibility = View.GONE
//            linearSellerLocations.visibility = View.GONE
                sellerOther.visibility = View.VISIBLE
                includeSeller2.visibility = View.VISIBLE
                includeOther.visibility = View.GONE
                imageView.visibility = View.VISIBLE
                others2.visibility = View.VISIBLE
                imageViewGradient.visibility = View.VISIBLE
                imageView1.visibility = View.GONE
                imageViewGradient1.visibility = View.GONE
                others2.text = "Stats & Activity"
                paymentInfo.setOnClickListener {
                    startActivity(
                        Intent(context, PaymentsMerchantActivity::class.java).putExtra(
                            "profile", "other"
                        )
                            .putExtra("modelProfile", modelGetProfile)
                            .putExtra("bank", bankDetailArray)
                            .putExtra("card", cardDetailArray)
                    )
                }

                manageTeam.setOnClickListener {
                    startActivity(
                        Intent(context, ManageTeamActivity::class.java).putExtra(
                            "notSelected",
                            "SelectionList"
                        )
                    )
                }

            } else if (type == "2") {
                space77.visibility = View.VISIBLE
                view77.visibility = View.VISIBLE
                space78.visibility = View.VISIBLE
                space79.visibility = View.VISIBLE
                view79.visibility = View.VISIBLE
                space65.visibility = View.VISIBLE
                view66.visibility = View.VISIBLE
                space68.visibility = View.VISIBLE
                space66.visibility = View.VISIBLE
                feedbackMerchant.visibility = View.VISIBLE
                notificationMerchant.visibility = View.VISIBLE
                linearBusinessInfo.visibility = View.VISIBLE
                helpCenterMerchant.visibility = View.VISIBLE
                sellerOthers.visibility = View.VISIBLE
                paymentsText.visibility = View.VISIBLE
                includePayments.visibility = View.VISIBLE
                updateSocialTeam.visibility = View.GONE
                space2Team.visibility = View.GONE
                viewTeam.visibility = View.GONE
                spaceTeam.visibility = View.GONE
                spaceBusiness1.visibility = View.VISIBLE
                spaceBusiness2.visibility = View.VISIBLE
                viewBusiness.visibility = View.VISIBLE
                address.visibility = View.GONE
                tvAddress.visibility = View.GONE
                space45.visibility = View.GONE
                view45.visibility = View.GONE
                space46.visibility = View.GONE
//                layoutMerchantAddress.visibility = View.GONE
                txtPhone.text = "Phone"
                view33.visibility = View.VISIBLE
                space33.visibility = View.VISIBLE
                txtSkills.visibility = View.VISIBLE
                linearSkillss.visibility = View.VISIBLE
                linearLinked.visibility = View.VISIBLE
                space36.visibility = View.GONE
                cardInvite.visibility = View.GONE
                textVerified.visibility = View.GONE
                textVerify.visibility = View.VISIBLE
                updateSkillsMerchant.visibility = View.VISIBLE
                space36.visibility = View.VISIBLE
                view34.visibility = View.VISIBLE
                space35.visibility = View.VISIBLE
                space34.visibility = View.VISIBLE
                tvAccount.text = "Contractor Info"
                imageViewRefer.setImageResource(R.drawable.ic_refer_a_business_blue)
                sellerOthers.visibility = View.VISIBLE
                other1.visibility = View.GONE
                tvLogout.setTextColor(resources.getColor(R.color.blue))
                other1.text = getString(R.string.location)
                layoutAccount.visibility = View.VISIBLE
//                layoutAccountSeller.visibility = View.GONE
                tvInviteFriends.visibility = View.GONE
                relativeGrow.visibility = View.GONE
                cardInvite.visibility = View.GONE
                tvBusinessInfo.visibility = View.VISIBLE
                clAccountLinked.visibility = View.VISIBLE
                linearCustomerOther1.visibility = View.GONE
                linearother2.visibility = View.GONE
//            linearSellerLocations.visibility = View.GONE
                sellerOther.visibility = View.VISIBLE
                includeSeller2.visibility = View.VISIBLE
                includeOther.visibility = View.GONE
                imageView.visibility = View.VISIBLE
                imageViewGradient.visibility = View.VISIBLE
                others2.visibility = View.VISIBLE
                imageView1.visibility = View.GONE
                imageViewGradient1.visibility = View.GONE
                others2.text = "Stats & Activity"
                paymentInfo.setOnClickListener {
                    startActivity(
                        Intent(context, PaymentsMerchantActivity::class.java).putExtra(
                            "profile", "other"
                        )
                            .putExtra("modelProfile", modelGetProfile)
                            .putExtra("bank", bankDetailArray)
                            .putExtra("card", cardDetailArray)
                    )
                }

                manageTeam.setOnClickListener {
                    startActivity(
                        Intent(context, ManageTeamActivity::class.java).putExtra(
                            "notSelected",
                            "SelectionList"
                        )
                    )
                }
            } else if (type == "3") {
                space77.visibility = View.VISIBLE
                view77.visibility = View.VISIBLE
                space78.visibility = View.VISIBLE
                space79.visibility = View.VISIBLE
                view79.visibility = View.VISIBLE
                space65.visibility = View.VISIBLE
                txtSkills.visibility = View.VISIBLE
                view66.visibility = View.VISIBLE
                space68.visibility = View.VISIBLE
                space66.visibility = View.VISIBLE
                space45.visibility = View.GONE
                view45.visibility = View.GONE
                space46.visibility = View.GONE
                linearSkillss.visibility = View.VISIBLE
                space35.visibility = View.VISIBLE
                linearLinked.visibility = View.VISIBLE
                view34.visibility = View.VISIBLE
                address.visibility = View.GONE
                tvAddress.visibility = View.GONE
                space34.visibility = View.VISIBLE
                space33.visibility = View.VISIBLE
                linearBusinessInfo.visibility = View.VISIBLE
                feedbackMerchant.visibility = View.VISIBLE
                notificationMerchant.visibility = View.VISIBLE
                helpCenterMerchant.visibility = View.VISIBLE
                includeSeller2.visibility = View.VISIBLE
                includeStats.visibility = View.VISIBLE
                imageView.visibility = View.VISIBLE
                imageViewGradient.visibility = View.VISIBLE
                imageView1.visibility = View.GONE
                imageViewGradient1.visibility = View.GONE
                spaceBusiness1.visibility = View.VISIBLE
                spaceBusiness2.visibility = View.VISIBLE
                viewBusiness.visibility = View.VISIBLE
                paymentsText.visibility = View.GONE
                includePayments.visibility = View.GONE
                updateSocialTeam.visibility = View.GONE
                space2Team.visibility = View.GONE
                viewTeam.visibility = View.GONE
                spaceTeam.visibility = View.GONE
                updateBusinessInfoBtn.visibility = View.GONE
                statusLinking.visibility = View.VISIBLE
                linearLinked.visibility = View.VISIBLE
                linearother2.visibility = View.GONE
                sellerOther.visibility = View.VISIBLE
                updateSkillsMerchant.visibility = View.VISIBLE
                view33.visibility = View.VISIBLE
                imageViewRefer.setImageResource(R.drawable.ic_refer_a_business_blue)
//                layoutMerchantAddress.visibility = View.GONE
                others2.visibility = View.VISIBLE
                others2.text = "Stats & Activity"
                linearCustomerOther1.visibility = View.GONE
                linearSkills.visibility = View.GONE
                linearTeamSubscription.visibility = View.VISIBLE
//                layoutAccountSeller.visibility = View.VISIBLE
                layoutAccount.visibility = View.VISIBLE
                clAccountLinked.visibility = View.GONE
                cardInvite.visibility = View.GONE
                sellerOthers.visibility = View.VISIBLE
                tvAccount.text = "Employee Info"
                tvBusinessInfo.visibility = View.GONE
                other1.text = "Subscriptions"
                tvLogout.setTextColor(resources.getColor(R.color.blue))

            }
        } else {
            imageViewRefer.setImageResource(R.drawable.ic_refer_a_business_customer)
            sellerOthers.visibility = View.GONE
            txtSkills.visibility = View.GONE
            space36.visibility = View.GONE
            space35.visibility = View.GONE
            space34.visibility = View.GONE
            linearLinked.visibility = View.GONE
            updateSkillsMerchant.visibility = View.GONE
            tvLogout.setTextColor(resources.getColor(R.color.colorSkip))
            tvAccount.text = "Account Info"
            other1.text = "Account Features"
            txtPhone.text = "Phone Number"
            address.visibility = View.VISIBLE
            tvAddress.visibility = View.VISIBLE
            layoutAccount.visibility = View.VISIBLE
            cardInvite.visibility = View.VISIBLE
            spaceBusiness1.visibility = View.GONE
            spaceBusiness2.visibility = View.GONE
            viewBusiness.visibility = View.GONE
            space5.visibility = View.GONE
            space33.visibility = View.GONE
//            layoutAccountSeller.visibility = View.GONE
            tvInviteFriends.visibility = View.GONE
            tvBusinessInfo.visibility = View.GONE
            clAccountLinked.visibility = View.GONE
            relativeGrow.visibility = View.GONE
            sellerOther.visibility = View.GONE
            paymentsText.visibility = View.GONE
            includePayments.visibility = View.GONE
            view33.visibility = View.GONE
            view34.visibility = View.GONE
            linearBusinessInfo.visibility = View.GONE
            updateSocialTeam.visibility = View.GONE
            viewTeam.visibility = View.GONE
            space2Team.visibility = View.GONE
            spaceTeam.visibility = View.GONE
            textVerified.visibility = View.VISIBLE
            textVerify.visibility = View.VISIBLE
            linearSkillss.visibility = View.GONE
//            linearSellerLocations.visibility = View.GONE
            linearother2.visibility = View.VISIBLE
            linearCustomerOther1.visibility = View.VISIBLE
            includeSeller2.visibility = View.GONE
            imageView1.visibility = View.VISIBLE
            includeOther.visibility = View.VISIBLE
            imageViewGradient1.visibility = View.VISIBLE
            others2.visibility = View.VISIBLE
            imageView.visibility = View.GONE
            imageViewGradient.visibility = View.GONE
            space45.visibility = View.VISIBLE
            view45.visibility = View.VISIBLE
            space46.visibility = View.VISIBLE
            others2.text = "Others"
        }

    }

    class MenuFragmentlinking(
        var type: String,
        var modelGetProfile: ModelGetProfile,
        var mViewModel: ManageTeamViewModel,
        var progressBar_small: ConstraintLayout
    ) : RoundedBottomSheetDialogFragment() {


        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val view = inflater.inflate(R.layout.request_linking_sheet, container, false)
            val tvTextTop = view.findViewById<TextView>(R.id.tvTextTop)
            val tvText = view.findViewById<TextView>(R.id.tvText)
            val imageView = view.findViewById<ImageView>(R.id.imageView)
            val no = view.findViewById<TextView>(R.id.no)
            val yes = view.findViewById<TextView>(R.id.yes)
            roundededImageView(imageView)

            if (type == "relink") {
                tvText.text = "Do you want to relink account?"
                tvTextTop.text = "Do you want to relink account?"
                yes.setOnClickListener {
                    progressBar_small.visibility = View.VISIBLE
                    mViewModel.getlinkedUnlinked(
                        "",
                        "1",
                        modelGetProfile.last_associated_id,
                        modelGetProfile.business_name
                    )
                    dismiss()
                }

            } else {
                tvText.text = "Do you want to unlink account?"
                tvTextTop.text = "Do you want to unlink account?"
                yes.setOnClickListener {
                    progressBar_small.visibility = View.VISIBLE
                    mViewModel.getlinkedUnlinked(
                        "",
                        "2",
                        modelGetProfile.business_id,
                        modelGetProfile.business_name
                    )
                    dismiss()
                }
            }


            no.setOnClickListener {
                dismiss()
            }



            return view
        }

        private fun roundededImageView(imageView: ImageView) {
            val curveRadius = 50F

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                imageView.outlineProvider = object : ViewOutlineProvider() {

                    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                    override fun getOutline(view: View?, outline: Outline?) {
                        outline?.setRoundRect(
                            0,
                            0,
                            view!!.width,
                            view.height + curveRadius.toInt(),
                            curveRadius
                        )
                    }
                }
                imageView.clipToOutline = true
            }

        }

    }


    @Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
    @SuppressLint("ValidFragment")
    class MenuFragment(
        var imageView: CircleImageView,
        var imageView1: CircleImageView
    ) : RoundedBottomSheetDialogFragment() {

        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val view = inflater.inflate(R.layout.change_profile_pic_layout, container, false)
            val isSeller =
                Constant.getSharedPrefs(context!!).getBoolean(Constant.IS_SELLER, false)

            val cancelDialog = view.findViewById<TextView>(R.id.cancelDialog)
            val takePicture = view.findViewById<LinearLayout>(R.id.takePicture)
            val galleryLayout = view.findViewById<LinearLayout>(R.id.galleryLayout)
            val textPicture = view.findViewById<TextView>(R.id.textPicture)
            val imgPicture = view.findViewById<ImageView>(R.id.imgPicture)
            val imgGallery = view.findViewById<ImageView>(R.id.imgGallery)
            val textGallery = view.findViewById<TextView>(R.id.textGallery)

            if (isSeller) {
                cancelDialog.setBackgroundResource(R.drawable.bg_seller_button_blue)
            } else {
                cancelDialog.setBackgroundResource(R.drawable.invite_button_background)
            }


            takePicture.setOnClickListener {
                val checkSelfPermission =
                    ContextCompat.checkSelfPermission(
                        this.activity!!,
                        android.Manifest.permission.CAMERA
                    )
                if (checkSelfPermission != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(
                        this.activity!!,
                        arrayOf(android.Manifest.permission.CAMERA),
                        2
                    )
                } else {
                    openCamera()
                }
            }

            galleryLayout.setOnClickListener {
                val checkSelfPermission =
                    ContextCompat.checkSelfPermission(
                        this.activity!!,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE
                    )
                if (checkSelfPermission != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(
                        this.activity!!,
                        arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE),
                        1
                    )
                } else {
                    openAlbum()
                    // dismiss()
                }
            }

            cancelDialog.setOnClickListener {
                dismiss()
            }

            return view
        }


        private fun openCamera() {
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            startActivityForResult(intent, 29)
        }

        private fun openAlbum() {
            val intent = Intent("android.intent.action.GET_CONTENT")
            intent.type = "image/*"
            startActivityForResult(intent, 1)

        }


        override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

            try {
                super.onActivityResult(requestCode, resultCode, data)
                val isSeller =
                    Constant.getSharedPrefs(context!!).getBoolean(Constant.IS_SELLER, false)

                if (requestCode == 1) {
                    //if (data != null) {
                    try {
                        val bitmap2: Uri = data!!.data
                        var typeToSend = ""

                        val isSeller = Constant.getSharedPrefs(context!!)
                            .getBoolean(Constant.IS_SELLER, false)
                        if (isSeller) {
                            typeToSend =
                                Constant.getPrefs(context!!).getString(Constant.type, "")
                        } else {
                            typeToSend = "0"
                        }
                        val file: File = Constant.uploadImage(bitmap2, context!!)
                        //..................................Api FOr Update Image from gallery....................................................................//
                        mViewModel.setImage(file, typeToSend)
                        dismiss()

                        dismiss()

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    // }
                } else if (requestCode == 29) {
                    try {
                        // if (data != null) {

                        var typeToSend = ""

                        val isSeller = Constant.getSharedPrefs(context!!)
                            .getBoolean(Constant.IS_SELLER, false)
                        if (isSeller) {
                            typeToSend =
                                Constant.getPrefs(context!!).getString(Constant.type, "")
                        } else {
                            typeToSend = "0"
                        }
                        val bitmap = data!!.extras.get("data") as Bitmap
                        val file = getImageUri22(bitmap)
                        mViewModel.setImage(file, typeToSend)

                        dismiss()
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
            dismiss()
        }

        fun getImageUriFromBitmap(context: Context, bitmap: Bitmap): Uri {
            val bytes = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
            val path =
                MediaStore.Images.Media.insertImage(
                    context.contentResolver,
                    bitmap,
                    "Title",
                    null
                )
            return Uri.parse(path.toString())
        }

        private fun getImageUri22(inImage: Bitmap): File {
            val root: String = Environment.getExternalStorageDirectory().toString()
            val myDir = File("$root/req_images")
            myDir.mkdirs()
            val fname = "Image_profile_" + System.currentTimeMillis().toString() + ".jpg"
            val file = File(myDir, fname)
            if (file.exists()) {
                file.delete()
            }

            try {
                val out = FileOutputStream(file)
                inImage.compress(Bitmap.CompressFormat.JPEG, 70, out)
                out.flush()
                out.close()
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return file
        }

        private fun saveImage(myBitmap: Bitmap): String {
            val bytes = ByteArrayOutputStream()
            myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes)
            val wallpaperDirectory = File(
                (Environment.getExternalStorageDirectory()).toString() + IMAGE_DIRECTORY
            )
            // have the object build the directory structure, if needed.

            if (!wallpaperDirectory.exists()) {

                wallpaperDirectory.mkdirs()
            }

            try {

                val f = File(
                    wallpaperDirectory, ((Calendar.getInstance()
                        .timeInMillis).toString() + ".jpg")
                )
                f.createNewFile()
                val fo = FileOutputStream(f)
                fo.write(bytes.toByteArray())
                MediaScannerConnection.scanFile(
                    context,
                    arrayOf(f.path),
                    arrayOf("image/jpeg"), null
                )
                fo.close()


                return f.absolutePath
            } catch (e1: IOException) {
                e1.printStackTrace()
            }

            return ""
        }

        companion object {
            private val IMAGE_DIRECTORY = "/demonuts"
        }

    }

//    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
//        if (item!!.itemId == R.id.edit_button) {
//            startActivity(Intent(activity, EditProfileActivity::class.java))
//        }
//        return super.onOptionsItemSelected(item)
//    }

}

