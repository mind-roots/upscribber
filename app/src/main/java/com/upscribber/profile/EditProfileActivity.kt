package com.upscribber.profile

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.upscribber.commonClasses.Constant
import com.upscribber.commonClasses.RoundedBottomSheetDialogFragment
import com.upscribber.upscribberSeller.subsriptionSeller.location.days.AddLocationActivity
import com.upscribber.R
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.activity_edit_profile.*
import kotlinx.android.synthetic.main.edit_profile_customer_layout.*
import kotlinx.android.synthetic.main.edit_profile_merchant_layout.*
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.lang.Exception


@Suppress(
    "NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS",
    "RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS"
)
class EditProfileActivity : AppCompatActivity() {

    private var phoneNumberLength = 0
    lateinit var linearLayout3: LinearLayout
    private lateinit var toolbarTitle: TextView
    private lateinit var toolbar: Toolbar
    lateinit var imageView: CircleImageView
    lateinit var imageViewGradient: CircleImageView
    lateinit var imageView1: CircleImageView
    lateinit var imageViewGradient1: CircleImageView
    lateinit var tvReset: TextView
    lateinit var mViewModel: ProfileViewModel
    var file: File? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profile)
        mViewModel = ViewModelProviders.of(this)[ProfileViewModel::class.java]
        initz()
        setToolbar()
        getProfile()
        click()
        observerInit()
    }

    private fun getProfile() {
        val isSeller = Constant.getSharedPrefs(this).getBoolean(Constant.IS_SELLER, false)
        val modelProfile = intent.getParcelableExtra<ModelGetProfile>("modelProfile")
        val imagePath = Constant.getPrefs(this).getString(Constant.dataImagePath, "")
        val type = Constant.getPrefs(this).getString(Constant.type, "")

        if (isSeller) {
            if (type == "1") {
                etBusinessName1.setText(modelProfile.business_name)
                eTEmail1.setText(modelProfile.business_email)
                eTPhone1.setText(
                    Constant.getSplitPhoneNumberWithoutCode(
                        modelProfile.business_phone,
                        this
                    )
                )
                if (modelProfile.profile_image.isEmpty()) {
                    Glide.with(this).load(R.mipmap.circular_placeholder)
                        .placeholder(R.mipmap.circular_placeholder)
                        .into(imageView)
                } else {
                    Glide.with(this).load(imagePath + modelProfile.profile_image)
                        .placeholder(R.mipmap.circular_placeholder)
                        .into(imageView)
                }
            } else if (type == "2") {
                etBusinessName1.setText(modelProfile.business_name)
                eTEmail1.setText(modelProfile.email)
                eTPhone1.setText(
                    Constant.getSplitPhoneNumberWithoutCode(
                        modelProfile.contact_no,
                        this
                    )
                )
                if (modelProfile.profile_image.isEmpty()) {
                    Glide.with(this).load(R.mipmap.circular_placeholder)
                        .placeholder(R.mipmap.circular_placeholder)
                        .into(imageView)
                } else {
                    Glide.with(this).load(imagePath + modelProfile.profile_image)
                        .placeholder(R.mipmap.circular_placeholder)
                        .into(imageView)
                }
            } else if (type == "3") {
                etBusinessName1.setText(modelProfile.name)
                eTEmail1.setText(modelProfile.email)
                eTPhone1.setText(
                    Constant.getSplitPhoneNumberWithoutCode(
                        modelProfile.contact_no,
                        this
                    )
                )
                if (modelProfile.profile_image.isEmpty()) {
                    Glide.with(this).load(R.mipmap.circular_placeholder)
                        .placeholder(R.mipmap.circular_placeholder)
                        .into(imageView)
                } else {

                    Glide.with(this).load(imagePath + modelProfile.profile_image)
                        .placeholder(R.mipmap.circular_placeholder)
                        .into(imageView)
                }

            }
        } else {
            editTextPersonName.setText(modelProfile.name)
            eTUserEmail.setText(modelProfile.email)
            eTUserNumber.text = Constant.formatPhoneNumber(modelProfile.contact_no)
            if (modelProfile.profile_image.isEmpty()) {
                if (modelProfile.gender == "") {
                    Glide.with(this).load(R.drawable.ic_male_dummy)
                        .placeholder(R.mipmap.circular_placeholder).into(imageView1)
                }
//                if (modelProfile.gender == "female") {
//                    Glide.with(this).load(R.drawable.ic_female_dummy)
//                        .placeholder(R.mipmap.circular_placeholder).into(imageView1)
//                } else if (modelProfile.gender == "male") {
//                    Glide.with(this).load(R.drawable.ic_male_dummy)
//                        .placeholder(R.mipmap.circular_placeholder)
//                        .into(imageView1)
//                }
            } else {
                Glide.with(this).load(imagePath + modelProfile.profile_image)
                    .placeholder(R.mipmap.circular_placeholder)
                    .into(imageView1)
            }
        }

    }

    private fun observerInit() {
        mViewModel.getmDataUpdate().observe(this, androidx.lifecycle.Observer {
            if (it.status == "true") {
                progressBar5.visibility = View.GONE
                val sharedPreferences = getSharedPreferences("updateProfile", Context.MODE_PRIVATE)
                val editor = sharedPreferences.edit()
                editor.putString("profile", "back")
                editor.apply()
                finish()
            }
        })

        mViewModel.getStatus().observe(this, androidx.lifecycle.Observer {
            if (it.msg.isNotEmpty()) {
                progressBar5.visibility = View.GONE
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
            }

        })

    }

    private fun initz() {
        linearLayout3 = findViewById(R.id.linearLayout3)
        imageView = findViewById(R.id.imageView)
        imageViewGradient = findViewById(R.id.imageViewGradient)
        imageView1 = findViewById(R.id.imageView1)
        imageViewGradient1 = findViewById(R.id.imageViewGradient1)
        tvReset = findViewById(R.id.tvReset)

    }


    private fun click() {
        val isSeller = Constant.getSharedPrefs(this).getBoolean(Constant.IS_SELLER, false)
        linearLayout3.setOnClickListener {
            val fragment = MenuFragment(imageView, imageView1)
            fragment.show(supportFragmentManager, fragment.tag)
        }

        eTPhone1.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                val text = eTPhone1.text.toString()
                val textLength = eTPhone1.text.length

                if (text.endsWith("-") || text.endsWith(" ") || text.endsWith(" "))
                    return
                if (textLength == 4) {
                    eTPhone1.setText(StringBuilder(text).insert(text.length - 1, " ").toString())
                    eTPhone1.setSelection(eTPhone1.text.length)
                }
                if (textLength == 8) {
                    eTPhone1.setText(StringBuilder(text).insert(text.length - 1, " ").toString())
                    eTPhone1.setSelection(eTPhone1.text.length)
                }
            }

            override fun afterTextChanged(s: Editable) {

            }
        })


        saveProfile.setOnClickListener {
            val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
            val type = Constant.getPrefs(this).getString(Constant.type, "")
            val phoneNumber = "+1" + eTPhone1.text.toString().trim().replace(" ", "").replace(
                "(",
                ""
            ).replace(")", "").replace("-", "")
            progressBar5.visibility = View.VISIBLE
            if (isSeller) {
                when (type) {
                    "1" -> {
                        mViewModel.updateProfile(
                            auth,
                            "1",
                            etBusinessName1.text.toString().trim(),
                            eTEmail1.text.toString().trim(), phoneNumber,
                            file
                        )
                    }
                    "2" -> {
                        mViewModel.updateProfile(
                            auth,
                            "2",
                            etBusinessName1.text.toString().trim(),
                            eTEmail1.text.toString().trim(), phoneNumber,
                            file
                        )
                    }
                    "3" -> {
                        mViewModel.updateProfile(
                            auth,
                            "3",
                            etBusinessName1.text.toString().trim(),
                            eTEmail1.text.toString().trim(), phoneNumber,
                            file
                        )
                    }
                }
            } else {
                mViewModel.updateProfile(
                    auth,
                    "0",
                    editTextPersonName.text.toString().trim(),
                    eTUserEmail.text.toString().trim(),
                    "", file
                )
            }


        }

        if (isSeller) {
            editButtonPink.visibility = View.GONE
            includeCustomerProfile.visibility = View.GONE
            includeMerchantProfile.visibility = View.VISIBLE
            editButtonGrey.visibility = View.VISIBLE
            imageView1.visibility = View.GONE
            imageViewGradient1.visibility = View.GONE
            imageView.visibility = View.VISIBLE
            imageViewGradient.visibility = View.VISIBLE
            saveProfile.setBackgroundResource(R.drawable.bg_seller_button_blue)


            tvReset.setOnClickListener {
                startActivity(Intent(this, ResetPasswordActivity::class.java))
            }


            editTextPersonName1.setOnFocusChangeListener { view, p1 ->
                if (p1) {
                    editTextPersonName1.setBackgroundResource(R.drawable.bg_seller_blue_outline)
                } else {
                    editTextPersonName1.setBackgroundResource(R.drawable.login_background_white)
                }
            }

            editLocation1.setOnFocusChangeListener { view, p1 ->
                if (p1) {
                    editLocation1.setBackgroundResource(R.drawable.bg_seller_blue_outline)
                } else {
                    editLocation1.setBackgroundResource(R.drawable.login_background_white)
                }
            }

            eTUserNumber1.setOnFocusChangeListener { view, p1 ->
                if (p1) {
                    eTUserNumber1.setBackgroundResource(R.drawable.bg_seller_blue_outline)
                } else {
                    eTUserNumber1.setBackgroundResource(R.drawable.login_background_white)
                }
            }

            eTUserEmail1.setOnFocusChangeListener { view, p1 ->
                if (p1) {
                    eTUserEmail1.setBackgroundResource(R.drawable.bg_seller_blue_outline)
                } else {
                    eTUserEmail1.setBackgroundResource(R.drawable.login_background_white)
                }
            }

            et_password1.setOnFocusChangeListener { view, p1 ->
                if (p1) {
                    linearLayout1.setBackgroundResource(R.drawable.bg_seller_blue_outline)
                    et_password1.setBackgroundColor(resources.getColor(R.color.editBack_button))
                } else {
                    linearLayout1.setBackgroundResource(R.drawable.login_background_white)
                    et_password1.setBackgroundColor(resources.getColor(R.color.colorWhite))
                }
            }

            etBusinessName1.setOnFocusChangeListener { view, p1 ->
                if (p1) {
                    etBusinessName1.setBackgroundResource(R.drawable.bg_seller_blue_outline)
                } else {
                    etBusinessName1.setBackgroundResource(R.drawable.login_background_white)
                }
            }

            eTPhone1.setOnFocusChangeListener { view, p1 ->
                if (p1) {
                    eTPhone1.setBackgroundResource(R.drawable.bg_seller_blue_outline)
                } else {
                    eTPhone1.setBackgroundResource(R.drawable.login_background_white)
                }
            }

            eTEmail1.setOnFocusChangeListener { view, p1 ->
                if (p1) {
                    eTEmail1.setBackgroundResource(R.drawable.bg_seller_blue_outline)
                } else {
                    eTEmail1.setBackgroundResource(R.drawable.login_background_white)
                }
            }

            eTLoc1.setOnFocusChangeListener { view, p1 ->
                if (p1) {
                    eTLoc1.setBackgroundResource(R.drawable.bg_seller_blue_outline)
                } else {
                    eTLoc1.setBackgroundResource(R.drawable.login_background_white)
                }
            }

            eTLoc2.setOnFocusChangeListener { view, p1 ->
                if (p1) {
                    eTLoc2.setBackgroundResource(R.drawable.bg_seller_blue_outline)
                } else {
                    eTLoc2.setBackgroundResource(R.drawable.login_background_white)
                }
            }


        } else {
            saveProfile.setBackgroundResource(R.drawable.invite_button_background)
            editButtonPink.visibility = View.VISIBLE
            includeCustomerProfile.visibility = View.VISIBLE
            includeMerchantProfile.visibility = View.GONE
            imageView.visibility = View.GONE
            imageViewGradient.visibility = View.GONE
            imageView1.visibility = View.VISIBLE
            imageViewGradient1.visibility = View.VISIBLE
            editButtonGrey.visibility = View.GONE

            editTextPersonName.setOnFocusChangeListener { view, p1 ->
                if (p1) {
                    editTextPersonName.setBackgroundResource(R.drawable.bg_edit_selected)
                } else {
                    editTextPersonName.setBackgroundResource(R.drawable.login_background_white)
                }
            }

            eTUserEmail.setOnFocusChangeListener { view, p1 ->
                if (p1) {
                    eTUserEmail.setBackgroundResource(R.drawable.bg_edit_selected)
                } else {
                    eTUserEmail.setBackgroundResource(R.drawable.login_background_white)
                }
            }

            eTUserNumber.setOnFocusChangeListener { view, p1 ->
                if (p1) {
                    eTUserNumber.setBackgroundResource(R.drawable.bg_edit_selected)
                } else {
                    eTUserNumber.setBackgroundResource(R.drawable.login_background_white)
                }
            }

            et_password.setOnFocusChangeListener { view, p1 ->
                if (p1) {
                    linearLayout.setBackgroundResource(R.drawable.bg_edit_selected)
                    et_password.setBackgroundColor(resources.getColor(R.color.editBack_button))
                } else {
                    linearLayout.setBackgroundResource(R.drawable.login_background_white)
                    et_password.setBackgroundColor(resources.getColor(R.color.colorWhite))
                }
            }

            confirm_password.setOnFocusChangeListener { view, p1 ->
                if (p1) {
                    linearLayout4.setBackgroundResource(R.drawable.bg_edit_selected)
                    confirm_password.setBackgroundColor(resources.getColor(R.color.editBack_button))
                } else {
                    linearLayout4.setBackgroundResource(R.drawable.login_background_white)
                    confirm_password.setBackgroundColor(resources.getColor(R.color.colorWhite))
                }
            }

            editLocation.setOnFocusChangeListener { view, p1 ->
                if (p1) {
                    editLocation.setBackgroundResource(R.drawable.bg_edit_selected)
                } else {
                    editLocation.setBackgroundResource(R.drawable.login_background_white)
                }
            }

        }

        addLocation.setOnClickListener {
            startActivity(
                Intent(
                    this,
                    AddLocationActivity::class.java
                )
            )
        }


    }


    // set toolbarTitle bar of the screen
    private fun setToolbar() {
        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        val actionbar: ActionBar? = supportActionBar
        actionbar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)
        }
        title = ""
        toolbarTitle = toolbar.findViewById(R.id.title) as TextView
        toolbarTitle.text = "Edit Profile"

    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        if (item!!.itemId == android.R.id.home) {
            onBackPressed()
        }

        return super.onOptionsItemSelected(item)
    }

    @SuppressLint("ValidFragment")
    class MenuFragment(
        var imageView: CircleImageView,
        var imageView1: CircleImageView
    ) : RoundedBottomSheetDialogFragment() {
        var file: File? = null
        private lateinit var imageUri: Uri
        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val view = inflater.inflate(R.layout.change_profile_pic_layout, container, false)
            val isSeller = Constant.getSharedPrefs(context!!).getBoolean(Constant.IS_SELLER, false)
            val cancelDialog = view.findViewById<TextView>(R.id.cancelDialog)
            val takePicture = view.findViewById<LinearLayout>(R.id.takePicture)
            val galleryLayout = view.findViewById<LinearLayout>(R.id.galleryLayout)

            if (isSeller) {
                cancelDialog.setBackgroundResource(R.drawable.bg_seller_button_blue)
            } else {
                cancelDialog.setBackgroundResource(R.drawable.invite_button_background)
            }

            takePicture.setOnClickListener {
                val checkSelfPermission =
                    ContextCompat.checkSelfPermission(
                        this.activity!!,
                        android.Manifest.permission.CAMERA
                    )
                if (checkSelfPermission != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(
                        this.activity!!,
                        arrayOf(android.Manifest.permission.CAMERA),
                        2
                    )
                } else {
                    openCamera()
                }

            }

            galleryLayout.setOnClickListener {
                val checkSelfPermission =
                    ContextCompat.checkSelfPermission(
                        this.activity!!,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE
                    )
                if (checkSelfPermission != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(
                        this.activity!!,
                        arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE),
                        1
                    )
                } else {
                    openAlbum()
                }
            }

            cancelDialog.setOnClickListener {
                dismiss()
            }

            return view
        }

        private fun openAlbum() {
            val intent = Intent(Intent.ACTION_PICK)
            intent.type = "image/*"
            startActivityForResult(intent, 2)

        }

        private fun openCamera() {
            try {
                val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                startActivityForResult(intent, 1)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }


        override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
            super.onActivityResult(requestCode, resultCode, data)
            val isSeller = Constant.getSharedPrefs(context!!).getBoolean(Constant.IS_SELLER, false)
            if (requestCode == 1) {
                try {
                    if (data != null) {
                        val bitmap = data.extras.get("data") as Bitmap
                        file = Constant.getImageUri(bitmap)
                        (activity as EditProfileActivity).getfile(file!!)
                        if (isSeller) {
                            imageView.setImageBitmap(bitmap)
                        } else {
                            imageView1.setImageBitmap(bitmap)
                        }
                    }
                    dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            if (requestCode == 2) {
                try {
                    val selectedImage: Uri = data!!.data
                    file = File(getPath(selectedImage))
                    (activity as EditProfileActivity).getfile(file!!)
                    val bitmap =
                        MediaStore.Images.Media.getBitmap(activity!!.contentResolver, selectedImage)
                    try {
                        if (file != null) {
                            if (isSeller) {
                                imageView.setImageBitmap(bitmap)
                            } else {
                                imageView1.setImageBitmap(bitmap)
                            }

                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                    dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

        }

        fun getPath(uri: Uri): String {
            val projection = arrayOf(MediaStore.Images.Media.DATA)
            val cursor =
                activity!!.contentResolver.query(uri, projection, null, null, null) ?: return ""
            val column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
            cursor.moveToFirst()
            val s = cursor.getString(column_index)
            cursor.close()
            return s
        }
    }

    private fun getfile(file1: File) {
        file = file1
    }
}





