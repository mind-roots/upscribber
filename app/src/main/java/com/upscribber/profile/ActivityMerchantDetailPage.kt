package com.upscribber.profile

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Outline
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.commonClasses.RoundedBottomSheetDialogFragment
import com.upscribber.notification.ModelNotificationCustomer
import com.upscribber.payment.RefundFlowActivity
import com.upscribber.payment.customerPayments.ModelCustomers
import com.upscribber.payment.customerPayments.PaymentsViewModel
import com.upscribber.payment.invitedCustomers.InvitedCustomersListActivity
import com.upscribber.upscribberSeller.customerBottom.profile.activity.ActivityModel
import com.upscribber.upscribberSeller.subsriptionSeller.subscriptionView.subsManage.SuccessfullyDeleteActivity
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.activity_merchant_detail_page.*


@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
@SuppressLint("Registered")
class ActivityMerchantDetailPage : AppCompatActivity() {

    lateinit var toolbar: Toolbar
    lateinit var toolbarTitle: TextView
    lateinit var mViewModel: PaymentsViewModel
    lateinit var modelCustomers: ModelCustomers
    lateinit var progressBar25: ConstraintLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_merchant_detail_page)
        mViewModel = ViewModelProviders.of(this)[PaymentsViewModel::class.java]
        toolbar = findViewById(R.id.toolbar)
        toolbarTitle = findViewById(R.id.title)
        progressBar25 = findViewById(R.id.progressBar25)
        setToolbar()
        setData()
        observerInit()
        clickListeners()
    }

    private fun clickListeners() {
        linearCustomers.setOnClickListener {
            startActivity(
                Intent(
                    this,
                    InvitedCustomersListActivity::class.java
                ).putExtra("customerList", modelCustomers)
            )
        }

        tvRefund.setOnClickListener {

            if (intent.hasExtra("modelActivities")) {
                val modelActivities = intent.getParcelableExtra<ActivityModel>("modelActivities")
                if (modelActivities.action_taken == "0") {
                    if (modelActivities.sub_type == "0") {
                        val fragment =
                            MenuFragmentUnredeem(mViewModel, progressBar25, modelActivities)
                        fragment.show(supportFragmentManager, fragment.tag)
                    } else if (modelActivities.sub_type == "5") {
                        val fragment = MenuFragment(mViewModel, progressBar25, modelActivities)
                        fragment.show(supportFragmentManager, fragment.tag)
                    }
                }
            } else {
                val notificationData =
                    intent.getParcelableExtra<ModelNotificationCustomer>("notificationData")

                if (notificationData.action_taken == "0") {
                    if (notificationData.type == "5") {
                        val fragment =
                            MenuFragmentNotification(mViewModel, progressBar25, notificationData)
                        fragment.show(supportFragmentManager, fragment.tag)
                    }
                }

            }


        }

    }

    class MenuFragmentUnredeem(
        var mViewModel: PaymentsViewModel,
        var progressBar: ConstraintLayout,
        var modelActivities: ActivityModel?
    ) : RoundedBottomSheetDialogFragment() {

        lateinit var textHeading: TextView
        lateinit var textDescription: TextView
        lateinit var no: TextView
        lateinit var yes: TextView
        lateinit var imageViewTop: ImageView

        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val view = inflater.inflate(R.layout.delete_product_sheet, container, false)
            textHeading = view.findViewById(R.id.textHeading)
            textDescription = view.findViewById(R.id.textDescription)
            no = view.findViewById(R.id.no)
            yes = view.findViewById(R.id.yes)
            imageViewTop = view.findViewById(R.id.imageViewTop)
            textHeading.text = "Unredeem Subscription?"
            textDescription.text = "Are you sure you want to\nUnredeem Subscription?"
            roundImage(imageViewTop)
            yes.setOnClickListener {
                apiImplimentation()
                dismiss()
            }

            no.setOnClickListener {
                dismiss()
            }

            return view

        }

        private fun roundImage(imageViewTop: ImageView) {
            val curveRadius = 50F

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                imageViewTop.outlineProvider = object : ViewOutlineProvider() {

                    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                    override fun getOutline(view: View?, outline: Outline?) {
                        outline?.setRoundRect(
                            0,
                            0,
                            view!!.width,
                            view.height + curveRadius.toInt(),
                            curveRadius
                        )
                    }
                }
                imageViewTop.clipToOutline = true
            }

        }

        private fun apiImplimentation() {
            val auth = Constant.getPrefs(activity!!).getString(Constant.auth_code, "")
            progressBar.visibility = View.VISIBLE
            mViewModel.getUnredeem(
                auth,
                modelActivities!!.model.redeem_id,
                modelActivities!!.model.campaign_name
            )
        }


    }

    class MenuFragment(
        var mViewModel: PaymentsViewModel,
        var progressBar25: ConstraintLayout,
        var modelActivities: ActivityModel
    ) : RoundedBottomSheetDialogFragment() {

        lateinit var imageView28: ImageView

        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val view = inflater.inflate(R.layout.refund_confirmation_sheet, container, false)
            val yes = view.findViewById<TextView>(R.id.yes)
            val imagePath = Constant.getPrefs(activity!!).getString(Constant.dataImagePath, "")
            imageView28 = view.findViewById(R.id.imageView28)
            val imageView16 = view.findViewById<ImageView>(R.id.imageView16)
            val imageCircle = view.findViewById<CircleImageView>(R.id.imageCircle)
            val textView97 = view.findViewById<TextView>(R.id.textView97)
            val textView17 = view.findViewById<TextView>(R.id.textView17)
            val textView124 = view.findViewById<TextView>(R.id.textView124)
            val totalQuantity = view.findViewById<TextView>(R.id.textView130)
            val no = view.findViewById<TextView>(R.id.no)
            val call = view.findViewById<TextView>(R.id.call)
            val totalAmount = view.findViewById<TextView>(R.id.textView360)
            val tvUsedQuantity = view.findViewById<TextView>(R.id.tvUsedQuantity)
            val enterAmount = view.findViewById<EditText>(R.id.enterAmount)

            roundImage()
            Glide.with(activity!!).load(imagePath + modelActivities.model.profile_image)
                .placeholder(R.mipmap.circular_placeholder).into(imageCircle)
            textView124.text = modelActivities.model.business_name
            totalAmount.text = "$" + modelActivities.model.merchant_amount
            totalQuantity.text =
                modelActivities.model.redeemtion_cycle_qty + " " + modelActivities.model.unit
            tvUsedQuantity.text =
                modelActivities.model.redeem_count + " " + modelActivities.model.unit
            enterAmount.setText("$" + modelActivities.model.merchant_amount)

            if (modelActivities.model.contact_no.isNotEmpty()){
                call.visibility = View.VISIBLE
                call.text = Constant.formatPhoneNumber(modelActivities.model.contact_no)
            }else{
                call.visibility = View.GONE
            }


            Glide.with(activity!!).load(imagePath + modelActivities.model.feature_image)
                .placeholder(R.mipmap.circular_placeholder).into(imageView16)

            Glide.with(activity!!).load(imagePath + modelActivities.model.profile_image)
                .placeholder(R.mipmap.circular_placeholder).into(imageCircle)


            val isSeller = Constant.getSharedPrefs(activity!!).getBoolean(Constant.IS_SELLER, false)
            if (isSeller) {
                textView97.setTextColor(resources.getColor(R.color.colorWhite))
                textView97.text =
                    resources.getString(R.string.are_you_sure_you_want_to_process_refund)
                textView17.text = modelActivities.model.campaign_name
                textView124.text = modelActivities.model.customer_name
                imageView28.setImageResource(R.mipmap.bg_popup)

            }

            val auth = Constant.getPrefs(activity!!).getString(Constant.auth_code, "")
            val amount = enterAmount.text.toString().replace("$", "")
            val adminAmount = (20 * amount.toDouble()) / 100
            val merchantAmount = amount.toDouble() - adminAmount
            yes.setOnClickListener {
                when {
                    enterAmount.text.toString().trim() > totalAmount.text.toString().trim() -> Toast.makeText(
                        activity!!,
                        "The maximum refund amount allowed is $" + modelActivities.model.merchant_amount,
                        Toast.LENGTH_SHORT
                    ).show()
                    totalAmount.text.toString().trim() == enterAmount.text.toString().trim() -> {
                        progressBar25.visibility = View.VISIBLE
                        mViewModel.generateRefund(
                            auth,
                            modelActivities.model.transaction_id,
                            modelActivities.model.merchant_amount,
                            "",
                            modelActivities.model.admin_amount, "1"
                        )
                    }
                    else -> {
                        progressBar25.visibility = View.VISIBLE
                        mViewModel.generateRefund(
                            auth,
                            modelActivities.model.transaction_id,
                            merchantAmount.toString(),
                            amount,
                            adminAmount.toString(), "2"
                        )
                    }
                }

                dismiss()

            }

//            call.setOnClickListener {
//                startActivity(
//                    Intent(
//                        Intent.ACTION_DIAL,
//                        Uri.fromParts("tel", modelActivities.model.contact_no, null)
//                    )
//                )
//            }


//            enterAmount.setOnEditorActionListener(object : OnEditorActionListener {
//                override fun onEditorAction(v: TextView, actionId: Int, event: KeyEvent?): Boolean {
//                    if (event != null && event.keyCode === KEYCODE_ENTER || actionId == EditorInfo.IME_ACTION_DONE) {
//                       totalAmount.text = "$" + merchantAmount
//                    }
//                    return false
//                }
//            })


            enterAmount.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(
                    s: CharSequence,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                    if (enterAmount.text.toString().trim() > totalAmount.text.toString().trim()) {
                        Constant.hideKeyboard(activity!!, enterAmount)
                        Toast.makeText(
                            activity!!,
                            "The maximum refund amount allowed is $" + modelActivities.model.merchant_amount,
                            Toast.LENGTH_SHORT
                        ).show()
                        enterAmount.setText("$" + modelActivities.model.merchant_amount)
                    }
                }

                override fun afterTextChanged(s: Editable) {

                }
            })


            no.setOnClickListener {
                mViewModel.deniedRefund(auth, modelActivities.model.order_id)
                progressBar25.visibility = View.VISIBLE
                dismiss()
            }

            return view
        }


        private fun roundImage() {
            val curveRadius = 50F

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                imageView28.outlineProvider = object : ViewOutlineProvider() {

                    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                    override fun getOutline(view: View?, outline: Outline?) {
                        outline?.setRoundRect(
                            0,
                            0,
                            view!!.width,
                            view.height + curveRadius.toInt(),
                            curveRadius
                        )
                    }
                }
                imageView28.clipToOutline = true
            }

        }

    }


    class MenuFragmentNotification(
        var mViewModel: PaymentsViewModel,
        var progressBar25: ConstraintLayout,
        var modelActivities: ModelNotificationCustomer
    ) : RoundedBottomSheetDialogFragment() {

        lateinit var imageView28: ImageView

        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val view = inflater.inflate(R.layout.refund_confirmation_sheet, container, false)
            val yes = view.findViewById<TextView>(R.id.yes)
            val imagePath = Constant.getPrefs(activity!!).getString(Constant.dataImagePath, "")
            imageView28 = view.findViewById(R.id.imageView28)
            val imageView16 = view.findViewById<ImageView>(R.id.imageView16)
            val imageCircle = view.findViewById<CircleImageView>(R.id.imageCircle)
            val textView97 = view.findViewById<TextView>(R.id.textView97)
            val textView17 = view.findViewById<TextView>(R.id.textView17)
            val textView124 = view.findViewById<TextView>(R.id.textView124)
            val totalQuantity = view.findViewById<TextView>(R.id.textView130)
            val no = view.findViewById<TextView>(R.id.no)
            val totalAmount = view.findViewById<TextView>(R.id.textView360)
            val call = view.findViewById<TextView>(R.id.call)
            val tvUsedQuantity = view.findViewById<TextView>(R.id.tvUsedQuantity)
            val enterAmount = view.findViewById<EditText>(R.id.enterAmount)

            roundImage()
            Glide.with(activity!!).load(imagePath + modelActivities.profile_image)
                .placeholder(R.mipmap.circular_placeholder).into(imageCircle)
            textView124.text = modelActivities.business_name
            totalAmount.text = "$" + modelActivities.merchant_amount
            totalQuantity.text = modelActivities.redeemtion_cycle_qty + " " + modelActivities.unit
            tvUsedQuantity.text = modelActivities.redeem_count + " " + modelActivities.unit
            enterAmount.setText("$" + modelActivities.merchant_amount)
            if (modelActivities.contact_no.isNotEmpty()){
                call.visibility = View.VISIBLE
                call.text = Constant.formatPhoneNumber(modelActivities.contact_no)
            }else{
                call.visibility = View.GONE
            }
            Glide.with(activity!!).load(imagePath + modelActivities.feature_image)
                .placeholder(R.mipmap.circular_placeholder).into(imageView16)

            Glide.with(activity!!).load(imagePath + modelActivities.profile_image)
                .placeholder(R.mipmap.circular_placeholder).into(imageCircle)
            textView97.setTextColor(resources.getColor(R.color.colorWhite))
            textView97.text = resources.getString(R.string.are_you_sure_you_want_to_process_refund)
            textView17.text = modelActivities.campaign_name
            textView124.text = modelActivities.customer_name
            imageView28.setImageResource(R.mipmap.bg_popup)


            val auth = Constant.getPrefs(activity!!).getString(Constant.auth_code, "")
            val amount = enterAmount.text.toString().replace("$", "")
            val adminAmount = (20 * amount.toDouble()) / 100
            val merchantAmount = amount.toDouble() - adminAmount
            yes.setOnClickListener {
                when {
                    enterAmount.text.toString().trim() > totalAmount.text.toString().trim() -> Toast.makeText(
                        activity!!,
                        "The maximum refund amount allowed is $" + modelActivities.merchant_amount,
                        Toast.LENGTH_SHORT
                    ).show()
                    totalAmount.text.toString().trim() == enterAmount.text.toString().trim() -> {
                        progressBar25.visibility = View.VISIBLE
                        mViewModel.generateRefund(
                            auth,
                            modelActivities.transaction_id,
                            modelActivities.merchant_amount,
                            "",
                            modelActivities.admin_amount, "1"
                        )
                    }
                    else -> {
                        progressBar25.visibility = View.VISIBLE
                        mViewModel.generateRefund(
                            auth,
                            modelActivities.transaction_id,
                            merchantAmount.toString(),
                            amount,
                            adminAmount.toString(), "2"
                        )
                    }
                }

                dismiss()

            }

//            call.setOnClickListener {
//                startActivity(
//                    Intent(
//                        Intent.ACTION_DIAL,
//                        Uri.fromParts("tel", modelActivities.contact_no, null)
//                    )
//                )
//            }


//            enterAmount.setOnEditorActionListener(object : OnEditorActionListener {
//                override fun onEditorAction(v: TextView, actionId: Int, event: KeyEvent?): Boolean {
//                    if (event != null && event.keyCode === KEYCODE_ENTER || actionId == EditorInfo.IME_ACTION_DONE) {
//                       totalAmount.text = "$" + merchantAmount
//                    }
//                    return false
//                }
//            })


            enterAmount.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(
                    s: CharSequence,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                    if (enterAmount.text.toString().trim() > totalAmount.text.toString().trim()) {
                        Constant.hideKeyboard(activity!!, enterAmount)
                        Toast.makeText(
                            activity!!,
                            "The maximum refund amount allowed is $" + modelActivities.merchant_amount,
                            Toast.LENGTH_SHORT
                        ).show()
                        enterAmount.setText("$" + modelActivities.merchant_amount)
                    }
                }

                override fun afterTextChanged(s: Editable) {

                }
            })


            no.setOnClickListener {
                mViewModel.deniedRefund(auth, modelActivities.order_id)
                progressBar25.visibility = View.VISIBLE
                dismiss()
            }

            return view
        }


        private fun roundImage() {
            val curveRadius = 50F

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                imageView28.outlineProvider = object : ViewOutlineProvider() {

                    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
                    override fun getOutline(view: View?, outline: Outline?) {
                        outline?.setRoundRect(
                            0,
                            0,
                            view!!.width,
                            view.height + curveRadius.toInt(),
                            curveRadius
                        )
                    }
                }
                imageView28.clipToOutline = true
            }

        }

    }


    private fun observerInit() {
        mViewModel.getCustomerList().observe(this, Observer {
            progressBar.visibility = View.GONE
            modelCustomers = it
            if (it.count == "1") {
                NoCustomerInvite.text = modelCustomers.count + " Customer"
            } else {
                NoCustomerInvite.text = modelCustomers.count + " Customers"
            }
        })

        mViewModel.getStatus().observe(this, Observer {
            progressBar.visibility = View.GONE
            if (it.msg == "Invalid auth code") {
                Constant.commonAlert(this)
            } else {
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
            }
        })


        mViewModel.getmDataUnredeem().observe(this, Observer {
            progressBar.visibility = View.GONE
            if (it.status == "true") {
                startActivityForResult(
                    Intent(
                        this,
                        SuccessfullyDeleteActivity::class.java
                    ).putExtra("activity", it.campaign_name), 1
                )

            }
        })


        mViewModel.getmDataGenerateRefund().observe(this, Observer {
            progressBar25.visibility = View.GONE
            startActivityForResult(
                Intent(this, RefundFlowActivity::class.java).putExtra(
                    "from",
                    "ActivityMerchantDetailPage"
                ), 1
            )
            //finish()
        })

        mViewModel.getmDataDenied().observe(this, Observer {
            progressBar25.visibility = View.GONE
            val intent = Intent()
            setResult(1, intent)
            finish()
        })


    }


    private fun setToolbar() {

        if (intent.hasExtra("modelActivities")) {
            val modelPayments = intent.getParcelableExtra<ActivityModel>("modelActivities")
            setSupportActionBar(toolbar)
            title = ""
            toolbarTitle.text = modelPayments.model.customer_name
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)
        } else {
            val notificationData =
                intent.getParcelableExtra<ModelNotificationCustomer>("notificationData")
            setSupportActionBar(toolbar)
            title = ""
            toolbarTitle.text = notificationData.customer_name
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)
        }


    }

    private fun setData() {

        if (intent.hasExtra("modelActivities")) {

            setDataFromMerchant()

        } else {

            setDataFromNotification()

        }
    }

    private fun setDataFromMerchant() {

        val modelPayments = intent.getParcelableExtra<ActivityModel>("modelActivities")
        textBusinessName.text = modelPayments.model.campaign_name
        textDescription.text = modelPayments.model.description
        textFrequency.text = modelPayments.model.frequency_type
        name.text = modelPayments.model.customer_name
        merchantName.text = modelPayments.model.customer_name
        val date = Constant.getDateMonthYear(modelPayments.created_at)
        val time = Constant.getTime(modelPayments.created_at)
        textDate.text = date
        textTime.text = time

        if (modelPayments.model.redeemtion_cycle_qty == "500000") {
            textQuantity.text = "Unlimited"
        } else {
            textQuantity.text = modelPayments.model.redeemtion_cycle_qty
        }



        if (modelPayments.model.total_amount == "") {
            val splitPos = modelPayments.model.price.split(".")
            if (splitPos[1] == "00" || splitPos[1] == "0") {
                textAmount.text = "$" + splitPos[0]
            } else {
                textAmount.text = "$" + modelPayments.model.price
            }
        } else {
            val splitPos = modelPayments.model.total_amount.split(".")
            if (splitPos[1] == "00" || splitPos[1] == "0") {
                textAmount.text = "$" + splitPos[0]
            } else {
                textAmount.text = "$" + modelPayments.model.total_amount
            }
        }


        if (modelPayments.action_taken == "0") {
            tvRefund.setBackgroundResource(R.drawable.bg_seller_button_blue)
        } else {
            tvRefund.setBackgroundResource(R.drawable.blue_button_background_opacity)
        }

        when (modelPayments.sub_type) {
            "1" -> {
                textView73.setBackgroundResource(R.drawable.bg_sea_green_rounded)
                textView73.text = "Share"
                cancelReason.visibility = View.GONE
                cancelReasonspecific.visibility = View.GONE
                linearCustomers.visibility = View.GONE
                view11.visibility = View.GONE
                view12.visibility = View.GONE
            }
            "2" -> {
                textView73.setBackgroundResource(R.drawable.bg_refund)
                textView73.text = "Pause"
                cancelReason.visibility = View.GONE
                cancelReasonspecific.visibility = View.GONE
                linearCustomers.visibility = View.GONE
                view11.visibility = View.GONE
                view12.visibility = View.GONE
            }
            "0" -> {
                if (modelPayments.model.rtype == "2" || modelPayments.model.rtype == "3") {
                    textView73.setBackgroundResource(R.drawable.bg_purple_rounded)
                    textView73.text = "Manual Redeem"
                } else {
                    textView73.setBackgroundResource(R.drawable.bg_pink_rounded)
                    textView73.text = "Redeem"
                }
                view11.visibility = View.GONE
                view12.visibility = View.GONE
                cancelReason.visibility = View.GONE
                cancelReasonspecific.visibility = View.GONE
                linearCustomers.visibility = View.GONE
                tvRefund.text = "Unredeem"
            }
            "5" -> {
                textView73.setBackgroundResource(R.drawable.bg_refund)
                textView73.text = "Refund Request"
                cancelReason.visibility = View.GONE
                cancelReasonspecific.visibility = View.GONE
                linearCustomers.visibility = View.GONE
                view11.visibility = View.GONE
                view12.visibility = View.GONE

            }
            "8" -> {
                textView73.setBackgroundResource(R.drawable.bg_black_rounded)
                textView73.text = "Cancel"
                cancelReason.visibility = View.VISIBLE
                cancelReasonspecific.visibility = View.VISIBLE
                if (modelPayments.model.cancel_reason.isEmpty()){
                    cancelReasonspecific.text = "Credits Refunded"
                    cancelReasonspecific.setCompoundDrawablesWithIntrinsicBounds(
                        R.drawable.ic_ms_apply_credit, 0, 0, 0)
                }else{
                    cancelReasonspecific.text = modelPayments.model.cancel_reason
                    cancelReasonspecific.setCompoundDrawablesWithIntrinsicBounds(
                        R.drawable.ic_bad_service, 0, 0, 0)
                }
                linearCustomers.visibility = View.GONE
                view11.visibility = View.GONE
                view12.visibility = View.GONE
                tvRefund.visibility = View.GONE

            }
            "9" -> {
                textView73.setBackgroundResource(R.drawable.bg_purple_rounded)
                textView73.text = "Renew"
                cancelReason.visibility = View.GONE
                cancelReasonspecific.visibility = View.GONE
                linearCustomers.visibility = View.GONE
                view11.visibility = View.GONE
                view12.visibility = View.GONE
                tvRefund.visibility = View.GONE

            }
            "10" -> {
                textView73.setBackgroundResource(R.drawable.bg_purple_rounded)
                textView73.text = "Refunded"
                cancelReason.visibility = View.GONE
                cancelReasonspecific.visibility = View.GONE
                linearCustomers.visibility = View.GONE
                view11.visibility = View.GONE
                view12.visibility = View.GONE
                tvRefund.visibility = View.GONE
            }
            "7" -> {
                textView73.setBackgroundResource(R.drawable.bg_purple_rounded)
                textView73.text = "Refund Denied"
                cancelReason.visibility = View.GONE
                cancelReasonspecific.visibility = View.GONE
                linearCustomers.visibility = View.GONE
                view11.visibility = View.GONE
                view12.visibility = View.GONE
                tvRefund.visibility = View.GONE
            }
            "4" -> {
                textView73.setBackgroundResource(R.drawable.bg_pink_rounded)
                textView73.text = "Unshared"
                cancelReason.visibility = View.GONE
                cancelReasonspecific.visibility = View.GONE
                linearCustomers.visibility = View.GONE
                view11.visibility = View.GONE
                view12.visibility = View.GONE
            }
            "6" -> {
                textView73.setBackgroundResource(R.drawable.bg_refund)
                textView73.text = "Unpause"
                cancelReason.visibility = View.GONE
                cancelReasonspecific.visibility = View.GONE
                linearCustomers.visibility = View.GONE
                view11.visibility = View.GONE
                view12.visibility = View.GONE
            }
            "11" -> {
                apiImplimentation(modelPayments)
                textView73.setBackgroundResource(R.drawable.bg_purple_rounded)
                textView73.text = "Invited"
                cancelReason.visibility = View.GONE
                cancelReasonspecific.visibility = View.GONE
                linearCustomers.visibility = View.VISIBLE
                view11.visibility = View.VISIBLE
                view12.visibility = View.VISIBLE
            }
            "12" -> {
                textView73.setBackgroundResource(R.drawable.bg_purple_rounded)
                textView73.text = "Business request"
                cancelReason.visibility = View.GONE
                cancelReasonspecific.visibility = View.GONE
                linearCustomers.visibility = View.GONE
                view11.visibility = View.GONE
                view12.visibility = View.GONE
                tvRefund.visibility = View.VISIBLE
            }
        }


    }

    private fun setDataFromNotification() {

        val notificationData =
            intent.getParcelableExtra<ModelNotificationCustomer>("notificationData")

        textBusinessName.text = notificationData.campaign_name
        textDescription.text = notificationData.description
        textFrequency.text = notificationData.frequency_type
        name.text = notificationData.customer_name
        merchantName.text = notificationData.customer_name
        val date = Constant.getDateMonthYear(notificationData.created_at)
        val time = Constant.getTime(notificationData.created_at)
        textDate.text = date
        textTime.text = time


        if (notificationData.redeemtion_cycle_qty == "500000") {
            textQuantity.text = "Unlimited"
        } else {
            textQuantity.text = notificationData.redeemtion_cycle_qty
        }

        if (notificationData.action_taken == "0") {
            tvRefund.setBackgroundResource(R.drawable.bg_seller_button_blue)
        } else {
            tvRefund.setBackgroundResource(R.drawable.blue_button_background_opacity)
        }


        textView73.setBackgroundResource(R.drawable.bg_refund)
        textView73.text = "Refund Request"
        cancelReason.visibility = View.GONE
        cancelReasonspecific.visibility = View.GONE
        linearCustomers.visibility = View.GONE
        view11.visibility = View.GONE
        view12.visibility = View.GONE

        if (notificationData.total_amount.contains(".")) {
            val splitPos = notificationData.total_amount.split(".")
            if (splitPos[1] == "00" || splitPos[1] == "0") {
                textAmount.text = splitPos[0]
            } else {
                textAmount.text = notificationData.total_amount
            }
        }else{
            textAmount.text = notificationData.total_amount
        }


    }

    private fun apiImplimentation(modelPayments: ActivityModel) {
        val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
        mViewModel.getCampaignInvitedCustomer(auth, modelPayments.model.campaign_id)
        progressBar.visibility = View.VISIBLE

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == 1) {
            val intent = Intent()
            setResult(1, intent)
            finish()
        }
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }

}
