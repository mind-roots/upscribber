package com.upscribber.profile

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.upscribberSeller.customerBottom.profile.activity.ActivityModel
import kotlinx.android.synthetic.main.payment_layout.view.*

class ActivityMerchantAdapter(
    private val mContext: Context,
    var stringExtra: String

) : RecyclerView.Adapter<ActivityMerchantAdapter.MyViewHolder>() {

    private var list: ArrayList<ActivityModel> = ArrayList()
    val listen = mContext as MyActivities

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(mContext).inflate(R.layout.payment_layout, parent, false)
        return MyViewHolder(v)

    }

    override fun getItemCount(): Int {
        return list.size

    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = list[position]
        val date = Constant.getDateMonthYear(model.created_at)
        val time = Constant.getTime(model.created_at)

        holder.itemView.textView30.text = model.model.campaign_name
        if (model.model.price == "") {
            val splitPos = model.model.total_amount.split(".")
            if (splitPos[1] == "00" || splitPos[1] == "0") {
                holder.itemView.textView32.text = splitPos[0]
            } else {
                holder.itemView.textView32.text = model.model.total_amount
            }
        } else {
            val splitPos = model.model.price.split(".")
            if (splitPos[1] == "00" || splitPos[1] == "0") {
                holder.itemView.textView32.text = splitPos[0]
            } else {
                holder.itemView.textView32.text = model.model.price
            }
        }

        holder.itemView.textView33.text = date
        holder.itemView.paymentTime.text = time
        holder.itemView.textView36.text = model.model.customer_name


        if (model.sub_type == "1") {
            holder.itemView.typePayment.text = "Share"
            holder.itemView.typePayment.setBackgroundResource(R.drawable.bg_sea_green_rounded)
        } else if (model.sub_type == "2") {

            holder.itemView.typePayment.text = "Pause"
            holder.itemView.typePayment.setBackgroundResource(R.drawable.bg_refund)

        } else if (model.sub_type == "0") {
            if (model.model.rtype == "2" || model.model.rtype == "3") {
                holder.itemView.typePayment.text = "Redeem"
                holder.itemView.typePayment.setBackgroundResource(R.drawable.bg_purple_rounded)
            } else {
                holder.itemView.typePayment.text = "Redeem"
                holder.itemView.typePayment.setBackgroundResource(R.drawable.bg_pink_rounded)
            }
        } else if (model.sub_type == "4") {
            holder.itemView.typePayment.text = "Unshare"
            holder.itemView.typePayment.setBackgroundResource(R.drawable.bg_pink_rounded)

        } else if (model.sub_type == "5") {
            holder.itemView.typePayment.text = "Refund Request"
            holder.itemView.typePayment.setBackgroundResource(R.drawable.bg_refund)

        } else if (model.sub_type == "6") {
            holder.itemView.typePayment.text = "Unpause"
            holder.itemView.typePayment.setBackgroundResource(R.drawable.bg_refund)

        } else if (model.sub_type == "8") {
            holder.itemView.typePayment.text = "Cancel"
            holder.itemView.typePayment.setBackgroundResource(R.drawable.bg_black_rounded)

        } else if (model.sub_type == "9") {
            holder.itemView.typePayment.text = "Renew"
            holder.itemView.typePayment.setBackgroundResource(R.drawable.bg_purple_rounded)
        } else if (model.sub_type == "10") {
            holder.itemView.typePayment.text = "Refunded"
            holder.itemView.typePayment.setBackgroundResource(R.drawable.bg_purple_rounded)
        } else if (model.sub_type == "11") {
            holder.itemView.typePayment.text = "Invited"
            holder.itemView.typePayment.setBackgroundResource(R.drawable.bg_purple_rounded)
        } else if (model.sub_type == "12") {
            holder.itemView.typePayment.text = "Business request"
            holder.itemView.typePayment.setBackgroundResource(R.drawable.bg_purple_rounded)
        } else if (model.sub_type == "7") {
            holder.itemView.typePayment.text = "Refund Denied"
            holder.itemView.typePayment.setBackgroundResource(R.drawable.bg_purple_rounded)
        }

        if (stringExtra == "deatils") {
            holder.itemView.textView36.visibility = View.VISIBLE

        } else {
            holder.itemView.textView36.visibility = View.GONE
        }

        holder.itemView.card_view4.setOnClickListener {
            listen.getResult(model)

//            Intent(mContext.applicationContext, ActivityMerchantDetailPage::class.java)
//            myactivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
//                .putExtra("modelActivities", model)
//            mContext.applicationContext.startActivity(myactivity)
        }
    }

    fun update(it: ArrayList<ActivityModel>) {
        list = it
        notifyDataSetChanged()

    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    interface MyActivities {
        fun getResult(model: ActivityModel)
    }
}