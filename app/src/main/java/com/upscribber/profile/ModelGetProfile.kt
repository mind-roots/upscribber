package com.upscribber.profile

import android.os.Parcel
import android.os.Parcelable
import org.json.JSONObject

class ModelGetProfile() : Parcelable {

    var new_notification_status: String = ""
    var push_setting: String = ""
    var pushValue: String = ""
    var stripe_status: String = ""
    var invite_count: String = ""
    var cash_earned: String = ""
    var notification_count: String = ""
    var schedule: String = ""
    var value: String = ""
    var user_id: String = ""
    var feedback: String = ""
    var payout_date: String = ""
    var available_payout: String = ""
    var selected_payout_method: String = ""
    var payment_method_available: String = ""
    var email_verify: String = ""
    var reg_type: String = ""
    var renew_coupon: String = ""
    var first_time_coupon: String = ""
    var about: String = ""
    var merchant_contact_no: String = ""
    var last_associated_id: String = ""
    var twitter: String = ""
    var instagram: String = ""
    var facebook: String = ""
    var associated_campaign: String = ""
    var address: String = ""
    var associated_date: String = ""
    var created_campaign: String = ""
    var city: String = ""
    var state: String = ""
    var street: String = ""
    var subscriber_since: String = ""
    var revenue: String = ""
    var active_campaign: String = ""
    var logo: String = ""
    var speciality: String = ""
    var business_email: String = ""
    var business_phone: String = ""
    var business_name: String = ""
    var social_id: String = ""
    var profile_image: String = ""
    var zip_code: String = ""
    var name: String = ""
    var contact_no: String = ""
    var email: String = ""
    var gender: String = ""
    var stripe_customer_id: String = ""
    var birthdate: String = ""
    var last_name: String = ""
    var is_merchant: String = ""
    var steps: String = ""
    var business_id: String = ""
    var status: String = ""
    var msg: String = ""
    var wallet: String = ""
    var type: String = ""
    var tax: Int = 0
    var payment_id = ""
    var stripe_account_id = ""
    var payment_method = ""
    var merchant_commission: Int = 0
    var invite_code: String = ""
    var primary_address: ModelPrimaryAddress = ModelPrimaryAddress()
    var billing_address: ModelBillingAddress = ModelBillingAddress()

    constructor(parcel: Parcel) : this() {
        new_notification_status = parcel.readString()
        push_setting = parcel.readString()
        pushValue = parcel.readString()
        stripe_status = parcel.readString()
        invite_count = parcel.readString()
        cash_earned = parcel.readString()
        notification_count = parcel.readString()
        schedule = parcel.readString()
        value = parcel.readString()
        user_id = parcel.readString()
        feedback = parcel.readString()
        payout_date = parcel.readString()
        available_payout = parcel.readString()
        selected_payout_method = parcel.readString()
        payment_method_available = parcel.readString()
        email_verify = parcel.readString()
        reg_type = parcel.readString()
        renew_coupon = parcel.readString()
        first_time_coupon = parcel.readString()
        about = parcel.readString()
        merchant_contact_no = parcel.readString()
        last_associated_id = parcel.readString()
        twitter = parcel.readString()
        instagram = parcel.readString()
        facebook = parcel.readString()
        associated_campaign = parcel.readString()
        address = parcel.readString()
        associated_date = parcel.readString()
        created_campaign = parcel.readString()
        city = parcel.readString()
        state = parcel.readString()
        street = parcel.readString()
        subscriber_since = parcel.readString()
        revenue = parcel.readString()
        active_campaign = parcel.readString()
        logo = parcel.readString()
        speciality = parcel.readString()
        business_email = parcel.readString()
        business_phone = parcel.readString()
        business_name = parcel.readString()
        social_id = parcel.readString()
        profile_image = parcel.readString()
        zip_code = parcel.readString()
        name = parcel.readString()
        contact_no = parcel.readString()
        email = parcel.readString()
        gender = parcel.readString()
        stripe_customer_id = parcel.readString()
        birthdate = parcel.readString()
        last_name = parcel.readString()
        is_merchant = parcel.readString()
        steps = parcel.readString()
        business_id = parcel.readString()
        status = parcel.readString()
        msg = parcel.readString()
        wallet = parcel.readString()
        type = parcel.readString()
        tax = parcel.readInt()
        payment_id = parcel.readString()
        stripe_account_id = parcel.readString()
        payment_method = parcel.readString()
        merchant_commission = parcel.readInt()
        invite_code = parcel.readString()
        primary_address = parcel.readParcelable(ModelPrimaryAddress::class.java.classLoader)
        billing_address = parcel.readParcelable(ModelBillingAddress::class.java.classLoader)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(new_notification_status)
        parcel.writeString(push_setting)
        parcel.writeString(pushValue)
        parcel.writeString(stripe_status)
        parcel.writeString(invite_count)
        parcel.writeString(cash_earned)
        parcel.writeString(notification_count)
        parcel.writeString(schedule)
        parcel.writeString(value)
        parcel.writeString(user_id)
        parcel.writeString(feedback)
        parcel.writeString(payout_date)
        parcel.writeString(available_payout)
        parcel.writeString(selected_payout_method)
        parcel.writeString(payment_method_available)
        parcel.writeString(email_verify)
        parcel.writeString(reg_type)
        parcel.writeString(renew_coupon)
        parcel.writeString(first_time_coupon)
        parcel.writeString(about)
        parcel.writeString(merchant_contact_no)
        parcel.writeString(last_associated_id)
        parcel.writeString(twitter)
        parcel.writeString(instagram)
        parcel.writeString(facebook)
        parcel.writeString(associated_campaign)
        parcel.writeString(address)
        parcel.writeString(associated_date)
        parcel.writeString(created_campaign)
        parcel.writeString(city)
        parcel.writeString(state)
        parcel.writeString(street)
        parcel.writeString(subscriber_since)
        parcel.writeString(revenue)
        parcel.writeString(active_campaign)
        parcel.writeString(logo)
        parcel.writeString(speciality)
        parcel.writeString(business_email)
        parcel.writeString(business_phone)
        parcel.writeString(business_name)
        parcel.writeString(social_id)
        parcel.writeString(profile_image)
        parcel.writeString(zip_code)
        parcel.writeString(name)
        parcel.writeString(contact_no)
        parcel.writeString(email)
        parcel.writeString(gender)
        parcel.writeString(stripe_customer_id)
        parcel.writeString(birthdate)
        parcel.writeString(last_name)
        parcel.writeString(is_merchant)
        parcel.writeString(steps)
        parcel.writeString(business_id)
        parcel.writeString(status)
        parcel.writeString(msg)
        parcel.writeString(wallet)
        parcel.writeString(type)
        parcel.writeInt(tax)
        parcel.writeString(payment_id)
        parcel.writeString(stripe_account_id)
        parcel.writeString(payment_method)
        parcel.writeInt(merchant_commission)
        parcel.writeString(invite_code)
        parcel.writeParcelable(primary_address, flags)
        parcel.writeParcelable(billing_address, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ModelGetProfile> {
        override fun createFromParcel(parcel: Parcel): ModelGetProfile {
            return ModelGetProfile(parcel)
        }

        override fun newArray(size: Int): Array<ModelGetProfile?> {
            return arrayOfNulls(size)
        }
    }


}