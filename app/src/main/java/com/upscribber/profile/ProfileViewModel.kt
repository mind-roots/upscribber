package com.upscribber.profile

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.upscribber.commonClasses.ModelStatusMsg
import com.upscribber.payment.paymentCards.PaymentCardModel
import java.io.File

class ProfileViewModel(application: Application) : AndroidViewModel(application) {

    var profileRepository: ProfileRepository = ProfileRepository(application)

    // ----------------------Profile ------------//
    fun getProfileData(auth: String, type: String, view_type: String, view_id: String) {
        profileRepository.getProfileData(auth, type, view_type, view_id)
    }

    fun getmDataProfile(): LiveData<ModelMainProfile> {
        return profileRepository.getmData()
    }

    fun getDataCard(): MutableLiveData<ArrayList<PaymentCardModel>> {
        return profileRepository.getmDataCard()
    }

    fun getDataBank(): MutableLiveData<ArrayList<PaymentCardModel>> {
        return profileRepository.getmDataBank()
    }

    fun getStatus(): LiveData<ModelStatusMsg> {
        return profileRepository.getmDataFailure()
    }


    //------------------Edit Profile -----------------//
    fun updateProfile(
        auth: String,
        type: String,
        name: String,
        email: String,
        phone: String,
        image: File?
    ) {
        profileRepository.updateProfileData(auth, type, name, email, phone, image)
    }

    fun getmDataUpdate(): LiveData<ModelGetProfile> {
        return profileRepository.getmDataUpdate()
    }

    fun getmDataUpdateSkill(): LiveData<ModelGetProfile> {
        return profileRepository.getmDataUpdateSkill()
    }


    //----------------------Change Password ---------------------//
    fun changePass(auth: String, oldPass: String, newPass: String) {
        profileRepository.updatePassword(auth, oldPass, newPass)

    }

    fun getmDataPassword(): LiveData<ModelUpdatePassword> {
        return profileRepository.getmDataPassword()
    }

//    ------------------------------------------Logout-------------------------------------------------------------//

    fun logoutWebservice(auth: String) {
        profileRepository.logout(auth)
    }

    fun observeLogout(): LiveData<Boolean> {
        return profileRepository.logoutObserver()
    }

    fun updateSkills(auth: String, type: String, skills: String, value: String) {
        profileRepository.updateSkills(auth, type, skills, value)
    }

    fun updateSocial(
        auth: String,
        type: String,
        facebook: String,
        insta: String,
        twitter: String,
        schedule: String
    ) {
        profileRepository.updateSocial(auth, type, facebook, insta,twitter,schedule)
    }


    fun getStatusSuccessful(): LiveData<ModelStatusMsg> {
        return profileRepository.getmDataSuccessful()
    }


    fun setLocation(
        auth: String,
        street: String,
        state: String,
        city: String,
        zipcode: String,
        type: String
    ) {
        profileRepository.updateLocation(auth,street,state,city,zipcode,type)
    }

    fun setImage(uri: File, type: String) {
        profileRepository.updateImage(uri, type)
    }

    fun getImageSuccessful(): LiveData<ModelStatusMsg> {
        return profileRepository.getmDataSuccessfulImage()
    }

    fun sendEmailVerification() {
        profileRepository.sendEmailVerification()
    }

    fun getmDataVerifyEmail(): LiveData<ModelStatusMsg> {
        return profileRepository.getmDataVerifyEmail()
    }

     fun getDeleteAccountCustomer(): LiveData<ModelStatusMsg> {
        return profileRepository.getmDataDeleteCustomet()
    }

    fun deleteCustomer(contactNo: String) {
        return profileRepository.deleteCustomer(contactNo)
    }


}