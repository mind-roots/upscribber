package com.upscribber.profile

import android.os.Parcel
import android.os.Parcelable

class ModelPrimaryAddress() : Parcelable {

    var suite: String = ""
    var street: String = ""
    var city: String = ""
    var state: String = ""
    var zip_code: String = ""

    constructor(parcel: Parcel) : this() {
        suite = parcel.readString()
        street = parcel.readString()
        city = parcel.readString()
        state = parcel.readString()
        zip_code = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(suite)
        parcel.writeString(street)
        parcel.writeString(city)
        parcel.writeString(state)
        parcel.writeString(zip_code)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ModelPrimaryAddress> {
        override fun createFromParcel(parcel: Parcel): ModelPrimaryAddress {
            return ModelPrimaryAddress(parcel)
        }

        override fun newArray(size: Int): Array<ModelPrimaryAddress?> {
            return arrayOfNulls(size)
        }
    }

}
