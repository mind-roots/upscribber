package com.upscribber.favourites

import android.os.Parcel
import android.os.Parcelable

class FavoriteModel() : Parcelable{
    var state_code: String = ""
    var subscription_id: String = ""
    var campaign_id: String = ""
    var price: String = ""
    var discount_price: String = ""
    var unit: String = ""
    var created_at: String = ""
    var updated_at: String = ""
    var frequency_type: String = ""
    var free_trial: String = ""
    var introductory_price: String = ""
    var frequency_value: String = ""
    var business_logo: String = ""
    var subscriber: String = ""
    var description: String = ""
    var campaign_name: String = ""
    var campaign_image: String = ""
    var like_percentage: String = ""
    var like: Int = 0
    var status: String = ""
    var msg: String = ""
    var id: String = ""
    var business_id: String = ""
    var street: String = ""
    var city: String = ""
    var state: String = ""
    var zip_code: String = ""
    var lat: String = ""
    var long: String = ""
    var store_timing: String = ""
    var free_trial_qty: String = ""

    var favorite : Boolean = false

    constructor(parcel: Parcel) : this() {
        state_code = parcel.readString()
        subscription_id = parcel.readString()
        campaign_id = parcel.readString()
        price = parcel.readString()
        discount_price = parcel.readString()
        unit = parcel.readString()
        created_at = parcel.readString()
        updated_at = parcel.readString()
        frequency_type = parcel.readString()
        free_trial = parcel.readString()
        introductory_price = parcel.readString()
        frequency_value = parcel.readString()
        business_logo = parcel.readString()
        subscriber = parcel.readString()
        description = parcel.readString()
        campaign_name = parcel.readString()
        campaign_image = parcel.readString()
        like_percentage = parcel.readString()
        like = parcel.readInt()
        status = parcel.readString()
        msg = parcel.readString()
        id = parcel.readString()
        business_id = parcel.readString()
        street = parcel.readString()
        city = parcel.readString()
        state = parcel.readString()
        zip_code = parcel.readString()
        lat = parcel.readString()
        long = parcel.readString()
        store_timing = parcel.readString()
        free_trial_qty = parcel.readString()
        favorite = parcel.readByte() != 0.toByte()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(state_code)
        parcel.writeString(subscription_id)
        parcel.writeString(campaign_id)
        parcel.writeString(price)
        parcel.writeString(discount_price)
        parcel.writeString(unit)
        parcel.writeString(created_at)
        parcel.writeString(updated_at)
        parcel.writeString(frequency_type)
        parcel.writeString(free_trial)
        parcel.writeString(introductory_price)
        parcel.writeString(frequency_value)
        parcel.writeString(business_logo)
        parcel.writeString(subscriber)
        parcel.writeString(description)
        parcel.writeString(campaign_name)
        parcel.writeString(campaign_image)
        parcel.writeString(like_percentage)
        parcel.writeInt(like)
        parcel.writeString(status)
        parcel.writeString(msg)
        parcel.writeString(id)
        parcel.writeString(business_id)
        parcel.writeString(street)
        parcel.writeString(city)
        parcel.writeString(state)
        parcel.writeString(zip_code)
        parcel.writeString(lat)
        parcel.writeString(long)
        parcel.writeString(store_timing)
        parcel.writeString(free_trial_qty)
        parcel.writeByte(if (favorite) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<FavoriteModel> {
        override fun createFromParcel(parcel: Parcel): FavoriteModel {
            return FavoriteModel(parcel)
        }

        override fun newArray(size: Int): Array<FavoriteModel?> {
            return arrayOfNulls(size)
        }
    }

}