package com.upscribber.favourites

import android.os.Bundle
import android.view.*
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.commonClasses.Constant
import com.upscribber.home.*
import com.upscribber.R
import kotlinx.android.synthetic.main.fragment_favourites.*


class FavouritesFragment : Fragment(), FavoriteInterface {

    private lateinit var mFavoriteRecyclerView: RecyclerView
    private lateinit var homeIcon: ArrayList<FavoriteModel>
    private lateinit var favoritesAdapter: FavoritesAdapter
    lateinit var mViewModel: FavouriteViewModel
    private var arrayFavSubscription: ArrayList<FavoriteModel> = ArrayList()
    lateinit var progressFav: LinearLayout
    var position1 = 0

    override fun onCreateView(
        inflater: LayoutInflater, container:
        ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        val view: View = inflater.inflate(R.layout.fragment_favourites, container, false)
        mViewModel = ViewModelProviders.of(this)[FavouriteViewModel::class.java]
//        setHasOptionsMenu(true)
        mFavoriteRecyclerView = view.findViewById(R.id.mFavoriteRecyclerView)
        progressFav = view.findViewById(R.id.progressFav)

        observerInit()
        setAdapter()
        return view
    }

    override fun onResume() {
        super.onResume()
        apiImplementation()
    }


    private fun observerInit() {
        mViewModel.getmDataHomeSubscription().observe(activity!!, Observer {
            if (it.status == "true") {
                if (arrayFavSubscription.size > 0) {
                    progressFav.visibility = View.GONE
                    arrayFavSubscription.removeAt(position1)
                    favoritesAdapter.update(arrayFavSubscription)
                    apiImplementation()
                } else {
                    tvNoData.visibility = View.VISIBLE
                    imageView82.visibility = View.VISIBLE
                    tvNoDataDescription.visibility = View.VISIBLE
                    mFavoriteRecyclerView.visibility = View.GONE
                }

            }
        })

        mViewModel.getFavouriteData().observe(this, Observer {
            if (it.size > 0) {
                tvNoData.visibility = View.GONE
                imageView82.visibility = View.GONE
                mFavoriteRecyclerView.visibility = View.VISIBLE
                tvNoDataDescription.visibility = View.GONE
                arrayFavSubscription.clear()
                arrayFavSubscription.addAll(it)
                progressFav.visibility = View.GONE
                favoritesAdapter.update(it)
            } else {
                imageView82.visibility = View.VISIBLE
                tvNoDataDescription.visibility = View.VISIBLE
                tvNoData.visibility = View.VISIBLE
                mFavoriteRecyclerView.visibility = View.GONE
                progressFav.visibility = View.GONE
            }
        })

        mViewModel.getStatus().observe(this, Observer {
            tvNoData.visibility = View.VISIBLE
            mFavoriteRecyclerView.visibility = View.GONE
            imageView82.visibility = View.VISIBLE
            tvNoDataDescription.visibility = View.VISIBLE
            progressFav.visibility = View.GONE
            if (it.msg == "Invalid auth code"){
                Constant.commonAlert(activity!!)
            }else if (it.msg != "No product available in favorites."){
                Toast.makeText(activity, it.msg, Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun apiImplementation() {
        val auth = Constant.getPrefs(activity!!).getString(Constant.auth_code, "")
        progressFav.visibility = View.VISIBLE
        mViewModel.getFavouritesList(auth)
    }


    private fun setAdapter() {
        mFavoriteRecyclerView.layoutManager = LinearLayoutManager(activity)
        favoritesAdapter = FavoritesAdapter(activity!!, this)
        mFavoriteRecyclerView.adapter = favoritesAdapter

    }


    override fun addToFav(position: Int) {
        position1 = position
        val auth = Constant.getPrefs(activity!!).getString(Constant.auth_code, "")
        mViewModel.setFavourite(auth, arrayFavSubscription[position])
//        val model = homeIcon[position]
//        model.favorite = !model.favorite
//        homeIcon[position] = model
//        favoritesAdapter.notifyDataSetChanged()
    }


}