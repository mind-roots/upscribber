package com.upscribber.favourites

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.upscribber.commonClasses.ModelStatusMsg

class FavouriteViewModel(application: Application) : AndroidViewModel(application) {
    var favouriteRepository : FavouriteRepository = FavouriteRepository(application)

    fun getFavouritesList(auth: String) {
        favouriteRepository.getFavouritesData(auth)
    }

    fun getFavouriteData(): LiveData<ArrayList<FavoriteModel>> {
        return favouriteRepository.getmData()
    }

    fun getStatus(): LiveData<ModelStatusMsg> {
        return favouriteRepository.getmDataFailure()
    }

    fun setFavourite(auth: String, model: FavoriteModel) {
        favouriteRepository.setFavourite(auth,model)
    }

    fun getmDataHomeSubscription(): LiveData<FavoriteModel> {
        return favouriteRepository.getmHomeSubscription()
    }

}