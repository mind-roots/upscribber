package com.upscribber.favourites

import android.content.Context
import android.content.Intent
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.upscribber.home.FavoriteInterface
import com.upscribber.subsciptions.merchantSubscriptionPages.MerchantSubscriptionViewActivity
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import kotlinx.android.synthetic.main.favorite_recycler.view.*
import java.math.BigDecimal
import java.math.RoundingMode

class FavoritesAdapter(
    private val mContext: Context,
    listen: FavouritesFragment
) :
    RecyclerView.Adapter<FavoritesAdapter.MyViewHolder>() {

    var listener = listen as FavoriteInterface
    private var list: ArrayList<FavoriteModel> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(mContext).inflate(R.layout.favorite_recycler, parent, false)
        return MyViewHolder(v)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = list[position]
//        holder.itemView.favorite_image.setImageResource(model.image)
        holder.itemView.tv_brazillian.text = model.campaign_name
        holder.itemView.tv_person_count.text = (model.subscriber + " subscribers")
        holder.itemView.tv_long_text.text = model.description


        val cost = Constant.getCalculatedPrice(model.discount_price, model.price)
        val splitPos = cost.split(".")
        if (splitPos[1] == "00" || splitPos[1] == "0") {
            holder.itemView.tv_cost.text = "$" + splitPos[0]
        }else{
            holder.itemView.tv_cost.text = "$" + BigDecimal(cost).setScale(2, RoundingMode.HALF_UP)
        }
        val actualCost = model.price.toDouble().toString()
        val splitActualCost = actualCost.split(".")
        if (splitActualCost[1] == "00" || splitActualCost[1] == "0") {
            holder.itemView.tv_last_text.text = "$" + splitActualCost[0]
        }else{
            holder.itemView.tv_last_text.text = "$" + actualCost.toInt()
        }

        holder.itemView.tv_off.text = model.discount_price.toDouble().toInt().toString() + "% off"
        Glide.with(mContext).load(model.campaign_image).placeholder(R.mipmap.placeholder_subscription).into(holder.itemView.favorite_image)
        Glide.with(mContext).load(model.business_logo).placeholder(R.mipmap.app_icon).into(holder.itemView.imgLogo)

        if (model.street.isNullOrEmpty()) {
            holder.itemView.textView36.text = model.city
        } else {
            holder.itemView.textView36.text = model.city + "," + model.state_code
        }

        holder.itemView.tv_last_text.paintFlags = holder.itemView.tv_last_text.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG

        if (model.free_trial == "No free trial" && (model.introductory_price == "0" ||model.introductory_price == "0.00")) {
            holder.itemView.tv_free_map.visibility = View.GONE
        } else if (model.free_trial != "No free trial"){
            holder.itemView.tv_free_map.visibility = View.VISIBLE
            holder.itemView.tv_free_map.text = "Free"
            holder.itemView.tv_free_map.setBackgroundResource(R.drawable.intro_price_background)
        }else{
            holder.itemView.tv_free_map.visibility = View.VISIBLE
            holder.itemView.tv_free_map.text = "Intro"
            holder.itemView.tv_free_map.setBackgroundResource(R.drawable.home_free_background)
        }

        if (model.like_percentage == "0") {
            holder.itemView.tv_percentage.visibility = View.GONE
        } else {
            holder.itemView.tv_percentage.visibility = View.VISIBLE
            holder.itemView.tv_percentage.text = model.like_percentage + "%"
        }
        val like = Constant.checkFav(model.campaign_id, mContext)
        holder.itemView.addFav1.setOnClickListener {
            listener.addToFav(position)
            if (like) {
                holder.itemView.addFav1.setImageResource(R.drawable.ic_favorite_fill)
            } else {
                holder.itemView.addFav1.setImageResource(R.drawable.ic_favorite_border_black_24dp)

            }
        }


        if (like) {
            holder.itemView.addFav1.setImageResource(R.drawable.ic_favorite_fill)
        } else {
            holder.itemView.addFav1.setImageResource(R.drawable.ic_favorite_border_black_24dp)

        }


        holder.itemView.setOnClickListener {
            mContext.startActivity(
                Intent(
                    mContext, MerchantSubscriptionViewActivity::class.java).putExtra("modelSubscriptionView", model.campaign_id)
                    .putExtra("modelSubscription",model)
            )

        }

    }

    fun update(it: java.util.ArrayList<FavoriteModel>) {
        list = it
        notifyDataSetChanged()

    }



    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)


}
