package com.upscribber.favourites

import android.app.Application
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.google.common.reflect.TypeToken
import com.google.gson.Gson
import com.upscribber.commonClasses.Constant
import com.upscribber.commonClasses.ModelStatusMsg
import com.upscribber.commonClasses.WebServicesCustomers
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class FavouriteRepository(var application: Application) {

    val mData = MutableLiveData<ArrayList<FavoriteModel>>()
    val mDataFailure = MutableLiveData<ModelStatusMsg>()
    val mDataSubscription = MutableLiveData<FavoriteModel>()

    fun getFavouritesData(auth: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.getFavourite(auth)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        val arrayFavouriteData = ArrayList<FavoriteModel>()
                        val favString = ArrayList<String>()
                        if (status == "true") {
                            val data = json.getJSONArray("data")
                            for (i in 0 until data.length()) {
                                val favData = data.optJSONObject(i)
                                val location = favData.optJSONObject("location")
                                val favouriteModel = FavoriteModel()

                                favouriteModel.subscription_id =
                                    favData.optString("subscription_id")
                                favouriteModel.campaign_id = favData.optString("campaign_id")
                                favString.add(favouriteModel.campaign_id)
                                favouriteModel.introductory_price =
                                    favData.optString("introductory_price")
                                favouriteModel.price = favData.optString("price")
                                favouriteModel.discount_price = favData.optString("discount_price")
                                favouriteModel.subscriber = favData.optString("subscriber")
                                favouriteModel.unit = favData.optString("unit")
                                favouriteModel.description = favData.optString("description")
                                favouriteModel.campaign_name = favData.optString("campaign_name")
                                favouriteModel.campaign_image = favData.optString("campaign_image")
                                favouriteModel.frequency_type = favData.optString("frequency_type")
                                favouriteModel.free_trial = favData.optString("free_trial")
                                favouriteModel.free_trial_qty = favData.optString("free_trial_qty")
                                favouriteModel.frequency_value =
                                    favData.optString("frequency_value")
                                favouriteModel.business_logo = favData.optString("business_logo")
                                favouriteModel.like_percentage =
                                    favData.optString("like_percentage")
                                //----------------------------- from Location Object -------------------------//

                                favouriteModel.id = location.optString("id")
                                favouriteModel.business_id = location.optString("business_id")
                                favouriteModel.street = location.optString("street")
                                favouriteModel.city = location.optString("city")
                                favouriteModel.state = location.optString("state")
                                favouriteModel.zip_code = location.optString("zip_code")
                                favouriteModel.lat = location.optString("lat")
                                favouriteModel.long = location.optString("long")
                                favouriteModel.state_code = location.optString("state_code")
                                favouriteModel.store_timing = location.optString("store_timing")
                                favouriteModel.created_at = location.optString("created_at")
                                favouriteModel.updated_at = location.optString("updated_at")
                                favouriteModel.status = status
                                favouriteModel.msg = msg
                                favouriteModel.like = 1
                                arrayFavouriteData.add(favouriteModel)
                            }
                            modelStatus.status = status
                            modelStatus.msg = msg
                            mData.value = arrayFavouriteData
                            val mPrefs = application.getSharedPreferences(
                                "data",
                                AppCompatActivity.MODE_PRIVATE
                            )
                            val prefsEditor = mPrefs.edit()
                            val gson = Gson()
                            val json = gson.toJson(favString)
                            prefsEditor.putString("favData", json)
                            prefsEditor.apply()
                            Log.e("favData", json)
                        } else {
                            val mPrefs = application.getSharedPreferences(
                                "data",
                                AppCompatActivity.MODE_PRIVATE
                            )
                            val prefsEditor = mPrefs.edit()
                            prefsEditor.putString("favData", "")
                            prefsEditor.apply()
                            modelStatus.msg = msg
                            mDataFailure.value = modelStatus
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus
            }

        })
    }

    fun getmDataFailure(): LiveData<ModelStatusMsg> {
        return mDataFailure
    }

    fun getmData(): LiveData<ArrayList<FavoriteModel>> {
        return mData
    }


    //-----------------------------Fav Api------------------------//
    fun setFavourite(auth: String, model: FavoriteModel) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()
        var likeOrUnlike = 1
        if (model.like.equals(1)) {
            likeOrUnlike = 2
        } else {
            likeOrUnlike = 1
        }
        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> =
            service.setFavorite(auth, model.campaign_id, likeOrUnlike.toString())
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()
                        if (status == "true") {
                            model.status = status
                            model.msg = msg
                            var arrayList: ArrayList<String> = ArrayList()
                            val mPrefs = application.getSharedPreferences(
                                "data",
                                AppCompatActivity.MODE_PRIVATE
                            )
                            val gson = Gson()
                            val getFav = mPrefs.getString("favData", "")
                            if (getFav != "") {
                                arrayList = gson.fromJson<ArrayList<String>>(
                                    getFav,
                                    object : TypeToken<ArrayList<String>>() {
                                    }.type
                                )
                                var exist = 0
                                for (item in arrayList) {
                                    if (model.campaign_id == item) {
                                        exist = 1
                                        break
                                    }
                                }
                                if (exist == 0) {
                                    arrayList.add(model.campaign_id)
                                }else{
                                    if(msg.contains("Favorite removed successfully.")){
                                        arrayList.remove(model.campaign_id)
                                    }
                                }
                            } else {
                                arrayList.add(model.campaign_id)
                            }

                            val prefsEditor = mPrefs.edit()
                            val json = gson.toJson(arrayList)
                            prefsEditor.putString("favData", json)
                            prefsEditor.apply()
                            Log.e("favData", json)
                            if (model.like == 1) {
                                model.like = 2
                            } else {
                                model.like = 1
                            }

                        } else {
                            model.status = status
                            modelStatus.msg = msg
                        }
                        mDataSubscription.value = model
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus

            }

        })

    }

    fun getmHomeSubscription(): LiveData<FavoriteModel> {
        return mDataSubscription
    }

}