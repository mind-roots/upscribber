package com.upscribber.discover

import android.content.Context
import android.content.Intent
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.upscribber.commonClasses.Constant
import com.upscribber.home.FavoriteInterface
import com.upscribber.subsciptions.merchantSubscriptionPages.MerchantSubscriptionViewActivity
import com.upscribber.R
import kotlinx.android.synthetic.main.discover_second_recycler.view.*


class DiscoverSecondAdapter(
    private val mContext: Context,
    listen: DiscoverActivity
) :
    RecyclerView.Adapter<DiscoverSecondAdapter.MyViewHolder>() {

    private var list: ArrayList<DiscoverSecondModel> = ArrayList()

    var listener = listen as FavoriteInterface

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(mContext).inflate(R.layout.discover_second_recycler, parent, false)
        return MyViewHolder(v)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        val model = list[position]
        val imagePath  = Constant.getPrefs(mContext).getString(Constant.dataImagePath, "")

        if (model.feature_image.isEmpty()){
            Glide.with(mContext).load(R.mipmap.placeholder_subscription).into(holder.itemView.favorite_image)
        }else{
            Glide.with(mContext).load(imagePath + model.feature_image).into(holder.itemView.favorite_image)
        }



//        holder.itemView.imagelogo.setImageResource(model.image_logo)
//        holder.itemView.tv_mars_spa.text = model.text_one
//        holder.itemView.tv_percentage.text = model.text_two
        holder.itemView.tv_brazillian.text = model.name
        holder.itemView.tv_person_count.text = model.subscriber + " subscribbr"
        holder.itemView.textDescription.text = model.description

//        holder.itemView.tv_free.text = model.text_nine
        holder.itemView.textLocation.text = model.city + " " + model.state
        holder.itemView.tv_last_text.text = "$" + model.price.toDouble().toInt()
        holder.itemView.tv_off.text = model.discount_price.toDouble().toInt().toString() + "% off"
        holder.itemView.tv_cost.text ="$" + Constant.getCalculatedPrice(model.discount_price,model.price)

        holder.itemView.addFav.setOnClickListener {
            listener.addToFav(position)
            if (model.like == 1) {
                holder.addFav.setImageResource(R.drawable.ic_favorite_fill)
            } else {
                holder.addFav.setImageResource(R.drawable.ic_favorite_border_black_24dp)

            }
        }

        if (model.like == 1) {
            holder.addFav.setImageResource(R.drawable.ic_favorite_fill)
        } else {
            holder.addFav.setImageResource(R.drawable.ic_favorite_border_black_24dp)

        }

        holder.itemView.setOnClickListener {
            mContext.startActivity(Intent(mContext, MerchantSubscriptionViewActivity::class.java).putExtra("modelSubscriptionView",model.campaign_id)
                .putExtra("modelSubscription",model))

        }

//        setAnimation(holder.itemView, position)


        holder.tv_last_text.paintFlags = holder.tv_last_text.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG

    }

    fun update(it: ArrayList<DiscoverSecondModel>) {
        list = it
        notifyDataSetChanged()

    }

//    private fun setAnimation(itemView: View, position: Int) {
//
//        if (position > lastPosition) {
//            val animation = AnimationUtils.loadAnimation(mContext, android.R.anim.slide_in_left)
//            itemView.startAnimation(animation)
//            lastPosition = position
//        }
//    }


    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var tv_last_text: TextView = itemView.findViewById(R.id.tv_last_text)
        var addFav: ImageView = itemView.findViewById(R.id.addFav)


    }

//    override fun onViewAttachedToWindow(holder: MyViewHolder) {
//        super.onViewAttachedToWindow(holder)
//        holder.itemView.clearAnimation()
//    }
//
//    override fun onViewDetachedFromWindow(holder: MyViewHolder) {
//        super.onViewDetachedFromWindow(holder)
//        setFadeAnimation(holder.itemView, holder.adapterPosition)
//    }
//
//    private var lastPosition: Int = -1
//
//    fun setFadeAnimation(view: View, position: Int) {
//
//        if (position > lastPosition) {
//            val animation = AnimationUtils.loadAnimation(mContext, R.anim.up_from_bottom)
//            view.startAnimation(animation)
//        } else {
//            val animation = AnimationUtils.loadAnimation(mContext, android.R.anim.fade_in)
//            view.startAnimation(animation)
//        }
//
//        lastPosition = position
//
//    }

}