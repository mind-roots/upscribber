package com.upscribber.discover

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.upscribber.home.BackGroundInterface
import com.upscribber.R
import com.upscribber.home.WhatsNewModelOne
import kotlinx.android.synthetic.main.home_whats_new_recycler.view.*


class DiscoverFirstAdapter(
    private val mContext: Context
) : RecyclerView.Adapter<DiscoverFirstAdapter.MyViewHolder>() {

    var listener = mContext as BackGroundInterface
    private var list: ArrayList<WhatsNewModelOne> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):
            MyViewHolder {
        val v = LayoutInflater.from(mContext)
            .inflate(R.layout.home_whats_new_recycler, parent, false)
        return MyViewHolder(v)

    }

    override fun getItemCount(): Int {
        return list.size

    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        val model = list[position]


        if (position == 0) {
            holder.itemView.tv_skinCare.text = "All"
            Glide.with(mContext).load(R.mipmap.circular_placeholder)
                .placeholder(R.mipmap.circular_placeholder).into(holder.itemView.img_icons)
            Glide.with(mContext).load(R.mipmap.circularprogress_black)
                .placeholder(R.mipmap.circular_placeholder).into(holder.itemView.img_icons1)
        } else {
            holder.itemView.tv_skinCare.text = model.name
            Glide.with(mContext).load(model.selected_image)
                .placeholder(R.mipmap.circular_placeholder).into(holder.itemView.img_icons)
            Glide.with(mContext).load(model.unselected_image)
                .placeholder(R.mipmap.circular_placeholder).into(holder.itemView.img_icons1)
        }



        if (model.background) {
            holder.itemView.linear1.setBackgroundResource(R.drawable.home_whats_new_background_pink)
            holder.itemView.img_icons.visibility = View.GONE
            holder.itemView.img_icons1.visibility = View.VISIBLE
            holder.itemView.tv_skinCare.setTextColor(Color.WHITE)

        } else {
            holder.itemView.linear1.setBackgroundResource(R.drawable.home_whts_new_bg_nonselected)
            holder.itemView.img_icons.visibility = View.VISIBLE
            holder.itemView.img_icons1.visibility = View.GONE
            holder.itemView.tv_skinCare.setTextColor(Color.parseColor("#8d8e9c"))

        }
        holder.itemView.setOnClickListener {
            listener.ChangeBackground(position)

        }
    }

    fun update(it: ArrayList<WhatsNewModelOne>) {
        list = it
        notifyDataSetChanged()
    }


    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)


}