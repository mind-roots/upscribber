package com.upscribber.discover

import android.os.Parcel
import android.os.Parcelable


class DiscoverSecondModel() : Parcelable{

    var like: Int = 0
    var msg: String = ""
    var status: String = ""
    var campaign_image: String = ""
    var campaign_name: String = ""
    var description: String = ""
    var subscriber: String = ""
    var discount_price: String = ""
    var price: String = ""
    var business_name: String = ""
    var campaign_id: String = ""
    var name: String = ""
    var feature_image: String = ""
    var favorite : Boolean = false

    var id: String = ""
    var business_id: String = ""
    var street: String = ""
    var city: String = ""
    var state: String = ""
    var zip_code: String = ""
    var lat: String = ""
    var long: String = ""
    var store_timing: String = ""
    var created_at: String = ""
    var updated_at: String = ""

    constructor(parcel: Parcel) : this() {
        like = parcel.readInt()
        msg = parcel.readString()
        status = parcel.readString()
        campaign_image = parcel.readString()
        campaign_name = parcel.readString()
        description = parcel.readString()
        subscriber = parcel.readString()
        discount_price = parcel.readString()
        price = parcel.readString()
        business_name = parcel.readString()
        campaign_id = parcel.readString()
        name = parcel.readString()
        feature_image = parcel.readString()
        favorite = parcel.readByte() != 0.toByte()
        id = parcel.readString()
        business_id = parcel.readString()
        street = parcel.readString()
        city = parcel.readString()
        state = parcel.readString()
        zip_code = parcel.readString()
        lat = parcel.readString()
        long = parcel.readString()
        store_timing = parcel.readString()
        created_at = parcel.readString()
        updated_at = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(like)
        parcel.writeString(msg)
        parcel.writeString(status)
        parcel.writeString(campaign_image)
        parcel.writeString(campaign_name)
        parcel.writeString(description)
        parcel.writeString(subscriber)
        parcel.writeString(discount_price)
        parcel.writeString(price)
        parcel.writeString(business_name)
        parcel.writeString(campaign_id)
        parcel.writeString(name)
        parcel.writeString(feature_image)
        parcel.writeByte(if (favorite) 1 else 0)
        parcel.writeString(id)
        parcel.writeString(business_id)
        parcel.writeString(street)
        parcel.writeString(city)
        parcel.writeString(state)
        parcel.writeString(zip_code)
        parcel.writeString(lat)
        parcel.writeString(long)
        parcel.writeString(store_timing)
        parcel.writeString(created_at)
        parcel.writeString(updated_at)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<DiscoverSecondModel> {
        override fun createFromParcel(parcel: Parcel): DiscoverSecondModel {
            return DiscoverSecondModel(parcel)
        }

        override fun newArray(size: Int): Array<DiscoverSecondModel?> {
            return arrayOfNulls(size)
        }
    }

}