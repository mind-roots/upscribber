package com.upscribber.discover

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.upscribber.commonClasses.Constant
import com.upscribber.filters.*
import com.upscribber.filters.Redemption.RedemptionFilterFragment
import com.upscribber.home.BackGroundInterface
import com.upscribber.home.FavoriteInterface
import com.upscribber.R
import com.upscribber.subsciptions.merchantSubscriptionPages.similarBrands.SimilarBrandsViewModel
import kotlinx.android.synthetic.main.activity_discover.*
import kotlinx.android.synthetic.main.discover_layout.*
import kotlinx.android.synthetic.main.filter_header.*
import org.json.JSONArray

class DiscoverActivity : AppCompatActivity(), BackGroundInterface, FavoriteInterface,
    FilterInterface,
    DrawerLayout.DrawerListener, FilterFragment.SetFilterHeaderBackground {

    private lateinit var mRvOne: RecyclerView
    private lateinit var mRvTwo: RecyclerView
    private lateinit var searchView: SearchView
    private var backgroundChange: ArrayList<DiscoverFirstModel> = ArrayList()
    private lateinit var homeIcon: ArrayList<DiscoverSecondModel>
    private lateinit var discoverFirstAdapter: DiscoverFirstAdapter
    private lateinit var homeWhatsNewTwoAdapter: DiscoverSecondAdapter
    private lateinit var toolbarTitle: TextView
    private lateinit var mfilters: TextView
    var drawerOpen = 0
    private lateinit var btnFilter: TextView
    private lateinit var filter_done: TextView
    private lateinit var fragmentManager: FragmentManager
    private lateinit var fragmentTransaction: FragmentTransaction
    private lateinit var mreset: TextView
    lateinit var toolbar: Toolbar
    lateinit var mViewModel: SimilarBrandsViewModel
    var position1 = 0
    private lateinit var arrayDiscover: ArrayList<DiscoverSecondModel>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_discover)
        mViewModel = ViewModelProviders.of(this)[SimilarBrandsViewModel::class.java]
        initz()
        setToolbar()
        setAdapter()
        setFilters()
        clickListeners()
        apiImplimentation()
        observersInit()
        drawerLayout1.addDrawerListener(this)
    }

    private fun observersInit() {
        mViewModel.getStatus().observe(this, Observer {
            if (it.msg == "Invalid auth code") {
                Constant.commonAlert(this)
            } else {
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
            }
        })

        mViewModel.getmDataAllSubscription().observe(this, Observer {
            getResponse()
            progressBar9.visibility = View.GONE

        })

        mViewModel.getmDataFav().observe(this, Observer {
            if (it.status == "true") {
                arrayDiscover[position1] = it
                val editorFeatues = Constant.getPrefs(this).edit()
                editorFeatues.putString(Constant.dataSimilarSubscription, arrayDiscover.toString())
                editorFeatues.apply()
                homeWhatsNewTwoAdapter.update(arrayDiscover)
            }

        })

    }

    private fun getResponse() {
        arrayDiscover =
            getArrayListFav(Constant.dataSimilarSubscription) as ArrayList<DiscoverSecondModel>
        homeWhatsNewTwoAdapter.update(arrayDiscover)
    }

    private fun getArrayListFav(key: String): List<DiscoverSecondModel>? {
        val prefs = Constant.getPrefs(this)
        val gson = Gson()
        val json = prefs.getString(key, null)
        var jsonType = JSONArray(json)
        val type = gson.fromJson<List<DiscoverSecondModel>>(
            json,
            object : TypeToken<List<DiscoverSecondModel>>() {

            }.type
        )

        return type

    }

    private fun apiImplimentation() {
        val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
        val modelCampaignId = intent.getStringExtra("modelSubscriptionView")
        mViewModel.getListSimilarSubscription(auth, modelCampaignId, "1", "2")
        progressBar9.visibility = View.VISIBLE

    }


    override fun onDrawerStateChanged(newState: Int) {
        var hh = newState
    }

    override fun onDrawerSlide(drawerView: View, slideOffset: Float) {
        var f = slideOffset
    }

    override fun onDrawerClosed(drawerView: View) {
        drawerOpen = 0
    }

    override fun onDrawerOpened(drawerView: View) {
        drawerOpen = 1
        drawerLayout1.openDrawer(GravityCompat.END)
        inflateFragment(FilterFragment(this))
        filterImgBack.visibility = View.GONE
        mfilters.text = "Filters"
        mreset.visibility = View.VISIBLE
    }


    override fun addToFav(position: Int) {
        position1 = position
        val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
        mViewModel.setFavourite(auth, arrayDiscover[position])
    }

    override fun filterHeaderBackground(s: String) {
        if (s == "new") {
            filterHeader.setBackgroundDrawable(getDrawable(R.drawable.filter_header_select_category))
        } else {
            filterHeader.setBackgroundDrawable(getDrawable(R.drawable.filter_header))

        }

    }


    private fun clickListeners() {

        filter_done.setOnClickListener {
            onBackPressed()
        }


    }

    private fun initz() {
        toolbar = findViewById(R.id.toolbar)
        btnFilter = findViewById(R.id.btnFilter1)
        mreset = findViewById(R.id.reset)
        mfilters = findViewById(R.id.filters)
        filter_done = findViewById(R.id.filter_done)
        toolbarTitle = findViewById(R.id.title)
        searchView = findViewById(R.id.searchView)
        mRvOne = findViewById(R.id.rvOne)
        mRvTwo = findViewById(R.id.rvTwo)


    }

    private fun setToolbar() {
        setSupportActionBar(toolbar)
        title = ""
        toolbarTitle.text = "Similar Subscriptions"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)

    }

    private fun setFilters() {

        btnFilter.setOnClickListener {
            drawerOpen = 1
            Constant.hideKeyboard(this, searchView)
            if (searchView != null) {
                searchView.setQuery("", false);
                searchView.clearFocus();
            }
            drawerLayout1.openDrawer(GravityCompat.END)
            inflateFragment(FilterFragment(this))
            filterImgBack.visibility = View.GONE
            mfilters.text = "Filters"
            mreset.visibility = View.VISIBLE
        }
    }


    private fun inflateFragment(fragment: Fragment) {
        fragmentManager = supportFragmentManager
        this.fragmentTransaction = fragmentManager.beginTransaction()
        fragmentTransaction.replace(R.id.containerSide1, fragment)
        fragmentTransaction.commit()
    }

    override fun onBackPressed() {
        if (drawerOpen == 1) {
            drawerOpen = 0
            drawerLayout1.closeDrawer(GravityCompat.END)
        } else {
            super.onBackPressed()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onResume() {
        super.onResume()
        Constant.hideKeyboard(this, searchView)
    }


    private fun setAdapter() {
//        mRvOne.layoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)
//        backgroundChange = getListTwo()
//        discoverFirstAdapter = DiscoverFirstAdapter(this)
//        mRvOne.adapter = discoverFirstAdapter

        mRvTwo.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        mRvTwo.isNestedScrollingEnabled = false
        homeWhatsNewTwoAdapter = DiscoverSecondAdapter(this, this)
        mRvTwo.adapter = homeWhatsNewTwoAdapter

    }

//    private fun getListTwo(): ArrayList<DiscoverFirstModel> {
//
//
//        val arrayList: ArrayList<DiscoverFirstModel> = ArrayList()
//
//        val whatsNewModelOne = DiscoverFirstModel()
//        whatsNewModelOne.backgroundSelected = true
//        whatsNewModelOne.imgIcons = R.drawable.activities_slug
//        whatsNewModelOne.imgIcons1 = R.drawable.activities_grey_slug
//        whatsNewModelOne.heading = "Activities"
//        arrayList.add(whatsNewModelOne)
//
//        val whatsNewModelOne1 = DiscoverFirstModel()
//        whatsNewModelOne1.backgroundSelected = false
//        whatsNewModelOne1.imgIcons = R.drawable.automotive_slug
//        whatsNewModelOne1.imgIcons1 = R.drawable.automotive_grey_slug
//        whatsNewModelOne1.heading = "Automotive"
//        arrayList.add(whatsNewModelOne1)
//
//        val whatsNewModelOne2 = DiscoverFirstModel()
//        whatsNewModelOne2.backgroundSelected = false
//        whatsNewModelOne2.imgIcons = R.drawable.coffee_shops_slug
//        whatsNewModelOne2.imgIcons1 = R.drawable.coffee_shops_grey_slug
//        whatsNewModelOne2.heading = "Coffee shops"
//        arrayList.add(whatsNewModelOne2)
//
//        val whatsNewModelOne3 = DiscoverFirstModel()
//        whatsNewModelOne3.backgroundSelected = false
//        whatsNewModelOne3.imgIcons = R.drawable.dance_arts_slug
//        whatsNewModelOne3.imgIcons1 = R.drawable.dance_arts_grey_slug
//        whatsNewModelOne3.heading = "Dance arts"
//        arrayList.add(whatsNewModelOne3)
//
//        val whatsNewModelOne4 = DiscoverFirstModel()
//        whatsNewModelOne4.backgroundSelected = false
//        whatsNewModelOne4.imgIcons = R.drawable.education_slug
//        whatsNewModelOne4.imgIcons1 = R.drawable.education_grey_slug
//        whatsNewModelOne4.heading = "Education"
//        arrayList.add(whatsNewModelOne4)
//
//        val whatsNewModelOne5 = DiscoverFirstModel()
//        whatsNewModelOne5.backgroundSelected = false
//        whatsNewModelOne5.imgIcons = R.drawable.groomins_spa_slug
//        whatsNewModelOne5.imgIcons1 = R.drawable.groomins_spa_grey_slug
//        whatsNewModelOne5.heading = "Groomins spa"
//        arrayList.add(whatsNewModelOne5)
//
//        val whatsNewModelOne6 = DiscoverFirstModel()
//        whatsNewModelOne6.backgroundSelected = false
//        whatsNewModelOne6.imgIcons = R.drawable.health_fitness_slug
//        whatsNewModelOne6.imgIcons1 = R.drawable.health_fitness_grey_slug
//        whatsNewModelOne6.heading = "Health Fitness"
//        arrayList.add(whatsNewModelOne6)
//
//        val whatsNewModelOne7 = DiscoverFirstModel()
//        whatsNewModelOne7.backgroundSelected = false
//        whatsNewModelOne7.imgIcons = R.drawable.membership_tickets_slug
//        whatsNewModelOne7.imgIcons1 = R.drawable.membership_tickets_grey_slug
//        whatsNewModelOne7.heading = "Membership Tickets"
//        arrayList.add(whatsNewModelOne7)
//
//        val whatsNewModelOne8 = DiscoverFirstModel()
//        whatsNewModelOne8.backgroundSelected = false
//        whatsNewModelOne8.imgIcons = R.drawable.mens_salon_slug
//        whatsNewModelOne8.imgIcons1 = R.drawable.mens_salon_grey_slug
//        whatsNewModelOne8.heading = "Mens Salon"
//        arrayList.add(whatsNewModelOne8)
//
//        val whatsNewModelOne10 = DiscoverFirstModel()
//        whatsNewModelOne10.backgroundSelected = false
//        whatsNewModelOne10.imgIcons = R.drawable.pets_slug
//        whatsNewModelOne10.imgIcons1 = R.drawable.pets_grey_slug
//        whatsNewModelOne10.heading = "Pets"
//        arrayList.add(whatsNewModelOne10)
//
//        val whatsNewModelOne11 = DiscoverFirstModel()
//        whatsNewModelOne11.backgroundSelected = false
//        whatsNewModelOne11.imgIcons = R.drawable.sports_clubs_slug
//        whatsNewModelOne11.imgIcons1 = R.drawable.sports_clubs_grey_slug
//        whatsNewModelOne11.heading = "Sports clubs"
//        arrayList.add(whatsNewModelOne11)
//
//        val whatsNewModelOne12 = DiscoverFirstModel()
//        whatsNewModelOne12.backgroundSelected = false
//        whatsNewModelOne12.imgIcons = R.drawable.suitcase_slug
//        whatsNewModelOne12.imgIcons1 = R.drawable.suitcase_grey_slug
//        whatsNewModelOne12.heading = "Suitcase"
//        arrayList.add(whatsNewModelOne12)
//
//        val whatsNewModelOne9 = DiscoverFirstModel()
//        whatsNewModelOne9.backgroundSelected = false
//        whatsNewModelOne9.imgIcons = R.drawable.others_slug
//        whatsNewModelOne9.imgIcons1 = R.drawable.others_grey_slug
//        whatsNewModelOne9.heading = "Others"
//        arrayList.add(whatsNewModelOne9)
//
//        return arrayList
//
//    }

    override fun ChangeBackground(position: Int) {
        val model = backgroundChange[position]
        if (model.backgroundSelected) {
            return

        } else {
            for (notSelected in backgroundChange) {
                notSelected.backgroundSelected = false
            }
            model.backgroundSelected = true
        }


        backgroundChange[position] = model
        discoverFirstAdapter.notifyDataSetChanged()
    }


    override fun nextFragments(i: Int) {
        when (i) {
            1 -> {
                inflateFragment(SortByFragment())
                mreset.visibility = View.GONE
                filterImgBack.visibility = View.VISIBLE
                filters.text = "Sort By"

                filterImgBack.setOnClickListener {
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"
                }

                filter_done.setOnClickListener {
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"
                }
            }


            2 -> {
                inflateFragment(CategoryFilterFragment(this))
                mreset.visibility = View.GONE
                filterImgBack.visibility = View.VISIBLE
                filters.text = "Category"

                filterImgBack.setOnClickListener {
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"

                }
                filter_done.setOnClickListener {
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"
                }
            }


            3 -> {
                inflateFragment(BrandsFilterFragment())
                mreset.visibility = View.GONE
                filterImgBack.visibility = View.VISIBLE
                filters.text = "Brands"

                filterImgBack.setOnClickListener {
                    Constant.hideKeyboard(this, searchView)
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"

                }

                filter_done.setOnClickListener {
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"
                }
            }
            4 -> {
                inflateFragment(RedemptionFilterFragment())
                mreset.visibility = View.GONE
                filterImgBack.visibility = View.VISIBLE
                filters.text = "Redemption"

                filterImgBack.setOnClickListener {
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"

                }
                filter_done.setOnClickListener {
                    inflateFragment(FilterFragment(this))
                    mreset.visibility = View.VISIBLE
                    filterImgBack.visibility = View.GONE
                    filters.text = "Filters"
                }
            }
        }

    }
}