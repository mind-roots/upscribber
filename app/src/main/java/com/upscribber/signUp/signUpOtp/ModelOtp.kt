package com.upscribber.signUp.signUpOtp

import android.os.Parcel
import android.os.Parcelable

class ModelOtp() : Parcelable{
    var value: String = ""
    var msg: String = ""
    var contact: String = ""
    var otp: String = ""
    var status: String = ""

    constructor(parcel: Parcel) : this() {
        value = parcel.readString()
        msg = parcel.readString()
        contact = parcel.readString()
        otp = parcel.readString()
        status = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(value)
        parcel.writeString(msg)
        parcel.writeString(contact)
        parcel.writeString(otp)
        parcel.writeString(status)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ModelOtp> {
        override fun createFromParcel(parcel: Parcel): ModelOtp {
            return ModelOtp(parcel)
        }

        override fun newArray(size: Int): Array<ModelOtp?> {
            return arrayOfNulls(size)
        }
    }


}
