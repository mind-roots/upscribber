package com.upscribber.signUp.signupInfo

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.provider.Settings
import android.text.SpannableString
import android.text.Spanned
import android.text.TextPaint
import android.text.method.HideReturnsTransformationMethod
import android.text.method.LinkMovementMethod
import android.text.method.PasswordTransformationMethod
import android.text.style.ClickableSpan
import android.text.style.UnderlineSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.widget.AppCompatCheckBox
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.commonClasses.RoundedBottomSheetDialogFragment
import com.upscribber.dashboard.DashboardActivity
import com.upscribber.payment.paymentCards.WebPagesOpenActivity
import com.upscribber.signUp.SignUpViewModel
import com.upscribber.signUp.clicks
import kotlinx.android.synthetic.main.fragment_sign_up.*
import java.io.File
import java.lang.Exception


@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class SignUpFragment : Fragment() {

    lateinit var update: clicks
    lateinit var mbuttonLogin: TextView
    lateinit var mbackToOtp: ImageView
    lateinit var tv_password: TextView
    lateinit var textView159: TextView
    lateinit var checkbox: AppCompatCheckBox
    lateinit var et_password: EditText
    lateinit var et_name: EditText
    lateinit var et_email: EditText
    lateinit var profilePictureClick: ImageView
    lateinit var plusbtn: ImageView
    lateinit var linearLayout: LinearLayout
    lateinit var mViewModel: SignUpViewModel
    lateinit var name: String
    lateinit var email: String

    companion object {
        var file: File? = null
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_sign_up, container, false)
        mViewModel = ViewModelProviders.of(this)[SignUpViewModel::class.java]

        mbuttonLogin = view.findViewById(R.id.goToSubscription)
        textView159 = view.findViewById(R.id.textView159)
        linearLayout = view.findViewById(R.id.linearLayout)
        mbackToOtp = view.findViewById(R.id.back_to_otp)
        checkbox = view.findViewById(R.id.checkbox)
        checkbox = view.findViewById(R.id.checkbox)
        et_password = view.findViewById(R.id.et_password)
        et_name = view.findViewById(R.id.et_name)
        tv_password = view.findViewById(R.id.tv_password)
        et_email = view.findViewById(R.id.et_email)
        profilePictureClick = view.findViewById(R.id.profilePictureClick)
        plusbtn = view.findViewById(R.id.plusbtn)
        et_name.setText(update.getName())
        et_email.setText(update.getEmail())
        et_password.setText(update.getPassword())
        setData()
        clickListeners()
        observerInit()
        spannableText()

        return view
    }

    private fun spannableText() {
        val spannableString =
            SpannableString("I agree to Upscribbr's Terms & Conditions and Privacy Policy")

        //1st click
        spannableString.setSpan(
            clickableSpanforSigleText, 23, 41, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        //2nd click
        spannableString.setSpan(
            clickableSpanforPrivacyPolicy, 46, 60, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        )

        textView159.movementMethod = LinkMovementMethod.getInstance()

        spannableString.setSpan(UnderlineSpan(), 23, 41, 0)
        textView159.text = spannableString

        spannableString.setSpan(UnderlineSpan(), 46, 60, 0)
        textView159.text = spannableString

    }

    val clickableSpanforSigleText = object : ClickableSpan() {
        @SuppressLint("SetJavaScriptEnabled")
        override fun onClick(textView: View) {
            startActivity(
                Intent(
                    activity,
                    WebPagesOpenActivity::class.java
                ).putExtra("terms", "terms")
            )

        }

        override fun updateDrawState(ds: TextPaint) {
            super.updateDrawState(ds)
            ds.isUnderlineText = false
        }
    }

    //set the click on the spannable
    val clickableSpanforPrivacyPolicy = object : ClickableSpan() {
        @SuppressLint("SetJavaScriptEnabled")
        override fun onClick(textView: View) {
            startActivity(
                Intent(
                    activity,
                    WebPagesOpenActivity::class.java
                ).putExtra("privacy", "privacy")
            )

        }
    }

    private fun setData() {
        name = update.login()
        email = update.emailLogin()
        et_name.setText(name)
        et_email.setText(email)

        if ((name.isEmpty() && email.isNotEmpty()) || (name.isNotEmpty() && email.isEmpty()) || (name.isNotEmpty() && email.isNotEmpty())) {
            linearLayout.visibility = View.GONE
            tv_password.visibility = View.GONE
        } else {
            linearLayout.visibility = View.GONE
            tv_password.visibility = View.GONE
        }
    }

    private fun observerInit() {

        mViewModel.getEmail().observe(this, androidx.lifecycle.Observer {
            if (it.status == "true") {
                val number = update.getNumber()
                val phoneNumber = number.replace(" ", "")
                val fireBaseToken: String = Constant.getPrefs(context!!).getString("deviceId", "")
                val android_id = Settings.Secure.getString(context!!.contentResolver, Settings.Secure.ANDROID_ID)
                mViewModel.getSignUp(
                    "+1$phoneNumber",
                    update.getEmail(),
                    "",
                    "1",
                    android_id,
                    fireBaseToken,
                    "",
                    "",
                    "",
                    update.getName(), "", file
                )
            } else {
                Toast.makeText(context, it.msg, Toast.LENGTH_LONG).show()
            }
        })

        mViewModel.getSignedUp().observe(this, androidx.lifecycle.Observer {
            if (it.status == "true") {
                progressBarEmail.visibility = View.GONE
                val intent = Intent(activity, DashboardActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
            } else {
                Toast.makeText(context, it.msg, Toast.LENGTH_LONG).show()
            }
        })

        mViewModel.getStatus().observe(this, androidx.lifecycle.Observer {
            if (it.msg.isNotEmpty()) {
                progressBarEmail.visibility = View.GONE
                Toast.makeText(context, it.msg, Toast.LENGTH_LONG).show()
            }
        })

    }

    private fun clickListeners() {

        mbuttonLogin.setOnClickListener {
            Constant.hideKeyboard(activity!!, et_name)
            update.setName(et_name.text.toString())
            update.setEmail(et_email.text.toString())
            update.setPassword(et_password.text.toString())
            validations()

        }

        mbackToOtp.setOnClickListener {
            update.back(3)
        }
//        minviteCode.setOnClickListener{
//            val intent = Intent(activity, InviteCodeActivity::class.java)
//            startActivity(intent)
//        }

        checkbox.setOnCheckedChangeListener { buttonView, isChecked ->
            if (!isChecked) {
                // show password
                et_password.transformationMethod = HideReturnsTransformationMethod.getInstance()
                checkbox.text = "Hide"
            } else {
                // hide password
                et_password.transformationMethod = PasswordTransformationMethod.getInstance()
                checkbox.text = "Show"
            }
        }

        profilePictureClick.setOnClickListener {
            val fragment = MenuFragment(profilePictureClick,plusbtn)
            fragment.show(activity!!.supportFragmentManager, fragment.tag)
        }

    }

    private fun validations() {

        if ((name.isEmpty() || email.isNotEmpty()) || (name.isNotEmpty() && email.isEmpty()) && file != null) {
            if (et_name.text.toString().trim().isEmpty() || et_email.text.toString().trim().isEmpty()) {
                Toast.makeText(
                    activity,
                    "Please Enter the following information",
                    Toast.LENGTH_LONG
                )
                    .show()
            } else if (et_name.text.toString().trim().isEmpty()) {
                Toast.makeText(activity, "Please enter your name to continue", Toast.LENGTH_LONG).show()
            } else if (et_email.text.toString().trim().isEmpty()) {
                Toast.makeText(activity, "Please enter your email address to continue", Toast.LENGTH_LONG).show()
            } else if (!checkBox2.isChecked) {
                Toast.makeText(
                    activity,
                    "Please select that you agree to Upscribbr's Terms & Conditions and Privacy Policy to continue",
                    Toast.LENGTH_LONG
                ).show()
            } else if (!Constant.isValidEmailId(et_email.text.toString().trim())) {
                Toast.makeText(activity, "Please enter your valid email address to continue", Toast.LENGTH_LONG)
                    .show()
            }  else {
                progressBarEmail.visibility = View.VISIBLE
                mViewModel.getEmailId(et_email.text.toString())
            }
        } else {
            if (et_name.text.toString().trim().isEmpty() && et_email.text.toString().trim().isEmpty()) {
                Toast.makeText(
                    activity,
                    "Please Enter the following information",
                    Toast.LENGTH_LONG
                )
                    .show()
            } else if (et_name.text.toString().trim().isEmpty()) {
                Toast.makeText(activity, "Please enter your name to continue", Toast.LENGTH_LONG).show()
            } else if (et_email.text.toString().trim().isEmpty()) {
                Toast.makeText(activity, "Please enter your email address to continue", Toast.LENGTH_LONG).show()
            } else if (!checkBox2.isChecked) {
                Toast.makeText(
                    activity,
                    "Please select that you agree to Upscribbr's Terms & Conditions and Privacy Policy to continue",
                    Toast.LENGTH_LONG
                ).show()
            } else if (!Constant.isValidEmailId(et_email.text.toString().trim())) {
                Toast.makeText(activity, "Please enter your valid email address to continue", Toast.LENGTH_LONG)
                    .show()
            }  else {
                progressBarEmail.visibility = View.VISIBLE
                mViewModel.getEmailId(et_email.text.toString())
            }
        }

    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        update = context as clicks
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        update = activity as clicks
    }

    @SuppressLint("ValidFragment")
    class MenuFragment(
        var imageView: ImageView,
        var plusbtn: ImageView
    ) : RoundedBottomSheetDialogFragment() {
        private lateinit var imageUri: Uri
        override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View? {
            val view = inflater.inflate(R.layout.change_profile_pic_layout, container, false)
            val isSeller = Constant.getSharedPrefs(context!!).getBoolean(Constant.IS_SELLER, false)
            val cancelDialog = view.findViewById<TextView>(R.id.cancelDialog)
            val takePicture = view.findViewById<LinearLayout>(R.id.takePicture)
            val galleryLayout = view.findViewById<LinearLayout>(R.id.galleryLayout)

            if (isSeller) {
                cancelDialog.setBackgroundResource(R.drawable.bg_seller_button_blue)
            } else {
                cancelDialog.setBackgroundResource(R.drawable.invite_button_background)
            }

            takePicture.setOnClickListener {
                val checkSelfPermission =
                    ContextCompat.checkSelfPermission(
                        this.activity!!,
                        android.Manifest.permission.CAMERA
                    )
                if (checkSelfPermission != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(
                        this.activity!!,
                        arrayOf(android.Manifest.permission.CAMERA),
                        2
                    )
                } else {
                    openCamera()
                }

            }

            galleryLayout.setOnClickListener {
                val checkSelfPermission =
                    ContextCompat.checkSelfPermission(
                        this.activity!!,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE
                    )
                if (checkSelfPermission != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(
                        this.activity!!,
                        arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE),
                        1
                    )
                } else {
                    openAlbum()
                }
            }

            cancelDialog.setOnClickListener {
                dismiss()
            }

            return view
        }

        private fun openAlbum() {
            val intent = Intent(Intent.ACTION_PICK)
            intent.type = "image/*"
            startActivityForResult(intent, 2)

        }

        private fun openCamera() {
            try {
                val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                startActivityForResult(intent, 1)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }


        override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
            super.onActivityResult(requestCode, resultCode, data)
            if (requestCode == 1) {
                try {
                    if (data != null) {
                        val bitmap = data.extras.get("data") as Bitmap
                        file = Constant.getImageUri(bitmap)
//                        imageView.setBackgroundResource(0)
                        plusbtn.visibility = View.GONE
                        imageView.setImageBitmap(bitmap)

                    }
                    dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

            if (requestCode == 2) {
                try {
                    val selectedImage: Uri = data!!.data
                    file = File(getPath(selectedImage))
                    val bitmap =
                        MediaStore.Images.Media.getBitmap(activity!!.contentResolver, selectedImage)
                    try {
                        if (file != null) {
                            plusbtn.visibility = View.GONE
//                            imageView.setBackgroundResource(0)
                            imageView.setImageBitmap(bitmap)


                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                    dismiss()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

        }

        fun getPath(uri: Uri): String {
            val projection = arrayOf(MediaStore.Images.Media.DATA)
            val cursor =
                activity!!.contentResolver.query(uri, projection, null, null, null) ?: return ""
            val column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
            cursor.moveToFirst()
            val s = cursor.getString(column_index)
            cursor.close()
            return s
        }
    }

    private fun getfile(file1: File) {
        file = file1
    }
}