package com.upscribber.signUp

import android.app.Application
import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.upscribber.commonClasses.Constant
import com.upscribber.commonClasses.ModelStatusMsg
import com.upscribber.commonClasses.WebServicesCustomers
import com.upscribber.commonClasses.WebServicesMerchants
import com.upscribber.signUp.finalLogin.ModelSigningUp
import com.upscribber.signUp.signUpOtp.ModelOtp
import com.upscribber.signUp.signupPhoneNumber.ModelSignUp
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import java.io.File

class SignUpRepository(var application: Application) {
    lateinit  var  requestFile:RequestBody;
    val mData = MutableLiveData<ModelSignUp>()
    val mDataSigned = MutableLiveData<ModelSignUp>()
    val mDataOtp = MutableLiveData<ModelOtp>()
    val mDataOtpSigned = MutableLiveData<ModelOtp>()
    val mDataOtp1 = MutableLiveData<ModelOtp>()
    //    private val mDataVerifiedOtp = MutableLiveData<ModelVerifiedOtp>()
    val mDataSigningUp = MutableLiveData<ModelSigningUp>()
    val mDataFailure = MutableLiveData<ModelStatusMsg>()
    val mVerifyEmail = MutableLiveData<ModelVerifyEmail>()
    val mVerifyInvite = MutableLiveData<ModelVerifyEmail>()
    val mVerifyGenerateNewPassword = MutableLiveData<ModelVerifyEmail>()
    val mVerifyFacebook = MutableLiveData<ModelVerifyEmail>()


    // Mobile Number Verify Service//
    fun getCustomerNumber(number: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.verifyMobileNumber(number)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelSignUp = ModelSignUp()
                        val modelStatus = ModelStatusMsg()
                        if (status == "true") {
                            val data = json.getJSONObject("data")
                            modelSignUp.code = data.optString("code")
                            modelSignUp.status = status
                            modelStatus.status = status
                            modelStatus.msg = msg
                            mData.value = modelSignUp
                        } else {
                            val data = json.optJSONObject("data")
                            modelStatus.reg_type = data.optString("reg_type")
                            modelStatus.msg = msg
                            mDataFailure.value = modelStatus
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus

            }

        })
    }

    fun getCustomerNumber1(number: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.verifyMobileNumber(number)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelSignUp = ModelSignUp()
                        val modelStatus = ModelStatusMsg()
                        if (status == "true") {
                            val data = json.getJSONObject("data")
                            modelSignUp.code = data.optString("code")
                            modelSignUp.status = status
                            modelStatus.status = status
                            modelStatus.msg = msg
                            mDataSigned.value = modelSignUp
                        } else {
                            val data = json.optJSONObject("data")
                            modelStatus.reg_type = data.optString("reg_type")
                            modelStatus.msg = msg
                            mDataFailure.value = modelStatus
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus

            }

        })
    }

    fun getmData(): LiveData<ModelSignUp> {
        return mData
    }

fun getmData1(): LiveData<ModelSignUp> {
        return mDataSigned
    }


    // Get Otp Service //
    fun getOtp(contactNo: String, i: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.GetOtp(contactNo)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelOtp = ModelOtp()
                        val modelStatus = ModelStatusMsg()
                        if (status == "true") {
                            val data = json.getJSONObject("data")
                            modelOtp.otp = data.optString("otp")
                            modelOtp.status = status
                            modelOtp.msg = msg
                            modelOtp.value = i
                            modelOtp.contact = contactNo
                            modelStatus.status = status
                            modelStatus.msg = msg
                            mDataOtp.value = modelOtp
                        } else {
                            modelOtp.status = status
                            modelStatus.msg = msg
                            modelStatus.status = status
                            mDataFailure.value = modelStatus
                        }

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus
            }
        })
    }
fun getOtp1(contactNo: String, i: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.GetOtp(contactNo)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelOtp = ModelOtp()
                        val modelStatus = ModelStatusMsg()
                        if (status == "true") {
                            val data = json.getJSONObject("data")
                            modelOtp.otp = data.optString("otp")
                            modelOtp.status = status
                            modelOtp.msg = msg
                            modelOtp.value = i
                            modelOtp.contact = contactNo
                            modelStatus.status = status
                            modelStatus.msg = msg
                            mDataOtpSigned.value = modelOtp
                        } else {
                            modelOtp.status = status
                            modelStatus.msg = msg
                            modelStatus.status = status
                            mDataFailure.value = modelStatus
                        }

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus
            }
        })
    }

    fun getVerifiedOtp(number: String, otp: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.VerifyOtp(number, otp)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {


                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus
            }
        })
    }

    fun getmDataOtp(): LiveData<ModelOtp> {
        return mDataOtp
    }

fun getmDataOtpSigned(): LiveData<ModelOtp> {
        return mDataOtpSigned
    }


    fun getmDataOtp1(): LiveData<ModelOtp> {
        return mDataOtp1
    }


    fun getmDataFailure(): LiveData<ModelStatusMsg> {
        return mDataFailure
    }

    fun getmDataSignedUp(): LiveData<ModelSigningUp> {
        return mDataSigningUp
    }

    fun getSignedUp(
        number: String,
        email: String,
        password: String,
        regType: String,
        androidId: String,
        fireBaseToken: String,
        flag: String,
        dob: String,
        inviteCode: String,
        name: String,
        socialId: String,
        uri: File?
    ) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()
        //..........................technique to storing the image in the web server......................//

        //pass it like this
        var file = uri


        if (file!=null) {
            requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file)
        }else{
            requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), "")
        }

        // MultipartBody.Part is used to send also the actual file name
        val body = MultipartBody.Part.createFormData("profile_image", "profilePhoto", requestFile)
            // add another part within the multipart request
        val number1 = RequestBody.create(MediaType.parse("multipart/form-data"), number)// type
        val email1 = RequestBody.create(MediaType.parse("multipart/form-data"), email)// type
        val password1 = RequestBody.create(MediaType.parse("multipart/form-data"), password)// type
        val regType1 = RequestBody.create(MediaType.parse("multipart/form-data"), regType)// type
        val androidId1 =
            RequestBody.create(MediaType.parse("multipart/form-data"), androidId)// type
        val fireBaseToken1 =
            RequestBody.create(MediaType.parse("multipart/form-data"), fireBaseToken)// type
        val flag1 = RequestBody.create(MediaType.parse("multipart/form-data"), flag)// type
        val dob1 = RequestBody.create(MediaType.parse("multipart/form-data"), dob)// type
        val inviteCode1 =
            RequestBody.create(MediaType.parse("multipart/form-data"), inviteCode)// type
        val name1 = RequestBody.create(MediaType.parse("multipart/form-data"), name)// type
        val socialId1 = RequestBody.create(MediaType.parse("multipart/form-data"), socialId)// type
        val android = RequestBody.create(MediaType.parse("multipart/form-data"), "android")// type
        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> =
            service.SignUp(
                number1,
                email1,
                password1,
                regType1,
                androidId1,
                fireBaseToken1,
                flag1,
                dob1,
                inviteCode1,
                name1,
                socialId1,
                android,
                body
            )
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelSignUp = ModelSigningUp()
                        val modelStatus = ModelStatusMsg()
                        if (status == "true") {
                            val data = json.getJSONObject("data")
                            val user_details = data.getJSONObject("user_details")
                            modelSignUp.contact_no = user_details.optString("contact_no")
                            modelSignUp.email = user_details.optString("email")
                            modelSignUp.password = user_details.optString("password")
                            modelSignUp.reg_type = user_details.optString("reg_type")
                            modelSignUp.device_id = user_details.optString("device_id")
                            modelSignUp.onesignal_id = user_details.optString("onesignal_id")
                            modelSignUp.gender = user_details.optString("gender")
                            modelSignUp.dob = user_details.optString("birthdate")
                            modelSignUp.invite_code = user_details.optString("invite_code")
                            modelSignUp.name = user_details.optString("name")
                            modelSignUp.id = user_details.optString("id")
                            modelSignUp.profile_image = user_details.optString("profile_image")
                            modelSignUp.statusOne = user_details.optString("status")
                            modelSignUp.address = user_details.optString("address")
                            modelSignUp.created_at = user_details.optString("created_at")
                            modelSignUp.updated_at = user_details.optString("updated_at")
                            modelSignUp.customer_wallet = user_details.optString("customer_wallet")
                            modelSignUp.business_wallet = user_details.optString("business_wallet")
                            modelSignUp.refer_by = user_details.optString("refer_by")
                            modelSignUp.stripe_customer_id =
                                user_details.optString("stripe_customer_id")
                            modelSignUp.stripe_account_id =
                                user_details.optString("stripe_account_id")
                            modelSignUp.sales = user_details.optString("sales")
                            modelSignUp.customers = user_details.optString("customers")
                            modelSignUp.auth_code = user_details.optString("auth_code")
                            modelSignUp.msg = msg
                            modelSignUp.status = status
                            modelStatus.status = status
                            modelStatus.msg = msg

                            val editor = Constant.getPrefs(application).edit()
                            editor.putString(Constant.auth_code, modelSignUp.auth_code)
                            editor.apply()

                            mDataSigningUp.value = modelSignUp
                        } else {
                            modelSignUp.msg = msg
                            mDataFailure.value = modelStatus
                        }


                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus

            }

        })
    }


//    ----------------------------------------------verify email-----------------------------------------------//

    fun verifyEmail(email: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.verifyEmail(email)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {

                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelVerifyEmail = ModelVerifyEmail()
                        val modelStatus = ModelStatusMsg()

                        if (status == "true") {
                            modelVerifyEmail.status = status
                            modelStatus.status = status
                            modelVerifyEmail.email = email
                            modelStatus.msg = msg
                            mVerifyEmail.value = modelVerifyEmail
                        } else {
                            modelStatus.msg = msg
                            mDataFailure.value = modelStatus

                        }
                    } catch (e: java.lang.Exception) {
                        e.printStackTrace()
                    }
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus
            }
        })
    }

    fun getVerifyEmail(): LiveData<ModelVerifyEmail> {
        return mVerifyEmail
    }

    fun getVerifyInviteCode(): LiveData<ModelVerifyEmail> {
        return mVerifyInvite
    }


    fun getmVerifyGenerateNewPassword(): LiveData<ModelVerifyEmail> {
        return mVerifyGenerateNewPassword
    }


    fun getVerifyFacebook(): LiveData<ModelVerifyEmail> {
        return mVerifyFacebook
    }


    fun verifyAccount(id: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.verify_account(id)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {

                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelVerifyEmail = ModelVerifyEmail()
                        val modelStatus = ModelStatusMsg()

                        if (status == "true") {
                            modelVerifyEmail.status = status
                            modelStatus.status = status
                            modelStatus.msg = msg
                            mVerifyFacebook.value = modelVerifyEmail
                        } else {
                            val data = json.optJSONObject("data")
                            modelStatus.msg = msg
                            modelStatus.auth_code = data.optString("auth_code")
                            val editor = Constant.getPrefs(application).edit()
                            editor.putString(Constant.auth_code, modelStatus.auth_code)
                            editor.apply()
                            mDataFailure.value = modelStatus

                        }
                    } catch (e: java.lang.Exception) {
                        e.printStackTrace()
                    }
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus
            }
        })

    }


    fun userRefer(auth: String, inviteCode: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.userRefer(auth, inviteCode)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {

                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelVerifyEmail = ModelVerifyEmail()
                        val modelStatus = ModelStatusMsg()

                        if (status == "true") {
                            modelVerifyEmail.status = status
                            modelStatus.status = status
                            modelStatus.msg = msg
                            mVerifyInvite.value = modelVerifyEmail
                        } else {
                            modelVerifyEmail.status = status
                            modelVerifyEmail.msg = msg
                            modelStatus.status = status
                            modelStatus.msg = msg
                            mVerifyInvite.value = modelVerifyEmail

                        }
                    } catch (e: java.lang.Exception) {
                        e.printStackTrace()
                    }
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus
            }
        })
    }

    fun setForgetPassword(
        number: String,
        password: String,
        fireBaseToken: String
    ) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.forgotPassword(number, password, fireBaseToken)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {

                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelVerifyEmail = ModelVerifyEmail()
                        val modelStatus = ModelStatusMsg()

                        if (status == "true") {
                            val data = json.getJSONObject("data")
                            modelVerifyEmail.auth_code = data.optString("auth_code")
                            modelVerifyEmail.status = status
                            modelStatus.status = status
                            modelStatus.msg = msg


                            val editor = Constant.getPrefs(application).edit()
                            editor.putString(Constant.auth_code, modelVerifyEmail.auth_code)
                            editor.apply()

                            mVerifyGenerateNewPassword.value = modelVerifyEmail
                        } else {
                            modelStatus.status = status
                            modelStatus.msg = msg
                            mDataFailure.value = modelStatus

                        }
                    } catch (e: java.lang.Exception) {
                        e.printStackTrace()
                    }
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus
            }
        })

    }

    fun getCountryNameCodes() {
        val retrofit = Retrofit.Builder()
            .baseUrl("http://ip-api.com/")
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.getCountryCode()
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {

                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelStatus = ModelStatusMsg()

                        if (status == "success") {
                            modelStatus.countryCode = json.optString("countryCode")
                            modelStatus.status = status
                            modelStatus.msg = msg


                            val editor = Constant.getPrefs(application).edit()
                            editor.putString(Constant.countryCode, modelStatus.countryCode)
                            editor.apply()

                        } else {
                            modelStatus.status = status
                            modelStatus.msg = msg
                            mDataFailure.value = modelStatus

                        }
                    } catch (e: java.lang.Exception) {
                        e.printStackTrace()
                    }
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus
            }
        })

    }

    fun sendOtpWithMobileVerification(contactNumber: String, type: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.sendOtpWithMobileVerification(contactNumber, type)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelOtp = ModelOtp()
                        val modelStatus = ModelStatusMsg()
                        if (status == "true") {
                            val data = json.getJSONObject("data")
                            modelOtp.otp = data.optString("otp")
                            modelOtp.msg = msg
                            modelOtp.contact = contactNumber
                            modelOtp.status = status
                            modelStatus.status = status
                            modelStatus.msg = msg
                            mDataOtp.value = modelOtp
                        } else {
                            modelStatus.msg = msg
                            modelStatus.status = status
                            mDataFailure.value = modelStatus
                        }

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus
            }
        })

    }

    fun getAuthCode(contact: String, fireBaseToken: String, deviceName: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> =
            service.getAuthCode(contact, fireBaseToken, deviceName)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelSignUp = ModelSigningUp()
                        val modelStatus = ModelStatusMsg()
                        if (status == "true") {
                            val data = json.getJSONObject("data")
                            modelSignUp.auth_code = data.optString("auth_code")
                            modelSignUp.msg = msg
                            modelSignUp.status = status
                            modelStatus.status = status
                            modelStatus.msg = msg

                            val editor = Constant.getPrefs(application).edit()
                            editor.putString(Constant.auth_code, modelSignUp.auth_code)
                            editor.apply()

                            mDataSigningUp.value = modelSignUp
                        } else {
                            modelSignUp.msg = msg
                            mDataFailure.value = modelStatus
                        }


                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus

            }

        })

    }

    fun sendOtpWithMobileVerification1(contactNumber: String, type: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(Constant.BASE_URL)
            .build()

        val service = retrofit.create(WebServicesCustomers::class.java)
        val call: Call<ResponseBody> = service.sendOtpWithMobileVerification(contactNumber, type)
        call.enqueue(object : Callback<ResponseBody> {

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                if (response.isSuccessful) {
                    try {
                        val res = response.body()!!.string()
                        val json = JSONObject(res)
                        val status = json.optString("status")
                        val msg = json.optString("msg")
                        val modelOtp = ModelOtp()
                        val modelStatus = ModelStatusMsg()
                        if (status == "true") {
                            val data = json.getJSONObject("data")
                            modelOtp.otp = data.optString("otp")
                            modelOtp.msg = msg
                            modelOtp.contact = contactNumber
                            modelOtp.status = status
                            modelStatus.status = status
                            modelStatus.msg = msg
                            mDataOtp1.value = modelOtp
                        } else {
                            modelStatus.msg = msg
                            modelStatus.status = status
                            mDataFailure.value = modelStatus
                        }

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                val modelStatus = ModelStatusMsg()
                modelStatus.status = "false"
                modelStatus.msg = "Network Error!"
                mDataFailure.value = modelStatus
            }
        })

    }


}