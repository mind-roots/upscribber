package com.upscribber.signUp.finalLogin

import android.annotation.SuppressLint
import android.app.Activity
import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.upscribber.dashboard.DashboardActivity
import com.upscribber.R
import java.util.*
import android.provider.Settings.Secure
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import com.upscribber.commonClasses.Constant
import com.upscribber.signUp.SignUpViewModel
import com.upscribber.signUp.clicks


class SignUpWelcomeFragment : Fragment() {

    lateinit var mbackToDetails: ImageView
    lateinit var meditText: EditText
    lateinit var textView8: TextView
    lateinit var mbtndone: TextView
    var flag: Boolean = true
    var flag2: Boolean = false
    lateinit var imgMale: ImageView
    lateinit var imgFemale: ImageView
    lateinit var progressBar2: ConstraintLayout
    lateinit var update: clicks
    var status: Boolean = true
    var statusBirthdate: Boolean = false
    var statusFinal: Boolean = false
    lateinit var mViewModel: SignUpViewModel
    @SuppressLint("HardwareIds")


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_sign_up_welcome, container, false)
        mViewModel = ViewModelProviders.of(this)[SignUpViewModel::class.java]
        mbackToDetails = view!!.findViewById(R.id.back_to_details)
        meditText = view.findViewById(R.id.editText3)
        mbtndone = view.findViewById(R.id.btn_done)
        imgMale = view.findViewById(R.id.img_male)
        imgFemale = view.findViewById(R.id.img_female)
        progressBar2 = view.findViewById(R.id.progressBar2)
        textView8 = view.findViewById(R.id.textView8)
        ObserversInit()

        clickEvents()

        return view
    }

    private fun ObserversInit() {
        textView8.text = "Welcome " +update.getName() + "!"

        mViewModel.getSignedUp().observe(this, androidx.lifecycle.Observer {
            if (it.status == "true") {
                progressBar2.visibility = View.GONE
                val intent = Intent(activity, DashboardActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(intent)
            } else {
                Toast.makeText(context, it.msg, Toast.LENGTH_LONG).show()
            }
        })

        mViewModel.getStatus().observe(this, androidx.lifecycle.Observer {
            progressBar2.visibility = View.GONE
            if (it.msg.isNotEmpty()) {
                Toast.makeText(context, it.msg, Toast.LENGTH_LONG).show()
            }

        })
    }

    @SuppressLint("SetTextI18n")
    private fun clickEvents() {
        val fireBaseToken: String = Constant.getPrefs(context!!).getString("deviceId", "")
        val android_id = Secure.getString(context!!.contentResolver, Secure.ANDROID_ID)
        val number = update.getNumber()
        val phoneNumber = number.replace(" ", "")
        val password = update.getPassword()
        var regType =""
        if(password == ""){
            regType = "2"
        }else{
            regType = "1"
        }
        val social_id = update.loginIddd()


        mbtndone.setOnClickListener {
            if (statusBirthdate) {
                var gender = "male"
                if (!flag) {
                    gender = "female"
                }
                progressBar2.visibility = View.VISIBLE
//                mViewModel.getSignUp(
//                    Constant.getCountryDialCode(activity!!)+ phoneNumber,
////                    "+1"+ phoneNumber,
//                    update.getEmail(),
//                    password,
//                    regType,
//                    android_id,
//                    fireBaseToken,
//                    gender,
//                    meditText.text.toString(),
//                    "",
//                    update.getName(), social_id, file
//                )

            } else {
                Toast.makeText(context, "Please Select your DOB", Toast.LENGTH_LONG).show()
            }

        }

        mbackToDetails.setOnClickListener {
            update.back(4)
        }

        imgMale.setOnClickListener {
            if (!flag) {
                flag = true
                flag2 = false
                imgMale.setImageResource(R.mipmap.male_selected)
                imgFemale.setImageResource(R.mipmap.female_unselected)
//                status = true
            }
        }

        imgFemale.setOnClickListener {
            if (!flag2) {
                flag2 = true
                flag = false
                imgFemale.setImageResource(R.mipmap.female_selected)
                imgMale.setImageResource(R.mipmap.male_unselected)
//                status=true
            }

        }

        meditText.setOnClickListener {

            val c = Calendar.getInstance()
            val year = c.get(Calendar.YEAR)
            val month = c.get(Calendar.MONTH)
            val day = c.get(Calendar.DAY_OF_MONTH)

            val dpd = DatePickerDialog(context, DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->

                meditText.setText("" + year  + "/" + (month + 1) + "/"  + dayOfMonth )
            }, year, month, day)

            if (meditText.text.toString().trim() !== "") {
                statusBirthdate = true
            }


//            dpd.setButton(DialogInterface.BUTTON_NEUTRAL, "Never Ends") { dialog, which ->
//                if (which == DialogInterface.BUTTON_NEUTRAL) {
////                    meditText.setText("")
//                }
//            }
            dpd.show()

        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        update = context as clicks
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        update = activity as clicks
    }

}