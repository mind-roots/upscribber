package com.upscribber.signUp.signupPhoneNumber

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.upscribber.commonClasses.Constant
import com.upscribber.R
import com.upscribber.login.LoginOtpActivity
import com.upscribber.signUp.SignUpViewModel
import com.upscribber.signUp.clicks


class SignUpPhoneFragment : Fragment() {

    lateinit var update: clicks
    lateinit var mtvphone: TextView
    lateinit var textView3: TextView
    lateinit var mimgBack: ImageView
    lateinit var editText2: TextView
    lateinit var done: ImageView
    lateinit var progressBar: ConstraintLayout
    var flag = 0
    lateinit var mViewModel: SignUpViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_sign_up_phone, container, false)
        mViewModel = ViewModelProviders.of(this)[SignUpViewModel::class.java]
        mtvphone = view.findViewById(R.id.tv_phone)
        mimgBack = view.findViewById(R.id.img_back)
        textView3 = view.findViewById(R.id.textView3)
        done = view.findViewById(R.id.done)
        editText2 = view.findViewById(R.id.editText2)
        progressBar = view.findViewById(R.id.progressBar)
        editText2.text = update.getNumber()

        getPhoneNumber()
        ObserversInit()
        clickEvents()
        return view
    }

    private fun ObserversInit() {
        mViewModel.getPhoneData().observe(this, Observer {
            progressBar.visibility = View.GONE
            if (it.status == "true") {
                update.setNumber(editText2.text.toString().trim())
                val number = editText2.text.toString().trim().replace(" ", "")
                mViewModel.getOtp("+1$number", "2")
            } else {
                Toast.makeText(context, it.msg, Toast.LENGTH_SHORT).show()
            }
        })

        mViewModel.getStatus().observe(this, Observer {
            progressBar.visibility = View.GONE
            if (it.msg == "Phone no already exist") {
                update.setNumber(editText2.text.toString().trim())
                val number = editText2.text.toString().trim().replace(" ", "")
                mViewModel.sendOtpWithMobileVerification1("+1$number", "3")
            } else if (it.msg.isNotEmpty()) {
                Toast.makeText(context, it.msg, Toast.LENGTH_SHORT).show()
            }
        })

        mViewModel.getmDataOtp().observe(this, Observer {
            progressBar.visibility = View.GONE
            if (it.status == "true") {
                update.setOTP(it.otp)
                update.next(2)
                Toast.makeText(activity!!, it.msg, Toast.LENGTH_LONG).show()

            }
        })


        mViewModel.getmDataOtp1().observe(this, Observer {
            progressBar.visibility = View.GONE
            if (it.status == "true") {
                Toast.makeText(activity,it.msg ,Toast.LENGTH_LONG).show()
                startActivity(Intent(activity,LoginOtpActivity::class.java).putExtra("modelOtp",it))
            }
        })

    }

    private fun getPhoneNumber() {
        if (update.getNumber().length == 12) {
            Constant.hideKeyboard(context!!, editText2)
            done.visibility = View.VISIBLE
            flag = 1
        } else {
            done.visibility = View.GONE
            flag = 0
        }

    }

    private fun clickEvents() {
        mtvphone.setOnClickListener {
            if (flag == 1) {
                progressBar.visibility = View.VISIBLE
                val number = editText2.text.toString().trim()
                val phoneNumber = number.replace(" ", "")
                mViewModel.getSignUpNumber("+1" + phoneNumber)
//                mViewModel.getSignUpNumber("+1"+ phoneNumber)
            } else {
                Toast.makeText(
                    context,
                    "Please enter a valid 10 digit phone number",
                    Toast.LENGTH_LONG
                ).show()
            }
        }


        editText2.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                val text = editText2.text.toString()
                val textLength = editText2.text.length

                if (text.endsWith("-") || text.endsWith(" ") || text.endsWith(" "))
                    return
                if (textLength == 4) {
                    editText2.text = StringBuilder(text).insert(text.length - 1, " ").toString()
                    (editText2 as EditText).setSelection(editText2.text.length)
                }
                if (textLength == 8) {
                    editText2.text = StringBuilder(text).insert(text.length - 1, " ").toString()
                    (editText2 as EditText).setSelection(editText2.text.length)

                }
//                if (textLength == 11){
//                    editText2.setText(StringBuilder(text).insert(text.length - 1, " ").toString())
//                    (editText2 as EditText).setSelection(editText2.text.length)
//
//                }
                if (textLength == 12) {
                    flag = 1
                    done.visibility = View.VISIBLE
                    Constant.hideKeyboard(activity!!, editText2)
                } else {
                    done.visibility = View.GONE
                    flag = 0
                }

            }

            override fun afterTextChanged(s: Editable) {
            }
        })

        mimgBack.setOnClickListener {
            update.back(1)
        }

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        update = context as clicks
    }

    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        update = activity as clicks
    }

}