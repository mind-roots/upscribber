package com.upscribber.signUp

/**
 * Created by amrit on 27/2/19.
 */
interface clicks {

    fun back(i: Int)
    fun next(i: Int)
    fun getNumber():String
    fun setNumber(i: String)
    fun getOtp() : String
    fun setOTP(i: String)
    fun getName() : String
    fun setName(i: String)
    fun setLogin(i: String)
    fun getEmail() : String
    fun setEmail(i: String)
    fun getPassword() : String
    fun setPassword(i: String)
    fun login() : String
    fun emailLogin() : String
    fun loginIddd() : String
    fun getLoginEmail(i : String)
    fun loginId(i : String)

}