package com.upscribber.signUp

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.cardview.widget.CardView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.upscribber.R
import com.upscribber.commonClasses.Constant
import com.upscribber.upscribberSeller.subsriptionSeller.subscriptionMain.customerCheckout.ScannedCustomerActivity
import kotlinx.android.synthetic.main.cost_too_much.*
import kotlinx.android.synthetic.main.invite_code_alert.*

class InviteCodeActivity : AppCompatActivity() {

    private lateinit var mInviteToolbar: Toolbar
    private lateinit var mNextSignUp: TextView
    private lateinit var mEditText: EditText
    lateinit var mViewModel: SignUpViewModel
    lateinit var cardInvite: CardView
    lateinit var mDone: ImageView
    var flag = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_invite_code)
        mViewModel = ViewModelProviders.of(this)[SignUpViewModel::class.java]

        initz()
        setToolbar()
        clickEvents()
        observerInit()

    }

    private fun observerInit() {
        mViewModel.getInvite().observe(this, Observer {
            if (it.status == "true") {
                val mDialogView = LayoutInflater.from(this).inflate(R.layout.invite_code_alert, null)
                val mBuilder = AlertDialog.Builder(this)
                    .setView(mDialogView)
                val mAlertDialog = mBuilder.show()
                mAlertDialog.setCancelable(false)
                mAlertDialog.window.setBackgroundDrawableResource(R.drawable.bg_alert)
                mAlertDialog.button.setOnClickListener {
                    mAlertDialog.dismiss()
                    finish()
                }
            } else {
                Toast.makeText(this, it.msg, Toast.LENGTH_SHORT).show()
            }
        })
    }

    private fun setToolbar() {

        setSupportActionBar(mInviteToolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_black_24dp)

    }

    private fun initz() {
        mInviteToolbar = findViewById(R.id.invite_toolbar)
        mNextSignUp = findViewById(R.id.nextSignUp)
        mDone = findViewById(R.id.done)
        mEditText = findViewById(R.id.editText)
        cardInvite = findViewById(R.id.cardInvite)

    }

    private fun clickEvents() {
        mNextSignUp.setOnClickListener {
            if (flag == 1) {
                val auth = Constant.getPrefs(this).getString(Constant.auth_code, "")
                mViewModel.setInvite(auth, mEditText.text.toString())

            } else {
                Toast.makeText(this, "Please enter valid 8 digit invite code", Toast.LENGTH_SHORT).show()
            }
        }


        mEditText.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (s.length == 8) {
                    mDone.visibility = View.VISIBLE
                    flag = 1
                } else {
                    mDone.visibility = View.GONE
                    flag = 0
                }
            }

            override fun afterTextChanged(s: Editable) {

            }
        })


        cardInvite.setOnClickListener {
            startActivity(
                Intent(
                    this,
                    ScannedCustomerActivity::class.java
                ).putExtra("generateCode", "generateCode")
            )
        }

    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}
