package com.upscribber.signUp

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.upscribber.commonClasses.ModelStatusMsg
import com.upscribber.signUp.finalLogin.ModelSigningUp
import com.upscribber.signUp.signUpOtp.ModelOtp
import com.upscribber.signUp.signupPhoneNumber.ModelSignUp
import java.io.File

class SignUpViewModel(application: Application) : AndroidViewModel(application) {

    var signUpRepository: SignUpRepository = SignUpRepository(application)

    fun getSignUpNumber(number: String) {
        signUpRepository.getCustomerNumber(number)
    }

    fun getSignUpNumber1(number: String) {
        signUpRepository.getCustomerNumber1(number)
    }

    fun getPhoneData(): LiveData<ModelSignUp> {
        return signUpRepository.getmData()
    }

    fun getPhoneData1(): LiveData<ModelSignUp> {
        return signUpRepository.getmData1()
    }

    fun getmDataOtp(): LiveData<ModelOtp> {
        return signUpRepository.getmDataOtp()
    }

    fun getmDataOtpSigned(): LiveData<ModelOtp> {
        return signUpRepository.getmDataOtpSigned()
    }

    fun getmDataOtp1(): LiveData<ModelOtp> {
        return signUpRepository.getmDataOtp1()
    }

    fun getOtp(contact_no: String, i: String) {
        signUpRepository.getOtp(contact_no, i)
    }

    fun getOtp1(contact_no: String, i: String) {
        signUpRepository.getOtp1(contact_no, i)
    }

    fun getStatus(): LiveData<ModelStatusMsg> {
        return signUpRepository.getmDataFailure()
    }

    fun getOtpVerified(number: String, otp: String) {
        signUpRepository.getVerifiedOtp(number, otp)

    }

//    fun getOtpVerify(): LiveData<ModelVerifiedOtp> {
//        return signUpRepository.getmDataOtpVerified()
//    }

    fun getSignedUp(): LiveData<ModelSigningUp> {
        return signUpRepository.getmDataSignedUp()
    }

    fun getSignUp(
        number: String,
        email: String,
        password: String,
        regType: String,
        androidId: String,
        fireBaseToken: String,
        flag: String,
        dob: String,
        inviteCode: String,
        name: String,
        socialId: String,
        file: File?
    ) {
        signUpRepository.getSignedUp(
            number,
            email,
            password,
            regType,
            androidId,
            fireBaseToken,
            flag,
            dob,
            inviteCode,
            name,
            socialId,
            file
        )

    }

    fun getEmailId(email: String) {
        signUpRepository.verifyEmail(email)
    }

    fun getEmail(): LiveData<ModelVerifyEmail> {
        return signUpRepository.getVerifyEmail()
    }


    fun getInvite(): LiveData<ModelVerifyEmail> {
        return signUpRepository.getVerifyInviteCode()
    }

    fun getmVerifyGenerateNewPassword(): LiveData<ModelVerifyEmail> {
        return signUpRepository.getmVerifyGenerateNewPassword()
    }


    fun getVerifyFacebook(): LiveData<ModelVerifyEmail> {
        return signUpRepository.getVerifyFacebook()
    }


    fun getLoginId(id: String) {
        signUpRepository.verifyAccount(id)

    }

    fun setInvite(auth: String, invite: String) {
        signUpRepository.userRefer(auth, invite)

    }

    fun setNewPasswordApi(
        number: String,
        password: String,
        fireBaseToken: String
    ) {
        signUpRepository.setForgetPassword(number, password, fireBaseToken)

    }

    fun getCountryNameCodes() {
        signUpRepository.getCountryNameCodes()

    }

    fun sendOtpWithMobileVerification(contact_number: String, type: String) {
        signUpRepository.sendOtpWithMobileVerification(contact_number, type)
    }

    fun getAuthCode(contact: String, fireBaseToken: String, device_name: String) {
        signUpRepository.getAuthCode(contact, fireBaseToken, device_name)

    }

    fun sendOtpWithMobileVerification1(contact_number: String, type: String) {
        signUpRepository.sendOtpWithMobileVerification1(contact_number, type)
    }
}