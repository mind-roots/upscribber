package com.upscribber.helpCenter

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.R

class HelpCenterActivity : AppCompatActivity() {

    lateinit var toolbar: Toolbar
    lateinit var toolbarTitle: TextView
    lateinit var include2: RecyclerView
    lateinit var include3: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_help_center)
        initz()
       setToolbar()

        setAdapter()
    }

    private fun setToolbar() {
        setSupportActionBar(toolbar)
        title = ""
        toolbarTitle.text = "Help Center"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white)

    }

    private fun initz() {
        toolbar = findViewById(R.id.helpToolbar)
        toolbarTitle = findViewById(R.id.titleHelp1)
        include2 = findViewById(R.id.include25)
        include3 = findViewById(R.id.include33)

    }

    private fun setAdapter() {

        include2.layoutManager = LinearLayoutManager(this)
        include2.adapter = faqAdapter(this, getList())

        include3.layoutManager = LinearLayoutManager(this)
        include3.adapter = RealtedAdapter(this, getList1())
    }




    private fun getList(): ArrayList<ModelFaq> {

        val arrayList: ArrayList<ModelFaq> = ArrayList()

        val modelFaq1 = ModelFaq()
        modelFaq1.faq = "How can i reedem my subscriptions?"
        arrayList.add(modelFaq1)

        val modelFaq2 = ModelFaq()
        modelFaq2.faq = "How can i share my subscription to someone?"
        arrayList.add(modelFaq2)

        val modelFaq3 = ModelFaq()
        modelFaq3.faq = "How can i manage my subscription?"
        arrayList.add(modelFaq3)

        val modelFaq4 = ModelFaq()
        modelFaq4.faq = "Can i use cards to purchase my subscriptions?"
        arrayList.add(modelFaq4)

        return arrayList

    }


    private fun getList1(): ArrayList<ModelRelated> {

        val arrayList: ArrayList<ModelRelated> = ArrayList()

        val modelRelated1 = ModelRelated()
        modelRelated1.relatedTopics = "General"
        arrayList.add(modelRelated1)

        val modelRelated2 = ModelRelated()
        modelRelated2.relatedTopics = "User Account"
        arrayList.add(modelRelated2)

        val modelRelated3 = ModelRelated()
        modelRelated3.relatedTopics = "Subscriptions"
        arrayList.add(modelRelated3)

        val modelRelated4 = ModelRelated()
        modelRelated4.relatedTopics = "Redementions"
        arrayList.add(modelRelated4)

        val modelRelated5 = ModelRelated()
        modelRelated5.relatedTopics = "Payments"
        arrayList.add(modelRelated5)

        return arrayList

    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item!!.itemId == android.R.id.home) {
            finish()
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.help_center, menu)
        return super.onCreateOptionsMenu(menu)
    }


}
