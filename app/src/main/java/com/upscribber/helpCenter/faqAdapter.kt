package com.upscribber.helpCenter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.help_center_layout1.view.*
import android.content.Intent.FLAG_ACTIVITY_NEW_TASK
import com.upscribber.R


class faqAdapter(private val mContext: Context, private val list: ArrayList<ModelFaq>) :
    RecyclerView.Adapter<faqAdapter.MyViewHolder>()  {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): faqAdapter.MyViewHolder {

        val v = LayoutInflater.from(mContext).inflate(R.layout.help_center_layout1, parent, false)
        return faqAdapter.MyViewHolder(v)

    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: faqAdapter.MyViewHolder, position: Int) {
        val model = list[position]

        holder.itemView.faq.text = model.faq

        holder.itemView.setOnClickListener {
            val myactivity = Intent(mContext.applicationContext, FaqNextActivity::class.java)
            myactivity.addFlags(FLAG_ACTIVITY_NEW_TASK)
            mContext.applicationContext.startActivity(myactivity)
        }
    }



    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}