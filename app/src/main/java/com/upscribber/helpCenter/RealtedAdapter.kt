package com.upscribber.helpCenter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.upscribber.R
import kotlinx.android.synthetic.main.help_center_layout2.view.*


class RealtedAdapter(private val mContext: Context, private val list: ArrayList<ModelRelated>) :
    RecyclerView.Adapter<RealtedAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(mContext).inflate(R.layout.help_center_layout2, parent, false)
        return MyViewHolder(v)

    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val model = list[position]

        holder.itemView.help_related.text = model.relatedTopics

        holder.itemView.setOnClickListener {
            val myactivity = Intent(mContext.applicationContext, FaqNextActivity::class.java)
            myactivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            mContext.applicationContext.startActivity(myactivity)
        }

    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}